<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

require(__DIR__ . '/web/Auth.php');
//Auth::routes();

Route::group(['middleware' => ['auth', 'roles']], function () {
    //Temp analytics test
    require(__DIR__ . '/web/Tests/AnalyticsTest.php');

    // Dashboard
    require(__DIR__ . '/web/Dashboard.php');

    // CMSUserActions
    require(__DIR__ . '/web/Bi/CMSUserActions.php');

    // Users
    require(__DIR__ . '/web/Bi/Users.php');

    // Scheduler
    require(__DIR__ . '/web/Bi/Scheduler.php');

    // Experiments
    require(__DIR__ . '/web/Bi/Experiments.php');

    // Changes log
    require(__DIR__ . '/web/Bi/ChangesLog.php');

    // EntityLock
    require(__DIR__ . '/web/Top/EntityLock.php');

    // Reports
    Route::group(['prefix' => 'reports', 'namespace' => 'Reports', 'as' => 'reports.'], function () {

        // Reports - Bo
        require(__DIR__ . '/web/Bi/Reports/Bo.php');

        // Reports - Filters
        require(__DIR__ . '/web/Bi/Reports/Filters.php');

        // Reports -  Logs
        require(__DIR__ . '/web/Bi/Reports/Logs.php');

        // Reports -  Alerts
        require(__DIR__ . '/web/Bi/Reports/Alerts.php');

        // Reports -  Publishers
        require(__DIR__ . '/web/Bi/Reports/Publishers.php');

        // Reports -  Track History
        require(__DIR__ . '/web/Bi/Reports/TrackHistory.php');

        // Reports -  ScraperUi
        require(__DIR__ . '/web/Bi/Reports/ScraperUi.php');

        // Reports - General Controller
        require(__DIR__ . '/web/Bi/Reports/BoReports.php');
    });

    // BO
    Route::group(['namespace' => 'Bo', 'prefix' => 'bo', 'as' => 'bo.'], function () {

        // BO -  Advertisers
        require(__DIR__ . '/web/Bi/Bo/Advertisers.php');

        // BO -  Publishers
        require(__DIR__ . '/web/Bi/Bo/Publishers.php');

        // BO -  Accounts
        require(__DIR__ . '/web/Bi/Bo/Accounts.php');

        //Bo - Companies
        require(__DIR__ . '/web/Bi/Bo/Companies.php');

        //Bo - Descrepancies
        require(__DIR__ . '/web/Bi/Bo/Discrepancies.php');

        //Bo - Budget Manager
        require(__DIR__ . '/web/Bi/Bo/BudgetManager.php');

        //Bo - BayesianTasks
        require(__DIR__ . '/web/Bi/Bo/BayesianTasks.php');

        //Bo - BayesianTasks
        require(__DIR__ . '/web/Bi/Bo/BayesianData.php');

        //Bo - Changes Log
        require(__DIR__ . '/web/Bi/Bo/ChangesLog.php');

        Route::group(['prefix' => 'old', 'as' => 'old.'], function() {
            require(__DIR__ . '/web/Bi/Bo/OldAdvertisers.php');
        });
    });

    /*
    Route::resource('sites', 'Sites\SitesController');

    Route::group(['namespace' => 'Sites'], function () {

        // Direct paths to sites
        // Used for direct interaction of widget blades
        Route::get('site-content-type-fields/select2', 'ContentType\SiteContentTypeFieldsController@select2')
            ->name('site-content-type-fields.select2');

        Route::get('sites-content-types/select2', 'ContentType\SiteContentTypesController@select2')
            ->name('sites-content-types.select2');

        Route::group(['prefix' => 'sites/{site}', 'as' => 'sites.{site}.'], function () {

            // Sites
            require(__DIR__ . '/web/Top/Sites.php');

            // Sites - Sections
            require(__DIR__ . '/web/Top/Sections.php');

            // Sites - AB Tests
            require(__DIR__ . '/web/Top/ABTests.php');

            // Sites - ContentTypes
            require(__DIR__ . '/web/Top/ContentTypes.php');

            // Sites - Media
            require(__DIR__ . '/web/Top/Media.php');

            // Sites - Migrations
            require(__DIR__ . '/web/Top/Migrations.php');

            // Sites - Templates
            require(__DIR__ . '/web/Top/Templates.php');

            // Sites - Widgets
            require(__DIR__ . '/web/Top/Widgets.php');

            // Sites - Devices
            require(__DIR__ . '/web/Top/Devices.php');

            // Sites - Domains
            require(__DIR__ . '/web/Top/Domains.php');

            // Sites - Members
            require(__DIR__ . '/web/Top/Members.php');

            // Sites - Member Groups
            require(__DIR__ . '/web/Top/MemberGroups.php');

            // Sites - ContentsAuthors
            require(__DIR__ . '/web/Top/ContentsAuthors.php');

            // Sites - Settings
            require(__DIR__ . '/web/Top/Settings.php');

            // Sites - Ribbons
            require(__DIR__ . '/web/Top/Ribbons.php');

            Route::group(['middleware' => ['trim.null']], function () {
                // Sites - Posts
                require(__DIR__ . '/web/Top/Posts.php');

                // Sites - Comments
                require(__DIR__ . '/web/Top/Comments.php');

                // Sites - Labels
                require(__DIR__ . '/web/Top/Labels.php');

                // Sites - pages
                require(__DIR__ . '/web/Top/Pages.php');

                // Sites - menus
                require(__DIR__ . '/web/Top/Menus.php');

                // Sites - Popups
                require(__DIR__ . '/web/Top/Popups.php');

                // Sites - Popups
                require(__DIR__ . '/web/Top/WidgetTemplates.php');

                // Sites - Dynamic list
                require(__DIR__ . '/web/Top/DynamicList.php');

                // Sites - Contact us
                require(__DIR__ . '/web/Top/ContactUs.php');

                // Sites - Rate it
                require(__DIR__ . '/web/Top/RateIt.php');
//
//            //Sites - Like It
//            require(__DIR__ . '/web/Top/LikeIt.php');
            });
        });
    });
    */
});
