<?php

Route::resource('menus', 'Menus\SiteMenusController', [
    'parameters' => [
        'menus' => 'menu'
    ],
    'except'       => [
        'show'
    ]
]);

Route::get('menus/{menu}/ajax-alert', 'Menus\SiteMenusController@ajaxAlert')
    ->name('menus.{menu}.ajax-alert');

Route::resource('menus/{menu}/items', 'Menus\SiteMenuItemsController', [
    'as' => 'menus.{menu}'
]);
