<?php

Route::resource('comments', 'SiteCommentsController');

Route::get('comments/choice', 'SiteCommentsController@choice')
    ->name('comments.choice');

Route::post('comments/datatable', 'SiteCommentsController@datatable')
    ->name('comments.datatable');

Route::get('comments/{comment}/ajax-modal', 'SiteCommentsController@ajaxModal')
    ->name('comments.ajax-modal');
