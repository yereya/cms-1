<?php

Route::resource('site-migrations', 'SiteMigrationsController');

Route::get('site-migrations/{migration_id}/ajax-modal', 'SiteMigrationsController@ajaxModal')
    ->name('site-migrations.{migration_id}.ajax-modal');
Route::get('site-migrations/{migration_id}/toggle-property', 'SiteMigrationsController@toggleProperty')
    ->name('site-migrations.{migration_id}.toggle-property');
