<?php
Route::resource('site-devices', 'SiteDevicesController');
Route::get('site-devices/{device_id}/ajax-modal', 'SiteDevicesController@ajaxModal')
    ->name('site-devices.{device_id}.ajax-modal');