<?php

Route::resource('dynamic-list', 'DynamicListController', [
    "except"     => ['show'],
    'parameters' => ['dynamic-list' => 'dynamic_list_record'],
]);

Route::group(['prefix' => 'dynamic-list', 'as' => 'dynamic-list.'], function () {
    Route::post('datatable', 'DynamicListController@datatable')
        ->name('datatable');

    Route::get('ajax-alert', 'DynamicListController@ajaxAlert')
        ->name('ajax-alert');

    Route::get('select2', 'DynamicListController@select2')
        ->name('select2');

    Route::post('{dynamic_list}/duplicate', 'DynamicListController@duplicate')
        ->name('{dynamic_list}.duplicate');
});
