<?php
Route::resource('content-types', 'ContentType\SiteContentTypesController', [
    'parameters' => [
        'content-types' => 'content_type'
    ]
]);

Route::get('fields/find', 'ContentType\SiteContentTypeFieldsController@find')
    ->name('fields.find');

Route::group(['namespace' => 'ContentType', 'prefix' => 'content-types', 'as' => 'content-types.'], function () {

    Route::get('ajax-modal', 'SiteContentTypesController@ajaxModal')
        ->name('ajax-modal');

    Route::group(['prefix' => '{content_type}', 'as' => '{content_type}.'], function () {

        // FIELDS
        Route::get('content-field/{type}', 'SiteFieldTypesController@show')
            ->name('content-field.show');

        Route::resource('fields', 'SiteContentTypeFieldsController',
            ['except' => ['show']]
        );
        Route::get('fields/{field}/duplicate', 'SiteContentTypeFieldsController@duplicate')
            ->name('fields.{field}.duplicate');
        Route::get('fields/{field}/ajax-modal',
            'SiteContentTypeFieldsController@ajaxModal')
            ->name('fields.ajax-modal');
        Route::get('fields/{field_id}/ajax-alert',
            'SiteContentTypeFieldsController@ajaxAlert')
            ->name('fields.ajax-alert');
        Route::post('fields/datatable', 'SiteContentTypeFieldsController@datatable')
            ->name('fields.datatable');
        Route::post('fields/priority', 'SiteContentTypeFieldsController@priority')
            ->name('fields.priority');

        Route::resource('field-groups', 'SiteContentTypeFieldGroupsController',
            [
                'only'       => ['store', 'destroy', 'edit', 'update', 'index', 'create'],
                'parameters' => [
                    'content-types' => 'content_type',
                    'field-groups'  => 'field_group'
                ]
            ]);
        Route::post('field-groups/order',
            'SiteContentTypeFieldGroupsController@order')
            ->name('field-groups.order');


        Route::get('field-groups/ajax-modal',
            'SiteContentTypeFieldGroupsController@ajaxModal')
            ->name('field-groups.ajax-modal');
        Route::get('field-groups/{field_group}/ajax-alert',
            'SiteContentTypeFieldGroupsController@ajaxAlert')
            ->name('field-groups.{field_group}.ajax-alert');

        Route::post('fields/ajax-order',
            'SiteContentTypeFieldsController@ajaxOrder')
            ->name('fields.ajax-order');

    });


});
