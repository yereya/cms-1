<?php

Route::resource('domains', 'SiteDomainsController');
Route::get('domains/{domain}/ajax-modal', 'SiteDomainsController@ajaxModal')
    ->name('domains.ajax-modal');