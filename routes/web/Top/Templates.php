<?php


Route::group(['namespace' => 'Templates', 'prefix' => 'templates', 'as' => 'templates.'], function () {

    // Ajax Alert
    Route::get('ajax-alert', 'SiteTemplatesController@ajaxAlert')
        ->name('ajax-alert');

    //Ajax modal
    Route::get('ajax-modal', 'SiteTemplatesController@ajaxModal')
        ->name('ajax-modal');


    //Ajax builder
    Route::get('{template}/builder', 'SiteTemplatesController@builder')
        ->name('builder');

    // Builder Items
    Route::get('{template}/builder-items', 'SiteTemplatesController@getBuilderItems')
        ->name('{template}.builder-items');
    Route::post('{template}/builder-items', 'SiteTemplatesController@storeBuilderItems')
        ->name('{template}.builder-items.store');

    // Duplicate
    Route::post('{template}/duplicate', 'SiteTemplatesController@duplicate')
        ->name('{template}.duplicate');

    // Custom html & scss
    Route::put('{template}/custom-scss', 'SiteTemplatesController@updateCustomScss')
        ->name('{template}.custom-scss.update');
    Route::put('{template}/custom-html', 'SiteTemplatesController@updateCustomHtml')
        ->name('{template}.custom-html.update');

});

Route::resource('templates', 'Templates\SiteTemplatesController', [
    'parameters' => [
        'templates' => 'template'
    ],
]);
