<?php

Route::resource('sections', 'SiteSectionsController', [
    'parameters' => [
        'sections' => 'section'
    ]
]);

Route::group(['prefix' => 'sections/{section}', 'as' => 'sections.'], function () {

    Route::get('set-section', 'SiteSectionsController@setSection')
        ->name('{section}.set-section');
    Route::get('ajax-modal', 'SiteSectionsController@ajaxModal')
        ->name('ajax-modal');
});
