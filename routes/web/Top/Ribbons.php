<?php
Route::resource('ribbons', 'SiteRibbonsController', [
    'except' => ['show']
]);

Route::get('ribbons/{ribbons}/ajax-alert', 'SiteRibbonsController@ajaxAlert')
    ->name('ribbons.{ribbons}.ajax-alert');

Route::get('ribbons/ribbon_post', 'SiteRibbonsController@ribbonPost')
    ->name('ribbons.ribbon-post');