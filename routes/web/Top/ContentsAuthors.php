<?php

Route::resource('content-authors', 'ContentType\SiteContentAuthorsController');

Route::get('content-authors/{content_author}/ajax-alert', 'ContentType\SiteContentAuthorsController@ajaxAlert')
    ->name('content-authors.{content_author}.ajax-alert');

Route::get('content-authors/ajax-modal', 'ContentType\SiteContentAuthorsController@ajaxModal')
    ->name('content-authors.ajax-modal');
