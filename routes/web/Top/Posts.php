<?php

Route::resource('posts', 'SitePostsController', [
    'except'     => 'show',
    'parameters' => [
        'posts' => 'post'
    ]
]);

Route::group(['prefix' => 'posts', 'as' => 'posts.'], function () {

    Route::get('{id}/revisions', 'SitePostRevisionsController@show')
        ->name('{post}.revisions');
    Route::get('{id}/revisions/json', 'SitePostRevisionsController@revisionsByPeriod')
        ->name('{post}.revisions.json');
    Route::get('{id}/revisions/{key}/before/{date}', 'SitePostRevisionsController@previousRevision')
        ->name('{post}.revisions.before');

    Route::post('{id}/revisions', 'SitePostRevisionsController@update');

    Route::post('datatable', 'SitePostsController@datatable')
        ->name('datatable');

    Route::get('{post}/ajax-alert', 'SitePostsController@ajaxAlert')
        ->name('{post}.ajax-alert');

    Route::post('{post}/duplicate', 'SitePostsController@duplicate')
        ->name('{post}.duplicate');

    Route::get('dynamic-list', 'SitePostsController@findPostsToDynamicList')
        ->name('dynamic-list');

    Route::get('find', 'SitePostsController@find')
        ->name('find');

    Route::get('select2', 'SitePostsController@select2')
        ->name('select2');
});
