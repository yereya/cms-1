<?php

Route::resource('dashboard', 'SiteDashboardController', ['only' => ['index']]);

Route::get('purge', 'PurgeController@purgeSite')
    ->name('purge');

Route::get('ajax-alert', 'SitesController@ajaxAlert')
    ->name('ajax-alert');
Route::get('migrations', 'SitesController@migrations')
    ->name('migrations');
Route::get('scss-compile', 'SitesController@scssCompile')
    ->name('scss-compile');
Route::get('css-upload', 'SitesController@cssUpload')
    ->name('css-upload');
Route::get('show-temp-scss', 'SitesController@getTempScssContent')
    ->name('show-temp-scss');
