<?php

Route::resource('member-groups', 'SiteMemberGroupsController', [
    'parameters' => [
        'member_groups' => 'member_group'
    ]
]);
Route::get('member-groups/{member-group}/ajax-modal', 'SiteMemberGroupsController@ajaxModal')
    ->name('member-groups.ajax-modal');