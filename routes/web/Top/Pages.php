<?php

Route::resource('pages', 'SitePagesController', [
    'parameters' => [
        'pages' => 'post'
    ],
    'only'       => [
        'index',
        'create',
        'store',
        'update',
        'destroy',
        'edit'
    ]
]);

Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {

    Route::get('{page}/ajax-alert', 'SitePagesController@ajaxAlert')
        ->name('{page}.ajax-alert');

    Route::get('{page}/ajax-modal', 'SitePagesController@ajaxModal')
        ->name('ajax-modal');

    Route::post('order', 'SitePagesController@storePagesOrder')
        ->name('order');

    Route::post('ajax-nestable', 'SitePagesController@ajaxNestable')
        ->name('ajax-nestable');

    Route::get('choice', 'SitePagesController@choice')
        ->name('choice');

    Route::get('select2', 'SitePagesController@select2')
        ->name('select2');

    Route::get('pages-path', 'SitePagesController@pagesListWithPaths')
        ->name('pages-path');
});
