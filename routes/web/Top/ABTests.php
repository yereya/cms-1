<?php

Route::resource('ab-tests', 'ABTests\ABTestsController', [
    'except'     => [
        'show'
    ],
    'parameters' => [
        'ab-tests' => 'test'
    ]
]);

Route::group(['namespace' => 'ABTests', 'prefix' => 'ab-tests', 'as' => 'ab-tests.'], function () {
    Route::get('ajax-modal', 'ABTestsController@ajaxModal')
        ->name('ajax-modal');

    Route::resource('{test}/rules', 'ABTestRulesController', [
        'except'     => ['show'],
        'parameters' => ['rules' => 'rule']
    ]);

    Route::get('{test}/rules/ajax-modal', 'ABTestRulesController@ajaxModal')
        ->name('{test}.rules.ajax-modal');

    Route::resource('{test}/location-rules', 'ABTestLocationRulesController', [
        'except'     => ['show'],
        'parameters' => ['location-rules' => 'rule']
    ]);

    Route::get('{test}/location-rules/ajax-modal', 'ABTestLocationRulesController@ajaxModal')
        ->name('{test}.location-rules.ajax-modal');

    Route::get('{test}/location-rules/select2', 'ABTestLocationRulesController@select2')
        ->name('{test}.location-rules.select2');

});