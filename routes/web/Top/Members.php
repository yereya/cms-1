<?php

Route::resource('members', 'SiteMembersController', [
    'parameters' => [
        'members' => 'member'
    ]
]);
Route::get('members/{members}/ajax-modal', 'SiteMembersController@ajaxModal')
    ->name('members.ajax-modal');