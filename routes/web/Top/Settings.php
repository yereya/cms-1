<?php

Route::group(['prefix' => 'settings', 'as' => 'settings.'], function ($router) {

    $router->resource('scss-files', 'ScssFileController');

    $router->post('update-priority', 'ScssFileController@updatePriorities')
        ->name('sass-files.update-priorities');

    $router->get('general', 'GeneralSettingsController@render')
        ->name('general');

    $router->put('general', 'GeneralSettingsController@update')
        ->name('general');

    $router->get('ajax-modal', 'SiteSettingsController@ajaxModal')
        ->name('ajax-modal');
});
