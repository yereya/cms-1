<?php

Route::resource('site-widgets', 'Widgets\SiteWidgetsController', [
    'parameters' => [
        'site-widgets' => 'widget'
    ],
    'except'     => ['show']
]);

Route::get('site-widgets/select2', 'Widgets\SiteWidgetsController@select2')
    ->name('site-widgets.select2');

Route::get('site-widgets/{widget}/ajax-modal', 'Widgets\SiteWidgetsController@ajaxModal')
    ->name('site-widgets.ajax-modal');

Route::resource('site-widgets/{widget}/fields', 'Widgets\SiteWidgetFieldsController', [
    'parameters' => [
        'fields' => 'field'
    ]
]);
Route::get('site-widgets/{widget}/fields/{field}/ajax-modal', 'Widgets\SiteWidgetsController@ajaxModal')
    ->name('site-widgets.{widget}.fields.ajax-modal');


// widgets page
Route::resource('widgets', 'Widgets\SiteWidgetsDataController', [
    'only' => ['index']
]);
Route::group(['namespace' => 'Widgets', 'prefix' => 'widgets', 'as' => 'widgets.'], function () {

    Route::post('datatable', 'SiteWidgetsDataController@datatable')
        ->name('datatable');

    // Widget Data
    Route::resource('{widget}/widget-data', 'SiteWidgetsDataController', [
        'only'       => ['store', 'update', 'destroy', 'show', 'create', 'edit'],
        'parameters' => [
            'widgets'      => 'widget',
            'widgets_data' => 'widget_data'
        ],
        'as'         => '{widget}'
    ]);

    // store and update for portlet view (not modal view) - the Widgets screen flow
    Route::post('{widget}/widget-data/store-portlet', 'SiteWidgetsDataController@storePortlet')
        ->name('{widget}.widget-data.store-portlet');
    Route::post('{widget}/widget-data/{widget_data}/update-portlet', 'SiteWidgetsDataController@updatePortlet')
        ->name('{widget}.widget-data.update-portlet');
    Route::get('{widget}/widget-data/{widget_data}/ajax-modal', 'SiteWidgetsDataController@ajaxModal')
        ->name('{widget}.widget-data.ajax-modal');
    Route::get('widget-data/select2', 'SiteWidgetsDataController@select2')
        ->name('widget-data.select2');
    Route::get('ajax-modal', 'SiteWidgetsDataController@ajaxModal')
        ->name('ajax-modal');
    Route::get('{widget}/widget-data/create', 'SiteWidgetsDataController@create')
        ->name('{widget}.widget-data.create');
    Route::get('{widget}/widget-data/{widget_data}/edit', 'SiteWidgetsDataController@edit')
        ->name('{widget}.widget-data.{widget_data}.edit');

    // Widget Template - Blades
    Route::get('{widget}/templates/ajax-modal', 'SiteWidgetTemplatesController@ajaxModal')
        ->name('{widget}.templates.ajax-modal');
    Route::post('{widget}/templates/update-scss', 'SiteWidgetTemplatesController@updateScss')
        ->name('{widget}.templates.update-scss');
    Route::post('{widget}/templates/update-blade', 'SiteWidgetTemplatesController@updateBlade')
        ->name('{widget}.templates.update-blade');
});
