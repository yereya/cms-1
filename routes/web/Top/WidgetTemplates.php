<?php

Route::group(['namespace' => 'Widgets'], function () {

    Route::resource('widget-templates', 'SiteWidgetTemplatesController', [
        'parameters' => [
            'widget-templates' => 'widget-template'
        ],
        'except'     => ['show']
    ]);

    Route::group(['prefix' => 'widget-templates', 'as' => 'widget-templates.'], function () {
        Route::post('datatable', 'SiteWidgetTemplatesController@datatable')
            ->name('datatable');

        Route::get('revisions', 'SiteWidgetTemplatesController@revisions')
            ->name('revisions');

        Route::get('choice', 'SiteWidgetTemplatesController@choice')
            ->name('choice');

        Route::get('ajax-alert', 'SiteWidgetTemplatesController@ajaxAlert')
            ->name('ajax-alert');

        Route::get('ajax-modal', 'SiteWidgetTemplatesController@ajaxModal')
            ->name('ajax-modal');

        Route::post('publish', 'SiteWidgetTemplatesController@publishAll')
            ->name('publish');

        Route::post('{widget_template}/duplicate', 'SiteWidgetTemplatesController@duplicate')
            ->name('{widget_template}.duplicate');
    });
});
