<?php

Route::resource('labels', 'SiteLabelsController', [
    'except' => 'show',
    'parameters' => [
        'labels' => 'label'
    ]
]);

Route::group(['prefix' => 'labels', 'as' => 'labels.'], function () {

    Route::get('ajax-alert', 'SiteLabelsController@ajaxAlert')
        ->name('ajax-alert');
    Route::get('{label}/toggle-property', 'SiteLabelsController@toggleProperty')
        ->name('{label}.toggle-property');
    Route::get('ajax-modal', 'SiteLabelsController@ajaxModal')
        ->name('ajax-modal');
    Route::get('dynamic-list/select2', 'SiteLabelsController@select2')
        ->name('dynamic-list.select2');
    Route::get('select2', 'SiteLabelsController@select2')
        ->name('select2');

});
