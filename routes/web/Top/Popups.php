<?php

Route::resource('popups',
    'SitePopupsController', [
        'parameters' => [
            'popups' => 'popup'
        ],
        'except'     => ['show']
    ]);

Route::get('popups/ajax-alert', 'SitePopupsController@ajaxAlert')
    ->name('popups.ajax-alert');

Route::get('popups/choice', 'SitePopupsController@choice')
    ->name('popups.choice');

Route::post('popups/datatable', 'SitePopupsController@datatable')
    ->name('popups.datatable');

Route::post('popups/{popup}/duplicate', 'SitePopupsController@duplicate')
    ->name('popups.{popup}.duplicate');
