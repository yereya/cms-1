<?php

Route::resource('contact-us', 'ContactUsController', [
    'except'     => ['show'],
    'parameters' => [
        'contact-us' => 'contact_us_record'
    ]
]);
Route::post('contact-us/datatable', 'ContactUsController@datatable')
    ->name('contact-us.datatable');
Route::get('contact-us/ajax-alert', 'ContactUsController@ajaxAlert')
    ->name('contact-us.ajax-alert');