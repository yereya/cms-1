<?php

# Interface for accessing Youtube API
//Route::get('youtubeSearch','youtubeController@index')->name('youtubeSearch');
//Route::post('youtubeSearch','youtubeController@process')->name('youtubeSearch');

/*Base dashboard.*/
Route::get('/', 'DashboardController@index')
    ->name('home')
    ->middleware('dashboard_auth');

Route::group(['prefix' => 'dashboards', 'namespace' => 'Dashboards', 'as' => 'dashboards.'], function () {

    /*DAILY STATS DASHBOARD.*/
    Route::get('daily-stats', 'DailyStatsController@index')
        ->name('daily-stats');

    Route::post('daily-stats/api', 'DailyStatsController@stats')
        ->name('daily-stats.api');

    /*PPC DASHBOARD.*/
    Route::get('advertisers-targets', 'TargetPpcController@index')
        ->name('advertisers-targets');

    Route::post('advertisers-targets/api', 'TargetPpcController@stats')
        ->name('advertisers-targets.api');

    /*DYNAMIC DASHBOARD */
    Route::get('dynamic', 'DynamicController@index')
        ->name('dynamic');

    Route::post('dynamic/api', 'DynamicController@stats')
        ->name('dynamic.api');

    Route::post('dynamic/save-alert-state', 'DynamicController@saveAlertStates')
        ->name('dynamic.save_alert_state');

    Route::get('dynamic-accounts/select2', 'DynamicController@select2')
        ->name('dynamic-accounts.select2');

    /* Manager Target Dashboard */
    Route::get('manager-target', 'ManagerTargetController@index')
        ->name('manager-target');

    /* Manager Target Dashboard */
    Route::post('manager-target/api', 'ManagerTargetController@stats')
        ->name('manager-target.api');
    Route::get('dynamic-campaigns/select2','DynamicController@select2')
        ->name('dynamic-campaigns.select2');

    /*MORNING ROUTINE PPC*/
    Route::get('ppc-morning-routine','PpcMorningRoutineController@index')
        ->name('ppc-morning-routine');

    Route::post('ppc-morning-routine/api','PpcMorningRoutineController@stats')
        ->name('ppc-morning-routine.api');

    Route::post('ppc-morning-routine/save-alert-state','PpcMorningRoutineController@saveAlertStates')
        ->name('ppc-morning-routine.save_alert_state');

    /* Page Speed Dashboard */
    Route::get('page-speed', 'PageSpeedController@index')
        ->name('page-speed');

    /* Page Speed Dashboard */
    Route::post('page-speed/api', 'PageSpeedController@stats')
        ->name('page-speed.api');


    Route::get('page-speed/refresh-func', 'PageSpeedController@refreshFunc')
        ->name('page-speed.refresh_func');

});


