<?php
Route::group(['prefix' => 'auth', 'namespace' => 'System'], function () {

    Route::group(['middleware' =>[ 'guest']],function (){
        /*LOGIN*/
        Route::get('login','Auth\LoginController@showLoginForm')->name('login');
        Route::post('login','Auth\LoginController@login')->name('authenticate');

        /*FORGOT PASSWORD*/
        Route::get('forget','Auth\ForgotPasswordController@showLinkRequestForm')->name('forgot-password');
        Route::post('forget','Auth\ForgotPasswordController@sendResetLinkEmail')->name('forgot-password-form');

        /*RESET PASSWORD*/
        Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('reset_password');
    });

    Route::get('logout', 'Auth\LoginController@logout')->name('logout');

    Route::group(['middleware' => ['auth', 'roles']], function () {
        Route::get('unicorn/login', 'UnicornAuthController@login')
            ->name('unicorn.login');

        Route::get('services/login/{service}', 'ServicesAuthController@auth')
            ->name('services.login');
    });
});
