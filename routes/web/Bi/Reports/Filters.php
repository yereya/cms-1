<?php
Route::resource('filters', 'FiltersController', [
    'only' => ['index', 'store', 'edit', 'destroy']
]);

Route::group(['as' => 'filters.'], function () {
    Route::get('filters/ajax-modal', 'FiltersController@ajaxModal')
        ->name('ajax-modal');
});