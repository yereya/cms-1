<?php
Route::group(['namespace' => 'ScraperUI'], function () {
    //Scrapers
    Route::resource('scraper', 'ScraperController', [
        'only' => ['index', 'create', 'store', 'show', 'edit', 'update', 'destroy', 'duplicate'],
    ]);

    Route::get('scraper/mail', 'ScraperController@mail')
        ->name('scraper.mail');

    Route::group(['prefix' => 'scraper/{scraper}', 'as' => 'scraper.{scraper_id}.'], function () {

        Route::resource('fetchers', 'FetchersController');

        Route::post('processors', 'ProcessorController@processors')
            ->name('processors');

        Route::post('processors/{processor_id}', 'ProcessorController@duplicate')
            ->name('processors.{processor_id}');

        Route::delete('processors/{processor_id}', 'ProcessorController@destroy')
            ->name('processors.{processor_id}');

        Route::get('duplicate', 'ScraperController@duplicate')
            ->name('duplicate');

        Route::group(['prefix' => 'fetchers', 'as' => 'fetchers.'], function () {

            Route::post('{fetcher_id}/actions', 'FetchersController@actions')
                ->name('actions');
            Route::post('priority', 'FetchersController@priority')
                ->name('priority');
        });

    });
});