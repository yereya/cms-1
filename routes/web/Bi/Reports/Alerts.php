<?php
Route::resource('alerts', 'AlertsController', ['only' => ['index', 'update']]);

Route::group(['as' => 'alerts.'], function () {

    Route::get('{notification_id}/ajax-modal', 'AlertsController@ajaxModal')
        ->name('{notification_id}.ajax-modal');
    Route::get('{notification_id}/toggle-property', 'AlertsController@toggleProperty')
        ->name('{notification_id}.toggle-property');
    Route::get('notifications-list', 'AlertsController@notificationsList')
        ->name('notifications-list');
    Route::post('alerts/datatable', 'AlertsController@datatable')
        ->name('datatable');
});