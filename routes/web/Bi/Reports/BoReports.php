<?php
Route::get('', 'BoReportsController@index')
    ->name('index');

Route::group([], function () {
    Route::get('/{report_id}', 'BoReportsController@index')
        ->name('{report_id}.index');
    Route::post('/{report_id}/filter', 'BoReportsController@filter')
        ->name('{report_id}.filter');
    Route::get('/{report_id}/select2', 'BoReportsController@select2')
        ->name('{report_id}.select2');
});



