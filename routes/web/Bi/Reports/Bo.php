<?php
// OUT Reports (bo)
Route::group([], function () {
    // Uploads
    Route::group(['prefix' => 'advertisers', 'namespace' => 'Advertisers', 'as' => 'advertisers.'], function () {

        // Tracks manager
        Route::resource('/tracks-manager', 'TracksManagerController', ['only' => ['index', 'store']]);
        Route::post('/tracks-manager/upload', 'TracksManagerController@upload')
            ->name('tracks-manager.upload');
        Route::get('/tracks-manager/approve', 'TracksManagerController@approve')
            ->name('tracks-manager.approve');

        // Campaign manager
        Route::resource('/campaigns-manager', 'CampaignsManagerController', ['only' => ['index', 'store']]);
        Route::post('/campaigns-manager/upload', 'CampaignsManagerController@upload')
            ->name('campaigns-manager.upload');
        Route::get('/campaigns-manager/approve', 'CampaignsManagerController@approve')
            ->name('campaigns-manager.approve');

        // Targets manager
        Route::resource('/targets-manager', 'TargetsManagerController', ['only' => ['index', 'store']]);
        Route::post('/targets-manager/upload', 'TargetsManagerController@upload')
            ->name('targets-manager.upload');
        Route::get('/targets-manager/approve', 'TargetsManagerController@approve')
            ->name('targets-manager.approve');

        // Auction Insights manager
        Route::resource('/auction-insights-manager', 'AuctionInsightsController', ['only' => ['index', 'store']]);
        Route::post('/auction-insights-manager/upload', 'AuctionInsightsController@upload')
            ->name('auction-insights-manager.upload');
    });
});