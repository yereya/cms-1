<?php
Route::group([], function () {
    // Publishers Manager
    Route::group(['prefix' => 'publishers', 'namespace' => 'Publishers', 'as' => 'publishers.'], function () {
        Route::resource('publishers-manager', 'PublishersManagerController', [
                'only' => ['index', 'store']
            ]
        );
        Route::post('publishers-manager/upload', 'PublishersManagerController@upload')
            ->name('publishers-manager.upload');
        Route::get('publishers-manager/approve', 'PublishersManagerController@approve')
            ->name('publishers-manager.approve');
    });
});

Route::group([
    'prefix'    => 'publishers/{publisher}',
    'namespace' => 'Publishers',
    'as'        => 'publishers.{publisher}'
], function () {

    //Accounts
    Route::resource('accounts', 'AccountsController', [
        'only' => ['index', 'update']
    ]);
    Route::get('accounts/updateApi', 'AccountsController@updateApi')
        ->name('accounts.updateApi');
    Route::get('accounts/xEditableApi', 'AccountsController@xEditableApi')
        ->name('accounts.xEditableApi');

    //Change
    Route::resource('/accounts/{account_id}/change', 'ChangesController');
    //Route::post('accounts/{account_id}/filter', 'ReportsController@filter')->name('reports.publishers.accounts.filter');
    Route::get('accounts/{account_id}/change/{change_id}/toggle-property', 'ChangesController@toggleProperty')
        ->name('accounts.{account_id}.change.{change_id}.toggle-property');
    Route::get('accounts/change/list-all', 'ChangesController@listAll')
        ->name('accounts.change.list-all');
    Route::get('accounts/{account_id}/api', 'ChangesController@api')
        ->name('accounts.api');

    //Keywords
    Route::get('keywords/select2', 'KeywordsController@select2')
        ->name('keywords.select2');
});