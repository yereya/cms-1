<?php
Route::group([], function () {
    Route::resource('changes-log', 'ChangesLogController', ['only' => ['index', 'store']]);
    Route::get('changes-log/history', 'ChangesLogController@history')->name('changes-log.history');
    Route::post('changes-log/datatable', 'ChangesLogController@datatable')->name('changes-log.datatable');
    Route::get('changes-log/select2', 'ChangesLogController@select2')->name('changes-log.select2');
    Route::get('changes-log/ajax-modal', 'ChangesLogController@ajaxModal')->name('changes-log.ajax-modal');
});