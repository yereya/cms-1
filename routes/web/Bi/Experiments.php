<?php
Route::group(['prefix' => 'experiments', 'namespace' => 'Experiments', 'as' => 'experiments.'], function () {
    Route::resource('monitors', 'MonitorController', [
        'only' => [
            'index',
            'edit',
            'update',
            'create',
            'store',
            'destroy'
        ]
    ]);
    Route::get('monitors/select2', 'MonitorController@select2')
        ->name('monitors.select2');
    Route::get('monitors/{monitor_id}/ajax-alert', 'MonitorController@ajaxAlert')
        ->name('monitors.{monitor_id}.ajax-alert');
});