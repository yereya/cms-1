<?php
Route::group(['prefix' => 'scheduler', 'namespace' => 'Scheduler', 'as' => 'scheduler.'], function () {
    Route::resource('tasks', 'TasksController',
        [
            'only' => ['index', 'create', 'store', 'edit', 'update', 'destroy']
        ]);
    Route::group(['prefix' => 'tasks', 'as' => 'tasks.'], function () {
        Route::get('select2', 'TasksController@select2')
            ->name('select2');
        Route::post('datatable', 'TasksController@datatable')
            ->name('datatable');
        Route::post('run-cmd', 'TasksController@runCmd')
            ->name('run-cmd');

        Route::get('{task_id}/ajax-modal', 'TasksController@ajaxModal')
            ->name('{task_id}.ajax-modal');
        Route::get('{task_id}/ajax-alert', 'TasksController@ajaxAlert')
            ->name('{task_id}.ajax-alert');
        Route::post('{task_id}/run-cmd', 'TasksController@runCmdByTaskId')
            ->name('{task_id}.run-cmd');
    });
});