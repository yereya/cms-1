<?php
Route::group(['as' => 'publishers.'], function () {
    Route::resource('', 'PublishersController', [
        'only' => [
            'index',
            'create',
            'store',
            'update',
            'edit'
        ]
    ]);

    Route::group(['namespace' => 'Publishers'], function () {

        Route::resource('{publisher_id}/accounts', 'AccountsController', [
            'only' => ['index', 'create', 'store', 'update', 'edit']
        ]);

        Route::resource('{publisher_id}/pixels', 'PixelsController', [
            'only' => ['create', 'store', 'edit', 'update', 'destroy']
        ]);

        Route::resource('{publisher}/medias', 'MediasController', [
            'only' => ['create', 'store', 'edit', 'update', 'index']
        ]);

        Route::resource('{publisher}/medias/{medias}/placements', 'PlacementsController', [
            'only' => ['index', 'create', 'store', 'update', 'edit']
        ]);

        Route::post('{publisher}/medias/{medias}/placement/search-brand', 'PlacementsController@searchBrand')
            ->name('{publisher}.medias.{medias}.placements.search-brand');
        Route::post('{publisher}/medias/{medias}/placement/search-campaign', 'PlacementsController@searchCampaign')
            ->name('{publisher}.medias.{medias}.placements.search-campaign');
        Route::get('{publisher}/medias/{medias}/placements/{placements}/links', 'PlacementsController@showLinks')
            ->name('{publisher}.medias.{medias}.placements.{placements}.links');
    });
});
