<?php

    Route::resource('bayesiantasks', 'BayesianTasksController',
        ['only' => ['index', 'edit','update','store','create']]
    );

    Route::get('bayesiantasks/{bayesiantasks_id}/approve','BayesianTasksController@approve')
        ->name('bayesiantasks.{bayesiantasks_id}.approve');

    Route::get('bayesiantasks/stop','BayesianTasksController@stop')
        ->name('bayesiantasks.stop');

    Route::post('bayesiantasks/datatable', 'BayesianTasksController@datatable')
    ->name('bayesiantasks.datatable');

    Route::get('bayesiantasks/select2', 'BayesianTasksController@select2')
        ->name('bayesiantasks.select2');


