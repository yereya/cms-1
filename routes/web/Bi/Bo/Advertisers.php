<?php

Route::resource('advertisers', 'AdvertisersController', [
    'except' => ['show']
]);

Route::post('advertisers/datatable', 'AdvertisersController@datatable')
    ->name('advertisers.datatable');

Route::group([
    'namespace' => 'Advertisers'
], function () {

    Route::group([
        'prefix' => 'advertisers',
        'as' => 'advertisers.'
    ], function () {

        Route::resource('domains', 'DomainsController');

        Route::group([
            'prefix' => '{advertiser_id}',
            'as' => '{advertiser_id}.'
        ], function () {
            Route::get('actions/ajax-modal', 'BrandsController@ajaxModal')
                ->name('brands.ajax-modal');

            Route::resource('brands', 'BrandsController', [
                'only' => [
                    'index',
                    'create',
                    'store',
                    'edit',
                    'update'
                ]
            ]);

            Route::group([
                'prefix' => 'brands/{brand_id}',
                'as' => 'brands.{brand_id}.'
            ], function () {
                Route::resource('campaigns', 'CampaignsController', [
                    'only' => [
                        'index',
                        'create',
                        'store',
                        'update',
                        'edit',
                        'destroy'
                    ]
                ]);

                Route::get('campaigns/ajax-modal', 'CampaignsController@ajaxModal')
                    ->name('campaigns.ajax-modal');

                Route::resource('campaign-groups', 'CampaignGroupController', [
                    'only' => [
                        'index',
                        'create',
                        'store',
                        'update',
                        'edit',
                        'destroy'
                    ]
                ]);

                Route::post('campaigns/{campaign_id}/groups', 'CampaignGroupController@associateGroup')
                    ->name('campaigns.{campaign_id}.groups');

                Route::resource('campaigns/{campaign_id}/lp', 'CampaignLandingsController', [
                    'as' => 'campaigns.{campaign_id}',
                    'only' => [
                        'index',
                        'create',
                        'store',
                        'update',
                        'edit',
                        'destroy'
                    ]
                ]);

                Route::group(['prefix' => 'actions',
                    'as' => 'actions.'
                ], function () {

                    Route::get('actions/{action} ', 'BrandsActionController@show')
                        ->name('actions.{action_id}.show');

                });

                Route::resource('actions', 'BrandsActionController', [
                    'only' => [
                        'index',
                        'create',
                        'store',
                        'edit',
                        'update',
                        'destroy'
                    ]
                ]);
            });
        });
    });

    Route::resource('advertisers/{advertiser_id}/accounts', 'AccountsController');
});
