<?php

Route::resource('{company_id}/brands-groups', 'BrandsGroupController',
    [
        'except'     => ['show'],
        'parameters' => [
            'brands_group' => 'brand-group-id'
        ],
    ]);

Route::get('brands-groups/select2', 'BrandsGroupController@select2')
    ->name('brands-group-select2');
