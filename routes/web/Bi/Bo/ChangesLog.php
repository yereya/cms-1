<?php

    Route::post('changes_log/update','ChangesLogController@update')
        ->name('changes_log.update');

    Route::post('changes_log/store','ChangesLogController@store')
       ->name('changes_log.store');

    Route::post('changes_log/delete','ChangesLogController@delete')
        ->name('changes_log.delete');

    Route::resource('changes_log', 'ChangesLogController',
        ['only' => ['index', 'create']]
    );

    Route::get('changes_log/select2', 'ChangesLogController@select2')
        ->name('changes_log.select2');

    Route::get('changes_log/getData','ChangesLogController@getData')
        ->name('changes_log.getData');





