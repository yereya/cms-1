<?php

    Route::post('discrepancies/update','DiscrepanciesController@update')
        ->name('discrepancies.update');

Route::post('discrepancies/store','DiscrepanciesController@store')
    ->name('discrepancies.store');

    Route::resource('discrepancies', 'DiscrepanciesController',
//        ['only' => ['index', 'edit','update','store','create']]
        ['only' => ['index', 'edit','create']]
    );

    Route::get('discrepancies/{discrepancy_id}/approve','DiscrepanciesController@approve')
        ->name('discrepancies.{discrepancy_id}.approve');

    Route::post('discrepancies/datatable', 'DiscrepanciesController@datatable')
    ->name('discrepancies.datatable');

    Route::get('discrepancies/select2', 'DiscrepanciesController@select2')
        ->name('discrepancies.select2');

    Route::get('discrepancies/getData','DiscrepanciesController@getData')
        ->name('discrepancies.getData');





