<?php

    Route::post('budget_manager/update','BudgetManagerController@update')
        ->name('budget_manager.update');

    Route::post('budget_manager/store','BudgetManagerController@store')
       ->name('budget_manager.store');

    Route::post('budget_manager/delete','BudgetManagerController@delete')
        ->name('budget_manager.delete');

    Route::resource('budget_manager', 'BudgetManagerController',
        ['only' => ['index', 'create']]
    );

    Route::get('budget_manager/select2', 'BudgetManagerController@select2')
        ->name('budget_manager.select2');

    Route::get('budget_manager/getData','BudgetManagerController@getData')
        ->name('budget_manager.getData');





