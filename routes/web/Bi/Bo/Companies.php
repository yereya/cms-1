<?php
/*Companies*/
Route::resource('companies', 'CompaniesController');
Route::group(['prefix'=>'companies','as'=>'companies.'], function (){
    //Bo - Brands Group
    require(__DIR__ . '/BrandsGroups.php');
});