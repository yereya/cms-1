<?php
Route::resource('accounts', 'AccountsController', [
    'only' =>
        ['create', 'index', 'edit', 'store', 'update']
]);
Route::get('accounts/select2', 'AccountsController@select2')->name('accounts.select2');
Route::post('accounts/datatable', 'AccountsController@datatable')->name('accounts.datatable');

Route::group(['namespace' => 'Accounts', 'prefix' => 'accounts', 'as' => 'accounts.{account_id}.'], function () {


    Route::resource('report-fields', 'ReportFieldsController');
    Route::get('report-fields/{field_id}/ajax-modal', 'ReportFieldsController@ajaxModal')
        ->name('report-fields.{field_id}.ajax-modal');
    Route::resource('block-queries', 'BlockQueriesController', [
        'only' => [
            'index',
            'create',
            'store',
            'update',
            'edit'
        ]
    ]);

    Route::resource('blocked-ips', 'BlockedIpsController', ['only' => ['index']]);
    Route::get('blocked-ips/release', 'BlockedIpsController@release')
        ->name('blocked-ips.release');
    Route::post('blocked-ips/datatable', 'BlockedIpsController@datatable')
        ->name('blocked-ips.datatable');
    Route::get('blocked-ips/refresh', 'BlockedIpsController@refresh')
        ->name('blocked-ips.refresh');
    Route::resource('campaigns', 'CampaignsController', ['only' => ['index']]);
});