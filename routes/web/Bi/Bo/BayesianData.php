<?php

    Route::resource('bayesiandata', 'BayesianDataController',
        ['only' => ['index', 'edit','update','store','create']]
    );


    Route::post('bayesiandata/datatable', 'BayesianDataController@datatable')
    ->name('bayesiandata.datatable');

    Route::get('bayesiandata/select2', 'BayesianDataController@select2')
        ->name('bayesiandata.select2');


