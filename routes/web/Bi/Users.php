<?php
Route::group(['namespace' => 'Users'], function () {

    // Users
    Route::resource('users', 'UsersController',
        ['only' => ['index', 'create', 'edit', 'store', 'update']]
    );
    Route::get('users/{user_id}/account', 'UsersController@account')
        ->name('users.{user_id}.account');
    Route::get('users/{user_id}/toggle-property', 'UsersController@toggleProperty')
        ->name('users.{user_id}.toggle-property');
    Route::get('users/select2', 'UsersController@select2')
        ->name('users.select2');

    // Profile
    Route::group(['prefix' => 'users/{user_id}'], function () {
        Route::put('profile/index', 'ProfileController@index')
            ->name('users.{user_id}.profile.index');
        Route::put('profile/update', 'ProfileController@update')
            ->name('users.{user_id}.profile.update');
    });

    Route::group(['namespace' => 'AccessControl'], function () {

        Route::resource('roles', 'RolesController');
        Route::group(['prefix' => 'roles', 'as' => 'roles.'], function () {
            // Roles
            Route::get('{user_id}/user', 'RolesController@user')
                ->name('{user_id}.user');
            Route::get('{role_id}/duplicate', 'RolesController@duplicate')
                ->name('{user_id}.duplicate');

            Route::get('{role_id}/toggle-property', 'RolesController@toggleProperty')
                ->name('{role_id}.toggle-property');
            Route::get('{role_id}/ajax-alert', 'RolesController@ajaxAlert')
                ->name('{role_id}.ajax-alert');
        });

        Route::resource('permissions', 'PermissionsController');
        Route::group(['prefix' => 'permissions', 'as' => 'permissions.'], function () {
            // Permissions
            Route::get('{user_id}/user', 'PermissionsController@user')
                ->name('{user_id}.user');
            Route::get('{role_id}/role-permissions', 'PermissionsController@rolePermissionsModal')
                ->name('{role_id}.role-permissions');
            Route::get('{role_id}/toggle-property', 'PermissionsController@toggleProperty')
                ->name('{role_id}.toggle-property');
            Route::get('{permission_id}/ajax-alert', 'PermissionsController@ajaxAlert')
                ->name('{permission_id}.ajax-alert');
        });
    });
});