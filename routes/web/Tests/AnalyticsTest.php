<?php
//
//Route::get('/ga_test_route', function () {
//    // v(version - default is 1), t(type - should be 'transaction'), tid(tracking - of the form UA-XXXXX-Y)
//    // cid(ga_client_id - should be one of our ids)
//    $params = request()->all();
//
//    $selected_keys = ['track_id', 'clkid', 'price', 'timestamp', 'type', 'event', 'source_account_id'];
//
//    $conversions = DB::table('bo.conversions')->where('source_account_id', '!=', 0)
//        ->whereDate('created_at', '>=', '2017-12-12 12:00:00')
//        ->where('status', 0)->whereNull('error')->limit(200)
//        ->get()->groupBy('source_account_id');
//
//    // source_account_id array
//    $source_account_ids = $conversions->keys()->toArray();
//
//    $invalid_accounts = DB::table('bo.accounts')->where('publisher_id', '<>', 95)
//        ->whereIn('source_account_id', array_values($source_account_ids))->get()
//        ->pluck('source_account_id')->filter();
//
//    foreach ($invalid_accounts as $account) {
//        $conversions->forget($account);
//    }
//
//    /**
//     * Groups conversions by advertiser id based on the source_account_id parameter
//     */
//    $conversions_by_advertiser = [];
//    // Transforms the conversions to an array
//    $conversions_by_source = $conversions->toArray();
//
//    // Retrieves accounts, keyed by their 'advertiser_id' for our valid source_account_ids
//    $accounts_by_advertisers = DB::table('bo.accounts')
//        ->whereIn('source_account_id', $conversions->keys())
//        ->groupBy('advertiser_id')->get()->keyBy('advertiser_id');
//
//    // Iterate over the accounts collection keyed by advertiser_id
//    $accounts_by_advertisers->each(function ($account, $advertiser_id) use (&$conversions_by_advertiser, $conversions_by_source) {
//        // Appends the conversion to its corresponding advertiser, by using the connected account's advertiser_id
//        $conversions_by_advertiser += [$advertiser_id => $conversions_by_source[$account->source_account_id]];
//    });
//
//    /**
//     * Build the conversion data we wish to push to analytics
//     */
//    $analytics = new \TheIconic\Tracking\GoogleAnalytics\Analytics();
//
//    $analytics->setProtocolVersion('1')
//        // Will add response parsing
//        ->setDebug(true)
//        // Get an anonymous ip address to avoid ip filtering
//        ->setAnonymizeIp('1');
//    /**
//     * Prepares hits properties
//     */
//    $selected_analytics_data = [];
//
//    foreach ($conversions_by_advertiser as $advertiser_id => $conversions) {
//        $selected_analytics_data[$advertiser_id] = array_map(function ($conversion) use ($selected_keys) {
//            // Converts conversions to array and returns the selected keys only to append to hits data
//            return array_intersect_key((array) $conversion, array_flip($selected_keys));
//        }, $conversions);
//    }
//
//    /**
//     * Commences the upload conversion process
//     */
//    foreach ($selected_analytics_data as $advertiser_id => $selected_conversions) {
//        // Retrieve the analytics account model
//        $analytics_model = \App\Entities\Models\Bo\AdvertiserAnalytics::where('advertiser_id', $advertiser_id)->get()->first() ?? null;
//
//
//        if (!$analytics_model) {
//            continue;
//        }
//        $analytics
//            // Account site id
//            ->setTrackingId($params['tid'])
//            // View id
//            ->setClientId($params['cid'] ?? $analytics_model->ga_client_id)
//            // Transaction(conversion) type hit
//            ->setHitType($params['t'] ?? 'transaction');
//
//        echo '<div class="row" style="margin-bottom:30px;"><h1>ID: ' . ($params['cid'] ?? $analytics_model->ga_client_id) . '</h1>';
//
//        foreach ($selected_conversions as $conv_idx => $current_selected_data) {
//            // Send hit transaction for every final conversion
//            $analytics_response = $analytics
//                // The linked adwords click id
//                ->setGoogleAdwordsId($current_selected_data['clkid'])
//                // A unique identifier for the current hit
//                ->setTransactionId($current_selected_data['track_id'])
//                // Mentions the transaction price
//                ->setItemPrice($current_selected_data['price'])
//                // Our event value type (will mostly be sale/lead)
//                ->setEventValue($current_selected_data['type'])
//                // Sends the processed data to google analytics
//                ->sendTransaction();
//            $passed_response = json_decode(json_encode($analytics_response->getDebugResponse()), true);
//            echo "<pre>";
//                print_r($passed_response);
//            echo "</pre>";
//        }
//
//        echo '</div>';
//    }
//});