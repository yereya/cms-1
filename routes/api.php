<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Demo route created strictly for this specific route name
// used in the comment-form.blade.php
Route::group(['middleware' => ['captcha']], function () {
    Route::post('api/comments', 'Sites\SiteCommentsController@store')
        ->name('api.comment.store');
});

