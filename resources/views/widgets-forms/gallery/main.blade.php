<div class="form-body">
    <div class="row">
        @include('partials.fields.select', [
                'name' => 'label_id',
                'label' => 'Label',
                'required' => true,
                'list' => \App\Entities\Models\Sites\Label::select('id', 'name as text')->get(),
                'value' => null
            ])
    </div>

    @include('widgets-forms.containers.common-fields-tabs')
</div>

<script type="application/javascript">
    (function () {
        App.ajaxModalStackable();
    })();
</script>