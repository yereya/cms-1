<div class="form-body">
    @include('partials.fields.input', [
        'name' => 'title',
        'label' => 'Title',
        'required' => true,
        'value' => $widget_data->data->title ?? '',
    ])

    @include('partials.fields.select', [
        'name' => 'wrapper_heading',
        'label' => 'Wrapper Heading',
        'required' => true,
        'list' => [['id' => 'h1', 'text' => 'H1'],['id' => 'h2', 'text' => 'H2'],['id' => 'h3', 'text' => 'H3'],['id' => 'h4', 'text' => 'H4'],['id' => 'h5', 'text' => 'H5'],['id' => 'h6', 'text' => 'H6'],['id' => 'span', 'text' => 'span'],],
        'value' => $widget_data->data->wrapper_heading ?? 1
    ])

    @include('partials.fields.input', [
        'name' => 'custom_content',
        'label' => 'Custom content',
        'value' => $widget_data->data->custom_content ?? ''
    ])

    @include('partials.fields.input', [
        'name' => 'subtitle',
        'label' => 'Subtitle',
        'value' => $widget_data->data->subtitle ?? ''
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>