<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'content_type_id',
        'label' => 'Content Type',
        'id' => 'post-content-type-list',
        'required' => true,
        'list' => \App\Entities\Models\Sites\ContentTypes\SiteContentType::where('site_id', $site->id)->get()->pluck('name', 'id'),
        'value' => !empty($widget_data->content_type_id) ? $widget_data->content_type_id : null
    ])

    @include('partials.fields.select', [
        'name' => 'label_id',
        'label' => 'Label',
        'ajax' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#post-content-type-list',
            'data-api_url' => route('sites.{site}.labels.select2',[$site->id]),
            'data-method' => 'list',
            'data-value' => !empty($widget_data->data->label_id)
                ? json_encode($widget_data->data->label_id)
                : '',
        ]
    ])

    @include('partials.fields.select', [
        'name' => 'post_field_ids',
        'label' => 'Field Name',
        'ajax' => true,
        'multi_select' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#post-content-type-list',
            'data-method' => 'getFields',
            'data-api_url' => route('site-content-type-fields.select2'),
            'data-value' => !empty($widget_data->data->post_field_ids)
                ? json_encode($widget_data->data->post_field_ids)
                : '',
        ]
    ])

    @include('partials.fields.input', [
        'name' => 'title',
        'label' => 'Title',
        'value' => $widget_data->data->title ?? ''
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>