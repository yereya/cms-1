@include('partials.containers.ajax-modal', [
            'part' => 'open',
            'title' => "Widget: $widget->name",
            'event' => 'widget-data-changed',
            'ajax_submit' => true
        ])

{!! Form::open([
    'url'    => '#',
    'method' => 'GET',
    'class'  => 'form-horizontal'
]) !!}

@include($view)

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'save_add' => isset($widget_data->id) ? null : [
            'attributes' => [
                'data-id' => $widget_data->id ?? '',
                'data-column-id' => $source_column->id ?? ''
            ]
        ],
        'close' => true
    ],
    'js' => [
        'select2_to_selects' => true,
        'modal_width' => 900,
        'code_mirror' => true
    ]
])