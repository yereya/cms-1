@extends('layouts.metronic.main')
@section('page_content')
    @include('partials.containers.portlet', [
        'part'  => 'open',
        'title' => "Widget: $widget->name"
    ])

    {!! Form::open([
        'url'    => isset($widget_data->id) ?
            route('sites.{site}.widgets.{widget}.widget-data.update-portlet', [$site->id, $widget->id, $widget_data->id]) :
            route('sites.{site}.widgets.{widget}.widget-data.store-portlet', [$site->id, $widget->id]),
        'method' => 'POST',
        'class'  => 'form-horizontal'
    ]) !!}

    @include($view)

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => 'true'
        ]
    ])
    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
