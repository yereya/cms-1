<hr>
<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#common-fields-tab" aria-controls="common-fields-tab" role="tab" data-toggle="tab">Base Fields</a></li>
            <li role="presentation"><a href="#scss-tab" aria-controls="scss-tab" role="tab" data-toggle="tab">Scss</a></li>
            <li role="presentation"><a href="#layout-attr" aria-controls="layout-attr" role="tab" data-toggle="tab">Layout</a></li>
            <li role="presentation"><a href="#text-attr" aria-controls="text-attr" role="tab" data-toggle="tab">Text</a></li>
            <li role="presentation"><a href="#misc-attr" aria-controls="misc-attr" role="tab" data-toggle="tab">Misc</a></li>
            <li role="presentation"><a href="#ab-tests-attr" aria-controls="ab-tests-attr" role="tab" data-toggle="tab">A/B
                    Tests</a></li>
            @if (request()->has('item_id'))
                <li role="presentation"><a href="#instance-configs-attr" aria-controls="instance-configs-attr" role="tab" data-toggle="tab">Instance Config</a></li>
            @endif
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content hide-with-height">
            <div id="instance-configs-attr" role="tabpanel" class="tab-pane">
                <div class="col-md-12">
                    @if (request()->has('item_id') && strstr(request()->get('item_id'),'temp-'))
                        <div class="alert alert-warning">The following settings can only be applied after the template has been saved.</div>
                    @else
                        <div class="alert alert-info">
                            <b>Notice:</b> Any configurations made here are per instance of the widget data, this means
                            any change will only act appon this widget inside this template.
                        </div>
                        <div class="form-group form-md-line-input">
                            @include('partials.fields.checkbox', [
                                'label' => 'Device',
                                'wrapper' => true,
                                'list' => [
                                    [
                                        'label' => 'Desktop',
                                        'name' => "item[show_on_desktop]",
                                        'checked' => $column_item->show_on_desktop ?? 1
                                    ],
                                    [
                                        'label' => 'Tablet',
                                        'name' => "item[show_on_tablet]",
                                        'checked' => $column_item->show_on_tablet ?? 1
                                    ],
                                    [
                                        'label' => 'Mobile',
                                        'name' => "item[show_on_mobile]",
                                        'checked' => $column_item->show_on_mobile ?? 1
                                    ]
                                ]
                            ])
                        </div>
                        <div class="form-group form-md-line-input">
                            @include('partials.fields.checkbox', [
                                'label' => 'Cache Control',
                                'wrapper' => true,
                                'list' => [
                                    [
                                        'label' => 'Live',
                                        'name' => "item[is_live]",
                                        'checked' => $column_item->is_live ?? 0
                                    ]
                                ]
                            ])
                        </div>
                    @endif
                </div>
            </div>
            <div id="common-fields-tab" role="tabpanel" class="tab-pane active">
                {!! Form::hidden('widget_data_id', $widget_data->id ?? '') !!}
                {!! Form::hidden('create_new', isset($widget_data->id) ? 'false' : 'true') !!}
                {!! Form::hidden('widget_id', $widget->id) !!}
                {!! Form::hidden('active', 0) !!}

                @if (request()->has('item_id'))
                    {!! Form::hidden('item_id', request()->get('item_id')) !!}
                @endif

                @include('partials.fields.input', [
                    'name' => 'name',
                    'value' => $widget_data->name ?? null,
                    'required' => true,
                ])

                @include('partials.fields.checkbox', [
                    'label' => 'Active',
                    'wrapper' => true,
                    'list' => [
                        [
                            'name' => 'active',
                            'value' => 1,
                            'checked' => $widget_data->active ?? 1
                        ],
                    ],
                ])

                @include('partials.fields.input', [
                    'name' => 'class',
                    'value' => $widget_data->data->class ?? null,
                ])

                <div class="form-group form-md-line-input">
                    @include('partials.fields.select', [
                        'name' => 'widget_template_id',
                        'class' => 'widget_template_id',
                        'label' => 'Widget Blade',
                        'force_selection' => true,
                        'required' => true,
                        'form_line' => true,
                        'column_input' => 6,
                        'list' => \App\Entities\Models\Sites\Widgets\SiteWidgetTemplate::where('widget_id', $widget->id)
                            ->where('site_id',$site->id)
                            ->get()
                            ->pluck('name', 'id'),
                        'value' => $widget_data->widget_template_id ?? -1,
                    ])

                    <div class="col-md-3">
                        @include('partials.fields.button', [
                        'name' => 'widget_edit_scss',
                        'url' => '#stack_base_field',
                        'value' => 'Edit Scss',
                        'id' => 'stack',
                        'class' => 'btn btn-success tooltips stackable fill-column',
                        'icon' => 'fa fa-edit',
                        'attributes' => [
                            'href' => '#stack_base_field',
                            'data-route' => route('sites.{site}.widgets.{widget}.templates.ajax-modal', [$site->id, $widget->id, 'method' => 'scss']),
                            'data-toggle' => 'modal',
                            'data-param' => 'widget_template_id'
                        ]
                    ])
                    </div>
                </div>

                <div class="form-group form-md-line-input">
                    <div class="col-md-3 col-md-offset-8">
                        @include('partials.fields.button', [
                        'name' => 'widget_edit_blade',
                        'url' => '#stack_base_field',
                        'value' => 'Edit Blade',
                        'id' => 'stack',
                        'class' => 'btn btn-success tooltips stackable fill-column',
                        'icon' => 'fa fa-edit',
                        'attributes' => [
                            'href' => '#stack_base_field',
                            'data-route' => route('sites.{site}.widgets.{widget}.templates.ajax-modal', [$site->id, $widget->id, 'method' => 'blade']),
                            'data-toggle' => 'modal',
                            'data-param' => 'widget_template_id'
                        ]
                    ])
                    </div>
                </div>

            </div>

            <div id="scss-tab" role="tabpanel" class="tab-pane">
                <div class="row">
                    <div class="col-md-12">
                        @include('partials.fields.textarea', [
                            'name' => 'scss',
                            'label' => 'Custom Scss',
                            //'codemirror'=>'scss',
                            'attributes' => [
                                'rows' => 20
                            ],
                            'value' => function() use ($widget_data, $widget) {
                                if (!empty($widget_data->scss)) {

                                   return $widget_data->scss;
                                } else {
                                    if (!empty($widget_data->class)) {
                                        $scss_str = ".$widget_data->class";
                                    } else {
                                        $scss_str = '.' . App\Entities\Models\Sites\Templates\WidgetData::PLACEHOLDER_CLASS;
                                    }

                                   return "$scss_str {\n    \n}";
                                }
                            }
                        ])
                    </div>
                </div>
            </div>

            <div id="layout-attr" role="tabpanel" class="tab-pane">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="text-center">Border</h5>
                        @include('partials.fields.input', [
                            'name' => 'css_attributes[border_top]',
                            'label' => 'Top',
                            'column_label' => 4,
                            'placeholder' => '5px solid #fff',
                            'value' => $widget_data->data->css_attributes->border_top ?? ''
                        ])
                        @include('partials.fields.input', [
                            'name' => 'css_attributes[border_right]',
                            'label' => 'Right',
                            'column_label' => 4,
                            'placeholder' => '5px solid #fff',
                            'value' => $widget_data->data->css_attributes->border_right ?? ''
                        ])
                        @include('partials.fields.input', [
                            'name' => 'css_attributes[border_bottom]',
                            'label' => 'Bottom',
                            'column_label' => 4,
                            'placeholder' => '5px solid #fff',
                            'value' => $widget_data->data->css_attributes->border_bottom ?? ''
                        ])
                        @include('partials.fields.input', [
                            'name' => 'css_attributes[border_left]',
                            'label' => 'Left',
                            'column_label' => 4,
                            'placeholder' => '5px solid #fff',
                            'value' => $widget_data->data->css_attributes->border_left ?? ''
                        ])
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-center">Padding</h5>
                        @include('partials.fields.input', [
                            'name' => 'css_attributes[padding_top]',
                            'label' => 'Top',
                            'column_label' => 4,
                            'placeholder' => '5px',
                            'value' => $widget_data->data->css_attributes->padding_top ?? ''
                        ])
                        @include('partials.fields.input', [
                            'name' => 'css_attributes[padding_right]',
                            'label' => 'Right',
                            'column_label' => 4,
                            'placeholder' => '5px',
                            'value' => $widget_data->data->css_attributes->padding_right ?? ''
                        ])
                        @include('partials.fields.input', [
                            'name' => 'css_attributes[padding_bottom]',
                            'label' => 'Bottom',
                            'column_label' => 4,
                            'placeholder' => '5px',
                            'value' => $widget_data->data->css_attributes->padding_bottom ?? ''
                        ])
                        @include('partials.fields.input', [
                            'name' => 'css_attributes[padding_left]',
                            'label' => 'Left',
                            'column_label' => 4,
                            'placeholder' => '5px',
                            'value' => $widget_data->data->css_attributes->padding_left ?? ''
                        ])
                    </div>
                </div>
            </div>

            <div id="text-attr" role="tabpanel" class="tab-pane">
                <div class="row">
                    <div class="col-md-6">
                        @include('partials.fields.input', [
                              'name' => 'css_attributes[family]',
                              'label' => 'Family',
                              'column_label' => 4,
                              'placeholder' => 'ariel',
                              'value' => $widget_data->data->css_attributes->family ?? ''
                          ])
                        @include('partials.fields.input', [
                              'name' => 'css_attributes[font_size]',
                              'label' => 'Font Size',
                              'column_label' => 4,
                              'placeholder' => '1.5em',
                              'value' => $widget_data->data->css_attributes->font_size ?? ''
                          ])
                        @include('partials.fields.input', [
                              'name' => 'css_attributes[line_height]',
                              'label' => 'Line Height',
                              'column_label' => 4,
                              'placeholder' => '22px',
                              'value' => $widget_data->data->css_attributes->line_height ?? ''
                          ])
                        @include('partials.fields.input', [
                              'name' => 'css_attributes[color]',
                              'label' => 'Color',
                              'column_label' => 4,
                              'placeholder' => '#eeeeee',
                              'value' => $widget_data->data->css_attributes->color ?? ''
                        ])
                    </div>
                    <div class="col-md-6">
                        @include('partials.fields.select', [
                              'name' => 'css_attributes[font_weight]',
                              'label' => 'Font Weight',
                              'column_label' => 4,
                              'list' => [
                                    ['id' => '_unset', 'text' => '_Unset'],
                                    ['id' => 'none', 'text' => 'None'],
                                    ['id' => '100', 'text' => '100'],
                                    ['id' => '200', 'text' => '200'],
                                    ['id' => '300', 'text' => '300'],
                                    ['id' => '400', 'text' => '400'],
                                    ['id' => '500', 'text' => '500'],
                                    ['id' => '600', 'text' => '600'],
                                    ['id' => '700', 'text' => '700'],
                                    ['id' => '800', 'text' => '800'],
                                    ['id' => '900', 'text' => '900'],
                              ],
                              'value' => $widget_data->data->css_attributes->font_weight ?? null
                          ])
                        @include('partials.fields.select', [
                              'name' => 'css_attributes[text_decoration]',
                              'label' => 'Text Decoration',
                              'column_label' => 4,
                              'list' => [
                                    ['id' => '_unset', 'text' => '_Unset'],
                                    ['id' => 'none', 'text' => 'None'],
                                    ['id' => 'overline', 'text' => 'Over Line'],
                                    ['id' => 'underline', 'text' => 'Under Line'],
                                    ['id' => 'line-through', 'text' => 'Line Through'],
                              ],
                              'value' => $widget_data->data->css_attributes->text_decoration ?? null
                          ])
                        @include('partials.fields.select', [
                              'name' => 'css_attributes[text_align]',
                              'label' => 'Text Align',
                              'column_label' => 4,
                              'list' => [
                                    ['id' => '_unset', 'text' => '_Unset'],
                                    ['id' => 'none', 'text' => 'None'],
                                    ['id' => 'initial', 'text' => 'Initial (Browser Default)'],
                                    ['id' => 'left', 'text' => 'Left'],
                                    ['id' => 'right', 'text' => 'Right'],
                                    ['id' => 'center', 'text' => 'Center'],
                                    ['id' => 'justify', 'text' => 'Justify'],
                              ],
                              'value' => $widget_data->data->css_attributes->text_align ?? null
                          ])
                    </div>
                </div>
            </div>

            <div id="misc-attr" role="tabpanel" class="tab-pane">
                <div class="row">
                    <div class="col-md-6">
                        @include('partials.fields.input', [
                              'name' => 'css_attributes[opacity]',
                              'label' => 'Opacity',
                              'column_label' => 4,
                              'placeholder' => '0.7',
                              'value' => $widget_data->data->css_attributes->opacity ?? ''
                          ])
                        @include('partials.fields.input', [
                              'name' => 'css_attributes[background_color]',
                              'label' => 'Background Color',
                              'column_label' => 4,
                              'placeholder' => '#eeeeee',
                              'value' => $widget_data->data->css_attributes->background_color ?? ''
                          ])
                        @include('partials.fields.input', [
                              'name' => 'css_attributes[background]',
                              'label' => 'Background',
                              'column_label' => 4,
                              'placeholder' => '',
                              'value' => $widget_data->data->css_attributes->background ?? ''
                          ])
                        @include('partials.fields.input', [
                              'name' => 'css_attributes[border_radius]',
                              'label' => 'Round Corners',
                              'column_label' => 4,
                              'placeholder' => '5px',
                              'value' => $widget_data->data->css_attributes->border_radius ?? ''
                          ])
                    </div>
                </div>
            </div>

            <div id="ab-tests-attr" role="tabpanel" class="tab-pane">
                @include('partials.fields.select', [
                    'name' => 'ab_tests',
                    'label' => 'A/B Tests',
                    'multi_select' => true,
                    'list' => $ab_tests->pluck('name', 'id'),
                    'value' => $widget_data->abTests ?? -1,
                    'column_label' => 2,
                    'attributes' => [
                        'data-toggle' => 'collapse',
                        'data-target' => '#ab-test-actions'
                    ]
                ])

                <div class="row" id="ab-test-actions">
                    <span class="col-md-2 control-label">
                        *Action
                    </span>

                    <div class="col-md-10">
                        <div class="row">
                            @include('partials.fields.radio', [
                                'column' => 4,
                                'radio_id' => 'ab-test-action-show',
                                'label' => 'Show',
                                'value' => 'show',
                                'name' => 'ab_tests_action',
                                'checked' => isset($widget_data) && $widget_data->ab_tests_action == 'show'
                            ])

                            @include('partials.fields.radio', [
                                'column' => 4,
                                'radio_id' => 'ab-test-action-hide',
                                'label' => 'Hide',
                                'value' => 'hide',
                                'name' => 'ab_tests_action',
                                'checked' => isset($widget_data) && $widget_data->ab_tests_action == 'hide'
                            ])

                            @include('partials.fields.radio', [
                                'column' => 4,
                                'radio_id' => 'ab-test-action-class',
                                'label' => 'Class',
                                'value' => 'class',
                                'name' => 'ab_tests_action',
                                'checked' => isset($widget_data) && $widget_data->ab_tests_action == 'class'
                            ])
                        </div>
                    </div>

                </div>
            </div>
        </div>
    {{-- TODO in the future we should add animation options --}}
    </div>

    <script type="application/javascript">
            var intervalApp = setInterval(function() {
                if ($('script[src*="app.js"]').length > 0) {
                    clearInterval(intervalApp);
                    App.ajaxModalStackable();
                }
            }, 1000);
    </script>
</div>