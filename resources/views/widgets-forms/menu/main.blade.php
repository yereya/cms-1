<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'menu_id',
        'label' => 'Menu',
        'required' => true,
        'list' => \App\Entities\Models\Sites\Menu::pluck('name', 'id'),
        'value' => $widget_data->menu_id ?? null
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>
