<div class="row"
     data-app_select="true"
     data-select_to_nestable="add_filter"
     data-app_nestable="nestable_filters"
>

    <div class="container portlet" id="add_filter"
         data-button=".mi_btn_add_item_filter"
         data-choice_list=".mi_choice_list_filter"
         data-nestable_list="#nestable_filters"
         data-save_input="fields_filter_ids">

        @include('partials.fields.h4-separator', ['label' => 'Filter'])

        <div class="col-md-5 col-sm-6 col-xs-12">
            <div>
                @include('partials.fields.select', [
                    'name' => 'field',
                    'label' => 'Field',
                    'column' => 12,
                    'list' => $fields,
                    'class' => 'mi_choice_list_filter'
                ])

                @include('partials.fields.button', [
                    'value' => 'Add to Filter',
                    'column' => 12,
                    'type' => 'button',
                    'class' => 'btn btn-success mi_btn_add_item_filter'
                ])

            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-12">
            @include('partials.containers.nestable.nestable', [
            'id' => 'nestable_filters',
            'tree' => $filters ?? null,
            'title' => 'display_name',
            'subtitle' => null,
            'class' => 'col-md-7',
            'attributes' => [
                'data-max_levels' => 3
            ],
            'actions' => [
                function ($item) {
                    $res = '';

                    $res .= '<a href="javascript:;" title="Remove from list"
                          class="tooltips remove_item"><span aria-hidden="true" class="fa fa-trash" ></span></a>';

                    return $res;
            }]])
        </div>
    </div>

</div>

<div class="row"
     data-app_select="true"
     data-select_to_nestable="add_order"
     data-app_nestable="nestable_orders"
>
    <div class="container" id="add_order"
         data-button=".mi_btn_add_item_order"
         data-choice_list=".mi_choice_list_order"
         data-nestable_list="#nestable_orders"
         data-save_input="fields_order_ids">

        @include('partials.fields.h4-separator', ['label' => 'Sort'])

        <div class="col-md-5 col-sm-6 col-xs-12">
            <div>
                @include('partials.fields.select', [
                    'name' => 'field',
                    'label' => 'Field',
                    'column' => 12,
                    'list' => $fields,
                    'class' => 'mi_choice_list_order'
                ])

                @include('partials.fields.button', [
                    'value' => 'Add to Order',
                    'column' => 12,
                    'type' => 'button',
                    'class' => 'btn btn-success mi_btn_add_item_order'
                ])

            </div>
        </div>
        <div class="col-md-7 col-sm-6 col-xs-12">
            @include('partials.containers.nestable.nestable', [
            'id' => 'nestable_orders',
            'tree' => $orders ?? null,
            'title' => 'display_name',
            'subtitle' => null,
            'class' => 'col-md-7',
            'attributes' => [
                'data-max_levels' => 3
            ],
            'actions' => [
                function ($item) {
                    $res = '';

                    $res .= '<a href="javascript:;" title="Remove from list"
                          class="tooltips remove_item"><span aria-hidden="true" class="fa fa-trash" ></span></a>';

                    return $res;
            }]])
        </div>
    </div>

</div>


