@foreach($ribbons as $ribbon)
    @include('partials.fields.select', [
        'label' => $ribbon->name,
        'name' => 'ribbon[_' . $ribbon->id . ']',
        'list' => $posts,
        'value' => $post_ribbons[$ribbon->id] ?? null
    ])
@endforeach

<script>
    (function () {
        App.initSelect2ToSelects();
    })()
</script>