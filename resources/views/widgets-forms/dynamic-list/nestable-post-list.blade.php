<div class="row" data-app_nestable="">
   @include('partials.containers.nestable.nestable', [
        'no_nest' => true,
        'tree' => $included,
        'connect_tree' => $not_included,
        'is_connect_label' => 'Disabled',
        'nested_second_id' => 'nest-disabled',
        'nested_label' => 'Base',
        'id' => 'nest-enabled',
        'is_connect' => true,
        'attributes' => [
            'data-connect_with' => '.sortable',
            'data-protect_root' => 0,
            'data-max_levels' => 1,
            'data-is_tree' => false,
            'data-save_tree' => 'post_ids',
            'data-nested_change' => 'nest-enabled'
        ]
   ])
</div>

{!! Form::hidden('post_ids_nestable', '') !!}
{!! Form::hidden('post_ids', '') !!}

