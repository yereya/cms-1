<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#main-tab" aria-controls="main-tab" role="tab" data-toggle="tab">Main</a></li>
            <li role="presentation"><a href="#carousel-tab" aria-controls="carousel-tab" role="tab" data-toggle="tab">Carousel</a></li>

            <li role="presentation">
                <a href="#client-filters-tab" aria-controls="client-filters-tab" role="tab"
                   data-toggle="tabajax"
                   data-id="client-filters-tab"
                   data-route="{{ route('sites.{site}.fields.find', [
                        $site->id,
                        'widget_data_id' => $widget_data->id ?? 0,
                        'content_type_id' => $widget_data->dynamicList->content_type_id ?? null
                        ]) }}"
                   data-tab_params="filters-tab"
                   data-view="client-filters-list"
                   data-once="true">Client Filters
                </a>
            </li>
            <li role="presentation">
                <a href="#ribbon-tab" aria-controls="order-tab" role="tab"
                   data-id="ribbon-tab"
                   data-toggle="tabajax"
                   data-route="{{ route('sites.{site}.ribbons.ribbon-post', [
                        $site->id,
                        'widget_data_id' => $widget_data->id ?? 0,
                        'content_type_id' => $widget_data->dynamicList->content_type_id ?? null
                        ]) }}"
                   data-tab_params="filters-tab"
                   data-view="ribbons-list"
                   data-once="true">Ribbons
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content">

            <div id="main-tab" role="tabpanel" class="tab-pane active">
                @include('widgets-forms.dynamic-list.tabs.main-tab')
            </div>

            <div id="carousel-tab" role="tabpanel" class="tab-pane">
                @include('widgets-forms.dynamic-list.tabs.carousel-tab')
            </div>

            <div id="client-filters-tab" role="tabpanel" class="tab-pane">
                @include('widgets-forms.dynamic-list.tabs.client-filters-tab')
            </div>

            <div id="ribbon-tab" role="tabpanel" class="tab-pane">
                @include('widgets-forms.dynamic-list.tabs.ribbon-tab')
            </div>

        </div>
    </div>
</div>

@include('widgets-forms.containers.common-fields-tabs')

<script type="application/javascript">
    (function(){
        var topAppConnect = $('script[scr*="topApp.js"]').length;
        if (topAppConnect == 0)  {
            var intervalTopAll = setInterval(function() {
                topAppConnect = $('script[src*="topApp.js"]').length;
                if (topAppConnect > 0) {
                    clearInterval(intervalTopAll);
                    topApp.tabAjax();
                }
            }, 1000);
        } else {
            topApp.tabAjax();
        }

        var appConnect = $('script[src*="app.js"]').length;
        if (appConnect == 0)  {
            var intervalApp = setInterval(function() {
                appConnect = $('script[src*="app.js"]').length;
                if (appConnect > 0) {
                    clearInterval(intervalApp);
                    App.initSelect2ToSelects();
                    App.initDuplicateGroup();
                }
            }, 1000);
        } else {
            App.initSelect2ToSelects();
            App.initDuplicateGroup();
        }
    })();
</script>