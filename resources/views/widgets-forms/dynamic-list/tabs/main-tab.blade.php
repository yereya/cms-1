<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'dynamic_list_id',
        'label' => 'Dynamic List',
        'id' => 'dynamic_list_id',
        'required' => true,
        'list' => \App\Entities\Models\Sites\DynamicLists\DynamicList::all()->pluck('name', 'id'),
        'value' => $widget_data->dynamic_list_id ?? null
    ])

    {!! Form::hidden('content_type_id', $widget_data->dynamicList->content_type_id ?? null) !!}
    {!! Form::hidden('order_manual', $widget_data->data->order_manual ?? 'auto') !!}
    {!! Form::hidden('fields_filter_ids', $widget_data->data->fields_filter_ids ?? null) !!}
    {!! Form::hidden('fields_order_ids', $widget_data->data->fields_order_ids ?? null) !!}
</div>