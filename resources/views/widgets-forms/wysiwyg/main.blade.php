<div class="form-body">
    @include('partials.fields.textarea', [
        'name' => 'content',
        'label' => 'Content',
        'required' => true,
        'value' => $widget_data->data->content ?? '',
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>