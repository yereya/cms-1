<div class="form-body">
    @include('partials.fields.input', [
        'name' => 'deadline_date',
        'label' => 'Deadline date',
        'required' => false,
        'value' => $widget_data->data->deadline_date ?? ''
    ])

    @include('partials.fields.input', [
        'name' => 'banner_link',
        'label' => 'Banner link',
        'required' => false,
        'value' => $widget_data->data->banner_link ?? ''
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>