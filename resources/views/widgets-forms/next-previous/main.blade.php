<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'dynamic_list_id',
        'label' => 'Dynamic List',
        'required' => true,
        'ajax' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-method' => 'dynamicList',
            'data-api_url' => route('sites.{site}.widgets.widget-data.select2',[$site->id]),
            'data-value' => $widget_data->data->dynamic_list_id ?? ''
        ]
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>