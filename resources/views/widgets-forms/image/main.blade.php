<div class="form-body">
    <div class="row">
        @include('partials.containers.image-upload',[
                        'name' => 'url',
                        'label' => 'Image Url',
                        'src' => isset($widget_data->data->image_id) ? fullImagePathById($widget_data->data->image_id) : null,
                        'form_line' => true,
                        'required' => true,
                        'media_id' => $widget_data->data->image_id ?? '',
                        'attributes' => [
                            'data-image_id' => $widget_data->data->image_id ?? ''
                        ],
                        'stackable' => [
                                'route' => route('sites.{site}.content-authors.ajax-modal',
                                    [$site->id, 'method' => 'fileModal', 'field' => 'url' ]
                                )
                        ]
                    ])
    </div>

    <div class="row">
        @include('partials.fields.h4-separator', ['label' => 'Link to a page'])
        @include('partials.fields.input', [
            'label' => 'Custom link',
            'name' => 'custom_link',
            'value' => $widget_data->data->custom_link ?? ''
        ])

        @include('partials.fields.select', [
              'name' => 'link_target',
              'label' => 'Target',
              'list' => ['_self' => 'SELF', '_blank' => 'BLANK', '_top' => 'TOP', '_parent' => 'PARENT'],
              'value' => $widget_data->data->link_target ?? ''
            ])

        @include('partials.fields.input', [
            'label' => 'Link Title',
            'name' => 'link_title',
            'value' => $widget_data->data->link_title ?? ''
        ])

        @include('partials.fields.input', [
            'label' => 'Link Custom Attributes',
            'name' => 'link_custom_attributes',
            'value' => $widget_data->data->link_custom_attributes ?? ''
        ])

        @include('partials.fields.select', [
                'name' => 'page_id',
                'label' => 'Page',
                'id' => 'page_id',
                'ajax' => true,
                'attributes' => [
                    'data-min_input' => 0,
                    'data-method' => 'pagesList',
                    'data-api_url' => route('sites.{site}.pages.select2', $site->id)
                ]
            ])

        @include('partials.fields.select', [
            'name' => 'post_id', // saved in widget_data attributes not pivot
            'label' => 'Post if archive',
            'ajax' => true,
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#page_id',
                'data-method' => 'postByPage',
                'data-api_url' => route('sites.{site}.posts.select2', $site->id)
            ]
        ])

            @include('partials.fields.checkbox',[
                'label' => 'Lightbox',
                'name' => 'lightbox',
                'value' => $widget_data->data->lightbox ?? ''
            ])

        @if(!empty($widget_data->data))
            @include('partials.fields.input',[
                'label'=>'light-box-url',
                'name'=>'light-box-url',
                'value'=>$widget_data->data->light_box_url ?? ''
            ])
        @endif

    </div>

    @include('widgets-forms.containers.common-fields-tabs')
</div>

<script type="application/javascript">
    (function () {
        App.ajaxModalStackable();
    })();
</script>