<div class="row"
     data-app_select="true"
     data-select_to_nestable="add_tab"
     data-app_nestable="nestable_tab"
>
    <div class="container" id="add_tab"
         data-button=".mi_btn_add_item_tab"
         data-choice_list=".mi_choice_list_tab"
         data-nestable_list="#nestable_tab"
         data-save_input="fields_filter_ids"
         data-with_container="with_container"
    >

        <div class="col-md-5 col-sm-6 col-xs-12">
            <div>
                @include('partials.fields.select', [
                    'name' => 'widget_type',
                    'label' => 'Type',
                    'id' => 'widget_type',
                    'column' => 12,
                    'list' => \App\Entities\Models\Sites\Widgets\SiteWidget::get()->pluck('name', 'id')
                ])

                @include('partials.fields.select', [
                    'name' => 'widget_id',
                    'label' => 'Name',
                    'column' => 12,
                    'ajax' => true,
                    'class' => 'mi_choice_list_tab',
                    'attributes' => [
                        'data-min_input' => 0,
                        'data-dependency_selector[0]' => '#widget_type',
                        'data-method' => 'list',
                        'data-api_url' => route('sites.{site}.widgets.widget-data.select2', $site->id)
                    ]
                ])

                @include('partials.fields.button', [
                    'value' => 'Add Tab',
                    'column' => 12,
                    'class' => 'btn btn-success mi_btn_add_item_tab'
                ])

                {!! Form::input('hidden', 'fields_filter_ids') !!}
            </div>
        </div>

        <div class="col-md-7 col-sm-6 col-xs-12">
            @include('partials.containers.nestable.nestable', [
            'id' => 'nestable_tab',
            'tree' => $widget_data->groups ?? null,
            'title' => 'name',
            'required' => true,
            'attributes' => [
                'data-max_levels' => 1
            ],
            'container' => function($item) {
                return view('widgets-forms.group.item-container')->with('tab', $item);
            },
            'actions' => [
                function ($item) {
                    $res = '';

                    $res .= '<a href="javascript:;" title="Show More" class="show_more"><i class="fa fa-plus"></i></a>';

                    return $res;
            }]])
        </div>

        @include('widgets-forms.group.item-container')
    </div>

</div>

@include('widgets-forms.containers.common-fields-tabs')

<script type="application/javascript">
    (function(){
        var appConnect = $('script[src*="app.js"]').length;

        if (appConnect == 0)  {
            var intervalApp = setInterval(function() {
                appConnect = $('script[src*="app.js"]').length;
                if (appConnect > 0) {
                    clearInterval(intervalApp);
                    App.initSelect2ToSelects();
                }
            }, 1000);
        }

        var topAppConnect = $('script[scr*="topApp.js"]').length;

        if (topAppConnect == 0)  {
            var intervalTopAll = setInterval(function() {
                topAppConnect = $('script[src*="topApp.js"]').length;
                if (topAppConnect > 0) {
                    clearInterval(intervalTopAll);
                    topApp.createNestable('add_tab');
                }
            }, 1000);
        }

    })();
</script>