<div class="mi-link-content nestable-content-box with_container template hidden panel-body" style="display: block;">
    @include('partials.fields.input', [
        'name' => isset($tab) ? "tab[_$tab->id][tab_name]" : 'tab_name',
        'label' => 'Name',
        'class' => 'item_tab_name',
        'value' => isset($tab) ? ($tab->pivot->tab_name ?? $tab->name) : ''
    ])

    @include('partials.fields.checkbox', [
            'label' => 'Default',
            'wrapper' => true,
            'list' => [
                [
                    'name' => 'default',
                    'checked' => $tab->pivot->by_default ?? false,
                    'class' => 'item_tab_default'
                ],
            ],
        ])

    @include('partials.fields.button', [
        'value' => 'Delete Tab',
        'right' => true,
        'class' => 'btn btn-danger remove_item',
        'icon' => 'fa fa-trash'
    ])
</div>