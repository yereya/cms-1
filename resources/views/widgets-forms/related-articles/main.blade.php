<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'content_type_id',
        'label' => 'Type',
        'id' => 'ra-content-type-list',
        'required' => true,
        'list' => \App\Entities\Models\Sites\ContentTypes\SiteContentType::where('site_id', $site->id)->get()->pluck('name', 'id'),
        'value' => $widget_data->content_type_id ?? null
    ])

    @include('partials.fields.select', [
        'name' => 'related_field_id',
        'label' => 'Related',
        'ajax' => true,
        'required' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#ra-content-type-list',
            'data-method' => 'getFields',
            'data-api_url' => route('site-content-type-fields.select2'),
            'data-value' => $widget_data->data->image_field_id ?? -1,
        ]
    ])

    @include('partials.fields.select', [
        'name' => 'image_field_id',
        'label' => 'Image',
        'ajax' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#ra-content-type-list',
            'data-method' => 'getFields',
            'data-api_url' => route('site-content-type-fields.select2'),
            'data-value' => $widget_data->data->image_field_id ?? -1,
        ]
    ])

    @include('partials.fields.select', [
        'name' => 'excerpt_field_id',
        'label' => 'Excerpt',
        'ajax' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#ra-content-type-list',
            'data-method' => 'getFields',
            'data-api_url' => route('site-content-type-fields.select2'),
            'data-value' => $widget_data->data->image_field_id ?? -1,
        ]
    ])

    @include('partials.fields.select', [
        'name' => 'limit',
        'label' => 'Limit',
        'value' => $widget_data->data->limit ?? 3,
        'list' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10]
    ])

    @include('widgets-forms.containers.common-fields-tabs', [
        'css_attributes' => isset($widget_params) ? $widget_params->css_attributes : null
    ])
</div>
<script>
    App.initSelect2ToSelects();
</script>