<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'content_type_id',
        'label' => 'Content Type',
        'id' => 'post-content-type-list',
        'required' => true,
        'list' => \App\Entities\Models\Sites\ContentTypes\SiteContentType::where('site_id', $site->id)->get()->pluck('name', 'id'),
        'value' => $widget_data->content_type_id ?? null
    ])

    @if($widget_data && !$widget_data->posts->isEmpty())
        @foreach($widget_data->posts as $post)
            @include('partials.fields.select', [
                'name' => 'post_ids',
                'label' => 'Post',
                'ajax' => true,
                'attributes' => [
                    'data-min_input' => 0,
                    'data-dependency_selector[0]' => '#post-content-type-list',
                    'data-method' => 'list',
                    'data-api_url' => route('sites.{site}.posts.select2',[$site->id]),
                    'data-value' => $post->post_id ?? '',
                ]
            ])
        @endforeach
    @else
        @include('partials.fields.select', [
        'name' => 'post_ids',
        'label' => 'Post',
        'ajax' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#post-content-type-list',
            'data-method' => 'list',
            'data-api_url' => route('sites.{site}.posts.select2',[$site->id]),
            'data-value' => '',
        ]
    ])
    @endif

    @include('partials.fields.select', [
         'name' => 'labels[ids]',
         'label' => 'Labels',
         'id' => 'label_group',
         'ajax'=>true,
         'multi_select'=> true,
          'attributes' => [
             'data-min_input' => 0,
             'data-dependency_selector[0]' => '#post-content-type-list',
             'data-method' => 'list',
             'data-api_url' => route('sites.{site}.labels.dynamic-list.select2', $site->id),
             'data-value' => !empty($widget_data->data->labels) ?  json_encode($widget_data->data->labels) : '',
         ]
     ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>