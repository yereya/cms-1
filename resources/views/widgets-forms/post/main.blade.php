<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'content_type_id',
        'label' => 'Content Type',
        'id' => 'post-content-type-list',
        'required' => true,
        'list' => \App\Entities\Models\Sites\ContentTypes\SiteContentType::where('site_id', $site->id)->get()->pluck('name', 'id'),
        'value' => !empty($widget_data->content_type_id) ? $widget_data->content_type_id : null
    ])

    @include('partials.fields.select', [
        'name' => 'post_ids',
        'label' => 'Post',
        'ajax' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#post-content-type-list',
            'data-method' => 'list',
            'data-api_url' => route('sites.{site}.posts.select2',[$site->id]),
            'data-value' => !empty($widget_data->posts)
                ? $widget_data->posts->first()->post_id ?? ''
                : '',
        ]
    ])

    @include('partials.fields.select', [
        'name' => 'post_field_ids',
        'label' => 'Field Name',
        'ajax' => true,
        'multi_select' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#post-content-type-list',
            'data-method' => 'getFields',
            'data-api_url' => route('site-content-type-fields.select2'),
            'data-value' => !empty($widget_data->data->post_field_ids)
                ? json_encode($widget_data->data->post_field_ids)
                : '',
        ]
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>