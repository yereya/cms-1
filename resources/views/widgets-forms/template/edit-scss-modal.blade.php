@include('partials.containers.ajax-modal', [
            'part' => 'open',
            'title' => 'Edit Widget Template Scss',
            'event' => 'widget-data-changed',
            'ajax_submit' => true
        ])



{!! Form::open([
        'route' => ['sites.{site}.widgets.{widget}.templates.update-scss',
            $site->id, $widget_data->widget_id, 'widget_template_id' => $site_widget_template->id],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',
        'data-url_validation'=> route('sites.{site}.widgets.{widget}.templates.update-scss',[$site->id,
        $site_widget_template->widget->id])
]) !!}

{!! Form::hidden('close_modal', 0) !!}
{!! Form::hidden('widget_template_id',$site_widget_template->id) !!}
@include('partials.fields.textarea', [
            'name' => 'scss',
            'required' => 'true',
            'no_label' => false,
            'attributes'=>[
                'data-validate' => false
            ],
            //'codemirror' => 'scss',
            'value' => function() use ($widget_data, $site_widget_template) {
                if (isset($site_widget_template->scss)
                    && strlen($site_widget_template->scss) > 30) {

                    return $site_widget_template->scss;
                } else {
                    $scss_str = '';
                    $scss_str .= ".{$widget_data->widget->class}";
                    $scss_str .= ".{$site_widget_template->class}";
                    $scss_str .= "{\n" . '    ' . "\n}";

                    return $scss_str;
                }
            }
        ])

{{--todo return to this place when activate the htmlValidation --}}
{{--@include('partials.containers.validation.scss-diff')--}}

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'close' => true
    ],
    'js' => [
        'modal_width' => 900,
        'code_mirror' => true
    ]
])