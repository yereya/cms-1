<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'content_type_id',
        'label' => 'Type',
        'id' => 'post-content-type-list',
        'required' => true,
        'list' => \App\Entities\Models\Sites\ContentTypes\SiteContentType::where('site_id', $site->id)->get()->pluck('name', 'id'),
        'value' => $widget_data->content_type_id ?? null
    ])

    @include('partials.fields.select', [
        'name' => 'post_ids',
        'label' => 'Choose Post',
        'multi_select' => true,
        'required' => true,
        'ajax' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[0]' => '#post-content-type-list',
            'data-method' => 'list',
            'data-api_url' => route('sites.{site}.posts.select2',[$site->id]),
            'data-value' => (isset($widget_data) && !$widget_data->posts->isEmpty())
                ? $widget_data->posts->first()->post_id ?? ''
                : '',
        ]
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>
