<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'feed_id',
        'label' => 'Feed ID',
        'force_selection' => true,
        'required' => true,
        'list' => [
            '1000093190' => 'Football - All',
            '1000461733' => 'England',
            '1000094985' => 'Premier League',
            '2000051195' => 'Europa League',
            '1000094981' => 'Championship',
            '1000094984' => 'FA Cup',
            '1000461728' => 'Germany',
            '1000094994' => 'Bundesliga',
        ],
        'value' => $widget_data->data->feed_id ?? null
    ])

    @include('partials.fields.input', [
        'name' => 'scroll_delay',
        'type' => 'number',
        'label' => 'Scroll Delay',
        'required' => false,
        'value' => $widget_data->data->scroll_delay ?? 3
    ])

    @include('partials.fields.input', [
        'name' => 'slides_to_show',
        'type' => 'number',
        'label' => 'Slides to Show',
        'required' => false,
        'value' => $widget_data->data->slides_to_show ?? 7
    ])

    @include('widgets-forms.containers.common-fields-tabs')
</div>