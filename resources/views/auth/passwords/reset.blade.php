@extends('layouts.base')

@section('content')
    <div class="logo">
        <a href="{{ route('home') }}">
            <img src="{{ asset('assets/pages/img/logo-big.png') }}" alt="{{ Layout::getPageTitle() }}"/>
        </a>
    </div>
    <div class="content">
        <form class="reset-password-form" role="form" method="POST" action="{{ route('reset_password') }}">

            <h3 class="form-title">Reset Password</h3>

            @if(Session::has('status'))
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <div> {{ Session::get('status') }} </div>
                </div>
            @endif

            @if($errors->count())
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    @foreach($errors->all() as $error)
                        <div> {{ $error }} </div>
                    @endforeach
                </div>
            @endif

            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group">
                <label for="email" class="control-label visible-ie8 visible-ie9">E-Mail Address</label>
                <div class="input-icon">
                    <i class="fa fa-envelope"></i>
                    <input id="email" type="email" class="form-control placeholder-no-fix" name="email"
                           value="{{ $email or old('email') }}" required autofocus placeholder="E-Mail Address">
                </div>
            </div>

            <div class="form-group">
                <label for="password" class="control-label visible-ie8 visible-ie9">Password</label>
                <div class="input-icon">
                    <i class="fa fa-key green"></i>
                    <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="control-label visible-ie8 visible-ie9">Confirm Password</label>
                <div class="input-icon">
                    <i class="fa fa-key"></i>
                    <input id="password-confirm" type="password" class="form-control"
                           name="password_confirmation" required placeholder="Confirm Password">
                </div>
            </div>

            <div class="form-actions">
                <button type="submit" name="reset-password" class="btn green pull-right"> Reset Password</button>
            </div>
        </form>
    </div>

    <div class="copyright">
        <span id="this-year"></span> &copy; Traffic Point LTD.
    </div>

    <script>
      var d = new Date();
      document.getElementById('this-year').innerHTML = d.getFullYear();
    </script>
@stop