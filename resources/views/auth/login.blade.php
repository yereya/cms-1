@extends('layouts.base')

@section('content')
    <div class="logo">
        <a href="{{ route('home') }}">
            <img src="{{ asset('assets/pages/img/logo-big.png') }}" alt="{{ Layout::getPageTitle() }}"/>
        </a>
    </div>
    <div class="content">
        <form class="login-form" action="{{ route('authenticate') }}" method="post">
            {!! csrf_field() !!}

            <h3 class="form-title">Login to your account</h3>

            @if(Session::has('success'))
                <div class="alert alert-success">
                    <button class="close" data-close="alert"></button>
                    <div> {{ Session::get('success') }} </div>
                </div>
            @endif

            @if($errors->count())
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    @foreach($errors->all() as $error)
                        <div> {{ $error }} </div>
                    @endforeach
                </div>
            @endif

            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Username</label>

                <div class="input-icon">
                    <i class="fa fa-user"></i>
                    <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username"
                           name="username" value="{{ old('username') }}"/>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>

                <div class="input-icon">
                    <i class="fa fa-lock"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off"
                           placeholder="Password" name="password"/>
                </div>
            </div>
            <div class="form-actions">
                <label class="checkbox">
                <input type="checkbox" name="remember" value="1"/>
                    Remember me
                </label>
                <button type="submit" name="login" class="btn green pull-right">Login</button>
            </div>
            <div class="forget-password">
                <h4>Forgot your password ?</h4>

                <p> no worries, click
                    <a href="{{route('forgot-password')}}" > here </a> to reset your password. </p>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>

    <div class="copyright">
        <span id="this-year"></span> &copy; Traffic Point LTD.
    </div>

    <script>
        var d = new Date();
        document.getElementById('this-year').innerHTML = d.getFullYear();
    </script>
@stop