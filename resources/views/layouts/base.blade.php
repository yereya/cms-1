<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html><!--<![endif]-->

<head>
    <meta charset="utf-8"/>
    <title>{{ Layout::getPageTitle() }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

{!! Assets::css() !!}

@yield('head')

<!-- Loading jquery in the header so it will recognized inside blade templates  -->
    <script type="application/javascript" src="/assets/global/plugins/jquery.min.js"></script>
</head>

<body {!! Layout::getBodyClasses() !!}>
<script type="application/javascript">

    // Used when you add js with jQuery inline
    // using this method the script execution will wait (defer) until the jquery
    // library is loaded and usable
    function defer(method, limitingLibrary) {
        limitingLibrary = limitingLibrary == undefined ? 'jQuery' : limitingLibrary;

        if (window[limitingLibrary] !== undefined)
            method();
        else
            setTimeout(function () {
                defer(method)
            }, 100);
    }
</script>
@yield('content')

{!! Assets::js() !!}

 {{-- TODO WARNING dont use this section 'scrips', this section contents app.js, global.js, layout.js --}}
@yield('scripts')
 {{--This section to your js code--}}
@yield('javascript')
<script data-main="/assets/js/main" src="/top-assets/libs/require.js"></script>
</body>
</html>