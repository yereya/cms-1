@extends('layouts.base')

@section('head')
<link href="/assets/main.css?salt=<?php echo getFileUpdateTime(
        'main.css');?>" type="text/css" rel="stylesheet">
@stop

@section('content')


    @include('layouts.metronic.header')
<!-- BEGIN CONTAINER -->
<div class="page-container">
    @include('layouts.metronic.sidebar')
    @include('layouts.metronic.content')
{{--    @include('layouts.metronic.quick-sidebar')--}}
</div>
<!-- END CONTAINER -->
@include('layouts.metronic.footer')
@stop

@section('scripts')
    @if(request()->route()->getName()!=='login')
        <script src="/assets/global/scripts/app.js?salt=<?php echo getFileUpdateTime(
                'global/scripts/app.js');?>"></script>
        <script src="/assets/global/scripts/global.js?salt=<?php echo getFileUpdateTime(
                'global/scripts/global.js');?>"></script>
        <script src="/assets/layout/scripts/layout.js?salt=<?php echo getFileUpdateTime(
                'layout/scripts/layout.js');?>"></script>
        <script src="/assets/layout/scripts/quick-sidebar.js?salt=<?php echo getFileUpdateTime(
                'layout/scripts/quick-sidebar.js');?>"></script>

    <script type="application/javascript">
        App.user;
        App.toastrMsgs = {!! json_encode(session('toastr_msgs')) !!};
        App.openNewTabUrl = {!! json_encode(session('open_new_tab')) !!};
        App.openNewModalUrl = {!! json_encode(session('open_new_modal')) !!};
        App.csrf = "{{ csrf_token() }}";
        App.lock_params = {!! $lock_params  ?? 'null' !!};
    </script>
    @endif
@stop