<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        @include('layouts.metronic.breadcrumbs-bar')
        <!-- BEGIN PAGE TITLE-->
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        @yield('page_content')
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
@include('layouts.metronic.ajax-modal')
