<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/">
                <img src="{{ asset('assets/global/layout/img/tp-header-logo.png') }}" alt="logo" class="logo-default" /> </a>

            <div class="menu-toggler sidebar-toggler"></div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-left">
                <!-- BEGIN SITE CHANGE DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
{{--                <li class="dropdown dropdown-site">--}}
{{--                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"--}}
{{--                       data-close-others="true">--}}
{{--                        <i class="icon-globe"></i>--}}
{{--                        <span class="label"> {{isset($current_site) ? snakeToWords($current_site->name) : 'Cms'}} </span>--}}
{{--                    </a>--}}
{{--                    <ul class="dropdown-menu dropdown-menu-default">--}}
{{--                        @foreach($sites as $site)--}}
{{--                            <li class="{{ isset($current_site) && $site->id == $current_site->id ? 'active' : null }}">--}}
{{--                                @if($site->id == config('app.cms_site_id'))--}}
{{--                                    <a href="{{route('home')}}">* {{ $site->name }} </a>--}}
{{--                                @else--}}
{{--                                    <a href="{{route('sites.{site}.dashboard.index', $site->id)}}"> {{ snakeToWords($site->name) }} </a>--}}
{{--                                @endif--}}
{{--                            </li>--}}
{{--                        @endforeach--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <!-- END SITE CHANGE DROPDOWN -->

{{--                @if(isset($current_site) && $current_site->id != config('app.cms_site_id'))--}}
{{--                    <!-- BEGIN SITE CHANGE DROPDOWN -->--}}
{{--                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->--}}
{{--                    <li class="dropdown dropdown-site">--}}
{{--                        <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"--}}
{{--                           data-close-others="true">--}}
{{--                            <i class="fa fa-puzzle-piece"></i>--}}
{{--                            <span class="label"> {{ session('site.section_name') ?? 'All Sections' }} </span>--}}
{{--                        </a>--}}
{{--                        <ul class="dropdown-menu dropdown-menu-default">--}}
{{--                            @if($site_sections ?? false)--}}
{{--                                <li class="">--}}
{{--                                    <a href="{{ route('sites.{site}.sections.{section}.set-section', [$current_site->id, 0]) }}"> [Unselect] </a>--}}
{{--                                </li>--}}
{{--                                @foreach($site_sections as $site_section)--}}
{{--                                    <li class="{{ session('site.section_id') == $site_section->id ? 'active' : null }}">--}}
{{--                                        <a href="{{ route('sites.{site}.sections.{section}.set-section', [$current_site->id, $site_section->id]) }}"> {{ snakeToWords($site_section->name) }} </a>--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <!-- END SITE CHANGE DROPDOWN -->--}}
{{--                @endif--}}
            </ul>

            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN NOTIFICATION DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                </li>
                <!-- END NOTIFICATION DROPDOWN -->
                <!-- BEGIN INBOX DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <i class="icon-envelope-open"></i>
                        <span class="badge badge-default"> 0 </span>
                    </a>
                    <ul class="dropdown-menu">
                        {{--<li class="external">--}}
                            {{--<h3>You have--}}
                                {{--<span class="bold">7 New</span> Messages</h3>--}}
                            {{--<a href="app_inbox.html">view all</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">--}}
                                {{--<li>--}}
                                    {{--<a href="#">--}}
                                                {{--<span class="photo">--}}
                                                    {{--<img src="{{ asset('assets/global/layout/img/avatar2.jpg') }}"--}}
                                                         {{--class="img-circle" alt=""> </span>--}}
                                                {{--<span class="subject">--}}
                                                    {{--<span class="from"> Lisa Wong </span>--}}
                                                    {{--<span class="time">Just Now </span>--}}
                                                {{--</span>--}}
                                        {{--<span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    </ul>
                </li>
                <!-- END INBOX DROPDOWN -->
                <!-- BEGIN TODO DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <i class="icon-calendar"></i>
                        <span class="badge badge-default"> 0 </span>
                    </a>
                    <ul class="dropdown-menu extended tasks">
                        {{--<li class="external">--}}
                            {{--<h3>You have--}}
                                {{--<span class="bold">12 pending</span> tasks</h3>--}}
                            {{--<a href="app_todo.html">view all</a>--}}
                        {{--</li>--}}
                        {{--<li>--}}
                            {{--<ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">--}}
                                {{--<li>--}}
                                    {{--<a href="javascript:;">--}}
                                                {{--<span class="task">--}}
                                                    {{--<span class="desc">New release v1.2 </span>--}}
                                                    {{--<span class="percent">30%</span>--}}
                                                {{--</span>--}}
                                                {{--<span class="progress">--}}
                                                    {{--<span style="width: 40%;" class="progress-bar progress-bar-success"--}}
                                                          {{--aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">--}}
                                                        {{--<span class="sr-only">40% Complete</span>--}}
                                                    {{--</span>--}}
                                                {{--</span>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                    </ul>
                </li>
                <!-- END TODO DROPDOWN -->

                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                       data-close-others="true">
                        <img alt="" class="img-circle" src="{!! (auth()->check() && !auth()->user()->thumbnail) ? asset('/assets/global/img/thumbnail/MargeSimpson5.gif') : asset(auth()->user()->thumbnail) !!}"/>
                        <span class="username username-hide-on-mobile"> {{ auth()->user()->first_name .' '. auth()->user()->last_name }} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="{{ route('users.{user_id}.profile.index', auth()->user()->id) }}">
                                <i class="icon-user"></i> My Profile </a>
                        </li>
                        <li>
                            <a href="#">
                                <del><i class="icon-calendar"></i> My Calendar </del></a>
                        </li>
                        <li>
                            <a href="#">
                                <del><i class="icon-envelope-open"></i> My Inbox</del>
                                <span class="badge badge-danger"> 3 </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <del> <i class="icon-rocket"></i> My Tasks</del>
                                <span class="badge badge-success"> 7 </span>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}">
                                <i class="icon-key"></i> Log Out </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                {{--<li class="dropdown dropdown-quick-sidebar-toggler">--}}
                    {{--<a href="javascript:;" class="dropdown-toggle">--}}
                        {{--<i class="icon-logout"></i>--}}
                    {{--</a>--}}
                {{--</li>--}}
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"></div>
<!-- END HEADER & CONTENT DIVIDER -->