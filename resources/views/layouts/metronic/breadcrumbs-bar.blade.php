<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>

        @foreach(getBreadcrumbs() as $crumb)
            <li>
                <a href="{{ $crumb['href'] }}">{{ $crumb['title'] }}</a>
                <i class="fa fa-angle-right"></i>
            </li>
        @endforeach
        <li>
           <span class="last-breadcrumbs">@yield('page_title', Layout::getPageTitle(false))</span>
        </li>
     </ul>
    <div class="page-toolbar">
    </div>
</div>