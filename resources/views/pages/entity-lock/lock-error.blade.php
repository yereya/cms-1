<p> Page is locked by User {{$user->first_name}} {{$user->last_name}} </p>

<a href="{{url()->previous()}}" class="btn btn-primary">Back</a>
@if($hard_lock_permission)
    <a href="#" class="btn btn-primary take-over-btn">Take Over</a>
@endif
