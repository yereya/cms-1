@extends('layouts.metronic.main')

    @section('page_content')

    <div class="row">
        <div class="col-sm-4">

        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Over view',
            'has_form' => true
        ])

        <div class="portlet light profile-sidebar-portlet ">
            <div class="profile-userpic">
                <img src="{{ asset($user->thumbnail) }}" class="img-responsive" alt=""> </div>
            <div class="profile-usertitle">
                <div class="profile-usertitle-name"> {{ $user->first_name . ' ' . $user->last_name }} </div>
                <div class="profile-usertitle-name"> [ {{ '@'. $user->username }} ] </div>
                <div class="profile-usertitle-name" style="font-size: 15px;color: cadetblue;"> {{ $user->email }} </div>
                <hr>
                <div class="profile-usertitle-job"> Role: {{ $user->role or 'Doing some TrafficPoint magic.' }} </div>
            </div>
            <div class="profile-userbuttons">
            </div>
            <div class="profile-usermenu">
                <ul class="nav">
                    <li class="active">
                        <a href="{{ route('users.{user_id}.profile.index', auth()->user()->id) }}">
                            <i class="icon-home"></i> Overview </a>
                    </li>
                    <li>
                        <a href="javascript:void();">
                            <i class="icon-settings"></i> <del>Account Settings</del> </a>
                    </li>
                </ul>
            </div>
        </div>

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])

    </div>

    <div class="col-sm-8">
        @if($user->id == auth()->user()->id ||
            ($user->id != auth()->user()->id && auth()->user()->can($permission_name.'.others.view')))
            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => 'Change details',
                'has_form' => true
            ])

            {!! Form::open([
                'route' => ['users.{user_id}.profile.update', $user->id],
                'method' => 'PUT',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]) !!}

            <div class="form-body profile">
                @include('partials.fields.input', [
                    'name' => 'first_name',
                    'required' => true,
                    'value' => $user->first_name
                ])

                @include('partials.fields.input', [
                    'name' => 'last_name',
                    'required' => true,
                    'value' => $user->last_name
                ])

                @include('partials.fields.input', [
                    'name' => 'password',
                    'type' => 'password',
                    'required' => true,
                    'value' => '********'
                ])

                @include('partials.fields.input', [
                    'name' => 'password_repeat',
                    'type' => 'password',
                    'required' => true,
                    'value' => '********'
                ])
            </div>

            @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => true
                ],
            ])

            {!! Form::close() !!}

            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        @endif

    </div>
</div>

@stop
