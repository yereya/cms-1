@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'User Permissions',
])

<div class="row table-filters">
    <div class="col-md-10">
        <div class="row">
            @include('partials.fields.select', [
                'name' => 'site',
                'label' => 'Site Name',
                'column' => 3,
                'column_label' => 12,
                'column_input' => 12,
                'class' => 'filter_select_datatable filter_select2',
                'list' => \App\Entities\Models\Sites\Site::groupBy('name')->pluck('name', 'id'),
                'value' => '',
                'attributes' => [
                    'data-column_id' => 3
                ]
            ])
        </div>
    </div>
</div>

@include('partials.containers.data-table', [
    'data' => $sites_roles,
    'class' => 'roles-user',
    'columns' => [
        [
            'key' => 'id',
            'label' => 'Role ID',
        ],
        [
            'key' => 'name',
            'label' => 'Role Name',
        ],
        [
            'key' => 'description',
            'label' => 'Description',
            'filter_type' => 'null',
        ],
        [
            'key' => 'site_id',
            'label' => 'Site ID',
        ],
        [
            'key' => 'site_name',
            'label' => 'Site Name',
        ],
        [
            'key' => 'has_permission',
            'label' => 'Toggle',
            'filter_type' => 'null',
            'value' => function ($data) use ($user_id) {
                $state = (isset($data['user_has_role']) && $data['user_has_role']) ? 'checked' : '';
                $res = "<a href='javascript:void(0);'
                    data-property_toggle='". route('roles.{role_id}.toggle-property', [
                        $data['id'],
                        "action=user_site_role" . "&site_id=". $data['site_id'] ."&role_id=". $data['id'] ."&user_id=". $user_id
                    ]) ."'>
                    <span class='toggle_checkbox' data-toggle_state='{$state}'> </span></a>";

                return $res;
            }
        ]
    ]
])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => true
    ]

])
