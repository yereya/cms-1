@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Assign accounts',
])

@include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Publisher Accounts'
    ])

@include('partials.containers.dropdown-buttons', [
            'side' => 'right',
            'dropdown_buttons' => [
                [
                    'icon' => 'glyphicon glyphicon-check',
                    'title' => 'Select All',
                    'attributes' => [
                        'onClick' => "App.toggleAllCheckBoxes('#users_accounts_adwords_table');"
                    ]
                ],
                [
                    'icon' => 'glyphicon glyphicon-unchecked',
                    'title' => 'Unselect All',
                    'attributes' => [
                        'onClick' => "App.toggleAllCheckBoxes('#users_accounts_adwords_table', false);"
                    ]
                ],
            ]
        ])

@include('partials.containers.data-table', [
    'id'   => 'users_accounts_adwords_table',
    'data' => $data,
    'name' => 'ad_words_tab',
    'columns' => [
        [
            'key' => 'source_account_id',
            'label' => 'Account ID',
            'filter_type' => 'null',
        ],
        [
            'key' => 'name',
            'label' => 'Account Name',
            'filter_type' => 'null',
        ],
        [
            'key' => 'status',
            'label' => 'Type'
        ],
        [
            'key' => 'has_permissions',
            'label' => 'Toggle',
            'filter_type' => 'null',
            'value' => function ($data) use ($user_id) {
                $state = $data['has_permissions'] ? 'checked' : '';
                $res = "<a href='javascript:void(0);'
                        data-property_toggle='". route('users.{user_id}.toggle-property', [
                            $user_id, "action=assign_accounts&account_id=" . $data['source_account_id'] .'&user_id='. $user_id]) ."'>
                        <span class='toggle_checkbox' data-toggle_state='{$state}'> </span></a>";
                return $res;
            }
        ]
    ]
])

@include('partials.containers.portlet', [
        'part' => 'close'
    ])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => false,
    ]
])



