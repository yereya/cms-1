@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'User Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($user) ? ['users.update', $user->id] : ['users.store'],
        'method' => isset($user) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">

        <?php
        //Build disable logic for email and user fields
        $user_email_disabled = false;
        if (isset($user) && !auth()->user()->can($permission_name . '.field[user,email].edit'))
            $user_email_disabled = true;
        ?>

        @include('partials.fields.input', [
            'name' => 'username',
            'required' => true,
            'disabled' => $user_email_disabled,
            'value' => isset($user) ? $user->username : null
        ])

        @include('partials.fields.input', [
            'name' => 'first_name',
            'required' => true,
            'value' => isset($user) ? $user->first_name : null
        ])

        @include('partials.fields.input', [
                'name' => 'last_name',
            'value' => isset($user) ? $user->last_name : null
        ])

        @include('partials.fields.input', [
            'name' => 'email',
            'type' => 'email',
            'required' => true,
            'value' => isset($user) ? $user->email : null
        ])

        @include('partials.fields.input-timepicker', [
            'name' => 'password_expiry_date',
            'label' => 'Password Expiry Date',
            'form_line' => true,
            'date' => true,
            'column_label' => 2,
            'value' => isset($user) ? $user->password_expiry_date : $default_password_expiry_date
        ])

        @include('partials.fields.input', [
            'name' => 'password',
            'type' => 'password',
            'required' => true,
            'disabled' => $user_email_disabled,
            'value' => isset($user) ? '********' : null
        ])

        @include('partials.fields.input', [
            'name' => 'password_repeat',
            'type' => 'password',
            'required' => true,
            'value' => isset($user) ? '********' : null
        ])

        @include('partials.fields.select', [
           'name' => 'status',
           'class' => '',
           'label'=>'User Status',
           'list'=>[0=>'Disabled',1 => 'Active'],
           'value' => isset($user) ? $user->status : 0
       ])

    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => true
        ],
        'checkboxes' => [
            'create_another' => isset($user) ? null : true
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop