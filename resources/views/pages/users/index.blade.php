@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Users',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('users.create')
            ]
        ]
    ])

    <style>
        .user-Logs-Download-btn {
            position: relative;
            background-color: #666;
            float: right;
            border: none;
            color: #FFFFFF;
            padding: 10px;
            text-align: center;
            transition-duration: 0.4s;
            text-decoration: none;
            overflow: hidden;
            cursor: pointer;
            margin-bottom: 15px;
            border-radius: 5px;
        }

        .user-Logs-Download-btn:hover{
            background-color: #777;
        }


        .user-Logs-Download-btn:after {
            content: "";
            background: #e1e5ec;
            display: block;
            position: absolute;
            padding-top: 300%;
            padding-left: 350%;
            margin-left: -20px!important;
            margin-top: -120%;
            opacity: 0;
            transition: all 0.8s ease
        }

        .user-Logs-Download-btn:active:after {
            padding: 0;
            margin: 0;
            opacity: 1;
            transition: 0s;
            border-radius: 100%;
        }

        .modal-users {
            display: none;
            position: fixed;
            z-index: 1;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: rgb(0,0,0);
            background-color: rgba(0,0,0,0.4);
        }

        .modal-content-users {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            border: 1px solid #888;
            width: 30%;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
            -webkit-animation-name: animatetop;
            -webkit-animation-duration: 0.4s;
            animation-name: animatetop;
            animation-duration: 0.4s
        }

        @-webkit-keyframes animatetop {
            from {
                top:-300px;
                opacity:0}
            to {
                top:0;
                opacity:1
            }
        }

        @keyframes animatetop {
            from {
                top:-300px;
                opacity:0
            }
            to {
                top:0;
                opacity:1
            }
        }

        .close-users {
            color: white;
            float: right;
            font-size: 32px;
            font-weight: bold;

        }

        .close-users:hover,
        .close-users:focus {
            color: #222;
            text-decoration: none;
            cursor: pointer;
        }

        .modal-header-users {
            padding-right: 2vw;
            padding-left: 2vw;
            padding-bottom: 0.5vh;
            background-color: #34495e;
            color: white;
            display: flex;
            justify-content: space-between;
            align-items: center;
            flex-direction: row;
        }
        .modal-body-users {
            padding: 50px;
        }
        .users-logs-filter{
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
        }
        .filter-field{
            display: flex;
            justify-content: flex-start;
            flex-direction: column;
            margin-bottom: 20px;

        }

    </style>

    @if($userLogsPermissions)
    <div class="download-btn-section">
        <button id="myBtn" class="user-Logs-Download-btn">Download users logs</button>
    </div>
    <div id="myModal" class="modal-users">
        <div class="modal-content-users">
            <div class="modal-header-users">
                <h2>User Logs filter</h2>
                <span class="close-users">&times;</span>
            </div>
            <div class="modal-body-users">
                <div class="users-logs-filter" >
                    <div class="filter-field">
                        <label for="from">From: </label> <input type="date" name="from" class="datepicker">
                    </div>
                    <div class="filter-field">
                        <label for="to">To: </label> <input type="date" name="to" class="datepicker">
                    </div>
                    @include('partials.fields.select', [
                        'name' => 'User_Filter',
                        'label' => 'User Filter',
                        'list' => array_merge(array(""=>""),$users->pluck('attributes.username')->toArray())
                    ])
                    <span data-href="CMSUserActions/exportCsv" id="export" class="btn btn-primary btn-lg">Download</span>
                </div>

            </div>
        </div>
    </div>
    @endif
    <script>
        let temp = $(".close-users").get(0);
        console.log(temp)
        const modal = $("#myModal").get(0)
        const btn = $("#myBtn");
        const span = $(".close-users");
        const form = $("#download-user-logs-form");
        $( window ).load(function (){
            btn.click(function() {
                modal.style.display = "block";
            })

            span.click(function() {
                modal.style.display = "none";
            })

            $(window).click(function(event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            })
            function Datesvalidate(){
                from = new Date($("[name=from]").val());
                console.log(from)
                to = new Date($("[name=to]").val())
                diff = (to-from)/(1000*24*60*60);
                console.log(diff)
                return diff >= 0 ? true : false
            }
            $("#export").click(function(e){
                console.log("CLICK")
                console.log($("[name=user__filter]")[0].nextSibling.textContent.slice(1));
                e.preventDefault();
                console.log($("[name='User_Filter']"));
                let user_name = $("[name=user__filter]")[0].nextSibling.textContent.slice(1)
                /*POST request*/
                if(Datesvalidate()) {
                    console.log("Download")
                    $.ajax({
                        type: "post",
                        url: "{{route("CMSUserActions.exportCsv")}}",
                        data: {
                            _token: "{{csrf_token()}}",
                            user_name: user_name,
                            from: $("[name=from]").val(),
                            to: $("[name=to]").val()
                        },
                        success: function (data, status, response) {
                            console.log(data);
                            csvFile = new Blob([data], {type: "text/csv"})
                            console.log(response.getResponseHeader('Content-Disposition').split(" ")[1].split("=")[1])
                            filename = response.getResponseHeader('Content-Disposition').split(" ")[1].split("=")[1];
                            console.log(filename)

                            // download link
                            let downloadLink = document.createElement("a");

                            // file name
                            downloadLink.download = filename;

                            // create link to file
                            downloadLink.href = window.URL.createObjectURL(csvFile);

                            // hide download link
                            downloadLink.style.display = "";

                            // add link to DOM
                            document.body.appendChild(downloadLink);

                            // click download link
                            downloadLink.click();
                        }
                    });
                }
                else{
                    alert("Dates are invalid, please check again.")
                }
                //window.location.href = "{{route("CMSUserActions.exportCsv")}}";
            })

        })


    </script>

    @include('partials.containers.data-table', [
        'data' => $users,
        'attributes' => [
            'data-page-length' => 50,
        ],
        'columns' => [
            [
                'key' => 'username',
                'value' => function ($data) {
                    return "<a href='". route('users.edit', [$data->id]) ."'>{$data->username}</a>";
                }
            ],
            [
                'key' => 'email',
                'value' => function ($data) {
                    return $data->email;
                }
            ],
            [
                'key' => 'fullname',
                'label' => 'Full Name',
                'value' => function ($data) {
                    return $data->first_name . ' ' . $data->last_name;
                }
            ],
            [
                'key' => 'joined',
                'class' => 'center',
                'value' => function ($data) {
                    return date('F d, Y', strtotime($data->created_at));
                }
            ],
            [
                'key' => 'status',
                'value' => function ($data) {
                    if ($data->status) {
                        return '<span class="label label-sm label-success"> Approved </span>';
                    } else {
                        return '<span class="label label-sm label-danger"> Disabled </span>';
                    }
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= "<a href='". route('users.edit', [$data->id]) ."' title='Edit' class='tooltips' ><span aria-hidden='true' class='icon-pencil'></span></a> ";
                    }

                    $res .= "<a href='". route('permissions.{user_id}.user', [$data->id]) ."'
                        title='Assign permissions'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'
                        ><span aria-hidden='true' class='icon-shield'></span></a> ";

                    $res .= "<a href='". route('roles.{user_id}.user', [$data->id]) ."'
                        title='Assign roles'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'
                        ><span aria-hidden='true' class='icon-key'></span></a> ";

                    $res .= "<a href='" . route('users.{user_id}.account', [$data->id]) . "'
                        title='Assign accounts'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'>
                        <i aria-hidden='true' class='fa fa-sitemap'></i></a>";

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])

@stop