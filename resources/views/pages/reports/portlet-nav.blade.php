{{-- Part: Open --}}
@if($part == 'open')
    <div class="portlet-nav">
        <ul>

            {{-- Part: Open new item --}}
            @elseif($part == 'open-item')
                <li class="nav-item">
                    <a href="javascript:void(0);" class="label">
                        <span class="portlet-nav-text">{{ $label }}</span>
                    </a>
                    <div class="content" tabindex="-1">

                        {{-- Part: Close new item --}}
                        @elseif($part == 'close-item')
                    </div>
                </li>

                {{-- Part: Close --}}
            @elseif($part == 'close')
        </ul>
    </div>
@endif
