@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Tracks History'
    ])

    {!! Form::open([
            'route'  => $submit_route,
            'method' => isset($method) ? $method : 'POST',
            'class'  => 'form-inline',
            'role'   => 'form'
    ]) !!}

    @include('partials.fields.input', [
        'name' => 'tracker_id',
        'label'  => 'Token',
        'column_label' => 3
    ])

    @include('partials.fields.button', [
        'value' => 'Search'
    ])

    {!! Form::close() !!}

    @include('partials.containers.data-table', [
        'data' => $track_history,
        'attributes' => [
            'data-page-length' => 50,
        ],
        'columns' => [
            [
                'key' => 'id',
            ],
            [
                'key' => 'timestamp',
                'label' => 'Time',
            ],
            [
                'key' => 'source_track',
                'label' => 'Source Track'
            ],
            [
                'key' => 'event',
            ],
            [
                'key' => 'campaign_name',
                'label' => 'Camp. Name',
            ],
            [
                'key' => 'commission_amount',
            ],
            [
                'key' => 'sid',
            ],
            [
                'key' => 'keyword',
            ],
            [
                'key' => 'network',
            ],
            [
                'key' => 'clkid',
            ],
            [
                'key' => 'vwr_browser',
                'label' => 'Browser',
            ],
            [
                'key' => 'device',
                'label' => 'Device',
            ],
            [
                'key' => 'vwr_os',
                'label' => 'OS',
            ],
            [
                'key' => 'vwr_country',
                'label' => 'Country',
            ],
            [
                'key' => 'advertiser_name',
                'label' => 'Adv. Name',
            ],

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@endsection