<?php
// Fixes a breaking scenario where $fields['variable'] was empty and
// code as still trying to access the id property



$field_variable = !empty($fields['variable']) ? $fields['variable']->first() : [];
?>
{!! Form::hidden('date_range[column]', isset($field_variable->id) ? $field_variable->id : '') !!}
@include('partials.fields.date-range-picker', [
    'class' => 'date-range',
    'column' => $column ?? 10,
    'column_label' => 12,
    'name' => 'date_range[value]',
    'label' => isset($label) ? $label : 'Date Range',
    'form_line' => true,
    'value' => isset($saved_filter_field) ?
        app(\App\Libraries\ShortCodes\ShortCodesLib::class)->setShortCodes($saved_filter_field->value ?? $saved_filter_field) :
        '',
    'raw_value' => isset($saved_filter_field) ? $saved_filter_field->value ?? $saved_filter_field : '',
    'id'=>$id ?? ''
])