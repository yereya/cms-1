@include('partials.fields.select', [
    'name' => 'where[0][column]',
    'label' => 'Where',
    'no_label' => true,
    'form_line' => true,
    'column' => 4,
    'column_label' => 12,
    'class' => 'select_conditioner select_where_column_name',
    'list' => $fields['where'],
    'value' => isset($saved_filter_field) ? [$saved_filter_field->query_field_id] : $default_values['where']['field'],
    'attributes' => [
                'data-field_input' => '.input_where_field',
                'data-field_select' => '.select_where_field'
            ]
])

@include('partials.fields.select', [
    'name' => 'where[0][operator]',
    'label' => 'Operator',
    'form_line' => true,
    'column' => 2,
    'column_label' => 12,
    'class' => 'where_operator',
    'list' => ['1' => '=', '2' => '<', '3' => '>', '4' => '<>', '5' => 'LIKE', '6' => 'NOT LIKE'],
    'value' => isset($saved_filter_field) && !empty($saved_filter_field->operator) ? [$saved_filter_field->operator] : [1],
    'attributes' => [
                'data-field_input'  => '.input_where_field',
                'data-field_select' => '.select_where_field'
            ]
])

@include('partials.fields.select', [
    'name' => 'where[0][value]',
    'label' => 'Value',
    'no_label' => true,
    'form_line' => true,
    'ajax' => true,
    'multi_select' => true,
    'column' => 4,
    'column_label' => 12,
    'class' => 'select_conditioner select_where_field',
    'value' => isset($saved_filter_field) && !empty($saved_filter_field->value) ? explode(',', $saved_filter_field->value) : [],
    'attributes' => [
            'data-min_input' => 0,
            'data-depend_select2_selector' => '.select_where_column_name',
            'data-depend_select2_field' => isset($saved_filter_field) && !empty($saved_filter_field->query_field_id) ? $saved_filter_field->query_field_id : 'column',
            'data-method' => 'reportWhereValues',
            'data-api_url' => route($route_select2_ajax, $report_id),
            'data-value' => isset($saved_filter_field) && !empty($saved_filter_field->value) ? $saved_filter_field->value : -1,
            'data-multi_select' => true
        ]
])

@include('partials.fields.input', [
    'name' => 'where[0][value][]',
    'label' => 'Value',
    'form_line' => true,
    'column' => 4,
    'column_label' => 12,
    'class' => 'input_where_field',
    'value' => isset($saved_filter_field) ? $saved_filter_field->value : $default_values['where']['value'],
])

@include('partials.containers.actions', [
    'class' => true,
    'column' => 2,
    'actions' => [
        [
            'action' => 'clone',
        ],
        [
            'action' => 'remove',
        ]
    ]
])
