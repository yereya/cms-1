@include('partials.fields.select', [
    'name' => 'having[0][column]',
    'label' => 'Having',
    'no_label' => true,
    'form_line' => true,
    'column' => 4,
    'column_label' => 12,
    'class' => 'select_conditioner tp-tag-form-control select_having_column_name',
    'list' => $fields['having'],
    'value' => isset($saved_filter_field) ? [$saved_filter_field->query_field_id] : [],
    'attributes' => [
                'data-field_input' => '.input_having_field',
            ]
])

@include('partials.fields.select', [
    'name' => 'having[0][operator]',
    'label' => 'Operator',
    'form_line' => true,
    'column' => 2,
    'column_label' => 12,
    'list' => ['1' => '=', '2' => '<', '3' => '>', '4' => '<>', '5' => 'LIKE', '6' => 'NOT LIKE'],
    'value' => isset($saved_filter_field) && !empty($saved_filter_field->operator) ? [$saved_filter_field->operator] : ['=']
])

@include('partials.fields.input', [
    'name' => 'having[0][value][]',
    'label' => 'Value',
    'column' => 4,
    'column_label' => 12,
    'class' => 'input_having_field',
    'form_line' => true,
    'value' => isset($saved_filter_field) ? $saved_filter_field->value : null,
])

@include('partials.containers.actions', [
    'class' => true,
    'column' => 2,
    'actions' => [
        [
            'action' => 'clone',
        ],
        [
            'action' => 'remove',
        ]
    ]
])
