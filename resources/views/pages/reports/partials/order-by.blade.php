@include('partials.fields.select', [
    'name' => 'order_by[key][]',
    'label' => 'Order By',
    'id' => null,
    'no_label' => true,
    'form_line' => true,
    'column' => 4,
    'column_label' => 12,
    'class' => 'key_order',
    'list' => $fields['order_by'],
    'value' => isset($saved_filter_field) ? [$saved_filter_field->query_field_id] : [],
])

@include('partials.fields.select', [
    'name' => 'order_by[value][]',
    'label' => 'Direction',
    'id' => null,
    'column' => 2,
    'column_label' => 12,
    'no_label' => true,
    'form_line' => true,
    'class' => 'key_group',
    'list' => ['ASC' => 'ASC', 'DESC' => 'DESC'],
    'value' => isset($saved_filter_field) && !empty($saved_filter_field->value) ? [$saved_filter_field->value] : ['ASC']
])

<div class="col-md-4"> </div>

@include('partials.containers.actions', [
    'class' => true,
    'column' => 2,
    'actions' => [
        [
            'action' => 'clone',
        ],
        [
            'action' => 'remove',
        ]
    ]
])
