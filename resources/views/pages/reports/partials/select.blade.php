@include('partials.fields.select', [
    'name' => 'select',
    'label' => 'Show Columns',
    'id' => 'select_show_columns',
    'no_label' => true,
    'form_line' => true,
    'sortable' => true,
    'column' => 12,
    'column_label' => 12,
    'class' => 'select_conditioner tp-tag-form-control',
    'list' => $fields['select'],
    'value' => isset($saved_filter_fields['select']) ? $saved_filter_fields['select'] : $fields['select']->filter(function($item) { return !empty($item->order) ;}),
])


