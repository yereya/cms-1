@include('partials.fields.select', [
    'name' => 'group_by[]',
    'label' => 'Group By',
    'id' => null,
    'form_line' => true,
    'column' => 10,
    'column_label' => 12,
    'class' => 'key_group',
    'list' => $fields['group_by'],
    'value' => isset($saved_filter_field) ? [$saved_filter_field->query_field_id] : [$default_values['group_by']]
])

@include('partials.containers.actions', [
    'class' => true,
    'column' => 2,
    'actions' => [
        [
            'action' => 'clone',
        ],
        [
            'action' => 'remove',
        ]
    ]
])
