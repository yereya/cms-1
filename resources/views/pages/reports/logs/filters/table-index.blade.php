<div class="row table-filters">
        {!! Form::open([
            'route' => ['reports.logs.datatable'],
            'method' => 'POST',
            'class' => 'form-horizontal',
            'role' => 'form',

        ]) !!}

    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'error_type',
            'label' => 'Error',
            'multi_select' => true,
            'column' => 4,
            'list' => $error_types
        ])

        @include('partials.fields.select', [
            'name' => 'system_id',
            'label' => 'System',
            'column' => 4,
            'id' => 'system_id',
            'list' => $systems
        ])
    </div>

    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'task_id',
            'label' => 'Task',
            'id' => 'task_id',
            'column' => 4,
            'list' => $tasks,
            'ajax' => true,
            'class' => 'select_conditioner select_where_field',
            'attributes' => [
                    'data-min_input' => 0,
                    'data-dependency_selector[0]' => '#system_id',
                    'data-method' => 'tasks',
                    'data-api_url' => route('reports.logs.select2'),
                ]
        ])

        @include('partials.fields.select', [
            'name' => 'initiator_user_id',
            'label' => 'Initiator',
            'multi_select' => true,
            'column' => 4,
            'list' => $initiator,
            'value' => [],
        ])

        <div class="form-group form-md-line-input col-md-1">
            @include('partials.fields.button', [
                'value' => 'Submit',
                'right' => true,
            ])
        </div>
    </div>

    {!! Form::close() !!}
</div>
