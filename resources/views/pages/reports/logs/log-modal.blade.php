@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => "Log - $title",
])

@if(isset($logs[0]))
    <a href="{{ route('reports.logs.{log_id}.list-group', $logs[0]->parent_id) }}" target="_blank">View in a tab</a>
@endif

@include('partials.containers.data-table', [
    'data' => $logs,
    'class' => 'log-view',
    'attributes' => [
        'data-order' => '[[0, "desc"]]',
   ],
    'columns' => [
           [
               'key' => 'message',
               'filter_type' => 'null',
               'value' => function ($data) {
                    $result = '';
                    switch ($data->error_type) {
                        case 'info' :
                            $result .= '<i class="glyphicon glyphicon-info-sign info-icon" title="Info"> </i>';
                        break;

                        case 'warning' :
                            $result .= '<i class="glyphicon glyphicon-warning-sign warning-icon" style="color: orange;" title="Warning"> </i>';
                        break;

                        case 'error' :
                            $result .= '<i class="glyphicon glyphicon-remove-sign error-icon" style="color: red;" title="Error"> </i>';
                        break;

                        case 'notice' :
                            $result .= '<i class="glyphicon glyphicon-bullhorn notice-icon" title="Notice"> </i>';
                        break;

                        case 'debug' :
                            $result .= '<i class="fa fa-bug debug-icon" title="Debug"> </i>';
                        break;
                    }
                    $result .= ' <span style="font-size: 10px; color: #ccc;">'. $data->created_at .'</span><br>';

                    $result .= '<div class="message">';
                    if (isJson($data->message)) {
                        $_message = strip_tags($data->message);
                        $_message = json_decode($_message, true);

                        if (!is_array($_message)) {
                            $result .= '<br>'. $_message;
                        } else {
                            $result .= array2table($_message, true, true);
                        }
                    } else {
                        $result .= $data->message;
                    }
                    $result .= '</div>';


                    return $result;
               }
           ],
    ]
])
<style type="text/css">
    /*.tp-table .tp-table .tp-table-cell {*/
        /*display: table-row;*/
    /*}*/
    #ajax-modal .dataTables_wrapper td {
        word-break: break-all;
    }
    table.log-view .message {
        font-family: monospace;
        white-space: pre-wrap;
    }
</style>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => [
            "scrollY" => "1000px",
            "bSort" => false
        ]
    ]
])
