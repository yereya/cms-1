@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => "Log - $title",
])

@if (isset($parent_id))
    <a href="{{ route('reports.logs.{log_id}.list-group', $parent_id) }}" target="_blank">View in a tab</a>
@endif



@include('partials.containers.data-table', [
    'data' => $logs,
    'class' => 'log-view',
    'attributes' => [
        'data-order' => '[[0, "desc"]]'
    ],
    'columns' => $columns
])
<style type="text/css">
    /*.tp-table .tp-table .tp-table-cell {*/
    /*display: table-row;*/
    /*}*/
    #ajax-modal .dataTables_wrapper td {
        word-break: break-all;
    }
    table.log-view .message {
        font-family: monospace;
        white-space: pre-wrap;
    }
</style>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => [
            "scrollY" => "1000px",
            "bSort" => false
        ]
    ]
])
