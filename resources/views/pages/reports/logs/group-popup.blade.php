@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.data-table', [
        'data' => $logs,
        'class' => 'log-view',
        'attributes' => [
            'data-order' => '[[0, "desc"]]',
            "data-sort" => "false",
            "data-paging" => "false"
        ],
        'columns' => [
               [
                   'key' => 'message',
                   'filter_type' => 'null',
                   'value' => function ($data) {
                        $result = '';
                        switch ($data->error_type) {
                            case 'info' :
                                $result .= '<i class="glyphicon glyphicon-info-sign info-icon" title="Info"> </i>';
                            break;

                            case 'warning' :
                                $result .= '<i class="glyphicon glyphicon-warning-sign warning-icon" style="color: orange;" title="Warning"> </i>';
                            break;

                            case 'error' :
                                $result .= '<i class="glyphicon glyphicon-remove-sign error-icon" style="color: red;" title="Error"> </i>';
                            break;

                            case 'notice' :
                                $result .= '<i class="glyphicon glyphicon-bullhorn notice-icon" title="Notice"> </i>';
                            break;

                            case 'debug' :
                                $result .= '<i class="fa fa-bug debug-icon" title="Debug"> </i>';
                            break;
                        }
                        $result .= ' <span style="font-size: 10px; color: #ccc;">'. $data->created_at .'</span><br>';

                        $result .= '<div class="message">';
                        if (isJson($data->message)) {
                            $_message = strip_tags($data->message);
                            $_message = json_decode($_message, true);

                            if (!is_array($_message)) {
                                $result .= '<br>'. $_message;
                            } else {
                                $result .= array2table($_message, true, true);
                            }
                        } else {
                            $result .= $data->message;
                        }
                        $result .= '</div>';


                        return $result;
                   }
               ],
        ]
    ])

    <style type="text/css">
        table.log-view .message {
            font-family: monospace;
            white-space: pre-wrap;
        }
    </style>
@endsection

