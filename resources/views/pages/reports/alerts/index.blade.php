@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part'  => 'open',
        'title' => 'Alerts',
    ])

    @include('partials.containers.data-table', [
        'data'  => [],
        'id'    => 'tp-alerts',
        'route' => 'reports.alerts.datatable',
        'server_side' => true,
        'attributes'  => [
            'data-filter_container' => 'table-filters',
            'data-page-length'      => '20',
            'data-order'            => '[[0, "desc"]]',
            'data-onchange_submit'  => 'true',
            'data-route_method'     => 'alertsQuery'
        ],
        'columns' => ['Created At', 'Error Level', 'System', 'Description', 'Content', 'Reminder Date', 'Actions']
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop