@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'notification',
])

@include('partials.containers.data-table', [
    'data' => $notification,
    'class' => 'log-view',
    'attributes' => [
        'data-order' => '[[0, "desc"]]',
   ],
    'columns' => [
           [
               'key' => 'message',
               'filter_type' => 'null',
               'value' => function ($data) {
                    $result = '';
                    switch ($data->error_level) {
                        case 'info' :
                            $result .= '<i class="glyphicon glyphicon-info-sign info-icon" title="Info"> </i>';
                        break;

                        case 'warning' :
                            $result .= '<i class="glyphicon glyphicon-warning-sign warning-icon" style="color: orange;" title="Warning"> </i>';
                        break;

                        case 'error' :
                            $result .= '<i class="glyphicon glyphicon-remove-sign error-icon" style="color: red;" title="Error"> </i>';
                        break;

                        case 'notice' :
                            $result .= '<i class="glyphicon glyphicon-bullhorn notice-icon" title="Notice"> </i>';
                        break;

                        case 'debug' :
                            $result .= '<i class="fa fa-bug debug-icon" title="Debug"> </i>';
                        break;
                    }
                    $result .= ' <span style="font-size: 10px; color: #ccc;">'. $data->created_at .'</span><br>';

                    $result .= '<div class="message">';

                    $result .= $data->content;

                    $result .= '</div>';
                    return $result;
               }
           ],
    ]
])
<style type="text/css">
    #ajax-modal .dataTables_wrapper td {
        word-break: break-all;
    }
    table.log-view .message {
        font-family: monospace;
        white-space: pre-wrap;
    }
</style>

{!! Form::open([
        'url'    => route('reports.alerts.update', [
            'notification_id' => $notification->first()->id
        ]),
        'method' => 'PUT',
        'class'  => 'form-horizontal',
        'role'   => 'form'
]) !!}

@include('partials.fields.input-timepicker', [
    'label'      => 'Follow-up Date Reschedule',
    'name'       => 'reschedule',
    'datetime'   => true,
    'form_line'  => true,
    'column'     => 6,
    'column_label' => 3,
    'label_align_left' => true,
    'value' => isset($notification->first()->pivot->reminder_date) ? $notification->first()->pivot->reminder_date : ''
])

{!! Form::close() !!}

<div class="row">
    <div class="col-md-10">
        <span class="">Mark as Read</span>
        <a href='javascript:void(0);' data-property_toggle="{{ route('reports.alerts.{notification_id}.toggle-property', [
            'notification_id' => $notification->first()->id,
            'action'   => 'viewedStatus',
            'alert_id' => $notification->first()->id
        ]) }}" >
            <span class='toggle_checkbox' data-toggle_state='{{ $state }}'> </span>
        </a>
    </div>
</div>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true,
        'save'  => true
    ],
    'js' => [
        'datatable' => [
            "scrollY" => "1000px",
            "bSort"   => false
        ],
        'date_time_picker' => true
    ]
])