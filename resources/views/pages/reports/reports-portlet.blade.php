@extends('layouts.metronic.main')

@section('page_content')

    @include('partials.containers.data-table-server-side', [
        'title' => isset($report_name) ? $report_name : 'Report',
        'form' => true,
        'part' => 'open',
        'hidden' => ['query_name' => 'bo_reports'],
        'route' => [$route_form, $report_id]
    ])
    <style>
        .portlet-nav .tb-actions-duplicate-group {
            padding-top: 60px;
        }
    </style>

    @include('pages.reports.portlet-nav', ['part' => 'open'])

    @include('pages.reports.portlet-nav', ['part' => 'open-item', 'label' => 'Group By'])
    <div class="group_by_container" data-group="Group by">
        @unset($saved_filter_field)
        @foreach(isset($saved_filter_fields) ? $saved_filter_fields['group_by'] : [] as $saved_filter_field)
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.group-by')
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endforeach
        @if(!isset($saved_filter_fields) || (isset($saved_filter_fields) && $saved_filter_fields['group_by']->isEmpty()))
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.group-by')
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endif
    </div>
    @include('pages.reports.portlet-nav', ['part' => 'close-item'])


    @include('pages.reports.portlet-nav', ['part' => 'open-item', 'label' => 'Date Range'])
    <div class="data_container" data-group="Date range">
        @unset($saved_filter_field)
        @foreach(isset($saved_filter_fields) ? $saved_filter_fields['variable'] : [] as $saved_filter_field)
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.date-range')
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endforeach
        @if(!isset($saved_filter_fields) || (isset($saved_filter_fields) && $saved_filter_fields['variable']->isEmpty()))
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.date-range')
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endif
    </div>
    @include('pages.reports.portlet-nav', ['part' => 'close-item'])


    @include('pages.reports.portlet-nav', ['part' => 'open-item', 'label' => 'Where'])
    <div class="wheres_container" data-group="Where">
        @unset($saved_filter_field)
        @foreach(isset($saved_filter_fields) ? $saved_filter_fields['where'] : [] as $saved_filter_field)
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.where', ['route_select2_ajax' => $route_select2_ajax])
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endforeach
        @if(!isset($saved_filter_fields) || (isset($saved_filter_fields) && $saved_filter_fields['where']->isEmpty()))
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.where', ['route_select2_ajax' => $route_select2_ajax])
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endif

    </div>
    @include('pages.reports.portlet-nav', ['part' => 'close-item'])


    @include('pages.reports.portlet-nav', ['part' => 'open-item', 'label' => 'Having'])
    <div class="having_container" data-group="Having">
        @unset($saved_filter_field)
        @foreach(isset($saved_filter_fields) ? $saved_filter_fields['having'] : [] as $saved_filter_field)
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.having')
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endforeach
        @if(!isset($saved_filter_fields) || (isset($saved_filter_fields) && $saved_filter_fields['having']->isEmpty()))
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.having')
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endif
    </div>
    @include('pages.reports.portlet-nav', ['part' => 'close-item'])


    @include('pages.reports.portlet-nav', ['part' => 'open-item', 'label' => 'Order By'])
    <div class="order_by_container" data-group="Order by">
        @unset($saved_filter_field)
        @foreach(isset($saved_filter_fields) ? $saved_filter_fields['order_by'] : [] as $saved_filter_field)
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.order-by')
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endforeach
        @if(!isset($saved_filter_fields) || (isset($saved_filter_fields) && $saved_filter_fields['order_by']->isEmpty()))
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            @include('pages.reports.partials.order-by')
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endif
    </div>
    @include('pages.reports.portlet-nav', ['part' => 'close-item'])


    @include('pages.reports.portlet-nav', ['part' => 'open-item', 'label' => 'Show Columns'])
    <div class="select_container" data-group="Show fields">
        @include('partials.containers.duplicate-group', ['part' => 'open'])
        @include('pages.reports.partials.select')
        @include('partials.containers.duplicate-group', ['part' => 'close'])
    </div>
    @include('pages.reports.portlet-nav', ['part' => 'close-item'])


    @include('pages.reports.portlet-nav', ['part' => 'close'])

    @include('partials.containers.data-table-server-side', [
        'part' => 'close',
        'form' => true
    ])

@endsection