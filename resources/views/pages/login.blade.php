@extends('layouts.base')

@section('content')
    <div class="logo">
        <a href="{{ route('home') }}"><img src="{{ asset('assets/pages/img/logo-big.png') }}"
                                           alt="{{ Layout::getPageTitle() }}"/></a>
    </div>

    <div class="content">

    </div>
    <div class="copyright"> <span id="this-year"></span> &copy; Traffic Point LTD.</div>
    <script>
        var d = new Date();
        document.getElementById('this-year').innerHTML = d.getFullYear();
    </script>
@stop