@extends('layouts.metronic.main')


@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Bayesian Tasks',
        'actions' => [

            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('bo.bayesiantasks.create')
            ]

        ]
    ])

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'bayesiantasks-table',
        'url' => route('bo.bayesiantasks.datatable', ['url_query' => $url_query]),
        'server_side' => true,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[0, "desc"]]',
            'data-onchange_submit' => 'true',
            'data-route_method' => 'bayesiantasksQuery',
            'data-search' => 'search',
            'data-scrollX' => 'true',

            //the "processing" loader
            'data-blocking_loader' => 'true'
        ],
        'columns' => ['test_name','ab_testing_sale','ab_testing_lead','ab_testing_ctr','learn_prior','days_for_prior','ab_testing_rpm','expirament_type','advertiser_name',
                        'page_name','dates_to_omit','dates_to_omit_prior','start_date','end_date','lineup_label_a','lineup_label_b','test_description','task_status','last_run','actions']
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop

