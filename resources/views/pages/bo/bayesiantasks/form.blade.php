@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'New Bayesian Task',
        'has_form' => true
    ])


    {{--Fetcher Portlet Start --}}
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => "Fill in the following form",
        'has_form' => true,
        'sortable' => 'portlet-sortable',
        'actions' => [

        ]
    ])

        {!! Form::open([
         'route' => isset($bayesiantask) ? ['bo.bayesiantasks.update', $bayesiantask->id] : ['bo.bayesiantasks.store'],
         'method' => isset($bayesiantask) ? 'PUT' : 'POST',
         'class' => 'form-horizontal',
         'role' => 'form'
        ]) !!}

<script>
$(document).ready(function() {
  $('#datepicker_datesToOmit').datepicker({
    multidate: true,
    format: "yyyy-mm-dd"
  });

  $('#datepicker_datesToOmitPrior').datepicker({
    multidate: true,
    format: "yyyy-mm-dd"
  });
});


</script>

    <div class="form-body">
{{--        form-md-line-input--}}
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class="control-label col-sm-pull-4" style="width: 50%;{!! isset($bayesiantask)?'pointer-events: none;':''!!}">
                @include('partials.fields.input', [
                        'name' => 'test_name',
                        'label' => "Test's Name",
                        'form_line' => true,
                        'column' => 8,
                        'column_label' => 3,
                        'required' => true,
                        'value' => isset($bayesiantask) ? $bayesiantask->TEST_NAME : ''
                    ])
            </div>
        </div>
        @include('partials.fields.h4-separator', ['label' => "AB Testing",])
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class=" col-sm-pull-4" style="width: 50%">
                @include('partials.fields.select', [
                   'name' => 'ab_testing_rpm',
                   'label' => 'AB Testing RPM',
                   'form_line' => false,
                   'column' => 8,
                   'column_label' => 3,
                   'list' => ['1' => 'True','0' => 'False'],
                   'value' =>  isset($bayesiantask)&&($bayesiantask->AB_TESTING_RPM == 0) ? '0' : '1'
               ])
            </div>
            <div  class=" col-sm-pull-4" style="width: 50%">
                @include('partials.fields.select', [
                    'name' => 'ab_testing_sale',
                    'label' => 'AB Testing Sale',
                    'form_line' => false,
                    'column' => 8,
                    'column_label' => 3,
                    'list' => ['1' => 'True','0' => 'False'],
                    'value' =>   isset($bayesiantask)&&($bayesiantask->AB_TESTING_SALE == 0) ? '0' : '1'
                ])

            </div>
        </div>

        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class=" col-sm-pull-4" style="width: 50%">
                @include('partials.fields.select', [
                              'name' => 'ab_testing_lead',
                              'label' => 'AB Testing Lead',
                              'form_line' => false,
                              'column' => 8,
                              'column_label' => 3,
                              'list' => ['1' => 'True','0' => 'False'],
                              'value' =>  isset($bayesiantask)&&($bayesiantask->AB_TESTING_LEAD == 0) ? '0' : '1'
                          ])
            </div>
            <div  class=" col-sm-pull-4" style="width: 50%">
                @include('partials.fields.select', [
                    'name' => 'ab_testing_ctr',
                    'label' => 'AB Testing CTR',
                    'form_line' => false,
                    'column' => 8,
                    'column_label' => 3,
                    'list' => ['1' => 'True','0' => 'False'],
                    'value' =>  isset($bayesiantask)&&($bayesiantask->AB_TESTING_CTR == 0) ? '0' : '1'
                ])
            </div>
        </div>
        <br><br>
        @include('partials.fields.h4-separator', ['label' => "Prior",])
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class=" col-sm-pull-4" style="width: 50%">
                @include('partials.fields.select', [
                    'name' => 'learn_prior',
                    'label' => 'Learn Prior',
                    'form_line' => false,
                    'column' => 8,
                    'column_label' => 3,
                    'list' => ['1' => 'True','0' => 'False'],
                    'value' =>   isset($bayesiantask)&&($bayesiantask->LEARN_PRIOR == 1) ? '1' : '0'
                ])
            </div>
            <div  class="control-label col-sm-pull-4" style="width: 50%;">
                @include('partials.fields.input', [
                        'name' => 'days_for_prior',
                        'label' => 'Days For Prior',
                        'form_line' => true,
                        'column' => 8,
                        'column_label' => 3,
                        'value' => isset($bayesiantask) ? $bayesiantask->DAYS_FOR_PRIOR : ''
                    ])
            </div>
        </div>

        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class=" col-sm-pull-4" style="width: 50%">
                @include('partials.fields.select', [
                    'name' => 'expirament_type',
                    'label' => 'Experiment Type',
                    'form_line' => false,
                    'column' => 8,
                    'column_label' => 3,
                    'required' => true,
                    'list' => ['__design_pc' => '__design_pc','__design_mobile' => '__design_mobile', '__lineup_label_pc' => '__lineup_label_pc', '__lineup_label_mobile' => '__lineup_label_mobile'],
                    'value' =>  isset($bayesiantask) ? $bayesiantask->EXPIRAMENT_TYPE :''
                ])
            </div>
        </div>
        <br><br>
        @include('partials.fields.h4-separator', ['label' => "Advertiser & Page Name",])
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class=" col-sm-pull-4" style="width: 50%">
                @include('partials.fields.select', [
                          'name' => 'advertiser_name',
                          'label' => 'Advertiser Name',
                          'form_line' => true,
                          'column' => 8,
                          'column_label' => 3,
                          'required' => true,
                          'list' => $advertisers_list->pluck('name','name'),
                          'value' => isset($bayesiantask) ? $bayesiantask->ADVERTISER_NAME :''
                      ])
            </div>
            <div  class="control-label col-sm-pull-4" style="width: 50%;">
                @include('partials.fields.input', [
                        'name' => 'advertiser_id',
                        'label' => 'Advertiser Id',
                        'form_line' => true,
                        'column' => 8,
                        'column_label' => 3,
                        'value' => ''
                    ])
            </div>
        </div>
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class="control-label col-sm-pull-4" style="width: 50%;">
                @include('partials.fields.input', [
                        'name' => 'page_name',
                        'label' => 'Page Name',
                        'form_line' => true,
                        'column' => 8,
                        'column_label' => 3,
                        'value' => isset($bayesiantask) ? $bayesiantask->PAGE_NAME :''
                    ])
            </div>
        </div>
        <br><br>
{{--        -----------------------------------------------------------------------------------------------------------}}
        @include('partials.fields.h4-separator', ['label' => "Dates",])
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
                <div  class="control-label col-sm-pull-4" style="width: 50%;margin-left: 0px;">
                    <div class="input-group date form-group" id="datepicker_datesToOmit">
                        @include('partials.fields.input', [
                            'name' => 'dates_to_omit',
                            'label' => 'Dates To Omit',
                            'form_line' => true,
                            'column' => 8,
                            'column_label' => 4,
                            'value' => isset($bayesiantask) ? $bayesiantask->DATES_TO_OMIT :''
                        ])
                        <span class="input-group-addon" style="border-bottom: 0px"></span>
                    </div>
                </div>
            <div  class="control-label col-sm-pull-4" style="width: 50%;">
                <div class="input-group date form-group" id="datepicker_datesToOmitPrior">
                    @include('partials.fields.input', [
                         'name' => 'dates_to_omit_prior',
                         'label' => 'Dates To Omit Prior',
                         'form_line' => true,
                         'column' => 8,
                         'column_label' => 4,
                         'value' => isset($bayesiantask) ? $bayesiantask->DATES_TO_OMIT_PRIOR :''
                     ])
                    <span class="input-group-addon" style="border-bottom: 0px"></span>
                </div>

            </div>
        </div>
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class="control-label col-sm-pull-4" style="width: 50%;">

                @include('partials.fields.input', [
                      'name' => 'start_date',
                      'label' => 'Start Date',
                      'form_line' => true,
                      'column' => 8,
                      'column_label' => 3,
                      'required' => true,
                      'value' => isset($bayesiantask) ? $bayesiantask->START_DATE : date('Y-m-d',strtotime('today'))
                  ])

            </div>
            <div  class="control-label col-sm-pull-4" style="width: 50%;">

                @include('partials.fields.input', [
                     'name' => 'end_date',
                     'label' => 'End Date',
                     'form_line' => true,
                     'column' => 8,
                     'column_label' => 3,
                     'value' => isset($bayesiantask) ? $bayesiantask->END_DATE : ''
                 ])
            </div>
        </div>
        <br><br>
        @include('partials.fields.h4-separator', ['label' => "Labels",])
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class="control-label col-sm-pull-4" style="width: 50%;">
                @include('partials.fields.input', [
                                      'name' => 'lineup_label_a',
                                      'label' => 'Lineup Label A',
                                      'form_line' => true,
                                      'column' => 8,
                                      'column_label' => 3,
                                      'value' => isset($bayesiantask) ? $bayesiantask->LINEUP_LABEL_A : ''
                                  ])
            </div>
            <div  class="control-label col-sm-pull-4" style="width: 50%;">
                @include('partials.fields.input', [
                                               'name' => 'lineup_label_b',
                                               'label' => 'Lineup Label B',
                                               'form_line' => true,
                                               'column' => 8,
                                               'column_label' => 3,
                                               'value' => isset($bayesiantask) ? $bayesiantask->LINEUP_LABEL_B : ''
                                           ])
            </div>
        </div>
        <br><br>
        @include('partials.fields.h4-separator', ['label' => "Test's Description",])

        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class="control-label col-sm-pull-4" style="width: 50%;{!! isset($bayesiantask)?'pointer-events: none;':''!!}">
                @include('partials.fields.input', [
                        'name' => 'test_description',
                        'label' => 'Test Description',
                        'form_line' => true,
                        'column' => 8,
                        'column_label' => 4,
                        'value' => isset($bayesiantask) ? $bayesiantask->TEST_DESCRIPTION : ''
                    ])
            </div>
        </div>

        <br><br>
        @include('partials.fields.h4-separator', ['label' => "Status",])
        <br><br>
        <div class="form-group form-horizontal form-md-line-input" style="display: flex;">
            <div  class=" col-sm-pull-4" style="width: 50%;{!! isset($bayesiantask->task_status)&&!in_array($bayesiantask->task_status,array('in_progress',''))?'pointer-events: none;':''!!}">
                @include('partials.fields.select', [
                   'name' => 'task_status',
                   'label' => 'Current Status',
                   'form_line' => true,
                   'column' => 8,
                   'column_label' => 3,
                   'list' =>!isset($bayesiantask)||in_array($bayesiantask->task_status,array('in_progress','')) ?  array(' '=>'','in_progress' => 'in_progress' ) : array(' '=>'','in_progress' => 'in_progress' ,'processing'=>'processing','done' => 'done')   ,
                   'value' => isset($bayesiantask) ? $bayesiantask->task_status  : ''
               ])
            </div>
            <div  class="col-sm-pull-4" style="width: 50%;{!! isset($bayesiantask->task_status)&&!in_array($bayesiantask->task_status,array('in_progress',''))?'pointer-events: none;':''!!}">
                @include('partials.fields.checkbox', [
                  'label' => 'Run Only Once',
                  'wrapper' => true,
                  'list' => [
                      [
                          'name' => 'RUN_ONCE',
                          'value' => 1,
                          'checked' => isset($bayesiantask) ? $bayesiantask->RUN_ONCE  : 0
                      ],
                  ],
              ])
            </div>
        </div>
    <br><br>


        @include('partials.containers.form-footer-actions', [
           'buttons' => [
               'cancel' => [
                   'route' => route('bo.bayesiantasks.index')//back_url()
               ],
                'submit' => 'true',
                'custom' => [
                    [
                        'type' => 'submit',
                        'value' => 'Stop',
                        'class' => isset($bayesiantask) ? 'btn btn-danger' : 'invisible',
                        'url' => isset($bayesiantask) ? route('bo.bayesiantasks.stop',['id' => $bayesiantask->id]) : route('bo.bayesiantasks.index')// stop task
                    ],
                ]
               ]
           ])
        <br>

    {!! Form::close() !!}
        <br><br>
    </div> {{-- End of form body --}}
    @include('partials.containers.portlet', [
        'part' => 'close'
    ])

@stop
