@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
            'title' => 'Accounts',
            'actions' => [
                [
                    'drop_down' => true,
                    'title' => 'Actions',
                    'icon' => 'fa fa-th',
                    'menus' => [
                        [
                            'title' => 'Add New Account',
                            'route' => url($link_create),
                            'permission' => auth()->user()->can($permission_name .'.add')
                        ],
                        [
                            'container' => true,
                            'context' => [
                                'include' => 'partials.containers.buttons.download',
                                'route' => ['bo.accounts.datatable'],
                                'title' => 'Download CSV'
                            ]
                        ]
                    ]
                ]
            ]
        ])

    @include('pages.bo.accounts.filters.table-accounts', [
        'publisher' => $publisher_id,
        'advertiser' => $advertiser_id
    ])
    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'accounts',
        'server_side' => true,
        'url' => $link_datatable,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[0, "asc"]]',
            'data-route_method' => 'accounts',

            //the "processing" loader
            'data-blocking_loader' => 'true'
        ],
        'columns' => ['Id', 'Name', 'Source account id', 'Chanel', 'Status', 'Actions']
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop