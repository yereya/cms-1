<div class="row table-filters">
    <div class="form-inline">
        {!! Form::open([
            'route' => ['bo.accounts.datatable'],
            'method' => 'POST',
            'class' => 'form-horizontal',
            'role' => 'form',
            'data-datatable_id' => 'accounts'
        ]) !!}

        @include('partials.fields.select', [
            'name' => 'advertiser',
            'label' => 'Advertisers',
            'column' => 4,
            'list' => \App\Entities\Models\Bo\Advertiser::orderBy('name', 'asc')->pluck('name', 'id'),
            'value' => isset($advertiser_id) ? $advertiser_id : ''
        ])

        @include('partials.fields.select', [
            'name' => 'publisher',
            'label' => 'Publishers',
            'column' => 4,
            'list' => \App\Entities\Models\Bo\Publishers\Publisher::orderBy('publisher_name', 'asc')->pluck('publisher_name', 'id'),
            'value' => isset($publisher_id) ? $publisher_id : ''
        ])

        <div class="form-group form-md-line-input col-md-4">
            @include('partials.fields.button', [
                'value' => 'Submit',
            ])
        </div>

        {!! Form::close() !!}
    </div>
</div>