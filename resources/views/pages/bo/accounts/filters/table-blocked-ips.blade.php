<div class="row table-filters">
{!! Form::open([
    'route' => ['bo.accounts.datatable'],
    'method' => 'POST',
    'class' => 'form-horizontal',
    'role' => 'form',
    'data-datatable_id' => 'accounts'

]) !!}

    {!! Form::hidden('account_id', $account_id) !!}

{!! Form::close() !!}
</div>