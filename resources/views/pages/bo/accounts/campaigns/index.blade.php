@extends('layouts.metronic.main')

@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Campaigns',
    ])

    @include('partials.containers.data-table', [
           'data' => $campaigns,
           'columns' => [
               [
                   'key' => 'id',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'campaign_id',
                   'value' => function ($data) {
                       return $data->campaign_id;
                   }
               ],
               [
                   'key' => 'campaign_name',
                   'value' => function ($data) use ($account_id, $permission_name) {
                        if (auth()->user()->can($permission_name .'.edit')) {
                            return '<a href="'.route('bo.accounts.{account_id}.campaigns.edit', [$account_id, $data->id]).'">'.$data->campaign_name.'</a>';
                        } else {
                            return $data->campaign_name;
                        }
                   }
               ]
           ]
        ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
