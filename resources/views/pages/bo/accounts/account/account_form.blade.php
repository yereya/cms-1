@extends('layouts.metronic.main')
{{ dump(get) }}
@section('page_title')
    <small>Publisher</small> {{ $publisher->publisher_name }} <small>Account</small>
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($account) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'route' => isset($account) ? ['bo.publishers.{publisher_id}.accounts.update', $publisher->id, $account->id] : ['bo.publishers.{publisher_id}.accounts.store', $publisher->id],
            'method' => isset($account) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($account) ? $account->name : null
        ])

    @include('partials.fields.select', [
        'name' => 'type',
        'label' => 'Status',
        'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
        'value' => isset($account) ? strtolower($account->status) : ''
    ])

    @include('partials.fields.h4-separator', ['label' => 'Accounts'])

    @if(isset($account))
        @foreach($accounts as $pacc)
            @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 2])

            @include('partials.fields.select', [
                'name' => 'accounts[]',
                'label' => '',
                'no_label' => true,
                'form_line' => true,
                'column' => 6,
                'column_label' => 0,
                'class' => 'key_group',
                'list' => \App\Entities\Models\Bo\Account::pluck('name', 'source_account_id'),
                'value' => $pacc->source_account_id
            ])

            @include('partials.containers.actions', [
                        'class' => true,
                        'actions' => [
                            [
                                'action' => 'clone',
                            ],
                            [
                                'action' => 'remove',
                            ]
                        ]
                        ])

            @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
        @endforeach
    @endif

    @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 2])

        @include('partials.fields.select', [
            'name' => 'accounts[]',
            'label' => '',
            'no_label' => true,
            'form_line' => true,
            'column' => 6,
            'column_label' => 0,
            'class' => 'key_group',
            'list' => \App\Entities\Models\Bo\Account::pluck('name', 'source_account_id'),
            'value' => []
        ])

        @include('partials.containers.actions', [
                    'class' => true,
                    'actions' => [
                        [
                            'action' => 'clone',
                        ],
                        [
                            'action' => 'remove',
                        ]
                    ]
                    ])

    @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])

    @include('partials.fields.h4-separator', ['label' => 'Conversions'])

    <div class="form-group form-horizontal form-md-line-input">
    @include('partials.fields.checkbox', [
        'label' => 'Lead',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'lead',
                  'value' => 1,
                  'checked' => isset($account) ? $account->lead : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Sale',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'sale',
                  'value' => 1,
                  'checked' => isset($account) ? $account->sale : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Call',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'call',
                  'value' => 1,
                  'checked' => isset($account) ? $account->call : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Click Out',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'click_out',
                  'value' => 1,
                  'checked' => isset($account) ? $account->click_out : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Install',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'install',
                  'value' => 1,
                  'checked' => isset($account) ? $account->install : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Canceled Sale',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'canceled_sale',
                  'value' => 1,
                  'checked' => isset($account) ? $account->canceled_sale : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Canceled Lead',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'canceled_lead',
                  'value' => 1,
                  'checked' => isset($account) ? $account->canceled_lead : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Adjustment',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'adjustment',
                  'value' => 1,
                  'checked' => isset($account) ? $account->adjustment : 1
            ],
        ]
    ])

    </div>

    @if(isset($publisher))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop


