@extends('layouts.metronic.main')

@section('page_title')
    <small>Accounts</small> {{ $account->name }} <small>Block queries</small>
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Block Queries',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Query',
                    'target' => route('bo.accounts.{account_id}.block-queries.create', [$account->id]),
                    'permission' => auth()->user()->can($permission_name .'.add')
                ]
            ]
        ])

    @include('partials.containers.data-table', [
           'data' => $block_queries,
           'columns' => [
               [
                   'key' => 'id',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'name',
                   'value' => function ($data) {
                       return $data->name . $data->source[0];
                   }
               ],
               [
                   'key' => 'type',
                   'value' => function ($data) {
                       return strtoupper($data->type);
                   }
               ],
               [
                    'key' => 'status',
                    'value' => function ($data) {
                        if ($data->status) {
                            return '<span class="label label-sm label-success"> Active </span>';
                        } else {
                            return '<span class="label label-sm label-danger"> Disabled </span>';
                        }
                    }
               ],
               [
                   'key' => 'last run',
                   'value' => function ($data) {
                       return strtoupper($data->last_run);
                   }
               ],
               [
                   'key' => 'created',
                   'value' => function ($data) {
                       return strtoupper($data->created_at);
                   }
               ],
               [
                   'key' => 'updated',
                   'value' => function ($data) {
                       return strtoupper($data->updated_at);
                   }
               ],
               [
                   'key' => 'actions',
                   'value' => function ($data) use ($account, $permission_name) {
                       $res = '';

                        if (auth()->user()->can($permission_name .'.view')) {
                            $res .= "<a href='" . route('bo.accounts.{account_id}.block-queries.edit', [$account->id, $data->id]) . "' class='tooltips' data-original-title='Edit action'><span aria-hidden='true' class='icon-pencil fa-fw'></span></a>";
                        }
                        if (auth()->user()->can('bo.advertisers.accounts.block_ips.view')) {
                            $res .= "<a href='" . route('bo.accounts.{account_id}.blocked-ips.index', [$account->id, 'query' => $data->id]) . "' class='tooltips' data-original-title='Blocked Ips'><i aria-hidden='true' class='fa fa-minus-circle fa-fw'></i></a>";
                        }

                       return $res;
                   }
               ]
           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop
