@extends('layouts.metronic.main')

@section('page_title')
    <small>Accounts</small> {{ $account->name }} <small>Block query</small>
    @if (isset($BlockQuery))
        {{ $BlockQuery->name }}
    @endif
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($BlockQuery) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'route' => isset($BlockQuery) ? ['bo.accounts.{account_id}.block-queries.update', $account->id, $BlockQuery->id] : ['bo.accounts.{account_id}.block-queries.store', $account->id],
            'method' => isset($BlockQuery) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($BlockQuery) ? $BlockQuery->name : null
        ])

    @include('partials.fields.select', [
            'name' => 'type',
            'required' => true,
            'list' => $types,
            'value' => isset($BlockQuery) ? $BlockQuery->type : null
        ])

    @include('partials.fields.select', [
            'name' => 'status',
            'required' => true,
            'list' => [
                    ['id' => 1, 'text' => 'Active'],
                    ['id' => 0, 'text' => 'Disabled'],
                ],
            'value' => isset($BlockQuery) ? $BlockQuery->status : 1
        ])

    @include('partials.fields.textarea', [
            'name' => 'query',
            'required' => true,
            'value' => isset($BlockQuery) ? $BlockQuery->query : null
        ])

    @if(isset($BlockQuery))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop


