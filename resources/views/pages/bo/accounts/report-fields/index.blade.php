@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Report Fields',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Report Fields',
                    'target' => route('bo.accounts.{account_id}.report-fields.create', $account_id),
                    'permission' => auth()->user()->can($permission_name .'.add')
                ]
            ]
        ])

    @include('partials.containers.data-table', [
           'data' => $report_fields,
           'columns' => [
               [
                   'key' => 'id',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'report',
                   'value' => function ($data) {
                       return $data->report->name;
                   }
               ],
               [
                   'key' => 'source_field',
                   'value' => function ($data) {
                       return $data->source_field;
                   }
               ],
               [
                   'key' => 'field',
                   'label' => 'Destination field',
                   'value' => function ($data) {
                       return $data->field;
                   }
               ],
               [
                   'key' => 'operator',
                   'value' => function ($data) {
                       return $data->operator;
                   }
               ],
               [
                   'key' => 'value_type',
                   'value' => function ($data) {
                       return $data->value_type;
                   }
               ],
               [
                   'key' => 'value',
                   'value' => function ($data) {
                       return $data->value;
                   }
               ],
               [
                   'key' => 'actions',
                   'filter_type' => 'null',
                   'value' => function ($data) use ($account_id, $permission_name) {
                        $res = '';

                        if (auth()->user()->can($permission_name .'.edit')) {
                            $res .= "<a href='" . route('bo.accounts.{account_id}.report-fields.edit', [$account_id, $data->id]) . "' class='tooltips' data-original-title='Edit action'><span aria-hidden='true' class='icon-pencil'></span></a>";
                        }

                        if (auth()->user()->can($permission_name.'.delete')) {
                            $res .= "<a href='". route('bo.accounts.{account_id}.report-fields.{field_id}.ajax-modal', [$account_id, $data->id, 'method=deleteField']) ."' data-toggle='modal'
                            data-target='#ajax-modal'><span aria-hidden='true' title='Delete field' class='icon-trash tooltips'></span></a> ";
                        }

                        return $res;
                   }
               ]
           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop
