@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($field) ? 'Edit Account Report Field' : 'Create Account Report Field'
    ])

    {!! Form::open([
            'route' => isset($field) ? ['bo.accounts.{account_id}.report-fields.update', $account->id, $field->id] : ['bo.accounts.{account_id}.report-fields.store', $account->id],
            'method' => isset($field) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
        'name' => 'account_name',
        'label' => 'Account Name',
        'disabled' => true,
        'value' => isset($account) ? $account->name : null
    ])

    {!! Form::hidden('account_report_id', $account->id) !!}

    @include('partials.fields.select', [
        'name' => 'report_id',
        'required' => true,
        'list' => \App\Entities\Models\Bo\Report::pluck('name', 'id'),
        'value' => isset($field) ? $field['report_id'] : null
    ])

    @include('partials.fields.select', [
        'name' => 'field',
        'list' => \App\Entities\Models\Bo\AccountReportField::getFieldName(),
        'value' => isset($field) ? $field['field'] : null
    ])

    @include('partials.fields.select', [
        'name' => 'source_field',
        'list' => \App\Entities\Models\Bo\AccountReportField::getSourceField(),
        'value' => isset($field) ? $field['source_field'] : null
    ])

    @include('partials.fields.select', [
        'name' => 'operator',
        'list' => $operators,
        'value' => isset($field) ? $field['operator'] : ''
    ])

    @include('partials.fields.select', [
        'name' => 'value_type',
        'list' => $value_types,
        'value' => isset($field) ? $field['value_type'] : ''
    ])

    @include('partials.fields.input', [
        'name' => 'value',
        'required' => true,
        'value' => isset($field) ? $field['value'] : null
    ])

    @if(isset($field))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop


