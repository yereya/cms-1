@extends('layouts.metronic.main')

@section('page_title')
    <small>Account</small> @if (isset($account)) {{ $account->name }} @endif
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($account) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'url' => isset($account) ? $update_link : $store_link,
            'method' => isset($account) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($account) ? $account['name'] : null
        ])

    @include('partials.fields.input', [
            'name' => 'source_account_id',
            'required' => true,
            'value' => isset($account) ? $account['source_account_id'] : null
        ])

    @include('partials.fields.select', [
        'name' => 'advertiser_id',
        'label' => 'Advertiser',
        'required' => true,
        'list' => $advertisers_list,
        'value' => isset($account) ? $account->advertiser_id : $advertiser_id
    ])

    @include('partials.fields.select', [
        'name' => 'publisher_id',
        'label' => 'Publisher',
        'required' => true,
        'list' => $publishers_list,
        'value' => isset($account) ? $account->publisher_id : $publisher_id
    ])

    @include('partials.fields.select', [
        'name' => 'status',
        'label' => 'Status',
        'required' => true,
        'list' => ['inactive' => 'Inactive', 'active' => 'Active', 'pending' => 'Pending'],
        'value' => isset($account) ? $account->status : 1
    ])

    @include('partials.fields.select', [
        'name' => 'chanel',
        'label' => 'Chanel',
        'list' => ['display' => 'Display', 'search' => 'Search', 'affiletes' => 'Affiletes', 'content' => 'Content', 'mobile' => 'Mobile', 'social' => 'Social'],
        'value' => isset($account) ? $account->chanel : ''
    ])

    @include('partials.fields.select', [
        'name' => 'currency',
        'label' => 'Currency',
        'list' => ['USD' => 'USD', 'EUR' => 'EUR', 'GBP' => 'GBP'],
        'value' => isset($account) ? $account->currency : ''
    ])

    @include('partials.fields.select', [
        'name' => 'sale_group',
        'label' => 'Sale Group',
        'list' => $sale_group_list,
        'value' => isset($account) ? $account->sale_group : ''
    ])

    @include('partials.fields.select', [
        'name' => 'product_name',
        'label' => 'Product Name',
        'list' => $product_name_list,
        'value' => isset($account) ? $account->product_name : ''
    ])

    @include('partials.fields.input', [
            'name' => 'days_to_reduce',
            'required' => true,
            'value' => isset($account) ? $account['days_to_reduce'] : null
        ])

    @include('partials.fields.input', [
            'name' => 'target_roi',
            'required' => true,
            'value' => isset($account) ? $account['target_roi'] : null
        ])

    @include('partials.fields.select', [
        'name' => 'mcc',
        'label' => 'Mcc',
        'list' => \App\Entities\Models\Bo\Mcc::pluck('name', 'id'),
        'value' => isset($account) ? $account->mcc : ''
    ])

    @include('partials.fields.select', [
        'name' => 'report',
        'multi_select' => true,
        'list' => \App\Entities\Models\Bo\Report::pluck('name', 'id'),
        'value' => isset($report) ? $report : ''
    ])

    @include('partials.fields.select', [
            'name' => 'time_zone_location',
            'label' => 'Time Zone Location',
            'list' => $time_zone_location,
            'required' => true,
            'id' => 'time_zone_location',
            'class' => 'select_conditioner select_where_field',
            'value' => isset($account) ? [$account->time_zone . ';' . $account->time_zone_location] : [],
        ])

    @include('partials.fields.h4-separator', ['label' => 'Conversions'])
    @include('partials.fields.checkbox', [
        'label' => 'Lead',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'lead',
                  'value' => 1,
                  'checked' => isset($account) ? $account->lead : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Sale',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'sale',
                  'value' => 1,
                  'checked' => isset($account) ? $account->sale : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Call',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'call',
                  'value' => 1,
                  'checked' => isset($account) ? $account->call : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Click Out',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'click_out',
                  'value' => 1,
                  'checked' => isset($account) ? $account->click_out : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Install',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'install',
                  'value' => 1,
                  'checked' => isset($account) ? $account->install : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Canceled Sale',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'canceled_sale',
                  'value' => 1,
                  'checked' => isset($account) ? $account->canceled_sale : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Canceled Lead',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'canceled_lead',
                  'value' => 1,
                  'checked' => isset($account) ? $account->canceled_lead : 1
            ],
        ]
    ])

    @include('partials.fields.checkbox', [
        'label' => 'Adjustment',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'adjustment',
                  'value' => 1,
                  'checked' => isset($account) ? $account->adjustment : 1
            ],
        ]
    ])

    @if(isset($account))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop


