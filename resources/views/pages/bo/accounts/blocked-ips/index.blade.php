@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Blocked Ips',
        'actions' => [
                [
                    'icon' => 'fa fa-trash',
                    'title' => 'Release All Ips',
                    'class' => 'red delete_btn',
                    'target' => route('bo.accounts.{account_id}.blocked-ips.release', $account_id)
                ]
        ]
    ])

    @include('pages.bo.accounts.filters.table-blocked-ips', [
        'account_id' => $account_id,
    ])

    @include('partials.containers.data-table', [
       'data' => [],
       'id' => 'accounts',
       'server_side' => true,
       'url' => $url,
       'attributes' => [
           'data-page-length' => '20',
           'data-route_method' => 'blockedIps',
           'data-filter_container' => 'table-filters'
       ],
       'columns' => ['id', 'block_query', 'status', 'ip', 'count', 'actions']
   ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
