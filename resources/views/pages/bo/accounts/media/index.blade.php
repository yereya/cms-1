@extends('layouts.metronic.main')

@section('page_title')
    <small>Publisher</small> {{ $publisher->publisher_name }} <small>Medias</small>
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Medias',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Media',
                    'target' => route('bo.publishers.{publisher_id}.accounts.create', $publisher->id),
                    'permission' => auth()->user()->can($permission_name .'.add')
                ]
            ]
        ])

        @include('partials.containers.data-table', [
           'data' => $medias,
           'columns' => [
               [
                   'key' => 'id',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'name',
                   'value' => function ($data) use ($publisher) {
                       return "<a href='".route('bo.publishers.{publisher_id}.accounts.{account_id}.placements.index', [$publisher->id, $data->id])."'>" .$data->name . "</a>";
                   }
               ],
               [
                   'key' => 'status',
                   'value' => function ($data) {
                        $status = strtolower($data['status']);
                        $class = 'label-success';

                        if ($status == 'inactive') {
                            $class = 'label-danger';
                        } elseif ($status == 'pending') {
                            $class = 'label-info';
                        }

                        return "<span class='label label-sm $class'>$status</span>";
                   }
               ],
               [
                   'key' => 'actions',
                   'filter_type' => 'null',
                   'value' => function ($data) use ($publisher, $permission_name) {
                       $res = '';

                        if (auth()->user()->can($permission_name .'.placement.edit')) {
                            $res .= "<a href='".route('bo.publishers.{publisher_id}.accounts.edit', [$publisher->id, $data->id])."' class='tooltips' data-original-title='Edit action'><span aria-hidden='true' class='icon-pencil fa-fw'></span></a>";
                        }
                        if (auth()->user()->can($permission_name .'.edit')) {
                            $res .= "<a href='".route('bo.publishers.{publisher_id}.accounts.{account_id}.placements.index', [$publisher->id, $data->id])."' class='tooltips' data-original-title='Show placeholders'><span aria-hidden='true' class='icon-grid fa-fw'></span></a>";
                        }

                       return $res;
                   }
               ]
           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])

    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Pixels',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Pixel',
                    'target' => route('bo.publishers.{publisher_id}.pixels.create', $publisher->id)
                ]
            ]
        ])

    @include('partials.containers.data-table', [
           'data' => $pixels,
           'columns' => [
               [
                   'key' => 'name',
                   'value' => function ($data) {
                       return $data->name;
                   }
               ],
               [
                   'key' => 'url',
                   'value' => function ($data) use ($publisher) {
                        return $data->url;
                   }
               ],
               [
                   'key' => 'actions',
                   'filter_type' => 'null',
                   'value' => function ($data) use ($publisher) {
                       $res = '';

                       $res .= "<a href='".route('bo.publishers.{publisher_id}.pixels.edit', [$publisher->id, $data->id])."' class='tooltips' data-original-title='Edit pixel'><span aria-hidden='true' class='icon-pencil fa-fw'></span></a>";
                       $res .= Form::open(['route' => ['bo.publishers.{publisher_id}.pixels.destroy', $publisher->id, $data->id], 'method' => 'delete', 'class' => 'form-invisible tooltips', 'data-original-title' => 'Delete pixel']);
                       $res .= '<button type="submit"><span aria-hidden="true" class="icon-trash"></span></button>';
                       $res .= Form::close();

                       return $res;
                   }
               ]
           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop
