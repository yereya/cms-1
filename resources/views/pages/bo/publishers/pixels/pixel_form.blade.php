@extends('layouts.metronic.main')

@section('page_title')
    <small>Publisher</small> {{ $publisher->publisher_name }} <small>Pixel</small>
    @if (isset($pixel))
        {{ $pixel->name }}
    @endif
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($pixel) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'route' => isset($pixel) ? ['bo.publishers.{publisher_id}.pixels.update', $publisher->id, $pixel->id] : ['bo.publishers.{publisher_id}.pixels.store', $publisher->id],
            'method' => isset($pixel) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($pixel) ? $pixel->name : null
        ])

    @include('partials.fields.input', [
            'name' => 'url',
            'required' => true,
            'value' => isset($pixel) ? $pixel->url : null
        ])

    @if(isset($pixel))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop


