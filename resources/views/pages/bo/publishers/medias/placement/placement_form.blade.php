@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($placement) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'route' => isset($placement)
                ? ['bo.publishers.{publisher}.medias.{medias}.placements.update', $publisher->id, $media->id, $placement->id]
                : ['bo.publishers.{publisher}.medias.{medias}.placements.store', $publisher->id, $media->id],
            'method' => isset($placement) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'placement_name',
            'required' => true,
            'value' => isset($placement) ? $placement->placement_name : null
        ])

    @include('partials.fields.select', [
        'name' => 'type',
        'label' => 'Type',
        'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
        'value' => isset($placement) ? $placement->type : ''
    ])

    <div class="multi-select-ajax">
        @include('partials.fields.select', [
            'name' => 'Advertiser',
            'class' => 'select_advertiser_placement',
            'list' => \App\Entities\Models\Bo\Advertiser::pluck('name', 'id'),
            'value' => isset($placement) ? [$advertiser_id] : [],
            'attributes' => [
                'data-show_select_class' => 'select_brand_placement',
                'data-url_search' => route('bo.publishers.{publisher}.medias.{medias}.placements.search-brand',
                    [$publisher->id, $media->id])
            ]
        ])

        @include('partials.fields.select', [
            'name' => 'Brand',
            'class' => 'select_brand_placement',
            'list' => isset($placement) ? \App\Entities\Models\Bo\Publishers\Brand::where('advertiser_id', $advertiser_id)->pluck('name', 'id') : [],
            'value' => isset($placement) ? [$brand_id] : [],
            'attributes' => [
                'data-show_select_class' => 'select_campaign_placement',
                'data-url_search' => route('bo.publishers.{publisher}.medias.{medias}.placements.search-campaign',
                    [$publisher->id, $media->id])
            ]
        ])

        @include('partials.fields.select', [
            'name' => 'Campaign',
            'class' => 'select_campaign_placement',
            'list' => isset($placement) ? \App\Entities\Models\Bo\Publishers\Campaign::where('brand_id', $brand_id)->pluck('name', 'id') : [],
            'value' => isset($placement) ? [$campaign_id] : []
        ])
    </div>



    @if(isset($publisher))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop


