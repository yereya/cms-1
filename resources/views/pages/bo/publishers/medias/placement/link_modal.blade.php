@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Links',
])

<div class="row">
    @include('partials.fields.select', [
        'name' => 'params',
        'class' => 'modal-link-select',
        'label' => 'Parameters',
        'list' => ['none' => 'None', 'search' => 'Google Search', 'display' => 'Google Display', 'msn' => 'MSN', 'facebook' => 'Facebook'],
        'value' => 'none'
    ])

    @include('partials.fields.textarea', [
        'name' => 'link',
        'value' => $link,
        'class' => 'modal-link-textarea',
        'attributes' => [
            'onclick' => 'select();'
        ]
    ])
</div>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => [
            'class' => 'permissions-user'
        ]
    ]

])

<script type="application/javascript">
    (function () {
        var suffix = [];
        @foreach($suffix as $key => $suf)
            suffix['{{ $key }}'] = '{!! $suf !!}';
        @endforeach

        $('.modal-link-select').on('change', function() {
            var value = $(this).val();
            var link = suffix[value];

            $('#form_control_link').text(link);
        })
    })();
</script>
