@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($media) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'route' => isset($media) ?
                ['bo.publishers.{publisher}.medias.update', $publisher->id, $media->id] :
                ['bo.publishers.{publisher}.medias.store', $publisher->id],
            'method' => isset($media) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'media_name',
            'required' => true,
            'value' => isset($media) ? $media->media_name : null
        ])

    @include('partials.fields.input', [
            'name' => 'url',
            'value' => isset($media) ? $media->url : null
        ])

    @include('partials.fields.select', [
        'name' => 'type',
        'required' => true,
        'list' => ['mobile_app' => 'Mobile App', 'table_app' => 'Table App', 'web_site' => 'Web Site', 'network' => 'Network', 'adwords' => 'Adwords'],
        'value' => isset($media) ? $media->status : 'network'
    ])

    @include('partials.fields.select', [
        'name' => 'status',
        'required' => true,
        'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
        'value' => isset($media) ? $media->status : 'inactive'
    ])

    @if(isset($publisher))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop


