@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => "Medias",
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Media',
                    'target' => route('bo.publishers.{publisher}.medias.create', $publisher->id)
                ]
            ]
        ])

    @include('partials.containers.data-table', [
       'data' => $medias,
       'columns' => [
           [
               'key' => 'media_name',
               'label' => 'Name',
               'value' => function ($data) {
                   return "<a href='#'>$data->media_name</a>";
               }
           ],
           [
               'key' => 'mongodb_id',
               'label' => 'Mongo Id',
               'value' => function ($data) {
                   return $data->mongodb_id;
               }
           ],
           [
               'key' => 'type',
               'value' => function ($data) {
                   return $data->type;
               }
           ],
           [
               'key' => 'status',
               'value' => function ($data) {
                    $status = strtolower($data['status']);
                    $class = 'label-success';

                    if ($status == 'inactive') {
                        $class = 'label-danger';
                    } elseif ($status == 'pending') {
                        $class = 'label-info';
                    }

                    return "<span class='label label-sm $class'>$status</span>";
               }
           ],
           [
               'key' => 'actions',
               'filter_type' => 'null',
               'value' => function ($data) use ($permission_name, $publisher) {
                   $res = '';

                   if (auth()->user()->can($permission_name .'.edit')) {
                       $res .= "<a href='".route('bo.publishers.{publisher}.medias.edit', [$publisher->id, $data->id])."' class='tooltips' data-original-title='Edit media'><span aria-hidden='true' class='icon-pencil'></span></a>";
                   }

                   if (auth()->user()->can($permission_name .'.placement.view')) {
                       $res .= "<a href='".route('bo.publishers.{publisher}.medias.{medias}.placements.index', [$publisher->id, $data->id])."' class='tooltips' data-original-title='Show placements'><span aria-hidden='true' class='fa fa-sliders fa-fw'></span></a>";
                   }

                   return $res;
               }
           ],
       ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop