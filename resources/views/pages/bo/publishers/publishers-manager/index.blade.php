@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Pubisher manager ',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New',
                    'target' => route('bo.publishers.create'),
                ]
            ]
        ])

        {!! Form::open([
            'route' => ['reports.publishers.publishers-manager.upload'],
            'method' => 'POST',
            'files' => true,
            'class' => 'form-horizontal',
            'role' => 'form',
            'enctype' => 'multipart/form-data',
        ]) !!}


        <div class="row">
            @include('partials.fields.select', [
                'name' => 'action',
                'label' => 'Action:',
                'required' => true,
                'list' => [
                    ['id' => "add", 'text' => "Add"],
                ],
                'value' => 'add'
            ])

            @include('partials.fields.input-upload', [
                'name' => 'file',
                'label' => 'File:',
                'required' => true,
            ])

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'submit' => 'true'
            ]
        ])
        </div>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-info">
                <h4 class="block">Fields</h4>
                <h5 class="block">Add</h5>
                <p>Required fields: sid, account_id, network_net_revenue, stats_date_tz, device, cost, source_report_uid</p>
                <p>Optional fields: campaign_id, campaign_name, keyword_name, <s>keyword_id</s>, match_type, ad_group_id, ad_group_name, clicks, impressions</p>
            </div>
        </div>
    </div>

        <hr>


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop