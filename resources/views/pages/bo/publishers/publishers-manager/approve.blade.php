@extends('layouts.metronic.main')

@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'approval candidates',
    ])

    @if(count($approval_candidates))
        @include('partials.containers.data-table', [
            'data' => $approval_candidates,
            'id' => 'approval_candidates',
            'attributes' => [
                'data-page-length' => '50',
            ],
        ])
    @else
        <p>It seems like no valid advertiser tracks were passed, <a href="{{ route('reports.publishers.publishers-manager.index') }}">try again</a>.</p>
    @endif

    {!! Form::open([
        'route' => ['reports.publishers.publishers-manager.store'],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',
    ]) !!}
        @if(count($approval_candidates))
            @include('partials.fields.h4-separator', ['label' => 'Approve',])
            @include('partials.containers.form-footer-actions', [
                            'buttons' => [
                                'cancel' => [
                                    'route' => back_url()
                                ],
                                'submit' => 'true'
                            ]
                        ])
        @endif
    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop