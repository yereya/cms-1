@extends('layouts.metronic.main')

@section('page_title')
    {{--<small>Publisher</small>--}}
    @if (isset($publisher))
        {{ $publisher->publisher_name }}
    @endif
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($publisher) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'route' => isset($publisher) ? ['bo.publishers.update', $publisher->id] : ['bo.publishers.store'],
            'method' => isset($publisher) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'publisher_name',
            'required' => true,
            'value' => isset($publisher) ? $publisher->publisher_name : null
        ])

    @include('partials.fields.select', [
        'name' => 'status',
        'label' => 'Status',
        'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
        'value' => isset($publisher) ? $publisher->status : ''
    ])

    @include('partials.fields.select', [
        'name' => 'type',
        'label' => 'Type',
        'list' => ['Normal' => 'Normal', 'AdWords' => 'AdWords'],
        'value' => isset($publisher) ? $publisher->type : ''
    ])

    @if(isset($publisher))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop


