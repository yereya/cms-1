@extends('layouts.metronic.main')

@section('head')
    <link href="{{ asset('assets/layout/css/changes_log.css') }}" rel="stylesheet">


@stop
@section('page_content')

    <div id="app2" >
        <template>

            <div class="row mt-3">
                <div class="main_header" style="">
                    <h3 class="text-info text-bold" > Changes Log</h3>
                </div>
            </div>
            <hr class="bg-info">
            <div class="row mt-3" >
                <div class="col-sm-12">
                    <div id="outer" class="outer">



                        <filter-btn :list="dates_list"   ref_name="dates_ref" label="Date" :filter_func="select_filter_param"
                                    selected_list="selected_dates" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="advertisers_list"  ref_name="advertiser_ref" label="Advertiser" :filter_func="select_filter_param"
                                    selected_list="selected_advertisers" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="columns"  ref_name="select_col_ref" label="Select Columns" :filter_func="select_filter_param"
                                    selected_list="selected_columns" :check_selection="check_selection"></filter-btn>

                        <div class=""style="display: inline-block;float: right">
                            <div class="dropdown">
                                <button class="btn btn-info float-right" @click="tableToExcel('table', 'Lorem Table')"><i class="fa fa-download"></i> </button>
                            </div>
                        </div>

                        <div class=""style="display: inline-block;float: right;margin-right: 3px;">
                                <button class="btn btn-info float-right" @click="clear_filters()"><i class="fa fa-refresh"></i> </button>
                        </div>

                        <button class="btn btn-info float-right" style="float:right;margin-right: 3px;" @click="form_status=0;new_row()">&nbsp;Add Log</button>
                    </div>
                    <label class="text-bold" style="font-size: 17px;padding-top: 17px;padding-left: 67px;"> Showing @{{ total_results }} records</label>
                    <div class="search-wrapper search-query" >
                        <input class="form-control" type="text" v-model="searchQuery" placeholder="Search" />
                    </div>
                </div>
            </div>

            <hr class="bg-info">
            <!-- Display Records -->
            <div>
                <div class="row" style="display: block;margin-left: 50px;" >
                    <div class="col-lg-12 table-out">
                        <div class="table-in">

                            <table class="table table-bordered table-striped " ref="table" id="loremTable" >
                                <thead >
                                <tr class="text-center bg-info text-light " >
                                    <th v-for="column in selected_columns" >
                                        <a @click="order_list(column)"> @{{ column  }} </a>
                                    </th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody  style="overflow: auto;">

                                    <tr class="text-center" v-for="change_log in computed_data" :key="change_log.id">
                                        <td v-for="column in selected_columns">
                                            @{{ change_log[column] }}
                                        </td>
                                        <td>
                                            <a href="#" class="text-success" @click="select_row(change_log);"><i class="fa fa-pencil-square-o"></i> </a>
                                            <a href="#" class="text-success" @click="delete_row(change_log);"><i class="fa fa-remove"></i> </a>
                                        </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>

                        <div id="overlay" v-if="showAddModal" >
                            ------------------------------------------------------------------------------------------------------- - Form ---------------------------------------------------------------
                            <div class="modal-dialog overlayed" style="margin-top: -25px">
                                <div class="modal-content" style="max-height: 95vh;overflow: scroll">
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="text-align: center;font-weight: bold;font-size: 19px">@{{ form_status === 0 ? 'Add A New Row' : 'Edit Row'}}</h5>
                                        <button type="button" class="close" @click="showAddModal=false">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body ">
                                        <form  action="#" method="post">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-md-offset-1 col-md-10">
                                                            <form class="form-horizontal" style="padding: 3px">
                                                                <div class="form-content">
                                                                    <div style="margin-top: 50px">
                                                                    </div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-12">
                                                                            <label class="control-label" for="advertiser_name"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="advertiser_name">Advertiser's Name</label>
                                                                            <select  class="form-control select_box" required v-model="selected_item.advertiser_name"   >
                                                                                <option selected value></option>
                                                                                <option v-for="advertiser in advertisers_full_list">@{{ advertiser.name }}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="department"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="department">Department</label>
                                                                            <select  class="form-control select_box" required v-model="selected_item.department"
                                                                            >
                                                                                <option selected value></option>
                                                                                <option v-for="department in departments_list" >@{{ department }}</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="importance"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="importance">Importance</label>
                                                                            <select  class="form-control select_box" required v-model="selected_item.importance">
                                                                                <option selected value></option>
                                                                                <option v-for="num in 10" >@{{ num }}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="event"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="event">Event</label>
                                                                            <input class="form-control" v-model="selected_item.event" id="event" placeholder="" type="text">

                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="event_date"><i class="fa fa-arrow-circle-o-down"></i></label>
                                                                            <label class="control-label" for="event_date">Event Date</label>
                                                                            <input class="form-control" v-model="selected_item.event_date" id="event_date" placeholder="" type="date">
                                                                        </div>
                                                                    </div>


                                                                    <div style="margin-top: 150px">

                                                                    <div class="col-md-6 col-lg-offset-2" style="top:10px;">
                                                                        <button style="border-radius: 10px;font-weight:600;padding: 5px 10px"
                                                                                type="button" class="btn btn-info form-btn" @click="showAddModal=false">Cancel</button>
                                                                        <button style="border-radius: 10px;font-weight:600;padding: 5px 10px"
                                                                                type="button" class="btn btn-info form-btn" @click="onSubmit()">Save</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ------------------------------------------------------------------------------------------------------ / Form   ---------------------------------------------------------------
                        </div>
                    </div>

                </div>
                <!-- / Display Records -->


                <div class="row" style="margin-left: -20px;margin-right: -20px;background: white">
                    <div class="col-md-4 offset-md-4 pages-bar"  v-if="!showAddModal">
                    </div>

                </div>
            </div>
            </div>
        </template>
    </div> <!-- Close App -->
<style>



</style>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
    <script type="text/javascript" >
      Vue.config.productionTip = false;

      Vue.component('filter-btn', {
        data: function () {
          return {
            count: 0,
            show_element: false,
            input_filter: ''
          }
        },
        props: {
          list: {
            required: true
          },
          ref_name: {
            type: String,
            required: true
          },
          label: {
            type: String,
            required: true
          },
          filter_func: {
            type: Function,
            required: true
          },
          selected_list: {
            type: String,
            required: true
          },
          check_selection: {
            type: Function,
            required: true
          }
        },
        computed : {
          filtered: function() {
            let filtered_list = Object.values(this.list).reduce((acc,curr) => {
              if(!curr || curr.toLowerCase().includes(this.input_filter))
                acc[curr] = curr;
              return acc;
            }, {});
            return filtered_list
          }
        },
        methods: {
          set_focus(name){
            // this.$refs.date_ref.focus();
            this.$refs[name].focus();
          },
          mouse_on(){
            this.show_element = true;
          },
          mouse_off(){
            this.show_element = false;
          }
        },
        template: `<div class="inline-block-display">
                      <div class="dropdown">
                          <button class="btn btn-info float-right" @mouseover=" mouse_on();set_focus(ref_name)" >@{{ label }}</button>
                          <div class="dropdown-content" >
                              <div><input type="text" :ref="ref_name" v-model="input_filter" style="color: black"></div>
                              <div v-for="status in filtered" style="text-align: left" @click="filter_func(selected_list, status)"
                                   :class="check_selection(selected_list,status)?'selected':''" v-if="show_element">
                                  @{{ status }}
                              </div>
                          </div>
                      </div>
                  </div>`
      });

      var app = new Vue({
        el: '#app2',
        data: {
          // ------ Download Table -------
          uri :'data:application/vnd.ms-excel;base64,',
          template:'<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="https://www.w3.org/TR/html40/"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
          base64: function(s){ return window.btoa(unescape(encodeURIComponent(s))) },
          format: function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) },

          // ------ / Download Table -------
          sorted_data: {},
          sort_order: 'desc',
          sort_key: '',
          showAddModal: false,
          data: [],
          selected_index: '',
          group_values: '',

          // cms_url: "http://cms.loc/bo/changes_log",
          cms_url: "http://cms.trafficpointltd.com/bo/changes_log",

          columns: [],
          form_status: 0,
          parased_change_logs: [],
          months: ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
          searchQuery: '',
          total_results: 0,
          advertisers_full_list: {!! json_encode($advertisers_full_list) !!},
          changes_log_data: {!! json_encode($changes_log_data) !!},
          advertisers_full_list: {!! json_encode($advertisers_full_list) !!},
          advertisers_list: {!! json_encode($advertisers) !!},
          dates_list: {!! json_encode($dates_list) !!},
          lp_by_brand_group_list: [],

          // Filters
          selected_columns : [],
          selected_advertisers: [],
          selected_dates: [],

          // Multiple options select
          departments_list: ['PPC', 'BizDev', 'Product', 'Analysis', 'Finance', 'R&D', 'Management'],
          update_response: [],
          devices_list: ['c','m','t'],
          excluded_columns: ['id','created_at','updated_at'],
          selected_item: {advertiser_name: '',advertiser_id: '',department: '', event: '', event_date: '', importance: ''}
        },
        computed: {
          computed_data: function() {
            let rows =  Object.keys(this.data).reduce((acc,curr_id) => {

              let row_date = new Date(this.data[curr_id].event_date);
              let row_date_month = this.months[row_date.getMonth()];
              let row_date_year = row_date.getFullYear();
              if(
                  (this.selected_advertisers.length == 0     || this.selected_advertisers.includes(this.data[curr_id].advertiser_name)) &&
                  (this.selected_dates.length == 0           || this.selected_dates.includes(row_date_month + '-' + row_date_year))
                  &&
                  (
                  this.searchQuery.length == 0 ||
                      (this.data[curr_id].advertiser_name && this.data[curr_id].advertiser_name.toLowerCase().includes(this.searchQuery))
                  )
              ){
                // Insert current user into reduce's accumulative parameter
                acc[curr_id] = this.data[curr_id];
              }
              // Return accumulative parameter
              return acc;
            }, {});

            // Add Total line to the table
            this.total_results = Object.keys(rows).length  || 0 ;

            // Sort
            if(this.sort_key !== ''){

              this.sorted_data = Object.entries(rows).sort(function(x,y) {

                // Compare values
                let res =  x[1][app.sort_key].toString().localeCompare(y[1][app.sort_key].toString(), undefined, {numeric: true, sensitivity: 'base'});

                // Handle desc sort order
                if(app.sort_order === 'desc')
                  res = res * (-1);

                return res;
              });

              rows =  this.sorted_data.reduce((acc,curr) => {
                acc.push(curr[1]);

                if(acc)
                    return acc;
              },[]);
            }else{
            }

            return rows;
        },
        },
        mounted: function(){
          this.$nextTick(function () {
            this.getAllUsers();
          })
        },
        methods: {
          order_list(col_name){
            this.sort_key = col_name;
            this.sort_order = this.sort_order === 'desc' ? 'asc' : 'desc';

          },
          clear_filters(){
            app.selected_advertisers = [];
            app.selected_dates = [];
            app.searchQuery = '';
            app.sort_key = '';
          },
          tableToExcel(table, name){
            if (!table.nodeType) table = this.$refs.table;
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
            window.location.href = this.uri + this.base64(this.format(this.template, ctx));
          },
          check_selection(selected_list, item){
            return this[selected_list].includes(item) ;
          },
          onSubmit(){
            if(!app.validate_form())
               return false;
            app.showAddModal = false;

            let formData = this.toFormData(app.selected_item);
            if(app.form_status !== 0){  // 0 => New Row, 1 => Edit
              axios.post(app.cms_url + "/update",formData)
                  .then(function(response){
                    app.update_response = response;
                    if(app.update_response.data.error)
                      alert(app.update_response.data.error);
                    else
                      app.updateUsers();
                    app.updateUsers();
                  });
            }else{
              axios.post(app.cms_url + "/store",formData)
                  .then(function(response){
                    // console.log('Responce on Submit: ' + response);
                    app.update_response = response;
                    if(app.update_response.data.error)
                      alert(app.update_response.data.error);
                    else
                      app.updateUsers();
                  });
            }
          },
          validate_form(){
            let msg = '';
            if(!app.selected_item.advertiser_name ){
              msg += 'Fill in Advertiser Name\n';
            }
            if(!app.selected_item.department ){
              msg += 'Fill in Department\n';
            }
            if(!app.selected_item.importance){
              msg += 'Fill in Importance\n';
            }
            if(!app.selected_item.event){
              msg += 'Fill in Event\n';
            }
            if(!app.selected_item.event_date){
              msg += 'Fill in Event Date\n';
            }

            if(msg)
            {
              alert(msg);
              return false;
            }
            return true;
          },
          UpdateByIndex(){
            app.data = app.parased_change_logs[app.group_values[app.selected_index]];
              },

          select_filter_param(type, data){
            let index = app[type].indexOf(data);

            if(index >= 0)
              delete app[type][index];
            else
              app[type].push(data);

            app[type] = app[type].filter(function (el) {
              return el != null;
            });
            app[type] = app[type].map(x => { if(x!= null) return x; });

            if(type === 'selected_columns' && index < 0){
              // Order selected columns based on the original order
                if(this.selected_columns.length !== 0){
                  let new_ordered_columns = [];
                  app.columns.forEach(col => {
                    if(app.selected_columns.indexOf( col ) >= 0){
                      new_ordered_columns.push(col);
                    }
                  });
                  app.selected_columns = new_ordered_columns;
                }
            }
          },
          delete_row(row){
            app.selected_advertisers = [];
            for (let key in row) {
              app.selected_item[key] = row[key];
            }
            if(confirm("You are going to delete \"" + app.selected_item.advertiser_name + "\" of department  \"" + app.selected_item.department +
                "\"  for event date : " + app.selected_item.event_date + "\n\n\nAre you sure?")){
              let formData = this.toFormData(app.selected_item);
                axios.post(app.cms_url + "/delete",formData)
                    .then(function(response){
                      app.update_response = response;
                      if(app.update_response.data.error){
                        alert(app.update_response.data.error);
                      }
                      app.updateUsers();
                    });
            }
          },
          select_row(row) {
            app.form_status = 1; // 1 => Edit, 0 => New Row
            app.showAddModal = true;
            for (let key in row) {
              app.selected_item[key] = row[key];
            }

          },
          new_row() {
            app.showAddModal = true;
            app.selected_item = {advertiser_name: '',advertiser_id: '',department: '', event: '', event_date: '', importance: ''};

            // Multiple options select
            app.selected_advertisers = [];
          },
          updateUsers(){
            axios.get(app.cms_url +"/getData")
                .then(function(response){
                  app.update_response = response;

                  if(app.update_response.data.error){
                    alert(app.update_response.data.error);
                    return;
                  }

                  app.changes_log_data = response.data;

                  try{
                    app.parased_change_logs = app.changes_log_data;
                    app.group_values = Object.keys(app.parased_change_logs);
                    app.data = app.parased_change_logs; //.slice(2, 20);
                  }catch(err){
                    console.log(err);
                  }
                });
          },
          getAllUsers() {
                  try{
                        app.parased_change_logs = JSON.parse(app.changes_log_data);
                        app.group_values = Object.keys(app.parased_change_logs);
                        app.selected_index = app.group_values.length - 1;
                        app.data = app.parased_change_logs;
                        app.columns = Object.keys(Object.values(app.data)[0]);
                        app.selected_columns = app.columns.filter( function( el ) {
                          return app.excluded_columns.indexOf( el ) < 0;
                        } );
                  }catch(err){
                        console.log(err);
                  }
          },
          toFormData(obj){
            let fd = new FormData();
            for (const i in obj){
              fd.append(i,obj[i]);
            }
            return fd;
          },
        }
      });

    </script>

@stop

