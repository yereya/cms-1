@extends('layouts.metronic.main')

@section('head')
    <link href="{{ asset('assets/layout/css/discrepancy.css') }}" rel="stylesheet">


@stop
@section('page_content')

    <div id="app2" >
        <template>

            <div class="row mt-3">
                <div class="col-lg-6 col-lg-offset-4">
                    <h3 class="text-info text-bold" > Discrepancy Manager</h3>
                </div>
            </div>
            <hr class="bg-info">
            <div class="row mt-3" >
                <div class="col-sm-12">
                    <div id="outer" class="outer">

                        <filter-btn :list="status_list"  ref_name="status_ref" label="Status" :filter_func="select_filter_param"
                                    selected_list="selected_status" :check_selection="check_selection" ></filter-btn>

                        <filter-btn :list="owner_list"   ref_name="owner_ref" label="Owner" :filter_func="select_filter_param"
                                    selected_list="selected_owners" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="reviewer_list"  ref_name="reviewer_ref" label="Reviewer" :filter_func="select_filter_param"
                                    selected_list="selected_reviewers" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="advertisers_list"  ref_name="advertiser_ref" label="Advertiser" :filter_func="select_filter_param"
                                    selected_list="selected_advertisers" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="group_name_list"  ref_name="group_name_ref" label="Group Name" :filter_func="select_filter_param"
                                    selected_list="selected_group_names" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="business_group_list"  ref_name="business_group_ref" label="Business Group" :filter_func="select_filter_param"
                                    selected_list="selected_business_groups" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="invoice_group_list"  ref_name="invoice_group_ref" label="Invoice Group" :filter_func="select_filter_param"
                                    selected_list="selected_invoice_groups" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="period_list"  ref_name="date_ref" label="Dates" :filter_func="select_filter_param"
                                    selected_list="selected_periods" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="columns"  ref_name="select_col_ref" label="Select Columns" :filter_func="select_filter_param"
                                    selected_list="selected_columns" :check_selection="check_selection"></filter-btn>


                        <div class=""style="display: inline-block;float: right">
                            <div class="dropdown">
                                <button class="btn btn-info float-right" @click="tableToExcel('table', 'Lorem Table')"><i class="fa fa-download"></i> </button>
                            </div>
                        </div>

                        <div class=""style="display: inline-block;float: right;margin-right: 3px;">
                                <button class="btn btn-info float-right" @click="clear_filters()"><i class="fa fa-refresh"></i> </button>
                        </div>

                        <button class="btn btn-info float-right" style="float:right;margin-right: 3px;" @click="form_status=0;new_discrepancy()">&nbsp;Add Discrepancy</button>
                    </div>
                    <label class="text-bold" style="font-size: 17px;padding-top: 17px;padding-left: 67px;"> Showing @{{ total_results }} records</label>
                    <div class="search-wrapper search-query" >
                        <input class="form-control" type="text" v-model="searchQuery" placeholder="Search" />
                    </div>
                </div>
            </div>

            <hr class="bg-info">
            <!-- Display Records -->
            <div>
                <div class="row" style="display: block;margin-left: 50px;" >
                    <div class="col-lg-12 table-out">
                        <div class="table-in">

                            <table class="table table-bordered table-striped " ref="table" id="loremTable" >
                                <thead >
                                <tr class="text-center bg-info text-light " >
                                    <th v-for="column in selected_columns" >
                                        <a @click="order_list(column)"> @{{ column  }} </a>
                                    </th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody  style="overflow: auto;">
                                <tr class="text-center" v-for="disc in computed_users " :key="disc.id">
                                         <td v-for="column in selected_columns">
                                             @{{ disc[column] }}
                                        </td>
                                    <td>
                                        <a href="#" class="text-success" @click="select_discrepancy(disc);"><i class="fa fa-pencil-square-o"></i> </a>
                                        <a href="#" class="text-success" @click="approve_discrepancy(disc);"><i class="fa fa-check"></i> </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                {{--        <!-- Add New User Modal --!>--}}
                        <div id="overlay" v-if="showAddModal" >
    {{--                    <div id="overlay" v-show="showAddModal" >--}}
                            {{--------------------------------------------------------------------------------------------------------- - Form -----------------------------------------------------------------}}
                            <div class="modal-dialog overlayed" style="margin-top: -25px">
                                <div class="modal-content" style="max-height: 95vh;overflow: scroll">
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="text-align: center;font-weight: bold;font-size: 16px">@{{ form_status === 0 ? 'Add A New Discrepancy' : 'Edit Discrepancy'}}</h5>
                                        <button type="button" class="close" @click="showAddModal=false">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body ">
                                        <form  action="#" method="post">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-md-offset-1 col-md-10">
                                                            <form class="form-horizontal" style="padding: 3px">
                                                                <div class="form-content">
                                                                    <h4 class="heading" style="text-decoration: underline">General Information</h4>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-12">
                                                                            <label class="control-label" for="brand_group_name"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="brand_group_name">Brand's Group Name</label>
                                                                            <select  class="form-control select_box" required
                                                                                    v-model="selected_item.brand_group_name" :disabled="form_status != 0" >
                                                                                <option v-for="group_name in group_name_list" >@{{ group_name }}</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="company_name"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="company_name">Company's Name</label>
                                                                            <input class="form-control" id="company_name" v-model="selected_item.company_name" placeholder="" type="text" disabled>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="month"><i class="fa fa-calendar"></i></label>
                                                                            <label class="control-label" for="month">Month</label>
                                                                            <input class="form-control" type="month" id="month" v-model="selected_item.date" :disabled="form_status > 0" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="currency"><i class="fa fa-usd"></i></label>
                                                                            <label class="control-label" for="currency">Currency</label>
                                                                            <select class="form-control select_box"
                                                                                    v-model="selected_item.currency" :disabled="form_status == 2" required>
                                                                                <option>USD</option>
                                                                                <option>EUR</option>
                                                                                <option>GBP</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="currency_rate"><i class="fa fa-usd"></i></label>
                                                                            <label class="control-label" for="currency_rate">Currency Rate</label>
                                                                            <input class="form-control" v-model="selected_item.currency_rate" id="currency_rate"
                                                                                   placeholder="" type="text" :disabled="form_status == 2 " required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>
                                                                    <h4 class="heading" style="text-decoration: underline;">Commissions</h4>

                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="base_commission_amount"><i class="fa fa-money"></i></label>
                                                                            <label class="control-label" for="base_commission_amount">Brand's Commissions</label>
                                                                            <input class="form-control" v-model="selected_item.base_commission_amount" id="base_commission_amount"
                                                                                   placeholder="" type="number"
                                                                                   :disabled="form_status == 2 ">
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="cms_commission_amount"><i class="fa fa-money"></i></label>
                                                                            <label class="control-label" for="cms_commission_amount" >CMS Commissions</label>
                                                                            <input class="form-control" v-model="selected_item.cms_commission_amount" id="cms_commission_amount" value="213456" type="number" disabled>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-12">
                                                                            <label class="control-label" for="exampleInputName2">Difference : @{{selected_item.base_commission_amount - selected_item.cms_commission_amount}}</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>


                                                                    <h4 class="heading" style="text-decoration: underline;" v-if="finance_permission">Finance</h4>
                                                                    <div class="form-group nomargin" v-if="finance_permission">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="base_commission_amount"><i class="fa fa-money"></i></label>
                                                                            <label class="control-label" for="base_commission_amount">Payment</label>
                                                                            <input class="form-control" v-model="selected_item.payment" id="payment" placeholder="" type="number" step="0.01">
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="day_of_payment"><i class="fa fa-calendar"></i></label>
                                                                            <label class="control-label" for="day_of_payment" >Day of Payment</label>
                                                                            <input class="form-control" v-model="selected_item.day_of_payment" id="day_of_payment" value="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin"  v-if="finance_permission"><div class="col-sm-12"><hr class="rounded"></div></div>

                                                                    <h4 class="heading" style="text-decoration: underline;">Comments</h4>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-12">
                                                                            <label class="control-label" for="owner_comment"><i class="fa fa-comment"></i></label>
                                                                            <label class="control-label" for="owner_comment">Owner's Comment</label>
                                                                            <textarea style="height: 40px;" class="form-control" v-model="selected_item.owner_comment" id="owner_comment"
                                                                                      placeholder="" :disabled="form_status == 2"></textarea>
                                                                        </div>
                                                                        <div class="col-sm-12">

                                                                        </div>
                                                                        <div class="col-sm-12" style="padding-bottom: 10px;">
                                                                            <label class="control-label" for="reviewer_comment"><i class="fa fa-comment"></i></label>
                                                                            <label class="control-label" for="reviewer_comment">Reviewer's Comment</label>
                                                                            <textarea style="height: 40px;" class="form-control" v-model="selected_item.reviewer_comment" id="reviewer_comment"
                                                                                      placeholder="" :disabled="form_status != 2"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>
                                                                    <h4 class="heading" id="status" style="text-decoration: underline;">Status</h4>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-5">
                                                                            <select class="form-control select_box"
                                                                                    v-model="selected_item.status" required>
                                                                                <option v-for="status in status_list">@{{ status }}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-offset-3" style="top:10px;">
                                                                        <button style="border-radius: 10px;font-weight:600;padding: 5px 10px"
                                                                                type="button" class="btn btn-info form-btn" @click="showAddModal=false">Cancel</button>
                                                                        <button style="border-radius: 10px;font-weight:600;padding: 5px 10px"
                                                                                type="button" class="btn btn-info form-btn" @click="onSubmit()">Save</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        {{-------------------------------------------------------------------------------------------------------- / Form   ---------------------------------------------------------------          --}}
                        </div>
                    </div>

                </div>
                <!-- / Display Records -->


                <div class="row" style="margin-left: -20px;margin-right: -20px;background: white">
                    <div class="col-md-4 offset-md-4 pages-bar"  v-if="!showAddModal">
                        <div class="pagination " v-if="pagination">
                            <a href="#" @click="selected_index - 1 >= 0 ? selected_index -= 1 : '';UpdateByIndex()">&laquo;</a>
                            <a v-if="selected_index - 1 >= 0" @click="selected_index -= 1;UpdateByIndex()">@{{group_values[selected_index - 1]}}</a>
                            <a :class="pagination ? 'active' : ''" href="#" >@{{group_values[selected_index]}}</a>
                            <a v-if="selected_index + 1 <= group_values.length - 1" @click="selected_index += 1;UpdateByIndex()">@{{group_values[selected_index + 1]}}</a>
                            <a href="#" @click="selected_index + 1 <= group_values.length - 1 ? selected_index += 1 : '';UpdateByIndex()">&raquo;</a>
                        </div>

                    </div>

                </div>
            </div>
        </template>
    </div> <!-- Close App -->
<style>



</style>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
    <script type="text/javascript" >
      Vue.config.productionTip = false;
      Vue.component('data-table', {
        data: function () {
          return {
            // show_data: false,
          }
        },
        props: {
          ref_name: {
            type: String,
            required: true
          },
          table_id: {
            type: String,
            required: true
          },
          selected_columns: {
            type: Array,
            required: true
          },
          computed_users: {
            type: Object,
            required: true
          },
          approve_discrepancy: {
            type: Function,
            required: true
          },
          select_discrepancy: {
            type: Function,
            required: true
          }
        },
        methods: {
        },
        template: `<table class="table table-bordered table-striped " ref="table" id="loremTable" >
                            <thead >
                            <tr class="text-center bg-info text-light " >
                                <th v-for="column in selected_columns">
                                        @{{ column  }}
                                </th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody  style="overflow: auto;" >
                            <tr class="text-center" v-for="disc in computed_users" :key="disc.id">
                                     <td v-for="column in selected_columns">
                                         @{{ disc[column] }}
                                    </td>
                                <td>
                                    <a href="#" class="text-success" @click="select_discrepancy(disc);"><i class="fa fa-pencil-square-o"></i> </a>
                                    <a href="#" class="text-success" @click="approve_discrepancy(disc);"><i class="fa fa-check"></i> </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>`
      });



      Vue.component('filter-btn', {
        data: function () {
          return {
            count: 0,
            show_element: false,
            input_filter: ''
          }
        },
        props: {
          list: {
            required: true
          },
          ref_name: {
            type: String,
            required: true
          },
          label: {
            type: String,
            required: true
          },
          filter_func: {
            type: Function,
            required: true
          },
          selected_list: {
            type: String,
            required: true
          },
          check_selection: {
            type: Function,
            required: true
          }
        },
        computed : {
          filtered: function() {
            let filtered_list = Object.values(this.list).reduce((acc,curr) => {
              if(!curr || curr.toLowerCase().includes(this.input_filter))
                acc[curr] = curr;
              return acc;
            }, {});
            return filtered_list
          }
        },
        methods: {
          set_focus(name){
            // this.$refs.date_ref.focus();
            this.$refs[name].focus();
          },
          mouse_on(){
            this.show_element = true;
          },
          mouse_off(){
            this.show_element = false;
          }
        },
        template: `<div class="inline-block-display">
                      <div class="dropdown">
<!--                          <button class="btn btn-info float-right" @mouseover=" mouse_on();set_focus(ref_name)" >@{{ label }}</button>-->
                          <button class="btn btn-info float-right" @mouseover=" mouse_on();set_focus(ref_name)" >@{{ label }}</button>
                          <div class="dropdown-content" >
                              <div><input type="text" :ref="ref_name" v-model="input_filter" style="color: black"></div>
                              <div v-for="status in filtered" style="text-align: left" @click="filter_func(selected_list, status)"
                                   :class="check_selection(selected_list,status)?'selected':''" v-if="show_element">
                                  @{{ status }}
                              </div>
                          </div>
                      </div>
                  </div>`
      });

      var app = new Vue({
        el: '#app2',
        data: {
          // ------ Download Table -------
          uri :'data:application/vnd.ms-excel;base64,',
          template:'<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="https://www.w3.org/TR/html40/"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
          base64: function(s){ return window.btoa(unescape(encodeURIComponent(s))) },
          format: function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) },

          // ------ / Download Table -------
          // cms_url: 'http://cms.loc', // Local
          cms_url: 'http://cms.trafficpointltd.com', // Production
          sorted_data: {},
          sort_order: 'desc',
          sort_key: '',
          pagination: true,
          showAddModal: false,
          users: [],
          selected_index: '',
          group_values: '',
          selected_columns : [],
          columns: [],
          form_status: 0,
          parased_disc: [],
          searchQuery: '',
          total_results: 0,

          edit_permissions: {!! json_encode($edit_permissions) !!},
          approve_permissions: {!! json_encode($approve_permissions) !!},
          status_list: {!! json_encode($status_list) !!},
          owner_list: {!! json_encode($owners) !!},
          reviewer_list: {!! json_encode($reviewers) !!},
          period_list: {!! json_encode($dates) !!},
          business_group_list: {!! json_encode($business_groups) !!},
          group_name_list: {!! json_encode($group_names) !!},
          invoice_group_list: {!! json_encode($invoice_group) !!},
          advertisers_list: {!! json_encode($advertisers) !!},
          discrepancies: {!! json_encode($discrepancies) !!} ,
          finance_permission: {!! json_encode($finance_permission) !!} ,

          selected_owners: [],
          selected_status: [],
          selected_reviewers: [],
          selected_group_names: [],
          selected_periods: [],
          selected_business_groups: [],
          selected_invoice_groups: [],
          selected_advertisers: [],
          numeric_columns: ['cms_commission_amount','base_commission_amount','base_cms_diff', 'payment_cms_diff'],
          update_response: [],
          excluded_columns: ['id','currency_rate','brand_group_id','company_id','tracks_created', 'tracks_updated','click_out','business_group','owner_comment',
            'reviewer_comment','deal_explanation','advertiser_id','invoice_group','day_of_payment'],
            // ,'advertiser_id','currency','click_out','click_out','brand_group_name','company_id','company_name'],
          selected_item: {brand_group_id: '', brand_group_name: '',company_name: '', date: '',currency:'', currency_rate: 1,
            base_commission_amount:'',cms_commission_amount:'',owner_comment:'',reviewer_comment:'',status:'',
            user_role: '', id: '', payment: '', day_of_payment: ''}
        },
        watch : {
            selected_periods: function() {
              if(this.selected_periods.length === 0){ // If all dates unchecked, set back pagination
                this.pagination = true;
                app.users = app.parased_disc[app.group_values[app.selected_index]];
              }else{
                this.pagination = false;
                app.users = {};
                Object.values(this.selected_periods).forEach(function(item) {
                  Object.assign(app.users, app.parased_disc[item]);
                });
              }
            }
        },
        computed: {
          computed_users: function() {
            let total_base_commissions = 0;
            let total_cms_commissions = 0;
            let total_payment = 0;
            let rows =  Object.keys(this.users).reduce((acc,curr_id) => {

              if(
                  (this.selected_status.length == 0          || this.selected_status.includes(this.users[curr_id].status))&&
                  (this.selected_reviewers.length == 0       || this.selected_reviewers.includes(this.users[curr_id].reviewer) ||
                      this.selected_reviewers.includes('none') && this.users[curr_id].reviewer == null )&&
                  (this.selected_group_names.length == 0     || this.selected_group_names.includes(this.users[curr_id].brand_group_name))&&
                  // (this.selected_periods.length == 0         || this.selected_periods.includes(this.users[curr_id].date))&&
                  (this.selected_owners.length == 0          || this.selected_owners.includes(this.users[curr_id].owner) ||
                      this.selected_owners.includes('none') && this.users[curr_id].owner == null )&&
                  (this.selected_business_groups.length == 0 || this.selected_business_groups.includes(this.users[curr_id].business_group))&&
                  (this.selected_invoice_groups.length == 0  || this.selected_invoice_groups.includes(this.users[curr_id].invoice_group))&&
                  (this.selected_advertisers.length == 0     || this.selected_advertisers.includes(this.users[curr_id].advertiser_name))
                  &&
                  (
                  this.searchQuery.length == 0 ||
                      (this.users[curr_id].status && this.users[curr_id].status.toLowerCase().includes(this.searchQuery))||
                      (this.users[curr_id].reviewer && this.users[curr_id].reviewer.toLowerCase().includes(this.searchQuery))||
                      (this.users[curr_id].brand_group_name && this.users[curr_id].brand_group_name.toLowerCase().includes(this.searchQuery))||
                      // (this.users[curr_id].date && this.users[ curr_id].date.toLowerCase().includes(this.searchQuery))||
                      (this.users[curr_id].owner && this.users[curr_id].owner.toLowerCase().includes(this.searchQuery))||
                      (this.users[curr_id].business_group && this.users[curr_id].business_group.toLowerCase().includes(this.searchQuery))||
                      (this.users[curr_id].invoice_group && this.users[curr_id].invoice_group.toLowerCase().includes(this.searchQuery))||
                      (this.users[curr_id].company_name && this.users[curr_id].company_name.toLowerCase().includes(this.searchQuery))||
                      (this.users[curr_id].advertiser_name && this.users[curr_id].advertiser_name.toLowerCase().includes(this.searchQuery))
                  )
              ){
                acc[curr_id] = this.users[curr_id];
                // Calculate Total
                total_base_commissions += this.users[curr_id].base_commission_amount;
                total_cms_commissions += this.users[curr_id].cms_commission_amount;
                total_payment += this.users[curr_id].payment;
              }
              return acc;
            }, {});
            // Add Total line to the table
            this.total_results = Object.keys(rows).length  || 0 ;

            // Add Total line
            let total_row = {};
            total_row.base_commission_amount = total_base_commissions;
            total_row.cms_commission_amount = total_cms_commissions;
            total_row.base_cms_diff = total_base_commissions - total_cms_commissions;
            total_row.payment_cms_diff = total_payment - total_cms_commissions;
            total_row.payment = total_payment;
            total_row.brand_group_name = 'Total';


            if(this.sort_key !== ''){

              // ---------------------- Working ----------------------

              this.sorted_data = Object.entries(rows).sort(function(x,y) {

                // Handle empty cells
                if(!x[1][app.sort_key])
                  x[1][app.sort_key] = Object.values(app.numeric_columns).includes(app.sort_key) ? 0 : '';
                if(!y[1][app.sort_key])
                  y[1][app.sort_key] = Object.values(app.numeric_columns).includes(app.sort_key) ? 0 : '';

                // Compare values
                let res =  x[1][app.sort_key].toString().localeCompare(y[1][app.sort_key].toString(), undefined, {numeric: true, sensitivity: 'base'});

                // Handle desc sort order
                if(app.sort_order === 'desc')
                  res = res * (-1);
                // Handle negative values for base cms diff column
                if((app.sort_key === 'base_cms_diff' || app.sort_key === 'payment_cms_diff') &&   x[1][app.sort_key] < 0 && y[1][app.sort_key] < 0)
                  res = res * (-1);
                return res;
              });

              rows =  this.sorted_data.reduce((acc,curr) => {
                acc.push(curr[1]);

                if(acc)
                    return acc;
              },[]);
              rows.push(total_row);
            }else{
              Object.assign(rows,{total:total_row});
            }

            return rows;
        }
      },
        mounted: function(){
          this.$nextTick(function () {
            this.getAllUsers();
          })
        },
        methods: {
          order_list(col_name){
            this.sort_key = col_name;
            this.sort_order = this.sort_order === 'desc' ? 'asc' : 'desc';

          },

          clear_filters(){
            app.selected_owners= [];
            app.selected_status= [];
            app.selected_reviewers= [];
            app.selected_group_names= [];
            app.selected_periods= [];
            app.selected_business_groups= [];
            app.selected_invoice_groups= [];
            app.selected_advertisers= [];
            app.searchQuery = '';
            app.pagination = true;
            app.users = app.parased_disc[app.group_values[app.selected_index]];
            app.sort_key = '';
            this.pagination = true;
          },
          tableToExcel(table, name){
            if (!table.nodeType) table = this.$refs.table;
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
            window.location.href = this.uri + this.base64(this.format(this.template, ctx));
          },
          check_selection(selected_list, item){
            return this[selected_list].includes(item) ;
          },
          onSubmit(){
            if(!app.validate_form())
               return false;
            app.showAddModal = false;
            let formData = this.toFormData(app.selected_item);
            if(app.form_status !== 0){
              axios.post(app.cms_url + "/bo/discrepancies/update",formData)
                  .then(function(response){
                    app.update_response = response;
                    //console.log('Responce on Submit: ' + response);
                    app.updateUsers();
                  });
            }else{
              axios.post(app.cms_url + "/bo/discrepancies/store",formData)
                  .then(function(response){
                    console.log('Responce on Submit: ' + response);
                    app.update_response = response;
                    if(app.update_response.data.error)
                      alert(app.update_response.data.error);
                    else
                      app.updateUsers();
                  });
            }

          },
          validate_form(){
            let msg = '';
            if(!app.selected_item.brand_group_name )
              msg += 'Fill in Brands Group Name\n';
            if(!app.selected_item.date )
              msg += 'Fill in Month\n';
            if(!app.selected_item.currency )
              msg += 'Fill in Currency\n';
            if(!app.selected_item.status )
              msg += 'Fill in Status\n';
            if(msg)
            {
              alert(msg);
              return false;
            }
            return true;
          },
          UpdateByIndex(){
            app.users = app.parased_disc[app.group_values[app.selected_index]];
          },
          select_filter_param(type, data){
            let index = app[type].indexOf(data);

            if(index >= 0)
              delete app[type][index];
            else
              app[type].push(data);

            app[type] = app[type].filter(function (el) {
              return el != null;
            });
            app[type] = app[type].map(x => { if(x!= null) return x; });

            if(type === 'selected_columns' && index < 0){
              // Order selected columns based on the original order
              if(this.selected_columns.length !== 0){
                let new_ordered_columns = [];
                app.columns.forEach(col => {
                  if(app.selected_columns.indexOf( col ) >= 0){
                    new_ordered_columns.push(col);
                  }
                });
                app.selected_columns = new_ordered_columns;
              }
            }

          },
          select_discrepancy(discrepancy) {
            if(!app.edit_permissions){
              alert("You don't have permissions to edit");
              return;
            }
            app.form_status = 1; // 1 => Edit mode for owner, 0 => New Discrepancy
            app.showAddModal = true;
            for (let key in discrepancy) {
              app.selected_item[key] = discrepancy[key];
            }
            app.selected_item.user_role = 'owner';
          },
          approve_discrepancy(discrepancy){
            if(!app.approve_permissions){
              alert("You don't have permissions to approve");
              return;
            }
            app.form_status = 2; // 2 => Edit mode for reviewer, 1 => Edit mode for owner, 0 => New Discrepancy
            for (let key in discrepancy) {
              app.selected_item[key] = discrepancy[key];
            }
            app.selected_item.user_role = 'reviewer';
            app.showAddModal = true;
          },
          new_discrepancy() {
            app.showAddModal = true;
            app.selected_item = {brand_group_name: '',company_name: '', date: '',currency:'', currency_rate: 1,
              base_commission_amount:'',cms_commission_amount:'',owner_comment:'',reviewer_comment:'',status:'', id:''};
            app.selected_item.user_role = 'owner';
          },
          updateUsers(){
            axios.get(app.cms_url + "/bo/discrepancies/getData")
                .then(function(response){

                  app.discrepancies = response.data;

                  try{
                    app.parased_disc = app.discrepancies;

                    app.group_values = Object.keys(app.parased_disc);
                    app.users = app.parased_disc[app.group_values[app.selected_index]]; //.slice(2, 20);
                  }catch(err){
                    console.log(err);
                  }
                });
          },
          getAllUsers() {

                  try{
                        app.parased_disc = JSON.parse(app.discrepancies);

                        app.group_values = Object.keys(app.parased_disc);
                        app.selected_index = app.group_values.length - 1;
                        app.users = app.parased_disc[app.group_values[app.selected_index]]; //.slice(2, 20);
                        app.columns = Object.keys(Object.values(app.users)[0]);
                        app.selected_columns = app.columns.filter( function( el ) {
                          return app.excluded_columns.indexOf( el ) < 0;
                        } );


                  }catch(err){
                        console.log(err);
                  }

          },
          toFormData(obj){
            let fd = new FormData();
            for (const i in obj){
              fd.append(i,obj[i]);
            }
            return fd;
          },
        }
      });

    </script>

@stop

