<div class="row table-filters">
        {!! Form::open([
            'route' => ['bo.discrepancies.datatable'],
            'method' => 'POST',
            'class' => 'form-horizontal',
            'role' => 'form',
        ]) !!}


    <div class="form-inline">
        <div class="row">
        @include('partials.fields.select', [
            'name' => 'Status',
            'label' => 'Status',
            'column' => 3,
             'id' => 'status',
            'list' => $status_list,
            'value' => [],
        ])

        @include('partials.fields.select', [
            'name' => 'owner',
            'label' => 'Owner',
            'column' => 3,
            'id' => 'owner',
            'list' => $owners,
            'value' => [],
        ])
        @include('partials.fields.select', [
            'name' => 'reviewer',
            'label' => 'Reviewer',
            'column' => 3,
            'id' => 'system_id',
            'list' => $reviewers,
            'value' => [],
        ])
        @include('partials.fields.select', [
            'name' => 'advertiser_name',
            'label' => 'Advertiser',
            'column' => 3,
            'id' => 'advertiser_name',
            'list' => $advertisers,
            'value' => [],
        ])
        </div>
    </div>

    <div class="form-inline">
        <div class="row">
        @include('partials.fields.select', [
            'name' => 'brand_group_name',
            'label' => 'Group Name',
            'id' => 'brand_group_name',
            'column' => 3,
            'list' => $group_names,
            'value' => [],
        ])

        @include('partials.fields.select', [
            'name' => 'date',
            'label' => 'Period',
            'multi_select' => true,
            'column' => 3,
            'list' => $dates,
            'value' => [],
        ])

        @include('partials.fields.select', [
          'name' => 'business_group',
          'label' => 'Business Group',
          'multi_select' => true,
          'column' => 3,
          'list' => $business_groups,
          'value' => [],
      ])

        @include('partials.fields.select', [
         'name' => 'invoice_group',
         'label' => 'Invoice group',
         'multi_select' => true,
         'column' => 3,
         'list' => $invoice_group,
         'value' => [],
      ])
        </div>

        <div class="form-group form-md-line-input col-md-11">
            @include('partials.fields.button', [
                'value' => 'Filter',
                'right' => true,
            ])
        </div>

    </div>

    {!! Form::close() !!}
</div>
