@extends('layouts.metronic.main')


@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Bayesian Data',
        'actions' => [

            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('bo.bayesiantasks.create')
            ]

        ]
    ])

{{--    @include('pages.bo.discrepancies.filters.table-index')--}}

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'bayesiandata-table',
        'url' => route('bo.bayesiandata.datatable', ['url_query' => $url_query]),
        'server_side' => true,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[0, "desc"]]',
            'data-onchange_submit' => 'true',
            'data-route_method' => 'bayesiandataQuery',
            'data-search' => 'search',
            'data-scrollX' => 'true',

            //the "processing" loader
            'data-blocking_loader' => 'true'
        ],
        'columns' => ['date','probability','test_name','kpi','created_at']
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop

