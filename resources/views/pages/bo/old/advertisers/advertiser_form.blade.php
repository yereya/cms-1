@extends('layouts.metronic.main')

@section('page_title')
    @if (isset($advertiser))
        {{ $advertiser->name }}
    @endif
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($advertiser) ? 'Edit' : 'Create'
    ])

    {!! Form::open([
            'route' => isset($advertiser) ? ['bo.old.advertisers.update', $advertiser['id']] : ['bo.old.advertisers.store'],
            'method' => isset($advertiser) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($advertiser) ? $advertiser['name'] : null
        ])

    @if (isset($advertiser))
        @include('partials.fields.input', [
                'name' => 'mongodb_id',
                'label' => 'Advertiser Id',
                'disabled' => true,
                'value' => isset($advertiser) ? $advertiser->mongodb_id : null
            ])
    @endif

    @include('partials.fields.select', [
        'name' => 'status',
        'label' => 'Status',
        'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
        'value' => isset($advertiser) ? strtolower($advertiser['status']) : ''
    ])

    @include('partials.fields.select', [
        'name' => 'type',
        'label' => 'Type',
        'list' => ['external' => 'External', 'internal' => 'Internal'],
        'value' => isset($advertiser) ? strtolower($advertiser['type']) : ''
    ])

    @include('partials.fields.input', [
            'name' => 'domain',
            'label' => 'Domain',
            'required' => true,
            'value' => isset($advertiser) ? $advertiser['domain'] : ''
        ])

    @include('partials.fields.input', [
            'name' => 'category',
            'required' => true,
            'value' => isset($advertiser) ? $advertiser['category'] : null
        ])

    @include('partials.fields.select', [
            'name' => 'time_zone',
            'label' => 'Time Zone',
            'list' => getTimeZones(),
            'value' => isset($advertiser) ? $advertiser['time_zone'] : ''
        ])

    @if(isset($advertiser))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop

