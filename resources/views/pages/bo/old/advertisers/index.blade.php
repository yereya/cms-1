@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
            'title' => 'Advertisers',
            'actions' => [
                [
                    'drop_down' => true,
                    'title' => 'Actions',
                    'icon' => 'fa fa-th',
                    'menus' => [
                        [
                            'title' => 'Add New Advertiser',
                            'route' => route('bo.old.advertisers.create'),
                            'permission' => auth()->user()->can($permission_name .'.add')
                        ],
                        [
                            'container' => true,
                            'context' => [
                                'include' => 'partials.containers.buttons.download',
                                'route' => ['bo.old.advertisers.datatable'], //['bo.old.accounts.datatable'],
                                'title' => 'Download CSV'
                            ]
                        ]
                    ]
                ]
            ]
        ])

    @include('pages.bo.advertisers.filters.table-advertisers', [])
    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'advertisers',
        'server_side' => true,
        'route' => 'bo.old.advertisers.datatable',
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-route_method' => 'advertisers',
        ],
        'columns' => ['Name', 'Advertiser ID', 'Category', 'Active', 'actions']
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop