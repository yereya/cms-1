
<div class="row">
    <div class="form-group form-horizontal form-md-line-input">
        @include('partials.fields.input', [
            'name' => 'mail_query',
            'label' => 'Mail query',
            'type' => 'text',
            'placeholder' => 'Filter mails',
            'help_text' => 'Enter mail filters seperated by ";". criterias: http://php.net/manual/en/function.imap-search.php',
            'form_line' => true,
            'column' => 6,
            'column_label' => 3,
            'value' => $fetcher->mail_query ?? ''
        ])
    </div>

    <div class="form-group form-horizontal form-md-line-input">
        @include('partials.fields.select', [
                'name' => 'mail_query_type',
                'label' => 'Query type',
                'required' => true,
                'form_line' => true,
                'column' => 6,
                'column_label' => 3,
                'list' => [
                    'attachment' => 'attachment',
                    'body' => 'body'
                ],
                'value' => [$fetcher->mail_query_type ?? '']
            ])
    </div>
</div>
