<div class="processor_row_container" data-processor_id="{{ isset($id) ? $id : 0 }}">
    @if (isset($rows))
@foreach($rows as $processor)
        <div class="row">
            <div class="col-md-12">
                <label class="col-md-2"><span class="pull-right">{{ $processor['name'] }}:</span></label>
                <div class="col-md-10">
                    @if ($processor['type'] == 'textarea')
                        <a href='javascript:;'
                           class='editable'
                           name='{{ strtolower($processor['name']) }}'
                           data-pk=''
                           data-pk_column='{{ strtolower($processor['name']) }}'
                           data-name='{{ $processor['name'] }}'
                           data-value='{{ $processor['value'] }}'
                           data-type='textarea'
                           data-mode="inline"
                           data-rows="2"
                           data-tpl='<textarea style="width: 600px;"></textarea>'
                           data-title='{{ $processor['name'] }}:'>
                        </a></br>
                    @elseif ($processor['type'] == 'select')
                        <a href='javascript:;'
                           class='editable'
                           name='{{ strtolower($processor['name']) }}'
                           data-pk=''
                           data-pk_column='{{ strtolower($processor['name']) }}'
                           data-name='{{ $processor['name'] }}'
                           data-value='{{ $processor['value'] }}'
                           data-type='select'
                           data-source="{{ isset($processor['source']) ? $processor['source'] : null }}"
                           data-mode="inline"
                           data-tpl='<select style="width: 600px;"></select>'
                           data-title='{{ $processor['name'] }}:'>
                        </a></br>
                    @else
                        <a href='javascript:;'
                           class='editable'
                           name='{{ strtolower($processor['name']) }}'
                           data-pk=''
                           data-pk_column='{{ strtolower($processor['name']) }}'
                           data-name='{{ $processor['name'] }}'
                           data-value='{{ $processor['value'] }}'
                           data-type='text'
                           data-mode="inline"
                           data-tpl='<input type="text" style="width: 600px;">'
                           data-title='{{ $processor['name'] }}:'>
                        </a></br>
                    @endif
                </div>
            </div>
        </div>

@endforeach
    <div class="row">
        <div class="col-md-12">
            <label class="col-md-2"><span class="pull-right">Is Active:</span></label>
            <div class="col-md-10">
                <input type="checkbox" name="active" class="form-control" {{ (isset($active) && $active) ? 'checked="checked"' : '' }}>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <label class="col-md-2"></label>
            <div class="col-md-10">
                <a href="{{ $delete['route'] }}"
                   class="btn default remove_processor_btn"><i class="fa icon-trash icon-trash-color fa-fw"></i>Delete</a>
                <a href="{{ $duplicate['route'] }}"
                   class="btn default duplicate_processor_btn"><i class="fa icon-trash icon-trash-color fa-fw"></i>Duplicate</a>
            </div>
        </div>
    </div>
    @include('partials.fields.h4-separator')

@else
        <div class="row">
            <div class="col-md-12">
                <label class="col-md-2"><span class="pull-right">{{ $processor['name'] }}:</span></label>
                <div class="col-md-10">
                    @if ($processor['type'] == 'textarea')
                        <a href='javascript:;'
                           class='editable'
                           name='{{ strtolower($processor['name']) }}'
                           data-pk=''
                           data-pk_column='{{ strtolower($processor['name']) }}'
                           data-name='{{ $processor['name'] }}'
                           data-value=''
                           data-type='textarea'
                           data-mode="inline"
                           data-rows="2"
                           data-tpl='<textarea style="width: 600px;"></textarea>'
                           data-title='{{ $processor['name'] }}:'>
                        </a></br>
                    @elseif ($processor['type'] == 'select')
                        <a href='javascript:;'
                           class='editable'
                           name='{{ strtolower($processor['name']) }}'
                           data-pk=''
                           data-pk_column='{{ strtolower($processor['name']) }}'
                           data-name='{{ $processor['name'] }}'
                           data-value=''
                           data-type='select'
                           data-source="{{ isset($processor['source']) ? $processor['source'] : null }}"
                           data-mode="inline"
                           data-tpl='<select style="width: 600px;"></select>'
                           data-title='{{ $processor['name'] }}:'>
                        </a></br>
                    @else
                        <a href='javascript:;'
                           class='editable'
                           name='{{ strtolower($processor['name']) }}'
                           data-pk=''
                           data-pk_column='{{ strtolower($processor['name']) }}'
                           data-name='{{ $processor['name'] }}'
                           data-value=''
                           data-type='text'
                           data-mode="inline"
                           data-tpl='<input type="text" style="width: 600px;">'
                           data-title='{{ $processor['name'] }}:'>
                        </a></br>
                    @endif
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <label class="col-md-2"><span class="pull-right">Is Active:</span></label>
                <div class="col-md-10">
                    <input type="checkbox" name="active" class="form-control">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label class="col-md-2"></label>
                <div class="col-md-10">
                    <a href="{{ $delete['route'] }}"
                       class="btn default remove_processor_btn"><i class="fa icon-trash icon-trash-color fa-fw"></i>Delete</a>
                    <a href="{{ $duplicate['route'] }}"
                       class="btn default new_processor_btn"><i class="fa icon-trash icon-trash-color fa-fw"></i>Duplicate</a>
                </div>
            </div>
        </div>
        @include('partials.fields.h4-separator')
@endif
</div>
