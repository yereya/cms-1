<div class="properties_fetcher">
    @include('partials.fields.h4-separator', ['label' => 'Translators',])

    @if(isset($fetcher->translators) && count($fetcher->translators) > 0)
        @foreach($fetcher->translators as $translator)
            @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

            @include('partials.fields.select', [
                'name' => 'translator[key][]',
                'label' => '',
                'id' => null,
                'no_label' => true,
                'form_line' => true,
                'column' => 5,
                'column_label' => 0,
                'class' => 'key_group',
                'list' => isset($translators) ? $translators : '',
                'value' => [isset($translator) ? $translator['key'] : '']
            ])

            @include('partials.fields.input', [
                'name' => 'translator[value][]',
                'label' => '',
                'type' => 'text',
                'placeholder' => 'Value',
                'form_line' => true,
                'column' => 5,
                'help_text' => '',
                'column_label' => 0,
                'value' => $translator['value']
            ])

            @include('partials.containers.actions', [
                'class' => true,
                'actions' => [
                    [
                        'action' => 'clone',
                    ],
                    [
                        'action' => 'remove',
                    ]
                ]
            ])

            @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
        @endforeach
    @else
        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

        @include('partials.fields.select', [
            'name' => 'translator[key][]',
            'label' => '',
            'id' => null,
            'no_label' => true,
            'form_line' => true,
            'column' => 5,
            'column_label' => 0,
            'class' => 'key_group',
            'list' => isset($translators) ? $translators : '',
            'value' => ''
        ])

        @include('partials.fields.input', [
            'name' => 'translator[value][]',
            'label' => false,
            'placeholder' => 'Value',
            'form_line' => true,
            'column' => 5,
            'column_label' => 0,
            'value' => ''
        ])

        @include('partials.containers.actions', [
            'class' => true,
            'actions' => [
                [
                    'action' => 'clone',
                ],
                [
                    'action' => 'remove',
                ]
            ]
        ])

        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
    @endif
</div>

<script>
    var translators_docs = {!! json_encode($translators_documentations) !!}

</script>