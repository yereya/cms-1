{{--Fetcher Portlet Start --}}
@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => "Fetcher ",
    'subtitle' => isset($fetcher) ? "type: $fetcher->fetcher_type name: $fetcher->name" : null,
    'has_form' => true,
    'sortable' => 'portlet-sortable',
    'tools' => [
        'collapse' => isset($scraper) ? ['class' => 'expand'] : ['class' => ''],
    ],
    'actions' => [
        [
            'title' => 'Add',
            'class' => isset($fetcher) ? 'default add_fetcher_btn' : 'default add_fetcher_btn hidden',
            'icon' => 'plus'
        ],
        [
            'title' => 'Delete',
            'class' => isset($fetcher) ? 'red delete_btn' : 'red delete_btn hidden',
            'icon' => 'trash'
        ],
        [
            'title' => 'Update',
            'class' => isset($fetcher) ? 'blue update_btn' : 'blue update_btn hidden',
            'icon' => 'cloud-upload'
        ],
        [
            'title' => 'Clear',
            'class' => isset($fetcher) ? 'default clear_btn hidden' : 'default clear_btn',
            'icon' => 'eraser'
        ],
        [
            'title' => 'Save',
            'class' => isset($fetcher) ? 'blue save_btn hidden' : 'blue save_btn',
            'icon' => 'cloud-upload'
        ],
        [
            'title' => 'Test',
            'class' => 'purple test_btn debug_true',
            'icon' => 'desktop'
        ]
    ]
])

{{-- default type --}}
@php($fetcher_action_type = $fetcher->type ?? 'cURL')

<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="{{ ($fetcher_action_type == 'cURL') ? 'active' : ''  }}"><a href="#type-curl-{{ $fetcher->id ?? '0' }}" aria-controls="method-type" role="tab" data-toggle="tab" onClick="toggleFetcherType(this, 'cURL')">cURL</a></li>
            <li role="presentation" class="{{ ($fetcher_action_type == 'Spreadsheet') ? 'active' : '' }}"><a href="#type-spreadsheet-{{ $fetcher->id ?? '0' }}" aria-controls="method-type" role="tab" data-toggle="tab" onClick="toggleFetcherType(this, 'Spreadsheet')">Spreadsheet</a></li>
            <li role="presentation" class="{{ ($fetcher_action_type == 'Mail') ? 'active' : '' }}"><a href="#type-mail-{{ $fetcher->id ?? '0' }}" aria-controls="method-type" role="tab" data-toggle="tab" onClick="toggleFetcherType(this, 'Mail')">Mail</a></li>
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="tab-content hide-with-height">
            {!! Form::open([
                 'class' => isset($fetcher) ? 'form-horizontal fetcher-form tab-content hide-with-height'
                    : 'tab-content hide-with-height form-horizontal fetcher-form form-to-clone',
                 'route' => isset($fetcher) ?
                      ['reports.scraper.{scraper_id}.fetchers.index', $scraper_id , $fetcher->id] :
                      ['reports.scraper.{scraper_id}.fetchers.index', $scraper_id],
                 'method' => 'POST',
                 'role' => 'form',
             ]) !!}

                {!! Form::hidden('type', $fetcher->type ?? 'cURL') !!}
                {!! Form::hidden('order', $fetcher->priority ?? '') !!}
                {!! Form::hidden('fetcher_id', $fetcher->id ?? 0) !!}

                <div id="type-curl-{{ $fetcher->id ?? '0' }}" class="tab-pane {{ ($fetcher_action_type == 'cURL') ? 'active' : ''  }}" role="tabpanel">
                    @include('pages.bo.scraper-brand-performance.fetcher-curl-form', [
                        'fetcher'       => $fetcher ?? null,
                        'scraper_id'    => $scraper_id
                    ])
                </div>

                <div id="type-spreadsheet-{{ $fetcher->id ?? '0' }}" class="tab-pane {{ ($fetcher_action_type == 'Spreadsheet') ? 'active' : '' }}" role="tabpanel">
                    @include('pages.bo.scraper-brand-performance.fetcher-spreadsheet-form', [
                        'fetcher'       => $fetcher ?? null,
                        'scraper_id'    => $scraper_id
                    ])
                </div>

                <div id="type-mail-{{ $fetcher->id ?? '0' }}" class="tab-pane {{ ($fetcher_action_type == 'Mail') ? 'active' : '' }}" role="tabpanel">
                    @include('pages.bo.scraper-brand-performance.fetcher-email-form', [
                        'fetcher'       => $fetcher ?? null,
                        'scraper_id'    => $scraper_id
                    ])
                </div>

                @include('pages.bo.scraper-brand-performance.scraper-translator')
            {!! Form::close() !!}
        </div>
    </div>
</div>

<script type="application/javascript">
    // TODO - needs to be included somewhere else (app.js ?)
    function toggleFetcherType(element, type) {
        var processed_element = document.getElementById(element.getAttribute('href').substring(1)).parentNode;
        var chosen_element = processed_element.querySelectorAll('input[name="type"]')[0];
        chosen_element.setAttribute('value', type);
    }
</script>

@include('partials.containers.portlet', [
    'part' => 'close'
])
{{--Fetcher Portlet End--}}
