<div class="log_container" data-log_url="{{ route('reports.logs.{log_id}.list-group', ['log_id']) . '?debug=true' }}">

@include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Logs',
        ])

@include('partials.containers.data-table', [
       'data' => $logs,
       'attributes' => [
            'data-order' => '[[0, "desc"]]',
            "data-scrollY" => "1000px",
            "data-bSort" => "false"
       ],
       'columns' => [
           [
               'label' => 'Date',
               'filter_type' => 'null',
               'value' => function ($data) {
                   return $data->created_at;
               }
           ],
           [
               'label' => 'Run result',
               'filter_type' => 'null',
               'value' => function ($data) {
                    switch ($data->error_type) {
                        case 'info' :
                            return '<i class="glyphicon glyphicon-info-sign info-icon" title="Info"> </i>';
                        break;

                        case 'warning' :
                            return '<i class="glyphicon glyphicon-warning-sign warning-icon" style="color: orange;" title="Warning"> </i>';
                        break;

                        case 'error' :
                            return '<i class="glyphicon glyphicon-remove-sign error-icon" style="color: red;" title="Error"> </i>';
                        break;

                        case 'notice' :
                            return '<i class="glyphicon glyphicon-bullhorn notice-icon" title="Notice"> </i>';
                        break;

                        case 'debug' :
                            return '<i class="fa fa-bug debug-icon" title="Debug"> </i>';
                        break;
                    }
               }
           ],
           [
               'label' => 'Message',
               'filter_type' => 'null',
               'value' => function ($data) {
                    if (isJson($data->message)) {
                        return arrayToString(json_decode($data->message, true));
                    } else {
                        return $data->message;
                    }
               }
           ],
            [
                'label' => 'Initiator',
                'filter_type' => 'null',
                'value' => function ($data) use ($users) {
                    if ($data->initiator_user_id == 0) {
                        return '<i class="fa fa-terminal tooltips" title="Ran in background"></i> Scheduler';
                    } else {
                        return @$users[$data->initiator_user_id]->username;
                    }
                }
            ],
           [
               'label' => 'Actions',
               'filter_type' => 'null',
               'value' => function ($data) {
                    $res = "";
                    $res .= "<a href='" . route('reports.logs.{log_id}.show-modal', [$data->id]) . "'
                        title='Show log'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'
                        data-original-title='Show log'
                        ><span aria-hidden='true' class='icon-magnifier'></span></a> ";
                    $res .= "<a href='" . route('reports.logs.{log_id}.show-modal', [$data->id]) . "?debug=true'
                        title='Show log with debug data'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'
                        data-original-title='Show log with debug data'
                        ><span aria-hidden='true' class='fa fa-bug'></span></a> ";

                    return $res;
               }
           ]
       ]
    ])

@include('partials.containers.portlet', [
'part' => 'close'
])

</div>