@extends('layouts.metronic.main')

@section('page_content')
    <div class="row">
        <div class="col-lg-12">

            {{--Basic Portlet--}}
            <div class="scraper_container">
                @include('pages.bo.scraper-brand-performance.form-scraper', [
                    'scraper' => isset($scraper) ? $scraper : null,
                    'day_hours' => $day_hours,
                    'time_between_executions' => $time_between_executions,
                ])
            </div>
            {{--Basic Portlet Ends--}}

            @if (isset($scraper))
                {{--Fetcher Portlet Start --}}
                @if (auth()->user()->can($permission_name.'.portlet[fetcher].view'))
                <div class="fetcher_container ui-sortable" id="sortable_portlets">
                    <div class="column sortable">
                    @if (count($fetchers) == 0)
                        @include('pages.bo.scraper-brand-performance.form-fetcher', [
                            'scraper_id' => $scraper->id
                        ])
                    @endif

                    @foreach($fetchers as $fetcher)
                        @include('pages.bo.scraper-brand-performance.form-fetcher', [
                            'fetcher' => $fetcher,
                            'scraper_id' => $scraper->id
                        ])
                    @endforeach
                    {{--Empty fetcher form--}}
                    <?php unset($fetcher); ?>
                    </div>
                </div>
                @endif
                {{--Fetcher Portlet End--}}

                @if (auth()->user()->can($permission_name.'.portlet[processor].view'))
                @include('pages.bo.scraper-brand-performance.form-processor', [
                    'processors' => $processors
                ])
                @endif

                @if (auth()->user()->can($permission_name.'.portlet[log].view'))
                @include('pages.bo.scraper-brand-performance.log', [
                    'logs' => isset($logs) ? $logs : []
                ])
                @endif
            @endif


        </div>
    </div>
@stop