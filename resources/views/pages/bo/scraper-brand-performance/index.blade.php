@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Scrapers',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New',
                    'target' => route('reports.scraper.create')
                ],
                [
                    'icon' => 'icon-cloud-upload',
                    'title' => 'Manage Tracks',
                    'target' => route('reports.advertisers.tracks-manager.index')
                ]
            ]
        ])

    @include('partials.containers.data-table', [
        'data' => $scrapers,
        'attributes' => [
                'data-page-length' => '20',
                'data-order' => '[[3, "asc"]]'
           ],
        'columns' => [
            [
                'key' => 'id',
                'value' => function ($data) {
                    return "<a href='". route('reports.scraper.edit', [$data->id]) ."'>{$data->id}</a>";
                }
            ],
            [
                'key' => 'Name',
                'value' => function ($data) {
                    return "<a href='". route('reports.scraper.edit', [$data->id]) ."'>{$data->scraper_name}</a>";
                }
            ],
            [
                'key' => 'advertiser_brand',
                'value' => function ($data) {
                    return isset($data->advertisers->name) ? $data->advertisers->name : '';
                }
            ],
            [
                'key' => 'is_active',
                'value' => function ($data) {
                    if ($data->active) {
                        return '<span class="label label-sm label-success"> Active </span>';
                    } else {
                        return '<span class="label label-sm label-danger"> Disabled </span>';
                    }
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) {
                    $res = '';
                    $res .= "<a href='". route('reports.scraper.edit', [$data->id]) ."' title='Edit' class='tooltips' ><span aria-hidden='true' class='icon-pencil'></span></a> ";
                    $res .= "<a href='". route('reports.scraper.{scraper_id}.duplicate', [$data->id]) ."' title='Duplicate this Scraper' class='tooltips' ><span aria-hidden='true' class='fa fa-copy fa-fw'></span></a> ";

                    return $res;
                }
            ]
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop