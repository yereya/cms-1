
    <div class="row">
        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.input', [
                'name' => 'url',
                'label' => 'Url',
                'required' => true,
                'form_line' => true,
                'column' => 12,
                'column_label' => 2,
                'value' => isset($fetcher) ? $fetcher->url : ''
            ])
        </div>

        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.select', [
                'name' => 'method',
                'label' => 'Method',
                'required' => true,
                'form_line' => true,
                'column' => 6,
                'column_label' => 3,
                'list' => [
                    'GET' => 'GET',
                    'POST' => 'POST',
                    'HEAD' => 'HEAD',
                    'PUT' => 'PUT',
                    'DELETE' => 'DELETE',
                    'OPTIONS' => 'OPTIONS',
                    'CONNECT' => 'CONNECT'
                ],
                'value' => [isset($fetcher) ? $fetcher->method : '']
            ])
        </div>

        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.select', [
                'name' => 'fetcher_type',
                'label' => 'Fetch type',
                'required' => true,
                'form_line' => true,
                'column' => 6,
                'column_label' => 3,
                'list' => [
                    'OpenPage' => 'OpenPage',
                    'Login' => 'Login',
                    'FetchCSRF' => 'FetchCSRF',
                    'FetchResource' => 'FetchResource'
                ],
                'value' => [isset($fetcher) ? $fetcher->fetcher_type : '']
            ])

            @include('partials.fields.select', [
                'name' => 'fetcher_sleep',
                'label' => 'Sleep',
                'form_line' => true,
                'column' => 6,
                'column_label' => 3,
                'list' => range(0, 90, 1),
                'value' => [isset($fetcher) ? $fetcher->fetcher_sleep : '']
            ])

        </div>

        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.checkbox', [
                'label' => 'Configurations:',
                'wrapper' => 'form-group',
                'list' => [
                    [
                        'name' => 'use_credentials_as_auth',
                        'label' => 'Use User/Pass as Auth',
                        'value' => 1,
                        'checked' => $fetcher->use_credentials_as_auth ?? 0
                    ],
                    [
                        'name' => 'protocol_1_0',
                        'label' => 'Use http 1.0 (alternative to 1.1)',
                        'value' => 1,
                        'checked' => $fetcher->protocol_1_0 ?? 0
                    ],
                    [
                        'name'    => 'ssl_certificate_verification',
                        'label'   => 'Disable SSL certificate verification',
                        'value'   => 1,
                        'checked' => $fetcher->ssl_certificate_verification ?? 0
                    ],
                    [
                        'name'    => 'disable_redirects',
                        'label'   => 'Disable redirects',
                        'value'   => 1,
                        'checked' => $fetcher->disable_redirects ?? 0
                    ],
                    [
                        'name'    => 'disable_http_errors',
                        'label'   => 'Disable Http Errors',
                        'value'   => 1,
                        'checked' => $fetcher->disable_http_errors ?? 0
                    ],
                ],
            ])
            @include('partials.fields.input', [
                'name' => 'concat_n_days_results',
                'label' => 'Concat days of results',
                'type' => 'number',
                'placeholder' => 'N days back',
                'help_text' => 'Concatenates the results from N days of results back. use [date] short code for range attributes.',
                'form_line' => true,
                'column' => 6,
                'column_label' => 3,
                'value' => isset($fetcher) ? $fetcher->concat_n_days_results : ''
            ])

        </div>

        {{-- Fetcher Propetries --}}
        <div class="form-md-line-input">

            <div class="properties_fetcher">
                @include('partials.fields.h4-separator', ['label' => 'Headers',])
                @if(isset($fetcher) && count($fetcher->header) > 0)
                    @foreach($fetcher->header as $header)
                        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                        @include('partials.fields.input', [
                            'name' => 'header[key][]',
                            'no_label' => true,
                            'placeholder' => 'Key',
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $header['key']
                        ])

                        @include('partials.fields.input', [
                            'name' => 'header[value][]',
                            'label' => false,
                            'placeholder' => 'Value',
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $header['value']
                        ])

                    @include('partials.containers.actions', [
                        'class' => true,
                        'actions' => [
                            [
                                'action' => 'clone',
                            ],
                            [
                                'action' => 'remove',
                            ]
                        ]
                    ])

                        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                    @endforeach
                @else
                    @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                    @include('partials.fields.input', [
                        'name' => 'header[key][]',
                        'no_label' => true,
                        'placeholder' => 'Key',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => ''
                    ])

                    @include('partials.fields.input', [
                        'name' => 'header[value][]',
                        'label' => false,
                        'placeholder' => 'Value',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => ''
                    ])

                    @include('partials.containers.actions', [
                        'class' => true,
                        'actions' => [
                            [
                                'action' => 'clone',
                            ],
                            [
                                'action' => 'remove',
                            ]
                        ]
                    ])

                    @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                @endif
            </div>

            <div class="properties_fetcher">
                @include('partials.fields.h4-separator', ['label' => 'Body',])

                @if(isset($fetcher->body) && count($fetcher->body) > 0)
                    @foreach($fetcher->body as $body)
                        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                        @include('partials.fields.input', [
                            'name' => 'body[key][]',
                            'no_label' => true,
                            'placeholder' => 'Key',
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $body['key']
                        ])

                        @include('partials.fields.input', [
                            'name' => 'body[value][]',
                            'label' => false,
                            'placeholder' => 'Value',
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $body['value']
                        ])

                        @include('partials.containers.actions', [
                            'class' => true,
                            'actions' => [
                                [
                                    'action' => 'clone',
                                ],
                                [
                                    'action' => 'remove',
                                ]
                            ]
                        ])

                        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                    @endforeach
                @else
                    @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                    @include('partials.fields.input', [
                        'name' => 'body[key][]',
                        'no_label' => true,
                        'placeholder' => 'Key',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => ''
                    ])

                    @include('partials.fields.input', [
                        'name' => 'body[value][]',
                        'label' => false,
                        'placeholder' => 'Value',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => ''
                    ])

                    @include('partials.containers.actions', [
                        'class' => true,
                        'actions' => [
                            [
                                'action' => 'clone',
                            ],
                            [
                                'action' => 'remove',
                            ]
                        ]
                    ])

                    @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                @endif
            </div>

            <div class="properties_fetcher">
                @include('partials.fields.h4-separator', ['label' => 'Url params',])

                @if(isset($fetcher->queries) && count($fetcher->queries) > 0)
                    @foreach($fetcher->queries as $query)
                        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                        @include('partials.fields.input', [
                            'name' => 'query[key][]',
                            'no_label' => true,
                            'placeholder' => 'Key',
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $query['key']
                        ])

                        @include('partials.fields.input', [
                            'name' => 'query[value][]',
                            'label' => false,
                            'placeholder' => 'Value',
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $query['value']
                        ])

                        @include('partials.containers.actions', [
                            'class' => true,
                            'actions' => [
                                [
                                    'action' => 'clone',
                                ],
                                [
                                    'action' => 'remove',
                                ]
                            ]
                        ])

                        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                    @endforeach
                @else
                    @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                    @include('partials.fields.input', [
                        'name' => 'query[key][]',
                        'no_label' => true,
                        'placeholder' => 'Key',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => ''
                    ])

                    @include('partials.fields.input', [
                        'name' => 'query[value][]',
                        'label' => false,
                        'placeholder' => 'Value',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => ''
                    ])

                    @include('partials.containers.actions', [
                        'class' => true,
                        'actions' => [
                            [
                                'action' => 'clone',
                            ],
                            [
                                'action' => 'remove',
                            ]
                        ]
                    ])

                    @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                @endif
            </div>
        </div>
    </div>
