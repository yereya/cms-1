<div class="processor_container" data-processors_url="{{ route('reports.scraper.{scraper_id}.processors', $scraper->id) }}">
    @include('partials.containers.portlet', [
         'part' => 'open',
         'title' => 'Processors',
         'tools' => [
            [
                //'class' => 'expand'
                'class' => 'collapse'
            ],
         ],
         'actions' => [
              [
                   'icon' => 'plus',
                   'title' => 'Add',
                   'class' => 'default new_processor_btn'
              ],
              [
                   'icon' => 'cloud-upload',
                   'title' => 'Save',
                   'class' => isset($processors) && count($processors) > 0 ? 'blue save_processor_btn hidden' : 'blue save_processor_btn'
              ],
              [
                   'icon' => 'cloud-upload',
                   'title' => 'Update',
                   'class' => isset($processors) && count($processors) > 0 ? 'blue update_processor_btn' : 'blue update_processor_btn hidden'
              ],
              [
                   'title' => 'Test',
                   'class' => isset($processors) && count($processors) > 0 ?
                        'purple test_all_btn debug_true' : 'purple test_all_btn debug_true hidden',
                   'icon' => 'desktop'
              ]
         ]
    ])

    <?php $events = [
            ['value' => "lead", 'text' => "lead"],
            ['value' => "sale", 'text' => "sale"],
            ['value' => "call", 'text' => "call"],
            ['value' => "install", 'text' => "install"],
            ['value' => "adjustment", 'text' => "adjustment"],
            ['value' => "canceled-sale", 'text' => "canceled-sale"],
            ['value' => "canceled-lead", 'text' => "canceled-lead"],
            ['value' => "deposit", 'text' => "deposit"],

    ]; ?>


    @if (count($processors) > 0)
    @foreach($processors as $processor)
        @include('pages.bo.scraper-brand-performance.processor_grid', [
            'id' => $processor['id'],
            'rows' => [
                [
                    'name' => 'Event',
                    'type' => 'select',
                    'source' => json_encode($events),
                    'value' => $processor['event']
                ],
                [
                    'name' => 'Date',
                    'type' => 'textarea',
                    'value' => $processor['date']
                ],
                [
                    'name' => 'Token',
                    'type' => 'textarea',
                    'value' => $processor['token']
                ],
                [
                    'name' => 'TRX_ID',
                    'type' => 'textarea',
                    'value' => $processor['trx_id']
                ],
                [
                    'name' => 'IO_ID',
                    'type' => 'textarea',
                    'value' => $processor['io_id']
                ],
                [
                    'name' => 'Commission_amount',
                    'type' => 'textarea',
                    'value' => $processor['commission_amount']
                ],
                [
                    'name' => 'Amount',
                    'type' => 'text',
                    'value' => $processor['amount']
                ],
                [
                    'name' => 'Currency',
                    'type' => 'text',
                    'value' => $processor['currency'] ? $processor['currency'] : '[var name="currency"]'
                ],
                [
                    'name' => 'Rule',
                    'type' => 'textarea',
                    'value' => $processor['rule']
                ],
            ],
            'active' => $processor['active'],
            'delete'    => ['route' => route('reports.scraper.{scraper_id}.processors.{processor_id}', [$scraper->id, $processor['id']])],
            'duplicate' => ['route' => route('reports.scraper.{scraper_id}.processors.{processor_id}', [$scraper->id, $processor['id']])]
        ])
    @endforeach
    @endif

    @if (count($processors) == 0 || !isset($processors))
        @include('pages.bo.scraper-brand-performance.processor_grid', [
            'rows' => [
                [
                    'name' => 'Event',
                    'type' => 'select',
                    'value' => '',
                    'source' => json_encode($events),
                ],
                [
                    'name' => 'Date',
                    'value' => '',
                    'type' => 'textarea',
                ],
                [
                    'name' => 'Token',
                    'value' => '',
                    'type' => 'textarea',
                ],
                [
                    'name' => 'TRX_ID',
                    'value' => '',
                    'type' => 'textarea',
                ],
                [
                    'name' => 'IO_ID',
                    'value' => '',
                    'type' => 'textarea',
                ],
                [
                    'name' => 'Commission_amount',
                    'value' => '',
                    'type' => 'textarea',
                ],
                [
                    'name' => 'Amount',
                    'value' => '',
                    'type' => 'text',
                ],
                [
                    'name' => 'Currency',
                    'value' => '',
                    'type' => 'text',
                ],
                [
                    'name' => 'Rule',
                    'value' => '',
                    'type' => 'textarea',
                ],
            ],
            'delete'    => ['route' => route('reports.scraper.{scraper_id}.processors.{processor_id}', [$scraper->id, 'pid'])],
            'duplicate' => ['route' => route('reports.scraper.{scraper_id}.processors.{processor_id}', [$scraper->id, 'pid'])]
        ])
    @endif

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
</div>

