{!! Form::open([
       'route' => isset($scraper) ? ['reports.scraper.update', $scraper->id] : ['reports.scraper.store'],
       'method' => isset($scraper) ? 'PUT' : 'POST',
       'class' => 'form-horizontal',
       'role' => 'form'
   ]) !!}

    @if($scraper)
        {!! Form::hidden('id', $scraper->id) !!}
    @endif

    {!! Form::hidden('debug', false) !!}

@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => "Scraper ",
    'subtitle' => isset($scraper) ? "(id: $scraper->id)" : null,
    'has_form' => true,
    'actions' => [
        [
            'type' => isset($scraper) ? 'link' : 'submit',
            'title' => isset($scraper) ? 'Update' : 'Save',
            'class' => isset($scraper) ? 'blue scraper_update_btn' : 'blue scraper_save_btn',
            'icon' => 'cloud-upload'
        ],
        [
            'type' => isset($scraper) ? 'link' : '',
            'title' => isset($scraper) ? 'Test' : '',
            'class' => isset($scraper) ? 'purple test_all_btn debug_true' : 'hidden',
            'icon' => 'desktop'
        ],
        [
            'type' => isset($scraper) ? 'link' : '',
            'title' => isset($scraper) ? 'Run' : '',
            'class' => isset($scraper) ? 'red-thunderbird test_all_btn debug_false' : 'hidden',
            'icon' => 'fa fa-play-circle'
        ],
    ]
])
    <div class="row">
        <div class="form-group form-horizontal form-md-line-input">

            @include('partials.fields.input', [
                    'name' => 'scraper_name',
                    'label' => 'Name',
                    'required' => true,
                    'value' => isset($scraper) ? $scraper->scraper_name : ''
                ])

        </div>

        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.select', [
                    'name' => 'advertiser_id',
                    'label' => 'Advertiser',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'required' => true,
                    'list' => App\Entities\Models\Reports\Publishers\Advertiser::pluck('name', 'mongodb_id'),
                    'value' => isset($scraper) ? $scraper->advertiser_id : ''
                ])
        </div>

        @include('partials.fields.h4-separator', ['label' => 'Run time',])

        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

        @include('partials.fields.input-timepicker', [
                'name' => 'run_time_start',
                'label' => '',
                'label_addon' => 'Run Time Start',
                'time' => true,
                'form_line' => true,
                'addon' => 'fa-hourglass-start',
                'column' => 5,
                'column_label' => 2,
                'value' => isset($scraper) && isset($scraper->run_time_start) ? $scraper->run_time_start : "00:00"
            ])

        @include('partials.fields.input-timepicker', [
                'name' => 'run_time_stop',
                'label' => '',
                'label_addon' => 'Run Time End',
                'form_line' => true,
                'addon' => 'fa-hourglass-end',
                'time' => true,
                'column' => 5,
                'column_label' => 2,
                'value' => isset($scraper) && isset($scraper->run_time_stop) ? $scraper->run_time_stop : "23:59"
            ])
        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])

        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.select', [
                    'name' => 'repeat_delay',
                    'label' => 'Repeat Delay',
                    'form_line' => true,
                    'time' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'required' => true,
                    'list' => $time_between_executions,
                    'value' => isset($scraper) ? $scraper->repeat_delay : 60
                ])

            @include('partials.fields.input', [
                    'name' => 'days_ago',
                    'label' => 'Run days back',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'list' => $time_between_executions,
                    'value' => isset($scraper) ? $scraper->days_ago : 3
                ])

        </div>


        @include('partials.fields.h4-separator', ['label' => 'Settings',])
        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.checkbox', [
                    'wrapper' => true,
                    'label' => 'Options:',
                    'list' => [
                        [
                            'name' => 'active',
                            'label' => 'Is Active',
                            'value' => 1,
                            'checked' => isset($scraper) ? $scraper->active : 0
                        ],
                        [
                            'name' => 'save_to_adserver',
                            'label' => 'Save To Ad Server (legacy system)',
                            'value' => 1,
                            'checked' => isset($scraper) ? $scraper->save_to_adserver : 0
                        ],
                        [
                            'name' => 'save_to_new_adserver',
                            'label' => 'Save To New Out (tracking system)',
                            'value' => 1,
                            'checked' => isset($scraper) ? $scraper->save_to_new_adserver : 0
                        ],
                        [
                            'name' => 'locked_date',
                            'label' => 'Date Locked',
                            'title' =>  'Limit returning records, in case it passed 5th day in current month, return only the the current month else return from last month',
                            'value' => 1,
                            'checked' => isset($scraper) ? $scraper->locked_date : 0
                        ],
                    ],
                ])
        </div>

        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.input', [
                    'name' => 'username',
                    'label' => 'Username',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'help_text' => 'ShortCode: [var name="username"]',
                    'value' => isset($scraper) ? $scraper->username : ''
                ])

            @include('partials.fields.input', [
                    'name' => 'password',
                    'label' => 'Password',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'help_text' => 'ShortCode: [var name="password"]',
                    'value' => isset($scraper) ? $scraper->password : ''
                ])
        </div>
        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.input', [
                    'name' => 'ioid',
                    'label' => 'ioId',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'help_text' => 'ShortCode: [var name="ioid"]',
                    'value' => isset($scraper) ? $scraper->ioid : ''
                ])

            @include('partials.fields.input', [
                    'name' => 'trxid',
                    'label' => 'trxId',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'help_text' => 'ShortCode: [var name="trxid"]',
                    'value' => isset($scraper) ? $scraper->trxid : ''
                ])
        </div>
        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.input', [
                    'name' => 'amount',
                    'label' => 'Amount',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'help_text' => 'ShortCode: [var name="amount"]',
                    'value' => isset($scraper) ? $scraper->amount : ''
                ])

            @include('partials.fields.input', [
                    'name' => 'commission_amount',
                    'label' => 'Commission Amount',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'help_text' => 'ShortCode: [var name="commission_amount"]',
                    'value' => isset($scraper) ? $scraper->commission_amount : ''
                ])
        </div>
        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.select', [
                    'name' => 'currency',
                    'label' => 'Currency',
                    'form_line' => true,
                    'column' => 6,
                    'column_label' => 3,
                    'list' => ['USD' => 'USD', 'EUR' => 'EUR', 'GBP' => 'GBP'],
                    'value' => [isset($scraper) ? $scraper->currency : '']
                ])
        </div>
    </div>

    @if(isset($scraper))
        <div class="advanced_container">
            @include('partials.fields.h4-separator', ['label' => 'Advanced',])
            @if(count($scraper->properties) > 0)
                @foreach($scraper->properties as $proper)
                    @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                    @include('partials.fields.input', [
                        'name' => 'properties[key][]',
                        'no_label' => true,
                        'placeholder' => 'Key',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => $proper['key']
                    ])

                    @include('partials.fields.input', [
                        'name' => 'properties[value][]',
                        'label' => false,
                        'placeholder' => 'Value',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => $proper['value']
                    ])

                    @include('partials.containers.actions', [
                    'class' => true,
                    'actions' => [
                        [
                            'action' => 'clone',
                        ],
                        [
                            'action' => 'remove',
                        ]
                    ]
                ])

                    @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                @endforeach
            @else
                @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                @include('partials.fields.input', [
                    'name' => 'properties[key][]',
                    'no_label' => true,
                    'placeholder' => 'Key',
                    'form_line' => true,
                    'column' => 5,
                    'column_label' => 0,
                    'value' => ''
                ])

                @include('partials.fields.input', [
                    'name' => 'properties[value][]',
                    'label' => false,
                    'placeholder' => 'Value',
                    'form_line' => true,
                    'column' => 5,
                    'column_label' => 0,
                    'value' => ''
                ])

                @include('partials.containers.actions', [
                    'class' => true,
                    'actions' => [
                        [
                            'action' => 'clone',
                        ],
                        [
                            'action' => 'remove',
                        ]
                    ]
                ])

                @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
            @endif
        </div>
    @endif

@include('partials.containers.portlet', [
    'part' => 'close'
])

{!! Form::close() !!}
