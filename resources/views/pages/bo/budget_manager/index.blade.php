@extends('layouts.metronic.main')

@section('head')
    <link href="{{ asset('assets/layout/css/budget_manager.css') }}" rel="stylesheet">


@stop
@section('page_content')

    <div id="app2" >
        <template>

            <div class="row mt-3">
                <div class="main_header" style="">
                    <h3 class="text-info text-bold" > Budget Manager</h3>
                </div>
            </div>
            <hr class="bg-info">
            <div class="row mt-3" >
                <div class="col-sm-12">
                    <div id="outer" class="outer">


                        <filter-btn :list="budget_name_list"   ref_name="budget_name_ref" label="Budget Name" :filter_func="select_filter_param"
                                    selected_list="selected_budget_name" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="dates_list"   ref_name="dates_ref" label="Date" :filter_func="select_filter_param"
                                    selected_list="selected_dates" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="advertisers_list"  ref_name="advertiser_ref" label="Advertiser" :filter_func="select_filter_param"
                                    selected_list="selected_advertisers" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="group_name_list"  ref_name="group_name_ref" label="Group Name" :filter_func="select_filter_param"
                                    selected_list="selected_group_names" :check_selection="check_selection"></filter-btn>

                        <filter-btn :list="columns"  ref_name="select_col_ref" label="Select Columns" :filter_func="select_filter_param"
                                    selected_list="selected_columns" :check_selection="check_selection"></filter-btn>

                        <div class=""style="display: inline-block;float: right">
                            <div class="dropdown">
                                <button class="btn btn-info float-right" @click="tableToExcel('table', 'Lorem Table')"><i class="fa fa-download"></i> </button>
                            </div>
                        </div>

                        <div class=""style="display: inline-block;float: right;margin-right: 3px;">
                                <button class="btn btn-info float-right" @click="clear_filters()"><i class="fa fa-refresh"></i> </button>
                        </div>

                        <button class="btn btn-info float-right" style="float:right;margin-right: 3px;" @click="form_status=0;new_budget()">&nbsp;Add Budget</button>
                    </div>
                    <label class="text-bold" style="font-size: 17px;padding-top: 17px;padding-left: 67px;"> Showing @{{ total_results }} records</label>
                    <div class="search-wrapper search-query" >
                        <input class="form-control" type="text" v-model="searchQuery" placeholder="Search" />
                    </div>
                </div>
            </div>

            <hr class="bg-info">
            <!-- Display Records -->
            <div>
                <div class="row" style="display: block;margin-left: 50px;" >
                    <div class="col-lg-12 table-out">
                        <div class="table-in">

                            <table class="table table-bordered table-striped " ref="table" id="loremTable" >
                                <thead >
                                <tr class="text-center bg-info text-light " >
                                    <th v-for="column in selected_columns" >
                                        <a @click="order_list(column)"> @{{ column  }} </a>
                                    </th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody  style="overflow: auto;">
                                <tr class="text-center" v-for="budget in computed_data " :key="budget.id">
                                         <td v-for="column in selected_columns">
                                             @{{ budget[column] }}
                                        </td>
                                    <td>
                                        <a href="#" class="text-success" @click="select_budget(budget);"><i class="fa fa-pencil-square-o"></i> </a>
                                        <a href="#" class="text-success" @click="delete_budget(budget);"><i class="fa fa-remove"></i> </a>
                                        <a href="#" class="text-success" @click="duplicate_budget(budget);"><i class="fa fa-clone"></i> </a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div id="overlay" v-if="showAddModal" >
                            ------------------------------------------------------------------------------------------------------- - Form ---------------------------------------------------------------
                            <div class="modal-dialog overlayed" style="margin-top: -25px">
                                <div class="modal-content" style="max-height: 95vh;overflow: scroll">
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="text-align: center;font-weight: bold;font-size: 19px">@{{ form_status === 0 ? 'Add A New Budget' : 'Budget'}}</h5>
                                        <button type="button" class="close" @click="showAddModal=false">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body ">
                                        <form  action="#" method="post">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-md-offset-1 col-md-10">
                                                            <form class="form-horizontal" style="padding: 3px">
                                                                <div class="form-content">
                                                                    <h4 class="heading" style="text-decoration: underline">Budget's Name and Brand Group</h4>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-12">
                                                                            <label class="control-label" for="budget_name"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="budget_name">Budget's Name</label>
                                                                            <input class="form-control" id="budget_name" v-model="selected_item.budget_name" placeholder="" type="text" required>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="brand_group_name"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="brand_group_name">Brand's Group Name</label>
                                                                            <select  class="form-control select_box" required v-model="brand_group_obj" @change="change_brand_group()"  >
                                                                                <option selected value></option>
                                                                                <option v-for="group_name in full_group_name_list"
                                                                                        :value="{ id: group_name.id, name: group_name.name,company_id: group_name.company_id }">@{{ group_name.name }}</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="company_name"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="company_name">Company's Name</label>
                                                                            <select  class="form-control select_box" required v-model="selected_item.company_name" @change="change_company()"
{{--                                                                            :disabled="selected_item.brand_group_name && selected_item.brand_group_name != ''"--}}
                                                                            >
                                                                                <option selected value></option>
                                                                                <option v-for="company_name in full_company_name_list" >@{{ company_name.name }}</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>
                                                                    <h4 class="heading" style="text-decoration: underline;">LP & Page & Country</h4>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="landingpage_name"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="landingpage_name">Landing Page</label>

                                                                            <select  class="form-control select_box" required v-model="selected_item.landingpage_name"
                                                                                     @change="select_filter_param('selected_landing_page_names',selected_item.landingpage_name)">
                                                                                <option selected value></option>
                                                                                <option v-for="ld in lp_by_brand_group_list" :value="ld.name"
                                                                                        :class="check_selection('selected_landing_page_names',ld.name)?'selected':''"
                                                                                >@{{ ld.name }}</option>
                                                                                <option disabled>Multiple Selection</option>
                                                                            </select>


                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="LP_rule"><i class="fa fa-arrow-circle-o-down"></i></label>
                                                                            <label class="control-label" for="LP_rule">Landing Page Rule</label>
                                                                            <select class="form-control select_box" v-model="selected_item.LP_rule" >
                                                                                <option selected value></option>
                                                                                <option>in</option>
                                                                                <option>not in</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="page"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="page">Page</label>
                                                                            <input class="form-control" v-model="selected_item.page" id="page" placeholder="" type="text">
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="page_rule"><i class="fa fa-arrow-circle-o-down"></i></label>
                                                                            <label class="control-label" for="page_rule" >Page Rule</label>
                                                                            <select class="form-control select_box" v-model="selected_item.page_rule" >
                                                                                <option selected value></option>
                                                                                <option>in</option>
                                                                                <option>not in</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="country"><i class="fa fa-user"></i></label>
                                                                            <label class="control-label" for="country">Country</label>
                                                                            <select  class="form-control select_box" required v-model="selected_item.country"
                                                                                     @change="select_filter_param('selected_countries',selected_item.country)">
                                                                                <option selected value></option>
                                                                                <option v-for="country in countries_list" :value="country.symbol"
                                                                                        :class="check_selection('selected_countries',country.symbol)?'selected':''"
                                                                                >@{{ country.name }}</option>
                                                                                <option disabled>Multiple Selection</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="country_rule"><i class="fa fa-arrow-circle-o-down"></i></label>
                                                                            <label class="control-label" for="country_rule" >Country Rule</label>
                                                                            <select class="form-control select_box" v-model="selected_item.country_rule" >
                                                                                <option selected value></option>
                                                                                <option>in</option>
                                                                                <option>not in</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>
                                                                    <h4 class="heading" style="text-decoration: underline;">Device & Event</h4>

                                                                    <div class="form-group nomargin">

                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="device"><i class="fa  fa-deviantart"></i></label>
                                                                            <label class="control-label" for="device">Device</label>
                                                                            <select  class="form-control select_box" required v-model="selected_item.device"
                                                                                     @change="select_filter_param('selected_devices',selected_item.device)">
                                                                                <option selected value></option>d
                                                                                <option v-for="device in devices_list"
                                                                                        :class="check_selection('selected_devices',device)?'selected':''"
                                                                                     >@{{ device }}</option>
                                                                                <option disabled>Multiple Selection</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="device_rule"><i class="fa fa-arrow-circle-o-down"></i></label>
                                                                            <label class="control-label" for="device_rule" >Device Rule</label>
                                                                            <select class="form-control select_box" v-model="selected_item.device_rule" >
                                                                                <option selected value></option>
                                                                                <option>in</option>
                                                                                <option>not in</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="event"><i class="fa  fa-deviantart"></i></label>
                                                                            <label class="control-label" for="event">Event</label>
                                                                            <select  class="form-control select_box" v-model="selected_item.event"  >
                                                                                <option selected value></option>
                                                                                <option>lead</option>
                                                                                <option>sale</option>
                                                                            </select>
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="event_amount"><i class="fa fa-sort-numeric-asc"></i></label>
                                                                            <label class="control-label" for="event_amount" >Event Amount</label>
                                                                            <input class="form-control" v-model="selected_item.event_amount" id="event_amount" placeholder=""
                                                                                   type="number" :disabled="selected_item.budget_base_amount > 0">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin"><div class="col-sm-12"><hr class="rounded"></div></div>
                                                                    <h4 class="heading" style="text-decoration: underline;">Date & Budget Amount</h4>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="restrict"><i class="fa fa-legal"></i></label>
                                                                            <label class="control-label" for="restrict" >Restrict</label>
                                                                            <select class="form-control select_box" v-model="selected_item.restrict" required>
                                                                                <option selected value></option>
                                                                                <option>Yes</option>
                                                                                <option>No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="budget_base_amount"><i class="fa fa-sort-numeric-asc"></i></label>
                                                                            <label class="control-label" for="budget_base_amount">Budget Base Amount</label>
                                                                            <input class="form-control" v-model="selected_item.budget_base_amount" id="budget_base_amount" placeholder=""
                                                                                   type="number" :disabled="selected_item.event_amount > 0 ">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="from_date"><i class="fa fa-calendar"></i></label>
                                                                            <label class="control-label" for="from_date">From Date</label>
                                                                            <input class="form-control" v-model="selected_item.from_date" id="from_date" placeholder="" type="date">
                                                                        </div>

                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="to_date"><i class="fa fa-calendar"></i></label>
                                                                            <label class="control-label" for="to_date" >To Date</label>
                                                                            <input class="form-control" v-model="selected_item.to_date" id="to_date" placeholder="" type="date">
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group nomargin">
                                                                        <div class="col-sm-6">
                                                                            <label class="control-label" for="currency"><i class="fa fa-usd"></i></label>
                                                                            <label class="control-label" for="currency">Currency</label>
                                                                            <select class="form-control select_box" v-model="selected_item.currency" required>
                                                                                <option selected value></option>
                                                                                <option>USD</option>
                                                                                <option>EUR</option>
                                                                                <option>GBP</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-6 col-lg-offset-3" style="top:10px;">
                                                                        <button style="border-radius: 10px;font-weight:600;padding: 5px 10px"
                                                                                type="button" class="btn btn-info form-btn" @click="showAddModal=false">Cancel</button>
                                                                        <button style="border-radius: 10px;font-weight:600;padding: 5px 10px"
                                                                                type="button" class="btn btn-info form-btn" @click="onSubmit()">Save</button>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        ------------------------------------------------------------------------------------------------------ / Form   ---------------------------------------------------------------
{{--                        </div>--}}
                    </div>

                </div>
                <!-- / Display Records -->


                <div class="row" style="margin-left: -20px;margin-right: -20px;background: white">
                    <div class="col-md-4 offset-md-4 pages-bar"  v-if="!showAddModal">
                    </div>

                </div>
            </div>
            </div>
        </template>
    </div> <!-- Close App -->
<style>



</style>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.2/axios.min.js"></script>
    <script type="text/javascript" >
      Vue.config.productionTip = false;

      Vue.component('filter-btn', {
        data: function () {
          return {
            count: 0,
            show_element: false,
            input_filter: ''
          }
        },
        props: {
          list: {
            required: true
          },
          ref_name: {
            type: String,
            required: true
          },
          label: {
            type: String,
            required: true
          },
          filter_func: {
            type: Function,
            required: true
          },
          selected_list: {
            type: String,
            required: true
          },
          check_selection: {
            type: Function,
            required: true
          }
        },
        computed : {
          filtered: function() {
            let filtered_list = Object.values(this.list).reduce((acc,curr) => {
              if(!curr || curr.toLowerCase().includes(this.input_filter))
                acc[curr] = curr;
              return acc;
            }, {});
            return filtered_list
          }
        },
        methods: {
          set_focus(name){
            // this.$refs.date_ref.focus();
            this.$refs[name].focus();
          },
          mouse_on(){
            this.show_element = true;
          },
          mouse_off(){
            this.show_element = false;
          }
        },
        template: `<div class="inline-block-display">
                      <div class="dropdown">
                          <button class="btn btn-info float-right" @mouseover=" mouse_on();set_focus(ref_name)" >@{{ label }}</button>
                          <div class="dropdown-content" >
                              <div><input type="text" :ref="ref_name" v-model="input_filter" style="color: black"></div>
                              <div v-for="status in filtered" style="text-align: left" @click="filter_func(selected_list, status)"
                                   :class="check_selection(selected_list,status)?'selected':''" v-if="show_element">
                                  @{{ status }}
                              </div>
                          </div>
                      </div>
                  </div>`
      });

      var app = new Vue({
        el: '#app2',
        data: {
          // ------ Download Table -------
          uri :'data:application/vnd.ms-excel;base64,',
          template:'<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="https://www.w3.org/TR/html40/"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
          base64: function(s){ return window.btoa(unescape(encodeURIComponent(s))) },
          format: function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) },

          // ------ / Download Table -------
          sorted_data: {},
          sort_order: 'desc',
          sort_key: '',
          showAddModal: false,
          data: [],
          selected_index: '',
          group_values: '',
          months: ["Jan", "Feb", "Mar","Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],

          // cms_url: "http://cms.loc/bo/budget_manager",
          cms_url: "http://cms.trafficpointltd.com/bo/budget_manager",

          columns: [],
          brand_group_obj: {}, // Contains brand_group_id, brand_group_name, company_id
          landing_page_obj: {}, // Contains lp_id, lp_name, brand_id, brands_group_name
          form_status: 0,
          parased_budgets: [],
          searchQuery: '',
          total_results: 0,
          {{--devices_list: {!! json_encode($devices) !!},--}}
          dates_list: {!! json_encode($dates_list) !!},
          budget_name_list: {!! json_encode($budget_names) !!},
          group_name_list: {!! json_encode($group_names) !!},
          full_group_name_list: {!! json_encode($full_group_names) !!},
          full_company_name_list: {!! json_encode($full_company_names) !!},
          advertisers_list: {!! json_encode($advertisers) !!},
          budgets: {!! json_encode($budgets) !!} ,
          full_landing_pages: {!! json_encode($landing_pages) !!} ,
          lp_by_brand_group_list: [],

          // Filters
          selected_budget_name: [],
          selected_columns : [],
          selected_group_names: [],
          selected_advertisers: [],

          // Multiple options select
          selected_landing_page_names: [],
          selected_landing_page_ids: [],
          selected_devices: [],
          selected_countries: [],
          selected_dates: [],

          numeric_columns: ['cms_commission_amount','base_commission_amount','diff'],
          update_response: [],
          countries_list: [{symbol:'US',name:'United States of America'},{symbol:'CA',name:'Canada'},{symbol:'CZ',name:'Czechia'},{symbol:'GB',name:'United Kingdom of Great Britain and Northern Ireland'},{symbol:'IT',name:'Italy'},{symbol:'IN',name:'India'},{symbol:'NG',name:'Nigeria'},{symbol:'NL',name:'Netherlands '},{symbol:'AU',name:'Australia'},{symbol:'NZ',name:'New Zealand'},{symbol:'PK',name:'Pakistan'},{symbol:'IE',name:'Ireland'},{symbol:'HN',name:'Honduras'},{symbol:'SA',name:'Saudi Arabia'},{symbol:'BE',name:'Belgium'},{symbol:'IL',name:'Israel'},{symbol:'FR',name:'France'},{symbol:'RO',name:'Romania'},{symbol:'VG',name:'Virgin Islands (British)'},{symbol:'CO',name:'Colombia'},{symbol:'SG',name:'Singapore'},{symbol:'MX',name:'Mexico'},{symbol:'AR',name:'Argentina'},{symbol:'KZ',name:'Kazakhstan'},{symbol:'HK',name:'Hong Kong'},{symbol:'DO',name:'Dominican Republic '},{symbol:'DE',name:'Germany'},{symbol:'IM',name:'Isle of Man'},{symbol:'DK',name:'Denmark'},{symbol:'KW',name:'Kuwait'},{symbol:'PT',name:'Portugal'},{symbol:'TR',name:'Turkey'},{symbol:'RU',name:'Russian Federation '},{symbol:'PH',name:'Philippines '},{symbol:'TW',name:'Taiwan'},{symbol:'CL',name:'Chile'},{symbol:'LU',name:'Luxembourg'},{symbol:'NI',name:'Nicaragua'},{symbol:'SN',name:'Senegal'},{symbol:'PR',name:'Puerto Rico'},{symbol:'IR',name:'Iran'},{symbol:'ES',name:'Spain'},{symbol:'MT',name:'Malta'},{symbol:'PL',name:'Poland'},{symbol:'CN',name:'China'},{symbol:'CY',name:'Cyprus'},{symbol:'KR',name:'Korea'},{symbol:'BR',name:'Brazil'},{symbol:'RS',name:'Serbia'},{symbol:'IQ',name:'Iraq'},{symbol:'CH',name:'Switzerland'},{symbol:'UA',name:'Ukraine'},{symbol:'HR',name:'Croatia'},{symbol:'EG',name:'Egypt'},{symbol:'VN',name:'Viet Nam'},{symbol:'UY',name:'Uruguay'},{symbol:'BO',name:'Bolivia'},{symbol:'RE',name:'Réunion'},{symbol:'CW',name:'Curaçao'},{symbol:'ID',name:'Indonesia'},{symbol:'MY',name:'Malaysia'},{symbol:'GR',name:'Greece'},{symbol:'GE',name:'Georgia'},{symbol:'EC',name:'Ecuador'},{symbol:'NO',name:'Norway'},{symbol:'GT',name:'Guatemala'},{symbol:'BS',name:'Bahamas '},{symbol:'GY',name:'Guyana'},{symbol:'JP',name:'Japan'}
          ],
          devices_list: ['c','m','t'],
          excluded_columns: ['id','brand_group_id','company_id','company_name','advertiser_id','usage','out_clicks','forecast','updated_at','created_at'],
          selected_item: {budget_name:'',brand_group_id:'',brand_group_name:'',company_id:'',company_name:'',advertiser_id:'',advertiser_name:'',
                            landingpage:'',LP_rule:'',landingpage_name:'',page:'',page_rule:'',country:'',country_rule:'',
                            device:'',device_rule:'',event:'',event_amount:'',budget_base_amount:'',currency:'',from_date:'',
                            to_date:'',restrict:''}
        },
        computed: {
          computed_data: function() {
            let total_budget_base_amount = 0;
            let total_event_amount = 0;
            let total_out_clicks = 0;

            let rows =  Object.keys(this.data).reduce((acc,curr_id) => {


              let row_date = new Date(this.data[curr_id].from_date);

              let row_date_month = this.months[row_date.getMonth()];
              let row_date_year = row_date.getFullYear();

              if(
                  (this.selected_budget_name.length == 0       || this.selected_budget_name.includes(this.data[curr_id].budget_name) ||
                      this.selected_budget_name.includes('none') && this.data[curr_id].budget_name == null )&&
                  (this.selected_group_names.length == 0     || this.selected_group_names.includes(this.data[curr_id].brand_group_name) ||
                      this.selected_group_names.includes('none') && this.data[curr_id].brand_group_name == null )&&
                  (this.selected_dates.length == 0           || this.selected_dates.includes(row_date_month + '-' + row_date_year))&&
                  // (this.selected_devices.length == 0          || this.selected_devices.includes(this.userdatas[curr_id].device) ||
                  //     this.selected_devices.includes('none') && this.data[curr_id].device == null )&&
                  (this.selected_advertisers.length == 0     || this.selected_advertisers.includes(this.data[curr_id].advertiser_name))
                  &&
                  (
                  this.searchQuery.length == 0 ||
                      (this.data[curr_id].brand_group_name && this.data[curr_id].brand_group_name.toLowerCase().includes(this.searchQuery))||
                      (this.data[curr_id].budget_name && this.data[curr_id].budget_name.toLowerCase().includes(this.searchQuery))||
                      (this.data[curr_id].advertiser_name && this.data[curr_id].advertiser_name.toLowerCase().includes(this.searchQuery))
                  )
              ){
                // Replace restrict field from 0/1 to No/Yes
                this.data[curr_id].restrict = this.data[curr_id].restrict == '1' || this.data[curr_id].restrict == 'Yes' ? 'Yes' : 'No';
                // Insert current user into reduce's accumulative parameter
                acc[curr_id] = this.data[curr_id];
                // Calculate Total
                total_budget_base_amount += parseInt(this.data[curr_id].budget_base_amount) || 0;
                total_event_amount += parseInt(this.data[curr_id].event_amount) || 0;
                total_out_clicks += parseInt(this.data[curr_id].out_clicks) || 0;
              }
              // Return accumulative parameter
              return acc;
            }, {});
            // Add Total line to the table
            this.total_results = Object.keys(rows).length  || 0 ;

            // Add Total line
            let total_row = {};
            total_row.budget_base_amount = total_budget_base_amount;
            total_row.event_amount = total_event_amount;
            total_row.out_clicks = total_out_clicks;
            total_row.budget_name = 'Total';

            // Sort
            if(this.sort_key !== ''){

              this.sorted_data = Object.entries(rows).sort(function(x,y) {

                // Handle empty cells
                if(!x[1][app.sort_key])
                  x[1][app.sort_key] = Object.values(app.numeric_columns).includes(app.sort_key) ? 0 : '';
                if(!y[1][app.sort_key])
                  y[1][app.sort_key] = Object.values(app.numeric_columns).includes(app.sort_key) ? 0 : '';

                // Compare values
                let res =  x[1][app.sort_key].toString().localeCompare(y[1][app.sort_key].toString(), undefined, {numeric: true, sensitivity: 'base'});

                // Handle desc sort order
                if(app.sort_order === 'desc')
                  res = res * (-1);

                return res;
              });

              rows =  this.sorted_data.reduce((acc,curr) => {
                acc.push(curr[1]);

                if(acc)
                    return acc;
              },[]);
              rows.push(total_row);
            }else{
              Object.assign(rows,{total:total_row});
            }

            return rows;
        },
        },
        mounted: function(){
          this.$nextTick(function () {
            this.getAllData();
          })
        },
        methods: {
          order_list(col_name){
            this.sort_key = col_name;
            this.sort_order = this.sort_order === 'desc' ? 'asc' : 'desc';

          },
          change_company(){
            app.brand_group_obj = { id: '', name:  '',company_id: '' };
            app.selected_item.brand_group_name = '';
            app.selected_item.company_id = app.full_company_name_list.find(row => row.name.trim() === app.selected_item.company_name.trim()).id;

            setTimeout(function(){
              app.lp_by_brand_group_list = app.full_landing_pages.filter(lp=> lp.name != null).sort((a,b)=>(a.name.trim().toLowerCase() > b.name.trim().toLowerCase()) ? 1 : -1);
            }, 250);

          },
          change_brand_group(){
            try{
              // app.selected_item.company_id = app.brand_group_obj.company_id;
              app.selected_item.company_id = '';
              app.selected_item.company_name = '';
              app.selected_item.brand_group_name = app.brand_group_obj.name;

              // --- Old Version - Set company based on the selected brand group ---
              // let company = app.full_company_name_list.find(function(company,index){
              //   if(company.id ===  app.selected_item.company_id) return true;
              // });
              // app.selected_item.company_name = company.name;
            }catch (e) {
              app.selected_item.company_name = '';
            }
            app.selected_landing_page_names = [];
            app.selected_item.landingpage_name = '';

            setTimeout(function(){
              let lp_list;
              if(app.brand_group_obj){
                lp_list = app.full_landing_pages.filter(lp => lp.brands_group_id === app.brand_group_obj.id);
              }else{
                lp_list =  app.full_landing_pages;
              }
              app.lp_by_brand_group_list = lp_list.filter(lp=> lp.name != null).sort((a,b)=>(a.name.trim().toLowerCase() > b.name.trim().toLowerCase()) ? 1 : -1);

              }, 250);
          },
          clear_filters(){
            app.selected_budget_name = [];
            app.selected_group_names = [];
            app.selected_advertisers = [];
            app.selected_dates = [];
            app.searchQuery = '';
            app.sort_key = '';
          },
          tableToExcel(table, name){
            if (!table.nodeType) table = this.$refs.table;
            var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML};
            window.location.href = this.uri + this.base64(this.format(this.template, ctx));
          },
          check_selection(selected_list, item){
            return this[selected_list].includes(item) ;
          },
          onSubmit(){
            if(!app.validate_form())
               return false;
            app.showAddModal = false;
            app.selected_item.brand_group_id = app.brand_group_obj.id;
            let landingpage_ids = [];
            try{
            app.selected_landing_page_names.forEach(lp => {
                landingpage_ids.push(app.lp_by_brand_group_list.find(row => row.name === lp).id); // Vonage (CA) - Nov18
            });
            }catch (e) {
              landingpage_ids = ['Not Found, error : ' + e];
            }
            app.selected_item.landingpage_name =  app.selected_landing_page_names.join();
            app.selected_item.landingpage =  landingpage_ids.join();
            app.selected_item.country = app.selected_countries.join();
            app.selected_item.device = app.selected_devices.join();
            let formData = this.toFormData(app.selected_item);
            if(app.form_status !== 0){  // 0 => New Budget, 1 => Edit
              axios.post(app.cms_url + "/update",formData)
                  .then(function(response){
                    app.update_response = response;
                    app.updateData();
                  });
            }else{
              axios.post(app.cms_url + "/store",formData)
                  .then(function(response){
                    // console.log('Responce on Submit: ' + response);
                    app.update_response = response;
                    if(app.update_response.data.error)
                      alert(app.update_response.data.error);
                    else
                      app.updateData();
                  });
            }
          },
          validate_form(){
            let msg = '';
            if(!app.selected_item.budget_name ){
              msg += 'Fill in Budget Name\n';
            }
            if(!app.selected_item.brand_group_name && !app.selected_item.company_name){
              msg += 'Fill in Brand Group Name or Company Name\n';
            }
            if(!app.selected_item.currency && app.selected_item.budget_base_amount > 0){
              msg += 'Fill in Currency with budget base amount\n';
            }
            if(app.selected_item.currency && app.selected_item.event_amount > 0){
              msg += 'You can not fill in Currency with Event Amount \n';
            }
            if(!app.selected_item.restrict){
              msg += 'Fill in Restrict\n';
            }
            if(!app.selected_item.from_date || !app.selected_item.to_date){
              msg += 'Fill in Dates\n';
            }
            if(!(app.selected_item.event_amount > 0) && !(app.selected_item.budget_base_amount > 0)){
              msg += 'Fill in only one of the following - event_amount or budget base amount\n';
            }
            let colAndRuleList = [{name:'page',rule:'page_rule'},{name:'country',rule:'country_rule'},
                                  {name:'device',rule:'device_rule'},{name:'event',rule:'event_amount'},
                                  {name:'landingpage_name',rule:'LP_rule'}];

            colAndRuleList.forEach(function(param)  {
              if((app.selected_item[param.name] && !app.selected_item[[param.rule]])||(!app.selected_item[param.name] && app.selected_item[[param.rule]])){
                msg += `You have to fill in either ${param.name} and ${param.rule} or none of them.\n`;
              }
            });

            if(msg)
            {
              alert(msg);
              return false;
            }
            return true;
          },
          UpdateByIndex(){
            app.data = app.parased_budgets[app.group_values[app.selected_index]];
              },

          select_filter_param(type, data){
            let index = app[type].indexOf(data);

            if(index >= 0)
              delete app[type][index];
            else
              app[type].push(data);

            app[type] = app[type].filter(function (el) {
              return el != null;
            });
            app[type] = app[type].map(x => { if(x!= null) return x; });

            if(type === 'selected_columns' && index < 0){
              // Order selected columns based on the original order
                if(this.selected_columns.length !== 0){
                  let new_ordered_columns = [];
                  app.columns.forEach(col => {
                    if(app.selected_columns.indexOf( col ) >= 0){
                      new_ordered_columns.push(col);
                    }
                  });
                  app.selected_columns = new_ordered_columns;
                }
            }
            if(type === 'selected_countries' ){
              if(data === ''){
                app[type] = [];
                return;
              }
              if(app[type].length > 1){ // Show Multiple selection label for country select box
                app.selected_item.country = 'Multiple Selection';
              }else if(app[type].length === 1){
                app.selected_item.country = app[type][0];
              }else{
                app.selected_item.country = '';
              }
            }
            if(type === 'selected_devices' ){
              if(data === ''){
                app[type] = [];
                return;
              }
              if(app[type].length > 1){ // Show Multiple selection label for select box
                app.selected_item.device = 'Multiple Selection';
              }else if(app[type].length === 1){
                app.selected_item.device = app[type][0];
              }else{
                app.selected_item.device = '';
              }
            }
            if(type === 'selected_landing_page_names' ){
              if(data === ''){
                app[type] = [];
                return;
              }
              if(app[type].length > 1){ // Show Multiple selection label for select box
                app.selected_item.landingpage_name = 'Multiple Selection';
              }else if(app[type].length === 1){
                app.selected_item.landingpage_name = app[type][0];
              }else{
                app.selected_item.landingpage_name = '';
              }
            }
          },
          delete_budget(budget){
            app.selected_countries = [];
            app.selected_devices = [];
            app.selected_landing_page_names = [];
            for (let key in budget) {
              app.selected_item[key] = budget[key];
            }
            if(confirm("You are going to delete " + app.selected_item.budget_name + "\nAre you sure?")){
              let formData = this.toFormData(app.selected_item);
                axios.post(app.cms_url + "/delete",formData)
                    .then(function(response){
                      app.update_response = response;
                      app.updateData();
                    });
            }
          },
          duplicate_budget(budget){
            new_budget = {...budget};
            new_budget.budget_name += '-1';
            delete new_budget.id;
            this.select_budget(new_budget, 0); // set form status to 0 = new budget
          },
          select_budget(budget, form_status = 1) {
            app.selected_countries = [];
            app.selected_devices = [];
            app.selected_landing_page_names = [];
            app.form_status = form_status; // 1 => Edit, 0 => New Budget
            app.showAddModal = true;
            for (let key in budget) {
              app.selected_item[key] = budget[key];
            }
            // Find the relevant group name object in the list of all the group names
            if( app.selected_item.brand_group_name){
              // Fill brand group object to the edit form
              app.brand_group_obj = app.full_group_name_list.find(row => row.name === app.selected_item.brand_group_name);
            }else{
              app.brand_group_obj = { id: '', name:  '',company_id: '' };
            }

            if(app.selected_item.country){
              app.selected_item.country.split(',').forEach(country => app.selected_countries.push(country));
              app.selected_item.country = app.selected_countries.length > 1 ? 'Multiple Selection' : app.selected_item['country'] ;
            }else{
              app.selected_countries = [];
            }

            if(app.selected_item.device){
              app.selected_item.device.split(',').forEach(device => app.selected_devices.push(device));
              app.selected_item.device = app.selected_devices.length > 1 ? 'Multiple Selection' : app.selected_item['device'] ;
            }else{
              app.selected_devices = [];
            }

            if(app.selected_item.landingpage){
              app.selected_item.landingpage_name.split(',').forEach(lp => app.selected_landing_page_names.push(lp));
              app.selected_item.landingpage_name = app.selected_landing_page_names.length > 1 ? 'Multiple Selection' : app.selected_item['landingpage_name'] ;
            }else{
              app.selected_landing_page_names = [];
            }
            // Filter Landing pages based on brand_group_id
            let lp_list;
            if(app.brand_group_obj.id){
              lp_list = app.full_landing_pages.filter(lp => lp.brands_group_id === app.brand_group_obj.id);
            }else{
              lp_list =  app.full_landing_pages;
            }
            app.landing_page_obj.name = app.selected_item.landingpage_name;
            app.landing_page_obj.id = app.selected_item.landingpage;
            app.lp_by_brand_group_list = lp_list.filter(lp=> lp.name != null).sort((a,b)=>(a.name.trim().toLowerCase() > b.name.trim().toLowerCase()) ? 1 : -1);

          },
          parse_date(date){
            let dd = String(date.getDate()).padStart(2, '0');
            let mm = String(date.getMonth() + 1).padStart(2, '0');
            let yyyy = date.getFullYear();

            return  yyyy + '-' + mm + '-' + dd;
          },
          new_budget() {
            app.brand_group_obj = { id: '', name:  '',company_id: '' };
            app.landing_page_obj = {};
            app.showAddModal = true;
            app.selected_item = {budget_name:'',brand_group_id:'',brand_group_name:'',company_id:'',company_name:'',advertiser_id:'',advertiser_name:'',
              landingpage:'',LP_rule:'',landingpage_name:'',page:'',page_rule:'',country:'',country_rule:'',
              device:'',device_rule:'',event:'',event_amount:'',budget_base_amount:'',currency:'',from_date:'',
              to_date:'',restrict:''};

            // Multiple options select
            app.selected_landing_page_names = [];
            app.selected_landing_page_ids = [];
            app.selected_devices = [];
            app.selected_countries = [];


            let date = new Date();
            app.selected_item.from_date = this.parse_date(new Date(date.getFullYear(), date.getMonth(), 1));
            app.selected_item.to_date = this.parse_date(new Date(date.getFullYear(), date.getMonth() + 1, 0));

            setTimeout(function(){
              app.lp_by_brand_group_list = app.full_landing_pages.filter(lp=> lp.name != null).sort((a,b)=>(a.name.trim().toLowerCase() > b.name.trim().toLowerCase()) ? 1 : -1);
            }, 250);
          },
          updateData(){
            axios.get(app.cms_url +"/getData")
                .then(function(response){
                  app.budgets = response.data;

                  try{
                    app.parased_budgets = app.budgets;
                    app.group_values = Object.keys(app.parased_budgets);
                    app.data = app.parased_budgets; //.slice(2, 20);
                  }catch(err){
                    console.log(err);
                  }
                });
          },
          getAllData() {
                  try{
                        app.parased_budgets = JSON.parse(app.budgets);
                        app.group_values = Object.keys(app.parased_budgets);
                        app.selected_index = app.group_values.length - 1;
                        app.data = app.parased_budgets;
                        app.columns = Object.keys(Object.values(app.data)[0]);
                        app.selected_columns = app.columns.filter( function( el ) {
                          return app.excluded_columns.indexOf( el ) < 0;
                        } );
                  }catch(err){
                        console.log(err);
                  }
          },
          toFormData(obj){
            let fd = new FormData();
            for (const i in obj){
              fd.append(i,obj[i]);
            }
            return fd;
          },
        }
      });

    </script>

@stop

