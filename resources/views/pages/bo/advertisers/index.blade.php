@extends('layouts.metronic.main')

@section('page_content')
    <div class="row">
        <div class="col-md-8">
            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => 'Advertisers',
                'actions' => [
                    auth()->user()->can($permission_name .'.add') ?
                    [
                        'icon'      => 'fa fa-plus',
                        'title'     => 'Add New Advertiser',
                        'target'    => route('bo.advertisers.create')
                    ] : [],
                    [
                        'icon'      => 'fa fa-list',
                        'title'     => 'Show all domains',
                        'target'    => route('bo.advertisers.domains.index')
                    ]
                ]
            ])

                @include('pages.bo.advertisers.filters.table-advertisers', ['permissions' => ['add' => auth()->user()->can($permission_name .'.add')]])

            @include('partials.containers.data-table', [
                'data' => $advertisers,
                'attributes' => [
                    'data-page-length' => 50,
                    'data-order' => '[[1, "asc"]]'
                ],
                'columns' => [
                    [
                        'key' => 'name',
                        'label' => 'Name',
                        'width' => '33%',
                        'filter_type' => 'null',
                        'value' => function ($data) {
                            return "<span class='label'><a href='" . route('bo.advertisers.{advertiser_id}.brands.index', $data->id) . "' class='tooltips' data-original-title='Show brands'>" . $data->name . "</a></span>";
                        }
                    ],
                    [
                        'key' => 'status',
                        'label' => 'Status',
                        'width' => '33%',
                        'value' => function ($data) {
                            if ($data->status == 'active') {
                                return '<span class="label label-sm label-success">Active</span>';
                            } elseif ($data->status == 'inactive'){
                                return '<span class="label label-sm label-danger">Disabled</span>';
                            } else {
                                return '<span class="label label-sm label-warning">Pending</span>';
                            }
                        }
                    ],
                    [
                        'key'   => 'actions',
                        'label' => 'Actions',
                        'width' => '33%',
                        'filter_type' => 'null',
                        'value' => function ($data) {
                            $res  = '';
                            $res .= "<a href='". route('bo.advertisers.{advertiser_id}.brands.index', [$data->id]) ."' title='Show brands' class='tooltips' ><span aria-hidden='true' class='btn btn-default btn-sm'>Show Brands</span></a> ";

                            return $res;
                        }
                    ]
                ],
            ])
            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        </div>
    </div>

@stop
