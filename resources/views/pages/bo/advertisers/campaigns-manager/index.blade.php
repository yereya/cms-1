@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Campaign Manager ',
            'actions' => []
        ])

    {!! Form::open([
        'route' => ['reports.advertisers.campaigns-manager.upload'],
        'method' => 'POST',
        'files' => true,
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data',
    ]) !!}


    <div class="row">
        @include('partials.fields.select', [
            'name' => 'action',
            'label' => 'Action:',
            'required' => true,
            'list' => [
                ['id' => "update", 'text' => "Update"],
            ],
            'value' => 'update'
        ])

        @include('partials.fields.input-upload', [
            'name' => 'file',
            'label' => 'File:',
            'required' => true,
        ])

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'submit' => 'true'
            ]
        ])
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-info">
                <h4 class="block">Fields</h4>
                <h5 class="block">Update</h5>
                <p>Required fields: campaign_id</p>
                <p>Optional fields: segment, country, target_roi, campaign_quality</p>
            </div>
        </div>
    </div>

    <hr>

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop