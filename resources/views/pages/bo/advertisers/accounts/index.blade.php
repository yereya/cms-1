@extends('layouts.metronic.main')

@section('page_title')
    {{ $advertiser->name }}
@endsection

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Accounts',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Account',
                    'target' => route('bo.advertisers.{advertiser_id}.accounts.create', $advertiser->id),
                    'permission' => auth()->user()->can($permission_name .'.add')
                ]
            ]
        ])

    @include('partials.containers.data-table', [
           'data' => $accounts,
           'columns' => [
               [
                   'key' => 'id',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'name',
                   'value' => function ($data) {
                       return $data->name;
                   }
               ],
               [
                   'key' => 'source_account_id',
                   'value' => function ($data) {
                       return $data->source_account_id;
                   }
               ],
               [
                   'key' => 'actions',
                   'filter_type' => 'null',
                   'value' => function ($data) use ($advertiser, $permission_name) {
                       $res = '';
                        if (auth()->user()->can($permission_name .'.edit')) {
                            $res .= "<a href='" . route('bo.advertisers.{advertiser_id}.accounts.edit', [$advertiser->id, $data->id]) . "' class='tooltips' data-original-title='Edit action'><span aria-hidden='true' class='icon-pencil fa-fw'></span></a>";
                       }
                        if (auth()->user()->can($permission_name .'.report_fields.view')) {
                            $res .= "<a href='" . route('bo.advertisers.{advertiser_id}.accounts.{account_id}.report-fields.index', [$advertiser->id, $data->id]) . "' class='tooltips' data-original-title='Report Fields'><span aria-hidden='true' class='icon-list fa-fw'></span></a> ";
                        }
                        if (auth()->user()->can($permission_name .'.block_query.view')) {
                            $res .= "<a href='" . route('bo.advertisers.{advertiser_id}.accounts.{account_id}.block-queries.index', [$advertiser->id, $data->id]) . "' class='tooltips' data-original-title='Show Block Queries'><span class='fa fa-ban fa-fw'></span></a>";
                        }
                        if (auth()->user()->can($permission_name .'.block_ips.view')) {
                            $res .= "<a href='" . route('bo.advertisers.{advertiser_id}.accounts.{account_id}.blocked-ips.index', [$advertiser->id, $data->id]) . "' class='tooltips' data-original-title='Show Blocked IPs'><span class='fa fa-minus-circle fa-fw'></span></a>";
                        }

                       return $res;
                   }
               ]
           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop
