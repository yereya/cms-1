@extends('layouts.metronic.main')

@section('page_title')
    @if (isset($domain))
        {{ $domain->domain }}
    @endif
@endsection

@section('page_content')

@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => isset($domain) ? 'Edit Domain - ' : 'Create Domain',
    'subtitle' => $domain->domain ?? ''
])

    {!! Form::open([
        'route' => isset($domain) ? ['bo.advertisers.domains.update', $domain->id] : ['bo.advertisers.domains.store'],
        'method' => isset($domain) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

        @include('partials.fields.input', [
            'name' => 'domain',
            'label' => 'Domain',
            'required' => true,
            'type' => 'url',
            'value' => isset($domain) ? $domain->domain : null
        ])

        @include('partials.fields.select', [
            'name' => 'status',
            'label' => 'Status',
            'required' => true,
            'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
            'value' => isset($domain) ? strtolower($domain->status) : ''
        ])

        @if(isset($domain))
            @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'custom' => [
                        [
                            'type' => 'submit',
                            'value' => 'Submit',
                            'class' => 'btn btn-primary'
                        ]
                    ]
                ]
            ])
        @else
            @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
        @endif

    {!! Form::close() !!}

@include('partials.containers.portlet', [
    'part' => 'close'
])

@stop


