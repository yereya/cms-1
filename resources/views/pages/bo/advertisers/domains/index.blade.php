@extends('layouts.metronic.main')

@section('page_content')
    <div class="row">
        <div class="col-md-8">
            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => 'Domains',
                'actions' => [
                    [
                        'icon'      => 'fa fa-plus',
                        'title'     => 'Add New Domain',
                        'target'    => route('bo.advertisers.domains.create')
                    ]
                ]
            ])

            @include('partials.containers.data-table', [
                'data' => $domains,
                'attributes' => [
                    'data-page-length' => 50
                ],
                'columns' => [
                    [
                        'key' => 'name',
                        'label' => 'Name',
                        'width' => '33%',
                        'filter_type' => 'null',
                        'value' => function ($data) {
                            return "<span class='label'><a href='" . route('bo.advertisers.domains.edit', $data->id) . "' class='tooltips' data-original-title='Show brands'>" . $data->domain . "</a></span>";
                        }
                    ],
                    [
                        'key' => 'status',
                        'label' => 'Status',
                        'width' => '33%',
                        'value' => function ($data) {
                            if ($data->status == 'active') {
                                return '<span class="label label-sm label-success">Active</span>';
                            } elseif ($data->status == 'inactive'){
                                return '<span class="label label-sm label-danger">Disabled</span>';
                            } else {
                                return '<span class="label label-sm label-warning">Pending</span>';
                            }
                        }
                    ],
                    [
                        'key'   => 'actions',
                        'label' => 'Actions',
                        'width' => '33%',
                        'filter_type' => 'null',
                        'value' => function ($data) {
                            $res  = '';
                            $res .= "<a href='". route('bo.advertisers.domains.edit', [$data->id]) ."' title='Edit domain' class='tooltips' ><span aria-hidden='true' class='btn btn-primary btn-sm'>Edit</span></a> ";
                            $res .= Form::open(['route' => ['bo.advertisers.domains.destroy', $data->id], 'method' => 'delete', 'class' => 'form-invisible tooltips', 'data-original-title' => 'Delete domain']);
                            $res .= '<button type="submit"><span aria-hidden="true" class="btn btn-danger btn-sm">Delete</span></button>';
                            $res .= Form::close();

                            return $res;
                        }
                    ]
                ],
            ])
            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        </div>
    </div>

@stop
