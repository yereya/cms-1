@extends('layouts.metronic.main')

@section('page_title')
    @if (isset($advertiser))
        {{ $advertiser->name }}
    @endif
@endsection

@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($advertiser) ? 'Edit Advertiser - ' . $advertiser->name : 'Create Advertiser'
    ])

        @include('pages.bo.advertisers.advertiser_included_form', ['route_param' => 'advertiser_edit'])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])

@stop


