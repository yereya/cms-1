@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Targets Manager ',
        'actions' => []
    ])

    {!! Form::open([
        'route' => ['reports.advertisers.targets-manager.upload'],
        'method' => 'POST',
        'files' => true,
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data',
    ]) !!}


    <div class="row">
        @include('partials.fields.select', [
            'name' => 'action',
            'label' => 'Action:',
            'required' => true,
            'list' => [
                ['id' => "add", 'text' => "Add"],
                ['id' => "update", 'text' => "Update"],
                ['id' => "remove", 'text' => "Remove"]
            ],
            'value' => 'add'
        ])

        @include('partials.fields.input-upload', [
            'name' => 'file',
            'label' => 'File:',
            'required' => true,
        ])

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'submit' => 'true'
            ]
        ])
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-info">
                <h4 class="block">Fields</h4>
                <h5 class="block">Add</h5>
                <p>Required fields: advertiser_id, month</p>
                <p>Optional fields: advertiser_name, revenue_per_sale, ftd, target_income, target_cost, target_profit, target_cpa</p>

                <hr>
                <h5 class="block">Update</h5>
                <p>Required fields: advertiser_id, month</p>
                <p>Optional fields: advertiser_name, revenue_per_sale, ftd, target_income, target_cost, target_profit, target_cpa</p>

                <hr>
                <h5 class="block">Remove</h5>
                <p>Required fields: advertiser_id</p>
            </div>
        </div>
    </div>

    <hr>


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop