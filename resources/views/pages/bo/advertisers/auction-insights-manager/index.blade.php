@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Auction Insights Manager ',
        ])

    {!! Form::open([
        'route' => ['reports.advertisers.auction-insights-manager.upload'],
        'method' => 'POST',
        'files' => true,
        'class' => 'form-horizontal',
        'role' => 'form',
        'enctype' => 'multipart/form-data',
    ]) !!}


    <div class="row">

        @include('partials.fields.h4-separator', ['label' => 'Settings',])

        <div class="form-group form-horizontal form-md-line-input">
            @include('partials.fields.checkbox', [
                    'wrapper' => true,
                    'label' => 'Options:',
                    'list' => [
                        [
                            'name' => 'save_to_mrr',
                            'label' => 'Save To MRR',
                            'value' => 1,
                            'checked' => 1
                        ]
                    ],
                ])
        </div>

        @include('partials.fields.input-upload', [
            'name' => 'file',
            'label' => 'File:',
            'required' => true,
        ])

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'submit' => 'true'
            ]
        ])
    </div>

    <hr>


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop