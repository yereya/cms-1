{!! Form::open([
    'route' => isset($brand) ? ['bo.advertisers.{advertiser_id}.brands.update', $advertiser->id, $brand->id] : ['bo.advertisers.{advertiser_id}.brands.store', $advertiser->id],
    'method' => isset($brand) ? 'PUT' : 'POST',
    'class' => 'form-horizontal',
    'role' => 'form'
]) !!}

{!! Form::hidden('advertiser_id', $advertiser->id) !!}

{!! Form::hidden('id', isset($brand) ? $brand->id : '') !!}

{!! Form::hidden('route_param', $route_param) !!}

@include('partials.fields.input', [
    'name' => 'name',
    'required' => true,
    'value' => isset($brand) ? $brand->name : ''
])

@include('partials.fields.select', [
    'name' => 'status',
    'label' => 'Status',
    'required' => true,
    'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
    'value' => isset($brand) ? $brand->status : null
])

@include('partials.fields.input', [
    'name' => 'client',
    'label' => 'Client',
    'value' => $brand->client ?? ''
])

@include('partials.fields.select', [
    'name' => 'brand_type',
    'label' => 'Brand Type',
    'list' => ['site_links' => 'Site Links', 'ppc' => 'PPC'],
    'value' => isset($brand) ? $brand->brand_type : null
])

@include('partials.fields.select',[
    'name'  => 'company_id',
    'id'    =>'company_id',
    'required'=>true,
    'label' => 'Company Name',
    'list'  => !empty($companies) ? $companies : [],
    'value' => !empty($brand->company_id)  && !empty($companies[$brand->company_id]) ? $brand->company_id : null
])

@include('partials.fields.select',[
    'name'  => 'brands_group_id',
    'label' => 'Brand Group',
    'required'=>true,
    'ajax' => true,
    'force-selection' => true,
    'value' => isset($brand) ? $brand->brands_group_id : '',
    'list' => !empty($brand_groups) ? $brand_groups : [],
    'attributes' => [
        'data-min_input' => 0,
        'data-dependency_selector[0]' => '#company_id',
        'force_selection'=>true,
        'data-method' => 'getBrandsGroupsByCompany',
        'data-api_url' => route('bo.companies.brands-group-select2'),
    ]
])

@include('partials.fields.input', [
    'name' => 'cpa',
    'label' => 'CPA',
    'value' => isset($brand) && isset($brand->cpa) ? $brand->cpa : ''
])

{{--'class' => 'input-daterange'--}}
@if(isset($brand))
    @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'submit' => 'true'
            ]
        ])
@else
    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
                'submit' => 'true'
            ],
            'checkboxes' => [
                'create_another' => isset($role) ? null : 'true'
            ]
        ])
@endif

{!! Form::close() !!}
