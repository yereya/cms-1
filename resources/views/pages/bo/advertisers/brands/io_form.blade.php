@extends('layouts.metronic.main')

@section('page_title')
    {{ $advertiser->name }}
    @if (isset($io))
        {{ $io->name }}
    @endif
@endsection

@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($io) ? 'Edit Brand - ' : 'Create Brand',
        'subtitle' => $io->name ?? ''
    ])
        @include('pages.bo.advertisers.brands.io_included_form', ['advertiser' => $advertiser, 'brand' => $io ?? null,
         'route_param' => 'brand_edit','companies' => $companies ?? [],'brand_group'=>$brand_group ?? []])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
