@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => "Action links",
    'class' => ''
])

    <div class="form-group row">
        @include('partials.fields.select', [
            'label' => false,
            'id' => 'group-id',
            'no_label' => true,
            'name' => 'group_id',
            'list' => function () use ($actions, $domain, $actions_url_queries, $brand_id) {
                $base_url = $domain . '/track/action/';
                $items = [
                    ['id' => 0, 'text' => 'None', 'data-change_to' => $base_url . '?ioid=' . $brand_id]
                ];

                if ( !($domain ?? false) ) {
                    $base_url = '';
                }

                foreach ($actions as $action) {
                    $params_query = $actions_url_queries[$action->id];
                    $items[] = ['id' => $action->id, 'text' => $action->name, 'data-change_to' => $base_url . $action->type . '/' . $params_query];
                }

                return $items;
            },
            'form_line' => true,
            'column' => 12,
            'column_label' => 0,
            'value' => $current_group->id ?? 0
        ])
    </div>

    <div class="form-group row">
        @include('partials.fields.input', [
            'name' => 'code',
            'label' => false,
            'placeholder' => 'Code',
            'form_line' => true,
            'column' => 12,
            'column_label' => 0,
            'value' => $domain . '/track/action/' . (array_keys($actions)[0] ?? '') . '/&'
        ])
    </div>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'modal_width' => '900'
    ]
])

<script type="text/javascript">
    (function () {
        var changeText = function (settings) {
            var defaults = {
                'options': 'select[name="group_id"]',
                'target_text': '[name="code"]'
            }

            function setDefaultParameters(default_params, settings) {
                $.extend(default_params, settings);
            }

            function attachEvents() {
                var $select = $(defaults.options),
                    $target_text = $(defaults.target_text);

                $select.on("change", function () {
                    $($select).find('option:selected').each(function (value, option) {
                        var text = $(option).data('change_to');
                        $target_text.attr('value', text);
                    });
                });
            }

            function initialExecution() {
                var $initial_option = $(defaults.options);
                $initial_option.trigger('change');
            }

            return {
                init: function () {
                    setDefaultParameters(defaults, settings);
                    attachEvents();
                    initialExecution();
                }
            }
        }

        $(document).ready(function () {
            changeText().init();
        });
    })();
</script>