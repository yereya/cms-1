@extends('layouts.metronic.main')

@php
    session(['advertiser' => $advertiser]);
@endphp

@section('page_title')
    {{ $advertiser->name }}
@endsection

@section('page_content')
    <div class="row">

    </div>
    <div class="col-md-12 col-lg-8">
        <div class="col-md-12">
            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => "Brands - ",
                'subtitle' => $advertiser->name,
                'tools' => [],
                'actions' => [
                    [
                        'icon' => 'fa fa-plus',
                        'title' => 'Add New Brand',
                        'target' => route('bo.advertisers.{advertiser_id}.brands.create', $advertiser->id)
                    ]
                ]
            ])

            @include('pages.bo.advertisers.brands.filters.table-brands', ['permissions' => ['add' => auth()->user()->can($permission_name .'.add')]])

            @include('partials.containers.data-table', [
               'data' => $ios,
               'attributes' => [
                   'data-page-length' => 50
               ],
               'columns' => [
                   [
                       'key' => 'name',
                       'width' => '33%',
                       'value' => function ($data) {
                           return "<span class='label'><a href='" . route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index', [$data->advertiser_id, $data->id]) . "'>$data->name</a></span>";
                       }
                   ],
                   [
                       'key' => 'status',
                       'width' => '33%',
                       'value' => function ($data) {
                            $status = strtolower($data->status);
                            $class = 'label-success';

                            if ($status == 'inactive') {
                                $class = 'label-danger';
                            } elseif ($status == 'pending') {
                                $class = 'label-warning';
                            }

                            return "<span class='label label-sm $class'>$status</span>";
                       }
                   ],
                   [
                       'key' => 'actions',
                       'width' => '33%',
                       'filter_type' => 'null',
                       'value' => function ($data) use ($advertiser) {
                           $res = '';

                           $res .= "<a href='" . route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index', [$data->advertiser_id, $data->id]) . "' class='tooltips' data-original-title='Show campaigns'><span class='btn btn-default btn-sm'>Show campaigns</span></a>";
                           if (!empty($data)) {
                                session(['brand_id' => $data->id]);

                                $res .= "<a href='"
                                    .route('bo.advertisers.{advertiser_id}.brands.ajax-modal',
                                     [$advertiser->id, 'brand_id' => $data->mongodb_id, 'method' => 'actionLink', 'actions' => $data->actions ?? null, 'domain' => $advertiser->url ?? ''])
                                     ."' class='tooltips' data-toggle='modal' data-target='#ajax-modal' data-original-title='Pixel'><span class='btn btn-primary btn-sm'>Pixel</span></a>";
                           }

                           return $res;
                       }
                   ],
               ]
            ])

            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        </div>
    </div>

    <div class="col-md-12 col-lg-4">
        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Edit Advertiser - ',
            'subtitle' => $advertiser->name,
            'has_form' => true,
            'tools' => [
                'collapse' => isset($advertiser) ? ['class' => 'collapse'] : ['class' => ''],
            ]
        ])

            @include('pages.bo.advertisers.advertiser_included_form', ['advertiser' => $advertiser, 'route_param' => 'brands_index'])

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])
    </div>
@stop