@extends('layouts.metronic.main')

@section('page_title')
    <small>Advertiser</small> <small>Brand</small> <small>Campaign</small>
    @if (isset($campaign))
        {{ $campaign->name }}
    @endif
@endsection

@section('page_content')



    @if (!empty($campaign))
        <div class="col-md-8">
            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => "Landing Pages - " . $campaign->name,
                'actions' => [
                    [
                        'icon' => 'fa fa-plus',
                        'title' => 'Add New Landing Page',
                        'target' => route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.create', [$advertiser->id, $brand->id, $campaign->id])
                    ]
                ]
            ])


                @include('partials.containers.data-table', [
                   'data' => $campaign->landing_pages,
                   'columns' => [
                       [
                           'key' => 'name',
                           'value' => function ($data) {
                               return $data->name;
                           }
                       ],
                       [
                           'key' => 'status',
                           'value' => function ($data) {
                                $status = $data->status ?? 'active';
                                $class = 'label-success';

                                if ($data->status == 'inactive') {
                                    $class = 'label-danger';
                                } elseif ($data->status == 'pending') {
                                    $class = 'label-warning';
                                }

                                return "<span class='label label-sm $class'>$status</span>";
                           }
                       ],
                       [
                           'key' => 'url',
                           'value' => function ($data) {
                               return strtoupper($data->url);
                           }
                       ],
                       [
                           'key' => 'weight',
                           'value' => function ($data) {
                               return strtoupper($data->weight);
                           }
                       ],
                       [
                           'key' => 'actions',
                           'filter_type' => 'null',
                           'value' => function ($data) use ($advertiser, $brand, $campaign) {
                               $res = '';

                               $res .= "<a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.edit', [$advertiser->id, $brand->id, $campaign->id, $data->id])."' class='tooltips' data-original-title='Edit landing page'><span aria-hidden='true' class='btn btn-primary'>Edit</span></a>";
                               $res .= Form::open(['route' => ['bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.destroy', $advertiser->id, $brand->id, $campaign->id, $data->id], 'method' => 'delete', 'class' => 'form-invisible tooltips', 'data-original-title' => 'Delete landing page']);
                               $res .= '<button type="submit"><span aria-hidden="true" class="btn btn-danger">Delete</span></button>';
                               $res .= Form::close();

                               return $res;
                           }
                       ],
                   ]
                ])
            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        </div>
    @else


    @endif

    <div class="col-md-{{ !empty($campaign) ? '4' : '8' }}">
        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => !empty($campaign) ? 'Edit Campaign - ' . $campaign->name: 'Create Campaign'
        ])

            @include('pages.bo.advertisers.brands.campaign.campaign_included_form', ['route_param' => 'campaign_edit'])

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])
    </div>
@stop