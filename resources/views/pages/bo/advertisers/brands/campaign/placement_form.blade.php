@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => "Placement variables and information",
    'ajax_submit' => true,
    'class' => ''
])

    <div class="row">
            {!! Form::open([
                'route' => ['bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.groups',
                    $advertiser_id, $brand_id, $campaign_id],
                'method' => 'POST',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]) !!}

                {!! Form::hidden('advertiser_id', $advertiser_id) !!}
                {!! Form::hidden('brand_id', $brand_id) !!}
                {!! Form::hidden('campaign_id', $campaign_id) !!}

                <div class="form-group">
                    @include('partials.fields.select', [
                        'label' => false,
                        'id' => 'group-id',
                        'no_label' => true,
                        'name' => 'group_id',
                        'list' => function () use ($groups, $domain, $placement, $groups_url_queries) {
                            $base_url = $domain . '/track/click/?pid=' . $placement;
                            $items = [
                                ['id' => 0, 'text' => 'None', 'data-change_to' => $base_url]
                            ];

                            if ( !($domain ?? false) || !($placement ?? false) ) {
                                $base_url = '';
                            }

                            foreach ($groups as $group) {
                                $params_query = $groups_url_queries[$group->id];
                                $items[] = ['id' => $group->id, 'text' => $group->name, 'data-change_to' => $base_url . '&' . $params_query];
                            }

                            return $items;
                        },
                        'form_line' => true,
                        'column' => 12,
                        'column_label' => 0,
                        'value' => $current_group->id ?? 0
                    ])

                </div>

                <div class="form-group">
                    @include('partials.fields.input', [
                        'name' => 'code',
                        'label' => false,
                        'placeholder' => 'Code',
                        'form_line' => true,
                        'column' => 12,
                        'column_label' => 0,
                        'value' => $domain . '/track/click/?pid=' . $placement . '&'
                    ])
                </div>


                {!! Form::close() !!}

    </div>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true,
        'save' => true
    ],
    'js' => [
        'modal_width' => '900'
    ]
])

<script type="text/javascript">
    (function () {
        var changeText = function (settings) {
            var defaults = {
                'options': 'select[name="group_id"]',
                'target_text': '[name="code"]'
            }

            function setDefaultParameters(default_params, settings) {
                $.extend(default_params, settings);
            }

            function attachEvents() {
                var $select = $(defaults.options),
                    $target_text = $(defaults.target_text);

                $select.on("change", function () {
                    $($select).find('option:selected').each(function (value, option) {
                        var text = $(option).data('change_to');
                        var decodedText = decodeURI(text);
                        $target_text.attr('value', decodedText);
                    });
                });
            }

            function initialExecution() {
                var $initial_option = $(defaults.options);
                $initial_option.trigger('change');
            }

            return {
                init: function () {
                    setDefaultParameters(defaults, settings);
                    attachEvents();
                    initialExecution();
                }
            }
        }

        $(document).ready(function () {
            changeText().init();
        });
    })();
</script>