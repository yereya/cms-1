{!! Form::open([
        'route' => isset($campaign) ?
            ['bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.update', $advertiser->id, $brand->id, $campaign->id] :
            ['bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.store', $advertiser->id, $brand->id],
        'method' => isset($campaign) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}
    {!! Form::hidden('advertiser_id', $advertiser->id) !!}

    {!! Form::hidden('brand_id', $brand->id) !!}

    {{ Form::hidden('route_param', $route_param) }}

    @include('partials.fields.input', [
        'name' => 'name',
        'required' => true,
        'value' => isset($campaign) ? $campaign->name : ''
    ])

    @include('partials.fields.select', [
        'name' => 'status',
        'label' => 'Status',
        'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
        'value' => isset($campaign) ? $campaign->status : null
    ])

    @include('partials.fields.input', [
        'name' => 'timezone',
        'label' => 'Timezone',
        'required' => true,
        'value' => $campaign->timezone ?? '+00:00',
        'disabled' => $campaign->timezone ?? false
    ])

    @if(!isset($campaign))

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])

        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => '*Initial landing page'
        ])

        @include('partials.fields.input', [
                'name' => 'lp_name',
                'label' => 'Name',
                'required' => true,
                'value' => ''
            ])

        @include('partials.fields.select', [
            'name' => 'lp_status',
            'label' => 'Status',
            'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
            'value' => 'Active'
        ])

        @include('partials.fields.input', [
            'name' => 'lp_url',
            'label' => 'url',
            'required' => true,
            'type' => 'url',
            'value' => ''
        ])
                @include('partials.fields.select', [
            'name' => 'appendix',
            'label' => 'appendix',
            'list' => ['google' => 'Google', 'taboola' => 'Taboola', 'msn' => 'MSN', 'facebook' => 'Facebook'],
        ])

        @include('partials.fields.input', [
            'name' => 'lp_page',
            'label' => 'page',
            'required' => false,
            'value' => ''
        ])

        @include('partials.fields.input', [
            'name' => 'lp_weight',
            'label' => 'Weight',
            'group' => true,
            'group_class' => 'fa fa-user',
            'value' => 100
        ])


        {{--    POPUP Component Start--}}
        <style>
            .modal-dialog{
                position: fixed;
                top: 25%;
                left: 25%;
                z-index: 999;
            }
            .modal-header{
                color: #dc3545;
            }
            .message-warning{
                font-size: 22px;
                font-weight: 400;
            }
        </style>

        <div class="modal-dialog">
            <div class="modal-content ">
                <div class="modal-header danger">Warning</div>
                <div class="modal-body">
                    <h3 class="message-warning"></h3>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-lg btn-danger ignore">Save</button>
                    <button type="button" class="btn btn-lg btn-primary check" >Edit</button>
                </div>
            </div>
        </div>

        <script  type="text/javascript" >
            $(".modal-dialog").hide();
            $(".form-horizontal").submit(function(e){
                e.preventDefault();
                const form = this;
                let url = $("#lp_url").val();
                if (!checkAtLeastOneTagExist(url)  ||  !OpenCloseBracesValidation(url) ||  !WebsiteValidationURL(url)) {
                    $(".modal-dialog").fadeIn(150);

                    $(".message-warning").text("The tracking parameters are missing or set incorrectly. Would you like to save anyway?")
                    $(".ignore").unbind().click(function (){
                        form.submit();
                        $(".modal-dialog").fadeOut(150)
                    })
                    $(".check").unbind().click(function (){ $(".modal-dialog").fadeOut(150)})
                }
                else{
                    form.submit();
                }
            })

            function checkAtLeastOneTagExist(url){
                return (url.match(/({gclid}|{token}|{msclkid}|\{\{fbclid\}\})/) || []).length > 0
            }

            function OpenCloseBracesValidation(url){
                return (url.match(/\{/gm) || []).length === (url.match(/\}/gm) || []).length;
            }

            function WebsiteValidationURL(url){
                switch ((url.match(/({gclid}|{msclkid}|\{\{fbclid\}\})/) || [])[0]){
                    case '\{\{fbclid\}\}':
                        return (url.match(/\{\{fbclid\}\}/gm) || []).length >= 2;
                        break;
                    case '{gclid}':
                        return (url.match(/{gclid}/) || []).length >= 1;
                        break;
                    case '{msclkid}':
                        return (url.match(/{msclkid}/) || []).length >= 1;
                        break;
                    default:
                        return true;
                        break;

                }
            }

            $( window ).load(function() {
                $("[name=appendix]").bind("change", function(e) {
                    console.log(e.target.value)
                    let url = $("#lp_url");
                    let appendix = e.target.value;
                    let toSlice = url.val().indexOf('?') > 0;
                    switch (appendix){
                        case '':
                            url.val((toSlice ? url.val().slice(0,url.val().indexOf('?')) : url.val())+"")
                            break;
                        case 'taboola':
                            url.val((toSlice ? url.val().slice(0,url.val().indexOf('?')) : url.val())+"?utm_medium=cpc&utm_source=taboola&gclid={click_id}")
                            break;
                        case 'google':
                            url.val((toSlice ? url.val().slice(0,url.val().indexOf('?')) : url.val())+"?gclid={gclid}")
                            break;
                        case 'msn':
                            url.val((toSlice ? url.val().slice(0,url.val().indexOf('?')) : url.val())+"?utm_medium=cpc&utm_source=bing&gclid={msclkid}")
                            break;
                        case 'facebook':
                            url.val((toSlice ? url.val().slice(0,url.val().indexOf('?')) : url.val())+`?utm_source=facebook&utm_medium=cpc&fbclid=\{\{fbclid\}\}&gclid=\{\{fbclid\}\}`)
                            break;
                        default:
                            break;
                    }
                })
            })
        </script>

        {{--    POPUP Component End--}}


        <div class="dynamic_values_container">
            @include('partials.fields.h4-separator', ['label' => 'Dynamic Values'])

            @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                @include('partials.fields.select', [
                    'name' => 'lp_dynamic[key][]',
                    'no_label' => true,
                    'label' => '',
                    'list' => ['DynamicP' => 'DynamicP'],
                    'form_line' => true,
                    'column' => 5,
                    'column_label' => 0,
                    'value' => ''
                ])

                @include('partials.fields.input', [
                    'name' => 'lp_dynamic[value][]',
                    'label' => false,
                    'placeholder' => 'Value',
                    'form_line' => true,
                    'column' => 5,
                    'column_label' => 0,
                    'value' => ''
                ])

                @include('partials.containers.actions', [
                    'class' => true,
                    'actions' => [
                        [
                            'action' => 'clone',
                        ],
                        [
                            'action' => 'remove',
                        ]
                    ]
                ])

            @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
        </div>

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])

    @endif
    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'submit' => 'true'
        ]
    ])
{!! Form::close() !!}