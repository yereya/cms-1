@extends('layouts.metronic.main')

@section('page_title')
    <small>Landing pages</small>
@endsection

@section('page_content')
    <div class="col-md-8">
        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Landing pages - ',
            'subtitle' => $campaign->name,
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Landing Page',
                    'target' => route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.create', [$advertiser->id, $brand->id, $campaign->id])
                ]
            ]
        ])

        @include('partials.containers.data-table', [
           'data' => $landings,
           'attributes' => [
               'data-page-length' => 50
           ],
           'columns' => [
               [
                   'key' => 'name',
                   'value' => function ($data) {
                       return "<span class='label' style='color:black;'>$data->name</span>";
                   }
               ],
               [
                   'key' => 'url',
                   'value' => function ($data) {
                       return $data->url;
                   }
               ],
               [
                   'key' => 'status',
                   'value' => function ($data) {
                        $status = !empty($data->status) ? $data->status : 'active';
                        $class = 'label-success';

                        if ($data->status == 'inactive') {
                            $class = 'label-danger';
                        } elseif ($data->status == 'pending') {
                            $class = 'label-warning';
                        }

                        return "<span class='label label-sm $class'>$status</span>";
                   }
               ],
               [
                   'key' => 'weight',
                   'value' => function ($data) {
                       return strtoupper($data->weight);
                   }
               ],
               [
                   'key' => 'actions',
                   'filter_type' => 'null',
                   'value' => function ($data) use ($advertiser, $brand, $campaign) {
                       $res = '';

                       $res .= "<a href='".route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.edit', [$advertiser->id, $brand->id, $campaign->id, $data->id])."' class='tooltips' data-original-title='Edit landing page'><span aria-hidden='true' class='btn btn-primary btn-sm icon-pencil'></span></a>";
                       /*
                       $res .= Form::open(['route' => ['bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.destroy', $advertiser->id, $brand->id, $campaign->id, $data->id], 'method' => 'delete', 'class' => 'form-invisible tooltips', 'data-original-title' => 'Delete landing page']);
                       $res .= '<button type="submit"><span aria-hidden="true" class="btn btn-danger btn-sm">Delete</span></button>';
                       $res .= Form::close();
                        */
                       return $res;
                   }
               ],
           ]
        ])
        @include('partials.containers.portlet', [
            'part' => 'close'
        ])
    </div>

    <div class="col-md-4">
        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Edit Campaign - ',
            'subtitle' => $campaign->name,
            'has_form' => true,
            'tools' => [
                'collapse' => isset($advertiser) ? ['class' => 'collapse'] : ['class' => ''],
            ]
        ])

            @include('pages.bo.advertisers.brands.campaign.campaign_included_form', ['route_param' => 'landing_index'])

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])
    </div>
@stop