<div class="row table-filters">
    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'Status',
            'label' => 'Status',
            'column' => 4,
            'column_label' => 4,
            'column_input' => 8,
            'class' => 'filter_select_datatable',
            'list' => ['active' => 'Active', 'inactive' => 'Inactive'],
            'value' => '',
            'attributes' => [
                'data-column_id' => 1
            ]
        ])
    </div>
</div>
