@extends('layouts.metronic.main')

@section('page_title')
    <small>Advertiser</small> <small>Brand</small> <small>Group</small> <small>Parameters </small>
@endsection

@section('page_content')
    <div class="col-md-12">
        {!! Form::open([
            'url' => isset($group) ? route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaign-groups.update', [$advertiser->id, $brand->id, $group->id])
                : route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaign-groups.store', [$advertiser->id, $brand->id]) ,
            'method' => isset($group) ? 'PUT' : 'POST',
            'class' => 'form-horizontal'
        ]) !!}

            {!! Form::hidden('advertiser_id', $advertiser->id) !!}
            {!! Form::hidden('brand_id', $brand->id) !!}

            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => 'Group'
            ])

                @include('partials.fields.input', [
                    'name' => 'name',
                    'label' => 'Name',
                    'value' => $group->name ?? ''
                ])

            @include('partials.fields.select', [
                'name' => 'status',
                'label' => 'Status',
                'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
                'value' => isset($brand) ? $brand->status : null
            ])

            @include('partials.containers.portlet', [
                'part' => 'close'
            ])


        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'Group parameters'
        ])

            <div class="dynamic_values_container">
                @include('partials.fields.h4-separator', ['label' => 'Group parameters',])

                @if(isset($group->group_parameters) && count($group->group_parameters) > 0)
                    @foreach($group->group_parameters as $param)
                        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                        @include('partials.fields.input', [
                            'name' => 'group_params[key][]',
                            'no_label' => true,
                            'placeholder' => 'Key',
                            'group' => false,
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $param->key
                        ])

                        @include('partials.fields.input', [
                            'name' => 'group_params[value][]',
                            'label' => false,
                            'placeholder' => 'Value',
                            'group' => false,
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $param->value
                        ])

                        @include('partials.containers.actions', [
                            'class' => true,
                            'actions' => [
                                [
                                    'action' => 'clone',
                                ],
                                [
                                    'action' => 'remove',
                                ]
                            ]
                        ])

                        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                    @endforeach
                @else
                    @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                    @include('partials.fields.input', [
                        'name' => 'group_params[key][]',
                        'no_label' => true,
                        'placeholder' => 'Key',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => ''
                    ])

                    @include('partials.fields.input', [
                        'name' => 'group_params[value][]',
                        'label' => false,
                        'placeholder' => 'Value',
                        'form_line' => true,
                        'column' => 5,
                        'column_label' => 0,
                        'value' => ''
                    ])

                    @include('partials.containers.actions', [
                            'class' => true,
                            'actions' => [
                                [
                                    'action' => 'clone',
                                ],
                                [
                                    'action' => 'remove',
                                ]
                            ]
                        ])

                    @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                @endif
            </div>

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])


        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => URL::previous()
                ],
                'submit' => [
                    'route' => back_url()
                ]
            ]
        ])
        {!! Form::close() !!}
    </div>
@stop