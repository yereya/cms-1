@extends('layouts.metronic.main')

@section('page_title')
    <small>Advertiser</small> <small>Pixel</small> <small>Parameters</small>
@endsection

@section('page_content')
    <div class="col-md-12">
        {!! Form::open([
            'url' => isset($action) ? route('bo.advertisers.{advertiser_id}.brands.{brand_id}.actions.update', [$advertiser->id, $brand->id, $action->id])
                : route('bo.advertisers.{advertiser_id}.brands.{brand_id}.actions.store', [$advertiser->id,  $brand->id]) ,
            'method' => isset($action) ? 'PUT' : 'POST',
            'class' => 'form-horizontal'
        ]) !!}

            {!! Form::hidden('advertiser_id', $advertiser->id) !!}
            {!! Form::hidden('brand_id', $brand->id) !!}

            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => 'pixel'
            ])

            @include('partials.fields.input', [
                'name' => 'name',
                'required' => true,
                'label' => 'Name',
                'value' => $action->name ?? ''
            ])

            @include('partials.fields.select', [
                'name' => 'status',
                'label' => 'Status',
                'required' => true,
                'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
                'value' => $action->status ?? null
            ])

            @include('partials.fields.select', [
                'name' => 'type',
                'label' => 'Type',
                'required' => true,
                'list' => ['install' => 'Install', 'lead' => 'Lead', 'sale' => 'Sale'],
                'value' => $action->type ?? null
            ])

            @include('partials.fields.checkbox', [
                'label' => 'Is Unique',
                'wrapper' => true,
                'list' => [
                    [
                        'name' => 'is_unique',
                        'value' => 1,
                        'checked' => $action->is_unique ?? 0
                    ],
                ],
            ])

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])

        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => 'action parameters'
        ])

            <div class="dynamic_values_container">
                @include('partials.fields.h4-separator', ['label' => 'Pixel parameters',])

                @if(isset($action->parameters) && count($action->parameters) > 0)
                    @foreach($action->parameters as $param)
                        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

                        @include('partials.fields.input', [
                            'name' => 'action_params[key][]',
                            'no_label' => true,
                            'placeholder' => 'Key',
                            'action' => false,
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $param->key
                        ])

                        @include('partials.fields.input', [
                            'name' => 'action_params[value][]',
                            'label' => false,
                            'placeholder' => 'Value',
                            'action' => false,
                            'form_line' => true,
                            'column' => 5,
                            'column_label' => 0,
                            'value' => $param->value
                        ])

                        @include('partials.containers.actions', [
                            'class' => true,
                            'actions' => [
                                [
                                    'action' => 'clone',
                                ],
                                [
                                    'action' => 'remove',
                                ]
                            ]
                        ])

                        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                    @endforeach
                @else

                    @foreach(['commission_amount', 'token', 'currency'] as $key)
                        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])
                            @include('partials.fields.input', [
                                'name' => 'action_params[key][]',
                                'no_label' => true,
                                'placeholder' => 'Key',
                                'form_line' => true,
                                'column' => 5,
                                'column_label' => 0,
                                'value' => $key
                            ])

                            @include('partials.fields.input', [
                                'name' => 'action_params[value][]',
                                'label' => false,
                                'placeholder' => 'Value',
                                'form_line' => true,
                                'column' => 5,
                                'column_label' => 0,
                                'value' => ''
                            ])

                            @include('partials.containers.actions', [
                                'class' => true,
                                'actions' => [
                                    [
                                        'action' => 'clone',
                                    ],
                                    [
                                        'action' => 'remove',
                                    ]
                                ]
                            ])
                        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
                    @endforeach
                @endif
            </div>

        @include('partials.containers.portlet', [
            'part' => 'close'
        ])

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => URL::previous()
                ],
                'submit' => [
                    'route' => back_url()
                ]
            ]
        ])
        {!! Form::close() !!}
    </div>
@stop