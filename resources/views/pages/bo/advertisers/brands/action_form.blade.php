@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($action) ? 'Update Action' : 'Create new Action'
    ])

    {!! Form::open([
            'route' => isset($action) ? ['bo.advertisers.{advertiser_id}.brands.actions.update', $advertiser['id'], $action['id']] : ['bo.advertisers.{advertiser_id}.brands.actions.store', $advertiser['id']],
            'method' => isset($action) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    {!! Form::hidden('advertiser_id', $advertiser->id) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'value' => isset($action) ? $action['name'] : null
        ])

    @include('partials.fields.select', [
        'name' => 'status',
        'label' => 'Status',
        'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
        'value' => isset($action) ? $action['status'] : null
    ])

    <div class="form-group form-horizontal form-md-line-input">
        <label class="col-md-2 control-label" for="form_control_budget_type">Unique</label>

        <div class="col-md-3">
                <div class="checkbox-list">
                    <label class="checkbox-inline">
                        <input type="checkbox" name="unique" id="form_control_active" class="form-control" {{ isset($action) && $action['unique'] ? 'checked="checked"' : null }}>
                    </label>
                </div>
        </div>
    </div>

    <div class="form-group form-horizontal form-md-line-input">
        <label class="col-md-2 control-label" for="form_control_budget_type">Parameters</label>

        <div class="col-md-2">
            <label class="col-md-7 control-label">Currency</label>
            <div class="checkbox-list">
                <label class="checkbox-inline">
                    <input type="checkbox" name="currency" id="form_control_active" class="form-control" {{ isset($action) && isset($action['currency']) && $action['currency'] ? 'checked="checked"' : null }}>
                </label>
            </div>
        </div>

        <div class="col-md-2">
            <label class="col-md-7 control-label">Amount</label>
            <div class="checkbox-list">
                <label class="checkbox-inline">
                    <input type="checkbox" name="amount" id="form_control_active" class="form-control" {{ isset($action) && isset($action['amount']) && $action['amount'] ? 'checked="checked"' : null }}>
                </label>
            </div>
        </div>

        <div class="col-md-2">
            <label class="col-md-7 control-label">Commission amount</label>
            <div class="checkbox-list">
                <label class="checkbox-inline">
                    <input type="checkbox" name="commission_amount" id="form_control_active" class="form-control" {{ isset($action) && isset($action['commission_amount']) && $action['commission_amount'] ? 'checked="checked"' : null }}>
                </label>
            </div>
        </div>

        <div class="col-md-2">
            <label class="col-md-7 control-label">trxId</label>
            <div class="checkbox-list">
                <label class="checkbox-inline">
                    <input type="checkbox" name="trxId" id="form_control_active" class="form-control" {{ isset($action) && isset($action['trxId']) && $action['trxId'] ? 'checked="checked"' : null }}>
                </label>
            </div>
        </div>

        <div class="col-md-2">
            <label class="col-md-7 control-label">brandId</label>
            <div class="checkbox-list">
                <label class="checkbox-inline">
                    <input type="checkbox" name="brandId" id="form_control_active" class="form-control" {{ isset($action) && isset($action['brandId']) && $action['brandId'] ? 'checked="checked"' : null }}>
                </label>
            </div>
        </div>
    </div>

    {{--'class' => 'input-daterange'--}}
    @if(isset($action))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop