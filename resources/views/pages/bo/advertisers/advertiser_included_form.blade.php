{!! Form::open([
    'route' => isset($advertiser) ? ['bo.advertisers.update', $advertiser->id] : ['bo.advertisers.store'],
    'method' => isset($advertiser) ? 'PUT' : 'POST',
    'class' => 'form-horizontal',
    'role' => 'form'
]) !!}

    {!! Form::hidden('route_param', $route_param) !!}

    @include('partials.fields.input', [
        'name' => 'name',
        'label' => 'Name',
        'required' => true,
        'value' => isset($advertiser) ? $advertiser->name : null
    ])

    @include('partials.fields.select', [
        'name' => 'status',
        'label' => 'Status',
        'required' => true,
        'list' => ['active' => 'Active', 'inactive' => 'Inactive', 'pending' => 'Pending'],
        'value' => isset($advertiser) ? strtolower($advertiser->status) : ''
    ])

    @include('partials.fields.select', [
        'name' => 'url',
        'type' => 'url',
        'label' => 'Domain',
        'list' =>  collect($domains)
                    ->prepend(['domain' => $advertiser->url ?? ''])
                    ->pluck('domain', 'domain'),
        'required' => true,
        'value' => $advertiser->url ?? -1
    ])

    @include('partials.fields.input', [
        'name' => 'timezone',
        'label' => 'Timezone',
        'required' => true,
        'value' => $advertiser->timezone ?? '+00:00',
        'disabled' => $advertiser->timezone ?? false
    ])

    @if(isset($advertiser))
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'custom' => [
                    [
                        'type' => 'submit',
                        'value' => 'Submit',
                        'class' => 'btn btn-primary'
                    ]
                ]
            ]
        ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                'submit' => 'true'
            ],
            'checkboxes' => [
                'create_another' => isset($role) ? null : 'true'
            ]
        ])
    @endif

{!! Form::close() !!}