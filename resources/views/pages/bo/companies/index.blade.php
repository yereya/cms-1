@extends('layouts.metronic.main')


@section('page_content')
    @include('partials.containers.portlet', [
           'part' => 'open',
            'title' => 'Companies',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Company',
                    'target' => route('bo.companies.create'),
                    'permission' => auth()->user()->can($permission_name .'.add')
                ]
            ]
        ])

    @include('partials.containers.data-table', [
           'data' => $companies,
           'columns' => [
               [
                   'key' => 'id',
                   'width'=>'2%',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'name',
                   'width'=>'20%',
                   'value' => function ($data) use ($permission_name) {
                        $res = $data->name;
                        if (auth()->user()->can("bo.companies.brands_groups.view")) {
                            $res = "<a href='" . route('bo.companies.brands-groups.index', [$data->id]) . "'  '' title='Enter Brand Group'>$data->name</a>";
                       }

                       return $res;
                   }
               ],
               [
                   'key' => 'product_description',
                   'value'=> function($data){
                   return $data->product_description;
                   }
               ],
               [
                   'key' => 'SAP_id',
                   'value'=> function($data){
                   return $data->SAP_id;
                   }
               ],
               [
                 'key' => 'actions',
                 'width'=>'1%',
                 'value'=>function($data) use ($permission_name){
                    $res = '';
                        if (auth()->user()->can("{$permission_name}.edit")) {
                            $res .= "<a href='" . route('bo.companies.edit', [$data->id]) . "' class='tooltips' data-original-title='Edit company'><span aria-hidden='true' class='icon-pencil fa-fw'></span></a>";
                       }

                       return $res;
                 }
               ]

           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop
