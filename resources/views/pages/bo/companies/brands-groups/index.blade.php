@extends('layouts.metronic.main')
@section('page_content')
    @include('partials.containers.portlet', [
           'part' => 'open',
            'title' => 'Brands Groups',
            'actions' => [
                [
                    'icon' => 'fa fa-plus',
                    'title' => 'Add New Brand Group',
                    'target' => route('bo.companies.brands-groups.create',['company_id'=> $company->id ]),
                    'permission' => auth()->user()->can($permission_name .'.add')
                ]
            ]
        ])


    @include('partials.containers.data-table', [
           'data' => $brands_groups,
           'columns' => [
               [
                   'key' => 'id',
                   'width'=>'2%',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'name',
                   'width'=>'5%',
                   'value' => function ($data) {
                       return $data->name;
                   }
               ],
               [
                   'key' => 'company_id',
                   'width'=>'6%',
                   'value'=> function($data){
                   return $data->company_id;
                   }
               ],
               [
                   'key' => 'company',
                   'width'=>'12%',
                   'value'=> function() use ($company){
                   return $company->name;
                   }
               ],
               [
                 'key' => 'actions',
                 'width'=>'1%',
                 'value'=>function($data) use ($permission_name,$company){
                    $res = '';
                        if (auth()->user()->can("{$permission_name}.edit")) {
                            $res .= "<a href='" . route('bo.companies.brands-groups.edit', [$company->id,$data->id]) . "' class='center-block tooltips' data-original-title='Edit Brand Group'><span aria-hidden='true' class='icon-pencil fa-fw'></span></a>";
                       }

                       return $res;
                 }
               ]

           ]
        ])

    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop
