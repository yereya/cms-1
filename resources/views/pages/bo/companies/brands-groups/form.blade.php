@extends('layouts.metronic.main')
@section('page_title')
    @if (isset($brand_group)) {{ $brand_group->name }} @endif
@endsection


@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($brand_group) ? 'Edit' : 'Create',
    ])
    {!! Form::open([
            'route' => isset($brand_group) ? ['bo.companies.brands-groups.update', $brand_group->company->id,$brand_group->id] : ['bo.companies.brands-groups.store',$company->id],
            'method' => isset($brand_group) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($brand_group) ? $brand_group->name : null
        ])

    @include('partials.fields.input', [
            'name' => 'description',
            'required' => true,
            'value' => isset($brand_group) ? $brand_group->description : null
        ])



    @include('partials.fields.select',[
       'name'=>'company_id',
       'label'=>'Company Name',
       'required'=>true,
       'readonly' => true,
       'list' => [ $company->id => $company->name] ?? [],
       'value' => $company->id
    ])

    @if(isset($brand_group))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url([$company->id])
                    ],
                    'submit' => 'true'
                ]
            ])
    @else


        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url([$company->id])
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop