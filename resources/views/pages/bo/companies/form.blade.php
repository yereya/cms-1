@extends('layouts.metronic.main')

@section('page_title')
    @if (isset($company)) {{ $company->name }} @endif
@endsection



@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($company) ? 'Edit' : 'Create',
    ])

    {!! Form::open([
            'route' => isset($company) ? ['bo.companies.update', $company->id] : ['bo.companies.store'],
            'method' => isset($company) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form'
        ]) !!}

    @include('partials.fields.input', [
            'name' => 'name',
            'required' => true,
            'value' => isset($company) ? $company->name : null
        ])

    @include('partials.fields.input', [
            'name' => 'product_description',
            'required' => true,
            'value' => isset($company) ? $company->product_description : null
        ])

    @include('partials.fields.input',[
     'name' => 'SAP_id',
            'value' => isset($company) ? $company->SAP_id : null
    ])

    @if(isset($company))
        @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => 'true'
                ]
            ])
    @else
        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                    'submit' => 'true'
                ],
                'checkboxes' => [
                    'create_another' => isset($role) ? null : 'true'
                ]
            ])
    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
'part' => 'close'
])
@stop