@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Roles',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('roles.create')
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $roles,
        'attributes' => [
            'data-page-length' => 20,
        ],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) use ($permission_name) {
                    if (auth()->user()->can($permission_name.'.edit')) {
                        return "<a href=\"". route('roles.edit', [$data->id]) ."\">{$data->name}</a>";
                    }
                    return $data->name;
                }
            ],
            'description',
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= "<a href=\"". route('roles.edit', [$data->id]) ."\"><span aria-hidden=\"true\" title='Edit' class=\"icon-pencil tooltips\"></span></a> ";
                    }

                    if (auth()->user()->can('permissions.modal[role_permission].view')) {
                        $res .= "<a href=\"". route('permissions.{role_id}.role-permissions', [$data->id]) ."\" data-toggle='modal'
                        data-target='#ajax-modal'><span aria-hidden=\"true\" title='Set permission' class=\"icon-shield tooltips\"></span></a> ";
                    }

                    if (auth()->user()->can($permission_name.'.add')) {
                        $res .= "<a href=\"". route('roles.{user_id}.duplicate', [$data->id]) ."\"><span aria-hidden=\"true\" title='Duplicate' class=\"fa fa-copy tooltips\"></span></a> ";
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res.="<a class='tooltips'
                        data-original-title='Delete Role'
                        data-target='sweetAlert'
                        data-alert_title='Delete Role'
                        data-alert_type='warning'
                        data-alert_href='".route('roles.{role_id}.ajax-alert', [$data->id, 'method=deleteRole'])."'><span class='icon-trash'></span></a>";
                    }

                    return $res;
                }
            ]
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop