@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part'  => 'open',
        'title' => 'Duplicate Role Form'
    ])

    {!! Form::open([
        'route'  => 'roles.store',
        'method' => 'POST',
        'class'  => 'form-horizontal',
        'role'   => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name'  => 'name',
            'value' => isset($role) ? $role->name : null
        ])

        @include('partials.fields.textarea', [
            'name'  => 'description',
            'value' => isset($role) ? $role->description : null
        ])

        @include('partials.fields.select', [
            'name' => 'permission_id',
            'list' => $permissions,
            'multi_select' => true,
            'value' => isset($role) ? $role->permissions : null
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($role) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop