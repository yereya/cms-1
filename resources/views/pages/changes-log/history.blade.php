@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Changes Log',
        'id' => 'change_log_portlet',
    ])

    @include('pages.changes-log.filters.history-table-filters')

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'changes_log_history_table',
        'route' => 'changes-log.datatable',
        'server_side' => true,
        'class' => ['tp-server-side'],
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '10',
            'data-route_method' => 'changeHistory',
            'data-filter' => 'false',
            'data-blocking_loader' => 'true',
        ],
        'columns' => [
            'Date',
            'Account',
            'Device',
            'Campaign',
            'Ad Group',
            'Match Type',
            'Keyword',
            'Change Type',
            'Change To',
            'Old Value',
            'New Value',
            'User',
            'Actions',
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop