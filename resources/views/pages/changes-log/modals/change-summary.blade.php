@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Status change',
])

<div class="row">
    <div class="col-md-12">
        <div class="well text-center">
            <h2>Change Summary</h2>
        </div>
    </div>
</div>

@include('partials.containers.data-table', [
    'data' => $change_summary,
    'attributes' => [
        'data-page-length' => '20',
    ],
    'columns' => array_merge($table_columns, [
        [
            'key'   => 'old_value',
            'value' => function ($data) {
                switch ($data['change_type']) {
                    case 'status' :
                        if ($data['old_value']) {
                            return "<span class='label label-sm label-primary'>Active</span>";
                        }
                        return "<span class='label label-sm label-default'>Paused</span>";

                    default :
                        return $data['old_value'];
                }
            }
        ],
        [
            'key'   => 'new_value',
            'value' => function ($data) {
                switch ($data['change_type']) {
                    case 'status' :
                        if ($data['new_value']) {
                            return "<span class='label label-sm label-primary'>Active</span>";
                        }
                        return "<span class='label label-sm label-default'>Paused</span>";

                    default :
                        return $data['new_value'];
                }
            }
        ],
        'failure_reason'
    ])
])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true,
    ],
])
