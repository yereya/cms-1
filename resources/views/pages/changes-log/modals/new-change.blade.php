@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Status change',
])

<div class="row">
    <div class="col-md-12">
        <div class="well text-center">
            <h2>ACTION : {{ $change_type }}</h2>
        </div>
    </div>
</div>


    {!! Form::open([
            'url'    => route('changes-log.store'),
            'method' => 'post',
            'class'  => 'form-horizontal',
            'role'   => 'form'
    ]) !!}


        @if(in_array($change_type, ['status', 'ad_pause']))
            @include('partials.fields.select', [
                'name' => snake_case($change_type),
                'label' => snakeToWords($change_type),
                'list' => [
                    ['id' => 'active', 'text' => 'Active'],
                    ['id' => 'paused', 'text' => 'Paused']
                ],
                'value' => 'active'
            ])

        @elseif(in_array($change_type, ['bid', 'bid_adjustment', 'budget', ]))

            <div class="row">
                <div class=" col-md-offset-2 col-md-2">
                    @include('partials.fields.select', [
                        'label' => '',
                        'name' => 'value_operation',
                        'required' => true,
                        'list' => $value_operation_list,
                        'value' => $value_operation_list[0],
                    ])
                </div>

                <div class="col-md-4">
                    @include('partials.fields.input', [
                        'name' => snake_case($change_type),
                        'label' => snakeToWords($change_type),
                        'required' => 'true',
                        'type' => 'number',
                        'value' => ''
                    ])
                </div>

                <div class="col-md-2">
                    @include('partials.fields.select', [
                        'label' => '',
                        'name' => 'value_type',
                        'required' => true,
                        'list' => $value_type_list,
                        'value' => $value_type_list[0],
                    ])
                </div>
            </div>

        @elseif(in_array($change_type, ['scheduling']))
            @include('partials.fields.input-timepicker', [
                'name' => snake_case($change_type) . '_time_start',
                'label' => snakeToWords($change_type) . ' Time Start',
                'time' => true,
                'required' => true,
                'column' => 6,
                'value'    =>  ''
            ])
            @include('partials.fields.input-timepicker', [
                'name' => snake_case($change_type) . '_time_end',
                'label' => snakeToWords($change_type) . ' Time End',
                'time' => true,
                'required' => true,
                'column' => 6,
                'value'    =>  ''
            ])

            @include('partials.fields.select', [
                'name' => snake_case($change_type) . '_days',
                'label' => snakeToWords($change_type) . ' Days',
                'required' => true,
                'list' => weekDays('select2'),
            ])
        @elseif(in_array($change_type, ['ad_change', 'location', 'campaign_settings', 'site_links', 'comment']))
            {{-- No extra special fields are required --}}
        @endif

        {{-- Set State: *** --}}

        <hr>

        <div class="row">
            <div class="col-md-6">
                @include('partials.fields.textarea', [
                    'label' => 'Expected Result',
                    'name' => 'expected_result',
                    'value' => ''
                ])
            </div>
            <div class="col-md-6">
                @include('partials.fields.textarea', [
                    'label' => 'Reason',
                    'name' => 'reason',
                    'value' => ''
                ])
            </div>
            <div class="col-md-6">
                @include('partials.fields.textarea', [
                    'label' => 'Comment',
                    'name' => 'comment',
                    'value' => ''
                ])
            </div>
            <div class="col-md-6" style="margin-top: 30px;">
                @include('partials.fields.input-timepicker', [
                    'label'    => 'Follow Up Date',
                    'name'     => 'follow_up_date',
                    'datetime' => true,
                    'value'    =>  date('Y-m-d 10:00', strtotime('+1 day'))
                ])
            </div>
        </div>


        {!! Form::hidden('filters', json_encode($filters)) !!}
        {!! Form::hidden('change_type', snake_case($change_type)) !!}
        {!! Form::hidden('change_level', snake_case($change_level)) !!}
        {{ csrf_field() }}
    {!! Form::close() !!}


@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true,
        'save' => [
            'attributes' => [
                'onclick' => 'App.blockUI();'
            ]
        ],
    ],
    'js' => [
        'date_time_picker' => true,
        'select2' => true,
    ]
])
