@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Status change',
])

<div class="row">
    <div class="col-md-12">
        <div class="well text-center">
            <h2>Details</h2>
        </div>
    </div>
</div>

@include('partials.containers.data-table', [
    'data' => $change,
    'columns' => [
        [
            'key' => 'created_at',
            'label' => "Date",
        ],
        [
            'key' => 'account_name',
            'label' => 'Account',
        ],
        [
            'key' => 'campaign_name',
            'label' => 'Campaign',
        ],
        [
            'key' => 'change_type',
            'label' => 'Change Type',
        ],
        [
            'key' => 'change_level',
            'label' => 'Change to',
        ],
        [
            'key' => 'old_value',
            'label' => 'Old Value',
        ],
        [
            'key' => 'new_value',
            'label' => 'New Value',
        ],
    ]
])

<div class="row">
    <div class="col-md-2">
        Comment:
    </div>
    <div class="col-md-10">
        {{ $change->first()->comment }}
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        Reason:
    </div>
    <div class="col-md-10">
        {{ $change->first()->reason }}
    </div>
</div>

<div class="row">
    <div class="col-md-2">
        Expected Result:
    </div>
    <div class="col-md-10">
        {{ $change->first()->expected_result }}
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="well text-center">
            <h2>Recent Changes for User</h2>
        </div>
    </div>
</div>

@include('partials.containers.data-table', [
    'data' => $five_recent_changes,
    'columns' => [
        [
            'key' => 'created_at',
            'label' => "Date",
        ],
        [
            'key' => 'account_name',
            'label' => 'Account',
        ],
        [
            'key' => 'campaign_name',
            'label' => 'Campaign',
        ],
        [
            'key' => 'change_type',
            'label' => 'Change Type',
        ],
        [
            'key' => 'change_level',
            'label' => 'Change to',
        ],
        [
            'key' => 'old_value',
            'label' => 'Old Value',
        ],
        [
            'key' => 'new_value',
            'label' => 'New Value',
        ],
    ]
])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true,
    ],
    'js' => [
        'modal_width' => 1200
    ]
])
