<div class="row table-filters">
    {!! Form::open([
        'route' => ['changes-log.datatable'],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',
    ]) !!}

    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'advertiser',
            'id' => 'advertiser_select',
            'label' => 'Advertiser',
            'column' => 4,
            'list' => $advertisers
        ])

        @include('partials.fields.select', [
            'name' => 'account',
            'label' => 'Account',
            'id' => 'account_select',
            'column' => 4,
            'ajax' => true,
            'class' => 'select_conditioner select_where_field',
            'value' => [],
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#advertiser_select',
                'data-method' => 'accounts',
                'data-api_url' => route('changes-log.select2'),
                'data-value' => -1
            ]
        ])

        @include('partials.fields.select', [
            'name' => 'device',
            'label' => 'Device',
            'id' => 'device_select',
            'column' => 3,
            'ajax' => true,
            'multi_select' => true,
            'class' => 'select_conditioner',
            'value' => [],
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#advertiser_select',
                'data-dependency_selector[1]' => '#account_select',
                'data-method' => 'device',
                'data-api_url' => route('changes-log.select2'),
                'data-value' => -1
            ]
        ])

    </div>

    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'campaign',
            'label' => 'Campaign',
            'id' => 'campaign_select',
            'column' => 4,
            'ajax' => true,
            'multi_select' => true,
            'class' => 'select_conditioner',
            'value' => [],
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#advertiser_select',
                'data-dependency_selector[1]' => '#account_select',
                'data-method' => 'campaigns',
                'data-api_url' => route('changes-log.select2'),
                'data-value' => -1
            ]
        ])

        @include('partials.fields.select', [
            'name' => 'ad_group',
            'label' => 'Ad Group',
            'id' => 'ad_group_select',
            'column' => 4,
            'ajax' => true,
            'multi_select' => true,
            'class' => 'select_conditioner',
            'value' => [],
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#advertiser_select',
                'data-dependency_selector[1]' => '#account_select',
                'data-dependency_selector[2]' => '#campaign_select',
                'data-method' => 'adGroups',
                'data-api_url' => route('changes-log.select2'),
                'data-value' => -1
            ]
        ])

        @include('partials.fields.select', [
            'name' => 'match_type',
            'label' => 'Match Type',
            'id' => 'match_type_select',
            'column' => 3,
            'ajax' => true,
            'multi_select' => true,
            'class' => 'select_conditioner',
            'value' => [],
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#advertiser_select',
                'data-dependency_selector[1]' => '#account_select',
                'data-dependency_selector[2]' => '#campaign_select',
                'data-dependency_selector[3]' => '#ad_group_select',
                'data-method' => 'matchType',
                'data-api_url' => route('changes-log.select2'),
                'data-value' => -1
            ]
        ])

    </div>

    <div class="form-inline">

        @include('partials.fields.select', [
            'name' => 'keyword',
            'label' => 'Keyword',
            'id' => 'keyword_select',
            'column' => 4,
            'ajax' => true,
            'multi_select' => true,
            'class' => 'select_conditioner',
            'value' => [],
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#advertiser_select',
                'data-dependency_selector[1]' => '#account_select',
                'data-dependency_selector[2]' => '#campaign_select',
                'data-dependency_selector[3]' => '#ad_group_select',
                'data-dependency_selector[4]' => '#match_type_select',
                'data-method' => 'keywords',
                'data-api_url' => route('changes-log.select2'),
                'data-value' => -1
            ]
        ])

        @include('partials.fields.select', [
            'name' => 'status',
            'label' => 'Status',
            'id' => 'status_select',
            'column' => 4,
            'multi_select' => true,
            'class' => 'select_conditioner',
            'list' => [
                'paused' => 'Paused',
                'active' => 'Active',
            ],
            'value' => [],
        ])

        @include('partials.fields.select', [
            'name' => 'change_type',
            'label' => 'Change Type',
            'id' => 'change_type',
            'column' => 3,
            'class' => 'select_conditioner',
            'list' => [
                'status' => 'Status',
                'bid' => 'Bid',
                'bid_adjustment' => 'Bid Adjustment',
                'budget' => 'Budget',
                'scheduling' => 'Scheduling',
                'location' => 'Location',
                'campaign_settings' => 'Campaign Settings',
                'site_links' => 'Site Links',
            ],
            'value' => [],
        ])

    </div>

    <div class="form-inline">
        @if($show_user_filter ?? false)
            @include('partials.fields.select', [
                'name' => 'user_id',
                'label' => 'Users',
                'id' => 'user',
                'column' => 4,
                'ajax' => true,
                'class' => 'select_conditioner',
                'value' => [],
                'attributes' => [
                    'data-min_input' => 0,
                    'data-method' => 'users',
                    'data-api_url' => route('users.select2'),
                    'data-value' => -1
                ]
            ])
        @endif

            @include('partials.fields.date-range-picker', [
                'class' => 'date-range',
                'column' => 5,
                'name' => 'date_range',
                'label' => 'Date Range',
                'form_line' => true,
                'value' => formatDate('-14 day', 'Y-m-d') . ' ' . formatDate('now', 'Y-m-d'),
            ])

            <div class="form-group form-md-line-input col-md-2">
                @include('partials.fields.button', [
                    'value' => 'Submit',
                ])
            </div>

        </div>
    {!! Form::close() !!}
</div>
