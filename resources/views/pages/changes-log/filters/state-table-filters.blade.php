<div class="row table-filters">
    {!! Form::open([
        'route' => ['changes-log.datatable'],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',

    ]) !!}
        <div class="form-inline">
            @include('partials.fields.select', [
                'name' => 'advertiser',
                'id' => 'advertiser_select',
                'label' => 'Advertiser',
                'class' => 'pull-right',
                'required' => true,
                'column' => 4,
                'list' => $advertisers
            ])

            @include('partials.fields.select', [
                'name' => 'account',
                'label' => 'Account',
                'id' => 'account_select',
                'required' => true,
                'column' => 4,
                'ajax' => true,
                'class' => 'select_conditioner select_where_field',
                'value' => [],
                'attributes' => [
                    'data-min_input' => 0,
                    'data-dependency_selector[0]' => '#advertiser_select',
                    'data-method' => 'accounts',
                    'data-api_url' => route('changes-log.select2'),
                    'data-value' => -1
                ]
            ])

            @include('partials.fields.select', [
                'name' => 'device',
                'label' => 'Device',
                'id' => 'device_select',
                'column' => 3,
                'ajax' => true,
                'multi_select' => true,
                'class' => 'select_conditioner',
                'value' => [],
                'attributes' => [
                    'data-min_input' => 0,
                    'data-dependency_selector[0]' => '#advertiser_select',
                    'data-dependency_selector[1]' => '#account_select',
                    'data-method' => 'device',
                    'data-api_url' => route('changes-log.select2'),
                    'data-value' => -1
                ]
            ])
        </div>

        <div class="form-inline">
            @include('partials.fields.select', [
                'name' => 'campaign',
                'label' => 'Campaign',
                'id' => 'campaign_select',
                'column' => 4,
                'ajax' => true,
                'multi_select' => true,
                'class' => 'select_conditioner',
                'value' => [],
                'attributes' => [
                    'data-min_input' => 0,
                    'data-dependency_selector[0]' => '#advertiser_select',
                    'data-dependency_selector[1]' => '#account_select',
                    'data-method' => 'campaigns',
                    'data-api_url' => route('changes-log.select2'),
                    'data-value' => -1
                ]
            ])
            @include('partials.fields.select', [
                'name' => 'ad_group',
                'label' => 'Ad Group',
                'id' => 'ad_group_select',
                'column' => 4,
                'ajax' => true,
                'multi_select' => true,
                'class' => 'select_conditioner',
                'value' => [],
                'attributes' => [
                    'data-min_input' => 0,
                    'data-dependency_selector[0]' => '#advertiser_select',
                    'data-dependency_selector[1]' => '#account_select',
                    'data-dependency_selector[2]' => '#campaign_select',
                    'data-method' => 'adGroups',
                    'data-api_url' => route('changes-log.select2'),
                    'data-value' => -1
                ]
            ])

            @include('partials.fields.select', [
                'name' => 'match_type',
                'label' => 'Match Type',
                'id' => 'match_type_select',
                'column' => 3,
                'ajax' => true,
                'multi_select' => true,
                'class' => 'select_conditioner',
                'value' => [],
                'attributes' => [
                    'data-min_input' => 0,
                    'data-dependency_selector[0]' => '#advertiser_select',
                    'data-dependency_selector[1]' => '#account_select',
                    'data-dependency_selector[2]' => '#campaign_select',
                    'data-dependency_selector[3]' => '#ad_group_select',
                    'data-method' => 'matchType',
                    'data-api_url' => route('changes-log.select2'),
                    'data-value' => -1
                ]
            ])
        </div>

        <div class="form-inline">
            @include('partials.fields.select', [
                'name' => 'keyword',
                'label' => 'Keyword',
                'id' => 'keyword_select',
                'column' => 4,
                'ajax' => true,
                'multi_select' => true,
                'class' => 'select_conditioner',
                'value' => [],
                'attributes' => [
                    'data-min_input' => 0,
                    'data-dependency_selector[0]' => '#advertiser_select',
                    'data-dependency_selector[1]' => '#account_select',
                    'data-dependency_selector[2]' => '#campaign_select',
                    'data-dependency_selector[3]' => '#ad_group_select',
                    'data-dependency_selector[4]' => '#match_type_select',
                    'data-method' => 'keywords',
                    'data-api_url' => route('changes-log.select2'),
                    'data-value' => -1
                ]
            ])

            @include('partials.fields.select', [
                'name' => 'status',
                'label' => 'Status',
                'id' => 'status_select',
                'column' => 4,
                'multi_select' => true,
                'class' => 'select_conditioner',
                'list' => [
                    'paused' => 'Paused',
                    'active' => 'Active',
                ],
                'value' => [],
            ])

        <div class="form-group form-md-line-input col-md-3">
            @include('partials.fields.button', [
                'value' => 'Submit',
                'right' => true
            ])
        </div>
    </div>
    {!! Form::close() !!}
</div>
