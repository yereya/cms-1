@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Changes Log',
        'id' => 'change_log_portlet',
        'dropdown_buttons' => [
            [
                'title' => 'Comment',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'comment',
                    'data-change_level' => 'keyword,ad_group,campaign'
                ]
            ],
            [
                'title' => 'Status',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'status',
                    'data-change_level' => 'keyword,ad_group,campaign'
                ]
            ],
            [
                'title' => 'Bid',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'bid',
                    'data-change_level' => 'ad_group,keyword'
                ]
            ],
            [
                'title' => 'Bid Adjustment',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'bid_adjustment',
                    'data-change_level' => 'campaign,ad_group'
                ]
            ],
            [
                'title' => 'Budget',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'budget',
                    'data-change_level' => 'campaign'
                ]
            ],
            [
                'title' => 'Scheduling',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'scheduling',
                    'data-change_level' => 'campaign'
                ]
            ],
            [
                'title' => '<s>Ad Pause</s>',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'ad_pause',
                    'data-change_level' => 'TODO'
                ]
            ],
            [
                'title' => '<s>Ad Change</s>',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'ad_change',
                    'data-change_level' => 'TODO'
                ]
            ],
            [
                'title' => 'Location',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'location',
                    'data-change_level' => 'campaign'
                ]
            ],
            [
                'title' => 'Campaign Settings',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'campaign_settings',
                    'data-change_level' => 'campaign'
                ]
            ],
            [
                'title' => 'Site Links',
                'attributes' => [
                    'data-route' => route('changes-log.ajax-modal', ['method' => 'newChange']),
                    'data-change_type' => 'site_links',
                    'data-change_level' => 'account,campaign'
                ]
            ],
        ]
    ])

    @include('pages.changes-log.filters.state-table-filters')

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'changes_log_state_table',
        'route' => 'changes-log.datatable',
        'server_side' => true,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '10',
            'data-order' => '[[2, "asc"]]',
            'data-route_method' => 'changeLogState',

            //the "processing" loader
            'data-blocking_loader' => 'true',
            'data-filter' => 'false',

        ],
        'columns' => [
            [
                'label' => '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                <input type="checkbox" class="group-checkable" title="group-checkable" value="ALL">
                                <span></span>
                            </label>',
                'attributes' => [
                    'class' => 'sorting_disabled',
                    'data-orderable' => 'false',
                    'width' => '2%',
                ]
            ],
            'Device',
            'Campaign',
            'Ad Group',
            'Match Type',
            'Keyword',
            'Status',
            'Budget',
            'Bid',
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop