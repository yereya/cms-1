@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => $title,
])

<p>{{ $text or 'Are you should you want to delete this.' }}</p>


@include('partials.containers.buttons.delete', [
    'route' => $route,
    'title' => isset($btn_title) ? $btn_title : $title,
])

<style type="text/css">
    /*.tp-table .tp-table .tp-table-cell {*/
        /*display: table-row;*/
    /*}*/
    #ajax-modal .dataTables_wrapper td {
        word-break: break-all;
    }
</style>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ]
])
