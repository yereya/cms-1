@extends('layouts.metronic.main')
@section('page_content')
    <div class="dash-stats-wrapper">
        <div class="row">
            @if(count($accounts))
                <div class="col-md-9">
                    @include('partials.containers.dashboards.stats.table',[
                     'title'=>'Target Vs Actual',
                     'wrapper_class'=> 'yesterday targets advertiser-target-wrapper',
                     'data'=>[
                            'type'=>'ppc',
                            'widget'=>'table',
                            'section'=>'targets',
                            'route'=>'dashboards.advertisers-targets.api',
                            'table_description'=>'target vs actual',
                            'table_name'=> 'advertiser_targets'
                            ],
                      'filters' => [
                      'searchBar' => true
                       ]
                    ])

                    @include('partials.containers.dashboards.stats.chart',[
                        'data'=>[
                            'widget'=>'chart',
                            'type'=> 'advertiser_target',
                            'name'=>'advertisersTargets',
                            'period'=>'daily',
                            'group'=>'ppc',
                            'target'=>'sales',
                            'route'=>'dashboards.advertisers-targets.api'
                        ]
                    ])
                </div>
                <div class="col-md-3">

                    {{--START FILTERS--}}
                    @include('pages.dashboards._partials.filters',[
                        'calculation'=>['daily','total'],
                        'date_range_default'=> '[date span="first day of this month"] [date span="-1 Day"]',
                        'fields'=>['advertiser','business_group','traffic_source']
                    ])
                    {{--END FILTERS--}}
                    <div class="clearfix"></div>
                </div>
            @else
                @include('partials.containers.portlet', [
                 'part' => 'open',
                 'title'=>  'Dashboard',
                   ])
                <div class="alert alert-warning">
                    <strong>Warning!</strong>
                    User accounts are required in order to display this view.
                </div>
                @include('partials.containers.portlet', [
                  'part' => 'close'
                  ])
            @endif
        </div>
    </div>
@stop
