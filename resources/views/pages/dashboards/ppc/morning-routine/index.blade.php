@extends('layouts.metronic.main')

@section('javascript')
    <script>
      $(document).ready(function() {
        $('.select3').select2();
      });
    </script>
@endsection

@section('page_content')
    @if(isset($accounts) && count($accounts))
        <div class="ppc-morning-routine-wrapper">
            {{--Summary Widget--}}
            @include('pages.dashboards.ppc.morning-routine.partials.summary')

            {{--Alerts Widget--}}
            @include('pages.dashboards.ppc.morning-routine.partials.alerts')

            {{--Chart Widget--}}
            @include('pages.dashboards.ppc.morning-routine.partials.morning-chart')
        </div>
    @else

        {{--THERES NO ACCOUNTS --}}
        @include('pages.dashboards.dynamic.partials.no-account-page')
    @endif

@endsection