<style>
    .chart {
        height: auto;
    }
</style>
<div class="chart">
    @include('partials.containers.dashboards.stats.chart',[
             'width'=>'81vw',
             'column'=>'',
             'data'=>[
                    'widget'=>'chart',
                    'type'=> 'ppc_morning_routine',
                    'name'=>'morningRoutine',
                    'target'=>'profit',
                    'period'=>'yesterday',
                    'url'=> 'dashboards.ppc-morning-routine.api'
            ],
            'dynamic_mode'=> [
                'label'=>'advertiser_name',
                'column'=>'profit'
             ]
    ])
    <div class="clearfix"></div>
</div>