<div class="summary row">

    @include('partials.containers.dashboards.stats.table',[
                'title'=>'Summary',
                'data'=> [
                       'type'=>'ppc-morning-routine',
                       'widget'=>'table',
                       'section'=>'summary',
                       'url'=>'dashboards.ppc-morning-routine.api',
                       'table_name'=>'daily_stats',
                       'table_description'=>'summary'
                     ],
                 'filters' => [
                      'fieldSelection' => true,
                      'searchBar' => true
                   ]
                ]
)
</div>
<div class="clearfix"></div>