<div class="alerts">
    @include('partials.containers.dashboards.stats.table-with-tabs', [
            'title'=>'Alerts',
            'effect'=>'fade',
            'data'=> [
                       'type'=>'ppc-morning-routine',
                       'widget'=>'table',
                       'section'=>'alerts',
                       'route'=>'dashboards.ppc-morning-routine.api',
                       'route-save-state'=>'dashboards.ppc-morning-routine.save_alert_state'
                     ],
            'headers' => [
                ['label'=> 'Budgets', 'active' => true],
                ['label'=> 'Accounts'],
                ['label'=> 'Campaigns'],
            ],
            'body' => [
                 [
                 'active'=> true,
                 'class'=>'budgets',
                 'table'=>'budgets',
                 'data'=>[
                    'table_name'=>'budgets',
                    'section'=>'alerts',
                    'table_description'=>'Budget alerts'
                 ]
                 ],
                 [
                 'class'=>'accounts',
                 'table'=>'accounts',
                 'data'=>[
                    'table_name'=>'accounts',
                    'section'=>'alerts',
                    'table_description'=>'Accounts alerts'
                   ]
                 ],
                 [
                 'class'=>'campaigns',
                 'table'=>'campaigns',
                     'data'=>[
                        'table_name'=>'campaigns',
                        'section'=>'alerts',
                        'table_description'=>'Campaigns alerts'
                     ]
                 ],
            ],
            'filters'=> [
                'searchBar' => true
            ]
         ]
       )
</div>