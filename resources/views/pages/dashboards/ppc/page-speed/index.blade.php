@extends('layouts.metronic.main')
@section('page_content')
    <div class="dash-stats-wrapper page-speed">
        <div class="page-speed-main row">
            <div class="col-md-13">
                @include('partials.containers.dashboards.stats.table',[
                 'title'=>'PageSpeed Scores',
                 'wrapper_class'=> 'yesterday page-speed',
                 'data'=> [
                        'type'=>'page-speed',
                        'widget'=>'table',
                        'section'=>'pageSpeed',
                        'route'=>'dashboards.page-speed.api'
                      ]
                ])

            </div>
        </div>

        {{ Form::open([
                          'route'=>'dashboards.page-speed.refresh_func',
                          'method' => 'GET',
                          'role' => 'form',
                          'class'=>'page-speed-form'
                    ]) }}
        @include('partials.fields.button', [
            'url' => route('dashboards.page-speed.refresh_func') ,
            'class' =>['page-speed-refresh-btn', 'btn', 'btn-circle', 'btn-primary'],
            'value' => 'Update Scores'
        ])
    </div>
@stop