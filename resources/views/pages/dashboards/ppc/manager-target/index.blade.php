@extends('layouts.metronic.main')
@section('page_content')
    <div class="dash-stats-wrapper">
        <div class="row">
            @if(count($accounts))
                <div class="col-md-9">
                    @include('partials.containers.dashboards.stats.table',[
                     'title'=>'Targets',
                     'wrapper_class'=> 'yesterday manager-target',
                     'data'=>[
                            'type'=>'manager-target',
                            'widget'=>'table',
                            'section'=>'managerTarget',
                            'route'=>'dashboards.manager-target.api',
                            'table_description'=>'manager targets'
                          ]
                    ])

                </div>
                <div class="col-md-3">

                    {{--START FILTERS--}}
                    @include('pages.dashboards._partials.filters',[
                        'fields'=>['vertical','advertiser'],
                        'duration'=>['daily', 'weekly', 'monthly']
                    ])
                    {{--END FILTERS--}}
                    <div class="clearfix"></div>
                </div>
            @else
                @include('partials.containers.portlet', [
                 'part' => 'open',
                 'title'=>  'Dashboard',
                   ])
                <div class="alert alert-warning">
                    <strong>Warning!</strong>
                    User accounts are required in order to display this view.
                </div>
                @include('partials.containers.portlet', [
                  'part' => 'close'
                  ])
            @endif
        </div>
    </div>
@stop