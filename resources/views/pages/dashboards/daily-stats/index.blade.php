@extends('layouts.metronic.main')
@section('page_content')
    <div class="dash-stats-wrapper">
        <div class="row">
            @if(count($accounts))
                <div class="col-md-9">
                    @include('partials.containers.dashboards.stats.summary',[
                     'title'=>'yesterday',
                     'wrapper_class'=> 'yesterday',
                     'data'=>[
                            'widget'=>'summary',
                            'period'=>'daily'
                     ]
                    ])

                    @include('partials.containers.dashboards.stats.summary',[
                       'title'=>'this month',
                       'wrapper_class'=> 'month',
                       'data'=>[
                            'widget'=>'summary',
                            'period'=>'monthly',
                        ]
                      ])

                    @include('partials.containers.dashboards.stats.chart',[
                        'width'=>'59vw',
                        'data'=>[
                            'widget'=>'chart',
                            'period'=>'daily',
                            'name'=>'dailyStats',
                            'type'=>'cost_profit',
                            'group'=>'daily_stats',
                            'target_type'=>'cost_profit',
                            'url'=>'dashboards.daily-stats.api'
                        ]
                    ])
                </div>
                <div class="col-md-3">
                    @include('pages.dashboards._partials.filters',[
                        'fields'=> ['account','vertical','advertiser','device','channel'],
                        'duration'=> ['daily','weekly', 'monthly']
                    ])
                    <div class="clearfix"></div>
                </div>
            @else
                @include('partials.containers.portlet', [
                 'part' => 'open',
                 'title'=>  'Dashboard',
                   ])
                <div class="alert alert-warning">
                    <strong>Warning!</strong>
                    Account are required in order to display this view.
                </div>
                @include('partials.containers.portlet', [
                  'part' => 'close'
                  ])
            @endif
        </div>
    </div>
@stop
