<div class="col-md-3 filters">
    @include('partials.containers.portlet', [
              'part' => 'open',
              'title'=>'Filters'
           ])

    <div class="filters-wrapper">
        <div class="pickers">
            @include('pages.dashboards.dynamic.partials.filters.date-range')
            @include('pages.dashboards.dynamic.partials.filters.levels')
            @include('pages.dashboards.dynamic.partials.filters.where')
        </div>
        <div class="instant-filters">
            @include('pages.dashboards.dynamic.partials.filters.duplicates')
            @include('pages.dashboards.dynamic.partials.filters.offence_defence')
            @include('pages.dashboards.dynamic.partials.filters.devices')
        </div>
    </div>
</div>
@include('partials.containers.portlet',['part'=>'close'])
</div>


