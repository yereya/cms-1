@include('partials.containers.portlet', [
               'part' => 'open',
               'title'=>  'Dashboard',
                 ])
<div class="alert alert-warning">

    <strong>Warning!</strong>
    Account are required in order to display this view.
</div>
@include('partials.containers.portlet', [
  'part' => 'close'
  ])
