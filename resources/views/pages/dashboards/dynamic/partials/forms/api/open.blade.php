{!! Form::open([
    'route'=> 'dashboards.dynamic.api',
    'method' => 'POST',
    'role' => 'form',
    'class'=>'widget-stats-form',
 ])!!}
{!! Form::hidden('widget','flag') !!}
{!! Form::hidden('resolved', !empty($resolved) ? $resolved->count() : 0) !!}
{!! Form::hidden('filtered',false) !!}
{!! Form::hidden('remove_duplicates','0') !!}
