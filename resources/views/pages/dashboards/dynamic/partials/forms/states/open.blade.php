{!! Form::open([
                  'route' => ['dashboards.dynamic.save_alert_state'],
                  'method' => 'POST',
                  'class' => 'form-horizontal alert-state-form',
                  'role' => 'form',
                  'style' => 'text-align: center;'
                ]) !!}

{!! Form::hidden('user_id',auth()->user()->id) !!}
{!! Form::hidden('record_id') !!}
{!! Form::hidden('records_id') !!}