<div class="dynamic-date-range picker">
    <div class="caption">
        <i aria-hidden="true" class="icon-notebook"></i>
        <span class="caption-subject font-dark bold uppercase">Date</span>
    </div>
    <div class="row">
        @include('pages.reports.partials.date-range',[
          'saved_filter_field'=> '[date span="-1 weeks"] [date]',
          'column'=>'',
          'label'=>'',
          'id'=>'date_range'
        ])
    </div>
    <div class="clearfix"></div>
</div>