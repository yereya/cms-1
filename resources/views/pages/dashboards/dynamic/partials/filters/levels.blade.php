<div class="dynamic-level-filters picker">
    <div class="caption">
        <i aria-hidden="true" class="icon-notebook"></i>
        <span class="caption-subject font-dark bold uppercase">Levels</span>
        @include('pages.dashboards.dynamic.partials.collapse',[
            'target'=>'level-filters'
        ])
    </div>
    <div class="row collapse" id="level-filters">
        @include('partials.fields.select', [
                       'name'=>'advertiser',
                       'label'=>'',
                       'column_label'=>12,
                       'class'=>'advertiser-filter',
                       'id'=>'advertiser-filter',
                       'list' => $advertisers ?? [],
                       'value' => null,
                       'attributes'=>[
                           'data-filter'=>'advertiser',
                           'data-placeholder'=>'Advertiser'
                       ]
                   ])
        <div class="clearfix"></div>
        @include('partials.fields.select', [
            'name'=>'account',
            'label'=>'',
            'column_label'=>12,
            'id'=>'account-filter',
            'column' => '',
            'force_selection'=>true,
            'value' => null,
            'ajax' => true,
            'attributes'=> [
                'data-dependency_selector[0]' => '#advertiser-filter',
                'data-param-selector[0]'=>'.date-range',
                'data-method' => 'getAccounts',
                'data-api_url' => route('dashboards.dynamic-accounts.select2'),
                'data-filter'=>'account',
                'data-placeholder'=>'Account',

            ]
        ])
        <div class="clearfix"></div>
        @include('partials.fields.select', [
            'name'=>'campaign',
            'label'=>'',
            'column_label'=>12,
            'id'=>'campaign-filter',
            'column' => '',
            'value' => null,
            'ajax' => true,
            'attributes'=> [
                'data-dependency_selector[0]' => '#advertiser-filter',
                'data-dependency_selector[1]' => '#account-filter',
                'data-param-selector[0]'=>'.date-range',
                'data-method' => 'getCampaigns',
                'data-api_url' => route('dashboards.dynamic-campaigns.select2'),
                'data-filter'=>'campaign',
                'data-placeholder'=>'Campaign'
            ]
        ])
        <div class="clearfix"></div>
    </div>
</div>