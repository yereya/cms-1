<div class="dynamic-param-filters picker">
    <div class="caption">
        <i aria-hidden="true" class="icon-notebook"></i>
        <span class="caption-subject font-dark bold uppercase">PARAMS</span>
        @include('pages.dashboards.dynamic.partials.collapse',[
            'target'=>'where-filters'
        ])
    </div>
    <div class="clearfix"></div>
    <div class="row collapse" id="where-filters">
        <div class="form-group">
            <label class="col-md-3 control-label">Cost</label>
            <div class="col-md-9 filter-field">
                <input type="hidden" name="where[0][field]" value="cost">
                <input type="hidden" name="where[0][operator]" value="" class="operator">

                <input type="number" name="where[0][value][1]"
                       class="form-control input-inline param-range"
                       data-operator="<"
                       placeholder="Lower than"
                       style="float:left;"
                >
                <input type="number" name="where[0][value][0]"
                       class="form-control input-inline param-range"
                       data-operator=">"
                       placeholder="Greater than"
                >
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="col-md-3 control-label">Profit</label>
            <div class="col-md-9 filter-field">
                <input type="hidden" name="where[1][field]" value="profit">
                <input type="hidden" name="where[1][operator]" value="" class="operator">
                <input type="number" name="where[1][value][1]"
                       class="form-control input-inline param-range"
                       placeholder="Lower than"
                       data-operator="<"
                       style="float:left;"
                >
                <input type="number" name="where[1][value][0]"
                       class="form-control input-inline param-range"
                       placeholder="Greater than"
                       data-operator=">"
                >
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <label class="col-md-3 control-label">Alert Age</label>
            <div class="col-md-9 filter-field">
                <input type="hidden" name="where[2][field]" value="alert_days">
                <input type="hidden" name="where[2][operator]" value="" class="operator">
                <input type="number" name="where[2][value][1]"
                       class="form-control input-inline param-range"
                       placeholder="Lower than" style="float:left;"
                       data-operator="<"
                >
                <input type="number" name="where[2][value][0]"
                       class="form-control input-inline param-range"
                       placeholder="Greater than"
                       data-operator=">"
                >
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="actions ">
        <button type="submit" class="btn btn-circle btn-primary refresh-stats pull-right"> Refresh
            <i class="fa fa-refresh fw"></i>
        </button>
    </div>
</div>