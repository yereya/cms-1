<div class="portlet-title tabbable-line ">
    <div class="caption">
        <i aria-hidden="true" class="icon-notebook"></i>
        <span class="caption-subject font-dark bold uppercase">Devices</span>
    </div>
</div>

@include('partials.containers.widget-row' ,
               [
                 'column_size' => 6,
                 'class'=>'devices',
                 'data' => [
                        [
                         'class'=>'desktop',
                         'attributes'=>[
                             'data-filter'=>'desktop',
                             'data-group'=> 'devices',
                             'clickable'=> true,
                            ],
                         'subtitle'=> [
                           'value' => 'Alerts',
                           'color' => '',
                           'length'=> true
                         ],
                         'icon' => [
                             'type' => 'icon-screen-desktop',
                             'bg'   => 'blue'
                          ],
                         'content'=> [
                            'value'=>'',
                            'color'=>''
                          ]
                       ],
                     [
                         'class'=>'tablet',
                         'attributes'=>[
                             'data-filter'=>'tablet',
                             'data-group'=> 'devices',
                             'clickable'=> true,
                            ],
                         'subtitle'=> [
                              'value' => 'Alerts',
                              'color'=> '',
                              'length'=> true
                         ],
                         'icon' =>[
                              'type' => 'icon-screen-tablet' ?? 'icon-bulb',
                              'bg'   => 'yellow-casablanca'
                         ],
                         'content'=> [
                            'value'=>'',
                            'color'=>''
                          ]
                     ],
                     [
                          'class'=>'mobile',
                          'attributes'=>[
                             'data-filter'=>'mobile',
                             'data-group'=> 'devices',
                             'clickable'=> true,
                            ],
                          'icon' => [
                              'type'=> 'icon-screen-smartphone' ?? 'icon-bulb',
                              'bg'  =>  'purple-studio'
                          ],
                          'subtitle'=>[
                              'value' => 'Alerts',
                              'color'=>'',
                              'length'=> true
                          ],
                          'content'=> [
                            'value'=>'',
                            'color'=>''
                          ]
                     ],
                     [
                          'class'=>'other',
                          'attributes'=>[
                             'data-filter'=>'other',
                             'data-group'=> 'devices',
                             'clickable'=> true,
                            ],
                          'icon' => [
                              'type'=> 'icon-ban' ?? 'icon-bulb',
                              'bg'  =>  'grey-mint'
                          ],
                          'subtitle'=>[
                              'value' => 'Alerts',
                              'color'=>'',
                              'length'=> true
                          ],
                          'content'=> [
                            'value'=>'',
                            'color'=>''
                          ]
                     ],

                  ]
              ])