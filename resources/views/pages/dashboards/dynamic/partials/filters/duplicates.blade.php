<div class="remove-duplicates">
    <div class="portlet-title tabbable-line ">
        <div class="caption">
            <i aria-hidden="true" class="icon-notebook"></i>
            <span class="caption-subject font-dark bold uppercase">Redundant Records</span>
            <a href="javascript:void(0);" class="pull-right duplicates">Hide
                <span class="toggle_checkbox" data-toggle_state="checked"></span>
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>