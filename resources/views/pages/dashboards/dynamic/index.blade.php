@extends('layouts.metronic.main')

@section('page_content')
    @if(count($accounts))

        {{--STATUS PORTLET--}}
        @include('pages.dashboards.dynamic.partials.status',[
            'user'=> $user ?? collect()
        ])

        {{--Dynamic wrapper for tree + data + filters--}}
        <div class="dynamic-wrapper">
            <div class="row">

                {{--TREE + DATA--}}
                <div class="list-n-data">
                    @include('pages.dashboards.dynamic.partials.list-n-data')
                </div>

                {{--FORM FOR GETTING ALERTS RECORDS ACCORDING TO FILTERS--}}
                @include('pages.dashboards.dynamic.partials.forms.api.open',[
                    'resolved'=>$resolved
                ])

                {{--FILTERS ( DEFENCE OFFENCE | MOBILE | DATERANGE )--}}
                <div class="dynamic-filters">
                    @include('pages.dashboards.dynamic.partials.filters')
                </div>

                {{--FORM CLOSE--}}
                @include('pages.dashboards.dynamic.partials.forms.api.close')

            </div>
        </div>
        <div class="clearfix"></div>
    @else

        {{--THERES NO ACCOUNTS --}}
        @include('pages.dashboards.dynamic.partials.no-account-page')
    @endif

@endsection