@include('partials.containers.portlet', [
             'part' => 'open',
             'title'=>'Filter Bar',
             'actions'=>[
                [ 'type' => 'submit',
                 'icon' => 'fa fa-refresh fw',
                 'title' => 'Refresh',
                 'class' => 'btn-primary refresh-stats'
                ]
             ],
          ])

{!! Form::open([
    'route'=>'dashboards.daily-stats.api',
    'method' => 'POST',
    'role' => 'form',
    'class'=>'dash-params-form',
    ])!!}

{{--FILTERS--}}
<div class="row">
    <div class="form-group col-md-12 duration-wrapper" style="padding-left:0">
        @if(!empty($duration))
            {{-- DURATIONS --}}
            <label class="control-label pull-left {{in_array('weekly', $duration, true) ? 'col-xs-3' : 'col-xs-3'}}">View By:</label>
            @if(in_array('daily',$duration,true))
                @include('partials.fields.radio', [
                    'column' => in_array('weekly', $duration, true) ? 3 : 4,
                    'radio_id' => 'duration_daily',
                    'label' => 'Days',
                    'value'=>'daily',
                    'data-duration' => 'Days',
                    'name' => 'duration',
                    'checked'=> true
                ])
            @endif
            @if(in_array('weekly', $duration, true))
                @include('partials.fields.radio', [
                    'column' => in_array('weekly', $duration, true) ? 3 : 5,
                    'radio_id' => 'duration_weekly',
                    'label' => 'Weeks',
                    'value' => 'weekly',
                    'data-duration' => 'Weeks',
                    'name' => 'duration'
                ])
            @endif
            @if(in_array('month',$duration,true))
                @include('partials.fields.radio', [
                            'column' => in_array('weekly', $duration, true) ? 3 : 5,
                            'radio_id' => 'duration_monthly',
                            'label' => 'Months',
                            'value' => 'monthly',
                            'data-duration'=>'Months',
                            'name' => 'duration'
                         ])
            @endif
        @endif
    </div>

    @if(!empty($calculation))
        <div class="form-group display-values-wrapper">
            <label class='control-label pull-left' for="">Display Values:</label>
            @include('partials.fields.radio', [
                   'column' => 4,
                   'name' => 'display-values',
                   'radio_id' => 'calculation-day',
                   'label' => 'Daily Avg',
                   'value'=>'daily',
                   'data-display' => 'daily',
                   'checked'=> true
               ])
            @include('partials.fields.radio', [
                   'column' => 4,
                   'name' => 'display-values',
                   'radio_id' => 'calculation-month',
                   'label' => 'Total',
                   'value'=>'till-now',
                   'data-display' => 'till-now'
               ])
            <div class="clearfix"></div>
        </div>
    @endif

    {{--DATE RANGE--}}
    @include('pages.reports.partials.date-range',[
    'column'=>'',
    'saved_filter_field'=> !empty($date_range_default) ? $date_range_default : '[date span="-1 month"] [date]'
    ])

    @if(!empty($fields))
        {{--ADVERTISERS TAB--}}
        @if(!empty($advertisers) && in_array('advertiser', $fields, true))
            @include('partials.fields.select', [
                        'name' => 'advertisers',
                        'multi_select' => true,
                        'value' => 'verticals',
                        'list'=> $advertisers ?? [],
                        'label'=>'Advertiser',
                        'column_label'=>12,
                        'class'=> 'btn btn-default',
                        'remove_class_col_md_line'=>'false'
                    ])
        @endif

        {{--Business Group TAB--}}
        @if(!empty($business_group) && in_array('business_group', $fields, true))
            @include('partials.fields.select', [
                        'name' => 'business_group',
                        'multi_select' => true,
                        'value' => 'verticals',
                        'list'=> $business_group ?? [],
                        'label'=>'Business Group',
                        'column_label'=>12,
                        'class'=> 'btn btn-default',
                        'remove_class_col_md_line'=>'false'
                    ])
        @endif

        {{--VERTICALS TAB--}}
        @if(!empty($verticals) && in_array('vertical',$fields))
            @include('partials.fields.select', [
                        'name' => 'verticals',
                        'multi_select' => true,
                        'value' => 'verticals',
                        'list'=> $verticals ?? [],
                        'label'=>'Vertical',
                        'column_label'=>12,
                        'class'=> 'btn btn-default',
                        'remove_class_col_md_line'=>'false'
                    ])
        @endif

        {{--DEVICE TAB--}}
        @if(!empty($device) && in_array('device',$fields))
            @include('partials.fields.select', [
                        'name' => 'devices',
                        'multi_select' => true,
                        'value' => 'Verticals',
                        'list'=>$device ?? [],
                        'label'=>'Device',
                        'column_label'=>12,
                        'class'=> 'btn btn-default',
                        'remove_class_col_md_line'=>'false'
                    ])
        @endif

        {{--ACCOUNTS TAB--}}
        @if(!empty($accounts) && in_array('account',$fields))
            @include('partials.fields.select', [
                        'name' => 'accounts',
                        'multi_select' => true,
                        'value' => '',
                        'list'=>$accounts ?? [],
                        'label'=>'Account',
                        'column_label'=>12,
                        'class'=> 'btn btn-default',
                        'remove_class_col_md_line'=>'false'
                    ])
        @endif

        {{--SALES GROUP TAB--}}
        @if(!empty($sale_groups) && in_array('sales_group',$fields))
            @include('partials.fields.select', [
                        'name' => 'sale_group',
                        'list'=> $sale_groups ?? [],
                        'label'=>'Sale Group',
                        'column_label'=>12,
                        'class'=> 'btn btn-default',
                        'remove_class_col_md_line'=>'false',
                        'not_selected'
                    ])
        @endif

        {{--CHANNEL TAB--}}
        @if(!empty($channel) && in_array('channel',$fields))
            @include('partials.fields.select', [
                        'name' => 'channel',
                        'list'=> $channel ?? [],
                        'label'=>'Channel',
                        'column_label'=>12,
                        'class'=> 'btn btn-default',
                        'remove_class_col_md_line'=>'false',
                        'not_selected'
                    ])
        @endif

        {{--TRAFFIC SOURCE TAB--}}
        @if(!empty($traffic_source) && in_array('traffic_source',$fields))
            @include('partials.fields.select', [
                        'name' => 'traffic_source',
                        'list'=> $traffic_source ?? [],
                        'label'=>'Traffic Source',
                        'column_label'=>12,
                        'class'=> 'btn btn-default',
                        'remove_class_col_md_line'=>'false',
                        'not_selected'
                    ])
        @endif

    @endif
</div>


{!! Form::close() !!}

@include('partials.containers.portlet',['part'=>'close'])
