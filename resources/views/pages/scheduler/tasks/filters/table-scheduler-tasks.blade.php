<div class="row table-filters">
    <div class="col-md-10">
        {!! Form::open([
            'route' => ['scheduler.tasks.datatable'],
            'method' => 'POST',
            'class' => 'form-horizontal',
            'role' => 'form',

        ]) !!}
            <div class="row">
                
                @include('partials.fields.select', [
                    'name' => 'system',
                    'label' => 'System',
                    'multi_select' => true,
                    'column' => 4,
                    'list' => $systems
                ])
                @include('partials.fields.select', [
                    'name' => 'task_name',
                    'label' => 'Task Name',
                    'multi_select' => true,
                    'column' => 4,
                    'id' => 'task_name',
                    'ajax' => true,
                    'attributes' => [
                        'data-min_input' => 1,
                        'data-method' => 'taskName',
                        'data-api_url' => route('scheduler.tasks.select2'),
                        'data-search_field' => 'task_id',
                    ]
                ])
                @include('partials.fields.select', [
                    'name' => 'active',
                    'label' => 'Active',
                    'column' => 3,
                    'list' => [
                        ['id' => '0', 'text' => 'Disabled'],
                        ['id' => '1', 'text' => 'Active'],
                    ]
                ])

                <div class="form-group form-md-line-input col-md-1">
                    @include('partials.fields.button', [
                        'value' => 'Submit',
                    ])
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
