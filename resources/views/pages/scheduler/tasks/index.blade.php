@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Scheduler Tasks',
        'actions' => [
                [
                    'drop_down' => true,
                    'title' => 'Actions',
                    'icon' => 'fa fa-th',
                    'menus' => [
                        [
                            'title' => 'Add New Task',
                            'route' => route('scheduler.tasks.create'),
                            'permission' => auth()->user()->can($permission_name .'.add')
                        ]
                    ]
                ]
        ]
    ])

    @include('pages.scheduler.tasks.filters.table-scheduler-tasks', [])
    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'scheduler-tasks',
        'class' => 'dataTable',
        'server_side' => true,
        'route' => 'scheduler.tasks.datatable',
        'attributes' => [
            'data-page-length' => '1000',
            'data-filter_container' => 'table-filters',
            'data-onchange_submit' => 'true',
            'data-route_method' => 'taskQuery',
            'data-auto-width' => 'false',
            'data-search' => 'search',
            'data-column-defs' => '[
                  { "width": "13%", "targets": 0 },
                  { "width": "25%", "targets": 1 },
                  { "width": "30%", "targets": 2 },
                  { "width": "8%", "targets": [3,4,5,6] }
            ]'
        ],
        'columns' => ['System', 'Task Name', 'Cli Custom Command', 'Repeat', 'Start Time', 'Active', 'Actions']
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop