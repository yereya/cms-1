@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Task Form'
    ])

    {!! Form::open([
        'route' => isset($task) ? ['scheduler.tasks.update', $task->id] : 'scheduler.tasks.store',
        'method' => isset($task) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="row">
        @include('partials.fields.select', [
            'label' => 'System / Sub System',
            'name' => 'field_query_params_id',
            'id' => 'field_query_params_id',
            'required' => true,
            'list' => $field_query_params,
            'value' => isset($task) && isset($task->field_query_params_id) ? $task->field_query_params_id : -1
        ])

        @include('partials.fields.select', [
            'label' => 'Task',
            'name' => 'task_id',
            'id' => 'task_id',
            'ajax' => true,
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector' => '#field_query_params_id',
                'data-api_url' => route('scheduler.tasks.select2'),
                'data-search_field' => 'task_id',
                'data-value' => isset($task) && isset($task->task_id) ? $task->task_id : -1,
            ]
        ])

        @include('partials.fields.input', [
            'name' => 'task_name',
            'label' => 'Task Name',
            'help_text' => 'Descriptive name of the task.',
            'value' => isset($task) ? $task->task_name : ""
        ])

    </div>
    <div class="row">

        @include('partials.fields.h4-separator', ['label' => 'Settings',])

        @include('partials.fields.input', [
            'name' => 'cli_custom_command',
            'label' => 'Cli Custom Command',
            'help_text' => 'Used when system default command is not set.',
            'value' => isset($task) ? $task->cli_custom_command : null
        ])

        @include('partials.fields.input', [
            'name' => 'cli_command_params',
            'label' => 'Cli Command Extra Params',
            'help_text' => 'Cli params concatenated to the task command.',
            'value' => isset($task) ? $task->cli_command_params : null
        ])

        @include('partials.fields.input', [
            'name' => 'description',
            'label' => 'Description',
            'help_text' => 'Description to the task command.',
            'value' => isset($task) ? $task->description : null
        ])

        <div class="col-md-12">
            @include('partials.fields.checkbox', [
                'wrapper' => true,
                'label' => 'Options:',
                'list' => [
                    [
                        'name' => 'active',
                        'label' => 'Is Active',
                        'value' => 1,
                        'checked' => isset($task) ? $task->active : 0
                    ],
                ],
            ])
        </div>



    </div>
    <div class="row">

        @include('partials.fields.h4-separator', ['label' => 'Run Time',])

        <!--
        @include('partials.fields.select', [
            'label' => 'Minutes From',
            'name' => 'minutes_from',
            'column' => 6,
            'column_label' => 4,
            'list' => [
                ['id' => '00', 'text' => '00'],
                ['id' => '05', 'text' => '05'],
                ['id' => '10', 'text' => '10'],
                ['id' => '15', 'text' => '15'],
                ['id' => '20', 'text' => '20'],
                ['id' => '25', 'text' => '25'],
                ['id' => '30', 'text' => '30'],
                ['id' => '35', 'text' => '35'],
                ['id' => '40', 'text' => '40'],
                ['id' => '45', 'text' => '45'],
                ['id' => '50', 'text' => '50'],
                ['id' => '55', 'text' => '55'],
                ['id' => '55', 'text' => '60'],
            ],
            'value' => isset($task) && isset($task->minutes_from) ? $task->minutes_from : -1
        ])

        @include('partials.fields.select', [
            'label' => 'Until',
            'name' => 'minutes_until',
            'column' => 6,
            'column_label' => 4,
            'list' => [
                ['id' => '00', 'text' => '00'],
                ['id' => '05', 'text' => '05'],
                ['id' => '10', 'text' => '10'],
                ['id' => '15', 'text' => '15'],
                ['id' => '20', 'text' => '20'],
                ['id' => '25', 'text' => '25'],
                ['id' => '30', 'text' => '30'],
                ['id' => '35', 'text' => '35'],
                ['id' => '40', 'text' => '40'],
                ['id' => '45', 'text' => '45'],
                ['id' => '50', 'text' => '50'],
                ['id' => '55', 'text' => '55'],
                ['id' => '55', 'text' => '60'],
            ],
            'value' => isset($task) && isset($task->minutes_until) ? $task->minutes_until : -1
        ])


        @include('partials.fields.select', [
            'label' => 'Hours From',
            'name' => 'hours_from',
            'column' => 6,
            'column_label' => 4,
            'list' => [
                ['id' => '00', 'text' => '00'],
                ['id' => '01', 'text' => '01'],
                ['id' => '02', 'text' => '02'],
                ['id' => '03', 'text' => '03'],
                ['id' => '04', 'text' => '04'],
                ['id' => '05', 'text' => '05'],
                ['id' => '06', 'text' => '06'],
                ['id' => '07', 'text' => '07'],
                ['id' => '08', 'text' => '08'],
                ['id' => '09', 'text' => '09'],
                ['id' => '10', 'text' => '10'],
                ['id' => '11', 'text' => '11'],
                ['id' => '12', 'text' => '12'],
                ['id' => '13', 'text' => '13'],
                ['id' => '14', 'text' => '14'],
                ['id' => '15', 'text' => '15'],
                ['id' => '16', 'text' => '16'],
                ['id' => '17', 'text' => '17'],
                ['id' => '18', 'text' => '18'],
                ['id' => '19', 'text' => '19'],
                ['id' => '20', 'text' => '20'],
                ['id' => '21', 'text' => '21'],
                ['id' => '22', 'text' => '22'],
                ['id' => '23', 'text' => '23'],
            ],
            'value' => isset($task) && isset($task->hours_from) ? $task->hours_from : -1
        ])

        @include('partials.fields.select', [
            'label' => 'Until',
            'name' => 'hours_until',
            'column' => 6,
            'column_label' => 4,
            'list' => [
                ['id' => '00', 'text' => '00'],
                ['id' => '01', 'text' => '01'],
                ['id' => '02', 'text' => '02'],
                ['id' => '03', 'text' => '03'],
                ['id' => '04', 'text' => '04'],
                ['id' => '05', 'text' => '05'],
                ['id' => '06', 'text' => '06'],
                ['id' => '07', 'text' => '07'],
                ['id' => '08', 'text' => '08'],
                ['id' => '09', 'text' => '09'],
                ['id' => '10', 'text' => '10'],
                ['id' => '11', 'text' => '11'],
                ['id' => '12', 'text' => '12'],
                ['id' => '13', 'text' => '13'],
                ['id' => '14', 'text' => '14'],
                ['id' => '15', 'text' => '15'],
                ['id' => '16', 'text' => '16'],
                ['id' => '17', 'text' => '17'],
                ['id' => '18', 'text' => '18'],
                ['id' => '19', 'text' => '19'],
                ['id' => '20', 'text' => '20'],
                ['id' => '21', 'text' => '21'],
                ['id' => '22', 'text' => '22'],
                ['id' => '23', 'text' => '23'],
            ],
            'value' => isset($task) && isset($task->hours_until) ? $task->hours_until : -1
        ])
                -->


        @include('partials.fields.select', [
            'label' => 'Days From',
            'name' => 'days_from',
            'column' => 6,
            'column_label' => 4,
            'list' => [
                ['id' => 0, 'text' => 'Sunday'],
                ['id' => 1, 'text' => 'Monday'],
                ['id' => 2, 'text' => 'Tuesday'],
                ['id' => 3, 'text' => 'Wednesday'],
                ['id' => 4, 'text' => 'Thursday'],
                ['id' => 5, 'text' => 'Friday'],
                ['id' => 6, 'text' => 'Saturday'],
            ],
            'value' => isset($task) && isset($task->days_from) ? $task->days_from : -1
        ])

        @include('partials.fields.select', [
            'label' => 'Until',
            'name' => 'days_until',
            'column' => 6,
            'column_label' => 3,
            'list' => [
                ['id' => 0, 'text' => 'Sunday'],
                ['id' => 1, 'text' => 'Monday'],
                ['id' => 2, 'text' => 'Tuesday'],
                ['id' => 3, 'text' => 'Wednesday'],
                ['id' => 4, 'text' => 'Thursday'],
                ['id' => 5, 'text' => 'Friday'],
                ['id' => 6, 'text' => 'Saturday'],
            ],
            'value' => isset($task) && isset($task->days_until) ? $task->days_until : -1
        ])


        @include('partials.fields.input-timepicker', [
            'name' => 'run_time_start',
            'label' => 'Run Time Start',
            'time' => true,
            'form_line' => true,
            'column' => 6,
            'column_label' => 4,
            'value' => isset($task) && isset($task->run_time_start) ? $task->run_time_start : "00:00"
        ])

        @include('partials.fields.input-timepicker', [
            'name' => 'run_time_stop',
            'label' => 'Run Time End',
            'time' => true,
            'form_line' => true,
            'column' => 6,
            'column_label' => 3,
            'value' => isset($task) && isset($task->run_time_stop) ? $task->run_time_stop : "23:59"
        ])

        @include('partials.fields.select', [
            'label' => 'Repeat Delay',
            'name' => 'repeat_delay',
            'column' => 6,
            'column_label' => 4,
            'required' => true,
            'list' => [
                ['id' => 1, 'text' => '1 Minutes'],
                ['id' => 2, 'text' => '2 Minutes'],
                ['id' => 3, 'text' => '3 Minutes'],
                ['id' => 4, 'text' => '4 Minutes'],
                ['id' => 5, 'text' => '5 Minutes'],
                ['id' => 6, 'text' => '6 Minutes'],
                ['id' => 7, 'text' => '7 Minutes'],
                ['id' => 8, 'text' => '8 Minutes'],
                ['id' => 9, 'text' => '9 Minutes'],
                ['id' => 10, 'text' => '10 Minutes'],
                ['id' => 15, 'text' => '15 Minutes'],
                ['id' => 20, 'text' => '20 Minutes'],
                ['id' => 30, 'text' => '30 Minutes'],
                ['id' => 60, 'text' => '60 Minutes'],
                ['id' => 120, 'text' => '2 Hours'],
                ['id' => 240, 'text' => '4 Hours'],
                ['id' => 360, 'text' => '6 Hours'],
                ['id' => 720, 'text' => '12 Hours'],
                ['id' => 1440, 'text' => '1 Day'],
            ],
            'value' => isset($task) && isset($task->repeat_delay) ? $task->repeat_delay : 60
        ])

        @include('partials.fields.select', [
            'label' => 'Day of Week',
            'name' => 'day_of_week',
            'column' => 6,
            'column_label' => 3,
            'list' => [
                ['id' => 0, 'text' => 'Sunday'],
                ['id' => 1, 'text' => 'Monday'],
                ['id' => 2, 'text' => 'Tuesday'],
                ['id' => 3, 'text' => 'Wednesday'],
                ['id' => 4, 'text' => 'Thursday'],
                ['id' => 5, 'text' => 'Friday'],
                ['id' => 6, 'text' => 'Saturday'],
            ],
            'value' => isset($task) && isset($task->day_of_week) ? $task->day_of_week : -1
        ])


        </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($task) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
