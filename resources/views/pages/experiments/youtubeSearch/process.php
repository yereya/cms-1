
<?php


// Set updated API Key

$apikey = 'AIzaSyAaA2gb6w1Rkwfbd5ExRbyT1FMzWLH-92U';

# --- Prepare dictionary for processing the data ---
$dict = array(
    'q' => '',
    'maxResults' => '',
    'region' => '',
    'relevanceLanguage' => '',
    'location' => '',
    'locationRadius ' => '',
    'publishedAfter' => '',
    'publishedBefore' => '',
    'order' => ''
);

# --- Query fields ---
$fields = array('kind' => '','videoId' => '','publishedAt' => '','channelId' => '' ,'title' => '','description' =>'','channelTitle'=>'','url' => '','viewCount'=>'','likeCount'=>'','dislikeCount'=>'','commentCount' => '');

# --- Generate URL based on user's input ---
if (isset($_POST['q']) and $_POST['q'] != "" && isset($_POST['maxResults']) and $_POST['maxResults'] != "") {
    $htmlBody = '';
    $videos = '';
    $_POST['q'] = rawurlencode($_POST['q']);
    $googleApiURL = 'https://www.googleapis.com/youtube/v3/search?type=video&part=snippet' . '&key=' . $apikey;
    foreach ($dict as $key => $val){
        if (isset($_POST[$key]) and $_POST[$key] != ""){
            switch($key){
                case 'maxResults':
                    $dict[$key] = $_POST[$key] > 50 ? 50 : $_POST[$key];
                    break;
                case 'publishedAfter':
                    $dict[$key] = date("Y-m-d", strtotime($dict[$key])) . 'T16%3A17%3A19.000Z' ;
                    break;
                case 'publishedBefore':
                    $dict[$key] = date("Y-m-d", strtotime($dict[$key])) . 'T16%3A17%3A19.000Z' ;
                    break;
                default:
                    $dict[$key] = $_POST[$key] ;
                    break;
            }

            $googleApiURL .= '&' . $key . '=' . $dict[$key];
        }
    }
    $full_items_list = []; // Store full list of videos with pagination
    $gURL_no_pageToken = $googleApiURL; // Store original google's url
    do{
        # --- Send Request to get the data ---
        $response_search = file_get_contents($googleApiURL);
        $items = json_decode($response_search, true)['items'];

        # --- Get list of video's ids ---
        $statistics = [];
        $video_ids = array_map(function ($ar) {
            if($ar['id']['videoId'])  return $ar['id']['videoId'];
        }, $items);

        # --- Get Statistics for videos ---
        $video_ids = implode(",",$video_ids);
        $url = 'https://www.googleapis.com/youtube/v3/videos?key=AIzaSyC4x2YG0o1kVn-S1kCg8uyFCQk65DPMvPo&part=snippet,statistics,snippet&id='.$video_ids;
        $response_videos = file_get_contents($url);
        $videos_statistics = json_decode($response_videos, true)['items'];

        $full_items_list = array_merge($full_items_list,$videos_statistics); // Add current's page items to the full list of videos

        $next_page_token = json_decode($response_search, true)['nextPageToken'];
        if($next_page_token){
            $googleApiURL = $gURL_no_pageToken . '&pageToken=' . $next_page_token;
        }
    }while($next_page_token and count($items) != 0 and count($full_items_list) < $_POST['maxResults']);

    $_POST['q'] = rawurldecode($_POST['q']);

    # --- Set Headers for csv file ---
    $headrs = array_keys($fields);
    array_push($headrs,'');

    # --- Show search params ---
    $htmlBody .= "<div class='left_side_params'><table>";
    $htmlBody .= '<tr>' . '<th>' . '<u>Parameter</u>' . '</th>' . '<th>' . '<u>Value</u>' . '</th>';
    foreach ($dict as $key => $val){
        if (isset($_POST[$key]) and $_POST[$key] != ""){
            array_push($headrs,$key);
            $dict[$key] = $_POST[$key] ;
            $htmlBody .= '<tr class="tr_paramas">' .'<td class="td_paramas">' . $key . '</td>' . '<td class="td_paramas">' . $dict[$key] .'</td>' .'</tr>';
        }
    }
    $htmlBody .= "</table></div>";

    # --- Create storage folder and path ---
    $report_folder = 'reports'.DIRECTORY_SEPARATOR.'youtubeSearch';
    Storage::makeDirectory($report_folder);
    $report_folder_path = storage_path('app') . DIRECTORY_SEPARATOR . $report_folder;
    $file_name = $report_folder_path.DIRECTORY_SEPARATOR.'output.csv';

    # --- Create CSV file ---
    $first_row = true;
    $fp = fopen($file_name, 'w');
    fputcsv($fp,$headrs);
    foreach ($full_items_list as $item){
        $merged = array_merge($item['statistics'],$item['snippet'],array('videoId'=>$item['id'],'kind'=>$item['kind']));
        foreach ($fields as $key => $val){
            if(isset($merged[$key])){
                $fields[$key] = $merged[$key];
            }
        }
        $fields['url'] = $fields['videoId'] ? 'https://youtube.com/watch?v='. $fields['videoId'] : "" ;
        $values = array_values($fields);
        if($first_row){
            array_push($values,'');
            foreach ($dict as $key => $val){
                if ($dict[$key] != "") {
                    array_push($values,$dict[$key]);
                }
            }
            $first_row = false;
        }
        fputcsv($fp,$values);
        $fields = array('kind' => '','videoId' => '','publishedAt' => '','channelId' => '' ,'title' => '','description' =>'','channelTitle'=>'','url' => '','viewCount'=>'','likeCount'=>'','dislikeCount'=>'','commentCount' => '');
    }
    fclose($fp);

    # --- Generate list of videos on the page ---

    for($i = 0; $i <= 10 ;$i++){
        $videos .= sprintf('<ul class="ulvideo"> <b><h3> %s </h3></b>', $full_items_list[$i]['snippet']['title']);
        $videos .= sprintf('<li class="livideo"> <b>Published At</b> : %s</li>',
            $full_items_list[$i]['snippet']['publishedAt']);
        $videos .= sprintf('<li class="livideo"> <b>Channel Title</b> : %s</li>',
            $full_items_list[$i]['snippet']['channelTitle']);
        $videos .= sprintf('<li class="livideo"> <b><Details><summary>Description</b> </summary> %s </Details></li>',
            $full_items_list[$i]['snippet']['description']);

        $videos .= sprintf('<li class="livideo"> <b>viewCount</b> : %s</li>',
            isset($full_items_list[$i]['statistics']['viewCount'])?$full_items_list[$i]['statistics']['viewCount']:'No Information');
        $videos .= sprintf('<li class="livideo"> <b>likeCount</b> : %s</li>',
            isset($full_items_list[$i]['statistics']['likeCount'])?$full_items_list[$i]['statistics']['likeCount']:'No Information');
        $videos .= sprintf('<li class="livideo"> <b>dislikeCount</b> : %s</li>',
            isset($full_items_list[$i]['statistics']['dislikeCount'])?$full_items_list[$i]['statistics']['dislikeCount']:'No Information');
        $videos .= sprintf('<li class="livideo"> <b>commentCount</b> : %s</li>',
            isset($full_items_list[$i]['statistics']['commentCount'])?$full_items_list[$i]['statistics']['commentCount']:'No Information');
        $videos .= sprintf('</ul>');

    }
    $htmlBody .= <<<END
    <div class="list_of_videos">
    <h2><u>List Of Videos</u></h2>
    <form action="" method="post">
    <input type="hidden" name="search" value="0">
    <input type="hidden" name="file_name" value=$file_name>
    <input class="download-file" type="submit" name="submit" value="Download File" />
    </form>       
   
        $videos
        </div>
END;
}

# --- Show list of videos ---
if(isset($htmlBody)){
    echo $htmlBody;
}
?>


