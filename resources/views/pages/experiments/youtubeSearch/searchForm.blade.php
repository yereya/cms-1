
<ul class="ull">
    <li class="ill"><a class="ill" href="/youtubeSearch">Home</a></li>
</ul><br><br><br>


<form method="post" action="/youtubeSearch"  >
    <div class="container">
        <div> <h2 class="search-app"><u>Youtube Search Application</u> </h2></div>
        <div class="form-insertion">
            <div style="margin-bottom:10px ">
                <b><label for="q">Search Term: </label></b><input type="search" id="q" name="q" placeholder="Enter Search Term">
            </div>
            <div>
                <details>
                    <summary><b>Details</b></summary>
                    <p class="pdetails">
                        Your request can also use the Boolean NOT (-) and OR (|) operators to exclude videos or to find videos that are associated with one of several search terms.<br>
                        For example, to search for videos matching either "boating" or "sailing", set the q parameter value to boating|sailing.<br>
                        Similarly, to search for videos matching either "boating" or "sailing" but not "fishing",<br>
                        set the q parameter value to <b>boating|sailing -fishing.</b>
                    </p>
                </details>
            </div><br>
            <div>
                <details>
                    <summary><b>Max Results: </b><input type="number" id="maxResults" name="maxResults" min="1" max="500" step="1" value="500"> Max allowed value - 500</summary>
                    <p>
                        The maxResults parameter specifies the maximum number of items that should be returned in the result set.<br>
                        Acceptable values are 0 to 50, inclusive. The default value is 5.
                    </p>
                </details>

            </div><br>
            <div>
                <details>
                    <summary><b>Region</b>: <select name="region">
                            <option value=""> Default </option>
                            <option value="UM"> USA </option>
                            <option value="CA"> Canada </option>
                            <option value="IL"> Israel </option>
                            <option value="GB"> UK </option>
                            <option value="FR"> France </option>
                        </select></summary><br><br>
                    <p class="pdetails">
                        The regionCode parameter instructs the API to return search results for videos that can be viewed in the specified country.<br>
                        The parameter value is an ISO 3166-1 alpha-2 country code.
                    </p>
                </details>
            </div><br>
            <div>
                <details>
                    <summary><b>Relevance Language: </b><select name="relevanceLanguage">
                            <option value="en"> English </option>
                            <option value="he"> Hebrew </option>
                            <option value="fr"> French </option>
                        </select><br><br></summary>
                    <p class="pdetails">The relevanceLanguage parameter instructs the API to return search results that are most relevant to the specified language.<br>
                        The parameter value is typically an ISO 639-1 two-letter language code. However, you should use the values zh-Hans for simplified Chinese and zh-Hant for traditional Chinese.<br>
                        Please note that results in other languages will still be returned if they are highly relevant to the search query term.<br></p>
                </details>
            </div>
            <div>
                <details>
                    <summary><b>Location</b> - Latitude : <input type="text" name="latitude" placeholder="Enter Latitude"> Longitude : <input type="text" name="longitude " placeholder="Enter Longitude ">
                        <strong> e.g. (37.42307,-122.08427) </strong><br> </summary>
                    <p class="pdetails">
                        The location parameter, in conjunction with the locationRadius parameter, defines a circular geographic area and also restricts a search to videos that specify,<br>
                        in their metadata, a geographic location that falls within that area. The parameter value is a string that specifies latitude/longitude coordinates e.g. (37.42307,-122.08427).<br>
                        <br>
                        The <b>location</b> parameter value identifies the point at the center of the area.<br>
                        The <b>locationRadius</b> parameter specifies the maximum distance that the location associated with
                        a video can be from that point for the video to still be included in the search results.<br></p>
                </details><br>
            </div>
            <div>
                <details>
                    <summary> <b>Location Radius </b><input type="text" name="locationRadius" placeholder="Enter Location Radius">
                        <strong>e.g,  1500m, 5km, 10000ft, and 0.75mi.</strong><br>
                    </summary>
                    <p class="pdetails">
                        The locationRadius parameter, in conjunction with the location parameter, defines a circular geographic area.<br>
                        The parameter value must be a floating point number followed by a measurement unit.<br>
                        Valid measurement units are m, km, ft, and mi.<br>
                        The API does not support locationRadius parameter values larger than 1000 kilometers.
                    </p>
                </details>
            </div><br>
            <div>
                <details>
                    <summary>
                        <b>Published Before</b> : <input type="date" name="publishedBefore" > <b> .e.g 01/01/2019</b>
                    </summary>
                    <p class="pdetails">
                        The publishedBefore parameter indicates that the API response should only contain resources created before or at the specified time.<br>
                    </p>
                </details>
            </div><br>
            <div>
                <details>
                    <summary>
                        <b>Published After</b> : <input type="date" name="publishedAfter" > <b> .e.g 01/01/2019</b>
                    </summary>
                    <p class="pdetails">
                        The publishedAfter parameter indicates that the API response should only contain resources created at or after the specified time.<br>
                    </p>
                </details>
            </div><br>
            <div>
                <details>
                    <summary>
                        <b>Order, </b>Choose preferred order : <select name="order">
                            <option value=""> Default </option>
                            <option value="date"> date </option>
                            <option value="rating"> rating </option>
                            <option value="relevance"> relevance </option>
                            <option value="title"> title </option>
                            <option value="videoCount"> videoCount </option>
                            <option value="viewCount"> viewCount </option>
                        </select> <br>
                    </summary>
                    <p class="pdetails">
                        The order parameter specifies the method that will be used to order resources in the API response. The default value is relevance.<br>
                    <ul><b>Acceptable values are:</b>
                        <li>date – Resources are sorted in reverse chronological order based on the date they were created.<br></li>
                        <li>rating – Resources are sorted from highest to lowest rating.<br></li>
                        <li>relevance – Resources are sorted based on their relevance to the search query. This is the default value for this parameter.<br></li>
                        <li>title – Resources are sorted alphabetically by title.<br></li>
                        <li>videoCount – Channels are sorted in descending order of their number of uploaded videos.</li>
                        <li>viewCount – Resources are sorted from highest to lowest number of views. For live broadcasts, videos are sorted by number of concurrent viewers while the broadcasts are ongoing.</li>
                    </ul>
                    </p>
                </details>
            </div><br>

            <div class="search-btn">
                <input type="hidden" name="search" value="1">
                <input type="submit" value="Search">
            </div>
        </div>
    </div>
</form>