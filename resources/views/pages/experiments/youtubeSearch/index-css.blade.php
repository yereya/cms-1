<style>
    html {

        background-image: url("assets/pages/img/bg_masthead_background.jpg") ;
        background-size:100%;
        background-repeat:no-repeat;
        -webkit-background-size:cover;
        -moz-background-size:cover;
        -o-background-size:cover;

    }
    body{
        background: unset;
    }
    form{
        padding: 30px;
        margin: auto;
        width: 80%;
    }
    h1 {
        background: cornsilk;
        color: blue;
        text-align: center;
        outline-style: solid;
        outline-color: black;
    }

    .bg {
        /* Full height */
        height: 100%;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    h2 {
        color:#111111;
        font-family: 'Source Sans Pro', sans-serif;
        font-size: 32px;
        font-weight: 400;
        line-height: 32px;
        margin-left: 180px;
    }
    dd{
        border: 1px solid;
        padding: 10px;
    }
    .form-insertion{

    }
    .container{
        margin-top: 20px;
        margin-left: 35%;
    }
    .pdetails{
        border: 1px solid;
        padding: 10px;
        margin: 10px;
    }
    .ulvideo {
        background: #80bdff;
        border: 1px 1px solid black;
        border-radius: 10px;
        padding: 10px;
        margin-right: 45%;
    }
    .livideo {
        list-style-type: none;
        color: black;
        font: inherit
    }
    .list_of_videos{
        margin-top: 100px;
        margin-left: 40%;
    }
    .collapse:not(.show) {
        display: none;
    }

    .search-btn{
        margin-left: 100px;
    }
    .search-app{
        padding: 10px;
        margin-bottom: 50px;
    }
    input, textarea,select {

        background-color : #ecb807;

    }
    input[type=submit] {
        color: white;
        background: #3387c4;
        border: none;
        padding: 5px !important;
        cursor: pointer;
        font:bold;
        box-sizing: border-box;
        font-size: 20px;
        border-radius: 8px;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    input.download-file {
        font-size:15px;
        margin-left: 120px ;
    }


    .ull {
        list-style-type: none;

    }
    a.ill{
        font-weight: bold;
        font-size: 20px;
        text-decoration:none;
        color: #0056b3;
        text-align: start;
    }

    .left_side_params{
        margin-top: 240px;
        margin-left: 50px;
        float: left;
        background: #2f96b4;
        border: 1px 1px solid black;
        border-radius: 10px;
    }
    .tr_paramas{
       margin-top: 10px;
    }

    .td_paramas{
        margin: 10px;
        padding: 10px;
        font-weight: bold;
        overflow: hidden;
        white-space: nowrap;
        width: 100px;
        max-width: 150px;
        text-align: center;
    }

</style>