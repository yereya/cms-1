
<!DOCTYPE html>
<html>
<head>
    <title>Youtube Search</title>
{{--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">--}}
    @include('pages.experiments.youtubeSearch.index-css')

</head>
<body>

    @include('pages.experiments.youtubeSearch.searchForm')

    @include('pages.experiments.youtubeSearch.process')


</body>
</html>