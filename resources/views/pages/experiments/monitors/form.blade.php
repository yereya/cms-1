@extends('layouts.metronic.main')
@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Monitor Form'
    ])

    <?php
            $can_edit = auth()->user()->can($permission_name . ".edit");
    ?>

    {!! Form::open([
        'route' => isset($monitor) ? ['experiments.monitors.update', $monitor->id] : 'experiments.monitors.store',
        'method' => isset($monitor) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="row">
        @include('partials.fields.input', [
            'label' => 'Script Name',
            'name' => 'name',
            'id' => 'script_name',
            'disabled'=> !$can_edit,
            'required' => true,
            'value' => isset($monitor) && isset($monitor->name) ? $monitor->name : ''
        ])

        @include('partials.fields.select', [
            'label' => 'Department',
            'name' => 'department',
            'id' => 'department',
            'disabled'=> !$can_edit,
            'required' => true,
            'list' => $departments,
            'value' => isset($monitor) && isset($monitor->department) ? $monitor->department : ''
        ])

        @if(isset($monitor) && isset($monitor->category))
            @include('partials.fields.select', [
            'label' => 'Category',
            'name' => 'category',
            'id' => 'category',
            'disabled'=> !$can_edit,
            'required' => true,
            'class' => 'select_conditioner select_where_field',
            'ajax' => true,
            'value' => $monitor->category,
            'list' =>[['id'=>$monitor->category, 'text'=>$monitor->category]],
            'attributes' => [
                'data-dependency_selector' => '#department',
                'data-method' => 'categories',
                'data-api_url' => route('experiments.monitors.select2')
                ]
            ])
        @else
            @include('partials.fields.select', [
            'label' => 'Category',
            'name' => 'category',
            'id' => 'category',
            'disabled'=> !$can_edit,
            'required' => true,
            'class' => 'select_conditioner select_where_field',
            'ajax' => true,
            'value' => '',
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector' => '#department',
                'data-method' => 'categories',
                'data-api_url' => route('experiments.monitors.select2'),
                ]
            ])
        @endif

        @include('partials.fields.input', [
            'label' => 'Target & Purpose',
            'name' => 'target',
            'id' => 'target',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->target) ? $monitor->target : ''
        ])

        @include('partials.fields.input', [
            'label' => 'Problem Script Intents To Solve',
            'name' => 'problem',
            'id' => 'problem',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->problem) ? $monitor->problem : ''
        ])

        @include('partials.fields.input', [
            'label' => 'Query ID',
            'name' => 'query_id',
            'id' => 'query_id',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->query_id) ? $monitor->query_id : ''
        ])

        @include('partials.fields.input', [
            'label' => 'Date Range',
            'name' => 'date_range',
            'id' => 'date_range',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->date_range) ? $monitor->date_range: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Segmentation Level',
            'name' => 'segmentation_level',
            'id' => 'segmentation_level',
            'disabled'=> !$can_edit,
            'help_text' => 'campaign,adgroup, ST,click,country etc...',
            'value' => isset($monitor) && isset($monitor->segmentation_level) ? $monitor->segmentation_level: ''
       ])

        @include('partials.fields.input', [
            'label' => 'KPIs To Calculate',
            'name' => 'calculate_kpi',
            'id' => 'calculate_kpi',
            'disabled'=> !$can_edit,
            'help_text' => 'KPIs to calculate in script (could be more than 1)',
            'value' => isset($monitor) && isset($monitor->calculate_kpi) ? $monitor->calculate_kpi: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Table',
            'name' => 'table',
            'id' => 'table',
            'disabled'=> !$can_edit,
            'help_text' => 'table to work on, can have more than one if needed',
            'value' => isset($monitor) && isset($monitor->table) ? $monitor->table: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Data Calculation',
            'name' => 'data_calculation',
            'id' => 'data_calculation',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->data_calculation) ? $monitor->data_calculation: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Filter',
            'name' => 'filter',
            'id' => 'filter',
            'disabled'=> !$can_edit,
            'help_text' => 'statistic rule (amount,% of,binom model significate , T.test )',
            'value' => isset($monitor) && isset($monitor->filter) ? $monitor->filter: ''
       ])


        @include('partials.fields.input', [
            'label' => 'Aggregation Type',
            'name' => 'aggregation_type',
            'id' => 'aggregation_type',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->aggregation_type) ? $monitor->aggregation_type: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Unique Key',
            'name' => 'unique_key',
            'id' => 'unique_key',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->unique_key) ? $monitor->unique_key: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Sending Monitor',
            'name' => 'sending_monitor',
            'id' => 'sending_monitor',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->sending_monitor) ? $monitor->sending_monitor: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Run Time',
            'name' => 'run_time',
            'id' => 'run_time',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->run_time) ? $monitor->run_time: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Advertiser',
            'name' => 'advertiser',
            'id' => 'advertiser',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->advertiser) ? $monitor->advertiser: ''
       ])

        @include('partials.fields.input', [
            'label' => 'recommendation',
            'name' => 'recommendation',
            'id' => 'recommendation',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->recommendation) ? $monitor->recommendation: ''
       ])

        @include('partials.fields.input', [
            'label' => 'New Advertiser',
            'name' => 'new_advertiser',
            'id' => 'new_advertiser',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->new_advertiser) ? $monitor->new_advertiser: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Definition',
            'name' => 'definition',
            'id' => 'definition',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->definition) ? $monitor->definition: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Report Column',
            'name' => 'report_column',
            'id' => 'report_column',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->report_column) ? $monitor->report_column: ''
       ])

        @include('partials.fields.input', [
            'label' => 'Comment',
            'name' => 'comment',
            'id' => 'comment',
            'disabled'=> !$can_edit,
            'value' => isset($monitor) && isset($monitor->comment) ? $monitor->comment: ''
       ])
    </div>

    @if(isset($can_edit) && $can_edit)

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url()
                ],
                'submit' => 'true'
            ]
        ])

    @else

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'custom' => [
                    [
                    'type' => 'close',
                    'url' => route('experiments.monitors.index'),
                    'class' => 'btn-info',
                    'value' => 'Close'
                    ]
                ],
            ]
        ])

    @endif


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop