@extends('layouts.metronic.main')

@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Monitors',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('experiments.monitors.create'),
                'permission' => auth()->user()->can($permission_name.'.edit')
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $monitors,
        'id' => 'experiments_monitor',
        'columns' =>[
               [
                   'key' => 'id',
                   'value' => function ($data) {
                       return $data->id;
                   }
               ],
               [
                   'key' => 'name',
                   'value' => function ($data) {
                       return "<a href='".route('experiments.monitors.create',['id' => $data->id])."'>". $data->name . "</a>";
                   }
               ],
               [
                   'key' => 'department',
                   'value' => function ($data) {
                       return $data->department;
                   }
               ],
               [
                   'key' => 'category',
                   'value' => function ($data) {
                       return $data->category;
                   }
               ],
               [
                   'key' => 'actions',
                   'value' => function ($data) {
                        return "<a class='tooltips'
                        data-original-title='Delete Monitor'
                        data-target='sweetAlert'
                        data-alert_title='Delete Monitor'
                        data-alert_type='warning'
                        data-alert_href='".route("experiments.monitors.{monitor_id}.ajax-alert",
                        [$data->id,'method=destroyMonitor'])."'><span class='icon-trash'></span></a>";
                   }
               ]
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop