@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Permissions',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('permissions.create')
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $permissions,
        'attributes' => [
            'data-page-length' => 20,
        ],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) use ($permission_name) {
                    $params = func_get_args();
                    $permission_name = isset($params[1]['permission_name']) ? $params[1]['permission_name'] : null;
                    if (auth()->user()->can($permission_name.'.edit')) {
                        return "<a href=\"". route('permissions.edit', [$data->id]) ."\">{$data->name}</a> ";
                    }
                    return $data->name;
                }
            ],
            [
                'key' => 'Type',
                'value' => function ($data) {
                    return $data->type;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= "<a href=\"". route('permissions.edit', [$data->id]) ."\"><span aria-hidden=\"true\" class=\"icon-pencil\"></span></a> ";
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res.="<a class='tooltips'
                        data-original-title='Delete Permission'
                        data-target='sweetAlert'
                        data-alert_title='Delete Permission'
                        data-alert_type='warning'
                        data-alert_href='".route('permissions.{permission_id}.ajax-alert', [$data->id, 'method=deletePermission'])."'><span class='icon-trash'></span></a>";
                    }

                    return $res;
                }
            ]
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop