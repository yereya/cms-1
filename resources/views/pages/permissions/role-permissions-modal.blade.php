@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'User Permissions',
])


    @include('partials.containers.dropdown-buttons', [
        'side' => 'right',
        'dropdown_buttons' => [
            [
                'icon' => 'glyphicon glyphicon-check',
                'title' => 'Select All',
                'attributes' => [
                    'onClick' => "App.toggleAllCheckBoxes('#role_permission_table');"
                ]
            ],
            [
                'icon' => 'glyphicon glyphicon-unchecked',
                'title' => 'Unselect All',
                'attributes' => [
                    'onClick' => "App.toggleAllCheckBoxes('#role_permission_table', false);"
                ]
            ],
        ]
    ])

    @include('partials.containers.data-table', [
        'id' => 'role_permission_table',
        'data' => $permissions,
        'class' => 'permissions-user',
        'columns' => [
            [
                'key' => 'Name',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $params = func_get_args();
                    $permission_name = isset($params[1]['permission_name']) ? $params[1]['permission_name'] : null;
                    if (auth()->user()->can($permission_name.'.edit')) {
                        return "<a href=\"". route('permissions.edit', [$data->id]) ."\">{$data->name}</a> ";
                    }
                    return $data->name;
                }
            ],
            [
                'key' => 'Type',
                'value' => function ($data) {
                    return $data->type;
                }
            ],
            [
                'label' => 'Has Role',
                'filter_type' => 'null',
                'value' => function ($data) use ($role_permissions, $role_id) {
                    $state = isset($role_permissions[$data->id]) ? 'checked' : '';

                    $res = "<a href='javascript:void(0);'
                            data-property_toggle='". route('permissions.{role_id}.toggle-property',[
                            $role_id, "action=rolePermissions&role_id={$role_id}&permission_id=". $data->id]) ."'>";
                    $res .= "<span class='toggle_checkbox' data-toggle_state='{$state}'> </span></a>";
                    return $res;
                }
            ]
        ]
    ])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => [
            'class' => 'permissions-user'
        ]
    ]

])



