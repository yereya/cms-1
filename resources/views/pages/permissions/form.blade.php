@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Permission Form'
    ])

    {!! Form::open([
        'route' => isset($permission) ? ['permissions.update', $permission->id] : 'permissions.store',
        'method' => isset($permission) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.select', [
            'name' => 'type',
            'list' => [
                'view' => 'View',
                'add' => 'Add',
                'edit' => 'Edit',
                'delete' => 'Delete',
                'download' => 'Download'
            ],
            'value' => isset($permission) ? $permission->type : null
        ])
        @include('partials.fields.input', [
            'name' => 'name',
            'value' => isset($permission) ? $permission->name : null
        ])

        @include('partials.fields.textarea', [
            'name' => 'description',
            'value' => isset($permission) ? $permission->description : null
        ])

        @include('partials.fields.select', [
            'name' => 'roles_id',
            'label' => 'Roles',
            'list' => App\Entities\Models\Users\AccessControl\Role::pluck('name', 'id'),
            'multi_select' => true,
            'value' => isset($permission) ? $permission->roles : null
        ])
    </div>


    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($permission) ? null : 'true'
        ]
    ])


    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop