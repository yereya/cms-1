@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'User Permissions',
])

    @include('partials.containers.data-table', [
        'data' => $permissions,
        'class' => 'permissions-user',
        'columns' => [
            [
                'key' => 'id',
            ],
            [
                'key' => 'name',
            ],
            [
                'key' => 'type',
            ],
            [
                'key' => 'description',
                'label' => 'Description',
                'filter_type' => 'null',
            ],
            [
                'key' => 'has_permission',
                'label' => 'From Role',
                'value' => function ($data) use ($user_permissions, $roles) {
                    $res = '';
                    $current_permission = $user_permissions->get($data->id);
                    if ($current_permission) {
                        $role_id = $current_permission->pivot->role_id;
                        $role_name = $roles->get($role_id) ? $roles->get($role_id)->name : 'no role attached' ;
                        $res .= "<span class='label label-sm label-success'> {$role_name} </span>";
                    }

                    return $res;
                }
            ],
            [
                'key' => 'has_permissions',
                'label' => 'Toggle',
                'filter_type' => 'null',
                'value' => function ($data) use ($user_permissions, $user) {
                    $state = $data['has_permissions'] ? 'checked' : '';
                    $res = "<a href='javascript:void(0);'
                        data-property_toggle='". route('users.{user_id}.toggle-property', [
                            'user_id' => $user->id,
                            'action'=> 'userPermissions',
                            'permission_id' => $data['id'],
                            'site_id' => 1,
                            ]) ."'>
                        <span class='toggle_checkbox' data-toggle_state='{$state}'> </span></a>";
                    return $res;
                }
            ]
        ]
    ])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => [
            'class' => 'permissions-user'
        ]
    ]

])



