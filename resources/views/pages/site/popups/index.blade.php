@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => "Popup index",
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New Popup',
                'target' => route('sites.{site}.popups.create', $site_id)
            ],
        ]
    ])


    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'posts-table',
        'route' => 'sites.{site}.popups.datatable',
        'route_params' => [$site_id],
        'server_side' => true,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[1, "asc"]]',
            'data-route_method' => 'list',

            //the "processing" loader
            'data-blocking_loader' => 'true',

        ],
        'columns' => [
            'Id',
            'Template Id',
            'Name',
            'Active',
            'Width',
            'Height',
            'Start Date',
            'End Date',
            'Appear Event',
            'param',
            'Cookie Time On Conversion',
            'Cookie Time On Close',
            'Actions',
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop


