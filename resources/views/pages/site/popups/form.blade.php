@extends('layouts.metronic.main')

@section('page_content')
    {!! Form::open([
        'route' => isset($popup) ? ['sites.{site}.popups.update', $site_id, $popup->id] : ['sites.{site}.popups.store', $site_id],
        'method' => isset($popup) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',
        'files' => true
    ]) !!}

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Popups Form',
        'has_form' => true,
        'actions' => [
                [
                    'type' => 'submit',
                    'title' => 'Save',
                    'class' => 'btn-primary',
                    'icon' => 'fa fa-save fw'
                ],
        ]
    ])

    <div class="form-body">
        <div class="form-group">
            @include('partials.fields.input', [
                    'label' => 'Name',
                    'name' => 'name',
                    'required' => true,
                    'value' => $popup->name ?? '',
                    'type' => 'text'
                ])

            @include('partials.fields.select', [
                'name' => 'active',
                'label' => 'Active',
                'required' => true,
                'force_selection' => true,
                'value' => $popup->active ?? 0,
                'list' => ['0'=>'Not Active', 1=> 'Active'],
            ])

            @include('partials.fields.select', [
                'name' => 'template_id',
                'label' => 'Template',
                'required' => true,
                'force_selection' => true,
                'value' => $popup->template_id ?? -1,
                'list' => $templates,
            ])

            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="form_control_template_id">
                    Display
                </label>
                <div class="col-md-2">
                    @include('partials.fields.radio', [
                           'column' => 6,
                           'radio_id' => 'pivot_exclude_no',
                           'label' => 'Include',
                           'value' => '0',
                           'name' => 'pivot_exclude',
                           'checked' => !($popup->pivot_exclude ?? false)
                       ])
                    @include('partials.fields.radio', [
                           'column' => 6,
                           'radio_id' => 'pivot_exclude_yes',
                           'label' => 'Exclude',
                           'value' => '1',
                           'name' => 'pivot_exclude',
                           'checked' => $popup->pivot_exclude ?? false
                       ])
                </div>
                @include('partials.fields.select', [
                    'column' => 7,
                    'column_label' => 12,
                    'hide_label' => true,
                    'multi_select' => true,
                    'form_line' => true,
                    'name' => 'post_ids',
                    'value' => isset($popup) ? $popup->posts->pluck('id')->toArray() : -1,
                    'list' => $posts,
                ])
                <div class="col-md-12">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <br /><div class="alert alert-warning">When the Exclude option is selected without any posts, the popup will be displayed in all pages.</div>
                    </div>
                </div>
                </div>
            </div>

            @include('partials.fields.input', [
                    'label' => 'Width',
                    'name' => 'width',
                    'value' => $popup->width ?? '',
                    'type' => 'text'
                ])

            @include('partials.fields.input', [
                    'label' => 'Height',
                    'name' => 'height',
                    'value' => $popup->height ?? '',
                    'type' => 'text'
                ])

            @include('partials.fields.input-timepicker', [
                 'name' => 'start_date',
                 'datetime' => true,
                 'form_line' => true,
                 'value' => $popup->start_date ?? null
            ])

            @include('partials.fields.input-timepicker', [
                 'name' => 'end_date',
                 'datetime' => true,
                 'datetime' => true,
                 'form_line' => true,
                 'value' => $popup->end_date ?? null
            ])

            @include('partials.fields.select', [
                'name' => 'appear_event',
                'label' => 'When The Popup should display',
                'required' => 'true',
                'force_selection' => true,
                'class' => 'choice_type',
                'value' => $popup->appear_event ?? -1,
                'list' => [
                    'on_page_load' => 'On Page Load',
                    'on_mouse_exit' => 'On Mouse Exit',
                    'scroll_page' => 'On Scroll Page with Percentage',
                    'x_seconds_of_inactivity' => 'On x seconds of inactivity'
                ],
                'attributes' => [
                    'data-route' => route('sites.{site}.popups.choice', [
                        $site_id,
                        'method' => 'getParamHtml',
                        'param' => $popup->param ?? null,
                    ]),
                ]
            ])

            <fieldset id="field-metadata"></fieldset>

            @include('partials.fields.input', [
                    'label' => 'Cookie Time on Close  (days)',
                    'name' => 'cookie_time_on_conversion',
                    'value' => $popup->cookie_time_on_conversion ?? '',
                    'type' => 'text'
                ])

            @include('partials.fields.input', [
                    'label' => 'Cookie Time on Conversion (days)',
                    'name' => 'cookie_time_on_close',
                    'value' => $popup->cookie_time_on_close ?? '',
                    'type' => 'text'
                ])
        </div>
    </div>

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])

    {!! Form::close() !!}
@stop