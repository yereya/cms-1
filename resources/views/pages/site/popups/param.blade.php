@include('partials.fields.input', [
        'label' => $properties['label'],
        'required' => $properties['required'],
        'name' => 'param',
        'value' => $param ?? '',
        'type' => 'text'
    ])

