@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Sites',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.create')
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => [$site],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) {
                    return "<a href=\"". route('sites.edit', [$data->id]) ."\">{$data->name}</a>";
                }
            ],
            [
                'key' => 'Is Public',
                'value' => function ($data) {
                    if ($data->is_public) {
                        return '<span class="label label-sm label-success"> Public </span>';
                    } else {
                        return '<span class="label label-sm label-danger"> Private </span>';
                    }
                }
            ],
            [
                'key' => 'DB Host',
                'value' => function ($data) {
                    return $data->db_host;
                }
            ],
            [
                'key' => 'DB Name',
                'value' => function ($data) {
                    return $data->db_name;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    if($data->id != 1) {
                        $res .= '<a href="'. route('sites.{site}.migrations', [$data->id]) .'"
                        title="Assign migrations"
                        class="tooltips"
                        data-toggle="modal"
                        data-target="#ajax-modal">
                                <span aria-hidden="true" class="btn btn-sm default"><i class="fa fa-compress"></i> migrations</span>
                            </a> ';

                        $res .= '<a href="'. route('sites.{site}.templates.index', [$data->id]) . '"
                         title="Manage templates"
                         class="tooltips">
                                <span aria-hidden="true" class="btn btn-sm default"><i class="fa fa-file-o"></i> templates</span>
                            </a> ';

                        $res .= '<a href="'. route('sites.{site}.sections.index', [$data->id]) . '"
                        title="Manage sections"
                        class="tooltips">
                                <span aria-hidden="true" class="btn btn-sm default"><i class="fa fa-puzzle-piece"></i> sections</span>
                            </a> ';

                        $res .= '<a href="'. route('sites.{site}.content-types.index', [$data->id]) . '"
                         title="Manage content types"
                         class="tooltips">
                                <span aria-hidden="true" class="btn btn-sm default"><i class="fa fa-file-image-o"></i> content types</span>
                            </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('sites.edit', [$data->id]) . '"
                         title="Edit site"
                         class="tooltips">
                            <span aria-hidden="true" class="icon-pencil btn btn-sm grey-cascade"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res.="<a
                        title='Delete site'
                        class='tooltips'
                        data-original-title='Delete Site'
                        data-target='sweetAlert'
                        data-alert_title='Delete Site'
                        data-alert_type='warning'
                        data-alert_href='".route('sites.{site}.ajax-alert', [$data->id, 'method=deleteSite'])."'><span class='icon-trash btn btn-sm red'></span></a>";
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop