@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => !isset($menu) ? 'Create New Menu': 'Edit Menu ' . $menu->name,
        'has_form' => true,
        'actions' => !isset($menu) ? [] :
        [
            [
            'title' => ' Delete Menu',
            'class' => 'fa fa-trash red',
            'attributes' => [
                'data-alert_title' => "Delete",
                'data-alert_type' => "warning",
                'data-target' => "sweetAlert",
                'data-alert_href' => route('sites.{site}.menus.{menu}.ajax-alert', [$site->id, $menu->id, 'method=deleteMenu'])
                ]
            ]

    ]
    ])

    {!! Form::open([
        'route' => isset($menu) ? ['sites.{site}.menus.update', $site->id, $menu->id] : ['sites.{site}.menus.store', $site->id],
        'method' => isset($menu) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'name',
            'required' => 'true',
            'value' => isset($menu) ? $menu->name : null,

        ])

    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=>$site->id])
            ],
           'submit' => 'true',
        ],
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
