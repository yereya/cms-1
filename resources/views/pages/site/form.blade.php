@extends('layouts.metronic.main')
@php($user_is_superAdmin = auth()->user()->hasRole('Super Admin'))
@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($site) ? ['sites.update', $site->id] : ['sites.store'],
        'method' => isset($site) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'label' => 'Name',
            'name' => 'name',
            'required' => 'true',
            'value' => $site->name ?? null
        ])

        @include('partials.fields.input',[
            'label'=>'Domain',
            'name'=>'domain',
            'required' => 'true',
            'value'=>isset($site)
                ? $site->domains()
                    ->pluck('domain')
                    ->first()
                : null
        ])

        @if($user_is_superAdmin)
            @include('partials.fields.checkbox', [
                    'label' => 'Is Public',
                    'wrapper' => true,
                    'list' => [
                        [
                            'name' => 'is_public',
                            'value' => 1,
                            'checked' => isset($site) ? $site->is_public : 1
                        ],
                    ],
                ])

            @include('partials.fields.input', [
                'label' => 'DB Host',
                'name' => 'db_host',
                'value' => $site->db_host ?? null
            ])

            @include('partials.fields.input', [
                'label' => 'DB Name',
                'name' => 'db_name',
                'value' => $site->db_name ?? null,
                'attributes'=>[
                     !empty($site->db_name)
                     ? "readonly"
                     : ''
                ]
            ])

            @include('partials.fields.input', [
                'label' => 'DB User',
                'name' => 'db_user',
                'value' => $site->db_user ?? null
            ])

            @include('partials.fields.input', [
                'label' => 'DB Password',
                'name' => 'db_pass',
                'value' => null
            ])

        @endif
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($site) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop