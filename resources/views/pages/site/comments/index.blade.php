@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Comments',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.comments.create', [$site->id]),
                'permission' => $can['add'],
            ]
        ]
    ])

    @include('pages.site.comments.table-filter')

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'data-table-comments',
        'server_side' => true,
        'url' => route('sites.{site}.comments.datatable',[$site->id]),
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-onchange_submit' => 'true',
            'data-page-length' => '20',
            'data-route_method' => 'list',

            //the "processing" loader
            'data-blocking_loader' => 'true',
        ],
        'columns' => [
          'id',
          'post_id',
          'Author',
          'Email',
          'Content',
          'Status',
          'Visible',
          'Rating',
          'Replay To',
          'Actions',
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
