@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Replay To comment',
])

@include('partials.fields.select', [
    'name' => 'none',
    'label' => 'Post',
    'disabled' => true,
    'list' =>  $post_name ?? [],
    'value' =>  $comment_to_replay->post_id
])


@include('partials.fields.input', [
    'name' => 'none',
    'label' => 'Author',
    'disabled' => 'true',
    'value' => $comment_to_replay->author
])

@include('partials.fields.input', [
    'name' => 'none',
    'label' => 'Email',
    'disabled' => 'true',
    'value' => $comment_to_replay->email
])


@include('partials.fields.input', [
    'name' => 'none',
    'label' => 'Date',
    'disabled' => 'true',
    'value' => $comment_to_replay->created_at
])

@include('partials.fields.select', [
    'name' => 'none',
    'required' => 'true',
    'disabled'  => true,
    'list' => $status_options,
    'value' => $comment_to_replay->status ?? null
])

@include('partials.fields.checkbox', [
    'label' => 'Visible',
    'wrapper' => true,
    'list' => [
        [
            'name' => 'none',
            'disabled' => 'true',
            'checked' => isset($comment_to_replay) ? $comment_to_replay->visible : 0
        ]
    ]
])

@if(isset($comment_to_replay->content))
    @foreach($comment_to_replay->content as $name => $value)
        @include('partials.fields.textarea', [
            'label' => ucfirst($name),
            'name' => 'none',
            'disabled' => 'true',
            'value' => $value ?? null
        ])
    @endforeach
@endif

<div class="clearfix"></div>

@include('partials.containers.portlet', [
    'part' => 'close'
])

