@foreach($fields as $name)
    @include('partials.fields.input', [
        'label' => ucfirst($name),
        'name' => 'content['. $name .']',
        'required' => 'true',
        'value' => null
    ])
@endforeach