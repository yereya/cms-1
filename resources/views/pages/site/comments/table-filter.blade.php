<div class="row table-filters">
    {!! Form::open([
        'route' => ['sites.{site}.comments.datatable',$site->id],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',
        'data-datatable_id' => 'data-table-comments'

    ]) !!}

    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'content_type_id',
            'id' => 'post-content-type-list',
            'label' => 'Content Types',
            'class' => 'pull-right',
            'column' => 3,
            'list' => $content_types,
            'value' => request()->get('content_type_id')
        ])

        @include('partials.fields.select', [
            'name' => 'post_id',
            'label' => 'Post',
            'class' => 'pull-right',
            'ajax' => true,
            'column' => 4,
            'attributes' => [
                'data-min_input' => 0,
                'data-dependency_selector[0]' => '#post-content-type-list',
                'data-method' => 'list',
                'data-api_url' => route('sites.{site}.posts.select2',[$site->id]),
                'data-value' => request()->get('post_id'),
            ]
        ])

        <div class="form-group form-md-line-input pull-right col-md-2">
            @include('partials.fields.button', [
                'value' => 'Filter',
                'class' => ['full-width', 'blue'],
                'right' => true
            ])
        </div>
    </div>
    {!! Form::close() !!}
</div>
