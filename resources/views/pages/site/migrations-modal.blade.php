@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Site Migrations',
])

@include('partials.containers.data-table', [
    'data' => $migrations,
    'class' => 'site-migrations',
    'columns' => [
        [
            'key' => 'Order',
            'value' => function ($data) {
                return $data->order;
            }
        ],
        [
            'key' => 'Name',
            'value' => function ($data) {
                return "<a href=\"". route('site-migrations.edit', [$data->id]) ."\">{$data->name}</a>";
            }
        ],
        [
            'key' => 'Status',
            'value' => function ($data) {
                if ($data->status) {
                    return '<span class="label label-sm label-success">Active</span>';
                } else {
                    return '<span class="label label-sm label-danger">Disabled</span>';
                }
            }
        ],
        [
            'key' => 'has_permission',
            'label' => 'Toggle',
            'filter_type' => 'null',
            'value' => function ($data) use ($site) {
                $res = '<a href="javascript:void(0);" data-property_toggle="'. route('site-migrations.{migration_id}.toggle-property', [
                        $data['id'], "action=sites", "site_id={$site->id}&migration_id={$data->id}"
                    ]) .'"><span class="toggle_checkbox" data-toggle_state="checked"></span></a>';

                return $res;
            }
        ]
    ]
])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => true
    ]
])
