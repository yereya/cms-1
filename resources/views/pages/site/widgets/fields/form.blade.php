@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Widget Field Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($field) ? ['site-widgets.{widget}.fields.update', $field->widget_id, $field->id] : ['site-widgets.{widget}.fields.store', $widget->id],
        'method' => isset($field) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}
    {!! Form::hidden('widget_id', isset($field) ? $field->widget_id : $widget->id) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'title',
            'required' => 'true',
            'value' => isset($field) ? $field->title : null
        ])

    @include('partials.fields.select', [
        'name' => 'type_id',
        'label' => 'Type',
        'required' => true,
        'list' => $types,
        'value' => isset($field) ? $field->type_id : null
    ])

    @include('partials.fields.input', [
        'name' => 'default',
        'value' => isset($field) ? $field->default : null
    ])

    @include('partials.fields.textarea', [
        'name' => 'help',
        'required' => 'true',
        'value' => isset($field) ? $field->help : null
    ])

    @include('partials.fields.input', [
        'name' => 'permission',
        'required' => 'true',
        'value' => isset($field) ? $field->permission : null
    ])
</div>

@include('partials.containers.form-footer-actions', [
    'buttons' => [
        'cancel' => [
            'route' => back_url()
        ],
        'submit' => 'true'
    ],
    'checkboxes' => [
        'create_another' => isset($field) ? null : 'true'
    ]
])

{!! Form::close() !!}

@include('partials.containers.portlet', [
    'part' => 'close'
])
@stop