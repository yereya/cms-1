@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $widget->name . ' Widget Fields',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('site-widgets.{widget}.fields.create', $widget->id)
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $fields,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Title',
                'value' => function ($data) {
                    return "<a href=\"". route('site-widgets.{widget}.fields.edit', [$data->widget_id, $data->id]) ."\">{$data->title}</a>";
                }
            ],
            [
                'key' => 'Type',
                'value' => function ($data) {
                    return $data->type->name;
                }
            ],
            [
                'key' => 'Default',
                'value' => function ($data) {
                    return $data->default ?: 'none';
                }
            ],
            [
                'key' => 'Permission',
                'value' => function ($data) {
                    return $data->permission;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('site-widgets.{widget}.fields.edit', [$data->widget_id, $data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('site-widgets.{widget}.fields.ajax-modal', [$data->widget_id, $data->id, 'method=deleteField']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete feild" class="icon-trash tooltips"></span>
                        </a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop