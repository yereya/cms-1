<div class="row table-filters">
        {!! Form::open([
            'url' => route('sites.{site}.widgets.datatable', $site->id),
            'method' => 'POST',
            'class' => 'form-horizontal',
            'role' => 'form',
        ]) !!}

    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'widget_type',
            'label' => 'Widget Type',
            'column' => 5,
            'list' => $widget_type
        ])

        @include('partials.fields.select', [
            'name' => 'templates',
            'label' => 'Templates',
            'column' => 5,
            'list' => $templates
        ])

        <div class="form-group form-md-line-input col-md-1">
            @include('partials.fields.button', [
                'value' => 'Submit',
                'right' => true,
            ])
        </div>
    </div>

    {!! Form::close() !!}
</div>
