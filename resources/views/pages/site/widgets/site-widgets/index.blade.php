@extends('layouts.metronic.main')


@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Widgets',
        'actions' => [
            [
                'type'=>'modal',
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.widgets.ajax-modal',[$site,'method'=>'select'])
            ]
        ],
        'attributes' => [
            'data-open' => 'modal',
            'data-target' => '#ajax-modal',
        ],
    ])

    @include('pages.site.widgets.site-widgets.filters.table-index')
    @include('partials.containers.data-table', [
        'data' => [],
        'route' => 'sites.{site}.widgets.datatable',
        'route_params' => $site,
        'server_side' => true,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[0, "desc"]]',
            'data-onchange_submit' => 'true',
            'data-route_method' => 'list',
            'data-search' => 'search',
            'data-scrollX' => 'true',

            //the "processing" loader
            'data-blocking_loader' => 'true'
        ],
        'columns' => ['Name', 'Widget type', 'Created at', 'Updated at', 'actions']
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
