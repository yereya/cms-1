@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Widget',
    'class'=>''
])


@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Widget Types'
])

@foreach($widgets as $widget)
    @include('partials.fields.button', [
      'url' => !empty($template)
            ? route('sites.{site}.widgets.ajax-modal', [$site->id, 'method'=>'type','widget_id'=>$widget->id,'template'=>$template->id, 'column_id' => $column_id])
            : route('sites.{site}.widgets.{widget}.widget-data.create', [$site->id, 'widget_id'=> $widget->id]),
      'icon' => $widget->icon ?? 'fa fa-rocket',
      'value' => $widget->name,
      'class'=>'',
      'attributes'=> !empty($template)
      ? [
        'data-open' => 'modal',
        'data-target' => '#ajax-modal',
        'title' => 'Add row',
        'data-placement' => 'bottom',
      ]
      : ''
    ])
@endforeach


{{--@include('partials.containers.portlet', ['part' => 'close'])--}}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'modal_width' => '900'
    ]
])
