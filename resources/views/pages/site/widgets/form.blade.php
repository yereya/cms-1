@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Widget Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($widget) ? ['site-widgets.update', $widget->id] : ['site-widgets.store'],
        'method' => isset($widget) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'name',
            'required' => 'true',
            'value' => isset($widget) ? $widget->name : null
        ])

        @include('partials.fields.input', [
            'name' => 'controller',
            'required' => 'true',
            'value' => isset($widget) ? $widget->controller : null
        ])

    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($widget) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop