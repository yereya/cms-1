@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Widgets',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('site-widgets.create')
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $widgets,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) {
                    return "<a href=\"". route('site-widgets.edit', [$data->id]) ."\">{$data->name}</a>";
                }
            ],
            [
                'key' => 'Controller',
                'value' => function ($data) {
                    return $data->controller;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    $res .= '<a href="'. route('site-widgets.{widget}.fields.index', [$data->id]) . '">
                        <span aria-hidden="true" class="fa fa-list"></span>
                    </a> ';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('site-widgets.edit', [$data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('site-widgets.ajax-modal', [$data->id, 'method=deleteWidget']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete widget" class="icon-trash tooltips"></span>
                        </a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop