@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Settings',
        'has_form' => true,
    ])

    {!! Form::open([
        'url'    => route('sites.{site}.settings.update', [$site->id, 'type']),
        'method' => 'PUT',
        'class'  => 'form-horizontal'
    ]) !!}

    @include('pages.site.settings.form', [
        'site_id' => $site->id
    ])

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=> $site->id])
            ],
            'submit' => 'true'
        ],
    ])

    {!! Form::close() !!}
    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
