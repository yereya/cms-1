{!! Form::open([
    'url'    => route('sites.{site}.settings.update', [$site_id, 'type']),
    'method' => 'PUT',
    'class'  => 'form-horizontal'
]) !!}

@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Settings',
    'event' => 'row-changed'
])

@include('pages.site.settings.form', [
    'site_id' => $site_id
])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'close' => true
    ],
    'js' => [
        'select2' => true,
        'uniform' => true,
    ]
])

{!! Form::close() !!}
