@extends('layouts.metronic.main')

@section('page_content')

    <div id="scss-editor-page">
        @if (\Session::has('success'))
            <div class="alert alert-success">{{ \Session::get('success') }}</div>
        @endif
        @if (\Session::has('error'))
            <div class="alert alert-danger">{{ \Session::get('error') }}</div>
        @endif
        @if (isset($errors) && $errors->first())
            <div class="alert alert-danger">{{ $errors->first() }}</div>
        @endif
        <div class="row">
            <div class="col-md-2 files-sidebar">
                <button class="btn btn-default getUrl" data-url="{{ route('sites.{site}.scss-compile',[$site->id]) }}">
                    Recompile SCSS
                </button>
                <button class="btn btn-default getUrl" data-url="{{ route('sites.{site}.css-upload',[$site->id]) }}">
                    Upload
                    SCSS
                </button>
                <br/><br/>
                @if (isset($scss_file))
                    <a class="btn btn-primary btn-circle btn-add-new"
                       href="{{ route('sites.{site}.settings.scss-files.index',[$site->id]) }}">New SCSS File</a>
                @endif
                <p>
                    <input type="search" class="form-control" id="queryFiles" placeholder="Search file by name"/>
                </p>
                <form id="updateOrder"
                      action="{{route('sites.{site}.settings.sass-files.update-priorities',[$site->id])}}"
                      method="POST">
                    <ul class="files-list draggable">
                        @foreach($scss_files as $index=> $file)
                            <li class="{{ isset($scss_file) && $scss_file->id == $file->id ? 'active' : '' }}">
                                <input type="hidden" name="ids[{{$file->id}}]" value="{{$index+1}}">
                                <a href="{{ route('sites.{site}.settings.scss-files.show',[$site->id,$file->id]) }}">{{ $file->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                    <br><br>
                    {!! csrf_field() !!}
                </form>

                @if ($scss_files->count() === 0)
                    <div class="alert alert-info">You may create a new scss file by clicking on the blue button located
                        on the right.
                    </div>
                @endif
            </div>

            @if (isset($scss_file))
                <form id="deleteCurrentFile"
                      action="{{ route('sites.{site}.settings.scss-files.destroy',[$site->id,$scss_file->id]) }}"
                      method="post">
                    {!! csrf_field() !!}
                    <input type="hidden" value="DELETE" name="_method"/>
                </form>
            @endif
            <form action="{{ $url ?? '' }}" method="POST" id="scssEditorForm">
                {!! csrf_field() !!}
                @if (isset($scss_file))
                    <input type="hidden" name="_method" value="PUT"/>
                @endif
                <div class="col-md-10">
                    <div class="text-right">
                        <div class="col-md-10">
                            <input type="text" name="name" value="{{ isset($scss_file) ? $scss_file->name : '' }}"
                                   class="form-control" placeholder="Choose a file name."/>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn btn-primary">
                                {{ isset($scss_file) ? 'Save Changes' : 'Create File' }}
                            </button>
                            <button class="btn btn-danger" {{ $scss_file ?? 'disabled' }} type="button"
                                    onclick="deleteCurrentFile()">Delete
                            </button>
                        </div>
                    </div>
                    <hr/>
                    <div class="col-md-12">
                              <textarea name="content"
                                        data-codemirror_lang="scss"
                                        class="codemirror"
                                        style="display: none;">{{ isset($scss_file) ? $scss_file->content : '' }}
                              </textarea>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        var selectors = {
            scssFile: '.draggable',
            updateOrderForm: '#updateOrder',


        };

        $(".getUrl").click(function () {
            var that = $(this);
            that.prop('disabled', true);
            $.get($(this).data('url'), function (res) {
                try {
                    var data;

                    if (typeof res !== 'object') {
                        data = JSON.parse(res);
                    } else {
                        data = res
                    }

                    if (!data.desc) {
                        return;
                    }

                    toastr.info(data.desc);
                } catch (ex) {
                    toastr.info('Invalid server response.');
                }
            }).always(function () {
                that.prop('disabled', false);
            });
        });

        function deleteCurrentFile() {
            if (!confirm('Are you sure?')) {
                return;
            }
            $("#deleteCurrentFile").submit();
        }

        $("#queryFiles").keyup(function () {
            var query = $(this).val().toLowerCase();
            $(".files-list li").each(function () {
                if ($(this).text().toLowerCase().indexOf(query) === -1) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        });

        // Save changes
        $(document).keydown(function (event) {
            if (!( String.fromCharCode(event.which).toLowerCase() == 's' && event.ctrlKey) && !(event.which == 19)) return true;
            event.preventDefault();
            $("#scssEditorForm").submit();
            return false;
        });

        /*Drag Setting*/
        var dragSetting = {
            stop: function () {
                var $form = $(selectors.updateOrderForm);

                var orderParams = $form.serialize();
                var settings = {
                    url: $form.attr('action'),
                    method: $form.attr('method'),
                    data: orderParams,
                    complete: function (response) {
                        if (typeof response == 'object') {
                            var response = JSON.parse(response.responseText);
                            if (response.status == 'success') {
                                toastr.info(response.desc);
                            }
                        }
                    }
                };
                $.ajax(settings);
            }
        };

        /*Enable Drag And Drop Option*/
        $(selectors.scssFile).sortable(dragSetting);

    </script>
@endsection