
{!! Form::hidden('type', $type) !!}

<div class="row">
    <div class="{{ isset($class) ? $class : 'col-md-6 col-md-offset-3'  }}">
        @if($settings->isEmpty())
            <h3>There are no settings for <strong>{{$type}}</strong></h3>
        @else
            @foreach($settings as $setting_item)
                @if ($setting_item->field_type->name == FieldType::TYPE_TEXT)
                    @include('partials.fields.input', [
                        'name' => "settings[{$setting_item->key}]",
                        'label' => $setting_item->name,
                        'value' => $setting_item->value ?? null
                    ])

                @elseif ($setting_item->field_type->name == FieldType::TYPE_TEXT_AREA)
                    @include('partials.fields.textarea', [
                        'name' => "settings[{$setting_item->key}]",
                        'label' => $setting_item->name,
                        'value' => $setting_item->value ?? null
                    ])

                @elseif ($setting_item->field_type->name == FieldType::TYPE_SELECT)
                    @include('partials.fields.select', [
                        'name' => "settings[{$setting_item->key}]",
                        'label' => $setting_item->name,
                        'list' => $setting_item->list,
                        'value' => $setting_item->value ?? null
                    ])

                @elseif ($setting_item->field_type->name == FieldType::TYPE_MULTI_SELECT)
                    @include('partials.fields.select', [
                        'name' => "settings[{$setting_item->key}]",
                        'multi_select' => true,
                        'label' => $setting_item->name,
                        'list' => $setting_item->list,
                        'value' => json_decode($setting_item->value ?? null)
                    ])

                @elseif ($setting_item->field_type->name == FieldType::TYPE_CHECKBOX)
                    @include('partials.fields.checkbox', [
                        'label' => $setting_item->name,
                        'wrapper' => true,
                        'type' => 'checkbox',
                        'list' => [
                            [
                                'name' => "settings[{$setting_item->key}]",
                                'value' => 1,
                                'checked' => $setting_item->value ?? 0
                            ],
                        ],
                    ])
                @elseif ($setting_item->field_type->name == FieldType::TYPE_MEDIA)
                    <div class="row">
                        <div class="container"  style="padding:10px" >
                            @include('partials.containers.image-upload',[
                                'name' => "settings[{$setting_item->key}]",
                                'class_name' => "image_{$setting_item->key}",
                                'form_line' => true,
                                'src' => $setting_item->media ? $setting_item->media->getFullMediaPath() : null,
                                'media_id' => $setting_item->media ? $setting_item->media->id : null,
                                'label' => $setting_item->name,
                                'href' => route('elfinder.ajax-modal',
                                    ['site' => $site_id, 'method' => 'fileModal', 'field' => "image_{$setting_item->key}"] )
                            ])
                        </div>
                    </div>
                @endif

            @endforeach
        @endif
            </div>
        </div>
