@extends('layouts.metronic.main')

@section('page_content')
    <input type="text"
           autofocus
           placeholder="Describe a setting for quick access"
           id="querySettings"
           data-search-in=".general-settings .portlet"
           data-empty-state="#empty-state"
           data-remove-class-on-single-result="#site-settings-portlet"
           data-class-to-remove="col-md-7"
           class="form-control quickSearch"/>

    <div class="text-center hide" id="empty-state">
        <h1 class="tossing">Whoops 😢</h1>
        <p>No results were found for your query, try using different words or choose from the following.</p>
        <ul>
            <li>Cache</li>
            <li>Language</li>
            <li>SEO</li>
            <li>Header Code</li>
        </ul>
    </div>
    <form action="" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="_method" value="PUT"/>
        <div class="row general-settings">
            <div class="col-md-7" id="site-settings-portlet">


                @include('partials.containers.portlet', ['part' => 'open','title' => 'SEO','has_form' => true,'save_changes_button' => true])

                @include('partials.fields.input', [
                           'name' => 'settings[title_prefix]',
                           'label' => 'Title Prefix',
                           'value' => $settings->get('title_prefix')->value ?? null,
                           'help_text' => 'This value will be displayed before every page title.'
                       ])

                @include('partials.fields.input', [
                           'name' => 'settings[title_suffix]',
                           'label' => 'Title Suffix',
                           'value' => $settings->get('title_suffix')->value ?? null,
                           'help_text' => 'This value will be displayed after every page title.'
                       ])

                @include('partials.fields.checkbox', [
                    'label' => 'Discourage search engines from indexing this site',
                    'wrapper' => true,
                    'type' => 'checkbox',
                    'column' => 6,
                    'list' => [
                        [
                            'name' => "settings[search_engine_visibility]",
                            'value' => 1,
                            'checked' => $settings->get('search_engine_visibility')->value ?? false
                        ],
                    ],
                ])

                <hr/>

                {{--Default Open Graph Definition--}}
                <div class="portlet-title">Default Open Graph Definitions</div>

                @include('partials.fields.input', [
                           'name' => 'settings[og_image]',
                           'label' => 'Og Image',
                           'value' => $settings->get('og_image')->value ?? null,
                           'help_text' => 'Will be displayed in case the current post is missing an og image definition.'
                       ])

                @include('partials.fields.input', [
                           'name' => 'settings[og_type]',
                           'label' => 'Og Type',
                           'value' => $settings->get('og_type')->value ?? null,
                           'help_text' => 'Will be displayed in case the current post is missing an og type definition.'
                       ])
                <hr/>
                <div class="portlet-title">Site Icons</div>


                {{--Image Upload--}}
                @include('partials.containers.image-upload',[
                                    'name' => "settings[fav-icon]",
                                    'class_name' => "image_fav-icon",
                                    'form_line' => true,
                                    'src' => $settings->get('fav-icon')->media ? $settings->get('fav-icon')->media->getFullMediaPath() : null,
                                    'media_id' => $settings->get('fav-icon')->media ? $settings->get('fav-icon')->media->id : null,
                                    'label' => 'Fav Icon',
                                    'href' => route('elfinder.ajax-modal',
                                        ['site' => $site->id, 'method' => 'fileModal', 'field' => "image_fav-icon"] )
                                ])


                @include('partials.containers.portlet', [
                            'part' => 'close'
                         ])

                {{--Facebook App--}}
                @include('partials.containers.portlet', ['part' => 'open','title' => 'Facebook App','has_form' =>true,'save_changes_button' => true])
                @include('partials.fields.input', [
                  'name' => 'settings[facebook_app_id]',
                  'label' => 'App Id',
                  'value' => $settings->get('facebook_app_id')->value ?? null,
                  'help_text' => 'add your app id according to your facebook app account.'
              ])

                @include('partials.containers.portlet', ['part' => 'close'])

                {{--Compliance Disclosure--}}
                @include('partials.containers.portlet', ['part' => 'open','title' => 'Compliance Disclosure','has_form' =>true,'save_changes_button' => true])

                @include('partials.fields.textarea', [
                  'name' => 'settings[advertiser_disclosure]',
                  'label' => 'Advertiser Disclosure',
                  'value' => $settings->get('advertiser_disclosure')->value ?? null,
                  'help_text' => 'add the general advertiser disclosure.'
              ])

                @include('partials.containers.portlet', ['part' => 'close'])


                {{--Other Settings--}}
                @include('partials.containers.portlet', ['part' => 'open','title' => 'Other','has_form' => true,'save_changes_button' => true])

                @include('partials.fields.checkbox', [
                    'label' => 'Scroll to Top Button',
                    'wrapper' => true,
                    'type' => 'checkbox',
                    'column' => 8,
                    'list' => [
                        [
                            'name' => "settings[scroll_to_top]",
                            'value' => 1,
                            'checked' => $settings->get('scroll_to_top')->value ?? null
                        ],
                    ],
                ])

                @include('partials.fields.checkbox', [
                    'label' => 'Disable push notification request',
                    'wrapper' => true,
                    'column' => 8,
                    'type' => 'checkbox',
                    'list' => [
                        [
                            'name' => "settings[disable_push_notification_request]",
                            'value' => 1,
                            'checked' => $settings->get('disable_push_notification_request')->value ?? null
                        ],
                    ],
                ])

                @include('partials.fields.input', [
                   'name' => 'settings[comment_default_avatar]',
                   'label' => 'Comments Default Avatar',
                   'value' => $settings->get('comment_default_avatar')->value ?? null,
                   'help_text' => 'Enter the avatar default image Url.'
                 ])

                @include('partials.containers.portlet', ['part' => 'close'])

            </div>
            <div class="col-md-5">
                @include('partials.containers.portlet', ['part' => 'open','title' => 'CACHE','has_form' => true])
                <div class="alert alert-warning">
                    Clear cached files to force the current site to re-render each of the site components.<br/>
                    <strong>Note:</strong> Purging the cache may temporarily degrade the performance of the site.
                </div>
                <p>
                    <button data-target="sweetAlert"
                            data-alert_title="Great!"
                            data-alert_type="success"
                            data-alert_href="{{ route('sites.{site}.purge',$site->id) }}"
                            type="button"
                            class="btn btn-lg btn-default">Purge Everything
                    </button>
                </p>
                @include('partials.containers.portlet', ['part' => 'close'])
                @include('partials.containers.portlet', ['part' => 'open','title' => 'Raw Codes','has_form' => true,'save_changes_button' => true])

                <div class="alert alert-warning">
                    <div class="pull-right icon">👹</div>
                    The following fields has the ability to completely break the site.<br/>
                    If your not sure about what your doing don't do it.
                </div>

                @include('partials.fields.textarea', [
                            'name' => "settings[header_raw_code]",
                            'label' => 'Header',
                            'value' => $settings->get('header_raw_code')->value ?? null,
                            'help_text' => 'This code will be printed inside the head tag.'
                        ])

                @include('partials.fields.textarea', [
                    'name' => "settings[body_raw_code]",
                    'label' => 'Body',
                    'value' => $settings->get('body_raw_code')->value ?? null,
                    'help_text' => 'This code will be printed inside the body tag.'
                ])

                @include('partials.fields.textarea', [
                            'name' => "settings[footer_raw_code]",
                            'label' => 'Footer',
                            'value' => $settings->get('footer_raw_code')->value ?? null,
                            'help_text' => 'This code will be printed inside the closing body tag.'
                        ])

                @include('partials.containers.portlet', ['part' => 'close'])

                @include('partials.containers.portlet', ['part' => 'open','title' => 'Language','has_form' => true,'save_changes_button' => true])

                @include('partials.fields.select', [
                'name' => "settings[language]",
                'label' => 'Site Language',
                'list' => [
                    'en_US' => 'English US'
                ],
                'value' => $settings->get('language')->value ?? null
                ])

                @include('partials.containers.portlet', ['part' => 'close'])

                @include('partials.containers.portlet', ['part' => 'open','title' => 'Recaptcha Token','has_form' =>
                true,'save_changes_button' => true])

                @include('partials.fields.input', [
                      'name' => 'settings[recaptcha_public_key]',
                      'label' => 'Public Key',
                      'value' => $settings->get('recaptcha_public_key')->value ?? null,
                      'help_text' => 'add it according to captcha public key in your specific site.'
                  ])

                @include('partials.fields.input', [
                           'name' => 'settings[recaptcha_secret_key]',
                           'label' => 'Secret Key',
                           'value' => $settings->get('recaptcha_secret_key')->value ?? null,
                           'help_text' => 'add it according to captcha secret key in your specific site.'
                       ])

                @include('partials.containers.portlet', ['part' => 'close'])

                @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => 'HomePage',
                'has_form' =>true,
                'save_changes_button' => true
                ])

                <div class="alert alert-info">Choose your homepage according to post.</div>

                @include('partials.fields.select',[
                    'name' => "settings[homepage]",
                    'label' => 'Post',
                    'list' => $pages,
                    'value' => $settings->get('homepage')->value ?? null
                ])

                @include('partials.containers.portlet', ['part' => 'close'])
            </div>
        </div>
    </form>
@endsection