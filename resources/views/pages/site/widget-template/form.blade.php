@extends('layouts.metronic.main')
@section('page_content')

    {!! Form::open([
            'route' => isset($widget_template) ? ['sites.{site}.widget-templates.update', $site_id, $widget_template->id] :  ['sites.{site}.widget-templates.store', $site_id],
            'method' => isset($widget_template) ? 'PUT' : 'POST',
            'class' => 'form-horizontal',
            'role' => 'form',
            'files' => true,
            'data-url_validation'=> isset($widget_template)
                ? route('sites.{site}.widgets.{widget}.templates.update-scss',[
                        $site_id,
                        $widget_template->widget->id
                    ])
                : null
        ]) !!}

    @if (isset($widget_template))
        {!! Form::hidden('widget_template_id', $widget_template->id) !!}
    @endif

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($widget_template) ? 'Edit  Template' : 'Create Template',
        'has_form' => true,
        'actions' => [
                [
                    'type' => 'submit',
                    'title' => 'Save',
                    'class' => 'btn-primary',
                    'icon' => 'fa fa-save fw'
                ],
                isset($widget_template) ? [
                    'icon' => 'fa fa-archive',
                    'title' => 'Revisions',
                    'target' => route('sites.{site}.widget-templates.revisions', [$site_id, 'model_id='. $widget_template->id]),
                    'class' => 'blue',
                ] : null,
        ]
    ])

    <div class="form-body">
        <div class="form-group">
            @include('partials.fields.input', [
                    'label' => 'Name',
                    'name' => 'name',
                    'required' => 'true',
                    'value' => $widget_template->name ?? '',
                    'type' => 'text'
                ])

            @include('partials.fields.input', [
                   'label' => 'File name',
                   'name' => 'file_name',
                   'required' => 'true',
                   'value' => $widget_template->file_name ?? '',
                   'type' => 'text'
               ])

            @include('partials.fields.textarea', [
                'label' => 'Description',
                'name' => 'description',
                'class' =>  '',
                'value' => $widget_template->description ?? '',
                'help_text' =>  '',
                'attributes' => [
                    'placeholder' => 'Describe your template',
                    'rows'=>'1'
                    ]
                ])

            @include('partials.fields.select', [
              'label' => 'Widget Name',
              'name' => 'widget_id',
              'required'=>'true',
              'force_selection' => true,
              'list'=> $widgets->all(),
              'value' => $widget_template->widget_id ?? null
           ])

            @include('partials.fields.select', [
                  'label' => 'Published',
                  'name' => 'published',
                  'required'=>'true',
                  'force_selection' => true,
                  'list'=>[false=>'Not Published',true=>'Published'],
                  'value' =>  $widget_template->published ??  0
               ])

            {{-- todo -  blade validation template
                 todo -  Commented out because it is part of the htmlValidator functionality, should be reintroduced when the feature will be finished
            --}}
            {{--@include('partials.containers.validation.blade-diff')--}}
            @include('partials.fields.textarea', [
                'name' => 'blade',
                'label' => 'Blade',
                'codemirror' => 'blade',
                'value' => $widget_template->blade ?? '',
                'help_text' =>  '',
                'attributes' => [
                    'placeholder' => 'insert the blade',
                    'rows'=>'5',
                    'data-validate' => true
                    ]
          ])

            @include('partials.fields.textarea', [
                'name' => 'Scss',
                'label' => 'scss',
                'codemirror' => 'sass',
                'value' => empty($widget_template->scss) ? $empty_scss_template : $widget_template->scss,
                'help_text' =>  '',
                'attributes' => [
                    'placeholder' => 'edit scss file',
                    'rows'=>'7'
                    ]
            ])
        </div>
    </div>
@stop
