
<div class="row table-filters">
    {!! Form::open([
        'route' => ['sites.{site}.widget-templates.datatable', $site_id],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',

    ]) !!}
    <div class="form-inline">

        @include('partials.fields.select', [
                   'name' => 'widget_id',
                   'label' => 'Widget',
                   'column' => 4,
                   'id'=>'widget_id',
                   'ajax' => true,
                   'attributes' => [
                       'data-min_input' => 0,
                       'data-method' => 'list',
                       'data-api_url' => route('sites.{site}.site-widgets.select2', $site_id)
                   ]
               ])

        @include('partials.fields.select', [
                    'name' => 'widget_data_id',
                    'label' => 'Widget Data',
                    'column' => 4,
                    'column_label'=>3,
                    'ajax' => true,
                    'id'=>'widget_data_id',
                    'attributes' => [
                        'data-min_input' => 0,
                        'data-method' => 'list',
                        'data-api_url' => route('sites.{site}.widgets.widget-data.select2', $site_id)
                    ]
                ])

        <div class="form-group form-md-line-input pull-right col-md-2">
            @include('partials.fields.button', [
                'value' => 'Filter',
                'class' => ['full-width', 'blue'],
                'column' => 4,
                'right' => true
            ])
        </div>
    </div>
    {!! Form::close() !!}
</div>
