@extends('layouts.metronic.main')

@section('page_content')

    @include('pages.site.widget-template.filters.widget-templates-filters')

    @include('partials.containers.portlet', [
       'part' => 'open',
       'title' => 'Widget Template',
       'has_form' => true,
       'actions' => [
           [
               'icon' => 'fa fa-plus',
               'title' => 'Add New Template',
               'target' => route('sites.{site}.widget-templates.create', $site_id)
           ],
           [
               'title' => 'Flush Local',
               'target' =>  '#',
               'class' =>"blue",
               'attributes' => [
                   'data-target' => "sweetAlert",
                   'data-alert_title' => 'Flush All Blades',
                   'data-alert_type' => 'success',
                   'data-alert_href' => route('sites.{site}.widget-templates.ajax-alert', [$site_id, 'method=flushLocal']),
               ]
            ],
           [
               'title' => 'Publish All',
               'target' =>  '#',
               'class' =>"red",
               'attributes' => [
                   'data-target' => "sweetAlert",
                   'data-alert_title' => 'Publish All Widgets Templates',
                   'data-alert_type' => 'warning',
                   'data-alert_href' => route('sites.{site}.widget-templates.ajax-alert', [$site_id, 'method=publish']),
                   'title' => "Delete page",

               ]
            ],
       ]
   ])

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'site_widget_templates-table',
        'route' => 'sites.{site}.widget-templates.datatable',
        'route_params' => [$site_id],
        'server_side' => true,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[1, "asc"]]',
            'data-route_method' => 'list',
            //the "processing" loader
            'data-blocking_loader' => 'true',

        ],
        'columns' => [
            [
                'label' => 'Id',
                'attributes' => [
                    'width' => '2%',
                ]
            ],
            'Widget',
            'Name',
            'Description',
            'File name',
            [
               'label' => 'Updated At',
               'key' => 'updated_at',
               'attributes'=>[
                    'width'=>'10%'
               ]
            ],
            [
               'label'=>'Actions',
               'attributes'=>[
                    'width'=>'4%'
               ]
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])

@stop
