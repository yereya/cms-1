@extends('layouts.metronic.main')

@section('page_content')

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Revision History'
        ])

    @include('partials.containers.data-table', [
    'data' => $revisions,
    'columns' => [
        [
            'key' => 'created_at',
            'label' => 'Created at',
        ],
        [
            'label' => 'User',
            'value' => function($row) {

              return $row->userResponsible()->first_name;
            }
        ],
        [
            'key' => 'key',
            'label' => 'Column',
        ],
        [
            'label' => 'Action',
            'value' => function($row)  use ($site) {

                $res = View::make('partials.containers.buttons.link', [
                    'route' => '#',
                    'class' => 'choice_type_click',
                    'title' => 'show',
                    'icon' => "icon-arrow-up",
                    'value' => ' Show',
                    'attributes' => [
                       'data-route' => route('sites.{site}.widget-templates.choice' ,[$site->id, 'method=getRevision',  'revision_id='. $row->id ]) ,
                       ]
                     ]);

                return $res;

            }
        ],
    ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])

    <fieldset id="field-metadata"></fieldset>

@stop
