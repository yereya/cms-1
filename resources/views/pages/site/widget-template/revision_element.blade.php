@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Revision History',
     'actions' => [
           [
               'icon' => "fa fa-arrow-up",
               'title' => 'Use this revision',
               'class' =>  'btn btn-primary',
               'target' => route('sites.{site}.widget-templates.edit', [$site->id, $revision->revisionable_id, 'revision_id='. $revision->id]),
           ],
     ]
    ])

<p>
    <strong>Changed by:</strong> {{$revision->userResponsible()->first_name}}
    <br>
    <strong>On</strong>: {{$revision->created_at}}
</p>

<div class="row">
    <div class="col-md-12">
        @include('partials.fields.textarea', [
                  'name' => 'none',
                  'no_label' => 'true',
                  'codemirror' => 'sass',
                  'disabled' => true,
                  'value' => $revision->old_value ?? '',
                  'help_text' =>  '',
                  'attributes' => [
                      'data-codemirror_column_wrap' => 80,
                      'rows'=>'7'
                  ]
         ])
    </div>
</div>


<script>
    App.codeMirrorInit();
</script>
@include('partials.containers.portlet', [
    'part' => 'close',
])
