@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site AB Tests Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($test) ? ['sites.{site}.ab-tests.update', $site->id, $test->id] : ['sites.{site}.ab-tests.store', $site->id],
        'method' => isset($test) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'name',
            'required' => 'true',
            'value' => isset($test) ? $test->name : null
        ])

        @include('partials.fields.select', [
            'name' => 'type',
            'required' => 'true',
            'value' => isset($test) ? $test->type : null,
            'list' => [
                'url' => 'URL',
                'cookie' => 'Cookie',
                'location' => 'Location'
            ]
        ])

        @include('partials.fields.select', [
            'name' => 'relation',
            'required' => 'true',
            'value' => isset($test) ? $test->relation : null,
            'list' => [
                'and' => 'AND',
                'or' => 'OR'
            ]
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(["site" => $site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($test) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop