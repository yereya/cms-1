@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' AB Tests',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add Test',
                'target' => route('sites.{site}.ab-tests.create', $site->id)
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $tests,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) use ($site) {
                    return "<a href=\"". route('sites.{site}.ab-tests.edit', [$site->id, $data->id]) ."\"><strong>{$data->name}</strong></a>";
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name, $site) {
                    $res = '';

                    if($data->type == 'location') {
                        $res .= '<a href="'. route('sites.{site}.ab-tests.{test}.location-rules.index', [$site->id, $data->id]) . '" class="tooltips" title="Location Rules">
                            <span aria-hidden="true" class="fa fa-list"></span>
                        </a> ';
                    } else {
                        $res .= '<a href="'. route('sites.{site}.ab-tests.{test}.rules.index', [$site->id, $data->id]) . '" class="tooltips" title="Rules">
                                <span aria-hidden="true" class="fa fa-list"></span>
                            </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('sites.{site}.ab-tests.edit', [$site->id, $data->id]) . '" class="tooltips" title="Edit Test">
                            <span aria-hidden="true" class="icon-pencil"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('sites.{site}.ab-tests.ajax-modal', [$site->id, 'test_id=' . $data->id, 'method=deleteTest']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete Test" class="icon-trash tooltips"></span>
                        </a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop