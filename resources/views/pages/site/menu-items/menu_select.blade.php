@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Menus ',
    'has_form' => true,
    'actions' => [
        [
            'icon' => 'fa fa-plus',
            'target' => route('sites.{site}.menus.create',[$site->id])
        ],
        [
            'icon' => 'fa fa-pencil',
            'target' => route('sites.{site}.menus.edit',[$site->id, $selected_menu->id])
        ],
    ]
])

<div class="clearfix" >
    @include('partials.fields.select', [
      'name' => 'menu',
      'column_label' => 0,
      'label' => '',
      'column' => 12,
      'list' => $menus,
      'value' => $selected_menu->id,
      'id' => 'mi-menu-select',
      'force_selection' => true
    ])
</div>

@include('partials.containers.portlet', [
    'part' => 'close'
])
