<div class="mi-link-content nestable-content-box">
    <div class="row">
        <div class="col-md-12">

            @include('partials.fields.input', [
              'name' => 'link_'. $node->id.'[name]',
              'value' => $node->name,
              'label' => 'Name',
            ])

            @include('partials.fields.input', [
              'name' => 'link_'.$node->id.'[title]',
              'value' => $node->title,
              'label' => 'Title',
            ])

            @if ($is_template || !empty($node->post_id) )
                @include('partials.fields.input', [
                  'name' => 'link_'.$node->id.'[post_id]',
                  'value' => $node->post_id,
                  'label' => 'post_id',
                  'type' => 'hidden',
                ])
            @endif

            @include('partials.fields.input', [
              'name' => 'link_'. $node->id.'[url]',
              'value' => $node->href,
              'label' => 'Url',
              'disabled' => !empty($node->post_id) ? true : false
            ])

            @include('partials.fields.input', [
              'name' => 'link_'.$node->id.'[class]',
              'value' => $node->class,
              'label' => 'Class',
            ])

            @include('partials.fields.select', [
              'name' => 'link_'.$node->id.'[target]',
              'label' => 'Target',
              'list' => ['_self' => 'SELF', '_blank' => 'BLANK', '_top' => 'TOP', '_parent' => 'PARENT'],
              'value' => $node->target
            ])

            @include('partials.fields.input', [
              'name' => 'link_'.$node->id.'[custom_html]',
              'value' => $node->custom_html,
              'label' => 'Custom Html',
            ])

            @include('partials.fields.input', [
              'name' => 'link_'.$node->id.'[attributes]',
              'value' => $node->attributes,
              'label' => 'Attributes',
            ])

            <div class="form-group form-md-line-input">
                <div class="col-md-12">

                    @include('partials.fields.button', [
                      'type' => 'link',
                      'class' => "btn  default mi-toggle-link-box",
                      'value' => 'Close',
                    ])

                    @include('partials.fields.button', [
                      'type' => 'link',
                      'class' => "btn red  mi-delete-menu-item",
                      'value' => 'Delete Link',
                    ])
                </div>
            </div>
        </div>
    </div>
</div>
