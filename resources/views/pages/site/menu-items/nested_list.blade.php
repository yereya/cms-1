@if($tree != null)
    <ol class="dd-list">
        @foreach($tree as $node)
            <li class="dd-item dd3-item" data-id="{{$node->id }}">
                <div class="dd-handle dd3-handle">Drag</div>
                <div class="dd3-content">
                    <span class="mi-link-name">{{$node->name}}</span>
                    <div class="pull-right control_buttons">
                        <a><span aria-hidden="true" class="mi-toggle-link-box fa fa-caret-down"></span></a>
                    </div>
                </div>
                @include('pages.site.menu-items.attributes_box', [
                  'node' => $node,
                  'is_template' => $is_template
                ])

                @include('pages.site.menu-items.nested_list', [
                   'tree' => $node->children,
                   'site_id' => $site_id,
                   'is_template' => $is_template
                  ])
            </li>
        @endforeach
    </ol>
@endif
