@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Add Page Item ',
    'has_form' => true
])
    <div>
        @include('partials.fields.select', [
            'name' => 'page',
            'label' => 'Page',
            'column' => 12,
            'list' => $pages,
            'id' => 'mi-select-page'
        ])

        @include('partials.fields.button', [
          'attributes' => [
              'id' => "mi-btn-add-page-to-menu",
             ],
          'value' => 'Add to Menu ',
          'column' => 12,
          'type' => 'link',
          'class' => 'btn green'
        ])

    </div>

@include('partials.containers.portlet', [
    'part' => 'close'
])


@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Add Link Item',
    'has_form' => true
])
    <div  >
        @include('partials.fields.input', [
          'name' => 'Link Name',
          'class' => 'mi-link-name'
        ])

        @include('partials.fields.input', [
          'name' => 'Url',
          'class' => 'mi-link-url'
        ])

        <div class="form-group form-md-line-input"  >
        @include('partials.fields.button', [
          'attributes' => [
              'id' => "mi-btn-add-link-to-menu",
             ],
          'value' => 'Add to Menu ',
          'class' => 'btn green'
        ])

        </div>
    </div>

@include('partials.containers.portlet', [
    'part' => 'close'
])
