@extends('layouts.metronic.main')

@section('page_content')

    <div class="row">
        <div class="col-md-5 col-sm-6">
            @include('pages.site.menu-items.menu_select', [
              'menus' => $menus,
              'selected_menu' => $selected_menu
            ])

            @include('pages.site.menu-items.add_items', [
              'pages' => $pages,
              'site' => $site,
            ])
        </div>
        <div class="col-md-7 col-sm-6">
            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => $selected_menu->name . ' Items',
                'has_form' => true,
            ])

            {!! Form::open([
                'url' => route('sites.{site}.menus.{menu}.items.store',[$site->id, $selected_menu->id]),
                'method' => 'post',
                'id' => "mi-nestable-menu-items-form"
            ]) !!}
            <div class="cf nestable-lists">
                <div id="mi-nestable-menu-items" class="dd">
                    @if($menu_items->count() == 0)
                        <div id="mi-empty-message-box"><h2>Menu Is empty</h2></div>
                    @endif
                    @include('pages.site.menu-items.nested_list', [
                      'tree' => $menu_items,
                      'site_id' => $site->id,
                      'is_template' => false
                ])
                </div>
            </div>
            {!! Form::close() !!}

            <div id="mi-control-buttons" class="form-actions">
                <div class="col-sm-10 col-sm-offset-2">
                    <div>
                        @include('partials.fields.button', [
                          'attributes' => [
                              'id' => "mi-save-menu-items-order",
                              'data-method' => "storeMenuItemsOrder",
                              'data-url' => route('sites.{site}.pages.ajax-nestable', $site->id ),
                              //'disabled' => true
                             ],
                          'value' => 'Save Changes',
                          'type' => 'link',
                        ])

                        @include('partials.fields.button', [
                          'attributes' => [
                              'onclick' => "javascript:location.reload()",
                              //'disabled' => true
                             ],
                          'type' => 'link',
                          'class' => "btn default",
                          'value' => 'Cancel',
                        ])
                    </div>
                </div>
            </div>
            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        </div>
    </div>

    <div class="mi-link-content-template" style="display: none">
        @include('pages.site.menu-items.nested_list', [
          'tree' => $empty_list,
          'site_id' => $site->id,
          'is_template' => true
        ])
    </div>
@stop
