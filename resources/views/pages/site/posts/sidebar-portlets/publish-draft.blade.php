@include('partials.containers.portlet', [
    'part'      => 'open',
    'title'     => isset($post) ? $post->published_display_status : 'Draft',
    'has_form'  => true,
    'actions' => [
        [
            'type' => 'submit'  ,
            'title' =>  'Save' ,
            'class' =>  'btn btn-primary',
            'icon' => 'fa fa-save'
        ],
        isset($delete_alert_url) ?
        [
            'title' =>  'Delete' ,
            'class' =>  'btn btn-danger',
            'icon' => 'fa fa-trash',
            'attributes' => [
                'data-target' => "sweetAlert" ,
                'data-alert_title' => "Delete Post",
                'data-alert_type' => "warning",
                'data-alert_href' => $delete_alert_url
            ]
        ]: null,
    ]
])


<div id="post-draft-publish">
    <div class="row">
        @include('partials.fields.radio', [
            'column' => 12,
            'radio_id' => 'radio_draft',
            'label' => 'Draft',
            'value' => 'draft',
            'name' => 'is_publish',
            'checked' => (!isset($post) || $post->isDraft)
        ])

        @include('partials.fields.radio', [
            'column' => 12,
            'radio_id' => 'radio_publish',
            'label' => 'Publish',
            'value' => 'publish',
            'name' => 'is_publish',
            'checked' => isset($post) && !$post->isDraft
        ])
    </div>
    <div class="row publish-date-picker"
         style="display: {{ (!isset($post) || $post->isDraft) ? 'none' : 'block'  }}">
        @include('partials.fields.input-timepicker', [
                    'label' => '',
                    'name' => 'published_at',
                    'column_label' => 2,
                    'datetime' => true,
                    'form_line' => true,
                    'value' => $post->published_at ?? ''
        ])
    </div>
</div>

<div class="clearfix"></div>

@include('partials.containers.portlet', [
    'part' => 'close'
])

