<div class="col-md-3"> {{-- right sidebar --}}
    @include('pages.site.posts.sidebar-portlets.publish-draft',[
        'delete_alert_url' => isset($post) ? route("sites.{site}.posts.{post}.ajax-alert",[$site->id, $post->id, 'method=delete']) : null
    ])
</div> {{-- end right sidebar --}}
@if(!empty($dynamic_lists))
    <div class="col-md-3">
        @include('pages.site.posts.sidebar-portlets.dynamic-list-links')
    </div>
@endif
@if(isset($urls))
    <div class="col-md-3">
        @include('pages.site.posts.sidebar-portlets.quick-links')
    </div>
@endif
@if ($post)
<div class="col-md-3">
    <div class="portlet light bordered" id="quick_links">
        <div class="portlet-title tabbable-line ">
            <div class="caption">
                <i aria-hidden="true" class="fa fa-thumbs-o-up"></i>
                <span class="caption-subject font-dark bold uppercase">Likes</span>
            </div>
        </div>
        <small class="portlet-body">
            <input type="number" name="likes_count" class="form-control" value="{{ $post->likes_sum ?? 0 }}" />
        </small>
    </div>
</div>
@endif

