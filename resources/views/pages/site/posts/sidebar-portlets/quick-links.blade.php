<div class="portlet light bordered" id="quick_links">
    <div class="portlet-title tabbable-line ">
        <div class="caption">
            <i aria-hidden="true" class="fa fa-television"></i>
            <span class="caption-subject font-dark bold uppercase">Quick Links</span>
        </div>
    </div>
    <small class="portlet-body">
        <ul>
            @foreach($urls as $link)
                <li>
                    <a class="btn" target="_blank" href="//{{ $link['full_url'] }}?render_draft=1&no_cache=1">
                        <i class="fa fa-chain"></i> Preview
                    </a>
                    @if ($link['page'])
                        <a class="btn" target="_blank"
                           href="/sites/{{ $site->id }}/templates/{{ $link['page']->template_id }}">
                            <i class="fa fa-edit"></i> Template
                        </a>
                        <a class="btn" target="_blank"
                           href="/sites/{{ $site->id }}/pages/{{ $link['page']->post_id }}/edit">
                            <i class="fa fa-edit"></i> Page
                        </a>
                    @endif
                    <div>
                        <input type="text" class="form-control" readonly value="{{ $link['path'] }}"/>
                    </div>
                </li>
            @endforeach
        </ul>
        <small>When you open links from here they will be rendered even while in draft mode, without cache.</small>
    </small>
</div>