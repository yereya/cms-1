@include('partials.containers.portlet', [
    'part'      => 'open',
    'title'     => 'Dynamic lists',
])

@if(!empty($dynamic_lists->all()))
    <ul>
        @foreach($dynamic_lists as $id => $name)
            <li>
                <a href={{route('sites.{site}.dynamic-list.edit',[$site->id, $id])}}>
                    {{$name}}
                </a>
            </li>
        @endforeach
    </ul>
@else
    <span>This post does not appear in any dynamic list</span>
@endif

@include('partials.containers.portlet', [
    'part' => 'close'
])
