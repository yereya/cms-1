@extends('layouts.metronic.main')

@section('page_content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.20.1/vis-timeline-graph2d.min.css">
    <div id="post_revisions" v-show="true" style="display:none">
        <div class="portlet light bordered">
            <input type="date" v-model="form.start_date"/>
            <input type="date" v-model="form.end_date"/>
            <button @click="getRevisions">Update Timeline</button>
            <div id="visualization" v-show="!loaders.timeline"></div>
            <loader :visible="loaders.timeline"></loader>
        </div>
        <div class="portlet light bordered" v-if="selected_revision"
             v-for="revision in selected_revision">
            <div class="portlet-title tabbable-line ">
                <div class="caption">
                    <i aria-hidden="true" class="fa fa-exchange"></i>
                    <span class="caption-subject font-dark bold uppercase">@{{ revision.key }}</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-4">
                        <strong>CURRENT</strong>
                    </div>
                    <div class="col-md-4">
                        <strong>SELECTED <a href="#"
                                            class="pull-right"
                                            @click="useRevision(revision.key,revision)">
                                <i class="fa fa-arrow-up"></i> REVISION PROPERTY</a>
                        </strong>
                        <small v-if="window.cms.users[revision.user_id]">
                            <img v-if="window.cms.users[revision.user_id].thumbnail"
                                 v-bind:src="window.cms.users[revision.user_id].thumbnail"
                                 class="profile-icon" />
                            @{{ window.cms.users[revision.user_id].first_name }} @{{ window.cms.users[revision.user_id].last_name }}
                        </small>
                    </div>
                    <div class="col-md-4">
                        <strong>PREVIOUS <a href="#"
                                            v-if="previous_revision && previous_revision[revision.key] && previous_revision[revision.key].revision"
                                            @click="useRevision(revision.key,previous_revision[revision.key].revision)"
                                            class="pull-right">
                                <i class="fa fa-arrow-up"></i> REVISION PROPERTY</a>
                        </strong>
                        <small v-if="window.cms.users[revision.user_id]">
                            <img v-if="window.cms.users[revision.user_id].thumbnail"
                                 v-bind:src="window.cms.users[revision.user_id].thumbnail"
                                 class="profile-icon" />
                            @{{ window.cms.users[revision.user_id].first_name }} @{{ window.cms.users[revision.user_id].last_name }}
                        </small>
                    </div>
                </div>
                <div class="row diff">
                    <div class="col-md-4">
                        <div class="change-row">
                            <span class="no-change animate">@{{ printFromPostByKey(revision.key) }}</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="change-row">
                            <div v-html="diff(revision)"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <loader :visible="loaders.previous"></loader>
                        <div class="change-row"
                             v-if="previous_revision && previous_revision[revision.key] && previous_revision[revision.key].revision">
                            <div v-html="diff(previous_revision[revision.key].revision)"></div>
                        </div>
                        <div class="alert alert-warning" v-else v-show="!loaders.previous">
                            No previous diff was found for the selected diff point.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        {!! $inline_json_data !!};
    </script>
@endsection