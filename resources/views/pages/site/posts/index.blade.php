@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => "Posts index",
        'actions' => [
            [
                'drop_down' => true,
                'title' => 'Actions',
                'icon' => 'fa fa-th',
                'menus' => $portlet_actions_menu_items,
            ]
        ]
    ])

    @include('pages.site.posts.filters.posts-filters')

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'posts-table',
        'route' => 'sites.{site}.posts.datatable',
        'route_params' => [$site->id],
        'server_side' => true,
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[1, "asc"]]',
            'data-route_method' => 'list',

            //the "processing" loader
            'data-blocking_loader' => 'true',

        ],
        'columns' => [
            [
                'label' => 'Id',
                'attributes' => [
                    'width' => '2%',
                ]
            ],
            'Name',
            'Slug',
            'content_type',
            'Section',
            'Update',
            'Status',
            'Actions',
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop