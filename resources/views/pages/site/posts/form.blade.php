@extends('layouts.metronic.main')

@section('page_content')
    {!! Form::open([
      'route' => isset($post) ? ['sites.{site}.posts.update', $site->id, $post->id] : ['sites.{site}.posts.store', $site->id,"content_type_id=".$content_type->id], 'method' => isset($post) ? 'PUT' : 'POST', 'class' => 'form-horizontal', 'role' => 'form', 'files' => true
    ]) !!}
<div class="row">
    <div class="col-md-9"> {{--left portlets--}}
        @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => $content_type->name . ' DATA FORM',
            'has_form' => true,
            'actions' => [
                    [
                        'type' => 'link',
                        'title' => 'Revisions',
                        'target' => './revisions',
                        'class' => 'btn-primary',
                        'icon' => 'fa fa-archive'
                    ],
                    [
                        'type' => 'submit',
                        'title' => 'Save',
                        'class' => 'btn-primary',
                        'icon' => 'fa fa-save fw'
                    ],
                   isset($post) ?
                    [
                        'icon' => 'fa fa-cog',
                        'target' => route('sites.{site}.settings.ajax-modal', [$site->id, 'method=showModal', 'type='.SiteSetting::getContentTypeItemType($content_type->id,$post->id)]),
                        'attributes' => [
                            'data-toggle' => 'modal',
                            'data-target' => '#ajax-modal',
                        ]
                    ]: null,
            ]
        ])

        <div class="form-body">
            <div class="form-group">
                @include('partials.fields.input', [
                        'label' => 'Title',
                        'name' => 'name',
                        'value' => $post->name ?? '',
                        'type' => 'text'
                    ])
                @include('partials.fields.input', [
                            'name' => 'slug',
                            'value' => $post->slug ?? '',
                            'type' => 'text',
                            'attributes' => [
                                'data-sanitizer_watch' => 'input[name=name]',
                                'data-sanitizer_method' => 'toSlug'
                            ]
                        ])
                @include('partials.fields.select', [
                            'label' => 'Section',
                            'name' => 'section_id',
                            'list' => $sections,
                            'value' => $post->section_id ?? $user_selected_section,
                        ])

                @include('partials.fields.select', [
                            'name' => 'labels',
                            'multi_select' => true,
                            'list' => $labels,
                            'value' => $post->labels ?? null
                        ])

                @include('partials.fields.select', [
                            'label' => 'Content Author',
                            'name' => 'content_author_id',
                            'list' => $content_authors,
                            'value' => $post->content->content_author_id ?? '',
                        ])
            </div>
            <hr>

            @include('pages.site.posts.content-type.fields-repeater', [
                'data' => $post->content ?? null,
                'content_type' => $content_type,
                'site' => $site,
                'fields' => $fields
            ])

            @include('partials.containers.portlet', [
                'part' => 'close'
            ])
        </div>

        @include('pages.site.posts.redirect-links')

        {{-- Grouped Fields --}}
        @if($grouped_fields ?? false)
            @foreach($grouped_fields as $grouped_field)
                @include('partials.containers.portlet', [
                    'part' => 'open',
                    'title' => $grouped_field[0]->fieldGroup->name ?? '',
                    'actions' => [
                                    [
                                    'target' => "javascript:window.history.back()",
                                    'title' =>  'Back to List' ,
                                    'class' =>  'btn btn-default',
                                    'icon' => 'fa fa-cancel'
                                    ],
                                    [
                                    'type' => 'submit'  ,
                                    'title' =>  'Save' ,
                                    'class' =>  'btn btn-primary',
                                    'icon' => 'fa fa-save'
                                    ],
                      ]
                ])

                @include('pages.site.posts.content-type.fields-repeater', [
                    'data' => $post->content ?? null,
                    'content_type' => $content_type,
                    'site' => $site,
                    'fields' => $grouped_field
                ])

                @include('partials.containers.portlet', [
                    'part' => 'close'
                ])
            @endforeach
        @endif
        {{-- End Grouped Fields --}}
        @include('partials.containers.sites.page.seo-portlet',[
            'seo_settings' => $post ?? null
        ])

    </div>{{-- end left portlet --}}

    @include('pages.site.posts.sidebar-portlets.form-side-bar', [
      'site' => $site ,
      'post' => $post ?? null
    ])

</div> {{--end of row--}}
{!! Form::close() !!}
@stop