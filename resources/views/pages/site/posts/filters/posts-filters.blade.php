<div class="row table-filters">
    {!! Form::open([
        'route' => ['sites.{site}.posts.datatable', $site->id],
        'method' => 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',

    ]) !!}
    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'section_id',
            'id' => 'sections_select',
            'label' => 'Sections',
            'column' => 3,
            'list' => $sections,
            'value' => request()->get('section_id')
        ])

        @include('partials.fields.select', [
            'name' => 'post_type',
            'id' => 'post_type',
            'label' => 'Type',
            'force_selection' => true,
            'column' => 3,
            'list' => ['drafts_and_published' => 'Drafts & Published', 'drafts' => 'Drafts', 'published' => 'Published', 'trashed' => 'Trashed'],
            'value' => request()->get('post_type') ?? 'draft_and_published'
        ])

        @include('partials.fields.select', [
            'name' => 'content_type_id',
            'id' => 'content_types_select',
            'label' => 'Content Types',
            'column' => 3,
            'list' => $content_types,
            'value' => request()->get('content_type_id')
        ])

        @include('partials.fields.select', [
            'name' => 'label_id',
            'id' => 'labels_select',
            'label' => 'Labels',
            'column' => 3,
            'list' => $labels,
            'value' => request()->get('label_id')
        ])

        <div class="form-group form-md-line-input pull-right col-md-2">
            @include('partials.fields.button', [
                'value' => 'Filter',
                'class' => ['full-width', 'blue'],
                'column' => 3,
                'right' => true
            ])
        </div>
    </div>
    {!! Form::close() !!}
</div>
