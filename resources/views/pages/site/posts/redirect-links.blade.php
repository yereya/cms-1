@if (isset($errors->messages()['redirect_links']) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->messages()['redirect_links'] as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Redirect Links',
    'has_form' => true,
   ]
)

@if(isset($post) && !$post->redirectUrls->isEmpty())
    @foreach($post->redirectUrls as $index => $redirectUrls)
        @include('pages.site.posts.redirect-link',[
           'redirectUrls' => $redirectUrls,
           'index' => $index
        ])
    @endforeach
@else
    @include('pages.site.posts.redirect-link')
@endif

@include('partials.containers.portlet', [
    'part' => 'close'
])

