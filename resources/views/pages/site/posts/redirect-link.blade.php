@include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 1])

@include('partials.fields.input', [
        'label' => 'url',
        'name' => 'redirect_links[url][]',
        'require' => true,
        'value' => $redirectUrls->url ?? '',
        'column' => 5,
        'column_input' => 12,
        'column_label' => 12,
        'form_line' => true,
        'type' => 'text',
    ])

@include('partials.fields.input', [
        'label' => 'Slug',
        'name' => 'redirect_links[slug][]',
        'require' => true,
        'value' => $redirectUrls->slug ?? '',
        'column' => 3,
        'column_input' => 12,
        'column_label' => 12,
        'form_line' => true,
        'type' => 'text',
 ])

@include('partials.fields.radio', [
    'name' => 'redirect_links[default][]',
    'label' => 'Default',
    'value' => $index ?? "0",
    'checked' => $redirectUrls->is_default ?? null,
    'radio_id' =>  isset($index) ? 'redirect_urls_' . $index : 'redirect_urls'
])

@include('partials.containers.actions', [
    'column' => 2,
    'class' => [
        'type' => 'form-group form-md-line-input tb-actions-duplicate-group',
    ],
    'actions' => [
        [ 'action' => 'clone'],
        [ 'action' => 'remove']
    ]
])

@include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => 0])
