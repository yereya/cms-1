<div class="modal-info">
    <p>In order to delete this post you need to delete the following widgets:</p>
    <ul>
        @foreach($widget_data_to_delete as $item)
            <li><strong>{{$item->name}} </strong>
                @if (isset($item->widgetDataItems->template))
                    <div> In Template : <a
                                href="{{route('sites.{site}.templates.show', [$site_id, $item->widgetDataItems->template])}}"
                                target="_blank"
                        >{{$item->widgetDataItems->template->name}}</a>
                    </div>
                @else
                    <div>No template</div>
                @endif
            </li>
        @endforeach
    </ul>
</div>

