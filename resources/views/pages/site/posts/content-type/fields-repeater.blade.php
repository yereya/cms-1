@foreach($fields as $field)
    <div data-slug_name="{{ $field->name }}">
        @if(in_array($field->type, ['text', 'number', 'email', 'url', 'phone_number']))
            @include('partials.fields.input', [
                'name' => $field->name,
                'label' => $field->display_name,
                'value' => $data->{$field->name} ?? $field->default,
                'type' => $field->type == 'url' ? 'text' : $field->type,
                'pattern' => $field->type == 'url' ? '(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})' : false,
                'help_text' => $field->metadata->instructions ?? '',
                'attributes' => [
                    'placeholder' => $field->metadata->placeholder ?? ''
                ]
            ])
        @elseif(in_array($field->type, ['textarea', 'wysiwyg']))
            @include('partials.fields.textarea', [
                'name' => $field->name,
                'label' => $field->display_name,
                'class' => [$field->type=='wysiwyg' ? 'wysiwyg' : ''],
                'value' => $data->{$field->name} ?? $field->default,
                'help_text' => $field->metadata->instructions ?? '',
                'attributes' => [
                    'placeholder' => $metadata->placeholder ?? '',
                    'data-route' => route('elfinder.tinymce4', ['site' => $site->id]),
                    'data-route_page_list' => route('sites.{site}.pages.pages-path', $site->id)
                    ]
            ])
        @elseif(in_array($field->type, ['percent']))
            @include('partials.fields.input', [
                'name' => $field->name,
                'label' => $field->display_name,
                'value' => $data->{$field->name} ?? $field->default,
                'pattern' => '[-+]?[0-9]*\.?[0-9]+',
                'addon' => '%',
                'help_text' => $field->metadata->instructions ?? '',
                'attributes' => [
                    'placeholder' => $field->metadata->placeholder ?? ''
                ]
            ])
        @elseif($field->type == 'image')
            <div class="row">
                <div class="container" style="padding:10px">
                    @include('partials.containers.image-upload',[
                        'name' => $field->name,
                        'form_line' => true,
                        'label' => $field->display_name,
                        'src' => $data->{$field->name} ?? '',
                        'help_text' => $field->metadata->instructions ?? '',
                        'media_id' => $data->{$field->name . '_media_id'} ?? '',
                        'href' => route('elfinder.ajax-modal',
                            ['site' => $site->id, $content_type->id, 'method' => 'fileModal', 'field' => $field->name]
                            )
                    ])
                </div>
            </div>
        @elseif($field->type == 'checkbox')
            @include('partials.fields.checkbox', [
                'label' => $field->display_name,
                'wrapper' => true,
                'type' => 'checkbox',
                'list' => [
                    [
                        'name' => $field->name,
                        'value' => 1,
                        'checked' => $data->{$field->name} ?? 1
                    ],
                ],
            ])
        @elseif($field->type == 'checkboxes')
            @include('partials.containers.sites.custom-fields.containers.checkboxes', [
                'field' => $field,
                'data' => $data->{$field->name} ?? null,
                'metadata' => $field->metadata->instructions ?? ''
            ])
        @elseif($field->type == 'select')
            @include('partials.containers.sites.custom-fields.containers.select', [
                'field' => $field,
                'data' => $data->{$field->name} ?? null,
                'metadata' => $field->metadata->instructions ?? ''
            ])
        @elseif($field->type == 'multi_select')
            @include('partials.containers.sites.custom-fields.containers.multi_select', [
                'field' => $field,
                'data' => $data->{$field->name} ?? null,
                'metadata' => $field->metadata->instructions ?? ''
            ])
        @elseif($field->type == 'date_picker')
            @include('partials.fields.input-timepicker', [
                 'name' => $field->name,
                 'datetime' => true,
                 'form_line' => true,
                 'value' => $data->{$field->name} ?? date('Y-m-d G:i')
            ])
        @elseif($field->type == 'currency')
            @include('partials.containers.sites.custom-fields.containers.currency', [
                'field' => $field,
                'value' => $data->{$field->name} ?? $field->default,
                'metadata' => $field->metadata->instructions ?? ''
            ])
        @elseif($field->type == 'content_type_link')
            @include('partials.containers.sites.custom-fields.containers.content_type_link', [
                'field' => $field,
                'value' => $data->{$field->name} ?? $field->default,
                'metadata' => $field->metadata->instructions ?? ''
            ])
        @endif
    </div>

@endforeach


