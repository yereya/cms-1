@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Pages',
        'has_form' => true,
        'actions_class' => 'nested-sortable-buttons',
        'actions' => [
           !$can['edit'] ? null:
           [
            'icon' => 'fa fa-pencil',
            'title' => 'Edit routes',
            'attributes' => [
               'id' => 'np-edit-nestable'
            ],
            'class' => 'btn green'
           ],
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New Page',
                'target' => route('sites.{site}.pages.create', $site->id)
            ],
            [
                'icon' => 'fa fa-cog',
                'target' => route('sites.{site}.settings.ajax-modal', [$site->id, 'method=showModal', 'type='.SiteSettingDefault::TYPE_PAGES]),
                'attributes' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#ajax-modal',
                ]
            ],
        ]
    ])

    <div class="row">
        @include('partials.containers.nestable.nestable', [
            'id' => 'nestable',
            'can' => $can,
            'tree' => $pages_tree,
            'child' => ['childrenRecursive'],
            'subtitle' => 'slug',
            'tooltip' => 'Slug',
            'no_nest' => true,
            'control' => [
                'url' => route('sites.{site}.pages.order', $site->id)
            ],
            'attributes' => [
                'data-max_levels' => 5,
                'data-protect_root' => false,
                'data-start_collapsed' => true,
                'data-type' => 'page'
            ],
            'adds' => [
                function ($item) {
                    $res = '<span class="label left-offset label-xs label-info tooltips" tootlip="Page type" data-original-title="Page Type" title="Page Type">' . !empty($item->page) && !empty($item->page->type) ? $item->page->type : 'no-page-type associated' . '</span>';

                    if($item->published_at) {
                      $res .= '<span class="label left-offset label-xs bg-blue-chambray tooltips" tootlip="Page type" data-original-title="Page Type" title="Page Type">Published</span>';
                    }

                    return $res;
                }
            ],
            'actions' => [
                function ($item) use ($site, $can) {
                    $res = '<a href="' . $item->href . '" target="_blank" title="Preview page"
                          class="tooltips"><span aria-hidden="true" class="fa fa-link" ></span></a>';

                    $res .= '<a href="' . route('sites.{site}.pages.ajax-modal', [$site->id, $item->id, "method=templatesInfo"]) . '" target="_blank" title="Page Info"
                          class="tooltips" data-toggle="modal" data-target="#ajax-modal" ><span aria-hidden="true" class="fa fa-info-circle" ></span></a>';

                    if ($can['edit']) {
                        $res .= '<a href="' . route('sites.{site}.pages.edit', [$site->id, $item->id]) . '"
                               title="Edit page"
                               class="tooltips"
                              ><span aria-hidden="true"class="fa fa-pencil"></span></a>';
                    }

                    if ($can['add'] ) {
                        $res .= '<a href="' . route('sites.{site}.pages.create', [$site->id, "parent_id" => $item->id]) . '"
                              title="Add child page"
                              class="tooltips"
                              ><span aria-hidden="true" class="fa fa-plus" ></span></a>';

                        if ($item->children->isEmpty()) {
                            $res .= '<span> '. View::make('partials.containers.buttons.alert', [
                                    'route' => route('sites.{site}.posts.{post}.ajax-alert', [
                                        $site->id,
                                        'post_id' => $item->id,
                                        'method'    => 'duplicate'
                                    ]),
                                    'title' => 'Duplicate Page',
                                    'tooltip' => 'Duplicate Page',
                                    'icon'  => "fa fa-files-o",
                                    'text'  => 'Are You sure you want to duplicate Post?',
                                    'type'  => 'warning'
                                ])->render(). ' </span>';
                        }
                    }


                    if ($can['delete']) {
                         $res .= '<a data-target="sweetAlert"
                              data-alert_title="Page Delete"
                              data-alert_type="warning"
                              data-alert_href="' . route('sites.{site}.pages.{page}.ajax-alert', [$site->id, $item->id, 'method=delete']) . '"
                              class="tooltips page_delete_link"
                              title="Delete page"
                           ><span aria-hidden="true" class="fa fa-trash"></span></a>';
                    }

                    if (! $item->children->isEmpty()) {
                        $res .= '<span class="disclose fa fa-caret-down"></span>';
                    }

                    return $res;
                }
            ],
        ])
    </div>

    </div>

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop