<div class="col-md-3"> {{-- right sidebar --}}
    @include('pages.site.posts.sidebar-portlets.publish-draft',[
        'delete_alert_url' => isset($post) ? route("sites.{site}.pages.{page}.ajax-alert",[$site->id, $post->id, 'method=delete']) : null
    ])
</div> {{-- end right sidebar --}}
