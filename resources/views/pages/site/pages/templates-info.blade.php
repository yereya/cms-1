@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => "Page Templates Posts Info",
])

@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Templates Info'
    ])

@include('partials.containers.data-table', [
    'data' => $templates_info,
    'attributes' => [
        'data-page-length' => '20'
    ],
    'columns' => [
       'name',
       'device',
       'type',
       [
               'key' => 'actions',
               'value' => function ($data) {
                      $res =  '<a href="' . $data['builder_url'] . '"
                                   title="Template builder"
                                   class="tooltips"
                                   > <span aria-hidden="true" class="fa fa-cogs" ></span> </a>';

                      $res .= '<a href="' . $data['page_url'] . '" target="_blank" title="Page link"
                              class="tooltips"> <span aria-hidden="true" class="icon-link" ></span> </a>';

                  return $res;
                }
       ]
    ]
])

@include('partials.containers.portlet', [
    'part' => 'close'
])


@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Posts Info'
    ])

@include('partials.containers.data-table', [
    'data' => $posts_info,
    'attributes' => [
        'pageLength' => '20'
    ],
    'columns' => [
       [
          'label' => 'Post Name',
          'key' => 'name'
       ],
       [
          'label' => 'Content Type',
          'key' => 'content_type'
       ],
       'slug',
       [
           'key' => 'labels',
           'value' => function ($data) {
                 return $data['labels'];
           }
       ],
       [
               'key' => 'actions',
               'value' => function ($data) {
                   $res = '';
                     if(isset($data['desktop_template_url'])) {
                          $res .=  '<a href="' . $data['desktop_template_url'] . '"
                                       title="Desktop Template"
                                       class="tooltips"
                                       > <span aria-hidden="true" class="icon-screen-desktop" ></span> </a>';
                     }

                     if(isset($data['mobile_template_url'])) {
                          $res .=  '<a href="' . $data['mobile_template_url'] . '"
                                       title="Mobile Template"
                                       class="tooltips"
                                       > <span aria-hidden="true" class="icon-screen-smartphone" ></span> </a>';
                     }

                      $res .= '<a href="' . $data['page_url'] . '" target="_blank" title="Page Link"
                              class="tooltips"> <span aria-hidden="true" class="icon-link" ></span> </a>';

                      $res .= '<a href="' . $data['post_url'] . '" target="_blank" title="Edit Post"
                              class="tooltips"> <span aria-hidden="true" class="icon-pencil" ></span> </a>';

                  return $res;
                }
       ]
    ]
])

@include('partials.containers.portlet', [
    'part' => 'close'
])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => [
            "paging" => true
        ]
    ]
])
