@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site AB Test Location Rules Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($rule)
            ? ['sites.{site}.ab-tests.{test}.location-rules.update', $site->id, $test->id, $rule->id]
            : ['sites.{site}.ab-tests.{test}.location-rules.store', $site->id, $test->id],
        'method' => isset($rule) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.select', [
            'id' => 'ab_test_rule_country_id',
            'name' => 'country_iso_code',
            'label' => 'Country',
            'value' => isset($rule) ? $rule->country_iso_code : null,
            'list' => $countries->pluck('country_name', 'country_iso_code')
        ])

        @include('partials.fields.select', [
                 'name' => 'city_id',
                 'label' => 'City',
                 'id' => 'ab_test_rule_city_id',
                 'ajax' => true,
                 'attributes' => [
                     'data-min_input' => 0,
                     'data-dependency_selector[0]' => '#ab_test_rule_country_id',
                     'data-method' => 'listCities',
                     'data-api_url' => route('sites.{site}.ab-tests.{test}.location-rules.select2', [$site->id, $test->id]),
                     'data-value' => $rule->city_id ?? '',
                 ]
             ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(["site" => $site->id, "test" => $test->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($rule) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop