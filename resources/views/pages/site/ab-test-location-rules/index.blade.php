@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' AB Test Location Rules',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add Rule',
                'target' => route('sites.{site}.ab-tests.{test}.location-rules.create', [$site->id, $test->id])
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $rules,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Country',
                'value' => function ($data)  {
                    return $data->country->country_name ?: 'none';
                }
            ],
            [
                'key' => 'City',
                'value' => function ($data)  {
                    return $data->city->city_name ?: 'none';
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name, $site, $test) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('sites.{site}.ab-tests.{test}.location-rules.edit', [$site->id, $test->id, $data->id]) . '" class="tooltips" title="Edit Rule">
                            <span aria-hidden="true" class="icon-pencil"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('sites.{site}.ab-tests.{test}.location-rules.ajax-modal', [$site->id, $test->id, 'rule_id=' . $data->id, 'method=deleteRule']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete Rule" class="icon-trash tooltips"></span>
                        </a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop