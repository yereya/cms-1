@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Temp scss',
    ])

    @include('partials.fields.textarea', [
        'name' => 'temp',
        'label' => 'Temp scss',
        'value' => $content ?? '',
  ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop

