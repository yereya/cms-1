@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Domains',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.domains.create', $site->id)
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $domains,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Domain',
                'value' => function ($data) use ($site) {
                    return "<a href=\"". route('sites.{site}.domains.edit', [$site->id, $data->id]) ."\"><strong>{$data->domain}</strong></a>";
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name, $site) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('sites.{site}.domains.edit', [$site->id, $data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil tooltips" title="Edit Domain"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('sites.{site}.domains.ajax-modal', [$site->id, $data->id, 'method=deleteDomain']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete domain" class="icon-trash tooltips"></span>
                        </a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop