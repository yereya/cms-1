@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Domain Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($domain) ? ['sites.{site}.domains.update', $site->id, $domain->id] : ['sites.{site}.domains.store', $site->id],
        'method' => isset($domain) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}
    {!! Form::hidden('site_id', isset($domain) ? $domain->site_id : $site->id) !!}
    <div class="form-body">

        @include('partials.fields.input', [
            'name' => 'domain',
            'required' => 'true',
            'value' => isset($domain) ? $domain->domain : null
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=> $site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($domain) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop