@extends('layouts.metronic.main')

@section('page_content')
    {!! Form::open([
        'route' => !empty($contact_us_record) ? ['sites.{site}.contact-us.update', $site_id, $contact_us_record->id] : ['sites.{site}.contact-us.store', $site_id],
        'method' => !empty($contact_us_record) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Contact Us Form',
        'has_form' => true
    ])

    @include('partials.fields.input', [
        'name' => 'name',
        'required' => 'true',
        'value' => $contact_us_record->name ?? null
    ])

    @include('partials.fields.input', [
        'name' => 'email',
        'required' => 'true',
        'value' => $contact_us_record->email ?? null
    ])

    @include('partials.fields.textarea', [
        'name' => 'message',
        'required' => 'true',
        'value' => $contact_us_record->message ?? null,
        'class'=>'codemirror',
        'code'=>'html'
    ])

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=> $site_id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($contact_us_record) ? null : 'true'
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close',

    ])

    {!! Form::close() !!}

@stop