@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Devices',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('site-devices.create')
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $devices,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) {
                    return "<a href=\"". route('site-devices.edit', [$data->id]) ."\">{$data->name}</a>";
                }
            ],
            [
                'key' => 'Group',
                'value' => function ($data) {
                    return $data->group;
                }
            ],
            [
                'key' => 'User Agent',
                'value' => function ($data) {
                    return $data->user_agent;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('site-devices.edit', [$data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('site-devices.{device_id}.ajax-modal', [$data->id, 'method=deleteDevice']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete device" class="icon-trash tooltips"></span>
                        </a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop