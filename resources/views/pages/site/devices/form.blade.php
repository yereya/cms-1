@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Device Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($device) ? ['site-devices.update', $device->id] : ['site-devices.store'],
        'method' => isset($device) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'name',
            'required' => 'true',
            'value' => isset($device) ? $device->name : null
        ])

        @include('partials.fields.select', [
            'name' => 'group',
            'label' => 'Group',
            'list' => $groups,
            'value' => isset($device) ? $device->group : null
        ])

        @include('partials.fields.input', [
            'name' => 'user_agent',
            'required' => 'true',
            'value' => isset($device) ? $device->user_agent : null
        ])

    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($device) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop