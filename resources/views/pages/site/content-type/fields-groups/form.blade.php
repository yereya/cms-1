@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($field_group) ? 'Edit Group' : 'Create Group',
        'subtitle' => 'Content Type: ' . $content_type->name . (isset($field_group) ? ' Group: ' . $field_group->name : ''),
        'has_form' => true
    ])

    {!! Form::open([
        'route' =>
            isset($field_group)
                ? ['sites.{site}.content-types.{content_type}.field-groups.update', $site->id, $content_type->id, $field_group->id]
                : ['sites.{site}.content-types.{content_type}.field-groups.store', $site->id, $content_type->id],
        'method' => isset($field_group) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        {!! Form::hidden('site_id', $site->id) !!}
        {!! Form::hidden('content_type_id', $content_type->id) !!}

        @include('partials.fields.input', [
            'label' => 'Title',
            'name' => 'name',
            'required' => 'true',
            'value' => $field_group->name ?? null
        ])

        @include('partials.fields.select', [
            'label' => 'Parent Group',
            'name' => 'parent_id',
            'list' => $groups,
            'attributes' => [
                'data-value' => $field_group->parent_id ?? null
            ]
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=> $site->id])
            ],
            'submit' => 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop