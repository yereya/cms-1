@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Field Groups ( ' . $content_type->name  . ' )',
        'has_form' => true,
        'actions' => [
            [
                'icon' => 'fa fa-pencil',
                'title' => 'Edit groups',
                'attributes' => [
                   'id' => 'np-edit-nestable'
                ],
                'class' => 'btn green'
            ],
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New Group',
                'target' => route('sites.{site}.content-types.{content_type}.field-groups.create', [$site->id, $content_type->id])
            ],
        ]
    ])

    <div class="row">
        @include('partials.containers.nestable.nestable', [
            'id' => 'nestable',
            'can' => $can,
            'tree' => $groups,
            'child' => ['childrenRecursive'],
            'no_nest' => true,
            'control' => [
                'url' => route('sites.{site}.content-types.{content_type}.field-groups.order', [$site->id, $content_type->id])
            ],
            'attributes' => [
                'data-max_levels' => 3,
                'data-protect_root' => false,
                'data-start_collapsed' => false,
                'data-type' => 'page'
            ],
            'actions' => [
                function ($item) use ($site, $can, $content_type) {
                    $res = '';

                    if ($can['edit']) {
                        $res .= '<a href="' . route('sites.{site}.content-types.{content_type}.field-groups.edit', [$site->id, $content_type->id, $item->id]) . '"
                               title="Edit page"
                               class="tooltips"
                              ><span aria-hidden="true"class="fa fa-pencil"></span></a>';
                    }

                    return $res;
                },
                function ($item) use ($site, $can, $content_type) {
                    $res = '';

                    if ($can['delete']) {
                         $res .= '<a data-target="sweetAlert"
                              data-alert_title="Group Delete"
                              data-alert_type="warning"
                              data-alert_href="' . route('sites.{site}.content-types.{content_type}.field-groups.{field_group}.ajax-alert', [$site->id, $content_type->id, $item->id, 'method=deleteGroup']) . '"
                              class="tooltips group_delete_link"
                              title="Delete Group"
                           ><span aria-hidden="true" class="fa fa-trash"></span></a>';
                    }

                    return $res;
                },
            ],
        ])
    </div>

    </div>

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop