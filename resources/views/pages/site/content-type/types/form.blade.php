@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Content Type Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($content_type) ? ['sites.{site}.content-types.update', $site->id, $content_type->id] : ['sites.{site}.content-types.store', $site->id],
        'method' => isset($content_type) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}
    {!! Form::hidden('site_id', $site->id) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'name',
            'required' => 'true',
            'value' => isset($content_type) ? $content_type->name : null
        ])

        @include('partials.fields.input', [
            'name' => 'table',
            'required' => 'true',
            'disabled' => isset($content_type) ? true : false,
            'value' => isset($content_type) ? $content_type->table : null,
            'attributes' => [
                'data-sanitizer_watch' => 'input[name=name]',
                'data-sanitizer_method' => isset($content_type) ?: 'toSnake'
            ]
        ])

        {!! Form::hidden('has_likes', 0) !!}
        @include('partials.fields.checkbox', [
            'label' => 'Has Likes',
            'wrapper' => true,
            'list' => [
                [
                    'name' => 'has_likes',
                    'value' => 1,
                    'checked' => isset($content_type) ? $content_type->has_likes : true
                ],
            ],
        ])

        {!! Form::hidden('has_comments', 0) !!}
        @include('partials.fields.checkbox', [
            'label' => 'Has Comments',
            'wrapper' => true,
            'list' => [
                [
                    'name' => 'has_comments',
                    'value' => 1,
                    'checked' => isset($content_type) ? $content_type->has_comments : true
                ],
            ],
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=>$site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($content_type) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop