@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Content Types',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.content-types.create', $site->id)
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $types,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) {
                    return "<a href=\"". route('sites.{site}.content-types.edit', [$data->site_id, $data->id]) ."\"><strong>{$data->name}</strong></a>";
                }
            ],
            [
                'key' => 'Table',
                'value' => function ($data) {
                    return $data->table;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    $res .= '<a href="'. route('sites.{site}.content-types.{content_type}.fields.index', [$data->site_id, $data->id]) . '"
                    class="tooltips"
                    title="Assign fields">
                        <span aria-hidden="true" class="fa fa-list fa-fw"></span></a>';

                    $res .= '<a href="'. route('sites.{site}.content-types.{content_type}.field-groups.index', [$data->site_id, $data->id]) . '"
                        class="tooltips" title="Field Groups"><span aria-hidden="true" class="fa fa-object-group fa-fw"></span></a>';

                    $res .= '<a href="'. route('sites.{site}.posts.index', [$data->site_id, 'content_type_id='.$data->id]) . '"
                        class="tooltips" title="View Data"><span aria-hidden="true" class="fa fa-table fa-fw"></span></a>';

                    $res .= '<a href="'. route('sites.{site}.comments.index', [$data->site_id, 'content_type_id='.$data->id]) . '"
                        class="tooltips" title="View Comments"><span aria-hidden="true" class="fa fa-comments fa-fw"></span></a>';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('sites.{site}.content-types.edit', [$data->site_id, $data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil fa-fw"></span></a>';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('sites.{site}.content-types.ajax-modal', [$data->site_id, $data->id, 'method=deleteContentType']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete Content Type" class="icon-trash tooltips fa-fw"></span></a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop