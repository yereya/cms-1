@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Content Type Field',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => $action == 'EDIT' ?
            ['sites.{site}.content-types.{content_type}.fields.update', $site->id, $content_type->id, $field->id] :
            ['sites.{site}.content-types.{content_type}.fields.store', $site->id, $content_type->id],
        'method' => $action == 'EDIT' ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}
    {!! Form::hidden('type_id', $content_type->id) !!}

    <div class="form-body">
        <div class="row">
            <div class="col-sm-6">
                @include('partials.fields.input', [
                    'label' => 'Display Name',
                    'name' => 'display_name',
                    'required' => 'true',
                    'value' => $field->display_name ?? null
                ])

                @include('partials.fields.input', [
                    'label' => 'Column Name',
                    'name' => 'name',
                    'help_text' => 'Code name which is used inside the code and DB',
                    'required' => 'true',
                    'value' => $field->name ?? null,
                    'attributes' => [
                        'data-sanitizer_watch' => 'input[name=display_name]',
                        'data-sanitizer_method' => $action == 'EDIT' ?: 'toSnake'
                    ]
                ])

                @include('partials.fields.input', [
                    'name' => 'default',
                    'help_text' => 'Default value',
                    'value' => isset($field) ? $field->default : null
                ])

                @include('partials.fields.select', [
                    'name' => 'index',
                    'list' => [null, 'index', 'unique', 'full_text'],
                    'value' => isset($field) ? $field->index : null
                ])

                @include('partials.fields.checkbox', [
                    'label' => 'Required',
                    'wrapper' => true,
                    'name' => 'is_required',
                    'list' => [
                        [
                            'label' => 'Required',
                            'name' => 'is_required',
                            'checked' => isset($field) ? $field->is_required : false
                        ]
                    ]
                ])

                @include('partials.fields.select', [
                    'name' => 'field_group_id',
                    'label' => 'Group',
                    'list' => $field_group,
                    'value' => isset($field) ? $field->field_group_id : null
                ])

            </div>

            <div class="col-sm-6">
                @include('partials.fields.h4-separator', ['label' => 'Field Type'])

                <div class="md-radio-list form-group form-md-line-input ">
                    @foreach($field_types as $field_type)
                        @include('partials.fields.radio', [
                            'name' => 'type',
                            'radio_id' => 'field_type_' . $field_type['type'],
                            'label' => snakeToWords($field_type['type']),
                            'column' => 12,
                            'value' => $field_type['type'],
                            'checked' => isset($field) && ($field->type == $field_type['type']) ? true : false,
                            'ajax' => true,
                            'attributes' => [
                                'data-route'=> route('sites.{site}.content-types.{content_type}.content-field.show', [
                                        $site->id,
                                        $content_type->id,
                                        'type' => ''
                                    ]),
                                'data-field_id' => isset($field) && ($field->type == $field_type['type']) ? $field->id : ''
                            ]
                        ])
                    @endforeach
                </div>

                <div class="row">
                    <fieldset id="field-metadata"></fieldset>
                </div>
            </div>
        </div>

    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => route('sites.{site}.content-types.{content_type}.fields.index', [$site->id, $content_type->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($field) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop