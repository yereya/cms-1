<div class="row table-filters">
        {!! Form::open([
            'route' => ['sites.{site}.content-types.{content_type}.fields.datatable', $site->id, $content_type->id],
            'method' => 'POST',
            'class' => 'form-horizontal',
            'role' => 'form',

        ]) !!}

    <div class="form-inline">
        @include('partials.fields.select', [
            'name' => 'groups',
            'label' => 'Groups',
            'id' => 'group_id',
            'column' => 4,
            'list' => $groups,
            'class' => 'select_conditioner',
            'value' => [],
        ])

        <div class="form-group form-md-line-input col-md-1">
            @include('partials.fields.button', [
                'value' => 'Filter',
                'right' => true,
            ])
        </div>
    </div>

    {!! Form::close() !!}
</div>
