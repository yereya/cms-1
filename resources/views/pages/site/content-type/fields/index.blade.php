@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Content Type ' .$content_type->name . ' Fields',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.content-types.{content_type}.fields.create', [$site->id, $content_type->id])
            ]
        ]
    ])

    <div class="row">
        @include('partials.containers.nestable.nestable', [
            'id' => 'nestable',
            'can' => $can,
            'tree' => $groups,
            'child' => ['fields', 'childrenRecursive'],
            'subtitle' => 'type',
            'tooltip' => 'Field Type',
            'attributes' => [
                'data-max_levels' => 4,
                'data-protect_root' => true,
                'data-start_collapsed' => true

            ],
            'returnable_values' => [
                'type' => function ($item) {
                    return isset($item->type) ? 'field' : 'group';
                }
            ],
            'control' => [
                'url' => route('sites.{site}.content-types.{content_type}.fields.priority', [$site->id, $content_type->id]),
                'show' => true
            ],
            'adds' => [
                function ($item) {
                    $type = isset($item->type) ? 'Field' : 'Group';
                    $class = isset($item->type) ? 'label-success' : 'label-info';
                    return '<span class="label left-offset label-xs ' . $class . ' tooltips" tootlip="Type" data-original-title="Type" title="Type">' . $type . '</span>';
                }
            ],
            'actions' => [
                function ($item) use ($can, $site, $content_type) {
                    $res = '';
                    if ($can['edit']) {
                        if (isset($item->type)) {
                            $res .= '<a href="' . route('sites.{site}.content-types.{content_type}.fields.{field}.duplicate',
                                [$site->id, $content_type->id, $item->id]) .'" title="Duplicate Field" class="tooltips"
                                ><span aria-hidden="true" class="fa fa-copy" ></span></a>';
                            $res .= '<a href="' . route('sites.{site}.content-types.{content_type}.fields.edit',
                                [$site->id, $content_type->id, $item->id]) .'" title="Edit Field" class="tooltips"
                                ><span aria-hidden="true" class="fa fa-edit" ></span></a>';
                        } else {
                            $res .= '<a href="' . route('sites.{site}.content-types.{content_type}.field-groups.edit',
                                [$site->id, $content_type->id, $item->id]) .'" title="Edit Group" class="tooltips"
                                ><span aria-hidden="true" class="fa fa-edit" ></span></a>';
                        }
                    }

                    return $res;
                },
                function ($item) use ($can, $site, $content_type) {
                    $res = '';

                    if ($can['delete']) {
                        if (isset($item->type)) {
                            $res .= '<a data-target="sweetAlert" data-alert_title="Field Delete" data-alert_type="warning" data-alert_href="' .
                                route('sites.{site}.content-types.{content_type}.fields.ajax-alert', [$site->id, $content_type->id, $item->id, 'method=deleteField']) . '"
                                class="tooltips page_delete_link" title="Field Delete"><span aria-hidden="true" class="fa fa-trash"></span></a>';
                        } else {
                            $res .= '<a data-target="sweetAlert" data-alert_title="Group Delete" data-alert_type="warning" data-alert_href="' .
                                route('sites.{site}.content-types.{content_type}.field-groups.{field_group}.ajax-alert', [$site->id, $content_type->id, $item->id, 'method=deleteGroup']) . '"
                                class="tooltips page_delete_link" title="Group Delete"><span aria-hidden="true" class="fa fa-trash"></span></a>';
                        }
                    }

                    return $res;
                },
                function ($item) {
                    $res = '';

                    if ($item->fields) {
                        $res .= '<span class="badge badge-danger">' . $item->fields->count() . '</span>';
                        $res .= '<span class="disclose fa fa-caret-down"></span>';
                    }

                    return $res;
                },
            ]
        ])
     </div>
    </div>

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop