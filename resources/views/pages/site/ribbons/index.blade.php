@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => "Ribbons",
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add Ribbon',
                'target' => route('sites.{site}.ribbons.create', $site->id)
            ]
        ]
    ])


    @include('partials.containers.data-table', [
        'data' => $ribbons,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Id',
                'value' => function($item) {
                    return $item->id;
                }
            ],
            [
                'key' => 'Name',
                'value' => function($item) {
                    return $item->name;
                }
            ],
            [
                'key' => 'Image',
                'value' => function($item) {
                    return $item->image;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'value' => function ($item) use ($can, $site) {
                    $res = '';

                    if ($can['edit']) {
                        $res .= '<a href="'
                            . route('sites.{site}.ribbons.edit', [$site->id, $item->id])
                            . '"><span aria-hidden="true" title="Edit" class="icon-pencil tooltips"></span></a>';
                    }

                    if ($can['delete']) {
                        $res.="<a class='tooltips'
                        data-original-title='Delete Ribbon'
                        data-target='sweetAlert'
                        data-alert_title='Delete Ribbon'
                        data-alert_type='warning'
                        data-alert_href='"
                            .route('sites.{site}.ribbons.{ribbons}.ajax-alert', [$site->id, $item->id, 'method=deleteRibbon'])
                            ."'><span class='icon-trash'></span></a>";
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop