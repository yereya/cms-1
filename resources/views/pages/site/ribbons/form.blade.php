@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($ribbon) ? 'Edit Ribbon' : 'Create Ribbon',
        'subtitle' => $ribbon->name ?? '',
        'has_form' => true,
    ])

    {!! Form::open([
        'route' => isset($ribbon) ? ['sites.{site}.ribbons.update', $site->id, $ribbon->id] : ['sites.{site}.ribbons.store', $site->id],
        'method' => isset($ribbon) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',
        'files' => true
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'label' => 'Title',
            'name' => 'name',
            'required' => 'true',
            'value' => $ribbon->name ?? ''
        ])

        @include('partials.fields.input', [
            'label' => 'Html class',
            'name' => 'html_class',
            'value' => $ribbon->html_class ?? ''
        ])

        <div class="container"  style="padding:10px" >
            @include('partials.containers.image-upload',[
                'name' => 'image_id',
                'form_line' => true,
                'label' => $ribbon->image->title ?? '',
                'src' => isset($ribbon->image) ? getBaseUrl() . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . $ribbon->image->path : '',
                'href' => route('elfinder.ajax-modal',
                    ['site' => $site->id, 'method' => 'fileModal', 'field' => 'image_id' ]
                    ),
            ])
        </div>

        @include('partials.containers.form-footer-actions', [
            'buttons' => [
                'cancel' => [
                    'route' => back_url(['site'=> $site->id])
                ],
                'submit' => 'true',
            ],
            'checkboxes' => [
                'create_another' => isset($post) ? null : 'true'
            ]
        ])
    </div>

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop