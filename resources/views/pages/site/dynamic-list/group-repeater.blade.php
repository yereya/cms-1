@if(!empty($group) && is_a($group,\Illuminate\Support\Collection::class))
    @if(count($group)>0)
        @foreach($group as $key =>$group)
            @include($view_path, ['group' => $group])
        @endforeach
    @else
        @include($view_path, ['group' => [] ])
    @endif
@elseif(count($group == 0))
    @include($view_path, ['group' => [] ])
@else
    <div class="alert-block">
        <span class="warning">Group passed should be a collection or array</span>
    </div>
@endif