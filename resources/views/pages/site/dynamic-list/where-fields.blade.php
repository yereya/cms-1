@include('partials.containers.duplicate-group', ['part' => 'open'])

<div class="col-md-offset-1 col-md-4">
    @include('partials.fields.select', [
        'name' => 'filters[field_id][]',
        'label' => 'Filter Field',
        'column_label'=> 3,
        'id' => 'dynamic_filter_field',
        'ajax' => true,
        'attributes' => [
            'data-min_input' => 0,
            'data-dependency_selector[]' => '#content_type_id',
            'data-method' => 'getFields',
            'data-api_url' => route('site-content-type-fields.select2',[$site->id]),
            'data-value' => $group->id ?? ''
        ]
    ])
</div>

<div class="col-md-3">
    @include('partials.fields.select', [
        'name' => 'filters[operator][]',
        'label' => '',
        'value' => $group->pivot->operator ?? '',
        'list' => \App\Libraries\Widgets\WidgetPostList::$operators,
        'force_selection' => true
    ])
</div>

<div class="col-md-2">
    @include('partials.fields.input', [
        'name' => 'filters[value][]',
        'label' => '',
        'placeholder' => 'Value',
        'value' => $group->pivot->value ?? ' '
    ])
</div>

@include('partials.containers.actions', [
'class' => true,
'actions' => [
        [
            'action' => 'clone',
        ],
        [
            'action' => 'remove',
        ]
    ]
])

@include('partials.containers.duplicate-group', ['part' => 'close'])
