<div class="form-body">
    @include('partials.fields.select', [
        'name' => 'carousel[slidesToShow]',
        'label' => 'Slides To Show',
        'list' => [1, 2, 3, 4, 5, 6, 7],
        'value' => $widget_data->data->carousel->slides_to_show ?? 0
    ])

    @include('partials.fields.select', [
        'name' => 'carousel[slidesToScroll]',
        'label' => 'Slides To Scroll',
        'list' => [1, 2, 3, 4, 5, 6, 7],
        'value' => $widget_data->data->carousel->slides_to_scroll ?? 0
    ])

    @include('partials.fields.select', [
        'name' => 'carousel[autoplay]',
        'label' => 'Autoplay',
        'list' => ['False', 'True'],
        'value' => $widget_data->data->carousel->autoplay ?? 0
    ])

    @include('partials.fields.input', [
        'name' => 'carousel[autoplaySpeed]',
        'label' => 'Autoplay Speed(ms)',
        'type' => 'number',
        'value' => $widget_data->data->carousel->autoplay_speed ?? 3000
    ])

    @include('partials.fields.input', [
        'name' => 'carousel[centerPadding]',
        'label' => 'Center Padding(px or %)',
        'value' => $widget_data->data->carousel->center_padding ?? '50px',
    ])

    @include('partials.fields.select', [
        'name' => 'carousel[dots]',
        'label' => 'Show Bullets',
        'list' => ['False', 'True'],
        'value' => $widget_data->data->carousel->dots ?? 0
    ])
</div>