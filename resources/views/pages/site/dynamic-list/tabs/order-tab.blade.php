<div class="form-group">
    <div class="col-md-12">
        <div class="actions pull-right nested-sortable-buttons">
            <button type="button" class="btn btn-success" id="np-edit-nestable" data-input_name="is_static_list"
                data-input_value="manual" style="display: none;"><i class="fa fa-pencil fw"></i> Edit order manually
            </button>

            <button type="button" class="btn btn-danger" id="np-refresh-nestable" data-input_name="is_static_list"
                data-input_value="auto" data-tab_hash="#order-tab" style="display: none;"><i class="fa fa-refresh fw"></i> Refresh and Set auto
            </button>

            <button type="submit" class="btn btn-primary" id="np-refresh-nestable" data-input_name="submit"
                data-input_value="submit" data-tab_hash="#order-tab"><i class="fa fa-refresh fw"></i> Save Changes
            </button>
        </div>
    </div>
</div>

<div class="nestable-lists"></div>