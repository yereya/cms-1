<div class="form-body">
    {!! Form::hidden('is_static_list', $is_static_list) !!}
    @include('partials.fields.input', [
           'name' => 'name',
           'required' => 'true',
           'value' => $dynamic_list_record->name ??  null
       ])

    @if ($is_static_list !== 'auto')
        <div class="alert alert-warning">
            While in manual mode, filters are redundant.
        </div>
    @endif

    <div style="position:relative;opacity: {{ $is_static_list !== 'auto' ? '0.3' : '1' }}">
        @if ($is_static_list !== 'auto')
            <div style="width: 100%;height:100%;position:absolute;left:0;top:0;z-index:5;"></div>
        @endif

        @include('partials.fields.select', [
            'name' => 'content_type_id',
            'label' => 'Content Type',
            'id' => 'content_type_id',
            'required' => true,
            'list' => \App\Entities\Models\Sites\ContentTypes\SiteContentType::where('site_id', $site->id)->get()->pluck('name', 'id'),
            'value' => $dynamic_list_record->content_type_id ?? ''
        ])

        @include('partials.fields.select', [
             'name' => 'labels[id]',
             'label' => 'Labels',
             'id' => 'dynamic_list_label',
             'ajax'=>true,
             'multi_select'=> true,
              'attributes' => [
                 'data-min_input' => 0,
                 'data-dependency_selector[0]' => '#content_type_id',
                 'data-method' => 'list',
                 'data-api_url' => route('sites.{site}.labels.dynamic-list.select2', $site->id),
                 'data-value' => !empty($dynamic_list_record) ?  json_encode($dynamic_list_record->labels->pluck('id') ):'',
             ]
         ])

        @include('pages.site.dynamic-list.group-repeater', [
            'view_path'=>'pages.site.dynamic-list.order-fields',
            'group'=> $dynamic_list_record->orders ?? []
        ])

        @include('pages.site.dynamic-list.group-repeater', [
            'view_path'=>'pages.site.dynamic-list.where-fields',
            'group'=> $dynamic_list_record->filters ?? []
        ])

        <div class="form-group form-md-line-input">
            <div class="col-md-6">
                @include('partials.fields.input', [
                    'name' => 'query_offset',
                    'label' => 'Post Offset',
                    'id' => 'dynamic_list_offset',
                    'form_line' => true,
                    'value' => $dynamic_list_record->query_offset ?? 0,
                    'column_input' => 6,
                    'column_label' => 4,
                ])
            </div>

            <div class="col-md-6">
                @include('partials.fields.input', [
                'name' => 'query_limit',
                'label' => 'Post Limit',
                'id' => 'dynamic_list_limit',
                'form_line' => true,
                'value' => $dynamic_list_record->query_limit ?? 10,
                'column_input' => 6,
                'column_label' => 4,
            ])
            </div>
        </div>
    </div>
</div>