@include('partials.containers.duplicate-group', ['part' => 'open'])


<div class="field-group">
    <div class="col-md-2">
        <h5 class="text-right text-bold">Order Field</h5>
    </div>
    {{--{{dump($group)}}--}}
    <div class="col-md-4">
        @include('partials.fields.select', [
             'name' => 'orders[field_id][]',
             'label' => '',
             'id' => 'dynamic_order_field_id',
             'ajax' => true,
             'column_label' => 0,
             'attributes' => [
                 'data-min_input' => 0,
                 'data-dependency_selector' => '#content_type_id',
                 'data-method' => 'getFields',
                 'data-api_url' => route('site-content-type-fields.select2',[$site->id]),
                 'data-value' => $group->pivot->field_id ?? '',
             ]
         ])
    </div>
    <div class="col-md-4">
        @include('partials.fields.select', [
                    'name' => 'orders[direction][]',
                    'label' => '',
                    'column_label'=>0,
                    'id' => 'dynamic_order_direction',
                    'list'=>['ascending'=>'Ascending',
                             'descending' => 'Descending'],
                    'value'=> $group->pivot->direction ?? 'ascending',
                    'force_selection' => true,
                    'attributes' => [
                         'data-min_input' => 0,
                         'data-dependency_selector' => '#content_type_id',
                         'data-method' => 'listOrders'
                        ]
                ])
    </div>

    @include('partials.containers.actions', [
         'class' => true,
         'actions' => [
              [
                  'action' => 'clone',
              ],
              [
                  'action' => 'remove',
              ]
         ]
    ])
</div>
@include('partials.containers.duplicate-group', ['part' => 'close'])
