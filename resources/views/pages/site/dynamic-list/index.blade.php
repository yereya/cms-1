@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Dynamic Lists',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.dynamic-list.create', [$site->id])
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => [],
        'id' => 'data-table-dynamic-list',
        'route_params' => [$site->id],
        'server_side' => true,
        'url' => route('sites.{site}.dynamic-list.datatable',[$site->id]),
        'attributes' => [
            'data-filter_container' => 'table-filters',
            'data-page-length' => '20',
            'data-order' => '[[0, "asc"]]',
            'data-route_method' => 'list',

            //the "processing" loader
            'data-blocking_loader' => 'true',
        ],
        'columns' => [
          'id',
          'Name',
          'content-type',
          'Updated_at',
          'Actions',
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
