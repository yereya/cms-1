@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => (!empty($dynamic_list_record)? 'Edit '. $dynamic_list_record->name :'Create Dynamic List'),
        'has_form' => true,
    ])
    {!! Form::open([
        'route' => !empty($dynamic_list_record) ? ['sites.{site}.dynamic-list.update', $site->id, $dynamic_list_record->id] : ['sites.{site}.dynamic-list.store', $site->id],
        'method' => !empty($dynamic_list_record) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        <div id="dynamic-list-state" class="alert alert-info">
            The current state of the dynamic list is <span>{{ $is_static_list }}</span>.
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#filters-tab" aria-controls="filters-tab" role="tab"
                                                              data-toggle="tab">Filters</a></li>
                    <li role="presentation">
                        <a href="#order-tab" aria-controls="order-tab" role="tab"
                           data-id="order-tab"
                           data-toggle="tabajax"
                           data-route="{{ route('sites.{site}.posts.dynamic-list', [$site->id, 'widget_data_id' => $dynamic_list_record->id ?? 0]) }}"
                           data-tab_params="filters-tab"
                           data-once="true">Order
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">

                    <div id="filters-tab" role="tabpanel" class="tab-pane active">
                        @include('pages.site.dynamic-list.tabs.filters-tab')
                    </div>

                    <div id="order-tab" role="tabpanel" class="tab-pane">
                        @include('pages.site.dynamic-list.tabs.order-tab')
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('partials.containers.form-footer-actions', [
    'buttons' => [
        'cancel' => [
            'route' => back_url(['site'=> $site->id])
        ],
        'submit' => 'true'
    ],
    'checkboxes' => [
        'create_another' => isset($dynamic_list) ? null : 'true'
    ]
])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
            'part' => 'close'
        ])
    <script type="application/javascript">
        (function () {
            var appConnect = $('script[src*="app.js"]').length;
            if (appConnect == 0) {
                var intervalApp = setInterval(function () {
                    appConnect = $('script[src*="app.js"]').length;
                    if (appConnect > 0) {
                        clearInterval(intervalApp);
                    }
                }, 1000);
            }

            var topAppConnect = $('script[scr*="topApp.js"]').length;

            if (topAppConnect == 0) {
                var intervalTopAll = setInterval(function () {
                    topAppConnect = $('script[src*="topApp.js"]').length;
                    if (topAppConnect > 0) {
                        clearInterval(intervalTopAll);
                        topApp.tabAjax();
                    }
                }, 1000);
            }

        })();

    </script>

@stop
