{!!Form::open([
    'url' => route('sites.{site}.content-authors.destroy', [$site_id, $author->id]),
    'method' => 'delete',
    'class' => 'form-horizontal'
])!!}

<div class="form-body">

    <p> Please choose another author to replace <strong>{{$author->first_name . ' '. $author->last_name}} </strong> author</p>

    <div class="row">
        @include('partials.fields.select', [
            'name' => 'author_to_replace',
            'required' => true,
            'label' => '',
            'list' => $authors,
            'value' => ''
        ])
    </div>

    @include('partials.fields.button', [
      'type' => 'submit',
      'class' => "btn btn-lg red",
      'value' => 'Delete',
    ])

    @include('partials.fields.button', [
      'url' => 'javascript:void()',
      'class' => "btn btn-lg green",
      'value' => 'Cancel',
      'attributes' => [
        'onclick' => "swal.close(); return false;"
      ]
    ])

</div>
{!! Form::close() !!}
<style>
    .swal2-confirm{
        display:none;
    }
</style>
