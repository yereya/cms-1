@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' =>  $page_title ,
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.content-authors.create', $site->id)
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $content_authors,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Name',
                'label' => "First Name",
                'value' => function ($data) use ($site) {
                   { return $data->first_name;}
                }
            ],
            [
                'key' => 'last_name',
                'label' => 'Last Name',
                'filter_type' => 'null',
                'value' => function ($data) use ($site) {
                    return $data->last_name;
                }
            ],
            [
                'key' => 'title',
                'label' => 'Title',
                'filter_type' => 'null',
                'value' => function ($data) use ($site) {
                    return $data->title;
                }
            ],
            [
                'key' => 'description',
                'label' => 'Description',
                'filter_type' => 'null',
                'value' => function ($data) use ($site) {
                    return $data->description;
                }
            ],
            [
                'key' => 'image',
                'label' => 'Image',
                'filter_type' => 'null',
                'value' => function ($data) use ($site) {
                    $src = isset($data->image) && $data->image->path ? getBaseUrl() . DIRECTORY_SEPARATOR . 'top-assets' . DIRECTORY_SEPARATOR . snake_case($site->name) . $data->image->path : '';
                    return '<img src="' . $src .'" class="table-image"  />' ;
                }
            ],
            [

                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($site, $can_delete) {
                    $res = '';

                    $res.="<a class='icon-pencil' href=\"". route('sites.{site}.content-authors.edit', [$site->id, $data->id]) ."\"><strong></strong></a>";

                    if ($can_delete) {
                        $res.="<a class='tooltips'
                        data-original-title='Delete Author'
                        data-target='sweetAlert'
                        data-alert_title='Delete Author'
                        data-alert_type='warning'
                        data-alert_href='".route('sites.{site}.content-authors.{content_author}.ajax-alert', [$site->id, $data->id, 'method=deleteContentAuthor'])."'><span class='icon-trash'></span></a>";
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop
