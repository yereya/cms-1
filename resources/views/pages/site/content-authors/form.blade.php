@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Content Authors Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($content_authors) ? ['sites.{site}.content-authors.update', $site->id, $content_authors->id] : ['sites.{site}.content-authors.store', $site->id],
        'method' => isset($content_authors) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}
    {!! Form::hidden('site_id', $site->id) !!}
    <div class="form-body">

        @include('partials.fields.input', [
            'name' => 'first_name',
            'required' => 'true',
            'value' => isset($content_authors) ? $content_authors->first_name : null
        ])

        @include('partials.fields.input', [
            'name' => 'last_name',
            'required' => 'true',
            'value' => isset($content_authors) ? $content_authors->last_name : null
        ])

        @include('partials.fields.input', [
            'name' => 'title',
            'required' => 'true',
            'value' => isset($content_authors) ? $content_authors->title : null
        ])

        @include('partials.fields.input', [
            'name' => 'description',
            'required' => 'true',
            'value' => isset($content_authors) ? $content_authors->description : null
        ])

        <div class="row" >
            <div class="container"  style="padding:10px" >
                @include('partials.containers.image-upload',[
                    'name' => 'image_id',
                    'form_line' => true,
                    'label' => $content_authors->image->title ?? '',
                    'src' => isset($content_authors->image) ? getBaseUrl() . DIRECTORY_SEPARATOR . 'media' . DIRECTORY_SEPARATOR . $content_authors->image->path : '',
                    'href' => route('elfinder.ajax-modal',
                        ['site' => $site->id, 'method' => 'fileModal', 'field' => 'image_id' ]
                        ),
                ])
            </div>
        </div>
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=> $site->id])
            ],
            'submit' => 'true'
        ],
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop

