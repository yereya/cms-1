@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Members Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($member) ? ['sites.{site}.members.update', $site->id, $member->id] : ['sites.{site}.members.store', $site->id],
        'method' => isset($member) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}
    {!! Form::hidden('site_id', isset($site) ? $site->id : $site->id) !!}
    <div class="form-body">

        @include('partials.fields.input', [
            'name' => 'email',
            'required' => 'true',
            'type' => 'email',
            'value' => isset($member) ? $member->email : null
        ])
        @include('partials.fields.input', [
            'name' => 'name',
            'value' => isset($member) ? $member->name : null
        ])

        @include('partials.fields.select', [
            'label' => 'status',
            'name' => 'Status',
            'list' => [
                'unverified' => 'Unverified',
                'verified' => 'Verified',
                'canceled' => 'Canceled'
            ],
            'force_selection' => true,
            'value' => isset($member) && isset($member->status)
            ? $member->status
            : 0
        ])

        @include('partials.fields.select', [
            'label' => 'Parent group',
            'name' => 'parent_id',
            'list' => $member_groups,
            'multi_select' => 'true',
            'value' => isset($member) && $member->memberGroups->count()
                ? $member->memberGroups->pluck('id')->toArray()
                : null
        ])
        @include('partials.fields.select', [
            'label' => 'EMail Verified',
            'name' => 'email_verified',
            'force_selection' => true,
            'list' => [
                    0=>'No',
                    1=>'Yes'
                ],
            'value' => isset($member)? $member->email_verified :0
        ])


        @include('partials.fields.select', [
            'label' => 'Newsletter Approved',
            'name' => 'newsletter_approved',
            'force_selection' => true,
            'list' => [
                    0=>'No',
                    1=>'Yes'
                ],
            'value' => isset($member)? $member->newsletter_approved :0
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=> $site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($members) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop