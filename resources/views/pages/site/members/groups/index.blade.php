@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Members',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.member-groups.create', $site->id)
            ]
        ]
    ])


    @include('partials.containers.data-table', [
        'data' => $member_groups,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) use ($site) {
                    return "<a href=\"". route('sites.{site}.member-groups.edit', [$site->id, $data->id]) ."\"><strong>{$data->name}</strong></a>";
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($can, $site) {
                    $res = '';

                    if ($can['edit']) {
                        $res .= '<a href="'. route('sites.{site}.member-groups.edit', [$site->id, $data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil tooltips" title="Edit Member"></span>
                        </a> ';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop