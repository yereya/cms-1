@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Member Groups Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($member_group) ? ['sites.{site}.member-groups.update', $site->id, $member_group->id] : ['sites.{site}.member-groups.store', $site->id],
        'method' => isset($member_group) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}
    {!! Form::hidden('site_id', isset($site) ? $site->id : $site->id) !!}
    <div class="form-body">

        @include('partials.fields.input', [
            'name' => 'name',
            'value' => isset($member_group) ? $member_group->name : null
        ])

        @include('partials.fields.select', [
            'label' => 'Parent group',
            'name' => 'parent_id',
            'list' => $member_groups,
            'value' => isset($member_groups) && isset($member_group->parent_id) ? $member_group->parent_id : null
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=> $site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($members) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop