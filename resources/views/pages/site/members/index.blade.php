@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Members',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.members.create', $site->id)
            ],
        ]
    ])


    @include('partials.containers.data-table', [
        'data' => $members,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'created_at'
            ],
            [
                'key' => 'Member',
                'value' => function ($data) use ($site) {
                    return "<strong>{$data->email}</strong>";
                }
            ],
            [
                'key' => 'name'
            ],
            [
                'key' => 'status',
                'value' => function ($data) {
                    switch ($data->status) {
                        case 'unverified' :
                            return "<span class='label label-sm label-primary'>Unverified</span>";

                        case 'verified' :
                            return "<span class='label label-sm label-primary'>Verified</span>";

                        case 'canceled' :
                            return "<span class='label label-sm label-default'>Canceled</span>";
                    }

                    return $str;
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($can, $site) {
                    $res = '';

                    if ($can['edit']) {
                        $res .= '<a href="'. route('sites.{site}.members.edit', [$site->id, $data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil tooltips" title="Edit Member"></span>
                        </a> ';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop