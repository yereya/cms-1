@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site AB Test Rules Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($rule) ? ['sites.{site}.ab-tests.{test}.rules.update', $site->id, $test->id, $rule->id] : ['sites.{site}.ab-tests.{test}.rules.store', $site->id, $test->id],
        'method' => isset($rule) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'key',
            'required' => 'true',
            'value' => isset($rule) ? $rule->key : null
        ])

        @include('partials.fields.textarea', [
            'name' => 'value',
            'required' => 'true',
            'value' => isset($rule) ? $rule->value : null,
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(["site" => $site->id, "test" => $test->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($rule) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop