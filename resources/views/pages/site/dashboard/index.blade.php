@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Dashboard ' . $site->name,
        'actions' => []
    ])


    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop