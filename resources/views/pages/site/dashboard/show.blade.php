@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Sites',
        'actions' => []
    ])


    @include('partials.containers.portlet', [
    'part' => 'close'
])
@stop