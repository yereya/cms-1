@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Label Form',
        'has_form' => true,
    ])

    {!! Form::open([
        'route' => isset($label_collect) ?
            ['sites.{site}.labels.update', $site->id, $label_collect->id] :
            ['sites.{site}.labels.store', $site->id],
        'method' => isset($label_collect) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'name',
            'label' => 'Label',
            'required' => 'true',
            'value' => $label_collect->name ?? null
        ])

        @include('partials.fields.select', [
            'name' => 'parent_id',
            'label' => 'Parent',
            'list' => $label_tree,
            'value' => isset($label_collect) ? $label_collect->parent_id : request()->get('parent_id')
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=>$site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($template) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop