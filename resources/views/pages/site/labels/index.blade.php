@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Labels',
        'has_form' => true,
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New Label',
                'target' => route('sites.{site}.labels.create', $site->id)
            ],
        ]
    ])

    <div class="row">
        <div class="col-xs-12">
            <div class="cf nestable-lists">
                <div class="dd">
                    @include('partials.containers.nested_list', [
                        'tree' => $label_tree,
                        'actions' => function($tree_item) use ($site, $can) {
                            $res = '';
                            if ($can['edit']) {
                                $res .= view('partials.fields.action-icon', [
                                    'url'     => route('sites.{site}.labels.edit', [
                                        $site->id,
                                        $tree_item->id,
                                    ]),
                                    'icon'    => 'icon-pencil',
                                    'tooltip' => 'Edit'
                                ])->render();
                            }

                            if ($can['add']) {
                                $res .= view('partials.fields.action-icon', [
                                    'url'     => route('sites.{site}.labels.create', [
                                        $site->id,
                                        'parent_id' => $tree_item->id
                                    ]),
                                    'icon'    => 'icon-plus',
                                    'tooltip' => 'Add Child'
                                ])->render();
                            }

                            if ($can['delete']) {
                                $res .= view('partials.fields.action-icon', [
                                    'icon'    => 'icon-trash',
                                    'tooltip' => 'Delete',
                                    'attributes' => [
                                        'data-target' => 'sweetAlert',
                                        'data-alert_title' => 'Delete Label',
                                        'data-alert_href' => route('sites.{site}.labels.ajax-alert', [
                                            $site->id,
                                            'method' => 'delete',
                                            'label_id' => $tree_item->id,
                                        ])
                                    ]
                                ])->render();
                            }

                            return $res;
                        }
                    ])
                </div>
            </div>
        </div>
    </div>

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop