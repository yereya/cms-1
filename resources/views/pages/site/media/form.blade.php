@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Media Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($media) ? ['sites.{site}.media.update', $site->id, $media->id] : ['sites.{site}.media.store', $site->id],
        'method' => isset($media) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form',
        'files' => true
    ]) !!}

    <div class="form-body">
        @if(isset($media) && $media->filename != '')
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label">Media</label>
                <div class="col-md-9">
                    <a href="<?php echo $media->url ?>" target="_blank">
                        <img style="height: 300px;" src="<?php echo $media->url ?>"/>
                    </a>
                </div>
            </div>

            @include('partials.containers.image-upload', [
                'name' => 'filename',
            ])
        @else
            @include('partials.containers.image-upload', [
                'name' => 'filename',
                'required' => true
            ])
        @endif

        @include('partials.fields.input', [
            'name' => 'title',
            'value' => isset($media) ? $media->title : null
        ])

        @include('partials.fields.input', [
            'name' => 'alt',
            'value' => isset($media) ? $media->alt : null
        ])

        @include('partials.fields.input', [
            'name' => 'caption',
            'value' => isset($media) ? $media->caption : null
        ])

    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site' => $site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($media) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop