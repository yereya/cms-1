@include('partials.containers.ajax-modal', [
            'part' => 'open',
            'title' => 'File labels',
            'ajax_submit' => true
        ])

{!! Form::open([
    'url'    => route('elfinder.labels', ['site' => $site->id]),
    'method' => 'POST',
    'class'  => 'form-horizontal'
]) !!}

@if (isset($file))
    {!! Form::hidden('media_id', $file->id) !!}
@endif

@include('partials.fields.select', [
    'label' => 'Labels',
    'name' => 'labels',
    'multi_select' => true,
    'list' => $labels,
    'value' => $file_labels
])

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'close' => true
    ]
])

<script>
    (function(){
        App.initSelect2ToSelects();
    })();
</script>