@include('partials.containers.ajax-modal', [
            'part' => 'open',
            'title' => 'File Info',
            'ajax_submit' => true
        ])

{!! Form::open([
    'url'    => route('elfinder.file.update', ['site' => $site->id]),
    'method' => 'POST',
    'class'  => 'form-horizontal'
]) !!}

    @if (isset($file['model']) && $file['model'])
        {!! Form::hidden('media_id', $file['model']['id']) !!}
    @endif

    @include('partials.fields.input', [
        'label' => 'File Alt',
        'name' => 'alt',
        'value' => isset($file['model']) && !empty($file['model']['alt']) ? $file['model']['alt'] : null
    ])

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'close' => true
    ]
])