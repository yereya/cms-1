@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Media',
        'actions' => []
    ])

    <div id="elfinder" data-route="{{ route('elfinder.connector', ['site' => $site->id]) }}" data-process_selected="1"></div>

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop