@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => $title,
])

@include('partials.fields.iframe', [
        'src' => route('elfinder.tinymce4', ['site' => $site->id]),
        'window_size' => true,
        'attributes' => [
            'data-field_name' => $field,
            'data-find_file' => route('elfinder.connector', ['site' => $site->id])
        ]
    ])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ]
])

<style type="text/css">
    /*.tp-table .tp-table .tp-table-cell {*/
    /*display: table-row;*/
    /*}*/
    #ajax-modal .dataTables_wrapper td {
        word-break: break-all;
    }

    .modal-body  .datetimepicker-dropdown-bottom-right {
        display: none;
    }

    .input-group .datetimepicker {
        z-index: 1;
    }
</style>

<script>
    (function () {
        App.initIframeWindowSize({'element': '.modal-body', 'height': 500});
        topApp.fileManagerEvent();
    })();
</script>