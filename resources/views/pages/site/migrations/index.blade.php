@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Migrations',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('site-migrations.create')
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $migrations,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Order',
                'value' => function ($data) {
                    return $data->order;
                }
            ],
            [
                'key' => 'Name',
                'value' => function ($data) {
                    return "<a href=\"". route('site-migrations.edit', [$data->id]) ."\">{$data->name}</a>";
                }
            ],
            [
                'key' => 'Status',
                'value' => function ($data) {
                    if ($data->status) {
                        return '<span class="label label-sm label-success">Active</span>';
                    } else {
                        return '<span class="label label-sm label-danger">Disabled</span>';
                    }
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('site-migrations.edit', [$data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('site-migrations.{migration_id}.ajax-modal', [$data->id, 'method=deleteSite']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete Migration" class="icon-trash tooltips"></span>
                        </a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop