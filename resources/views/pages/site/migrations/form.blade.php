@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Migration Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($migration) ? ['site-migrations.update', $migration->id] : ['site-migrations.store'],
        'method' => isset($migration) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'Name',
            'required' => 'true',
            'value' => isset($migration) ? $migration->name : null
        ])

        @include('partials.fields.checkbox', [
            'label' => 'Status',
            'wrapper' => true,
            'list' => [
                [
                    'name' => 'status',
                    'value' => 1,
                    'checked' => isset($migration) ? $migration->status : 1
                ],
            ],
        ])

        @include('partials.fields.textarea', [
            'label' => 'SQL Up',
            'name' => 'sql_up',
            'required' => 'true',
            'value' => isset($migration) ? $migration->sql_up : null
        ])

        @include('partials.fields.textarea', [
            'label' => 'SQL Down',
            'name' => 'sql_down',
            'required' => 'true',
            'value' => isset($migration) ? $migration->sql_down : null
        ])

    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url()
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($migration) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop