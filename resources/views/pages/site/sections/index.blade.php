@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Sections',
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New',
                'target' => route('sites.{site}.sections.create', $site->id)
            ]
        ]
    ])

    @include('partials.containers.data-table', [
        'data' => $sections,
        'attributes' => [
            'data-page-length' => '20'
        ],
        'columns' => [
            [
                'key' => 'Name',
                'value' => function ($data) use ($site) {
                    return "<a href=\"". route('sites.{site}.sections.edit', [$site->id, $data->id]) ."\"><strong>{$data->name}</strong></a>";
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name, $site) {
                    $res = '';

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= '<a href="'. route('sites.{site}.sections.edit', [$site->id, $data->id]) . '">
                            <span aria-hidden="true" class="icon-pencil"></span>
                        </a> ';
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= '<a href="'. route('sites.{site}.sections.ajax-modal', [$site->id, $data->id, 'method=deleteSection']) . '" data-toggle="modal" data-target="#ajax-modal">
                            <span aria-hidden="true" title="Delete section" class="icon-trash tooltips"></span>
                        </a>';
                    }

                    return $res;
                }
            ]

        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop