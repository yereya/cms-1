@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Section Form',
        'has_form' => true
    ])

    {!! Form::open([
        'route' => isset($section) ? ['sites.{site}.sections.update', $site->id, $section->id] : ['sites.{site}.sections.store', $site->id],
        'method' => isset($section) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'name',
            'required' => 'true',
            'value' => isset($section) ? $section->name : null
        ])

        @include('partials.fields.select', [
            'label' => 'Brand',
            'name' => 'media_id',
            'required' => true,
            'list' => \App\Entities\Models\Bo\Publishers\Media::where('publisher_id', $site->publisher_id)->pluck('media_name', 'id'),
            'value' => isset($section) ? $section->media_id : -1
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=> $site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($section) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop