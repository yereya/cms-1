@extends('layouts.metronic.main')

@section('page_content')
    <style type="text/css">
        iframe {
            height: 90Vh;
            border: 0;
            margin: 0 auto;
            display: block;
        }

        iframe.full {
            width: 100%;
        }

        iframe.tablet-horizontal {
            width: 1024px;
        }

        iframe.tablet-vertical {
            width: 768px;
        }

        iframe.mobile-horizontal {
            width: 640px;
        }

        iframe.mobile-vertical {
            width: 320px;
        }
    </style>
    <iframe class="full" src="{{ route('sites.{site}.templates.builder', [$site->id, $template->id]) }}"></iframe>
@stop