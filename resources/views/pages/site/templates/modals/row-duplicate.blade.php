@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Duplicate Row',
    'event' => 'row-duplicate'
])

{!! Form::open([
    'url'    => '#',
    'method' => 'GET',
    'class'  => 'form-horizontal'
]) !!}

{!! Form::hidden('id', $id) !!}
{!! Form::hidden('is_item', strtolower($is_item) == 'true') !!}

<p>Duplicate Row?</p>
@include('partials.fields.button', [
    'icon'  => 'fa fa-file-o',
    'class' => 'btn-info',
    'value' => 'Duplicate',
    'type'  => 'submit'
])

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ]
])
