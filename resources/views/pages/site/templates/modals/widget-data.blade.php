@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Widget Data',
    'event' => 'widget-data-changed',
    'ajax_submit' => true
])

{!! Form::open([
    'url'    => '#',
    'method' => 'GET',
    'class'  => 'form-horizontal'
]) !!}

{!! Form::hidden('data_id', $data ? $data->id : 0) !!}
{!! Form::hidden('widget_id', $widget->id) !!}

<div class="form-body">
    @include('partials.fields.input', [
        'name' => 'name',
        'value' => $data ? $data->name : null
    ])

    {!! Form::hidden('active', 0) !!}
    @include('partials.fields.checkbox', [
        'label' => 'Active',
        'wrapper' => true,
        'list' => [
            [
                'name' => 'active',
                'value' => 1,
                'checked' => isset($data) ? $data->active : 1
            ],
        ],
    ])

   @foreach($fields as $field)
        @if(in_array($field->type->name, ['text', 'number']))
            @include('partials.fields.input', [
                'label' => $field->title,
                'name' => $field->name,
                'value' => $data ? $data->data->{$field->name} : $field->default,
                'type' => $field->type->name
            ])
        @elseif(in_array($field->type->name, ['textarea', 'wysiwyg']))
            @include('partials.fields.textarea', [
                'label' => $field->title,
                'name' => $field->name,
                'class' => $field->type->name,
                'value' => $data ? $data->data->{$field->name} : $field->default
            ])
        @endif
    @endforeach
</div>

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'close' => true
    ]
])