@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Delete Widget',
    'event' => 'widget-deleted'
])

{!! Form::open([
    'url'    => '#',
    'method' => 'GET',
    'class'  => 'form-horizontal'
]) !!}

{!! Form::hidden('column_id', $column_id) !!}
{!! Form::hidden('id', $id) !!}

<p>Are you should you want to delete this.</p>

@include('partials.fields.button', [
    'icon'  => 'fa fa-trash',
    'class' => 'btn-danger',
    'value' => 'Delete',
    'type'  => 'submit'
])

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ]
])
