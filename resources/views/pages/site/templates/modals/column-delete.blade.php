@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Delete Column',
    'event' => 'column-deleted'
])

{!! Form::open([
    'url'    => '#',
    'method' => 'GET',
    'class'  => 'form-horizontal'
]) !!}

{!! Form::hidden('row_id', $row_id) !!}
{!! Form::hidden('id', $id) !!}

<p>Are you should you want to delete this.</p>

@include('partials.fields.button', [
    'icon'  => 'fa fa-trash',
    'class' => 'btn-danger',
    'value' => 'Delete',
    'type'  => 'submit'
])

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ]
])
