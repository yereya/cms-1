@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Custom HTML',
    'ajax_submit' => true
])

{!! Form::open([
    'url'    => route('sites.{site}.templates.{template}.custom-html.update', [$site_id, $template->id]),
    'method' => 'PUT',
    'class'  => 'form-horizontal'
]) !!}

@include('partials.fields.textarea', [
    'name'  => 'custom_html_head',
    'label' => 'Head',
    'value' => $template->custom_html_head ?? null
])

@include('partials.fields.textarea', [
    'name'  => 'custom_html_body',
    'label' => 'Body',
    'value' => $template->custom_html_body ?? null
])

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save'  => true,
        'close' => true
    ]
])