@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Row',
    'event' => 'row-changed'
])

{!! Form::open([
    'url'    => '#',
    'method' => 'GET',
    'class'  => 'form-horizontal'
]) !!}

{!! Form::hidden('id', $row_data['id'] ?? '') !!}
{!! Form::hidden('parent_column_id', $parent_column_id) !!}

<div class="row">
    <div class="col-sm-6">
        @include('partials.fields.input', [
            'name' => 'html_element_id',
            'label' => 'Element ID',
            'value' => $row_data['html_element_id'] ?? null
        ])

        @include('partials.fields.textarea', [
            'name' => 'html_class',
            'value' => $row_data['html_class'] ?? 'row'
        ])

        @include('partials.fields.textarea', [
            'name' => 'html_container_class',
            'value' => $row_data['html_container_class'] ?? 'container'
        ])

        @include('partials.fields.textarea', [
            'name' => 'html_wrapper_class',
            'value' => $row_data['html_wrapper_class'] ?? null
        ])
    </div>

    <div class="col-sm-6">
        @include('partials.fields.select', [
            'name' => 'total_size',
            'list' => ['12' => '12 Columns Grid'],
            'value' => $row_data['total_size'] ?? 12
        ])

        {!! Form::hidden('locked', 0) !!}
        @include('partials.fields.checkbox', [
            'label' => 'Locked',
            'wrapper' => true,
            'list' => [
                [
                    'name' => 'locked',
                    'value' => 1,
                    'checked' => $row_data['locked'] ?? 0
                ],
            ],
        ])

        {!! Form::hidden('active', 0) !!}
        @include('partials.fields.checkbox', [
            'label' => 'Active',
            'wrapper' => true,
            'list' => [
                [
                    'name' => 'active',
                    'value' => 1,
                    'checked' => $row_data['active'] ?? 1
                ],
            ],
        ])

        @include('partials.fields.select', [
            'name' => 'ab_tests_ids',
            'label' => 'A/B Tests',
            'multi_select' => true,
            'list' => $ab_tests->pluck('name', 'id'),
            'value' => $row_data['ab_tests_ids'] ?? -1,
            'column_label' => 2,
            'attributes' => [
                'data-toggle' => 'collapse',
                'data-target' => '#ab-test-actions'
            ]
        ])

        <div class="row" id="ab-test-actions">
            <span class="col-md-2 control-label">
                *A/B Action
            </span>

            <div class="col-md-10">
                <div class="row">
                    @include('partials.fields.radio', [
                        'column' => 4,
                        'radio_id' => 'ab-test-action-show',
                        'label' => 'Show',
                        'value' => 'show',
                        'name' => 'ab_tests_action',
                        'checked' => isset($row_data['ab_tests_action']) && $row_data['ab_tests_action'] == 'show'
                    ])

                    @include('partials.fields.radio', [
                        'column' => 4,
                        'radio_id' => 'ab-test-action-hide',
                        'label' => 'Hide',
                        'value' => 'hide',
                        'name' => 'ab_tests_action',
                        'checked' => isset($row_data['ab_tests_action']) && $row_data['ab_tests_action'] == 'hide'
                    ])

                    @include('partials.fields.radio', [
                        'column' => 4,
                        'radio_id' => 'ab-test-action-class',
                        'label' => 'Class',
                        'value' => 'class',
                        'name' => 'ab_tests_action',
                        'checked' => isset($row_data['ab_tests_action']) && $row_data['ab_tests_action'] == 'class'
                    ])
                </div>
            </div>

        </div>
    </div>
</div>

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'close' => true
    ]
])
