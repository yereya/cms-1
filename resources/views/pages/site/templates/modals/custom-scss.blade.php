@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Custom Scss',
    'ajax_submit' => true
])

<style>
    div.CodeMirror {
        height: 500px;
    }
</style>

{!! Form::open([
    'url'    => route('sites.{site}.templates.{template}.custom-scss.update', [$site_id, $template->id]),
    'method' => 'PUT',
    'class'  => 'form-horizontal'
]) !!}

@include('partials.fields.textarea', [
    'name'  => 'scss',
    'value' => $template->scss ?? '.' . $template->class . " {\n\n}",
    'no_label' => false,
    //'codemirror' => 'scss',
])

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save'  => true,
        'close' => true
    ],
    'js' => [
        'modal_width' => '900',
        'code_mirror' => true
    ]
])