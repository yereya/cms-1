@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Column',
    'event' => 'column-changed',
])

{!! Form::open([
    'url'    => '#',
    'method' => 'GET',
    'class'  => 'form-horizontal'
]) !!}

{!! Form::hidden('id', $data['id'] ?? '') !!}
{!! Form::hidden('row_id', $row_data['id']) !!}

<div class="row">
    <div class="col-sm-6">
        @include('partials.fields.input', [
            'name' => 'html_element_id',
            'label' => 'Element ID',
            'value' => $data['html_element_id'] ?? null
        ])

            @include('partials.fields.textarea', [
                'name' => 'html_class',
                'value' => $data['html_class'] ?? null
            ])
    </div>

    <div class="col-sm-6">
        @include('partials.fields.select', [
            'name' => 'size',
            'list' => function () use ($row_data) {
                $items = [];
                for($i = 1; $i <= $row_data['total_size']; $i++) {
                    $items[$i] = "{$i} / {$row_data['total_size']} Columns";
                }

                return $items;
            },
            'value' => $data['size'] ?? ''
        ])

        {!! Form::hidden('locked', 0) !!}
        @include('partials.fields.checkbox', [
            'label' => 'Locked',
            'wrapper' => true,
            'column' => 9,
            'list' => [
                [
                    'name' => 'locked',
                    'value' => 1,
                    'checked' => $data['locked'] ?? ''
                ],
            ],
        ])
    </div>
</div>

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'save' => true,
        'close' => true
    ]
])