@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Delete Row',
    'event' => 'row-deleted'
])

{!! Form::open([
    'url'    => '#',
    'method' => 'GET',
    'class'  => 'form-horizontal'
]) !!}

{!! Form::hidden('id', $id) !!}
{!! Form::hidden('is_item', strtolower($is_item) == 'true') !!}

<p>Are you sure you want to delete this.</p>

@include('partials.fields.button', [
    'icon'  => 'fa fa-trash',
    'class' => 'btn-danger',
    'value' => 'Delete',
    'type'  => 'submit'
])

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ]
])