@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Widget',
])


@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Widget Types'
])

@foreach($widgets as $widget)
      @include('partials.fields.button', [
        'url' => route('sites.{site}.templates.ajax-modal', [$site->id, $template->id, 'method' => 'widget', 'widget_id' => $widget->id, 'column_id' => $column_id]),
        'icon' => $widget->icon ?? 'fa fa-rocket',
        'value' => $widget->name,
        'attributes' => [
            'data-open' => 'modal',
            'data-target' => '#ajax-modal',
        ],
      ])
@endforeach


@include('partials.containers.portlet', ['part' => 'close'])

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'modal_width' => '900'
    ]
])