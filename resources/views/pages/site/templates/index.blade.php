@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => $site->name . ' Templates',
        'has_form' => true,
        'actions' => [
            [
                'icon' => 'fa fa-plus',
                'title' => 'Add New Template',
                'target' => route('sites.{site}.templates.create', $site->id)
            ],
            [
                'icon' => 'fa fa-cog',
                'target' => route('sites.{site}.settings.ajax-modal', [$site->id, 'method=showModal', 'type='.SiteSettingDefault::TYPE_TEMPLATES]),
                'attributes' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#ajax-modal',
                ]
            ],
        ]
    ])

    <div class="row">
        @include('partials.containers.nestable.nestable', [
            'id' => 'nestable',
            'can' => $can,
            'tree' => $templates_tree,
            'child' => ['children'],
            'no_nest' => true,
            'control' => [
                'url' => route('sites.{site}.templates.create', $site->id)
            ],
            'attributes' => [
                'data-max_levels' => 5,
                'data-protect_root' => false,
                'data-start_collapsed' => true,
                'data-type' => 'page'
            ],

            'link'=>
                function($item) use ($site) {
                   return "<a href='". route('sites.{site}.templates.show', [$site->id, $item->id]) . "'>". $item->name ."</a>";
                },

            'actions' => [
                function ($item) use ($site) {
                    $res = '<a href="' . route('sites.{site}.templates.show',[$site->id, $item->id]) . '"
                           title="Template builder"
                           class="tooltips"
                           ><span aria-hidden="true" class="fa fa-cogs" ></span></a>';

                    $res .= '<a href="' . route('sites.{site}.templates.ajax-modal', [$site->id, 'template_id='.$item->id, "method=pagesInfo"]) . '" target="_blank" title="Template Info"
                          class="tooltips" data-toggle="modal" data-target="#ajax-modal" ><span aria-hidden="true" class="fa fa-info-circle" ></span></a>';

                    return $res;
                },

                function ($item) use ($site, $can) {
                    if ($can['add']) {
                         $res = '<a href="' . route('sites.{site}.templates.create', [$site->id, 'parent_id' => $item->id]) . '"
                               title="Add template"
                               class="tooltips"
                            ><span aria-hidden="true" class="fa fa-plus" ></span></a>';

                         $res .= '<a data-target="sweetAlert"
                              data-alert_title="Duplicate Template"
                              data-alert_type="warning"
                              data-alert_href="' . route('sites.{site}.templates.ajax-alert', [$site->id, 'template_id' => $item->id, 'method' => 'duplicate']) . '"
                              title="Duplicate Template"
                              class="tooltips page_delete_link"
                           ><span aria-hidden="true" class="fa fa-files-o"></span></a>';

                         return $res;
                    }
                },
                function ($item) use ($site, $can) {
                    if ($can['edit']) {
                        $res = '<a href="' . route('sites.{site}.templates.edit', [$site->id, $item->id]) . '"
                               title="Edit template"
                               class="tooltips"
                              ><span aria-hidden="true"class="fa fa-pencil"></span></a>';

                        return $res;
                    }
                },
                function ($item) use ($site, $can) {
                    if ($can['delete']) {
                         $res = View::make('partials.containers.buttons.alert', [
                             'route' => route('sites.{site}.templates.ajax-alert', [
                                     $site->id,
                                     'template_id='. $item->id,
                                     'method=deleteTemplate']),
                             'title' => 'Delete template',
                             'icon'  => "icon-trash",
                             'text'  => '',
                             'type'  => 'warning'
                         ])->render();

                         return $res;
                    }
                },
                function ($item) {
                    if ($item->children->count() != 0) {
                        $res = '<span class="disclose fa fa-caret-down"></span>';

                        return $res;
                    }
                }
            ],
        ])
    </div>

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop