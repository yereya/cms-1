@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => "Template Pages Info",
])

@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Pages Info'
    ])

@include('partials.containers.data-table', [
    'data' => $pages_info,
    'attributes' => [
        'data-page-length' => '20'
    ],
    'columns' => [
       'name',
       'slug',
       'device',
       'type',
       [
          'label' => 'Status',
           'value' => function ($data) {
                 return $data['published_status'];
           }
       ],
       [
           'key' => 'labels',
           'value' => function ($data) {
                 return $data['labels'];
           }
       ],
       [
         'label' => 'actions',
         'value' => function ($data) {
                $res =  '<a href="' . $data['template_url'] . '"
                                 title="Template Builder"
                                 class="tooltips"
                                 > <span aria-hidden="true" class="fa fa-cogs" ></span> </a>';

                $res .= '<a href="' . $data['page_url'] . '" target="_blank" title="Page Link"
                        class="tooltips"> <span aria-hidden="true" class="icon-link" ></span> </a>';

                $res .= '<a href="' . $data['page_edit_url'] . '" target="_blank" title="Edit Post"
                        class="tooltips"> <span aria-hidden="true" class="icon-pencil" ></span> </a>';

                return $res;
          }
       ]
    ]
])

@include('partials.containers.portlet', [
    'part' => 'close'
])

@include('partials.containers.portlet', [
    'part' => 'open',
    'title' => 'Posts Info'
    ])

@include('partials.containers.data-table', [
    'data' => $posts_info,
    'attributes' => [
        'data-page-length' => '20'
    ],
    'columns' => [
       'name',
       'slug',
       'device',
       'type',
       [
          'label' => 'Content Type',
          'key' => 'content_type'
       ],
       [
          'label' => 'Status',
           'value' => function ($data) {
                 return $data['published_status'];
           }
       ],
       [
           'key' => 'labels',
           'value' => function ($data) {
                 return $data['labels'];
           }
       ],
       [
           'key' => 'actions',
           'value' => function ($data) {
                  $res =  '<a href="' . $data['template_url'] . '"
                                   title="Template Builder"
                                   class="tooltips"
                                   > <span aria-hidden="true" class="fa fa-cogs" ></span> </a>';

                  $res .= '<a href="' . $data['page_url'] . '" target="_blank" title="Page Link"
                          class="tooltips"> <span aria-hidden="true" class="icon-link" ></span> </a>';

                  $res .= '<a href="' . $data['post_url'] . '" target="_blank" title="Edit Post"
                          class="tooltips"> <span aria-hidden="true" class="icon-pencil" ></span> </a>';

              return $res;
            }
       ]

    ]
])


@include('partials.containers.portlet', [
    'part' => 'close'
])


@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ],
    'js' => [
        'datatable' => [
            "paging" => true
        ]
    ]
])
