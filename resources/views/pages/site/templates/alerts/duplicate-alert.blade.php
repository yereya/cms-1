{!!Form::open([
    'url' => $route,
    'class' => 'form-invisible'
])!!}

@if($is_sub_template)
    <p>Template has sub templates.
        <br>Do you want to duplicate All Template Tree or only this Template?
    </p>
    <button type="submit" class="btn btn-lg yellow-lemon">Only This</button>
    <button type="submit" name="all-tree" class="btn btn-lg red">All Tree</button>
@else
    <p>Duplicate template?</p>
    <button type="submit" class="btn btn-lg red">Duplicate</button>
@endif
<a class="btn btn-lg green" onclick="swal.close()">Cancel</a>
{!! Form::close() !!}
<style>
    .swal2-confirm {
        display: none;
    }
</style>
