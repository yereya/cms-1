@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Site Templates Form',
        'has_form' => true,
        'actions' => [
           isset($template) ?
            [
                'icon' => 'fa fa-cog',
                'target' => route('sites.{site}.settings.ajax-modal', [$site->id, 'method=showModal', 'type='.SiteSetting::getTemplateType($template->id)]),
                'attributes' => [
                    'data-toggle' => 'modal',
                    'data-target' => '#ajax-modal',
                ]
            ]: null,
        ]
    ])

    {!! Form::open([
        'route' => isset($template) ? ['sites.{site}.templates.update', $site->id, $template->id] : ['sites.{site}.templates.store', $site->id],
        'method' => isset($template) ? 'PUT' : 'POST',
        'class' => 'form-horizontal',
        'role' => 'form'
    ]) !!}

    <div class="form-body">
        @include('partials.fields.input', [
            'name' => 'name',
            'required' => 'true',
            'value' => isset($template) ? $template->name : null
        ])

        {!! Form::hidden('active', 0) !!}
        @include('partials.fields.checkbox', [
            'label' => 'Active',
            'wrapper' => true,
            'list' => [
                [
                    'name' => 'active',
                    'value' => 1,
                    'checked' => isset($template) ? $template->active : 1
                ],
            ],
        ])

        @include('partials.fields.select', [
            'name' => 'parent_id',
            'label' => 'Parent',
            'list' => ['Select a parent'] + $templates->pluck('name', 'id')->toArray(),
            'value' => isset($template) ? $template->parent_id : (isset($parent_id) ? $parent_id: null)
        ])
    </div>

    @include('partials.containers.form-footer-actions', [
        'buttons' => [
            'cancel' => [
                'route' => back_url(['site'=>$site->id])
            ],
            'submit' => 'true'
        ],
        'checkboxes' => [
            'create_another' => isset($template) ? null : 'true'
        ]
    ])

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@stop