<div class="row">
    <div class="col-xs-12 breadcrumbs-bar">
        <ul class="breadcrumbs-links">
            @foreach($template_bread_crumbs as $template_item)
                <li>
                    <a href="{{ route('sites.{site}.templates.show',[$site->id, $template_item->id]) }}" target="{{ $target ?? '_self' }}">{{ $template_item->name }}</a>
                    <i class="fa fa-angle-right"></i>
                </li>
            @endforeach
            <li>
                <span >{{ $template->name }}</span>
                <i class="fa fa-angle-right"></i>
            </li>
        </ul>
    </div>
</div>
