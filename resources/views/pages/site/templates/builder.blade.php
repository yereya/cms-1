@extends('layouts.base')

@section('content')
    <style id="inline-css">
        /* Generated Custom Inline CSS */
        <?php echo app(\App\Libraries\TemplateBuilder\TemplateScss::class,[$template->id,$site])->generate(true); ?>
    </style>
    <script>
        window.CMS = {
            site_id: '<?php echo $site->id; ?>',
            template_id: '<?php echo $template->id; ?>'
        };
    </script>
    <div id="builder">
        <div class="toolbar">
            <div class="row">
                <div class="col-sm-6">
                    @include('partials.fields.button',[
                        'type' => 'button',
                        'value' => '',
                        'class' => 'btn btn-primary tooltips',
                        'url' => route('sites.{site}.templates.ajax-modal', [$site->id, 'method' => 'row', 'template'=> $template->id]),
                        'icon' => 'fa fa-plus',
                        'attributes' => [
                            'data-toggle' => 'modal',
                            'data-target' => '#ajax-modal',
                            'title' => 'Add row',
                            'data-placement' => 'bottom',
                        ]
                    ])

                    <div class="btn-group">
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => 'SCSS',
                            'class' => 'btn btn-default btn-sm',
                            'icon' => 'fa fa-css3',
                            'url' => route('sites.{site}.templates.ajax-modal', [$site->id, 'method' => 'customScss', 'template'=> $template->id]),
                            'attributes' => [
                                'data-toggle' => 'modal',
                                'data-target' => '#ajax-modal',
                                'title' => 'Custom SCSS',
                                'data-placement' => 'bottom',
                            ]
                        ])
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => 'HTML',
                            'class' => 'btn btn-default btn-sm',
                            'icon' => 'fa fa-html5',
                            'url' => route('sites.{site}.templates.ajax-modal', [$site->id, 'method' => 'customHtml', 'template'=> $template->id]),
                            'attributes' => [
                                'data-toggle' => 'modal',
                                'data-target' => '#ajax-modal',
                                'title' => 'Custom HTML',
                                'data-placement' => 'bottom',
                            ]
                        ])
                    </div>

                    <div class="btn-group">
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => 'Recompile SCSS',
                            'class' => 'btn btn-default btn-sm',
                            'icon' => 'fa fa-cogs',
                            'url' => route('sites.{site}.scss-compile', [$site->id]),
                            'attributes' => [
                                'onClick' => 'BuilderAPI.recompileScss(event, this)',
                            ]
                        ])
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => 'Upload CSS',
                            'class' => 'btn btn-default btn-sm tooltips',
                            'icon' => 'fa fa-bomb',
                            'url' => route('sites.{site}.css-upload', [$site->id]),
                            'attributes' => [
                                'onClick' => 'BuilderAPI.uploadCss(event, this)',
                                'title' => 'Upload CSS file into ' . $site->name,
                                'data-placement' => 'bottom'
                            ]
                        ])
                    </div>
                </div>

                <div class="col-sm-6 text-right">
                    <div class="btn-group">
                        <label for="dynamic_view">Dynamic View</label>
                        <input type="checkbox" id="dynamic_view" checked />
                    </div>
                    <div class="btn-group">
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => 'Compact View',
                            'class' => 'btn btn-default btn-sm toggle-compact-view',
                            'icon' => 'fa fa-eye'
                        ])
                    </div>

                    <div class="btn-group breakpoints">
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => '',
                            'class' => 'btn btn-default active',
                            'icon' => 'fa fa-desktop',
                            'attributes' => [
                                'title' => 'Desktop',
                                'data-size' => "full",
                            ]
                        ])
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => '',
                            'class' => 'btn btn-default',
                            'icon' => 'fa fa-tablet',
                            'attributes' => [
                                'title' => 'Tablet Horizontal',
                                'data-size' => "tablet-horizontal",
                            ]
                        ])
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => '',
                            'class' => 'btn btn-default',
                            'icon' => 'fa fa-tablet',
                            'attributes' => [
                                'title' => 'Tablet Vertical',
                                'data-size' => "tablet-vertical",
                            ]
                        ])
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => '',
                            'class' => 'btn btn-default',
                            'icon' => 'fa fa-mobile',
                            'attributes' => [
                                'title' => 'Mobile Horizontal',
                                'data-size' => "mobile-horizontal",
                            ]
                        ])
                        @include('partials.fields.button',[
                            'type' => 'button',
                            'value' => '',
                            'class' => 'btn btn-default',
                            'icon' => 'fa fa-mobile',
                            'attributes' => [
                                'title' => 'Mobile Vertical',
                                'data-size' => "mobile-vertical",
                            ]
                        ])
                    </div>

                    <div class="btn-group">
                        @include('partials.fields.button',[
                            'value' => '',
                            'class' => 'btn btn-success save tooltips',
                            'icon' => 'fa fa-floppy-o',
                            'attributes' => [
                                'title' => 'Save',
                                'data-placement' => 'bottom',
                            ]
                        ])
                    </div>
                </div>
            </div>

            <div class="hierarchy"></div>
        </div>

        @include('pages.site.templates.breadcrumbs', [
            'target' => '_top'
        ])

        <div class="canvas {{$template_classes}}">
            <div class="editor"></div>

            @include('partials.ui-features.ruler')
            @include('partials.ui-features.scrollToTop')
        </div>

        {!! $template->custom_html_head !!}
        {!! $template->custom_html_body !!}
    </div>

    <ul id="contextMenu" class="dropdown-menu" role="menu" style="display:none; position: absolute; z-index: 9999999;"></ul>
@stop

@section('scripts')
    <script type="text/javascript">
        App.csrf = "{{ csrf_token() }}";

        function rowFormSuccess(response) {
            $('.editor').trigger('row-changed', response);
        }

        function rowFormError(response) {
            $('.editor').trigger('modal-error', response);
        }

        function columnFormSuccess(response) {
            $('.editor').trigger('column-changed', response);
        }

        function columnFormError(response) {
            $('.editor').trigger('modal-error', response);
        }
    </script>

    <script type="text/javascript">
        var routes = {
            getBuilderItems: '{{ route('sites.{site}.templates.{template}.builder-items', [$site->id, $template->id]) }}',
            storeBuilderItems: '{{ route('sites.{site}.templates.{template}.builder-items', [$site->id, $template->id]) }}',
            getWidgetData: '{{ route('sites.{site}.widgets.{widget}.widget-data.show', [$site->id, ':widgetId', ':widgetDataId']) }}',
            storeWidgetData: '{{ route('sites.{site}.widgets.{widget}.widget-data.store', [$site->id, ':widgetId']) }}',
            updateWidgetData: '{{ route('sites.{site}.widgets.{widget}.widget-data.update', [$site->id, ':widgetId', ':widgetDataId']) }}',
            widgetDataCtrl: '{{ route('sites.{site}.widgets.ajax-modal', [$site->id,'template' => $template->id]) }}',
            templateCtrl: '{{ route('sites.{site}.templates.ajax-modal', [$site->id,'template' => $template->id]) }}'
        };
    </script>

    <script id="hierarchy-template" type="text/x-handlebars-template">
        @{{#if element}}
        <span class="close-hierarchy">
                <i class="fa fa-times-circle" aria-hidden="true"></i> Close
            </span>

        <div class="info">
            <table class="table">
                <tr>
                    <th>ID:</th>
                    <td>@{{ id }}</td>
                </tr><tr>
                    <th>Type</th>
                    <td>@{{ type }}</td>
                </tr>

                @{{#equals type 'widget'}}
                <tr>
                    <th>Name</th>
                    <td>@{{ element.name }}</td>
                </tr><tr>
                    <th>Active</th>
                    <td>@{{ element.active }}</td>
                </tr><tr>
                    <th>Class</th>
                    <td>@{{ element.html_class }}</td>
                </tr>
                @{{/equals}}

                @{{#equals type 'column'}}
                <tr>
                    <th>Size</th>
                    <td>@{{ element.size }}</td>
                </tr><tr>
                    <th>Element ID</th>
                    <td>@{{ element.html_element_id }}</td>
                </tr><tr>
                    <th>Class</th>
                    <td>@{{ element.html_class }}</td>
                </tr><tr>
                    <th>Locked</th>
                    <td>@{{ element.locked }}</td>
                </tr>
                @{{/equals}}

                @{{#equals type 'row'}}
                <tr>
                    <th>Element ID</th>
                    <td>@{{ element.html_element_id }}</td>
                </tr><tr>
                    <th>Class</th>
                    <td>@{{ element.html_class }}</td>
                </tr><tr>
                    <th>Container Class</th>
                    <td>@{{ element.html_container_class }}</td>
                </tr><tr>
                    <th>Wrapper Class</th>
                    <td>@{{ element.html_wrapper_class }}</td>
                </tr><tr>
                    <th>Total Size</th>
                    <td>@{{ element.total_size }}</td>
                </tr><tr>
                    <th>Locked</th>
                    <td>@{{ element.locked }}</td>
                </tr><tr>
                    <th>Active</th>
                    <td>@{{ element.active }}</td>
                </tr>
                @{{/equals}}
            </table>
        </div>

        <ul>
            @{{#each elements}}
            @{{> hierarchyElement }}
            @{{/each}}
        </ul>
        @{{/if}}
    </script>

    <script id="hierarchy-element-template" type="text/x-handlebars-template">
        @{{#unless deleted}}
        <li class="element" data-type="row"
            data-id="@{{ id }}"
            data-is-item="@{{ isItem }}"
            data-data="@{{ escapeHTML data }}">
            <span class="title">Row[@{{ id }}] - @{{ html_container_class }} @{{#if locked}}<i class="fa fa-lock" aria-hidden="true"></i>@{{/if}}</span>

            @{{#if sortedColumns}}
            <ul>
                @{{#each sortedColumns}}
                @{{#unless deleted}}
                <li class="element" data-type="column"
                    data-id="@{{ id }}"
                    data-data="@{{ escapeHTML data }}"
                    data-row-data="@{{ escapeHTML row_data }}">
                    <span class="title">Column[@{{ id }}] - @{{ size }} @{{#if locked}}<i class="fa fa-lock" aria-hidden="true"></i>@{{/if}}</span>

                    @{{#if sortedItems}}
                    <ul>
                        @{{#each sortedItems}}
                        @{{#equals element_type 'widget' }}
                        <li class="element" data-type="widget" data-widget-id="@{{#with (lookup @root.widgetsData element_id) }}@{{ widget_id }}@{{/with}}" data-id="@{{ id }}" data-data-id="@{{ element_id }}" data-column-id="@{{ column_id }}">
                            <span class="title">Widget Data[@{{ element_id }}] - @{{#with (lookup @root.widgetsData element_id) }} @{{ name }} @{{/with}}</span>
                        </li>
                        @{{/equals}}

                        @{{#equals element_type 'row'}}
                        @{{> hierarchyElement instance }}
                        @{{/equals}}
                        @{{/each}}
                    </ul>
                    @{{/if}}
                </li>
                @{{/unless}}
                @{{/each}}
            </ul>
            @{{/if}}
        </li>
        @{{/unless}}
    </script>

    <script id="row-template" type="text/x-handlebars-template">
        <div class="builder-row" data-type="row"
             data-id="@{{ item.id }}"
             data-is-item="@{{ isItem }}"

             data-template-id="@{{ template_id }}"

             data-locked="@{{ locked }}"
             data-inherited="@{{ item.inherited }}"
             data-created="@{{ item.created }}"

             data-order="@{{ item.order }}"
             data-data="@{{{ escapeHTML data }}}">

            <div class="actions">
                @{{#unless locked}}
                <span class="action tooltips move" data-original-title="Move">
                        Row <i class="fa fa-arrows" aria-hidden="true"></i>
                    </span>
                @{{else}}
                <span class="">
                        Row <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
                @{{/unless}}
            </div>

            @{{#unless html}}
            <div class="empty">Empty Row...</div>
            @{{/unless}}

            @{{#if html_wrapper_class}}
            <div class="@{{ html_wrapper_class }}">
                @{{/if}}

                @{{#if html_container_class}}
                <div class="@{{ html_container_class }}">
                    @{{/if}}

                    <div class="@{{ html_class }}" @{{#if html_element_id}} id="@{{ html_element_id }}" @{{/if}}>
                        @{{{ html }}}
                    </div>

                    @{{#if html_container_class}}
                </div>
                @{{/if}}

                @{{#if html_wrapper_class}}
            </div>
            @{{/if}}
        </div>


    </script>

    <script id="column-template" type="text/x-handlebars-template">
        <div class="builder-column col-md-@{{ size }} @{{ html_class }}" data-type="column"
             @{{#if html_element_id}} id="@{{ html_element_id }}" @{{/if}}

             data-id="@{{ id }}"
             data-row-id="@{{ row_id }}"
             data-template-id="@{{ template_id }}"

             data-inherited="@{{ inherited }}"
             data-created="@{{ created }}"

             data-order="@{{ order }}"
             data-data="@{{ escapeHTML data }}"
             data-row-data="@{{ escapeHTML row_data }}">

            <div class="actions">
                <span class="">
                    Column - @{{ size }} @{{#if locked}} <i class="fa fa-lock" aria-hidden="true"></i> @{{/if}}
                </span>
            </div>

            @{{#unless html}}
            <div class="empty">Empty Column...</div>
            @{{/unless}}

            @{{{ html }}}
        </div>
    </script>

    <script id="widget-template" type="text/x-handlebars-template">
        <div class="builder-widget
        @{{#unless item.show_on_desktop }}  hidden-lg hidden-md @{{/unless}}
        @{{#unless item.show_on_mobile }} hidden-xs @{{/unless}}
        @{{#unless item.show_on_tablet }}  hidden-sm @{{/unless}}"
             data-type="widget" data-is-item="true"
             data-id="@{{ item.id }}"
             data-data-id="@{{ id }}"
             data-row-id="@{{ row.id }}"
             data-column-id="@{{ column.id }}"
             data-widget-id="@{{ widget_id }}"
             data-widget-template-id="@{{ widget_template_id }}"
             data-template-id="@{{ item.template_id }}"

             data-is_live="@{{ item.is_live }}"

             data-show_on_desktop="@{{ item.show_on_desktop }}"
             data-show_on_tablet="@{{ item.show_on_tablet }}"
             data-show_on_mobile="@{{ item.show_on_mobile }}"

             data-inherited="@{{ item.inherited }}"
             data-created="@{{ item.created }}"
             data-locked="@{{ item.locked }}"

             data-order="@{{ item.order }}">

            @{{#if item.locked}}
            <div class="lock">
                    <span class="tooltips" data-original-title="Locked">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
            </div>
            @{{else}}
            <div class="actions">
                    <span class="action tooltips move" data-original-title="Move">
                        Widget <i class="fa fa-arrows" aria-hidden="true"></i>
                    </span>
            </div>
            @{{/if}}

            <div class="data-html" data-widget-name="@{{ name }}">
                @{{{ html }}}
            </div>
        </div>
    </script>

    <script id="context-menu-template" type="text/x-handlebars-template">
        @{{#each menuItems}}
        @{{#if devider}}
        <li class="divider"></li>
        @{{else}}
        <li class="@{{ disabled }}">
            @{{#unless disabled}}
            <a tabindex="-1"
               href="@{{ modal_url }}"
               data-toggle="modal"
               data-target="#ajax-modal"
               data-original-title="@{{ title }}">@{{{ title }}}</a>
            @{{else}}

            <a href="javascript:void(0)">@{{{ title }}}</a>
            @{{/unless}}
        </li>
        @{{/if}}
        @{{/each}}
    </script>
@stop

@include('layouts.metronic.ajax-modal')