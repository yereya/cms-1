@extends('layouts.metronic.main')

@section('page_content')
    <div class="row">
        <div class="col-md-12">

            @include('partials.containers.portlet', [
                'part' => 'open',
                'title' => 'Account: '. isset($account->account_name) ? $account->account_name : '',
                'title_icon' => 'icon-notebook',
                'tabs' => [
                    [
                        'id' => 'campaigns',
                        'label' => 'Campaigns',
                    ],
                    [
                        'id' => 'ad_group',
                        'label' => 'Ad Group',
                    ],
                    [
                        'id' => 'keyword',
                        'label' => 'Keywords',
                    ],
                    [
                        'id' => 'scheduling',
                        'label' => 'Scheduling',
                    ],
                ],
            ])


            {!! Form::open([
                'route' => isset($change_item) ? ['reports.publishers.{publisher}.accounts.{account_id}.change.update', $account->publisher, $account->account_id, $change_item->id, '#campaign'] : ['reports.publishers.{publisher}.accounts.{account_id}.change.store', $account->publisher, $account->account_id, '#campaign'],
                'method' => isset($change_item) ? 'PUT' : 'POST',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]) !!}
            {!! Form::hidden('account_id', isset($account->account_id) ? $account->account_id : '') !!}
            {!! Form::hidden('change_level', 'campaign') !!}


            @include('partials.fields.select', [
                    'label' => 'Campaign',
                    'name' => 'campaign_id',
                    'list' => $campaigns_list,
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->campaign_id) ? $change_item->campaign_id : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Action',
                    'name' => 'comment',
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->comment) ? $change_item->comment : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Reason',
                    'name' => 'reason',
                    'value' => isset($change_item) && isset($change_item->reason) ? $change_item->reason : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Expected Results',
                    'name' => 'expected_results',
                    'value' => isset($change_item) && isset($change_item->expected_results) ? $change_item->expected_results : null
                ])

            @include('partials.fields.input-timepicker', [
                    'label' => 'Follow Up Date',
                    'name' => 'follow_up_date',
                    'datetime' => true,
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->follow_up_date) ? $change_item->follow_up_date : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Budget',
                    'name' => 'budget',
                    'type' => 'number',
                    'help_text' => 'Set the budget for this campaign.',
                    'value' => isset($change_item) && isset($change_item->budget) ? $change_item->budget : null
                ])

            @include('partials.fields.input', [
                    'label'     => 'Mobile Bid Adjustment',
                    'name'      => 'mobile_bid_adjustment',
                    'help_text' => 'Mobile Bid Adjustment raise or lower in percents.',
                    'value'     => isset($change_item) && isset($change_item->mobile_bid_adjustment) ? decimalFractionToPercent($change_item->mobile_bid_adjustment) : null
                ])

            @include('partials.fields.input-timepicker', [
                    'label' => 'Change Date',
                    'name' => 'date_time_changed',
                    'datetime' => true,
                    'value' => isset($change_item) && isset($change_item->date_time_changed) ? $change_item->date_time_changed : date('Y-m-d G:i')
                ])

            @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => true
                ],
                'checkboxes' => [
                    'create_another' => isset($change_item) ? null : 'true'
                ]
            ])


            {!! Form::close() !!}


            {{-- AD GROUP --}}
            @include('partials.containers.portlet', [
                'part' => 'nextTab',
                'tab_id' => 'ad_group'
            ])

            {!! Form::open([
                'route' => isset($change_item) ? ['reports.publishers.{publisher}.accounts.{account_id}.change.update', $account->publisher, $account->account_id, $change_item->id, '#ad_group'] : ['reports.publishers.{publisher}.accounts.{account_id}.change.store', $account->publisher, $account->account_id, '#ad_group'],
                'method' => isset($change_item) ? 'PUT' : 'POST',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]) !!}
            {!! Form::hidden('account_id', $account->account_id) !!}
            {!! Form::hidden('change_level', 'ad_group') !!}


            @include('partials.fields.select', [
                    'label' => 'Campaign',
                    'name' => 'campaign_id',
                    'id' => 'select_ad_group_campaign_id',
                    'list' => $campaigns_list,
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->campaign_id) ? $change_item->campaign_id : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Action',
                    'name' => 'comment',
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->comment) ? $change_item->comment : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Reason',
                    'name' => 'reason',
                    'value' => isset($change_item) && isset($change_item->reason) ? $change_item->reason : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Expected Results',
                    'name' => 'expected_results',
                    'value' => isset($change_item) && isset($change_item->expected_results) ? $change_item->expected_results : null
                ])

            @include('partials.fields.input-timepicker', [
                    'label' => 'Follow Up Date',
                    'name' => 'follow_up_date',
                    'datetime' => true,
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->follow_up_date) ? $change_item->follow_up_date : null
                ])

            @include('partials.fields.select', [
                    'label' => 'Ad Group',
                    'name' => 'ad_group_id',
                    'ajax' => true,
                    'attributes' => [
                        'data-search_field' => 'ad_group',
                        'data-min_input' => 0,
                        'data-dependency_selector' => '#select_ad_group_campaign_id',
                        'data-where_account_id' => $account->account_id,
                        'data-api_url' => route('reports.publishers.{publisher}.keywords.select2', [$account->publisher], false),
                        'data-value' => isset($change_item) && isset($change_item->ad_group_id) ? $change_item->ad_group_id : null,
                    ],
                    'required' => true,
                ])

            @include('partials.fields.select', [
                    'label' => 'State',
                    'name' => 'state',
                    'list' => $state_types,
                    'value' => isset($change_item) && isset($change_item->state) ? $change_item->state : null
                ])

            @include('partials.fields.input', [
                    'label' => 'New Bid',
                    'name' => 'bid',
                    'help_text' => 'New bid in the accounts currency (only numbers).',
                    'value' => isset($change_item) && isset($change_item->bid) ? $change_item->bid : null
                ])

            @include('partials.fields.input', [
                    'label'     => 'Mobile Bid Adjustment',
                    'name'      => 'mobile_bid_adjustment',
                    'help_text' => 'Mobile Bid Adjustment raise or lower in percents.',
                    'value'     => isset($change_item) && isset($change_item->mobile_bid_adjustment) ? decimalFractionToPercent($change_item->mobile_bid_adjustment) : null
                ])

            @include('partials.fields.input-timepicker', [
                    'label' => 'Change Date',
                    'name' => 'date_time_changed',
                    'datetime' => true,
                    'value' => isset($change_item) && isset($change_item->date_time_changed) ? $change_item->date_time_changed : date('Y-m-d G:i')
                ])

            @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => true
                ],
                'checkboxes' => [
                    'create_another' => isset($change_item) ? null : 'true'
                ]
            ])

            {!! Form::close() !!}
            {{-- END AD GROUP --}}


            {{-- KEYWORD --}}
            @include('partials.containers.portlet', [
                'part' => 'nextTab',
                'tab_id' => 'keyword'
            ])

            {!! Form::open([
                'route' => isset($change_item) ? ['reports.publishers.{publisher}.accounts.{account_id}.change.update', $account->publisher, $account->account_id, $change_item->id, '#keyword'] : ['reports.publishers.{publisher}.accounts.{account_id}.change.store', $account->publisher, $account->account_id, '#keyword'],
                'method' => isset($change_item) ? 'PUT' : 'POST',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]) !!}
            {!! Form::hidden('account_id', $account->account_id) !!}
            {!! Form::hidden('change_level', 'keyword') !!}


            @include('partials.fields.select', [
                    'label' => 'Campaign',
                    'name' => 'campaign_id',
                    'id' => 'select_keyword_campaign_id',
                    'list' => $campaigns_list,
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->campaign_id) ? $change_item->campaign_id : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Action',
                    'name' => 'comment',
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->comment) ? $change_item->comment : null,
                ])

            @include('partials.fields.input', [
                    'label' => 'Reason',
                    'name' => 'reason',
                    'value' => isset($change_item) && isset($change_item->reason) ? $change_item->reason : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Expected Results',
                    'name' => 'expected_results',
                    'value' => isset($change_item) && isset($change_item->expected_results) ? $change_item->expected_results : null
                ])

            @include('partials.fields.input-timepicker', [
                    'label' => 'Follow Up Date',
                    'name' => 'follow_up_date',
                    'datetime' => true,
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->follow_up_date) ? $change_item->follow_up_date : null
                ])


            @include('partials.fields.select', [
                    'label' => 'Ad Group',
                    'name' => 'ad_group_id',
                    'id' => 'select_keyword_ad_group_id',
                    'ajax' => true,
                    'attributes' => [
                        'data-search_field' => 'ad_group',
                        'data-min_input' => 0,
                        'data-dependency_selector' => '#select_keyword_campaign_id',
                        'data-where_account_id' => $account->account_id,
                        'data-api_url' => route('reports.publishers.{publisher}.keywords.select2', [$account->publisher], false),
                        'data-value' => isset($change_item) && isset($change_item->ad_group_id) ? $change_item->ad_group_id : null,
                    ],
                    'required' => true
                ])

            @include('partials.fields.select', [
                    'label' => 'Keyword',
                    'name' => 'keyword_id',
                    'ajax' => true,
                    'attributes' => [
                        'data-search_field' => 'keyword',
                        'data-dependency_selector' => '#select_keyword_ad_group_id',
                        'data-where_account_id' => $account->account_id,
                        'data-api_url' => route('reports.publishers.{publisher}.keywords.select2', [$account->publisher], false),
                        'data-value' => isset($change_item) && isset($change_item->keyword_id) ? $change_item->keyword_id : null,
                    ]
                ])

            @include('partials.fields.select', [
                    'label' => 'State',
                    'name' => 'keyword_state',
                    'list' => $state_types,
                    'value' => isset($change_item) && isset($change_item->state) ? $change_item->state : null
                ])

            @include('partials.fields.input', [
                    'label' => 'New Bid',
                    'name' => 'bid',
                    'help_text' => 'New bid in the accounts currency (only numbers).',
                    'value' => isset($change_item) && isset($change_item->bid) ? $change_item->bid : null
                ])


            @include('partials.fields.input-timepicker', [
                    'label' => 'Change Date',
                    'name' => 'date_time_changed',
                    'datetime' => true,
                    'value' => isset($change_item) && isset($change_item->date_time_changed) ? $change_item->date_time_changed : date('Y-m-d G:i')
                ])


            @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => true
                ],
                'checkboxes' => [
                    'create_another' => isset($change_item) ? null : 'true'
                ]
            ])


            {!! Form::close() !!}
            {{-- END KEYWORD --}}


            {{-- SCHEDULING --}}
            @include('partials.containers.portlet', [
                'part' => 'nextTab',
                'tab_id' => 'scheduling'
            ])

            {!! Form::open([
                'route' => isset($change_item) ? ['reports.publishers.{publisher}.accounts.{account_id}.change.update', $account->publisher, $account->account_id, $change_item->id, '#scheduling'] : ['reports.publishers.{publisher}.accounts.{account_id}.change.store', $account->publisher, $account->account_id, '#scheduling'],
                'method' => isset($change_item) ? 'PUT' : 'POST',
                'class' => 'form-horizontal',
                'role' => 'form'
            ]) !!}
            {!! Form::hidden('account_id', $account->account_id) !!}
            {!! Form::hidden('change_level', 'scheduling') !!}


            @include('partials.fields.select', [
                    'label' => 'Campaign',
                    'name' => 'campaign_id',
                    'list' => $campaigns_list,
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->campaign_id) ? $change_item->campaign_id : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Action',
                    'name' => 'comment',
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->comment) ? $change_item->comment : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Reason',
                    'name' => 'reason',
                    'value' => isset($change_item) && isset($change_item->reason) ? $change_item->reason : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Expected Results',
                    'name' => 'expected_results',
                    'value' => isset($change_item) && isset($change_item->expected_results) ? $change_item->expected_results : null
                ])

            @include('partials.fields.input-timepicker', [
                    'label' => 'Follow Up Date',
                    'name' => 'follow_up_date',
                    'datetime' => true,
                    'required' => true,
                    'value' => isset($change_item) && isset($change_item->follow_up_date) ? $change_item->follow_up_date : null
                ])

            @include('partials.fields.select', [
                    'label' => 'Days',
                    'name' => 'scheduling_days',
                    'multi_select' => true,
                    'list' => $week_days_list,
                    'value' => isset($change_item) && isset($change_item->days) ? $change_item->days : null
                ])

            @include('partials.fields.input-timepicker', [
                    'label' => 'From Time',
                    'time' => true,
                    'name' => 'from_time',
                    'value' => isset($change_item) && isset($change_item->time_from) ? $change_item->time_from : null
                ])

            @include('partials.fields.input-timepicker', [
                    'label' => 'Until Time',
                    'time' => true,
                    'name' => 'until_time',
                    'value' => isset($change_item) && isset($change_item->time_until) ? $change_item->time_until : null
                ])

            @include('partials.fields.select', [
                    'label' => 'State',
                    'name' => 'state',
                    'list' => $state_types,
                    'value' => isset($change_item) && isset($change_item->state) ? $change_item->state : null
                ])

            @include('partials.fields.input', [
                    'label' => 'Bid Multiplier',
                    'name' => 'bid_multiplier',
                    'help_text' => 'Bid raise or lower in percents 1 is 100%.',
                    'value' => isset($change_item) && isset($change_item->bid) ? $change_item->bid : null
                ])


            @include('partials.fields.input-timepicker', [
                    'label' => 'Change Date',
                    'name' => 'date_time_changed',
                    'datetime' => true,
                    'value' => isset($change_item) && isset($change_item->date_time_changed) ? $change_item->date_time_changed : date('Y-m-d G:i')
                ])


            @include('partials.containers.form-footer-actions', [
                'buttons' => [
                    'cancel' => [
                        'route' => back_url()
                    ],
                    'submit' => true
                ],
                'checkboxes' => [
                    'create_another' => isset($change_item) ? null : 'true'
                ]
            ])


            {!! Form::close() !!}
            {{-- END SCHEDULING --}}


            @include('partials.containers.portlet', [
                'part' => 'closeTabs'
            ])
        </div>
    </div>
@stop
