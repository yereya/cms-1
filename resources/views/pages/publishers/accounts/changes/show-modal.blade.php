@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => 'Change details',
])


<div class="row">
    <div class="col-md-6">
        <table class="table">
            <tbody>
            <tr>
                <td style="width: 30%;">Change ID</td>
                <td>{{ $changes['change']['id'] }} </td>
                <td></td>
            </tr>
            <tr>
                <td>Change Level</td>
                <td>{{ $changes['change']['change_level'] or '' }} </td>
                <td></td>
            </tr>
            @if($changes['change']['bid'])
                <tr>
                    <td>New Bid</td>
                    <td>
                        {{ $changes['change']['bid'] }}
                        @foreach($changes['change']['bid'] as $history_change)
                            @if (isset($history_change['bid']))
                                <span class="extra-info">
                                    <b>{{ $history_change['bid'] }}</b> - {{ sqlDateFormat($history_change['date_time_changed'], false) }}
                                </span>
                            @endif
                        @endforeach
                    </td>
                    <td></td>
                </tr>
            @endif
            @if($changes['change']['bid_multiplier'])
                <tr>
                    <td>Bid Multiplier</td>
                    <td>
                        {{ $changes['change']['bid_multiplier'] }}
                        @foreach($changes['change']['history'] as $history_change)
                            @if (isset($history_change['bid_multiplier']))
                                <span class="extra-info">
                                    <b>{{ $history_change['bid_multiplier'] }}</b> - {{ sqlDateFormat($history_change['date_time_changed'], false) }}
                                </span>
                            @endif
                        @endforeach
                    </td>
                    <td></td>
                </tr>
            @endif
            @if($changes['change']['mobile_bid_adjustment'])
                <tr>
                    <td>Mobile Bid Adjustment</td>
                    <td>
                        {{ $changes['change']['mobile_bid_adjustment'] }}
                        @foreach($changes['change']['history'] as $history_change)
                            @if (isset($history_change['mobile_bid_adjustment']))
                                <span class="extra-info">
                                    <b>{{ $history_change['mobile_bid_adjustment'] }}</b> - {{ sqlDateFormat($history_change['date_time_changed'], false) }}
                                </span>
                            @endif
                        @endforeach
                    </td>
                    <td></td>
                </tr>
            @endif
            @if(isset($changes['days']) && count($changes['days']))
                <tr>
                    <td>Days</td>
                    <td>
                        @foreach($changes['days'] as $day)
                            <span class="badge badge-default">{{ $day['day'] }}</span>
                        @endforeach
                    </td>
                    <td></td>
                </tr>
            @endif
            @if($changes['change']['state'])
                <tr>
                    <td>State Change</td>
                    <td>{{ $changes['change']['state'] }}</td>
                    <td></td>
                </tr>
            @endif
            @if($changes['change']['time_from'] || $changes['change']['time_from'])
                <tr>
                    <td>State Change</td>
                    <td>{{ $changes['change']['state'] }} - {{ $changes['change']['time_from'] }}</td>
                    <td></td>
                </tr>
            @endif
            @if($changes['change']['comment'])
            <tr>
                <td>Action</td>
                <td>{{ $changes['change']['comment'] }}</td>
                <td></td>
            </tr>
            @endif
            @if($changes['change']['reason'])
            <tr>
                <td>Reason</td>
                <td>{{ $changes['change']['reason'] }}</td>
                <td></td>
            </tr>
            @endif
            @if($changes['change']['budget'])
                <tr>
                    <td>Budget</td>
                    <td>
                        {{ $changes['change']['budget'] }}
                        @foreach($changes['change']['history'] as $history_change)
                            @if (isset($history_change['budget']))
                                <span class="extra-info">
                                    <b>{{ $history_change['budget'] }}</b> - {{ sqlDateFormat($history_change['date_time_changed'], false) }}
                                </span>
                            @endif
                        @endforeach
                    </td>
                    <td></td>
                </tr>
            @endif
            @if($changes['change']['expected_results'])
            <tr>
                <td>Expected Results</td>
                <td>{{ $changes['change']['expected_results'] }}</td>
                <td></td>
            </tr>
            @endif

            </tbody>
        </table>
    </div>
    <div class="col-md-6">
        <table class="table">
            <tbody>
                <tr>
                    <td>Change By</td>
                    <td>{{ $changes['user']['last_name'] .' '. $changes['user']['first_name'] .' ['. $changes['user']['first_name'] .']' }} </td>
                    <td></td>
                </tr>
                @if($changes['account'])
                    <tr>
                        <td>Account Name</td>
                        <td>{{ $changes['account']['account_name'] or '' }} </td>
                        <td></td>
                    </tr>
                @endif
                @if(isset($changes['advertiser']['name']))
                    <tr>
                        <td>Advertiser</td>
                        <td>{{ $changes['advertiser']['name'] or '' }} </td>
                        <td></td>
                    </tr>
                @endif
                @if(isset($changes['advertiser']['name']))
                    <tr>
                        <td>Account Advertiser</td>
                        <td>{{ $changes['advertiser']['name'] or '' }} </td>
                        <td></td>
                    </tr>
                @endif
                @if(isset($changes['campaign']['campaign']))
                    <tr>
                        <td>Campaign</td>
                        <td>{{ $changes['campaign']['campaign'] or '' }} </td>
                        <td></td>
                    </tr>
                @endif
                @if(isset($changes['ad_group']['ad_group']))
                    <tr>
                        <td>Ad Group</td>
                        <td>{{ $changes['ad_group']['ad_group'] or '' }} </td>
                        <td></td>
                    </tr>
                @endif
                @if(isset($changes['keyword']['keyword']))
                    <tr>
                        <td>Keyword</td>
                        <td>{{ $changes['keyword']['keyword'] or '' }} </td>
                        <td></td>
                    </tr>
                @endif
                @if($changes['change']['follow_up_date'] != '0000-00-00 00:00:00')
                    <tr>
                        <td>Follow Up Date</td>
                        <td>{{ $changes['change']['follow_up_date'] }}</td>
                        <td><a href="javascript:void(0);"
                               title="Add or remove a flag for follow up"
                               data-property_toggle="{!! route('reports.publishers.{publisher}.accounts.{account_id}.change.{change_id}.toggle-property', [
                                    $publisher,
                                    $account_id,
                                    $changes['change']['id'],
                                    'action=follow_up_state'. '&change_id='. $changes['change']['id']
                               ]) !!}">
                                <span class='toggle_checkbox' data-toggle_state='{{ $changes['change']['follow_up_state'] ? 'checked' : '' }}'> </span></a></td>
                    </tr>
                @endif
                @if($changes['change']['date_time_changed'])
                <tr>
                    <td>Change Date</td>
                    <td>{{ $changes['change']['date_time_changed'] }}</td>
                    <td></td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => [
        'close' => true
    ]
])
