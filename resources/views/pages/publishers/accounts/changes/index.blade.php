@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Account: '. $account->account_name .' ['. $account->account_id .']',
        'tabs' => [
            [
                'id' => 'campaign',
                'label' => 'Campaigns',
            ],
            [
                'id' => 'ad_group',
                'label' => 'Ad Group',
            ],
            [
                'id' => 'keyword',
                'label' => 'Keywords',
            ],
            [
                'id' => 'scheduling',
                'label' => 'Scheduling',
            ],
        ],
    ])

    <div class="actions text-right">
        <a class="btn btn-circle btn-icon btn-default" title="Add new" href="{{ route('reports.publishers.{publisher}.accounts.{account_id}.change.create', [$account->publisher, $account->account_id, '#campaign']) }}">
            <i class="icon-plus"></i> Add New</a>
    </div>


    @include('partials.containers.data-table', [
        'data' => $logs_campaigns,
        'name' => 'campaigns_log',
        'columns' => [
            [
                'key' => 'id',
                'label' => 'ID',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->id;
                }
            ],
            [
                'key' => 'user',
                'label' => 'By',
                'value' => function ($data) {
                    return $data->first_name .' '. $data->last_name;
                }
            ],
            [
                'key' => 'campaign',
                'class' => 'center',
                'filter_type' => 'select',
                'value' => function ($data) {
                    return $data->campaign;
                }
            ],
            [
                'key' => 'comment',
                'label' => 'Action',
                'class' => 'center',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->comment;
                }
            ],
            [
                'key' => 'budget',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->budget;
                }
            ],
            [
                'key' => 'Mobile Bid Adjustment',
                'label' => 'Bid %',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->mobile_bid_adjustment;
                }
            ],
            [
                'key' => 'date_time_changed',
                'label' => 'Change Date',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->date_time_changed;
                }
            ],
            [
                'key' => 'follow_up_date',
                'label' => 'Follow Up',
                'filter_type' => 'null',
                'value' => function ($data) {
                    if ($data->follow_up_date && $data->follow_up_date != "0000-00-00 00:00:00") {
                        if ($data->follow_up_state)
                            return '<strike>' . date('Y-m-d', strtotime($data->follow_up_date)) . '</strike>';
                        return date('Y-m-d', strtotime($data->follow_up_date));
                    }
                }
            ],
            [
                'key' => 'actions',
                'attributes' => [
                    'data-order_by' => 'false'
                ],
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    $res .= "<a href='" . route('reports.publishers.{publisher}.accounts.{account_id}.change.show', [$data->publisher, $data->account_id, $data->id]) . "'
                        title='Show details'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'>
                        <i aria-hidden='true' class='icon-magnifier'> </i></a>";

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= "<a href='". route('reports.publishers.{publisher}.accounts.{account_id}.change.edit', [$data->publisher, $data->account_id, $data->id, '#campaign']) ."' title='Edit' class='tooltips'>
                        <span aria-hidden='true' class='icon-pencil'></span></a> ";
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= Form::open(['route' => ['reports.publishers.{publisher}.accounts.{account_id}.change.destroy', $data->publisher, $data->account_id, $data->id, '#scheduling'], 'method' => 'delete', 'class' => 'form-invisible']);
                        $res .= '<button type="submit" title="Delete" class="tooltips"><span aria-hidden="true" class="icon-trash"></span></a></button>';
                        $res .= Form::close();
                    }

                    return $res;
                }
            ],
        ]
    ])


    @include('partials.containers.portlet', [
        'part' => 'nextTab',
        'tab_id' => 'ad_group'
    ])

    <div class="actions text-right">
        <a class="btn btn-circle btn-icon btn-default" title="Add new" href="{{ route('reports.publishers.{publisher}.accounts.{account_id}.change.create', [$account->publisher, $account->account_id, '#ad_group']) }}">
            <i class="icon-plus"></i> Add New</a>
    </div>


    @include('partials.containers.data-table', [
        'data' => $logs_ad_group,
        'name' => 'ad_groups_log',
        'columns' => [
            [
                'key' => 'id',
                'label' => 'ID',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->id;
                }
            ],
            [
                'key' => 'user',
                'label' => 'By',
                'value' => function ($data) {
                    return $data->first_name .' '. $data->last_name;
                }
            ],
            [
                'key' => 'campaign',
                'class' => 'center',
                'value' => function ($data) {
                    return $data->campaign;
                }
            ],
            [
                'key' => 'ad_group',
                'class' => 'center',
                'value' => function ($data) {
                    return $data->ad_group;
                }
            ],
            [
                'key' => 'comment',
                'label' => 'Action',
                'class' => 'center',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->comment;
                }
            ],
            [
                'key' => 'bid',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->bid;
                }
            ],
            [
                'key' => 'Mobile Bid Adjustment',
                'label' => 'Bid %',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->mobile_bid_adjustment;
                }
            ],
            [
                'key' => 'state',
                'class' => 'center',
                'value' => function ($data) {
                    return $data->state;
                }
            ],
            [
                'key' => 'date_time_changed',
                'label' => 'Change Date',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->date_time_changed;
                }
            ],
            [
                'key' => 'follow_up_date',
                'label' => 'Follow Up',
                'filter_type' => 'null',
                'value' => function ($data) {
                    if ($data->follow_up_date && $data->follow_up_date != "0000-00-00 00:00:00") {
                        if ($data->follow_up_state)
                            return '<strike>' . date('Y-m-d', strtotime($data->follow_up_date)) . '</strike>';
                        return date('Y-m-d', strtotime($data->follow_up_date));
                    }
                }
            ],
            [
                'key' => 'actions',
                'attributes' => [
                    'data-order_by' => 'false'
                ],
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    $res .= "<a href='" . route('reports.publishers.{publisher}.accounts.{account_id}.change.show', [$data->publisher, $data->account_id, $data->id]) . "'
                        title='Show details'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'>
                        <i aria-hidden='true' class='icon-magnifier'></i></a>";

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= "<a href=\"". route('reports.publishers.{publisher}.accounts.{account_id}.change.edit', [$data->publisher, $data->account_id, $data->id, '#ad_group']) ."\"  title='Edit' class='tooltips'>
                        <span aria-hidden=\"true\" class=\"icon-pencil\"></span></a> ";
                    }
                    //dd(auth()->user()->roles->first()->permissions->toJson());

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= Form::open(['route' => ['reports.publishers.{publisher}.accounts.{account_id}.change.destroy', $data->publisher, $data->account_id, $data->id, '#scheduling'], 'method' => 'delete', 'class' => 'form-invisible']);
                        $res .= '<button type="submit" title="Delete" class="tooltips"><span aria-hidden="true" class="icon-trash"></span></a></button>';
                        $res .= Form::close();
                    }

                    return $res;
                }
            ],
        ]
    ])


    @include('partials.containers.portlet', [
        'part' => 'nextTab',
        'tab_id' => 'keyword'
    ])

    <div class="actions text-right">
        <a class="btn btn-circle btn-icon btn-default" title="Add new" href="{{ route('reports.publishers.{publisher}.accounts.{account_id}.change.create', [$account->publisher, $account->account_id, '#keyword']) }}">
            <i class="icon-plus"></i> Add New</a>
    </div>


    @include('partials.containers.data-table', [
        'data' => $logs_keywords,
        'name' => 'keywords_log',
        'columns' => [
            [
                'key' => 'id',
                'label' => 'ID',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->id;
                }
            ],
            [
                'key' => 'user',
                'label' => 'By',
                'value' => function ($data) {
                    return $data->first_name .' '. $data->last_name;
                }
            ],
            [
                'key' => 'campaign',
                'class' => 'center',
                'value' => function ($data) {
                    return $data->campaign;
                }
            ],
            [
                'key' => 'Keyword',
                'value' => function ($data) {
                    return $data->keyword;
                }
            ],
            [
                'key' => 'comment',
                'label' => 'Action',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->comment;
                }
            ],
            [
                'key' => 'bid',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->bid;
                }
            ],
            [
                'key' => 'state',
                'class' => 'center',
                'value' => function ($data) {
                    return $data->state;
                }
            ],
            [
                'key' => 'date_time_changed',
                'label' => 'Change Date',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->date_time_changed;
                }
            ],
            [
                'key' => 'follow_up_date',
                'label' => 'Follow Up',
                'filter_type' => 'null',
                'value' => function ($data) {
                    if ($data->follow_up_date && $data->follow_up_date != "0000-00-00 00:00:00") {
                        if ($data->follow_up_state)
                            return '<strike>' . date('Y-m-d', strtotime($data->follow_up_date)) . '</strike>';
                        return date('Y-m-d', strtotime($data->follow_up_date));
                    }
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'attributes' => [
                    'data-order_by' => 'false'
                ],
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    $res .= "<a href='" . route('reports.publishers.{publisher}.accounts.{account_id}.change.show', [$data->publisher, $data->account_id, $data->id]) . "'
                        title='Show details'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'>
                        <i aria-hidden='true' class='icon-magnifier'></i></a>";

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= "<a href=\"". route('reports.publishers.{publisher}.accounts.{account_id}.change.edit', [$data->publisher, $data->account_id, $data->id, '#keyword']) ."\">
                        <span aria-hidden=\"true\" class=\"icon-pencil\" title='Edit' class='tooltips'></span></a> ";
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= Form::open(['route' => ['reports.publishers.{publisher}.accounts.{account_id}.change.destroy', $data->publisher, $data->account_id, $data->id, '#scheduling'], 'method' => 'delete', 'class' => 'form-invisible']);
                        $res .= '<button type="submit" title="Delete" class="tooltips"><span aria-hidden="true" class="icon-trash"></span></a></button>';
                        $res .= Form::close();
                    }

                    return $res;
                }
            ],
        ]
    ])


    @include('partials.containers.portlet', [
        'part' => 'nextTab',
        'tab_id' => 'scheduling'
    ])

    <div class="actions text-right">
        <a class="btn btn-circle btn-icon btn-default" title="Add new" href="{{ route('reports.publishers.{publisher}.accounts.{account_id}.change.create', [$account->publisher, $account->account_id, '#scheduling']) }}">
            <i class="icon-plus"></i> Add New</a>
    </div>


    @include('partials.containers.data-table', [
        'data' => $logs_scheduling,
        'name' => 'scheduling_log',
        'columns' => [
            [
                'key' => 'id',
                'label' => 'ID',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->id;
                }
            ],
            [
                'key' => 'user',
                'label' => 'By',
                'value' => function ($data) {
                    return $data->first_name .' '. $data->last_name;
                }
            ],
            [
                'key' => 'campaign',
                'class' => 'center',
                'value' => function ($data) {
                    return $data->campaign;
                }
            ],
            [
                'key' => 'comment',
                'label' => 'Action',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->comment;
                }
            ],
            [
                'key' => 'days',
                'label' => 'Days',
                'class' => 'center',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return array2HtmlTags(explode(',', $data->days_list));
                }
            ],
            [
                'key' => 'hours',
                'filter_type' => 'null',
                'value' => function ($data) {
                    $_str = $data->time_from;
                    if ($_str)
                        $_str .= ' - ';
                    return $_str . $data->time_until;
                }
            ],
            [
                'key' => 'bid',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->bid;
                }
            ],
            [
                'key' => 'bid_multiplier',
                'label' => 'Bid %',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->bid_multiplier;
                }
            ],
            [
                'key' => 'state',
                'class' => 'center',
                'value' => function ($data) {
                    return $data->state;
                }
            ],
            [
                'key' => 'date_time_changed',
                'label' => 'Change Date',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->date_time_changed;
                }
            ],
            [
                'key' => 'follow_up_date',
                'label' => 'Follow Up',
                'filter_type' => 'null',
                'value' => function ($data) {
                    if ($data->follow_up_date && $data->follow_up_date != "0000-00-00 00:00:00") {
                        if ($data->follow_up_state)
                            return '<strike>' . date('Y-m-d', strtotime($data->follow_up_date)) . '</strike>';
                        return date('Y-m-d', strtotime($data->follow_up_date));
                    }
                }
            ],
            [
                'key' => 'actions',
                'attributes' => [
                    'data-order_by' => 'false'
                ],
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $res = '';

                    $res .= "<a href='" . route('reports.publishers.{publisher}.accounts.{account_id}.change.show', [$data->publisher, $data->account_id, $data->id]) . "'
                        title='Show details'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'>
                        <i aria-hidden='true' class='icon-magnifier'></i></a>";

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= "<a href=\"". route('reports.publishers.{publisher}.accounts.{account_id}.change.edit', [$data->publisher, $data->account_id, $data->id, '#scheduling']) ."\">
                        <span aria-hidden=\"true\" class=\"icon-pencil\" title='Edit' class='tooltips'></span></a> ";
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= Form::open(['route' => ['reports.publishers.{publisher}.accounts.{account_id}.change.destroy', $data->publisher, $data->account_id, $data->id, '#scheduling'], 'method' => 'delete', 'class' => 'form-invisible']);
                        $res .= '<button type="submit" title="Delete" class="tooltips"><span aria-hidden="true" class="icon-trash"></span></a></button>';
                        $res .= Form::close();
                    }

                    return $res;
                }
            ],
        ]
    ])


    @include('partials.containers.portlet', [
        'part' => 'closeTabs'
    ])
@stop



