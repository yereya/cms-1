@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'All Changes'
        ])

    @include('partials.containers.data-table', [
        'data' => $changes,
        'name' => 'campaigns_log',
        'columns' => [
            [
                'key' => 'follow_up_date',
                'label' => 'Change Date',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return $data->follow_up_date ? $data->follow_up_date : $data->date_time_changed;
                }
            ],
            [
                'key' => 'follow_up_date',
                'label' => 'Follow Up',
                'filter_type' => 'null',
                'value' => function ($data) {
                    if ($data->follow_up_date && $data->follow_up_date != "0000-00-00 00:00:00") {
                        if ($data->follow_up_state)
                            return '<strike>' . date('Y-m-d', strtotime($data->follow_up_date)) . '</strike>';
                        return date('Y-m-d', strtotime($data->follow_up_date));
                    }
                }
            ],
            [
                'key' => 'user_name',
                'label' => 'User name',
                'value' => function ($data) {
                    return $data->users['username'];
                }
            ],
            [
                'key' => 'change_level',
                'value' => function($data) {
                    return $data->change_level;
                }
            ],
            [
                'key' => 'account',
                'label' => 'Account',
                'value' => function ($data) {
                    return $data->accountByAccountId['account_name'];
                }
            ],
            [
                'key' => 'campaign',
                'value' => function($data) {
                    return $data->keywordByCampaignId['campaign'];
                }
            ],
            [
                'key' => 'comment',
                'label' => 'Action',
                'value' => function($data) {
                    return $data->comment;
                }
            ],
            [
                'key' => 'actions',
                'attributes' => [
                    'data-order_by' => 'false'
                ],
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name, $publisher) {
                    $res = '';

                    $res .= "<a href='" . route('reports.publishers.{publisher}.accounts.{account_id}.change.show', [$publisher, $data->account_id, $data->id]) . "'
                        title='Show details'
                        class='tooltips'
                        data-toggle='modal'
                        data-target='#ajax-modal'>
                        <i aria-hidden='true' class='icon-magnifier'> </i></a>";

                    if (auth()->user()->can($permission_name.'.edit')) {
                        $res .= "<a href='". route('reports.publishers.{publisher}.accounts.{account_id}.change.edit', [$publisher, $data->account_id, $data->id, '#campaign']) ."' title='Edit' class='tooltips' target='_blank'>
                        <span aria-hidden='true' class='icon-pencil'></span></a> ";
                    }

                    if (auth()->user()->can($permission_name.'.delete')) {
                        $res .= Form::open(['route' => ['reports.publishers.{publisher}.accounts.{account_id}.change.destroy', $publisher, $data->account_id, $data->id], 'method' => 'delete', 'class' => 'form-invisible']);
                        $res .= '<button type="submit" title="Delete" class="tooltips"><span aria-hidden="true" class="icon-trash"></span></a></button>';
                        $res .= Form::close();
                    }

                    return $res;
                }
            ]
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'close',
        ])
@endsection