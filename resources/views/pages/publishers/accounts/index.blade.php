@extends('layouts.metronic.main')

@section('page_content')
    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => 'Publisher Accounts',
        'tabs' => [
            [
                'id' => 'adwords',
                'label' => 'Adwords',
            ],
            [
                'id' => 'bing',
                'label' => 'Bing',
            ]
        ]
    ])

    <div class="actions text-right">
        @if(auth()->user()->can($permission_name. '.field[list_all].view'))
            <a class="btn btn-circle btn-icon btn-default tooltips" title="Show all changes"
               href="{{ route('reports.publishers.{publisher}.accounts.change.list-all', 'adwords') }}"
               data-container="body" data-placement="top">
                <i class="glyphicon glyphicon-list-alt"></i> Show all changes</a>
        @endif
        @if(auth()->user()->can($permission_name. '.field[update_accounts_n_keywords].view'))
            <a class="btn btn-circle btn-icon btn-default tooltips" title="Pull accounts from publisher"
               href="javascript:;"
               data-ajax_call="{{ route('reports.publishers.{publisher}.accounts.updateApi', 'adwords', false) }}"
               data-ajax_spinner="true"
               data-container="body" data-placement="top">
                <i class="icon-cloud-download"></i> Update Accounts & Keywords</a>
        @endif
    </div>

    @include('partials.containers.data-table', [
        'data' => $accounts_adwords,
        'columns' => [
            [
                'key' => 'account_name',
                'label' => 'Account Name',
                'value' => function ($data) {
                    return $data->account_name;
                }
            ],
            [
                'key' => 'account_id',
                'label' => 'Account ID',
                'value' => function ($data) {
                    return $data->account_id;
                }
            ],
            [
                'key' => 'account_type',
                'label' => 'Account Type',
                'class' => 'center',
                'filter_type' => 'null',
                'permission' => auth()->user()->can($permission_name. '.column[account_type].view'),
                'value' => function ($data) {
                    return "<a href='javascript:;'
                        class='editable editable-ajax'
                        data-pk='{$data->account_id}'
                        data-pk_column='account_id'
                        data-name='type'
                        data-value='{$data->type}'
                        data-type='select'
                        data-title='Account Type:'
                        data-source='". route('reports.publishers.{publisher}.accounts.xEditableApi', [$data->publisher, 'get=accountTypes']) ."'
                        data-url='". route('reports.publishers.{publisher}.accounts.xEditableApi', [$data->publisher, 'set=accountTypes']) ."'>{$data->type}</a>";
                }
            ],
            [
                'key' => 'advertiser',
                'label' => 'Advertiser',
                'class' => 'center',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $advertiser_name = isset($data->advertisers) ? $data->advertisers->name : '';
                    if (auth()->user()->can($permission_name. '.field[advertiser].edit')) {
                        return "<a href='javascript:;'
                            class='editable editable-ajax'
                            data-pk='{$data->account_id}'
                            data-pk_column='account_id'
                            data-name='advertiser_id'
                            data-value='{$advertiser_name}'
                            data-type='select'
                            data-title='Advertiser:'
                            data-source='". route('reports.publishers.{publisher}.accounts.xEditableApi', [$data->publisher, 'get=advertisers']) ."'
                            data-url='". route('reports.publishers.{publisher}.accounts.xEditableApi', [$data->publisher, 'set=advertisers']) ."'>{$advertiser_name}</a>";
                    }
                    else {
                        return $advertiser_name;
                    }
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return "<a href='". route('reports.publishers.{publisher}.accounts.{account_id}.change.create', [$data->publisher, $data->account_id, '#adwords']) ."' title='Report change' class='tooltips'><span aria-hidden='true' class='icon-plus'></span></a> ".
                    "<a href='". route('reports.publishers.{publisher}.accounts.{account_id}.change.index', [$data->publisher, $data->account_id, '#adwords']) ."' title='View reported changes' class='tooltips'><span aria-hidden='true' class='icon-magnifier'></span></a>";
                }
            ]
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'nextTab',
        'tab_id' => 'bing'
    ])

    <div class="actions text-right">
        @if(auth()->user()->can($permission_name. '.field[list_all].view'))
            <a class="btn btn-circle btn-icon btn-default tooltips" title="Show all changes"
               href="{{ route('reports.publishers.{publisher}.accounts.change.list-all', 'bing') }}"
               data-container="body" data-placement="top">
                <i class="glyphicon glyphicon-list-alt"></i> Show all changes</a>
        @endif
        @if(auth()->user()->can($permission_name. '.field[update_accounts_n_keywords].view'))
            <a class="btn btn-circle btn-icon btn-default tooltips" title="Update accounts and keywords"
               href="javascript:;"
               data-ajax_call="{{ route('reports.publishers.{publisher}.accounts.updateApi', 'bing') }}"
               data-ajax_spinner="true"
               data-container="body" data-placement="top">
                <i class="icon-cloud-download"></i> Update Accounts & Keywords</a>
        @endif
    </div>

    @include('partials.containers.data-table', [
        'data' => $accounts_bing,
        'columns' => [
            [
                'key' => 'account_name',
                'label' => 'Account Name',
                'value' => function ($data) {
                    return $data->account_name;
                }
            ],
            [
                'key' => 'account_id',
                'label' => 'Account ID',
                'value' => function ($data) {
                    return $data->account_id;
                }
            ],
            [
                'key' => 'account_type',
                'label' => 'Account Type',
                'class' => 'center',
                'filter_type' => 'null',
                'permission' => auth()->user()->can($permission_name. '.column[account_type].view'),
                'value' => function ($data) {
                    return "<a href='javascript:;'
                        class='editable editable-ajax'
                        data-pk='{$data->account_id}'
                        data-pk_column='account_id'
                        data-name='type'
                        data-value='{$data->type}'
                        data-type='select'
                        data-title='Account Type:'
                        data-source='". route('reports.publishers.{publisher}.accounts.xEditableApi', [$data->publisher, 'get=accountTypes']) ."'
                        data-url='". route('reports.publishers.{publisher}.accounts.xEditableApi', [$data->publisher, 'set=accountTypes']) ."'>{$data->type}</a>";
                }
            ],
            [
                'key' => 'advertiser',
                'label' => 'Advertiser',
                'class' => 'center',
                'filter_type' => 'null',
                'value' => function ($data) use ($permission_name) {
                    $advertiser_name = isset($data->advertisers) ? $data->advertisers->name : '';
                    if (auth()->user()->can($permission_name. '.field[advertiser].edit')) {
                        return "<a href='javascript:;'
                            class='editable editable-ajax'
                            data-pk='{$data->account_id}'
                            data-pk_column='account_id'
                            data-name='advertiser_id'
                            data-value='{$advertiser_name}'
                            data-type='select'
                            data-title='Advertiser:'
                            data-source='". route('reports.publishers.{publisher}.accounts.xEditableApi', [$data->publisher, 'get=advertisers']) ."'
                            data-url='". route('reports.publishers.{publisher}.accounts.xEditableApi', [$data->publisher, 'set=advertisers']) ."'>{$advertiser_name}</a>";
                    }
                    else {
                        return $advertiser_name;
                    }
                }
            ],
            [
                'key' => 'actions',
                'label' => 'Actions',
                'filter_type' => 'null',
                'value' => function ($data) {
                    return "<a href='". route('reports.publishers.{publisher}.accounts.{account_id}.change.create', [$data->publisher, $data->account_id, '#bing']) ."' title='Report change' class='tooltips'><span aria-hidden='true' class='icon-plus'></span></a> ".
                    "<a href='". route('reports.publishers.{publisher}.accounts.{account_id}.change.index', [$data->publisher, $data->account_id, '#bing']) ."' title='View reported changes' class='tooltips'><span aria-hidden='true' class='icon-magnifier'></span></a>";
                }
            ]
        ]
    ])

    @include('partials.containers.portlet', [
        'part' => 'closeTabs'
    ])
@stop