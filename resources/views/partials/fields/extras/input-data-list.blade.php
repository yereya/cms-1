<datalist id="list_{{ snake_case($name) }}">
    @foreach($suggestions as $suggestion)
        <option value="{{ $suggestion }}"></option>
    @endforeach
</datalist>
