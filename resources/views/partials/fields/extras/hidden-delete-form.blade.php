{{-- Hidden delete form --}}
{{-- Used for delete forms --}}
{!! Form::open([
    'url' => $route_url,
    'method' => 'POST',
    'class' => 'delete_item',
    'role' => 'form',
    'style' => 'display:none'
]) !!}
    {!! Form::hidden('_method', 'delete') !!}
    {!! Form::hidden('_token', csrf_token()) !!}
{!! Form::close() !!}
