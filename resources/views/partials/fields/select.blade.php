<?php $value = old(snake_case($name), isset($value) ? $value : '') ?>
@if($form_line ?? false)
    <div class="{{ isset($column) ? 'col-md-' . $column : '' }}">
        @else
            <div class="form-group form-md-line-input {{ !empty($errors) && $errors->get($name) ? ' has-error' : '' }} {{ isset($column) && $column ? 'col-md-' . $column : '' }}">
                @endif
                @if (!isset($hide_label))
                    <label class="col-md-{{ isset($column_label) ? $column_label : 2 }} control-label"
                           for="form_control_{{ snake_case($name) }}">
                        {{ isset($required) ? '*' : '' }}{!! $label or ucwords(str_replace(['-', '_'], ' ', $name)) !!}
                    </label>
                @endif

                @if(isset($column_input))
                    <div class="col-md-{{ $column_input }}">
                        @elseif(!isset($column_input) && isset($column_label) && $column_label == 12)
                            <div class="col-md-12">
                                @else
                                    <div class="col-md-{{ isset($column_label) ? 12 - $column_label : 9 }}">
                                        @endif

                                        <select class="form-control input-sm select2
        @if (isset($sortable)) select2-multiple sortable
        @elseif (isset($multi_select)) select2-multiple
        @endif
                                        {{ isset($ajax) ? 'select2-ajax' : '' }} {{ isset($class) ? $class : '' }}"
                                                {!! isset($required) || isset($required_not_asterisk) ? 'required="required"' : '' !!}
                                                {!! isset($readonly) ? 'readonly' : '' !!}
                                                name="{{ snake_case($name) }}{!! (isset($multi_select) || isset($sortable)) ? '[]' : '' !!}"
                                                id="{{ isset($id) ? $id : '' }}"
                                                {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
                                                {!! (isset($force_selection) && $force_selection) ? 'data-force-selection="true"' : '' !!}
                                                {!! isset($disabled) && $disabled ? 'disabled="disabled"' : '' !!}
                                                {!! isset($multi_select) || isset($sortable) ? 'multiple="multiple"' : '' !!}
                                                data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}"
                                                {!! isset($taginput) && $taginput ? 'data-role="tagsinput"' : '' !!}
                                        >
                                            @if(isset($list))
                                                <?php
                                                if (is_a($list, \Illuminate\Support\Collection::class)) {
                                                    $list = $list->toArray();
                                                } else if (is_callable($list)) {
                                                    $list = $list();
                                                }
                                                ?>
                                                @foreach($list as $key => $item)
                                                    <?php
                                                    $_id = $key;
                                                    $_attributes = '';
                                                    if (is_array($item)) {
                                                        $_id         = $item['id'];
                                                        $_attributes = HTML::attributes($item);
                                                    }
                                                    ?>
                                                    <option value="{{ $_id }}"
                                                            {!! $_attributes !!}
                                                            @if(isset($value))
                                                            @if(
                                                                (is_array($value) && in_array($_id, $value))
                                                                ||
                                                                (is_a($value, \Illuminate\Support\Collection::class) && $value->find($_id))
                                                                ||
                                                                ((is_string($value) || is_int($value)) && $_id == $value)
                                                            )
                                                            selected="selected"
                                                            @endif
                                                            @endif
                                                    >{{ is_array($item) ? $item['text'] : $item }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                        <div class="form-control-focus"></div>

                                        @if(isset($help_text))
                                            <span class="help-block">{{ $help_text }}</span>
                                        @endif

                                        @if (!empty($errors))
                                            @foreach($errors->get($name) as $error)
                                                <span class="help-block error">{{ $error }}</span>
                                            @endforeach
                                        @endif
                                    </div>
                            </div>