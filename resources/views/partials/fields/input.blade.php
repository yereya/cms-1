{{--
 Traffic point input -  full documentation of use cases & options can be found in a doc file
 called 'Input Documentation' on our shared drive
 --}}
@if (!isset($type) || (isset($type) && $type != 'hidden'))
    @if(!isset($form_line))
        <div class="form-group form-md-line-input {{ !empty($errors) && $errors->get($name) ? ' has-error' : '' }}">
            @else
                <div class="{{ isset($column) ? 'col-md-' . $column : '' }}">
                    @endif
                    @if(!isset($no_label))
                        <label class="col-md-{{ isset($column_label) ? $column_label : 2 }} control-label"
                               for="form_control_{{ snake_case($name) }}">
                            {{ (isset($required) && $required) ? '*' : '' }}{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}
                        </label>
                    @endif

                    @if(isset($column_input))
                        <div class="col-md-{{ $column_input }}">
                            @elseif(!isset($column_input) && isset($column_label) && $column_label == 12)
                                <div class="col-md-12">
                                    @else
                                        <div class="col-md-{{ isset($column_label) ? 12 - $column_label : 9 }}">
                                            @endif
                                            @endif
                                            @if (isset($group) && $group)
                                                <div class="input-group">
                                                    @endif
                                                    @if(isset($all_mighty) && $all_mighty)
                                                        @include('partials.fields.input-all-mighty')
                                                    @else
                                                        @include('partials.fields.input-default')
                                                    @endif
                                                    <div class="form-control-focus"></div>

                                                    @if(isset($help_text))
                                                        <span class="help-block">{{ $help_text }}</span>
                                                    @endif

                                                    @if (!empty($errors))
                                                        @foreach($errors->get($name) as $error)
                                                            <span class="error-block">{{ $error }}</span>
                                                        @endforeach
                                                    @endif

                                                    @if (!isset($type) || (isset($type) && $type != 'hidden'))
                                                </div>
                                                <div class="clearfix"></div>
                                        </div>
    @endif