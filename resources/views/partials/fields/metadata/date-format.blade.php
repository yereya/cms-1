@include('partials.fields.input',[
    'name'=>'metadata[date_format]',
    'type'=> 'text',
    'label'=>'placeholder',
    'value'=> isset($data) && isset($data->date_format) ? $data->date_format : 'YYYY-MM-DD'
])