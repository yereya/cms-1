@include('partials.fields.input',[
    'name' => 'metadata[height]',
    'type' => 'number',
    'label' => 'height',
    'value' => isset($data) && isset($data->height) ? $data->height : ''
])