<?php
if (isset($data) && !is_null($data) && is_string($data)) {
    $data = json_decode($data);
}
?>

@include('partials.fields.select', [
    'label' => 'Content Type',
    'name' => 'metadata[content_type_id]',
    'value' => $data->content_type_id ?? 0,
    'list' => $content_types ?? []
])

{!! Form::hidden('metadata[current_content_type_id]', $content_type->id) !!}

<script type="application/javascript">
    (function() {
        App.initSelect2ToSelects();
    })();
</script>