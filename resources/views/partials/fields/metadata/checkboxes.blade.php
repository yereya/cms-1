<?php
if (isset($data) && !is_null($data) && is_string($data)) {
    $data = json_decode($data);
}
?>

<div class="col-md-offset-2">
    <div class="column sortable">
        @if(isset($data->checkbox))
            @foreach($data->checkbox as $row)
                @include('partials.containers.duplicate-group', ['part' => 'open'])
                <div class="col-md-1 ui-sortable-handle" style="margin-top: 7px;">
                    <i class="draggable-icon fa fa-arrows" style=" display: none; cursor: move;"></i>
                </div>

                @include('partials.fields.input', [
                    'name' => 'checkbox[key][]',
                    'label' => 'key',
                    'value' => $row->key,
                    'form_line' => true,
                    'type' => 'input',
                    'column' => 4,
                    'help_text' => isset($metadata->instructions) ? $metadata->instructions : '',
                    'attributes' => [
                    'placeholder' => $metadata->placeholder ?? ''
                    ]
                ])

                @include('partials.containers.actions', [
                     'class' => true,
                     'actions' => [
                          [
                              'action' => 'clone',
                          ],
                          [
                              'action' => 'remove',
                          ]
                     ]
                ])
                @include('partials.containers.duplicate-group', ['part' => 'close'])
            @endforeach
        @else
            @include('partials.containers.duplicate-group', ['part' => 'open'])
            <div class="col-md-1 ui-sortable-handle" style="margin-top: 7px;">
                <i class="draggable-icon fa fa-arrows" style=" display: none; cursor: move;"></i>
            </div>

            @include('partials.fields.input', [
                'name' => 'checkbox[key][]',
                'label' => 'key',
                'value' => '',
                'form_line' => true,
                'type' => 'input',
                'column' => 4,
                'help_text' => isset($metadata->instructions) ? $metadata->instructions : '',
                'attributes' => [
                'placeholder' => $metadata->placeholder ?? ''
                ]
            ])

            @include('partials.containers.actions', [
                 'class' => true,
                 'actions' => [
                      [
                          'action' => 'clone',
                      ],
                            [
                                'action' => 'remove',
                            ]
                        ]])
            @include('partials.containers.duplicate-group', ['part' => 'close'])
        @endif
    </div>
</div>

<script type="application/javascript">
    App.initDuplicateGroup();
    topApp.sortable();
</script>