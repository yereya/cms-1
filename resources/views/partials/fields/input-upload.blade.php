@if (!isset($type) || (isset($type) && $type != 'hidden'))
    @if(!isset($form_line))
        <div class="form-group form-md-line-input fileupload-buttonbar {{ !$errors->get($name) ? '' : ' has-error' }}">
            @else
                <div class="fileupload-buttonbar {{ isset($column) ? 'col-md-' . $column : '' }}">
                    @endif
                    @if(!isset($no_label))
                        <label class="col-md-{{ isset($column_label) ? $column_label : 2 }} control-label" for="form_control_{{ snake_case($name) }}">
                            {{ isset($required) ? '*' : '' }}{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}
                        </label>
                    @endif

                    @if(isset($column_input))
                        <div class="col-md-{{ $column_input }}">
                            @else
                                <div class="col-md-{{ isset($column_label) ? 12 - $column_label : 9 }}">
                                    @endif
                                    @endif
                                    @if (isset($group) && $group) <div class="input-group"> @endif
                                        <span class="btn green fileinput-button">

        <i class="fa fa-upload"></i>
        <span> Select file </span>

        <input type="file"
               name="{{ $name or '' }}"
               id="{{ isset($id) ? $id : snake_case($name) }}"
               class="form-control {{ isset($class) ? implode(" ", (array) $class) : '' }}"
               {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
               value="{{ old(snake_case($name), isset($value) ? $value : '') }}"
                {!! isset($required) && $required ? 'required="required"' : '' !!}
                {!! isset($disabled) && $disabled ? 'disabled="disabled"' : '' !!}
                {!! isset($placeholder) ?  'placeholder=" '. $placeholder .'"' : '' !!}
        >
    </span>


                                        <div class="form-control-focus"></div>

                                        @foreach($errors->get($name) as $error)
                                            <span class="error-block">{{ $error }}</span>
                                        @endforeach
                                        @if (!isset($type) || (isset($type) && $type != 'hidden'))
                                    </div>
                                </div>
    @endif