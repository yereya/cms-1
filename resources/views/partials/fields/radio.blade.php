<div class="col-md-{{ isset($column) ? $column : 2 }}">
    <div class="md-radio {{ isset($class) ? $class : '' }}">
        <input type="radio" id="{{ $radio_id }}"
               name="{{ isset($name) ? snake_case($name) : snake_case($label) }}"
               value="{{ isset($value) ? $value : 0 }}"
               class="md-radiobtn {{ isset($ajax) && $ajax ? 'ajax-radio-button' : '' }}"
               {{ isset($checked) && $checked ? 'checked="checked"' : '' }}
               {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
               data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}"
        >
        <label for="{{ $radio_id }}">
            <span class="inc"></span>
            <span class="check"></span>
            <span class="box"></span>{{ $label }}</label>
    </div>
</div>