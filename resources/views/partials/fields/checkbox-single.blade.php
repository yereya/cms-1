<div class="col-md-{{ isset($column) ? $column : 2 }}">

<label class="checkbox-inline">
    <input type="{{ $type or 'checkbox' }}"
           name="{{ isset($name) ? snake_case($name) : snake_case($label) }}"
           class="{{ isset($class) ? implode(" ", (array) $class) : '' }}"
           {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
           value="{{ old(isset($name) ? snake_case($name) : snake_case($label), isset($value) ? $value : "0") }}"
            {{ isset($disabled) ? "disabled=\"\"" : "" }}
            {{ isset($checked) && $checked ? "checked" : "" }}
           data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}"
    > {{ $label or '' }} </label>

</div>