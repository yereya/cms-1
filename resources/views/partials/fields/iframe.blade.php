<iframe
        src="{{ $src }}"
        class="{{ isset($class) ? $class : '' }} {{ isset($window_size) && $window_size ? 'iframe-file-manager' : '' }}"
        {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
        style="border: 0; margin: 0;">

</iframe>