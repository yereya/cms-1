    @if(isset($url) && !is_null($url))
    <a href="{{ $url }}"
           class="btn {{ isset($class) ? implode(" ", (array) $class) : 'default' }}"
           {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
    >
        @if(isset($icon) && !is_null($icon))
            <i class="{{ $icon }}"></i>
        @endif
        {!! $value or 'Submit' !!}</a>
@else
    <button
            type="{{ $type or 'submit' }}"
            class="btn {{ isset($class) ? implode(" ", (array) $class) : 'blue' }} {!! isset($right) ? 'pull-right' : '' !!}"
            {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
    >{!! isset($icon) ? '<i class="' . $icon . '"></i> ' : '' !!}{!! $value or 'Submit' !!}</button>
@endif
