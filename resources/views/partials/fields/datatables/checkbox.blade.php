<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
    <input type="checkbox" name="{{ $name }}" class="checkboxes" value="{{ $value or 1 }}">
    <span>{{ $label or '' }}</span>
</label>