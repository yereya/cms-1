
@if(isset($wrapper))
    <div class="form-group form-md-line-input">
        @if(isset($label))
            <label class="col-md-{{ isset($column) ? 12-$column : 2 }} control-label">
                {{ $label }}
            </label>
        @endif
    <div class="{{ isset($column) ? 'col-md-' . $column : 'col-md-9' }}">
@endif
        {{-- Options: ["list", "inline"] --}}
        <div class="checkbox-{{ isset($display) ? $display : 'list' }}">
            @if (isset($list))
            @foreach($list as $item)
                <label class="checkbox-inline" title="{{$item['title'] or ''}}">
                    <input type="hidden"
                           name="{{ isset($item['name']) ? $item['name'] : snake_case($item['label']) }}"
                           value="0"/>

                    <input type="{{ $type or 'checkbox' }}"
                           title="{{ $item['title'] or '' }}"
                           name="{{ isset($item['name']) ? $item['name'] : snake_case($item['label']) }}"
                           class="{{ isset($item['class']) ? implode(" ", (array) $item['class']) : '' }}"
                           {!! isset($item['attributes']) ? HTML::attributes($item['attributes']) : '' !!}
                           value="{{ old(isset($item['name']) ? snake_case($item['name']) : snake_case($item['label']), isset($item['value']) ? $item['value'] : "1") }}"
                            {{ isset($item['disabled']) ? "disabled=\"\"" : "" }}
                            {{ isset($item['checked']) && $item['checked'] ? "checked" : "" }}
                           data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $item['name'])) }}"
                    > {{ $item['label'] or '' }} </label>
            @endforeach
            @endif
        </div>
@if(isset($wrapper))
    </div>
    <div class="clearfix"></div>
</div>
@endif