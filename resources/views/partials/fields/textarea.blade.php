@php($attributes['rows'] = isset($attributes['rows']) ? $attributes['rows'] : 3)
<div class="form-group form-md-line-input @if($errors->get($name)) has-error @endif ">

    @if(!isset($no_label))
        <label class="col-md-2 control-label" for="form_control_{{ snake_case($name) }}"
        >{{ isset($required) ? '*' : '' }}{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}</label>
    @endif

    <div class="{{isset($no_label) ? 'col-md-12' : 'col-md-9' }}">
        <textarea
                name="{{ snake_case($name) }}"
                id="form_control_{{ snake_case($name) }}"
                {!! isset($codemirror)? 'data-codemirror_lang="'.$codemirror.'"': '' !!}
                  {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
                  {!! isset($required) ? 'required="required"' : '' !!}
                  {!! isset($placeholder) ?  'placeholder=" '. $placeholder : '' !!}
                  {!! isset($disabled) && $disabled ? 'disabled="disabled"' : '' !!}
                class="form-control {{ isset($class) ? implode(" ", (array) $class) : '' }} {{isset($codemirror) ? 'codemirror' : ''}}"
                data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}"
        @if(is_callable($value))
             <?php $value = $value(); ?>
        @endif
        >{{ old(snake_case($name), isset($value) ? $value : '') }}</textarea>

        <div class="form-control-focus"></div>
        @if(isset($help_text))
            <span class="help-block">{{ $help_text }}</span>
        @endif

        @foreach($errors->get($name) as $error)
            <span class="help-block">{{ $error }}</span>
        @endforeach
    </div>
    <div class="clearfix"></div>
</div>
