<input type="{{ $type or 'text' }}"
       class="form-control {{ isset($class) ? implode(" ", (array) $class) : '' }}"
       {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
       value="{{ old(snake_case($name), isset($value) ? $value : '') }}"
        {!! isset($required) && $required ? 'required="required"' : '' !!}
        {!! isset($disabled) && $disabled ? 'disabled="disabled"' : '' !!}
        {!! isset($placeholder) ?  'placeholder=" '. $placeholder .'"' : '' !!}

        {{-- Adds datalist list--}}
        {!! isset($suggestions) ? 'list="list_ ' . snake_case($name) . '"' : '' !!}
        @if ($id) id="{{ $id }}" @endif
       data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}"
>