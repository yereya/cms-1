@if (!isset($type) || (isset($type) && $type != 'hidden'))
    @if(!isset($form_line))
        <div class="form-group form-md-line-input {{ !$errors->get($name) ? '' : ' has-error' }}">
    @else
        <div class="{{ isset($column) ? 'col-md-' . $column : '' }}">
    @endif

    @if(!isset($no_label))
        <label class="col-md-{{ isset($column_label) ? $column_label : 2 }} control-label"
               for="form_control_{{ snake_case($name) }}">
            {{ isset($required) ? '*' : '' }}{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}
        </label>
    @endif

    @if(isset($column_input))
        <div class="col-md-{{ $column_input }}">
    @elseif(!isset($column_input) && isset($column_label) && $column_label == 12)
        <div class="col-md-12">
    @else
        <div class="col-md-{{ isset($column_label) ? 12 - $column_label : 9 }}">
    @endif
@endif

        {{--<div class="col-md-4">--}}
        <div class="input-group date-range-picker" >
            <input type="text"
                   class="form-control {{ @isset($class) ? $class : '' }}"
                   value="{{ old(snake_case($name), isset($value) ? $value : '') }}"
                   name="{{ @isset($name) ? $name : '' }}"
                    {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
                    {!! isset($required) && $required ? 'required="required"' : '' !!}
                    {!! isset($disabled) && $disabled ? 'disabled="disabled"' : '' !!}
                    {!! isset($placeholder) ?  'placeholder=" '. $placeholder .'"' : '' !!}
            >
            <span class="input-group-btn">
                <button class="btn default date-range-toggle" type="button">
                    <i class="fa fa-calendar"></i>
                </button>
            </span>
            @if($raw_value ?? false)
                {!! Form::hidden('date_range[range_name]', $raw_value, array('class' => 'date_range_short_code')) !!}
            @endif
        </div>

        @if(isset($help_text))
            <span class="help-block">{{ $help_text }}</span>
        @endif

        @foreach($errors->get($name) as $error)
            <span class="error-block">{{ $error }}</span>
        @endforeach

        @if (!isset($type) || (isset($type) && $type != 'hidden'))
            </div>
            </div>
        @endif