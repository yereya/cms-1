<div class="tp-form-group-collection {{ isset($class) ? $class : null }}">
    <div class="form-group form-md-line-input {{ !$errors->get($name) ?: ' has-error' }}">
        <label class="control-label {{ isset($column) ? 'col-md-' . $column : 'col-md-2' }}"
               form_control_{{ snake_case($name) }}>
            {{ isset($required) ? '*' : '' }}{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}
        </label>
        <div class="col-md-9">
            <input type="text"
                   class="form-control {{ isset($class) ? implode(" ", (array) $class) : '' }}"
                   value="{{ implode(',', $value) }}"
                   name="{{ $name }}"
                   data-role="tagsinput">
        </div>
    </div>

    @if(isset($controls))
        <div class="form-group form-md-line-input">
            <div class="col-md-9 col-md-offset-2">
                <div class="form-inline {{ $name . '_control' }}">
                    @foreach($controls as $control)
                        <div class="form-group form-md-line-input {{ isset($hidden) ? 'hidden' : ''}}">
                            <label class="control-label">{{ $control['label'] or ucwords(str_replace(['-', '_'], ' ', $control['name'])) }}</label>
                            @if($control['type'] == 'select')
                                @include('partials.fields.select-only', [
                                    'name' => $control['name'],
                                    'list' => isset($control['list']) ? $control['list'] : null,
                                    'value' => $control['value'],
                                    'select2' => isset($control['select2']) ? $control['select2'] : false,
                                    'ajax' => isset($control['ajax']) ? $control['ajax'] : false,
                                    'class' => isset($control['class']) ? $control['class'] : 'tp-tag-form-control',
                                    'attributes' => isset($control['attributes']) ? $control['attributes'] : null,
                                    'id' => isset($control['id']) ? $control['id'] : false
                                ])
                            @elseif ($control['type'] == 'input')
                                @include('partials.fields.input-only', [
                                    'name' => $control['name'],
                                    'value' => $control['value'],
                                    'class' => isset($control['class']) ? $control['class'] : 'tp-tag-form-control',
                                    'id' => isset($control['id']) ? $control['id'] : false,
                                ])
                            @endif
                        </div>
                    @endforeach
                    <button type="button" class="btn btn-success add_to_tags_btn">Add</button>
                </div>
            </div>
        </div>
    @endif

</div>