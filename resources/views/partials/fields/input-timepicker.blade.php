@if(isset($form_line))
    <div class="form-group form-md-line-input {{ $errors->get($name) ? 'has-error' : '' }} {{ isset($column) ? 'col-md-' . $column : '' }}">
@else
    <div class="{{ isset($column) ? 'col-md-' . $column : '' }} {{ isset($offset) ? 'col-md-offset-' . $offset : ''}}">
@endif

<label class="col-md-{{ isset($column_label) ? $column_label : 2 }} control-label {{ isset($label_align_left) ? 'label-align-left' : ''}}"
       for="form_control_{{ snake_case($name) }} ">
    {{ isset($required) ? '*' : '' }}{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}
</label>

@if(isset($range))
    <div class="col-md-{{ isset($column_label) ? 12 - $column_label : 9 }}">
        <div class="input-group input-large date-picker input-daterange"
             data-date-format="mm/dd/yyyy" {{ isset($class) ? implode(" ", (array) $class) : '' }}>
            <span class="input-group-addon"> from </span>
            <input type="text" class="form-control" id="{{ $id_to or '' }}"
                   name="{{ snake_case($name_from) }}"
                   value="{{ old(snake_case($name_from), isset($value_from) ? $value_from : '') }}"
                    {!! isset($required) ? 'required="required"' : '' !!}>
            <span class="input-group-addon"> to </span>
            <input type="text" class="form-control" id="{{ $id_from or '' }}"
                   name="{{ snake_case($name_to) }}"
                   value="{{ old(snake_case($name_to), isset($value_to) ? $value_to : '') }}"
                    {!! isset($required) ? 'required="required"' : '' !!}>
        </div>
    </div>
@else
    <div class="col-md-{{ isset($column_label) ? 12 - $column_label : 9 }}">
        <div class="input-group">
            @if (isset($addon))
                <span class="input-group-addon">
                    <i class="fa {{ $addon }} fa-fw"></i>
                    {{ isset($label_addon) ? $label_addon : ''}}
                </span>
            @endif
            <input type="text"
                   class="form-control {{ isset($class) ? implode(" ", (array) $class) : '' }} {{ isset($date) ? 'date-picker' : '' }} {{ isset($time) ? 'timepicker' : '' }} {{ isset($datetime) ? 'datetimepicker' : '' }}"
                   id="{{ $id or '' }}"
                   name="{{ snake_case($name) }}"
                   value="{{ old(snake_case($name), isset($value) ? $value : '') }}"
                    {!! isset($required) ? 'required="required"' : '' !!}
                    {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
                    {!! isset($placeholder) ?  'placeholder=" '. $placeholder .'"' : '' !!}
            >
            <span class="input-group-btn">
                <button class="btn default date-set" type="button"><i class="fa fa-clock-o"></i></button>
            </span>
        </div>
    </div>
@endif

</div>
