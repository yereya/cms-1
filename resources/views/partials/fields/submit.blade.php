<div class="form-actions">
    <div class="row">
        <div class="col-md-offset-2 col-md-10">
            <input type="submit"
                   value="Submit"
                   {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
                   class="btn blue {{ isset($class) ? implode(" ", (array) $class) : '' }}"
            >
        </div>
    </div>
</div>