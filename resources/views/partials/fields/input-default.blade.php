@if (isset($addon))
    <div class="input-group">
    <span class="input-group-btn"><button class="btn default">{{ $addon }}</button></span>
            @endif

<input type="{{ $type or 'text' }}"
       name="{{ snake_case($name) }}"
       id="{{ isset($id) ? $id : snake_case($name) }}"
       class="form-control {{isset($minicolors) ? "minicolors minicolors-input" : "" }} {{ isset($class) ? implode(" ", (array) $class) : '' }}"
       {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
       value="{{ old(snake_case($name), isset($value) ? $value : '') }}"
        {!! isset($required) && $required ? 'required="required"' : '' !!}
        {!! isset($disabled) && $disabled ? 'disabled="disabled"' : '' !!}
        {!! isset($placeholder) ?  'placeholder=" '. $placeholder .'"' : '' !!}
        {!! isset($pattern) && $pattern ? 'pattern="' . $pattern . '"' : '' !!}
        {{-- Adds datalist list--}}
        {!! isset($suggestions) ? 'list="list_' . snake_case($name) . '"' : '' !!}
        {!! isset($step) ? 'step="0.01"' : ''!!}
       data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}"
>

@if (isset($group) && $group)
    <span class="input-group-addon">%</span>
    </div>
@endif

@if (isset($addon_right))
    <span class="input-group-btn"><button class="btn blue" type="button">{{ $addon_right }}</button></span>
@endif

@if (isset($addon_right_href) && $addon_right_href)
    <span class="input-group-btn"><a class="btn grey-cascade" target="_blank" href="//{{ $addon_right_href }}"><i class="fa fa-link"></i></a></span>
@endif

@if (isset($addon))
    </div>
@endif

{{-- Adds datalist list--}}
@if(isset($suggestions))
    @include('partials.fields.extras.input-data-list', [
        'name' => $name,
        'suggestions' => $suggestions
    ])
@endif
{{-- End Adds datalist list--}}