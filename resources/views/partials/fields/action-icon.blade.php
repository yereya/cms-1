<a href="{{ $url or "javascript:;" }}" {!! isset($attributes) ? HTML::attributes($attributes) : '' !!} >
    <span aria-hidden="true" class="{{ $icon }} {{ isset($tooltip) ? 'tooltips' : ''}}" title="{{ $tooltip or '' }}"></span>
</a>