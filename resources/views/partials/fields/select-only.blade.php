<select class="form-control {{ isset($select2) && $select2 ? 'select2' : '' }} {{ isset($multi_select) ? 'select2-multiple' : '' }} {{ isset($ajax) && $ajax ? 'select2-ajax' : '' }} {{ isset($class) ? $class : '' }}"
            {!! isset($required) ? 'required="required"' : '' !!}
            {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
            @if ($id) id="{{ $id }}" @endif
            data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}"
    >
        @if(isset($list))
            @foreach(is_a($list, \Illuminate\Support\Collection::class) ? $list->toArray() : $list as $key => $item)
                <?php
                $_id = $key;
                $_attributes = '';
                if (is_array($item)) {
                    $_id = $item['id'];
                    $_attributes = HTML::attributes($item);
                }
                ?>
                <option value="{{ $_id }}"
                        {!! $_attributes !!}
                        @if(isset($value))
                        @if(
                            (is_array($value) && in_array($_id, $value))
                            ||
                            (is_a($value, \Illuminate\Support\Collection::class) && $value->find($_id))
                            ||
                            ((is_string($value) || is_int($value)) && $_id == $value)
                        )
                        selected="selected"
                        @endif
                        @endif
                >{{ is_array($item) ? $item['text'] : $item }}</option>
            @endforeach
        @endif
    </select>