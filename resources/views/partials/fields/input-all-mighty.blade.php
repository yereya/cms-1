<?php
$id = isset($id) && $id ? $id : $name;
?>
<div id="{{$id}}"></div>
<script>
    $(document).ready(function () {
        $('#<?php echo $id?>').magicSuggest({
            @if(isset($ajax) && $ajax) //data source is from a remote endpoint
            data: '{{$list}}',
            @else // data source is a given array
            data: <?php echo json_encode($list) ?>,
            @endif
            @if(isset($value) && !empty($value)) // an array of default values is set
            value: <?php echo json_encode($value) ?>,
            @endif
            @if(isset($value_field) && $value_field) //which key is default value relates when data structure is JSON
            valueField: '{{$value_field}}',
            @endif
            name:'{{$name}}', //generated inputs are item in a form array, define the array's name
            expandOnFocus: true, // open the options list on focus
            strictSuggest: true, // suggest by words instead of case (regex)
            selectFirst: true, // mark the first option as selected
            ajaxConfig: {method:'GET'},
            displayField:'{{isset($display_field) ? $display_field : 'text'}}',//for json data structure, which key
            // is for display
            allowFreeEntries: {{ isset($allow_custom_values) && $allow_custom_values ? 'true' : 'false' }} //allow free input
        });
    });
</script>