<div class="rulers">
        <div id="mouse-x-pos"></div>
        <div id="mouse-y-pos"></div>

        <div class="ruler-x">
                @for ($i = 0; $i <= 20; $i++)
                        <label>{{$i * 100}}</label>
                @endfor
        </div>

        <div class="ruler-y">
                @for ($i = 0; $i <= 100; $i++)
                        <label>{{$i * 100}}</label>
                @endfor
        </div>
</div>