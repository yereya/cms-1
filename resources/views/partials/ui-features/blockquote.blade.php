<blockquote>
    <small>{{ $title or '' }}
        <cite title="Source Title">{{ $source_title or '' }}</cite>
    </small>
    <p> {{ $text }} </p>
</blockquote>