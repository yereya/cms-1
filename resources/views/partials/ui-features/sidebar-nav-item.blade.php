<li class="nav-item {{ isset($active) ? 'active open' : '' }}">
    <a href="{{ $navigation['route'] ? route($navigation['route'], $navigation['route_params'] ?? []) : 'javascript:;' }}" class="nav-link nav-toggle">
        <i class="{{ $navigation['menu_icon'] ?? 'icon-feed'}}"></i>
        <span class="title">{{ $navigation['name'] }}</span>
        @if (isset($navigation['active']))
            <span class="selected"></span>
        @endif
        {{--<span class="arrow open"></span>--}}
    </a>
    <ul class="sub-menu">
        @if($navigation['children'] ?? false)
            @foreach($navigation['children'] as $navigation)
                @include('partials.ui-features.sidebar-nav-item', [
                    'navigation' => $navigation,
                ])
            @endforeach
        @endif
    </ul>
</li>