@if($part == 'open')
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <button type="button" class="expand-modal"><i class="glyphicon glyphicon-resize-full"> </i></button>
        <h4 class="modal-title">{{ $title or '' }}</h4>
    </div>
    <div class="modal-body {{ isset($ajax_submit) && $ajax_submit ? 'ajax-submit' : '' }} {{ isset($event) && $event ? "triggers-event" : '' }}"
        {{ isset($event) && $event ? "data-event=$event" : '' }}
        {{ isset($success) && $success ? "data-success=$success" : '' }}
        {{ isset($error) && $error ? "data-error=$error" : '' }}>
@endif

@if($part == 'close')
    </div>

    {{-- FOOTER BUTTONS --}}
    <div class="modal-footer">
        @if(isset($buttons['close']))
            @include('partials.fields.button', [
                'class' => ['btn-default'],
                'value' => 'Close',
                'type'  => 'button',
                'attributes' => [
                    'data-dismiss' => 'modal'
                ]
            ])
        @endif

        @if(isset($buttons['delete']))
            @include('partials.fields.button', [
                'class' => ['btn-default'],
                'value' => 'Delete',
                'type'  => 'button',
                'attributes' => [
                    'data-dismiss' => 'modal',
                    'onclick' => '$(this).parent().find("form.delete_item").submit();'
                ]
            ])
            @include('partials.fields.extras.hidden-delete-form', ['route_url' => $buttons['delete']['route']])
        @endif

        @if(isset($buttons['save']))
            @include('partials.fields.button', [
                'class' => ['btn-primary', 'submit-button'],
                'value' => 'Save changes',
                'attributes' => $buttons['save']['attributes'] ?? null,

            ])
        @endif

        @if(isset($buttons['save_add']))
            @include('partials.fields.button', [
                'type' => 'button',
                'class' => ['btn-primary', 'save-add-widget-data'],
                'value' => 'Save and Add',
                'attributes' => $buttons['save_add']['attributes'] ?? null
            ])
        @endif

        @if(isset($buttons['custom']))
            @foreach($buttons['custom'] as $custom_button)
                @include('partials.fields.button',$custom_button)
            @endforeach
        @endif
    </div>
    {{-- END FOOTER BUTTONS --}}

    @if(isset($js))
        <script type="application/javascript">

            {{-- Init datatables, and allow to pass extra params --}}
            @if(isset($js['datatable']))
                (function(){
                    App.initDatatable($('#ajax-modal table'), {
                        <?php
                        $params = [
                            "scrollY" => "800px",
                            "scrollCollapse" => true,
                            "paging" => false,
                        ];
                        if (is_array($js['datatable'])) {
                            $params = array_merge($params, $js['datatable']);
                        }
                        ?>
                        @foreach($params as $key => $val)
                            "{{$key}}": "{{$val}}",
                        @endforeach
                    });
                })();
            @endif

            {{-- Activates tooltips inside the ajax-modal --}}
            @if(isset($js['tooltips']))
                (function(){
                    $('#ajax-modal .tooltips').tooltip();
                })();
            @endif

            {{-- Used to pass certain form attributes --}}
            @if(isset($js['fetch_form']))
                (function(){
                    var filterFormElement = $('#{{ $js['fetch_form'] }}');
                    var formValues = JSON.stringify(filterFormElement.serializeArray());
                    $('#ajax-modal input#fetch_form').val(formValues);
                })();
            @endif

            {{-- Used to activate date selection fields --}}
            @if(isset($js['date_time_picker']))
                (function(){
                    App.initDatePickers();
                })();
            @endif

            {{-- Use to activate select fields --}}
            @if(isset($js['select2']))
                (function(){
                    App.initSelect2($('#ajax-modal .select2'));
                })();
            @endif

            {{-- Sexy forms with jQuery http://opensource.audith.org/uniform/ --}}
            @if(isset($js['uniform']))
                (function(){
                    App.initUniform($('#ajax-modal input:checkbox, #ajax-modal input:radio'));
                })();
            @endif

            {{-- Used to activate selecte2 for select fields   --}}
            @if(isset($js['select2_to_selects']))
                (function(){
                    App.initSelect2ToSelects();
                })();
            @endif

            {{-- Controll modal width --}}
            @if(isset($js['modal_width']))
                (function(){
                    App.ajaxModalWidth({{ $js['modal_width'] }});
                })();
            @endif

            @if(isset($js['code_mirror']))
                (function(){
                    App.codeMirrorInit();
                })();
            @endif

            var isValidationRequired = $('textarea[data-validate]').length;

            {{--Init html validation on modal open  --}}
            if(isValidationRequired){
                require(['htmlValidator'],function (htmlValidator) {
                    htmlValidator.init();htmlValidator.init();
                });
            }
        </script>
    @endif
@endif


