@if($part == 'open')
    <style>
        section .section-body {
            {{ $body_style or '' }}
        }
    </style>
    <section class="{{ $class or '' }} {{ (isset($collapsed) and $collapsed) ? 'collapsed' : '' }}" {{ isset($section_attr) ? HTML::attributes($section_attr) : '' }}>

        <div class="section-header">
            <div class="caption">
                <span aria-hidden="true" class="icon-notebook"></span>
                <span class="caption-subject font-dark bold uppercase">{{ $title }}</span>
            </div>
            <div class="actions">
                <a class="btn btn-circle btn-icon-only btn-default action-collapse" href="javascript:;">
                    <i class="icon-control-play" style="transform: rotate(90deg); margin-top: 5px;"></i>
                </a>
            </div>
        </div>
        <div class="section-body">
@endif

@if($part == 'close')
        </div>
    </section>
@endif
