<table
        id="{!! isset($name) ? 'table_' . snake_case($name) . '' : '' !!}"
        class="table table-striped table-bordered table-hover table-advance">
    <thead>
    <tr>
        @foreach($columns as $column)
            @if(!isset($column['permission']) || (isset($column['permission']) && $column['permission'] != false))
                <th>
                    @if(isset($column['label']))
                        {{ ucfirst($column['label']) }}
                    @else
                        {{ ucfirst(str_replace(['-', '_'], ' ', (strtolower( isset($column['key']) ? $column['key'] : $column )))) }}
                    @endif
                </th>
            @endif
        @endforeach
    </tr>
    </thead>

    <tbody>
    <?php $params = isset($params) ? $params : [] ?>
    @foreach($data as $row)
        <tr>
            @foreach($columns as $column)
                @if(!isset($column['permission']) || (isset($column['permission']) && $column['permission'] != false))
                    <td>
                        @if(isset($column['value']))
                            {!! call_user_func_array($column['value'], [$row] + $params) !!}
                        @elseif(isset($column['key']))
                            @if(is_array($row))
                                {{ $row[$column['key']] }}
                            @else
                                {{ $row->{$column['key']} }}
                            @endif
                        @else
                            @if(is_array($row))
                                {{ $row->$column }}
                            @else
                                {{ $row[$column] }}
                            @endif
                        @endif
                    </td>
                @endif
            @endforeach
        </tr>
    @endforeach

    @if(!$data)
        <tr>
            @foreach($columns as $column)
                @if(!isset($column['permission']) || (isset($column['permission']) && $column['permission'] != false))
                    <td>
                        @if(isset($column['value']))
                            {!! call_user_func_array($column['value'], $params) !!}
                        @endif
                    </td>
                @endif
            @endforeach
        </tr>
    @endif
    </tbody>
</table>