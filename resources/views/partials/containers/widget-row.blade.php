<div class="row widget-row">
    @if(!empty($data))
        @foreach($data as $widget)
            <div class="col-md-{{$column_size ?? 4}} {{$class ?? ''}}">
                @if(!empty($widget['attributes']['clickable']))
                  <a href="#">
                @endif
                    <!-- BEGIN WIDGET THUMB -->
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered"
                         data-filter="{{$widget['attributes']['data-filter'] ?? ''}}"
                         data-group={{$widget['attributes']['data-group'] ?? ''}}>
                        @if(!empty($widget['title']))
                            <h4 class="widget-thumb-heading">{{$widget['title'] ?? 'default Title' }}</h4>
                        @endif
                        <div class="widget-thumb-wrap">

                            @if(!empty($widget['icon']))
                                <i class="widget-thumb-icon bg-{{$widget['icon']['bg'] ?? 'purple'}}
                                {{$widget['icon']['type'] ?? 'icon-bulb'}}">
                                </i>
                            @endif

                            <div class="widget-thumb-body {{$widget['class'] ?? ''}}">
                                @if(isset($widget['subtitle']))
                                    <span class="widget-thumb-subtitle {{$widget['subtitle']['color'] ?? ''}} text-center">
                                    @if((!empty($widget["subtitle"]['length'])))
                                            <span class="length"></span>
                                        @endif
                                        <span class="text">{{$widget['subtitle']['value'] ?? ''}}</span>
                                    </span>
                                @endif
                                <span class="widget-thumb-body-stat text-center">
                                    @if(!empty($widget['content']['length']))
                                        <span class="length"></span>
                                    @endif
                                    <span class="text">{{$widget['content']['value']??''}} $<span class="sum"></span></span>

                            </div>
                        </div>
                    </div>
                    <!-- END WIDGET THUMB -->
               @if(!empty($widget['attributes']['clickable']))
                  </a>
              @endif
            </div>
        @endforeach
    @endif
</div>