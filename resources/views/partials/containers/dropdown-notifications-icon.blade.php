<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
   data-close-others="true">
    <i class="icon-bell"></i>
    <span class="badge badge-default"> {!! $notifications_unread !!} </span>
</a>