<div class="form-actions">
    <div class="row">
        <div class="col-md-10 {{ isset($offset) ? 'col-md-offset-' . $offset : 'col-md-offset-2' }}">

            @if(isset($buttons['cancel']))
                @include('partials.fields.button', [
                    'type' => 'link',
                    'url' => $buttons['cancel']['route'],
                    'value' => 'Cancel'
                ])
            @endif

            @if(isset($buttons['submit']))
                @include('partials.fields.button', [
                    'type' => 'submit',
                    'value' => 'Submit'
                ])
            @endif

            @if(isset($buttons['custom']))
                @foreach($buttons['custom'] as $button)
                    @include('partials.fields.button',
                        [
                            'type' => isset($button['type']) ? $button['type'] : '',
                            'value' => isset($button['value']) ? $button['value'] : 'Save',
                            'class' => isset($button['class']) ? $button['class'] : 'btn-default',
                            'url' => isset($button['url']) ? $button['url'] : null,
                            'attributes' => isset($button['attributes']) ? $button['attributes'] : null,
                            'icon' => isset($button['icon']) ? $button['icon'] : null
                        ]
                    )
                @endforeach
            @endif

            @if(isset($checkboxes['create_another']))
                @include('partials.fields.checkbox', [
                    'display' => 'inline',
                    'list' => [
                        [
                            'label' => 'Create another',
                            'name' => 'create_another'
                        ]
                    ]
                ])
            @endif
        </div>
    </div>
</div>

