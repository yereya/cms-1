@if(isset($side))
    <div class="actions pull-{{ $side }}">
@endif

    <div class="btn-group">
        <a href="" class="btn btn-sm purple dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
           data-close-others="true"> {{ $dropdown_buttons['label'] or 'Actions' }}
            <span class="fa fa-angle-down"> </span>
        </a>
        <ul class="dropdown-menu pull-right">
            @foreach($dropdown_buttons as $button)
                @if(isset($button['divider']))
                    <li class="divider"></li>
                @elseif(is_array($button))
                    <li>
                        <a
                                href="{{ $button['href'] or 'javascript:;' }}"
                                {!! isset($button['attributes']) ? HTML::attributes($button['attributes']) : '' !!}
                        ><i class='{{ $button['icon'] or 'fa fa-square'}}'></i> {!!  $button['title'] !!}</a>
                    </li>
                @endif
            @endforeach
        </ul>
    </div>

@if(isset($side))
    </div>
@endif
