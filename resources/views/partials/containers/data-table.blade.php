<table
        id="{{ $id or '' }}"
        {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
        {!! isset($route) ? 'data-route_datatable="' . route($route, $route_params ?? null) . '"' : '' !!}
        {!! isset($url) ? 'data-route_datatable="' . url($url) . '"' : '' !!}
        class="table data-table table-striped table-bordered table-hover table-checkable order-column
        {{ isset($class) ? implode(" ", (array) $class) : '' }}
        {{ isset($server_side) && $server_side ? 'tp-server-side-datatable' : '' }}"
>
    <thead>
        <tr>
            @foreach(isset($columns) ? $columns : array_keys($data[endKey($data)]) as $column)
                @if(!isset($column['permission']) || (isset($column['permission']) && $column['permission'] != false))
                    @if(isset($column['label']))
                        <th {!! isset($column['attributes']) ? HTML::attributes($column['attributes']) : '' !!}>
                            {!! ucfirst($column['label']) !!}
                        </th>
                    @elseif (isset($columns))
                        <th>
                            {!! ucfirst(str_replace(['-', '_'], ' ', (strtolower( isset($column['key']) ? $column['key'] : $column )))) !!}
                        </th>
                    @else
                        <th>
                            {{ ucfirst(str_replace(['-', '_'], ' ', (strtolower( $column )))) }}
                        </th>
                    @endif
                @endif
            @endforeach
        </tr>
    </thead>

    @if(isset($data) && count($data) > 0)
        <tbody>
        <?php $params = isset($params) ? $params : [] ?>
        @foreach($data as $row)
            <tr>
                @foreach(isset($columns) ? $columns : $row as $column)
                    @if(!isset($column['permission']) || (isset($column['permission']) && $column['permission'] != false))
                        <td @if(isset($column['width'])) width="{{ $column['width'] }} @endif">
                            @if (isset($columns))
                                @if(isset($column['value']))
                                    {!! call_user_func_array($column['value'], [$row] + $params) !!}
                                @elseif(isset($column['key']))
                                    @if(is_array($row))
                                        {{ $row[$column['key']] }}
                                    @else
                                        {{ $row->{$column['key']} }}
                                    @endif
                                @else
                                    @if(is_array($row))
                                        {{ $row[$column] }}
                                    @else
                                        {{ $row->$column }}
                                    @endif
                                @endif
                            @else
                                {{ $column }}
                            @endif
                        </td>
                    @endif
                @endforeach
            </tr>
        @endforeach
        </tbody>
    @endif

</table>