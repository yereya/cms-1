<?php
if (!is_null($field->metadata) && is_string($field->metadata)) {
    $metadata = json_decode($field->metadata);
}

$data_metadata = [];
if (!is_null($data) && is_string($data)) {
    $data_metadata = json_decode($data);
}
?>

<div class="col-md-offset-2">
    @include('partials.fields.h4-separator', ['label' => $field->display_name])
    <div class="column">
        @if(!empty($metadata->checkbox))
            @foreach($metadata->checkbox as $row)
                @include('partials.fields.checkbox', [
                    'label' => $row->key,
                    'wrapper' => true,
                    'type' => 'checkbox',
                    'list' => [
                        [
                            'name' => $field->name . '[' . $row->key . ']',
                            'value' => isset($row->value) ? $row->value : 0,
                            'checked' => empty($data_metadata->{$row->key}) ? 0 : 1,
                        ],
                    ],
                ])
            @endforeach
        @endif

    </div>
</div>
