@php
    $brands = [];
    $connected = [];

    if (!is_null($field->metadata) && is_string($field->metadata)) {
        $metadata = json_decode($field->metadata);

        $key = $linked_posts->get($metadata->content_type_id);

        if (isset($key)) {
            $brands = $linked_posts->get($metadata->content_type_id)->pluck('name', 'id');
        }
    }

    if (!empty($linked_to_post[$field->name])) {
        $connected = json_decode($linked_to_post[$field->name]);
    }
@endphp

@include('partials.fields.select', [
   'name' => $field->name,
   'label' => $field->display_name,
   'multi_select' => true,
   'taginput' => true,
   'list' => sortSelectByList($brands, $connected),
   'value' => $connected
])

