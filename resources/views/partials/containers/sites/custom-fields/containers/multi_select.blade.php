<?php
$select = [];
$stored = [];

if (!is_null($field->metadata) && is_string($field->metadata)) {
    $metadata = json_decode($field->metadata);

    if (isset($metadata->multi_select)) {
        foreach($metadata->multi_select as $option) {
            $select[] = ['id' => $option->priority, 'text' => $option->value] ;
        }
    }
}

if (is_string($data) && !is_null($data)) {
    $stored = json_decode($data);
}
?>

<div class="col-md-offset-2">
    @include('partials.fields.h4-separator', ['label' => $field->display_name])

    <div class="column">
        @if(!empty($select))
            @include('partials.fields.select', [
                 'name' => $field->name,
                 'label' => 'Options',
                 'list' => $select,
                 'multi_select' => true,
                 'value' => $stored ?? '',
            ])
        @endif

    </div>
</div>
