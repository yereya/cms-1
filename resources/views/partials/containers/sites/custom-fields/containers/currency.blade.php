<?php
if (!is_null($value) && is_string($value)) {
    $data = json_decode($value);

    if ($data) {
        $deposit = $data->deposit ?? null;
        $currency = $data->currency ?? '$';
    }
}
?>

<div class="col-md-offset-2">
    <div class="row">
        <div class="form-group col-md-6">
            @include('partials.fields.input', [
                'label' => $field->display_name,
                'name' => $field->name . '[deposit]',
                'type' => 'number',
                'value' => $deposit ?? 0,
                'step' => '0.01'
            ])
        </div>

        <div class="form-group col-md-4">
            @include('partials.fields.select', [
                'name' => $field->name . '[currency]',
                'label' => '',
                'list' => [
                    ['id' => 'USD', 'text' => '$'],
                    ['id' => 'GBP', 'text' => '£'],
                    ['id' => 'EUR', 'text' => '€']
                ],
                'value' => $currency ?? '$',
            ])
        </div>
    </div>

</div>