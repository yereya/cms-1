@include('partials.containers.portlet', [
    'part'      => 'open',
    'title'     => 'SEO Settings',
    'has_form'  => true,
    'actions' => [
        [
            'target' =>  '',
            'title' =>  'Back to List' ,
            'class' =>  'btn btn-default',
            'icon' => 'fa fa-cancel'
        ],
        [
            'type' => 'submit'  ,
            'title' =>  'Save' ,
            'class' =>  'btn btn-primary',
            'icon' => 'fa fa-save'
        ],
    ]
])

{{ Form::hidden('og_type', $seo_settings->og_type ?? '') }}

<div class="form-body">
    @include('partials.fields.input', [
        'label' => 'Title',
        'name' => 'seo_post_title',
        'value' => $seo_settings->seo_post_title ?? '',
        'attributes' => [
          'data-char-limit' => 60,
          'maxlength'=>255
        ]
    ])

    @include('partials.fields.textarea', [
        'label' => 'Description',
        'name' => 'description',
        'value' => $seo_settings->description ?? '',
        'attributes' => [
          'data-char-limit' => 160,
          'maxlength'=>2200
        ]
    ])

    @include('partials.fields.textarea', [
        'label' => 'Keywords',
        'name' => 'keywords',
        'value' => $seo_settings->keywords ?? ''
    ])

    @include('partials.fields.input', [
        'label' => 'og:title',
        'name' => 'og_title',
        'value' => $seo_settings->og_title ?? ''
    ])

    @include('partials.fields.input', [
        'label' => 'og:url',
        'name' => 'og_url',
        'value' => $seo_settings->og_url ?? ''
    ])

    @include('partials.fields.input', [
        'label' => 'og:image',
        'name' => 'og_image',
        'value' => $seo_settings->og_image ?? ''
    ])

    @include('partials.fields.input', [
        'label' => 'og:description',
        'name' => 'og_description',
        'value' => $seo_settings->og_description ?? ''
    ])

    @include('partials.fields.input', [
        'label' => 'Robots Custom',
        'name' => 'robots_custom',
        'value' => $seo_settings->robots_custom ?? ''
    ])

    @include('partials.fields.input', [
        'label' => 'Permalink',
        'name' => 'permalink',
        'value' => $seo_settings->permalink ?? ''
    ])

    @include('partials.fields.input', [
        'label' => 'Canonical URL',
        'name' => 'canonical_url',
        'value' => $seo_settings->canonical_url ?? ''
    ])

    <div class="form-group form-md-line-input">
        <label class="control-label pull-left col-md-2">Show on site map</label>
        @include('partials.fields.radio', [
            'column' => 2,
            'radio_id' => 'show_on_sitmap_exclude',
            'label' => 'Exclude',
            'value' => 0,
            'name' => 'show_on_sitemap',
            'checked' => (isset($seo_settings) && $seo_settings->show_on_sitemap == 0)
            || !isset($seo_settings->show_on_sitemap)
        ])
        @include('partials.fields.radio', [
            'column' => 1,
            'radio_id' => 'show_on_sitmap_include',
            'label' => 'Include',
            'value' => 1,
            'name' => 'show_on_sitemap',
            'checked' => (isset($seo_settings) && $seo_settings->show_on_sitemap == 1)
        ])
    </div>

    <div class="form-group form-md-line-input">
        <label class="control-label pull-left col-md-2">Robots index</label>
        @include('partials.fields.radio', [
            'column' => 2,
            'radio_id' => 'robots_no_index',
            'label' => 'No index',
            'value' => 0,
            'name' => 'robots_index',
            'checked' => (isset($seo_settings) && $seo_settings->robots_index == 0)
            || !isset($seo_settings->robots_index)
        ])
        @include('partials.fields.radio', [
           'column' => 1,
           'radio_id' => 'robots_index',
           'label' => 'Index',
           'value' => 1,
           'name' => 'robots_index',
           'checked' => (isset($seo_settings) && $seo_settings->robots_index == 1)
       ])
    </div>

    <div class="form-group form-md-line-input">
        <label class="control-label pull-left col-md-2">Follow/No follow</label>
        @include('partials.fields.radio', [
            'column' => 2,
            'radio_id' => 'robots_no_follow',
            'label' => 'No follow',
            'value' => 0,
            'name' => 'robots_follow',
            'checked' => (isset($seo_settings) && $seo_settings->robots_follow == 0)
             || !isset($seo_settings->robots_follow )
        ])
        @include('partials.fields.radio', [
            'column' => 1,
            'radio_id' => 'robots_follow',
            'label' => 'Follow',
            'value' => 1,
            'name' => 'robots_follow',
            'checked' => (isset($seo_settings) && $seo_settings->robots_follow == 1)
        ])
    </div>
</div>

@include('partials.containers.portlet', [
    'part' => 'close'
])