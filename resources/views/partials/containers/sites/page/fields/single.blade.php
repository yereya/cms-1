@include('partials.fields.input', [
    'label' => 'Slug',
    'name' => 'slug',
    'required' => true,
    'disabled' => true,
    'addon' => $slug,
    'value' => '*'
])
@include('partials.fields.select', [
    'label' => 'Template',
    'name' => 'template_id',
    'value' => isset($post) ? $post->content->template_id : null,
    'list' => $templates
])

<script>
    App.initSelect2ToSelects();
</script>