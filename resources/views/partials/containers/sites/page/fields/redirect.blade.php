@include('partials.fields.select', [
    'label' => 'Redirect Type',
    'name' => 'url_type',
    'value' => $post->content->url_type ?? null,
    'list' => [
        ['id' => 301, 'text' => '301 Moved Permanently'],
        ['id' => 302, 'text' => '302 Found'],
        ['id' => 307, 'text' => '307 Temporary Redirect'],
        ['id' => 410, 'text' => '410 Gone'],
    ]
])

@include('partials.fields.select', [
    'label' => 'Redirect Page',
    'name' => 'redirect_page_short_code',
    'value' => $post->content->redirect_page_short_code ?? null,
    'list' => $pages
])

@include('partials.fields.input', [
    'label' => 'Redirect URL',
    'name' => 'url_redirect',
    'value' => $post->content->url_redirect ?? null,
    'pattern' => "(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})"
])

@include('partials.fields.checkbox', [
        'label' => 'URL query params',
        'type' => 'checkbox',
        'wrapper' => true,
        'display' => 'inline',
        'list' => [
             [
                  'name' => 'url_query',
                  'checked' => $post->content->url_query ?? 0
            ],
        ]
    ])

<script>
    (function () {
        topApp.sanitized();
        App.initSelect2ToSelects();
    })();
</script>