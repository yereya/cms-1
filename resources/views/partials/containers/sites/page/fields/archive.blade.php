@include('partials.fields.select', [
    'label' => 'Template Desktop',
    'name' => 'template_id',
    'value' => $post->page->template_id ?? null,
    'list' => $templates
])

@include('partials.fields.select', [
     'label' => 'Template Mobile',
     'name' => 'template_mobile_id',
     'value' => $post->page->template_mobile_id ?? null,
     'list' => $templates
])

<div> {{-- duplicate must have contatiner to works  --}}

<?php
    if (empty($templates_archive) || $templates_archive->count() === 0){
        $templates_archive->push([]);
    }
?>

@foreach($templates_archive as $index => $content)
    <div class="form-group form-md-line-input">
        <label class="col-md-2 control-label">
          Single
        </label>
        <div class="col-md-9">

        @include('partials.containers.duplicate-group', ['part' => 'open', 'offset' => 0])
            <div class="col-md-11">

        @include('partials.fields.select', [
            'label' => 'Desktop Template',
            'required' => true,
            'name' => 'archive[' . $index . '][template_desktop_id]',
            'value' => $content->template_id ?? -1,
            'list' => $templates,
            'column' => 6,
            'column_input' => 12,
            'column_label' => 12,
            'form_line' => true
        ])

        @include('partials.fields.select', [
            'label' => 'Content Type',
            'required' => true,
            'name' => 'archive[' . $index . '][content_type_id]',
            'value' => $content->content_type_id ?? null,
            'list' => $content_types->pluck('name', 'id'),
            'column' => 6,
            'column_input' => 12,
            'column_label' => 12,
            'form_line' => true
        ])

        @include('partials.fields.select', [
           'label' => 'Mobile Template',
           'name' => 'archive[' . $index . '][template_mobile]',
           'value' => $content->template_mobile_id ?? -1,
           'list' => $templates,
           'column' => 6,
           'column_input' => 12,
           'column_label' => 12,
           'form_line' => true
         ])

         @include('partials.fields.select', [
           'label' => 'Label',
           'name' => 'archive[' . $index . '][label]',
           'value' => $content->label_id ?? -1,
           'list' => $labels,
           'column' => 6,
           'column_input' => 12,
           'column_label' => 12,
           'form_line' => true
       ])

            </div>
            <div class="col-md-1">
                <div class="buttons-wrapper">
                    @include('partials.containers.actions', [
                        'column' => 6,
                        'class' => [
                            'type' => 'form-group form-md-line-input tb-actions-duplicate-group',
                        ],
                        'actions' => [
                            [
                                'action' => 'clone',
                            ],
                            [
                                'action' => 'remove',
                            ]
                        ]
                    ])
                </div>
            </div>
        @include('partials.containers.duplicate-group', ['part' => 'close', 'offset' => true])
        </div>
    </div>
    @endforeach
</div>

<script>
    (function () {
                topApp.sanitized();
                App.initSelect2ToSelects();
                App.initDuplicateGroup();
            }
    )();
</script>