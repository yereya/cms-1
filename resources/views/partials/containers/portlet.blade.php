@if($part == 'open')

    {{-- PORTLET BODY --}}
    <div id="{{ $id or '' }}"
         class="portlet light bordered {{ isset($sortable) ? ' portlet-sortable' : ''}} {{ isset($class) ? implode(' ', $class) : '' }}">
        @if($title)
            <div class="portlet-title tabbable-line {{ isset($sortable) ? ' ui-sortable-handle' : ''}}">
                <div class="caption">
                    <i aria-hidden="true" class="{!! $title_icon or 'icon-notebook' !!}"></i>
                    <span class="caption-subject font-dark bold uppercase">{{ $title }}</span>
                    @if(isset($subtitle) && !is_null($subtitle))
                        <span class="caption-helper">{{ $subtitle }}</span>
                    @endif
                </div>
                @if (isset($save_changes_button))
                    <button class="btn btn-primary pull-right">Save Changes</button>
                @endif
                @if(isset($tools))
                    <div class="tools">
                        @foreach($tools as $tool)
                            <a href="javascript:;" class="{{ $tool['class'] }}" data-original-title="" title=""></a>
                        @endforeach
                    </div>
                @endif

                @if(isset($actions))
                    <div class="actions {{ $actions_class ?? '' }}">
                        @foreach($actions as $action)
                            @if($action AND (!isset($action['permission']) OR (isset($action['permission']) AND $action['permission']))  )
                                @if(isset($action['type']) && $action['type'] == 'submit')
                                    <button type="submit"
                                            class="btn btn-circle {{ isset($action['class']) ? $action['class'] : '' }}"
                                            {!! isset($action['attributes']) ? HTML::attributes($action['attributes']) : null !!}> {{ $action['title'] }}
                                        <i class="{{ $action['icon'] }}"></i>
                                    </button>
                                @elseif (isset($action['drop_down']))
                                    <div class="btn-group">
                                        <a class="btn yellow btn-circle" href="javascript:;" data-toggle="dropdown"
                                           aria-expanded="false">
                                            <i class="{{ isset($action['icon']) ? $action['icon'] : '' }}"></i>
                                            <span class="hidden-xs">{{ $action['title'] }}</span>
                                            <i class="fa fa-angle-down"></i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            @foreach($action['menus'] as $menu)
                                                @if (!isset($menu['permissions']) || (isset($action['permission']) AND $action['permission']))
                                                    <li>
                                                        @if (isset($menu['container']))
                                                            @include($menu['context']['include'], [
                                                                'route' => $menu['context']['route'],
                                                                'title' => $menu['context']['title']
                                                            ])
                                                        @else
                                                            <a href="{{ (isset($menu['route'])) ? $menu['route'] : 'javascript:;'}}"
                                                                    {!! isset($menu['attributes']) ? HTML::attributes($menu['attributes']) : null !!}
                                                            >{{ $menu['title'] }}</a>
                                                        @endif
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                @else
                                    <a href="{{ isset($action['target']) ? $action['target'] : 'javascript:;' }}"
                                       @if(isset($action['type']) && $action['type'] == 'modal')
                                       data-{{ $action['modal_method'] or 'toggle' }}="modal" data-target="#ajax-modal"
                                       @endif
                                       class="btn btn-circle {{ isset($action['class']) ? $action['class'] : 'default' }}"
                                            {!! isset($action['attributes']) ? HTML::attributes($action['attributes']) : null !!}>
                                        @if(isset($action['icon']))
                                            <i class="{{ $action['icon'] }}"></i>
                                        @endif

                                        @if(isset($action['title']))
                                            {{ $action['title'] }}
                                        @endif
                                    </a>
                                @endif
                            @endif
                        @endforeach
                    </div>
                @endif

                @if(isset($tabs))
                    <ul class="nav nav-tabs">
                        @foreach($tabs as $key => $tab)
                            <li class="{{ !$key ? 'active' : '' }}">
                                <a href="#{{ $tab['id'] }}_tab" data-toggle="tab"> {{ $tab['label'] }} </a>
                            </li>
                        @endforeach
                    </ul>
                @endif


                @if(isset($dropdown_buttons))
                    <style>
                        .portlet.light > .portlet-title > .actions {
                            margin-right: 40px;
                        }
                    </style>
                    @include('partials.containers.dropdown-buttons', [
                        'dropdown_buttons' => $dropdown_buttons,
                        'side' => 'right'
                    ])
                @endif

            </div>
        @endif
        <div class="portlet-body {{ isset($has_form) && $has_form ? 'form' : null }}">
            @if(isset($tabs))
                <div class="tab-content">
                    <div class="tab-pane active" id="{{ $tabs[0]['id'] }}_tab">
                        @endif
                        @endif


                        {{-- Open new tab and close old --}}
                        {{-- @params array $tab['id'] --}}
                        @if($part == 'nextTab')
                    </div>
                    <div class="tab-pane" id="{{ $tab_id }}_tab">
                        @endif


                        {{-- Closes tabs --}}
                        @if($part == 'closeTabs')
                    </div>
                </div>
        </div>
    </div>
    {{-- END PORTLET BODY --}}
    @endif


    @if($part == 'close')
    </div>
    </div>
    {{-- END PORTLET BODY --}}
@endif


