@if($part == 'open')
    <div class="form-group tb-duplicate-group">
        @if(isset($offset))
            <div class="row">
                <div class="col-md-offset-{{ $offset }}">
        @endif
@endif


@if($part == 'close')
            @if(isset($offset))
                </div>
            </div>
        @endif
    </div>
@endif
