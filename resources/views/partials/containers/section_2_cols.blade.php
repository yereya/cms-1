<div class="portlet-body form {{ isset($portlet_name) ? $portlet_name : '' }}">
    <h4>{{ $header }}</h4>
    <hr>

    <div class="section_container_{{ strtolower($header) }} form-md-line-input">
        @if(count($values) > 0)
        @foreach($values as $value)
            <div class="row col-lg-offset-2 clone">
                <div class="col-md-4 ">
                    <input class="form-control key_group" placeholder="key" type="text" name="{{ $input_name }}[key][]"
                           value="{{ $value['key'] }}">
                </div>
                <div class="col-md-4">
                    <input class="form-control value_group" placeholder="value" type="text"
                           name="{{ $input_name }}[value][]" value="{{ $value['value'] }}">
                </div>
                <div class="col-md-2 btn_section_remove">
                    <a href="javascript:;" class="btn btn-icon-only grey-cascade"
                       data-btn_section="section_container_{{ strtolower($header) }}">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
        @endforeach
        @endif

        <div class="row col-lg-offset-2 clone">
            <div class="col-md-4 ">
                <input class="form-control key_group" placeholder="key" type="text" name="{{ $input_name }}[key][]">
            </div>
            <div class="col-md-4">
                <input class="form-control value_group" placeholder="value" type="text"
                       name="{{ $input_name }}[value][]">
            </div>
            <div class="col-md-2 btn_section_remove">
                <a href="javascript:;" class="btn btn-icon-only grey-cascade"
                   data-btn_section="section_container_{{ strtolower($header) }}">
                    <i class="fa fa-times"></i>
                </a>
            </div>
        </div>
    </div>
</div>