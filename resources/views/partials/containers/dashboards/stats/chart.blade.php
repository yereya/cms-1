<div class="{{$column ?? 'col-md-12'}}">
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title'=>'Chart',
            ])

    {!! Form::open([
        'route'=>[!empty($data['route'])
            ? $data['route']
            :'dashboards.daily-stats.api'
        ],
        'method' => 'POST',
        'role' => 'form',
        'class'=>'chart-form',
        'data-controllers'=>'dash-params-form',
        'data-name' => $data['name']
      ])
   !!}
    {{csrf_field()}}

    @foreach($data as $key=>$val)
        @if($key == 'date')
            {{Form::hidden('from',$val['from'])}}
            {{Form::hidden('to',$val['to'])}}
        @else
            {!! Form::hidden($key,$val) !!}
        @endif

        @if(!empty($dynamic_mode))
            {!! Form::hidden('dynamic_mode',true) !!}
            {!! Form::hidden('dynamic_label', $dynamic_mode['label'] ?? '') !!}
            {!! Form::hidden('dynamic_column', $dynamic_mode['column'] ?? '') !!}
        @endif
    @endforeach

    <div class="row">
        <div class="chart-container" style="position: relative; width:{{$width ?? '58vw' }}">

            @if(!empty($data['target_type']))
                <label class="control-label pull-left col-xs-2">Target Selection :</label>
                @if($data['group'] == 'ppc')
                    @include('partials.fields.radio', [
                       'column' =>  1,
                       'radio_id' => 'sales',
                       'label' => 'Sales',
                       'name' => 'target',
                       'value'=>'sales',
                       'checked'=>true,
                   ])

                    @include('partials.fields.radio', [
                      'column' => 1,
                      'radio_id' => 'cpa',
                      'label' => 'CPA',
                      'name' => 'target',
                      'value'=>'cpa'
                  ])

                    @include('partials.fields.radio', [
                      'column' => 1,
                      'radio_id' => 'profit',
                      'label' => 'Profit',
                      'name' => 'target',
                      'value'=>'profit'
                      ])
                @endif

                @if($data['group'] == 'daily_stats')

                    @include('partials.fields.radio', [
                       'column' =>  2,
                       'radio_id' => 'cost_profit',
                       'label' => 'Cost Profit',
                       'name' => 'target',
                       'value'=>'cost_profit',
                       'checked'=>true,
                   ])

                    @include('partials.fields.radio', [
                       'column' =>  2,
                       'radio_id' => 'clicks',
                       'label' => 'Clicks',
                       'name' => 'target',
                       'value'=>'clicks',
                   ])

                    @include('partials.fields.radio', [
                      'column' => 2,
                      'radio_id' => 'sales_count',
                      'label' => 'Sales',
                      'name' => 'target',
                      'value'=>'sales_count'
                  ])

                    @include('partials.fields.radio', [
                      'column' => 2,
                      'radio_id' => 'avg_position',
                      'label' => 'Avg. Position',
                      'name' => 'target',
                      'value'=>'avg_position'
                      ])

                    @include('partials.fields.radio', [
                      'column' => 2,
                      'radio_id' => 'cts',
                      'label' => 'CTS',
                      'name' => 'target',
                      'value'=>'cts'
                      ])

                    @include('partials.fields.radio', [
                       'column' => 2,
                       'radio_id' => 'site_ctr',
                       'label' => 'Site CTR',
                       'name' => 'target',
                       'value'=>'site_ctr'
                       ])

                    @include('partials.fields.radio', [
                       'column' => 2,
                       'radio_id' => 'cpc',
                       'label' => 'CPC',
                       'name' => 'target',
                       'value'=>'cpc'
                       ])

                    @include('partials.fields.radio', [
                       'column' => 2,
                       'radio_id' => 'click_out_unique',
                       'label' => 'Unique Site Ctr',
                       'name' => 'target',
                       'value'=>'click_out_unique'
                       ])
                @endif
            @endif

            <div class="clearfix"></div>
            <canvas id="chart"></canvas>
        </div>
    </div>

    @include('partials.containers.portlet', [
               'part' => 'close'
       ])

    {!! Form::close() !!}
</div>
