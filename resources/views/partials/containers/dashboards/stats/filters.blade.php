<div class="filters">
    <div class="row">

        {{--Filter --}}
        @if(!empty($filters['fieldSelection']))
            <div class="fieldSelectorWrapper hidden">
                <div class="col-md-10 ">
                    <div class="col-md-1 title">Fields:</div>
                    <div class="fieldsSelector col-md-11">
                        <label>
                            <select class="fieldsSelect2" name="field" multiple></select>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        @endif

        @if(!empty($filters['searchBar']))
            <div class="col-md-2 searchBar col-sm-12 pull-right hidden">
                <div id="experiments_monitor_filter" class="dataTables_filter pull-right">
                    <label>Search:
                        <input type="search"
                               class="searchInput form-control input-sm input-small input-inline"
                               placeholder="" aria-controls="experiments_monitor">
                    </label>
                </div>
            </div>
        @endif
    </div>
</div>
