<style>
    .table-stats .filters {
        margin-top: 25px;
        margin-bottom: 25px;
    }
    .fieldSelectorWrapper .title {
        line-height: 35px;
    }
</style>

<div class="col-md-12">
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title'=>  $title,
    ])

    {!! Form::open([
          'route'=>[!empty($data['route'])
              ? $data['route']
              :'dashboards.daily-stats.api'

          ],
          'method' => 'POST',
          'role' => 'form',
          'class'=>'table-stats-form',
          'data-controllers'=>'dash-params-form'
       ])
    !!}

    {{csrf_field()}}

    @foreach($data as $key=>$val)
        {!! Form::hidden($key,$val) !!}
    @endforeach
    @if(!empty($filters))
        @foreach($filters as $key=>$filter)
            {!! Form::hidden($key,$filter) !!}
        @endforeach
    @endif


    <div class="table-stats  {{$wrapper_class ?? ''}}">
        @if(!empty($filters))
            @include('partials.containers.dashboards.stats.filters', ['filters'=>$filters])
        @endif
        <div class="table-responsive"></div>
    </div>
</div>
<div class="clearfix"></div>


{!! Form::close() !!}

@include('partials.containers.portlet', [
           'part' => 'close'
   ])