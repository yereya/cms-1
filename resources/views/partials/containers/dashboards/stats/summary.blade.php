<div class="col-md-6">
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title'=>  $title,
    ])

    {!! Form::open([
          'route'=>['dashboards.daily-stats.api'],
          'method' => 'POST',
          'role' => 'form',
          'class'=>'stats-form',
          'data-controllers'=>'dash-params-form'
       ])
    !!}

    {{csrf_field()}}

    @foreach($data as $key=>$val)
        {!! Form::hidden($key,$val) !!}
    @endforeach


    <div id="{{$data['period']}}" class="dash-stats {{ $wrapper_class ?? '' }} text-center">
        <div class="period cell">
            <span class="text-capitalize title col-md-offset-3 col-md-6">profit change</span>
            <div class="clearfix"></div>
            <div class="ratio">
                <i class="fa fa-arrow-up"></i>
                <span class="percent">N/A</span>
                <span class="diff">
                </span>
            </div>
        </div>
        <div class="col-md-4 cost cell">
            <p class="number"><span>N/A</span></p>
            <p class="title">Cost</p>
        </div>
        <div class="col-md-4 revenue cell">
            <p class="number"><span>N/A</span></p>
            <p class="title">Revenue</p>
        </div>
        <div class="col-md-4 profit cell">
            <p class="number"><span>N/A</span></p>
            <p class="title">Profit</p>
        </div>
    </div>
    <div class="clearfix"></div>

    {!! Form::close() !!}

    @include('partials.containers.portlet', [
               'part' => 'close'
       ])

</div>