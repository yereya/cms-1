@if(!$tree->isEmpty())
    <ol class="dd-list" id="{{ $id ?? null }}">
        @foreach($tree as $tree_item)
            <li class="dd-item dd3-item" data-id="{{$tree_item->id }}">
                <div class="dd-handle dd3-handle">Drag</div>
                <div class="dd3-content">{{$tree_item->name}}
                    <div class="pull-right control_buttons">
                        @if(is_callable($actions))
                            {!! $actions($tree_item) !!}
                        @endif
                    </div>
                </div>
                @if (isset($tree_item->children))
                    @include('partials.containers.nested_list', [
                        'tree' => $tree_item->children,
                        'can' => $can,
                        'actions' => $actions
                    ])
                @endif
            </li>
        @endforeach
    </ol>
@endif


