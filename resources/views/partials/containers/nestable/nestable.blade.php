<div class="col-xs-12">
    <div class="nestable-lists {{ $class ?? '' }}" {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
        {{ isset($required) ? 'required="required"' : '' }}
        data-required_name="{{ $required_name ?? 'List' }}"
    >
        @include('partials.containers.nestable.list', [
          'tree' => $tree ?? [],
          'connect_tree' => $connect_tree ?? null,
          'can' => $can ?? null,
          'id' => $id ?? null,
          'item_type' => 'parent',
          'title' => $title ?? 'name',
          'subtitle' => $subtitle ?? null,
          'tooltip' => $tooltip ?? '',
          'adds' => $adds ?? [],
          'actions' => $actions ?? [],
          'no_nest' => $no_nest ?? false,
          'child' => $child ?? [],
          'multi_tree' => $multi_tree ?? null,
          'is_connect' => isset($is_connect) ? 'sortable' : null,
          'container' => $container ?? null
        ])
    </div>
</div>

@if (isset($control) && $control)
    <div class="form-actions nestable-control-buttons {{ isset($control['show']) ? 'display' : '' }}">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-2">
                {!! Form::open([
                    'url' => $control['url'],
                    'method' => $control['method'] ?? 'POST',
                    'class' => 'form-horizontal submit_changes_fields',
                    'role' => 'form'
                ]) !!}

                {!! Form::hidden('list', null) !!}
                {!! Form::close() !!}

                @include('partials.fields.button', [
                      'attributes' => [
                          'disabled' => true,
                          'data-form_class' => '.submit_changes_fields'
                      ],
                      'value' => 'Save Changes',
                      'type' => 'link',
                      'disabled' => true
                    ])

                @include('partials.fields.button', [
                    'attributes' => [
                       'onclick' => "javascript:location.reload()",
                    ],
                    'type' => 'link',
                    'class' => "btn default",
                    'value' => 'Cancel',
                ])
            </div>
        </div>
    </div>
@endif

@section('javascript')
    <script type="application/javascript">
        (function () {
           App.nestableSortable('{{ $id ?? null }}');
        })();
    </script>
@stop