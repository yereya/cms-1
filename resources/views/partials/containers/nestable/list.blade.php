@if (isset($nested_label))
    @include('partials.fields.h4-separator', ['label' => $nested_label])
@endif

<ol class="dd-list {{ $no_nest ? 'no-nest' : null }} {{ $is_connect ?? '' }} nestable-color-blue" id="{{ $id ?? '' }}">
    @if (!empty($multi_tree))
        @foreach($multi_tree as $tree)
            @foreach($tree as $item)
                <li class="dd-item dd3-item"
                    data-id="{{ $item->id ?? 0 }}"
                    item_id="{{ $item->id ?? 0 }}"
                    id="{{ $item_type . '_' . ($item->id ?? 0) }}"
                        {!! isset($returnable_values['type'])
                         ? 'data-type=' . $returnable_values['type']($item)
                         : '' !!}
                >
                    <div class="dd3-content">
                        @if(isset($link))
                            {!! $link($item) !!}
                        @else
                            {{ $item->{$title} }}
                        @endif
                            @if($subtitle)
                                <span class="sub-title tooltips"
                                      tootlip="{{ $tooltip ?? '' }}"
                                      data-original-title="{{ $tooltip ?? '' }}"
                                      title="{{ $tooltip ?? '' }}">
                                {{ $item->{$subtitle} }}</span>
                            @endif

                            @foreach($adds as $add)
                                @if (is_callable($add))
                                    {!! $add($item) !!}
                                @endif
                            @endforeach

                            <div class="pull-right control_buttons">
                                @foreach($actions as $action)
                                    @if(is_callable($action))
                                        {!! $action($item) !!}
                                    @endif
                                @endforeach
                            </div>
                        </div>

                    @if(!empty($container) && is_callable($container))
                        {!! $container !!}
                    @endif

                    <div class="dd-handle dd3-handle">Drag</div>

                    @foreach($child as $c)
                        @if ($child && isset($item->{$c}))
                            @include('partials.containers.nestable.list', [
                                'tree' => $item->{$c},
                                'can' => $can,
                                'actions' => $actions,
                                'item_type' => 'child',
                                'id' => null
                            ])
                        @endif
                    @endforeach

                </li>
            @endforeach
            <hr>
        @endforeach
    @else
        @if(!empty($tree))
            @foreach($tree as $item)
                <li class="dd-item dd3-item"
                    data-id="{{ $item->id ?? 0 }}"
                    item_id="{{ $item->id ?? 0 }}"
                    id="{{ $item_type . '_' . ($item->id ?? 0) }}"
                        {!! isset($returnable_values['type']) ? 'data-type=' . $returnable_values['type']($item) : '' !!}
                >
                    <div class="dd3-content">
                        @if(isset($link))
                            {!! $link($item) !!}
                        @else
                            {{ $item->{$title} }}
                        @endif

                        @if($subtitle)
                            <span class="sub-title tooltips"
                                  tootlip="{{ $tooltip ?? '' }}"
                                  data-original-title="{{ $tooltip ?? '' }}"
                                  title="{{ $tooltip ?? '' }}">
                            {{ $item->{$subtitle} }}</span>
                        @endif

                        @foreach($adds as $add)
                            @if (is_callable($add))
                                {!! $add($item) !!}
                            @endif
                        @endforeach

                        <div class="pull-right control_buttons">
                            @foreach($actions as $action)
                                @if(is_callable($action))
                                    {!! $action($item) !!}
                                @endif
                            @endforeach
                        </div>
                    </div>

                    @if(!empty($container) && is_callable($container))
                        {!! $container($item) !!}
                    @endif

                    <div class="dd-handle dd3-handle">Drag</div>

                    @foreach($child as $c)
                        @if ($child && isset($item->{$c}))
                            @include('partials.containers.nestable.list', [
                                'tree' => $item->{$c},
                                'can' => $can,
                                'actions' => $actions,
                                'item_type' => 'child',
                                'id' => null
                            ])
                        @endif
                    @endforeach

                </li>
            @endforeach
        @endif
    @endif
</ol>

@if (isset($is_connect) && $is_connect)

    @if (isset($is_connect_label))
        @include('partials.fields.h4-separator', ['label' => $is_connect_label])
    @else
        <hr>
    @endif

    <ol class="dd-list {{ $no_nest ? 'no-nest' : null }} {{ $is_connect }} nestable-color-green
        @if (count($connect_tree) == 0) nestable-empty-list @endif
    " id="{{ $nested_second_id ?? null }}">
    @foreach($connect_tree as $item)
        <li class="dd-item dd3-item"
            data-id="{{ $item->id ?? 0 }}"
            item_id="{{ $item->id ?? 0 }}"
            id="{{ $item_type . '_' . ($item->id ?? 0) }}"
        >
            <div class="dd3-content">{{ $item->{$title} }}
                @if($subtitle)
                    <span class="sub-title tooltips" tootlip="{{ $tooltip ?? '' }}" data-original-title="{{ $tooltip ?? '' }}"
                          title="{{ $tooltip ?? '' }}">{{ $item->{$subtitle} }}</span>
                @endif

                @foreach($adds as $add)
                    @if (is_callable($add))
                        {!! $add($item) !!}
                    @endif
                @endforeach

                <div class="pull-right control_buttons">
                    @foreach($actions as $action)
                        @if(is_callable($action))
                            {!! $action($item) !!}
                        @endif
                    @endforeach
                </div>
            </div>

            @if(!empty($container) && is_callable($container))
                {!! $container !!}
            @endif

            <div class="dd-handle dd3-handle">Drag</div>

            @foreach($child as $c)
                @if ($child && isset($item->{$c}))
                    @include('partials.containers.nestable.list', [
                        'tree' => $item->{$c},
                        'can' => $can,
                        'actions' => $actions,
                        'item_type' => 'child',
                        'id' => null
                    ])
                @endif
            @endforeach

        </li>
    @endforeach
    </ol>
@endif
