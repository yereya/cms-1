<a href="javascript:void(0);"
    data-property_toggle="{{ $route }}"
    title="{{ $title or '' }}"
    {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
    class="{{ isset($class) ? (is_array($class)) ? implode($class, ' ') : $class : '' }}"
><span class='toggle_checkbox' data-toggle_state="{{ $checked ? 'checked' : '' }}"> </span></a>