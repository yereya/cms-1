{!! Form::open([
        'route' => $route,
        'method' => isset($method) ? $method : 'POST',
        'title' => $title,
        'class' => isset($class) ? $class : 'download_file'
      ])
!!}
<button type="submit" class="btn btn-link">{{ isset($title) ? $title : 'Download' }}</button>
{!! Form::close() !!}