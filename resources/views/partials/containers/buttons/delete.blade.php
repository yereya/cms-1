{!! Form::open([
        'route' => $route,
        'method' => 'delete',
        'title' => $title,
        'class' => 'form-invisible tooltips'
    ])
!!}
    <button type="submit" class="btn red-mint"><span aria-hidden="true" class="icon-trash"></span> Delete </button>
{!! Form::close() !!}