<a data-target="sweetAlert"
data-alert_title="{{$title}}"
data-alert_type="{{$type}}"
data-alert_href="{{$route}}"
data-alert_text="{{$text}}"
title="{{$tooltip ?? ''}}"
class="tooltips"><span aria-hidden="true" class="{{$icon}}"></span></a>