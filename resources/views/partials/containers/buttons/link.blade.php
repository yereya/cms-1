<a href="{{ $route }}"
   class="{{ isset($class) ? (is_array($class)) ? implode($class, ' ') : $class : '' }}"
    {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
>@if(isset($icon))<span aria-hidden="true" title="{{ $title }}" class="{{ $icon }} tooltips"></span>@endif{{ $value or '' }}</a>