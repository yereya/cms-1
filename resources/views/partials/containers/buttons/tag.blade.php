@if($link ?? false)
    <a href="{{ $route }}"
@else
    <span
@endif
        @php // tag options: warning, success, danger, default, primary @endphp
        class="{{ isset($class) ? (is_array($class)) ? implode($class, ' ') : $class : '' }} tag label label-{{ $tag_type or 'primary' }}"
        {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
>@if(isset($icon))<span aria-hidden="true" title="{{ $title }}"
                        class="{{ $icon }} tooltips"></span>@endif{{ $value or '' }}
@if($link ?? false)
    </a>
@else
    </span>
@endif
