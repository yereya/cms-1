@php

  $buttons = [
       [
           'type' => 'link',
           'value' => 'Clear',
           'icon' => 'fa fa-ban',
           'url' => isset($route_back) ? $route_back : back_url(),
           'class' => 'btn grey ',
           'attributes' => [
               'title' => 'Clear filters',
           ]
       ],
       [
           'type' => 'button',
           'value' => '',
           'class' => 'btn grey-salsa',
           'icon' => 'fa fa-copy',
           'attributes' => [
               'onClick' => 'App.copyToClipboard("report");',
               'title' => 'Copy table to clipboard',
           ]
       ],
       [
           'type' => 'button',
           'value' => '',
           'class' => 'green-meadow download_file',
           'icon' => 'fa fa-download',
           'attributes' => [
               'title' => 'Download the report as csv',
           ]
       ],
       [
           'type' => 'submit',
           'value' => 'Run',
           'icon' => 'fa fa-play',
           'class' => 'btn-primary filter_btn',
           'attributes' => [
               'title' => 'Run report',
           ]
       ],
    ];
  $buttons_without_download = array_values(array_filter($buttons,function($button){return strpos($button['class'],'download_file') == false;}))

@endphp

{{--- start filter portlet ---}}
@if ($part == 'open')
    @include('partials.containers.portlet', [
            'part' => 'open',
            'title' => isset($title) ? $title : 'Bo Report',
            'tools' => [
                'collapse' => ['class' => 'collapse'],
            ],
    ])
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px;">

            {!! Form::open([
                    'url' => route('reports.{report_id}.index', Route::getCurrentRoute()->parameters()['report_id']),
                    'method' => 'GET',
                    'id' => 'view_bookmark',
                    'class' => 'form-horizontal',
                    'role' => 'form',
            ]) !!}
                @include('partials.fields.select', [
                    'name' => 'filter_id',
                    'id' => 'select_bookmark',
                    'label' => 'Bookmarked Filters:',
                    'form_line' => true,
                    'column' => 5,
                    'column_label' => 5,
                    'list' => isset($saved_filters) ? $saved_filters->pluck('name', 'id') : [],
                    'value' => isset($selected_filter_id) ? [$selected_filter_id] : [],
                    'attributes' => [
                        'data-selected_filter' => isset($selected_filter_id) ? $selected_filter_id : ''
                    ]
                ])

            <div class="col-md-7">
                @if(auth()->user()->can($permission_name. '.button[bookmark].delete'))
                    @if(isset($selected_filter_id))
                        @include('partials.fields.button',[
                                'type' => 'button',
                                'value' => '',
                                'class' => 'btn-default red tooltips',
                                'url' => route('reports.filters.ajax-modal',
                                    ['method' => 'deleteFilter', 'filter_id' => $selected_filter_id]),
                                'icon' => 'glyphicon glyphicon-trash',
                                'attributes' => [
                                    'data-toggle' => 'modal',
                                    'data-target' => '#ajax-modal',
                                    'title' => 'Delete',
                                ]
                            ]
                        )
                    @endif
                @endif
                @if(auth()->user()->can($permission_name. '.button[bookmark].add'))
                    @include('partials.fields.button',[
                            'value' => '',
                            'class' => 'btn-default yellow tooltips',
                            'url' => route('reports.filters.ajax-modal', [
                                'method' => 'saveFilters',
                                'filtersFormId' => 'filters_form'
                            ]),
                            'attributes' => [
                                'data-toggle' => 'modal',
                                'data-target' => '#ajax-modal',
                                'title' => 'Save new bookmark',
                            ],
                            'icon' => 'glyphicon glyphicon-bookmark'
                        ]
                    )
                @endif
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endif


{{--- start form or container filter ---}}
@if (isset($form) && $part == 'open')
    <div class="row">
        <div class="col-md-12">
            {!! Form::open([
                    'route' => $route,
                    'method' => isset($method) ? $method : 'POST',
                    'id' => 'filters_form',
                    'class' => 'form-horizontal tp-server-side ' . (isset($class) ? $class : ''),
                    'role' => 'form',
                    'data-datatable_class' => 'tp-server-side-datatable',
                    'data-datatable_table' => '#report',
            ]) !!}

            {!! Form::hidden('query_id', $report_id) !!}

    {{-- TODO remove this code since we have the query_id --}}
    @if (isset($hidden))
        @foreach ($hidden as $name => $value)
            {!! Form::hidden($name, $value) !!}
        @endforeach
    @endif
    {{-- TODO remove end --}}

@elseif (!isset($form) && $part == 'open')
    <div class="tp-server-side-datatable {{ isset($class) ? $class : '' }}"
         data-server_side_url="{{ $route }}"
         data-server_side_method="{{ isset($method) ? $method : 'POST'}}"
         data-datatable_table="#report"
         {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
    >

        {{ csrf_field() }}
@endif


@if (isset($form) && $part == 'close')
    <div class="row">
        <div class="col-md-1">
            Filters:
        </div>
        <div class="col-md-10 filter-bread-crumbs">

        </div>
    </div>
    @include('partials.fields.h4-separator')
    @include('partials.containers.form-footer-actions', [
                'offset' => '1',
                'buttons' => [
                    'custom' => $can_download ? $buttons  : $buttons_without_download
                ]
            ])
    {!! Form::close() !!}
        </div>
    </div>

    @elseif ($part == 'close' && !isset($form))
    </div>
@endif

@if ($part == 'close')

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])

    @if (($report_id == "1" OR $report_id == "26") AND isset($last_update))
        <p style="font-weight:bold;" class="title">Last update: {{$last_update}}</p>
    @endif

    @if (($report_id == "3") AND isset($last_update) AND isset($last_update) )
        <p style="font-weight:bold;" class="title">Last cost update: {{$last_cost}}</p>
        <p style="font-weight:bold;" class="title">Last conversions update: {{$last_update}}</p>
    @endif

    @include('partials.containers.portlet', [
        'part' => 'open',
        'title' => isset($title) ? $title : 'Data'
    ])

    <table class="table table-striped table-bordered dataTable no-footer" role="grid" id="report"></table>

    @include('partials.containers.portlet', [
        'part' => 'close'
    ])
@endif