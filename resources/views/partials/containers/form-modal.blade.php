@include('partials.containers.ajax-modal', [
    'part' => 'open',
    'title' => $title,
])
{!! Form::open([
    'url' => $form['route'],
    'method' => $form['method'],
    'id' => '',
    'class' => 'form-horizontal',
    'role' => 'form'
]) !!}

    {{-- Inputs --}}
    @foreach($inputs as $input)
        @include($input['include'], $input['params'])
    @endforeach

    {{-- Hidden input field --}}
    {{-- used to store the key values dynamically loaded --}}
    {!! Form::hidden('fetch_form', null, ['id' => 'fetch_form']) !!}

{!! Form::close() !!}

@include('partials.containers.ajax-modal', [
    'part' => 'close',
    'buttons' => isset($modal_buttons) ? $modal_buttons : [
        'close' => true,
        'save' => true
    ],
    'js' => isset($js) ? $js : null
])