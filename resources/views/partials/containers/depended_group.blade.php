@if ($part == 'open')
    <div class="form-group form-md-line-input depended-group {{ $class or '' }}"
         data-dependency_selector="{{ $selector }}"
         {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}>
@endif

@if ($part == 'close')
    </div>
@endif