@if (!isset($type) || (isset($type) && $type != 'hidden'))
    @if(!isset($form_line))
        <div class="form-group form-md-line-input fileupload-buttonbar {{ !$errors->get($name) ? '' : ' has-error' }}">
    @else
        <div class="fileupload-buttonbar {{ isset($column) ? 'col-md-' . $column : '' }}">
    @endif
    @if(!isset($no_label))
    <label class="col-md-{{ isset($column_label) ? $column_label : 2 }} control-label" for="form_control_{{ snake_case($name) }}">
        {{ isset($required) ? '*' : '' }}{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}
    </label>
    @endif

    @if(isset($column_input))
        <div class="col-md-{{ $column_input }}">
    @else
        <div class="col-md-{{ isset($column_label) ? 12 - $column_label : 9 }}">
    @endif
@endif
@if (isset($group) && $group) <div class="input-group"> @endif
    <div class="image-upload  {{ empty($src)  ? 'iu-no-image' : ''}}" >
        <div class="image-upload-header" title="{{ basename($src)}}" >{{ basename($src)}}</div>
        <div class="image-upload-toolbar">
            @if (isset($stackable) && $stackable)
                @include('partials.fields.button', [
                    'class' => 'stackable btn green fileinput-button',
                    'value' => 'Browse',
                    'id' => 'stack_image',
                    'url' => '#stack_image',
                    'icon' => 'fa fa-upload',
                    'attributes' => [
                        'href' => '#stack_image',
                        'data-route' => $stackable['route'],
                        'data-toggle' => 'modal',
                    ]
                ])
            @else
                <a href="{{ $href }}" data-toggle="modal" data-target="#ajax-modal" class="btn green fileinput-button">
                    <i class="fa fa-upload"></i>
                </a>
            @endif
            <a href="{{ $src }}" target="_blank" style="display: {{ $src ? 'inline-block' : 'none' }}" class="btn green fileinput-button iu-toolbar-preview">
                <i class="fa fa-eye"></i>
            </a>
            <span class="btn red iu-toolbar-delete fa fa-trash" data-name="{{ $name }}" style="display: {{ $src ? 'inline-block' : 'none' }}"></span>
        </div>
        <div class="image-upload-img-wrapper" >
          <span class="iu-img-helper"></span>
          <img class="img-responsive profile-img {{ 'image_preview_' . ($class_name ?? $name) }}" src="{{ !empty($src)  ? $src : '/assets/global/img/no-image.png' }}" >
        </div>
        <div class="image-upload-footer" >
            <div class="iu-footer-height" ></div>
            <div class="iu-footer-size" ></div>
        </div>
        <input type="hidden" {{ isset($required) ? 'required="required"' : '' }}
               data-required_name="{{ $label or ucwords(str_replace(['-', '_'], ' ', $name)) }}"
               name="{{ $name . '_filename' }}"
               class="{{ 'image_preview_' . ($class_name ?? $name) }}"
               value="{{ $src ?? '' }}">
        <input type="hidden" name="{{ $name }}" class="{{ 'image_preview_id_' . ($class_name ?? $name) }}" value="{{ $media_id ?? '' }}">
    </div>

    <div class="form-control-focus"></div>


    @foreach($errors->get($name) as $error)
        <span class="error-block">{{ $error }}</span>
    @endforeach
    @if (!isset($type) || (isset($type) && $type != 'hidden'))
    </div>
    <div class="clearfix"></div>
</div>
@endif