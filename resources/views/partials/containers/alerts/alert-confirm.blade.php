<p>{!! $text or 'Do you confirm performing this action?' !!}</p>
<a class="btn btn-lg green" onclick="{{$on_confirm}}">Confirm</a>
<a class="btn btn-lg red" onclick="swal.close()">Cancel</a>