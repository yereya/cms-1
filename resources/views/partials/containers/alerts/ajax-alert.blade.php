@if(isset($action) && $action)
    @if($action=='delete' && isset($route) && $route)
        @include('partials.containers.alerts.alert-delete',[
            'route' => $route,
            'route_params' => isset($route_params) && $route_params ? $route_params : '',
            'text' => $text
        ])
    @elseif($action=='form-confirm' && isset($route) && $route)
        @include('partials.containers.alerts.alert-form-confirm',[
            'route' => $route,
            'route_params' => $route_params ?? '',
            'text' => $text
        ])
    @elseif($action=='confirm')
        @include('partials.containers.alerts.alert-confirm',[
            'on_confirm' => isset($on_confirm) && $on_confirm ? $on_confirm : '',
            'text' => $text
        ])
    @else
        @include('partials.containers.alerts.alert-info',[
            'text' => $text
        ])
    @endif
@else
    @dump('ajax-alert.blade : action is required')
@endif
<style>
    .swal2-confirm{
        display:none;
    }
</style>