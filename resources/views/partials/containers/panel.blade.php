@if($part == 'open')
    {{-- PANEL --}}
    <div class="clearfix">
        <div class="panel panel-success">
            <!-- Default panel contents -->
            <div class="panel-heading">
                <h3 class="panel-title">{{ $title or '' }}</h3>
            </div>
            <div class="panel-body">
@endif

@if($part == 'close')
            </div>
        </div>
    </div>
    {{-- END PANEL --}}
@endif
