<li class="external">
    <h3>
        <span class="bold">{{ $notifications_read }} read</span>
        <span class="bold">{{ $notifications_unread }} unread</span>
    </h3>
    <a href="/reports/alerts">view all</a>
</li>