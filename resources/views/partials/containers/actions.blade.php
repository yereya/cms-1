<div class="{{ isset($class) ? ($class['type'] ?? 'tb-actions-duplicate-group') : '' }} {{ isset($column) ? 'col-md-' . $column : 'col-md-2' }}">
@foreach($actions as $action)
    @if($action['action'] == 'remove')
        <a href="javascript:;" class="btn btn-link default remove-row-btn">
                <i class="fa fa-trash"></i>
        </a>
    @elseif($action['action'] == 'clone')
            <a href="javascript:;" class="btn btn-link default clone-row-btn">
                <i class="fa fa-plus"></i>
            </a>
    @endif
@endforeach
</div>