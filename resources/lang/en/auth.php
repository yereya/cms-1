<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password_expired' => 'Your password has expired. It is time to visit the IT department (bring bribe).',
    'password_about_to_be_expired' => 'You password is about to be expired please reset it.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];
