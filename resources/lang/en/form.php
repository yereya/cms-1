<?php

return [
    'submit' => 'submit',
    'success' => 'The action was performed successfully',

    'fields' => [
        'username' => 'User Name',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'E-Mail',
        'password' => 'Password',
        'password_repeat' => 'Password Repeat',
        'name' => 'Name',
        'type' => 'Type',
        'description' => 'Description'
    ]
];