import $ from "jquery";
import slick from "../lib/slick/slick/slick.js";

class Carousel {

    constructor() {
        let $slick = $('.slider-wrapper');
        if (!$slick) {return;}

        let attributes = $slick.data();
        let default_settings = this.slickSettings();
        let settings = {};

        if (attributes) {
            $.each(attributes, function (i, v) {
                let key = topApp.string['toCamel'](i);
                settings[key] = v;
            });
        }

        $.extend(default_settings, settings);

        $slick.slick(default_settings);
    }


    slickSettings() {
        /*
         * Slick settings
         * Full documentation http://kenwheeler.github.io/slick/#settings
         */
        return {
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 300,
            useTransform: true,
            adaptiveHeight: true,
            vertical: false,
            swipe: true,
            verticalSwiping: false
        }
    }
}

export default Carousel;