import $ from "jquery";
import tooltipster from "../lib/tooltipster/js/tooltipster.bundle.js";

class ToolTipster {
    constructor() {
        $('.tooltipster').tooltipster({
            theme: 'tooltipster-shadow'
        });
    }
}

export default ToolTipster;