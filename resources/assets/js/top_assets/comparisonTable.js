import $ from 'jquery';

class ComparisonTable {

    /**
     * init comparison
     */
    constructor() {
        this.$comparison = $('#comparison');
        this.brandSelectedCount = 5;

        this.logos = this.$comparison.find('.comparison-logos');
        this.table = this.$comparison.find('.comparison-table');

        this.table.hide();

        $.each(this.logos.find('.brand-logo-container'), function () {
            $(this).addClass('not-selected');

            $(this).on('click', this.comparisonSelected);
        });

        let index = 1;
        $.each(this.table.find('.comparison-column'), function () {
            $(this).attr('index', index);
            index++;
            $(this).hide();
        })
    }

    comparisonSelected() {
        if ($(this).hasClass('not-selected')) {
            if (!this.table.is(':visible')) {
                this.table.show();
            }
            if (this.brandSelectedCount >= 1) {
                $(this).removeClass('not-selected');
                this.brandSelectedCount--;
                this.showColumn(this);
            }
        } else {
            $(this).addClass('not-selected');
            this.brandSelectedCount++;
            this.hideColumn(this);

            if (this.brandSelectedCount == 5) {
                this.table.hide();
            }
        }
    }

    showColumn(element) {
        let index = -1;

        //TODO
        // let reorder = function () {
        //     $.each(this.table.find('.comparison-column'), function () {
        //         if ($(this).is('visible')) {
        //             index = $(this).css('order');
        //         } else {
        //             let currentOrder = parseInt($(this).css('order'));
        //             $(this).css('order', parseInt(currentOrder + 1));
        //         }
        //     })
        // }

        let showOrderElement = function () {
            let brand_id = $(element).attr('brand_id');
            let column = this.$comparison.find('.comparison-column#brand_id_' + brand_id);
            column.css('order', parseInt(index + 1));
            column.fadeIn(600);
        };

        // reorder();
        showOrderElement();
    }

    hideColumn(element) {
        let brand_id = $(element).attr('brand_id');
        let column = this.$comparison.find('.comparison-column#brand_id_' + brand_id);
        column.fadeOut(600);
    }

}

export default ComparisonTable;