// requirejs.config({
//     baseUrl: "/top-assets/",
//     paths: {
//         // Libraries
//         "jquery": "libs/jquery-3.1.1.min",
//         "bootstrap": "libs/bootstrap/javascripts/bootstrap",
//         "slick": "libs/slick/slick",
//         "tooltipster": "libs/tooltipster/js/tooltipster.bundle",
//
//         'carousel': 'js/carousel',
//         'comparisonTable': 'js/comparisonTable',
//         'toolTips': 'js/toolTipster'
//     }
// });
//
// if (typeof jQuery === 'function') {
//     define('jquery', function ($) {
//         return $;
//     });
// }
//
// var mainJs = (function() {
//     function init($) {
//         "use strict";
//
//         if ($('.slider-wrapper')) {
//             requirejs(['carousel']);
//         }
//
//         if ($('#comparison')) {
//             requirejs(['comparisonTable']);
//         }
//
//         if ($('.tooltipster')) {
//             requirejs(['toolTips']);
//         }
//     }
//
//     return {
//         init: function ($) {
//             init($);
//         }
//     }
// })();
//
//
// requirejs(['jquery', 'slick'], function($) {
//     mainJs.init($);
// });