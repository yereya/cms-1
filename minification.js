/**
 * This file is responsible for moving distributing and minifying
 * The top assets javascript files across the cms and top5v2 projects.
 *
 * How to Install?
 * ----------------------------
 * 1. npm install --no-bin-links
 * 2. set env value like so TOP_PROJECT_FOLDER_PATH=relative path to your top5 project.
 *
 * How to use?
 * ----------------------------
 * Run the following from the root project folder.
 * gulp --gulpfile=minification.js
 *
 **/
'use strict';

require('dotenv').config();

const gulp = require('gulp');
const elixir = require('laravel-elixir');
const uglify = require('gulp-uglify');
const prompt = require('gulp-prompt');

/**
 * A basic env method for setting defaults for env values.
 *
 * @param {string} key
 * @param {mixed} default_value
 *
 * @return mixed
 **/
const env = function (key, default_value) {
    return process.env[key] || default_value;
};

/**
 * @param {object} args
 * @param {string} args.from
 * @param {array}  args.to
 * @param {array}  pipes
 **/
elixir.extend('topMove', function (args) {
    new elixir.Task('folder', function () {
        let src = gulp.src(args.from);

        if (args.pipes) {
            args.pipes.forEach(function (pipe) {
                src = src.pipe(pipe());
            });
        }

        args.to.forEach(function (destination) {
            src = src.pipe(gulp.dest(destination))
        });
    }).watch(args.from);
});

elixir(function (mix) {
    /**
     * @ver {string}
     **/
    const TOP_PROJECT_FOLDER_PATH = env('TOP_PROJECT_FOLDER_PATH', '../top5v2');

    /**
     * @ver {string}
     **/
    const TOP_JS_PATH = TOP_PROJECT_FOLDER_PATH + '/public/top-assets/js';

    // Move all of the top assets JavaScript files to top5V2 top-assets/js folder.
    // Without making any changes to there content.
    mix.topMove({
        from: './public/top-assets/js/*.js',
        to: [TOP_JS_PATH],
        pipes: [
            function () {
                return prompt.confirm(`I'm going to overwrite all Top5V2 JavaScript files located on "${TOP_JS_PATH}", is that ok?`)
            }
        ]
    });

    // Move the files again only this time to the /min folder with uglification.
    mix.topMove({
        from: './public/top-assets/js/*.js',
        to: [
            './public/top-assets/js/min',
            TOP_PROJECT_FOLDER_PATH + '/public/top-assets/js/min'
        ],
        pipes: [uglify]
    });

});