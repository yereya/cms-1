var sass = require('node-sass');
var fs = require('fs');
var argv = require('yargs').argv;
var compass = require('compass-importer');

sass.render({
    file: argv.inputFile,
    outputStyle: argv.outputStyle,
    includePaths: [argv.includePath],
    importer: compass
}, function (error, result) { // node-style callback from v3.0.0 onwards
    if (!error) {
        // No errors during the compilation, write this result on the disk
        fs.writeFile(argv.outputFile, result.css, function (err) {
            if (err) {
                console.log(err);
            }
        });
    } else {
        console.log(error);
    }
});

