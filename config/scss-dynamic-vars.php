<?php

return [
    'paths' => [
        '$css-media-path'             => '/top-assets/sites/{site_id}/images/',
        '$star'                       => '\e915',
        '$half-star'                  => '\e917',
        '$empty-star'                 => '\e914',
        '$global-default-font-normal' => 'Roboto-Regular',
        '$global-default-font-thin'   => 'Roboto-Thin',
        '$global-default-font-light'  => 'Roboto-Light',
        '$global-default-font-bold'   => 'Roboto-Bold',
        '$global-default-font-black'  => 'Roboto-Black'
    ]
];

