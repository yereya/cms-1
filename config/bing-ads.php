<?php

# ---- Old Version ----

//return [
//    'client_id'       => '000000004C178591',
//    'client_secret'   => '3nzQaMHilQbYvEZisdcfZ2IzTQ4aFtmh',
//    'developer_token' => '0089V7SMS0041965',
//    'customer_id'     => 9048309,
//    'refresh_token'   => "MCS7c3jAoqsk7MtiUZK4NtU6hdwgcqyUX5FETkwqLlcI6FStKtJto!!81UBZwUH7MYCAOKg6JhXIJWuiHttMfWdMPhTNqE5ff3f4eX*B1ugEA7s6WpbL002Sq6yBstqggfFfx3iDhFJsNhiPrUGXQUAhKrvf2*Fa18cTyARWnibCn!ytj0!YJdAW4PFSamjRor1Be5x43Q7rC3pOXVJe6ewzVVjN!!uTt!7or4ARdeS6evtX4PDoKzEyTlYPGUz46m*7NXFCkDQAd0Mcu82T!4sP*lNMi67TtyXsbFxPwc9WstIoPh!TKQTA4ZGQ8rMZ7zbUvy73DGHS*WW8hNDgqSiYuMDCGTK08pdXnY3DArNeG",
//    'user_name'       => 'simon.white@trafficpointltd.com',
//    'password'        => '359lenox!',
//    'OAuthRefreshTokenPath' => 'storage/app/bing-token.txt'
//];




# ---- New Version ----

return [
    'client_id'       => '1f5955c6-fe38-4b38-a3c7-b3dbb2a2a33f',
    'client_secret'   => 'fvrkUABA421+~~hsuVUQ46@',
    'developer_token' => '0089V7SMS0041965',
    'customer_id'     => 9048309,
    'refresh_token'   => "MCS7c3jAoqsk7MtiUZK4NtU6hdwgcqyUX5FETkwqLlcI6FStKtJto!!81UBZwUH7MYCAOKg6JhXIJWuiHttMfWdMPhTNqE5ff3f4eX*B1ugEA7s6WpbL002Sq6yBstqggfFfx3iDhFJsNhiPrUGXQUAhKrvf2*Fa18cTyARWnibCn!ytj0!YJdAW4PFSamjRor1Be5x43Q7rC3pOXVJe6ewzVVjN!!uTt!7or4ARdeS6evtX4PDoKzEyTlYPGUz46m*7NXFCkDQAd0Mcu82T!4sP*lNMi67TtyXsbFxPwc9WstIoPh!TKQTA4ZGQ8rMZ7zbUvy73DGHS*WW8hNDgqSiYuMDCGTK08pdXnY3DArNeG",
    'user_name'       => 'simon.white@trafficpointltd.com',
    'password'        => '359lenox!',
    'OAuthRefreshTokenPath' => 'storage/app/bing-token.txt'
];





/*IF THE TOKEN EXPIRED YOU SHOULD FOLLOW  THESE STEPS TO REFRESH IT:

# ---- New Version ----

#########  Option A :  #########

 * 1. Enter  this url inside explorer:
 *
 * https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=---- Your Client ID Goes HERE ---- &scope=openid%20profile%20https://ads.microsoft.com/ads.manage%20offline_access&response_type=code&redirect_uri=https://login.microsoftonline.com/common/oauth2/nativeclient&state=ClientStateGoesHere&prompt=consent
 *
 * Example -
 *
 * https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=1f5955c6-fe38-4b38-a3c7-b3dbb2a2a33f&scope=openid%20profile%20https://ads.microsoft.com/ads.manage%20offline_access&response_type=code&redirect_uri=https://login.microsoftonline.com/common/oauth2/nativeclient&state=ClientStateGoesHere&prompt=consent
 *
 * 2.
 *
 * Require Access Token;

 # ---- Send Post Request: ----

$this->httpService = new HttpService();
$responseJson = $this->httpService->post(
            'https://login.microsoftonline.com/common/oauth2/v2.0/token',
                       array(
            'client_id'=>'-------',
             'scope'=>'https://ads.microsoft.com/ads.manage',
              'code'=>'---- Enter Code From Previous Step Here ----',
               'redirect_uri'=>'https://login.microsoftonline.com/common/oauth2/nativeclient',
                'grant_type'=>'authorization_code',
                'client_secret'=>'-------')
            );

# ---- For Example: ----

$responseJson = $this->httpService->post(
            UriOAuthService::ENDPOINT_URLS[$endpointType]['OAuthTokenUrl'],
                       array(
            'client_id'=>'1f5955c6-fe38-4b38-a3c7-b3dbb2a2a33f',
             'scope'=>'https://ads.microsoft.com/ads.manage',
              'code'=>'M994cd5c9-c981-2971-cae5-76762a24c448',
               'redirect_uri'=>'https://login.microsoftonline.com/common/oauth2/nativeclient',
                'grant_type'=>'authorization_code',
                'client_secret'=>'fvrkUABA421+~~hsuVUQ46@')
            );

And extract refresh token from the response

 * 3. paste in in the => this file (at refresh token param) and also in => storage/app/bing-token.txt

#########  Option B :  #########

1.
Create file called Get-Tokens-Production.ps1

2.
Paste next code inside and replace client Id and client secret:

"
$clientId = "***** Enter your Client Id *****" For Example : "1f5955c6-fe38-4b38-a3c7-b3dbb2a2a33f"
$client_secret = "***** Enter your Client Secret *****" For Example : "fvrkUABA421%2B~~hsuVUQ46%40"

Start-Process "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=$clientId&scope=openid%20profile%20https://ads.microsoft.com/ads.manage%20offline_access&response_type=code&redirect_uri=https://login.microsoftonline.com/common/oauth2/nativeclient&state=ClientStateGoesHere&prompt=consent"

$code = Read-Host "Grant consent in the browser, and then enter the code here (see ?code=UseThisCode&...)"

$response = Invoke-WebRequest https://login.microsoftonline.com/common/oauth2/v2.0/token -ContentType application/x-www-form-urlencoded -Method POST -Body "client_id=$clientId&scope=https://ads.microsoft.com/ads.manage%20offline_access&code=$code&grant_type=authorization_code&redirect_uri=https%3A%2F%2Flogin.microsoftonline.com%2Fcommon%2Foauth2%2Fnativeclient&client_secret=$client_secret"

$oauthTokens = ($response.Content | ConvertFrom-Json)
Write-Output "Access token: " $oauthTokens.access_token
Write-Output "Access token expires in: " $oauthTokens.expires_in
Write-Output "Refresh token: " $oauthTokens.refresh_token

# The access token will expire e.g., after one hour.
# Use the refresh token to get a new access token.

$response = Invoke-WebRequest https://login.microsoftonline.com/common/oauth2/v2.0/token -ContentType application/x-www-form-urlencoded -Method POST -Body "client_id=$clientId&scope=https://ads.microsoft.com/ads.manage%20offline_access&code=$code&grant_type=refresh_token&refresh_token=$($oauthTokens.refresh_token)&client_secret=$client_secret"

$oauthTokens = ($response.Content | ConvertFrom-Json)
Write-Output "Access token: " $oauthTokens.access_token
Write-Output "Access token expires in: " $oauthTokens.expires_in
Write-Output "Refresh token: " $oauthTokens.refresh_token
"

3.

Run the file ("Get-Tokens-Production.ps1") using PowerShell as administrator and follow the instructions to get Access and Refresh Tokens

# ---- Old Versoin - live connect ----

 * 1. Enter  this url inside 'edge' explorer:
 *  https://login.live.com/oauth20_authorize.srf?client_id=000000004C178591&scope=bingads.manage&response_type=code&redirect_uri=https://login.live.com/oauth20_desktop.srf&state=ClientStateGoesHere
 *
 * 2. change the client id => with the following id -> 000000004C178591
  3. get the code from the redirect link and paste it into the following url: (paste it into the code query param)
      https://login.live.com/oauth20_token.srf?client_id=000000004C178591&scope=bingads.manage&code=paste the code here&grant_type=authorization_code&redirect_uri=https://login.live.com/oauth20_desktop.srf

 * 4. if you did everything right you will get the refresh token:
 * 5. paste in in the => this file (at refresh token param) and also in => storage/app/bing-token.txt
 *


*/