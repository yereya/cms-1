<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sites content type field types
    |--------------------------------------------------------------------------
    |
    | An array which represents all the supported custom fields types
    | and their corresponding metadata fields
    |
    */
    'db_prefix' => env('SITE_DB_PREFIX', 'tpsite'),

    'field_types' => [
        [
            'type' => 'text',
            'metadata' => [
                'placeholder',
                'instructions'
            ]
        ],
        [
            'type' => 'textarea',
            'metadata' => [
                'placeholder',
                'instructions'
            ]
        ],
        [
            'type' => 'wysiwyg',
            'metadata' => [
                'instructions',
                'height'
            ]
        ],
        [
            'type' => 'checkbox',
            'metadata' => [
                'instructions'
            ]
        ],
        [
            'type' => 'checkboxes',
            'metadata' => [
                'instructions',
                'checkboxes'
            ]
        ],
        [
            'type' => 'select',
            'metadata' => [
                'instructions',
                'select'
            ]
        ],
        [
            'type' => 'number',
            'metadata' => [
                'instructions'
            ]
        ],
        [
            'type' => 'email',
            'metadata' => [
                'instructions',
            ]
        ],
        [
            'type' => 'url',
            'metadata' => [
                'instructions',
            ]
        ],
        [
            'type' => 'image',
            'metadata' => [
                'instructions',
            ]
        ],
        [
            'type' => 'multi_select',
            'metadata' => [
                'instructions',
                'multi_select'
            ]
        ],
//        [
//            'type' => 'dropdown',
//            'metadata' => [
//                'instructions',
//            ]
//        ],
//        [
//            'type' => 'radio_buttons',
//            'metadata' => [
//                'instructions'
//            ]
//        ],
        [
            'type' => 'date_picker',
            'metadata' => [
                'instructions',
                'date-format'
            ]
        ],
        [
            'type' => 'phone_number',
            'metadata' => [
                'instructions'
            ]
        ],
        [
            'type' => 'currency',
            'metadata' => [
                'instructions'
            ]
        ],
        [
            'type' => 'percent',
            'metadata' => [
                'instructions'
            ]
        ],
        [
            'type' => 'content_type_link',
            'metadata' => [
                'instructions',
                'content_type_link'
            ]
        ],
//        [
//            'type' => 'video',
//            'metadata' => [
//                'instructions'
//            ]
//        ],
//        [
//            'type' => 'color_picker',
//            'metadata' => [
//                'instructions',
//            ]
//        ]
    ]

];