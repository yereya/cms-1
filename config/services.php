<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'), //need to replace with production
        'secret' => env('MAILGUN_SECRET')
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'unicorn' => [
      'jwt-secret' => env('UNICORN_JWT_SECRET', '1F4KHh82aiD9TwE4BHTi5pqoU1Aq6PD2'),
      'home-url' => env('UNICORN_HOME_URL', 'http://uni.trafficpointltd.com:8080'),
      'hours-for-token-expiration' => env('UNICORN_HOURS_FOR_TOKEN_EXPIRATION', 4),
    ],

    'dali' => [
        'jwt-secret'                 => env('DALI_JWT_SECRET', '1F4KHh82aiD9TwE4BHTi5pqoU1Aq6PD2'),
        'home-url'                   => env('DALI_HOME_URL', 'http://dali.trafficpointltd.com:8080'),
        'hours-for-token-expiration' => env('DALI_HOURS_FOR_TOKEN_EXPIRATION', 4),
    ],

    'tracking_system' => [
       'jwt-secret' => env('TRACKING_SYSTEM_SECRET', 'MiUuxR1ti75LWVgLFKH4mufNa5VJFDV2'),
       'hours-for-token-expiration' => env('TRACKING_SYSTEM_HOURS_FOR_TOKEN_EXPIRATION', 4),
       'tracking_url' => env('TRACKING_URL', 'localhost:3000')
    ],

    'page_speed' => [
        'key' => 'AIzaSyDpTt42PFQlryWk8qKE3Nu7Bdmi2jika8o'
    ]
];
