<?php

return [

    /*
    |--------------------------------------------------------------------------
    | PDO Fetch Style
    |--------------------------------------------------------------------------
    |
    | By default, database results will be returned as instances of the PHP
    | stdClass object; however, you may desire to retrieve records in an
    | array format for simplicity. Here you can tweak the fetch style.
    |
    */

    'fetch' => PDO::FETCH_CLASS,

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'cms'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [


        'cms' => [
            'driver'    => 'mysql',
            'host'      => env('CMS_HOST', 'localhost'),
            'database'  => env('CMS_DATABASE', 'tp_cms'),
            'username'  => env('CMS_USERNAME', 'root'),
            'password'  => env('CMS_PASSWORD', '1234'),
            'port'      => env('CMS_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'sites' => [
            'driver'    => 'mysql',
            'host'      => env('SITES_HOST', 'localhost'),
            'database'  => env('SITES_DATABASE', 'sites'),
            'username'  => env('SITES_USERNAME', 'root'),
            'password'  => env('SITES_PASSWORD', '1234'),
            'port'      => env('SITES_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'bo' => [
            'driver'    => 'mysql',
            'read'      => [
                'host' => env('BO_HOST_READ', 'localhost')
            ],
            'write'     => [
                'host' => env('BO_HOST_WRITE', 'localhost')
            ],
            'database'  => env('BO_DATABASE', 'tp_bo'),
            'username'  => env('BO_USERNAME', 'root'),
            'password'  => env('BO_PASSWORD', '1234'),
            'port'      => env('BO_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'dwh' => [
            'driver'    => 'mysql',
            'read'      => [
                'host' => env('DWH_HOST_READ', 'localhost')
            ],
            'write'     => [
                'host' => env('DWH_HOST_WRITE', 'localhost')
            ],
            'database'  => env('DWH_DATABASE', 'tp_dwh'),
            'username'  => env('DWH_USERNAME', 'root'),
            'password'  => env('DWH_PASSWORD', '1234'),
            'port'      => env('DWH_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'mrr' => [
            'driver'    => 'mysql',
            'read'      => [
                'host' => env('MRR_HOST_READ', 'localhost')
            ],
            'write'     => [
                'host' => env('MRR_HOST_WRITE', 'localhost')
            ],
            'database'  => env('MRR_DATABASE', 'tp_mrr'),
            'username'  => env('MRR_USERNAME', 'root'),
            'password'  => env('MRR_PASSWORD', '1234'),
            'port'      => env('MRR_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'options'   => [PDO::ATTR_EMULATE_PREPARES => true],
            'prefix'    => '',
            'strict'    => false,
        ],

        'stg' => [
            'driver'    => 'mysql',
            'read'      => [
                'host' => env('STG_HOST_READ', 'localhost')
            ],
            'write'     => [
                'host' => env('STG_HOST_WRITE', 'localhost')
            ],
            'database'  => env('STG_DATABASE', 'tp_stg'),
            'username'  => env('STG_USERNAME', 'root'),
            'password'  => env('STG_PASSWORD', '1234'),
            'port'      => env('STG_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'alert' => [
            'driver'    => 'mysql',
            'read'      => [
                'host' => env('ALERT_HOST_READ', 'localhost')
            ],
            'write'     => [
                'host' => env('ALERT_HOST_WRITE', 'localhost')
            ],
            'database'  => env('ALERT_DATABASE', 'tp_alert'),
            'username'  => env('ALERT_USERNAME', 'root'),
            'password'  => env('ALERT_PASSWORD', '1234'),
            'port'      => env('ALERT_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'out' => [
            'driver'    => 'mysql',
            'read'      => [
                'host' => env('OUT_HOST_READ', 'localhost')
            ],
            'write'     => [
                'host' => env('OUT_HOST_WRITE', 'localhost')
            ],
            'database'  => env('OUT_DATABASE', 'tp_alert'),
            'username'  => env('OUT_USERNAME', 'root'),
            'password'  => env('OUT_PASSWORD', '1234'),
            'port'      => env('OUT_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ],

        'putin' => [
            'driver'    => 'mysql',
            'host'      => env('PUTIN_HOST', 'localhost'),
            'database'  => env('PUTIN_DATABASE', 'db_out'),
            'username'  => env('PUTIN_USERNAME', 'root'),
            'password'  => env('PUTIN_PASSWORD', '1234'),
            'port'      => env('PUTIN_PORT', 3306),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
        ]

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer set of commands than a typical key-value systems
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'cluster' => false,

        'default' => [
            'host'     => '127.0.0.1',
            'port'     => 6379,
            'database' => 0,
        ],

    ],

];
