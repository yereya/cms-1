<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInReportBrandScrapers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('reports_brand_scrapers', function (Blueprint $table) {
            $table->string('ioid', 150)->nullable();
            $table->string('trxid', 150)->nullable();
            $table->string('amount', 150)->nullable();
            $table->string('commission_amount', 150)->nullable();
            $table->dropColumn('token');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            $table->text('token');
            $table->dropColumn('ioid');
            $table->dropColumn('trxid');
            $table->dropColumn('amount');
            $table->dropColumn('commission_amount');
        });
    }
}
