<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduler_tasks', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('field_query_params_id')->nullable();
            $table->integer('task_id')->nullable();
            $table->string('task_name', 255)->nullable();
            $table->string('cli_custom_command', 255)->nullable();
            $table->string('cli_command_params', 255)->nullable();
            $table->integer('minutes_from')->nullable();
            $table->integer('minutes_until')->nullable();
            $table->integer('hours_from')->nullable();
            $table->integer('hours_until')->nullable();
            $table->integer('days_from')->nullable();
            $table->integer('days_until')->nullable();
            $table->integer('repeat_delay')->nullable();
            $table->time('run_time_start')->nullable();
            $table->time('run_time_stop')->nullable();
            $table->boolean('active')->default(0);
            $table->integer('log_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scheduler_tasks');
    }
}