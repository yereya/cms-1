<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDisplayFormatColumnToReportsQueryFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_query_fields', function (Blueprint $table) {
            $table->string('display_format')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_query_fields', function (Blueprint $table) {
            $table->dropColumn('display_format');
        });
    }
}
