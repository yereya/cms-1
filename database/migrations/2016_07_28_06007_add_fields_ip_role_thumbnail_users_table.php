<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class AddFieldsIpRoleThumbnailUsersTable
 */
class AddFieldsIpRoleThumbnailUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('permitted_ips', 100)->after('last_name')->nullable();
            $table->string('role', 50)->after('last_name')->nullable();
            $table->string('thumbnail', 255)->after('last_name')->nullable();
            $table->timestamp('password_change_date')->after('password')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('password');
            $table->dropColumn('role');
            $table->dropColumn('thumbnail');
            $table->dropColumn('password_change_date');
        });
    }
}