<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsBrandScrapers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_brand_scrapers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('advertiser_id');
            $table->enum('currency', ['USD', 'EUR', 'GBP'])->default('USD');
            $table->string('username', 50);
            $table->string('password', 100);
            $table->boolean('active');
            $table->string('run_day', 20);
            $table->text('token');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports_brand_scrapers');
    }
}
