<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateReportsPublishersForeignKeys
 */
class CreateReportsPublishersForeignKeys extends Migration
{
    public function up()
    {
        Schema::table('reports_publishers_changes', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });

        Schema::table('reports_publishers_change_days', function (Blueprint $table) {
            $table->foreign('change_id')->references('id')->on('reports_publishers_changes')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    public function down()
    {
        Schema::table('reports_publishers_changes', function (Blueprint $table) {
            $table->dropForeign('reports_publishers_changes_user_id_foreign');
        });

        Schema::table('reports_publishers_change_days', function (Blueprint $table) {
            $table->dropForeign('reports_publishers_change_days_change_id_foreign');
        });
    }
}