<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeEnumTypeScraperProcessors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            DB::statement("ALTER TABLE `reports_brand_scraper_processor` CHANGE `event` `event` ENUM ('lead', 'sale','call', 'install', 'canceled-sale', 'adjustment', 'canceled-lead','deposit')");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            DB::statement("ALTER TABLE `reports_brand_scraper_processor` CHANGE `event` `event` ENUM ('LEAD', 'SALE', 'INSTALL', 'REFUND')");
        });
    }
}
