<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNamesInReportBrandScraperProcessor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            $table->renameColumn('date_value', 'date');
            $table->renameColumn('token_value', 'token');
            $table->renameColumn('trx_id_value', 'trx_id');
            $table->renameColumn('io_id_value', 'io_id');
            $table->dropColumn('commission_amount_value');
            $table->dropColumn('amount_value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            $table->renameColumn('date', 'date_value');
            $table->renameColumn('token', 'token_value');
            $table->renameColumn('trx_id', 'trx_id_value');
            $table->renameColumn('io_id', 'io_id_value');
        });
    }
}
