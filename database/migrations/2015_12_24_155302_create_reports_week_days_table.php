<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateReportsPublishersChangeDaysTable
 */
class CreateReportsWeekDaysTable extends Migration
{
    public function up()
    {
        Schema::create('reports_week_days', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('name', 50);
        });
    }

    public function down()
    {
        Schema::drop('reports_week_days');
    }
}