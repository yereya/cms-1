<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPriorityIntoScssFilesTable extends Migration
{
    /**
     * Add Priority Field To ScssFiles Table
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scss_files',function (Blueprint $table){
            $table->integer('priority')->default(10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scss_files', function (Blueprint $table) {
            $table->dropColumn('priority');
        });
    }
}
