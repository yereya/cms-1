<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScraperAddNewSaveToOutConnectionCheckbox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scrapers', function(Blueprint $table) {
            $table->boolean('save_to_new_adserver')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers', function(Blueprint $table) {
            $table->dropColumn('save_to_new_adserver');
        });
    }
}
