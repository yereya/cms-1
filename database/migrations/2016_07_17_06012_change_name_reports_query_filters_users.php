<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNameReportsQueryFiltersUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('reports_query_filter_users', 'reports_query_filters');

        Schema::table('reports_query_filters', function (Blueprint $table) {
            $table->renameColumn('filter_name', 'name');
            $table->renameColumn('report_query_id', 'query_id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_query_filters', function (Blueprint $table) {
            $table->renameColumn('name', 'filter_name');
            $table->renameColumn('query_id', 'report_query_id');
        });

        Schema::rename('reports_query_filters', 'reports_query_filter_users');
    }
}
