<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldTypeWithTheNameCliCustomCommandInTableSchedulerTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('scheduler_tasks', function(Blueprint $table) {
            $table->text('cli_custom_command')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('scheduler_tasks', function(Blueprint $table) {
            $table->string('cli_custom_command')->change();
        });
    }
}
