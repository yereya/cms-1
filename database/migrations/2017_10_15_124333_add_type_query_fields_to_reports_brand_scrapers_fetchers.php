<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeQueryFieldsToReportsBrandScrapersFetchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scrapers_fetchers', function(Blueprint $table) {
            $table->text('mail_query');
            $table->enum('type', ['cURL', 'Mail', 'Spreadsheet'])->default('cURL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->dropColumn('mail_query');
            $table->dropColumn('type');
        });
    }
}
