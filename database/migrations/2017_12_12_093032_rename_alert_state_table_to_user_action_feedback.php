<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAlertStateTableToUserActionFeedback extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $currentTableName = 'alert_state';
        $newTableName = 'user_action_feedback';
        Schema::rename($currentTableName,$newTableName);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $currentTableName = 'user_action_feedback';
        $newTableName = 'alert_state';
        Schema::rename($currentTableName,$newTableName);
    }
}
