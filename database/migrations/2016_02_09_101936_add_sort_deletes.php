<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSortDeletes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scrapers', function (Blueprint $table) {
            $table->softDeletes();
        });
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
