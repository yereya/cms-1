<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsQueryFilterValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_query_filter_values', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('filter_user_id');
            $table->unsignedInteger('query_field_id');
            $table->string('operator_name', 255);
            $table->string('operator_id', 255);
            $table->string('value_name', 255);
            $table->string('value_id', 255);
            $table->unsignedInteger('order')->default(100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports_query_filter_values');
    }
}
