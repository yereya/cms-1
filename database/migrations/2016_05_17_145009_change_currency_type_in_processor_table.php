<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCurrencyTypeInProcessorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            DB::statement("ALTER TABLE `reports_brand_scraper_processor` CHANGE `currency` `currency` VARCHAR (500)");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            DB::statement("ALTER TABLE `reports_brand_scraper_processor` CHANGE `currency` `currency` ENUM ('USD', 'EUR', 'GBP')");
        });
    }
}
