<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateReportsPublishersKeywordsTable
 */
class CreateReportsPublishersKeywordsTable extends Migration
{
    public function up()
    {
        Schema::create('reports_publishers_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('keyword_id')->unsigned();
            $table->string('keyword', 70)->nullable();
            $table->bigInteger('campaign_id')->unsigned();
            $table->string('campaign', 70)->nullable();
            $table->bigInteger('ad_group_id')->unsigned()->nullable();
            $table->string('ad_group', 70)->nullable();
            $table->string('match_type', 20)->nullable();
            $table->smallInteger('time_zone')->default('0');
            $table->integer('cost')->default('0');
            $table->integer('currency')->unsigned()->default('1');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('reports_publishers_keywords');
    }
}