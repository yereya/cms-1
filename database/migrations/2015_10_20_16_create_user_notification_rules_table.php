<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateUserNotificationRulesTable
 */
class CreateUserNotificationRulesTable extends Migration
{
    public function up()
    {
        Schema::create('user_notification_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['error', 'warning', 'information', 'announcement'])->index();
            $table->text('message');
        });
    }

    public function down()
    {
        Schema::drop('user_notification_rules');
    }
}