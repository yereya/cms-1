<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateAlertsLogTable
 */
class CreateAlertsLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alert_type_id')->unsigned();
            $table->foreign('alert_type_id')->references('id')->on('alert_types');
            $table->string('description')->nullable();
            $table->string('content', 10000)->nullable();
            $table->enum('error_level', ['info', 'notice', 'warning', 'critical'])->default('info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alerts_log');
    }
}