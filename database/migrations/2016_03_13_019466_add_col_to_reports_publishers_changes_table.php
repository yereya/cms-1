<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColToReportsPublishersChangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_publishers_changes', function (Blueprint $table) {
            $table->double('mobile_bid_adjustment', 10, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_publishers_changes', function (Blueprint $table) {
            $table->dropColumn('mobile_bid_adjustment');
        });
    }
}