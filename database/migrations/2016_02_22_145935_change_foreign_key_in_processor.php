<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeForeignKeyInProcessor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scraper_processor', function(Blueprint $table) {
            $table->foreign('scraper_id')->references('id')->on('reports_brand_scrapers')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scraper_processor', function(Blueprint $table) {
            $table->dropForeign('reports_brand_scraper_processor_scraper_id_foreign');
        });
    }
}
