<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateUserLoginAttemptsTable
 */
class CreateUserLoginAttemptsTable extends Migration
{
    public function up()
    {
        Schema::create('user_login_attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('username', 30)->nullable();
            $table->string('ip', 15)->index();
            $table->timestamp('timestamp')->index();
        });
    }

    public function down()
    {
        Schema::drop('user_login_attempts');
    }
}