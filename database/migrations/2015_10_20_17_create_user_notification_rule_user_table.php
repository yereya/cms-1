<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateUserNotificationRuleUserTable
 */
class CreateUserNotificationRuleUserTable extends Migration
{
    public function up()
    {
        Schema::create('user_notification_rule_user', function (Blueprint $table) {
            $table->integer('rule_id')->unsigned();
            $table->integer('user_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::drop('user_notification_rule_user');
    }
}