<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsSystems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('systems', function(Blueprint $table) {
            $table->renameColumn('parent', 'parent_id');
            $table->boolean('show_in_side_menu', 500)->default(0)->after('route');
            $table->string('permission_name')->nullable()->after('route');
            $table->integer('menu_item_order')->default(100)->after('route');
            $table->string('menu_icon')->nullable()->after('route');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('systems', function(Blueprint $table) {
            $table->dropColumn('show_in_side_menu');
            $table->dropColumn('permission_name');
            $table->dropColumn('menu_item_order');
            $table->dropColumn('menu_icon');
            $table->renameColumn('parent_id', 'parent');
        });
    }
}