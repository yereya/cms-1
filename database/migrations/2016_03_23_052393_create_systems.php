<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('controller')->nullable();
            $table->string('namespace')->nullable();
            $table->string('route')->nullable();
            $table->string('parent')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('systems');
    }
}