<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanyInsideBoDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bo')
            ->create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('product');
            $table->string('bank');
            $table->integer('SAP_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('bo')->dropIfExists('company');
    }
}
