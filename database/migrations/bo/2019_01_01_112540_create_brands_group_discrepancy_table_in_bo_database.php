<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsGroupDiscrepancyTableInBoDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('bo')
            ->create('brands_group_discrepancy', function (Blueprint $table) {
            $table->increments('id');
            $table->string('brand_group','255');
            $table->integer('brand_id');
            $table->string('company_name','255');
            $table->integer('company_id');
            $table->enum('currency', ['USD','GBP','EUR']);
            $table->double('currency_rate',8,3);
            $table->double('base_commission_amount',8,2);
            $table->double('commission_amount',8,2);
            $table->integer('month');
            $table->integer('sales_user');
            $table->integer('checker_user');
            $table->text('sale_comment');
            $table->text('checker_comment');
            $table->text('deal_explanation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('bo')
            ->dropIfExists('brands_group_discrepancy');
    }
}
