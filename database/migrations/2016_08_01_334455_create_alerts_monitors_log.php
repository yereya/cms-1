<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateAlertsMonitorsLog
 */
class CreateAlertsMonitorsLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts_monitors_log', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alerts_log_id')->nullable()->unsigned();
            $table->foreign('alerts_log_id')->references('id')->on('alert_types');

            $table->string('client_name')->nullable();
            $table->float('cost', 10,2)->nullable();
//            $table->integer('cpa')->nullable()->unsigned();
//            $table->integer('clicks')->nullable()->unsigned();
//            $table->integer('sales')->nullable()->unsigned();
//            $table->integer('out_clicks')->nullable()->unsigned();
//            $table->integer('site_ctr')->nullable()->unsigned();
//            $table->integer('cpc')->nullable()->unsigned();
//            $table->integer('lead')->nullable()->unsigned();
//            $table->integer('cpl')->nullable()->unsigned();
//            $table->integer('tp-revenue')->nullable()->unsigned();
//            $table->integer('profit')->nullable()->unsigned();
//            $table->integer('roi')->nullable()->unsigned();
//            $table->integer('ctl')->nullable()->unsigned();
//            $table->integer('lts')->nullable()->unsigned();
//            $table->integer('cts')->nullable()->unsigned();
//            $table->float('earning_per_sale')->nullable();
//            $table->integer('position')->nullable()->unsigned();
//            $table->integer('impressions')->nullable()->unsigned();
//            $table->integer('ctr')->nullable()->unsigned();
//            $table->integer('expect_increase_in_clicks')->nullable()->unsigned();
//            $table->string('account_name')->nullable();
//            $table->string('campaign_name')->nullable();
//            $table->string('ad_group_name')->nullable();
//            $table->string('keywords')->nullable();
//            $table->string('device')->nullable();
//            $table->string('match_type')->nullable()->unsigned();
//            $table->string('hour')->nullable()->unsigned();
//            $table->string('week_day')->nullable();
//            $table->string('country')->nullable();
//            $table->string('placement')->nullable();
//            $table->string('search_term')->nullable();
//            $table->string('ads')->nullable();
//            $table->string('state_region')->nullable();
//            $table->string('theme')->nullable();
//            $table->float('campaign_age')->nullable();
//            $table->string('target_cpa')->nullable();
//            $table->string('epl')->nullable()->unsigned();
//            $table->string('date_range')->nullable()->unsigned();
//            $table->integer('number_of_days')->nullable()->unsigned();
//            $table->float('avg_click')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alerts_monitors_log');
    }
}