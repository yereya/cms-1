<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnMessageLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        DB::statement('ALTER TABLE `logs` CHANGE `message` `message` MEDIUMTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        Schema::table('logs', function (Blueprint $table) {
            $table->renameColumn('running_in_console', 'initiator_user_id')->change();
            $table->integer('running_in_console')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
        
        DB::statement('ALTER TABLE `logs` CHANGE `message` `message` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;');
        Schema::table('logs', function (Blueprint $table) {
            $table->renameColumn('initiator_user_id', 'running_in_console')->change();
            $table->tinyInteger('running_in_console')->change();
        });
    }
}