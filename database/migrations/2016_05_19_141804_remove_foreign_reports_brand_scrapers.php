<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveForeignReportsBrandScrapers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scrapers', function(Blueprint $table) {
            $table->dropForeign('reports_brand_scrapers_advertiser_id_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers', function(Blueprint $table) {
            $table->foreign('advertiser_id')->references('id')->on('reports_publishers_advertisers')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }
}
