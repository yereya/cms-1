<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNameReportsQueryFilterProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('reports_query_filter_values', 'reports_query_filters_properties');

        Schema::table('reports_query_filters_properties', function (Blueprint $table) {
            $table->renameColumn('filter_user_id', 'filter_id');
            $table->dropColumn('value_id');
            $table->renameColumn('value_name', 'value');
            $table->dropColumn('operator_id');
            $table->renameColumn('operator_name', 'operator');
        });

        Schema::table('reports_query_filters_properties', function (Blueprint $table) {
            $table->string('operator', 255)->nullable()->change();
            $table->string('value', 255)->nullable()->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_query_filters_properties', function (Blueprint $table) {
            $table->renameColumn('filter_id', 'filter_user_id');
        });

        Schema::rename('reports_query_filters_properties', 'reports_query_filter_values');
    }
}
