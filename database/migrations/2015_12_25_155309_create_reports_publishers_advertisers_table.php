<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateReportsPublishersAdvertisersTable
 */
class CreateReportsPublishersAdvertisersTable extends Migration
{
    public function up()
    {
        Schema::create('reports_publishers_advertisers', function (Blueprint $table) {
            $table->string('id', 100);
            $table->string('name', 45)->nullable();
            $table->string('status', 45)->nullable();
            $table->string('type', 45)->nullable();
            $table->string('timezone', 45)->default('+00:00');
            $table->primary('id');
        });
    }

    public function down()
    {
        Schema::drop('reports_publishers_advertisers');
    }
}