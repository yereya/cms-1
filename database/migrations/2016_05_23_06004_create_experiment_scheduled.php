<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperimentScheduled extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiment_scheduled', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('system_id')->nullable();
            $table->integer('task_id')->nullable();
            $table->string('params', 1000)->nullable();
            $table->timestamp('run_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('experiment_scheduled');
    }
}