<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateReportsPublishersChangesTable
 */
class CreateReportsPublishersChangesTable extends Migration
{
    public function up()
    {
        Schema::create('reports_publishers_changes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->enum('change_level', ['campaign', 'ad_group', 'keyword', 'scheduling'])->default('campaign');
            $table->bigInteger('account_id')->unsigned();
            $table->bigInteger('campaign_id')->unsigned()->nullable();
            $table->bigInteger('ad_group_id')->unsigned()->nullable();
            $table->bigInteger('keyword_id')->unsigned()->nullable();
            $table->text('comment')->nullable();
            $table->float('bid', 8, 2)->nullable();
            $table->float('bid_multiplier', 10, 2)->nullable();
            $table->float('budget', 14, 2)->nullable();
            $table->integer('desktop')->nullable();
            $table->integer('tablet')->nullable();
            $table->integer('mobile')->nullable();
            $table->integer('state')->nullable();
            $table->timestamp('date_time_changed');
            $table->time('time_from')->nullable();
            $table->time('time_until')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::drop('reports_publishers_changes');
    }

}