<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monitors', function(Blueprint $table){
            $table->string('problem',1024);
            $table->string('query_id',32);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monitors', function(Blueprint $table){
            $table->dropColumn('problem');
            $table->dropColumn('query_id');
        });
    }
}
