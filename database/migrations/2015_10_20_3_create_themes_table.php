<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateThemesTable
 */
class CreateThemesTable extends Migration
{
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->index();
            $table->boolean('is_active')->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('themes');
    }
}