<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportQueryUserFilter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_query_user_filter', function(Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('filter_id');
            $table->unique(['user_id', 'filter_id'], 'unique');
            $table->foreign('filter_id')->references('id')->on('reports_query_filters')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports_query_user_filter');
    }
}
