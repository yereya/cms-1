<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateForeignKeys
 */
class CreateForeignKeys extends Migration
{
    public function up()
    {
        Schema::table('user_login_attempts', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('restrict');
        });
        Schema::table('permission_role', function (Blueprint $table) {
            $table->foreign('role_id')->references('id')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('permission_role', function (Blueprint $table) {
            $table->foreign('permission_id')->references('id')->on('permissions')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('user_notifications', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('user_notifications', function (Blueprint $table) {
            $table->foreign('user_notification_rule_id')->references('id')->on('user_notification_rules')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('widget_fields', function (Blueprint $table) {
            $table->foreign('widget_id')->references('id')->on('widgets')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('widget_fields', function (Blueprint $table) {
            $table->foreign('widget_field_type_id')->references('id')->on('widget_field_types')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
        Schema::table('user_notification_rule_user', function (Blueprint $table) {
            $table->foreign('rule_id')->references('id')->on('user_notification_rules')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('user_notification_rule_user', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    public function down()
    {
        Schema::table('user_login_attempts', function (Blueprint $table) {
            $table->dropForeign('user_login_attempts_user_id_foreign');
        });
        Schema::table('role_permission', function (Blueprint $table) {
            $table->dropForeign('role_permission_role_id_foreign');
        });
        Schema::table('role_permission', function (Blueprint $table) {
            $table->dropForeign('role_permission_permission_id_foreign');
        });
        Schema::table('user_notifications', function (Blueprint $table) {
            $table->dropForeign('user_notifications_user_id_foreign');
        });
        Schema::table('user_notifications', function (Blueprint $table) {
            $table->dropForeign('user_notifications_user_notification_rule_id_foreign');
        });
        Schema::table('widget_fields', function (Blueprint $table) {
            $table->dropForeign('widget_fields_widget_id_foreign');
        });
        Schema::table('widget_fields', function (Blueprint $table) {
            $table->dropForeign('widget_fields_widget_field_type_id_foreign');
        });
        Schema::table('user_notification_rule_user', function (Blueprint $table) {
            $table->dropForeign('user_notification_rule_user_rule_id_foreign');
        });
        Schema::table('user_notification_rule_user', function (Blueprint $table) {
            $table->dropForeign('user_notification_rule_user_user_id_foreign');
        });
    }
}