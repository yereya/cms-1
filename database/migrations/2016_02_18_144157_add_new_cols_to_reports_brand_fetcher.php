<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColsToReportsBrandFetcher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->string('translator', 100)->nullable();
            $table->string('translator_params', 3000)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->dropColumn('translator');
            $table->dropColumn('translator_params');
        });
    }
}
