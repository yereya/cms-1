<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateReportsPublishersChangeDaysTable
 */
class CreateReportsPublishersChangeDaysTable extends Migration
{
    public function up()
    {
        Schema::create('reports_publishers_change_days', function (Blueprint $table) {
            $table->integer('change_id')->unsigned();
            $table->enum('day', ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'])
                ->index();
            $table->primary(array('change_id', 'day'));
        });
    }

    public function down()
    {
        Schema::drop('reports_publishers_change_days');
    }
}