<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusStateToReportsPublishersKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_publishers_keywords', function (Blueprint $table) {
            $table->string('keyword_state', 100)->nullable();
            $table->string('campaign_state', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_publishers_keywords', function (Blueprint $table) {
            $table->dropColumn('keyword_state');
            $table->dropColumn('campaign_state');
        });
    }
}
