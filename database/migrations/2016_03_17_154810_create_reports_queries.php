<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsQueries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_queries', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('controller_name', 100);
            $table->text('query');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('reports_query_selectors', function(Blueprint $table) {
            $table->foreign('query_id')->references('id')->on('reports_queries')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_query_selectors', function(Blueprint $table) {
            $table->dropForeign('reports_query_selectors_query_id_foreign');
        });

        Schema::drop('reports_queries');
    }
}
