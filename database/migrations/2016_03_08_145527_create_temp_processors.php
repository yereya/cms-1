<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTempProcessors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_processors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scraper_id')->unsigned()->nullable();
            $table->integer('brand_id')->unsigned()->nullable();
            $table->integer('advertiser_id')->unsigned()->nullable();
            $table->string('date_value', 500)->nullable();
            $table->string('token_value', 500)->nullable();
            $table->enum('event', ['LEAD', 'SALE', 'INSTALL', 'REFUND'])->nullable();
            $table->string('trx_id_value', 500)->nullable();
            $table->string('io_id_value', 500)->nullable();
            $table->double('commission_amount')->nullable();
            $table->string('commission_amount_value', 500)->nullable();
            $table->double('amount')->nullable();
            $table->string('amount_value', 500)->nullable();
            $table->enum('currency', ['USD', 'EUR', 'GBP']);
            $table->string('rule', 3000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('temp_processors');
    }
}
