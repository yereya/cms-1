<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSSLVerificationColumnToReportBrandScrapersFetchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->boolean('ssl_certificate_verification')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->dropColumn('ssl_certificate_verification');
        });
    }
}