<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldQueryParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields_query_params', function(Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['select2', 'datatable', 'togglable', 'scheduler_tasks'])->default('select2');
            $table->string('display_name')->nullable();
            $table->integer('system_id')->nullable();
            $table->string('selector')->nullable();
            $table->string('options_query', 5000)->nullable();
            $table->enum('db', ['cms', 'bo', 'ad'])->default('cms');
            $table->enum('input_type', ['select', 'textarea', 'text', 'checkbox', 'multiselect', 'date', 'datetime', 'time'])->nullable();
            $table->string('attributes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fields_query_params');
    }
}