<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDefaultTypeReportsPublishersAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_publishers_accounts', function (Blueprint $table) {
            DB::statement("ALTER TABLE `reports_publishers_accounts` CHANGE `type` `type` ENUM ('Inactive', 'Display', 'Search', 'Mcc') NOT NULL");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_publishers_accounts', function (Blueprint $table) {
            DB::statement("ALTER TABLE `reports_publishers_accounts` CHANGE `type` `type` ENUM ('Display', 'Search', 'Mcc', 'Inactive') NOT NULL");
        });
    }
}
