<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScraperBrandProcessor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_brand_scraper_processor', function(Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('scraper_id');
            $table->string('date_value', 500)->nullable();
            $table->string('date_format', 3000)->nullable();
            $table->string('token_value', 500)->nullable();
            $table->string('token_format', 3000)->nullable();
            $table->enum('event', ['LEAD', 'SALE', 'INSTALL', 'REFUND'])->nullable();
            $table->string('trx_id_value', 500)->nullable();
            $table->string('trx_id_format', 3000)->nullable();
            $table->string('io_id_value', 500)->nullable();
            $table->string('io_id_format', 3000)->nullable();
            $table->double('commission_amount')->nullable();
            $table->double('amount')->nullable();
            $table->enum('currency', ['USD', 'EUR', 'GBP']);
            $table->string('rule', 3000)->nullable();
            $table->boolean('active')->default(1);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports_brand_scraper_processor');
    }
}
