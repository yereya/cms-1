<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatementTypeToHolsteredQueries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('holstered_queries', function(Blueprint $table) {
            $table->renameColumn('type', 'db');
            $table->enum('statement', ['delete', 'insert', 'update', 'select', 'statement'])->default('select')->after('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holstered_queries', function(Blueprint $table) {
            $table->dropColumn('statement');
        });
    }
}