<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNameReportBrandScrapers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scrapers', function (Blueprint $table) {
            $table->dropColumn('run_day');
            $table->enum('days_ago', ['today', 'yesterday'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers', function (Blueprint $table) {
            $table->integer('run_day');
            $table->dropColumn('days_ago', ['today', 'yesterday'])->nullable();
        });
    }
}
