<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAlertTypeIdColAlertsUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('alerts_users', function(Blueprint $table) {
            $table->renameColumn('alert_type_id', 'alert_log_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alerts_users', function(Blueprint $table) {
            $table->renameColumn('alert_log_id', 'alert_type_id');
        });
    }
}