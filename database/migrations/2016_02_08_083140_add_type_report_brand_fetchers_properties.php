<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeReportBrandFetchersProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_fetchers_properties', function(Blueprint $table) {
           $table->integer('type_field');
        });
        Schema::table('reports_brand_advance_properties', function(Blueprint $table) {
           $table->integer('type_field');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_fetchers_properties', function(Blueprint $table) {
           $table->dropColumn('type_field');
        });
        Schema::table('reports_brand_advance_properties', function(Blueprint $table) {
            $table->dropColumn('type_field');
        });
    }
}
