<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsLabelPermissionsToReportQueryFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_query_fields', function(Blueprint $table) {
            $table->string('label_description')->nullable();
            $table->string('permission')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_query_fields', function(Blueprint $table) {
            $table->dropColumn('label_description');
            $table->dropColumn('permission');
        });
    }
}
