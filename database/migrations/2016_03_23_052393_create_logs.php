<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('entry_record_id')->nullable()->unsigned();
            $table->integer('system_id')->nullable()->unsigned();
            $table->integer('task_id')->nullable()->unsigned();
            $table->string('class_name')->nullable();
            $table->string('function_name')->nullable();
            $table->text('message')->nullable();
            $table->enum('error_type', ['info', 'warning', 'error', 'notice', 'debug'])->default('info');
            $table->integer('run_duration')->nullable()->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logs');
    }
}