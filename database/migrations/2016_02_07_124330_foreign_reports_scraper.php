<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignReportsScraper extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scrapers', function(Blueprint $table) {
            $table->foreign('advertiser_id')->references('id')->on('reports_publishers_advertisers')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('reports_brand_advance_properties', function(Blueprint $table) {
            $table->foreign('scraper_id')->references('id')->on('reports_brand_scrapers')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->foreign('scraper_id')->references('id')->on('reports_brand_scrapers')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('reports_brand_fetchers_properties', function (Blueprint $table) {
            $table->foreign('fetcher_id')->references('id')->on('reports_brand_scrapers_fetchers')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers', function(Blueprint $table) {
            $table->dropForeign('reports_brand_scrapers_advertiser_id_foreign');
        });
        Schema::table('reports_brand_advance_properties', function(Blueprint $table) {
            $table->dropForeign('reports_brand_advance_properties_scraper_id_foreign');
        });
        Schema::table('reports_brand_scrapers_fetchers', function(Blueprint $table) {
            $table->dropForeign('reports_brand_scrapers_fetchers_scraper_id_foreign');
        });
        Schema::table('reports_brand_fetchers_properties', function(Blueprint $table) {
            $table->dropForeign('reports_brand_fetchers_properties_fetcher_id_foreign');
        });
    }
}
