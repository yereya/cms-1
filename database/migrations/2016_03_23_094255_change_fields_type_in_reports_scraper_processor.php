<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldsTypeInReportsScraperProcessor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');
        
        Schema::table('reports_brand_scraper_processor', function(Blueprint $table) {
            $table->string('commission_amount', 500)->nullable()->change();
            $table->string('amount', 500)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('reports_brand_scraper_processor', function(Blueprint $table) {
             $table->double('commission_amount')->nullable()->change();
             $table->double('amount')->nullable()->change();
         });
    }
}
