<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveDevicesReportsPublishersChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_publishers_changes', function (Blueprint $table) {
            $table->dropColumn('mobile');
            $table->dropColumn('tablet');
            $table->dropColumn('desktop');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_publishers_changes', function (Blueprint $table) {
            $table->boolean('mobile')->nullable();
            $table->boolean('tablet')->nullable();
            $table->boolean('desktop')->nullable();
        });
    }
}