<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperimentTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('experiment_tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('system_id')->nullable();
            $table->integer('system_task_id')->nullable();
            $table->string('params', 1000)->nullable();
            $table->string('command', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('experiment_tasks');
    }
}