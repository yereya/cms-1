<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldDisableHttpErrorsToReportsBrandScrapersFetchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->tinyInteger('disable_http_errors')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scrapers_fetchers', function (Blueprint $table) {
            $table->dropColumn('disable_http_errors');
        });
    }
}
