<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastRunLastEffectiveFieldsHolsteredQueries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('holstered_queries', function(Blueprint $table) {
            $table->timestamp('last_run')->after('system_id')->nullable();
            $table->timestamp('last_effective_run')->after('system_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holstered_queries', function(Blueprint $table) {
            $table->dropColumn('last_run');
            $table->dropColumn('last_effective_run');
        });
    }
}