<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateUserNotificationsTable
 */
class CreateUserNotificationsTable extends Migration
{
    public function up()
    {
        Schema::create('user_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('user_notification_rule_id')->unsigned();
            $table->enum('location', ['top', 'modal'])->index();
            $table->boolean('is_viewed')->index();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('user_notifications');
    }
}