<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableAlertState extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alert_state',function (Blueprint $table){
            $table->increments('id');
            $table->integer('dynamic_id');
            $table->integer('user_id')->unsigned()->references('id')->on('users');
            $table->string('state');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alert_state');
    }
}
