<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqueAccountIdInReportsPublishersAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_publishers_accounts', function(Blueprint $table) {
            $table->unique('account_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_publishers_accounts', function(Blueprint $table) {
            $table->dropUnique('account_id');
        });
    }
}
