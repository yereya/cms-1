<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateSiteThemeTable
 */
class CreateSiteThemeTable extends Migration
{
    public function up()
    {
        Schema::create('site_theme', function (Blueprint $table) {
            $table->integer('site_id')->unsigned();
            $table->integer('theme_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::drop('site_theme');
    }
}