<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ChangePermissionsUniqueTable
 */
class ChangePermissionsUniqueTable extends Migration
{
    public function up()
    {
        $platform = Schema::getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Schema::table('user_permissions', function (Blueprint $table) {
            $table->string('name', 128)->change();
            $table->unique(['name', 'type'], 'unique');
        });
    }

    public function down()
    {
        Schema::table('user_permissions', function (Blueprint $table) {
            $table->string('name', 1024)->change();
            $table->dropUnique('unique');
        });
    }
}