<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitors',function(Blueprint $table){
            $table->increments('id');
            $table->string('name',1024);
            $table->string('category',64);
            $table->string('department',64);
            $table->string('target',1024)->nullable();
            $table->string('date_range',1024)->nullable();
            $table->string('segmentation_level',1024)->nullable();
            $table->string('calculate_kpi',1024)->nullable();
            $table->string('table',1024)->nullable();
            $table->string('data_calculation',1024)->nullable();
            $table->string('filter',1024)->nullable();
            $table->string('aggregation_type',1024)->nullable();
            $table->string('unique_key',1024)->nullable();
            $table->string('sending_monitor',1024)->nullable();
            $table->string('run_time',1024)->nullable();
            $table->string('advertiser',1024)->nullable();
            $table->string('recommendation',1024)->nullable();
            $table->string('new_advertiser',1024)->nullable();
            $table->string('definition',1024)->nullable();
            $table->string('report_column',1024)->nullable();
            $table->string('comment',1024)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('monitors');
    }
}
