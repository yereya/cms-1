<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportSelectQueries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_query_selectors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('query_id')->unsigned();
            $table->string('selector', 100);
            $table->text('query')->nullable();
            $table->string('input_type', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports_query_selectors');
    }
}
