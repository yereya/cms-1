<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoIpLocationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_location_countries', function (Blueprint $table) {
            $table->integer('geoname_id')->unsigned()->primary();
            $table->string('locale_code')->index();
            $table->string('continent_code')->index();
            $table->string('continent_name');
            $table->string('country_iso_code')->index();
            $table->string('country_name');

            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::create('geo_location_country_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('network')->unique();
            $table->integer('network_subnet')->index();
            $table->integer('range_start')->index();
            $table->integer('range_end')->index();
            $table->integer('geoname_id')->unsigned();
            $table->integer('registered_country_geoname_id')->nullable();
            $table->integer('represented_country_geoname_id')->nullable();
            $table->boolean('is_anonymous_proxy')->index();
            $table->boolean('is_satellite_provider')->index();

            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::table('geo_location_country_blocks', function (Blueprint $table) {
            $table->foreign('geoname_id')->references('geoname_id')->on('geo_location_countries')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('geo_location_cities', function (Blueprint $table) {
            $table->integer('geoname_id')->unsigned()->primary();
            $table->string('locale_code')->index();
            $table->string('continent_code')->index();
            $table->string('continent_name');
            $table->string('country_iso_code')->index();
            $table->string('country_name');
            $table->string('subdivision_1_iso_code')->nullable();
            $table->string('subdivision_1_name')->nullable();
            $table->string('subdivision_2_iso_code')->nullable();
            $table->string('subdivision_2_name')->nullable();
            $table->string('city_name')->nullable();
            $table->string('metro_code')->nullable();
            $table->string('time_zone')->nullable();

            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::create('geo_location_city_blocks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('network')->unique();
            $table->integer('network_subnet')->index();
            $table->integer('range_start')->index();
            $table->integer('range_end')->index();
            $table->integer('geoname_id')->unsigned();
            $table->integer('registered_country_geoname_id')->nullable();
            $table->integer('represented_country_geoname_id')->nullable();
            $table->boolean('is_anonymous_proxy')->index();
            $table->boolean('is_satellite_provider')->index();
            $table->string('postal_code')->nullable();
            $table->float('latitude', 8, 4)->nullable();
            $table->float('longitude', 8, 4)->nullable();
            $table->integer('accuracy_radius')->nullable();

            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
        });

        Schema::table('geo_location_city_blocks', function (Blueprint $table) {
            $table->foreign('geoname_id')->references('geoname_id')->on('geo_location_cities')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_location_city_blocks');
        Schema::dropIfExists('geo_location_cities');
        Schema::dropIfExists('geo_location_country_blocks');
        Schema::dropIfExists('geo_location_countries');
    }
}
