<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveForeignFromUserAccountAssign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_assigned_accounts', function(Blueprint $table) {
            $table->dropForeign('assign_accounts_account_id_foreign');
        });

        Schema::table('user_assigned_accounts', function(Blueprint $table) {
            $table->unique(['user_id', 'account_id'], 'unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_assigned_accounts', function(Blueprint $table) {
            $table->foreign('user_assigned_accounts')->references('id')->on('account')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }
}
