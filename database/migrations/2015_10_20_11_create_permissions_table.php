<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreatePermissionsTable
 */
class CreatePermissionsTable extends Migration
{
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', ['view', 'add', 'edit', 'delete'])->index();
            $table->string('name', 60)->index();
            $table->text('description')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('permissions');
    }
}