<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ChangeColPassChangeDateUsersTable
 */
class ChangeColPassChangeDateUsersTable extends Migration
{
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('password_change_date', 'password_expiry_date');
        });
        Schema::table('users', function (Blueprint $table) {
            $table->date('password_expiry_date')->change();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp('password_expiry_date')->change();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->renameColumn('password_expiry_date', 'password_change_date');
        });
    }
}