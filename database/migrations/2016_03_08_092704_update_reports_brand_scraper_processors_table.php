<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateReportsBrandScraperProcessorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            $table->dropColumn('date_format');
            $table->dropColumn('token_format');
            $table->dropColumn('trx_id_format');
            $table->dropColumn('io_id_format');
            $table->string('commission_amount_value', 500)->nullable();
            $table->string('amount_value', 500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_brand_scraper_processor', function (Blueprint $table) {
            $table->string('date_format', 3000)->nullable();
            $table->string('token_format', 3000)->nullable();
            $table->string('trx_id_format', 3000)->nullable();
            $table->string('io_id_format', 3000)->nullable();
            $table->dropColumn('commission_amount_value');
            $table->dropColumn('amount_value');
        });
    }
}
