<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsBrandScraperAdvanceProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_brand_advance_properties', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('scraper_id')->unsigned();
            $table->string('key', 255);
            $table->text('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports_brand_advance_properties');
    }
}
