<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueFromTypeIndexFromTableSiteSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sites')
            ->table('site_settings',function(Blueprint $table){
                $table->dropUnique('type_key_index');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sites')
            ->table('site_settings',function(Blueprint $table){
                $table->unique(array('key', 'type'));
            });

    }
}
