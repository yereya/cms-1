<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsRouteColumnInSiteContentTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sites')
        ->table('site_content_types',function(Blueprint $table){
            $table->integer('is_router')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sites')
        ->table('site_content_types',function(Blueprint $table){
            $table->dropColumn('is_router');
        });
    }
}

