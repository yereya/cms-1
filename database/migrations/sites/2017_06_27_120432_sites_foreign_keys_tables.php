<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SitesForeignKeysTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * FK site content types
         */
        Schema::connection('sites')->table('site_content_types', function (Blueprint $table) {
            $table->foreign('site_id')
                ->references('id')
                ->on('sites')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::connection('sites')->table('site_content_type_field_groups', function (Blueprint $table) {
            $table->foreign('site_id')
                ->references('id')
                ->on('sites')
                ->onDelete('cascade');
        });

        Schema::connection('sites')->table('site_widget_templates', function (Blueprint $table) {
            $table->foreign('widget_id')
                ->references('id')
                ->on('site_widgets')
                ->onDelete('cascade');
        });

        Schema::connection('sites')->table('site_settings', function (Blueprint $table) {
            $table->foreign('field_type_id')
                ->references('id')
                ->on('site_widget_field_types');
        });

        Schema::connection('sites')->table('site_settings_defaults', function (Blueprint $table) {
            $table->foreign('field_type_id')
                ->references('id')
                ->on('site_widget_field_types');
        });

        Schema::connection('sites')->table('site_widget_fields', function(Blueprint $table) {
            $table->foreign('type_id')
                ->references('id')
                ->on('site_widget_field_types')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });

        Schema::connection('sites')->table('site_settings', function (Blueprint $table) {
            $table->foreign('site_id')
                ->references('id')
                ->on('sites');
            $table->foreign('type_id')
                ->references('id')
                ->on('site_widget_fields');
        });

        Schema::connection('sites')->table('site_navigation', function (Blueprint $table) {
            $table->foreign('parent_id')
                ->references('id')
                ->on('site_navigation')
                ->onDelete('cascade');
        });

        Schema::connection('sites')->table('site_user_permission', function (Blueprint $table) {
            $table->foreign('site_id')
                ->references('id')
                ->on('sites')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });

        Schema::connection('sites')->table('site_domains', function (Blueprint $table) {
            $table->foreign('site_id')
                ->references('id')
                ->on('sites')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });

        Schema::connection('sites')->table('site_user_role', function (Blueprint $table) {
            $table->foreign('site_id')
                ->references('id')
                ->on('sites')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sites')->table('site_content_type_fields', function (Blueprint $table) {
            $table->dropForeign('site_content_type_fields_type_id_foreign');
        });

        Schema::connection('sites')->table('site_content_types', function (Blueprint $table) {
            $table->dropForeign('site_content_types_site_id_foreign');
        });

        Schema::connection('sites')->table('site_widget_fields' , function(Blueprint $table) {
            $table->dropForeign('site_widget_fields_type_id_foreign');
        });

        Schema::connection('sites')->table('site_user_role', function (Blueprint $table) {
            $table->dropForeign('site_user_role_site_id_foreign');
            $table->dropForeign('site_user_role_user_id_foreign');
            $table->dropForeign('site_user_role_role_id_foreign');
        });

        Schema::connection('sites')->table('site_user_permission', function (Blueprint $table) {
            $table->dropForeign('site_user_permission_site_id_foreign');
            $table->dropForeign('site_user_permission_user_id_foreign');
            $table->dropForeign('site_user_permission_permission_id_foreign');
        });

        Schema::connection('sites')->table('site_domains', function (Blueprint $table) {
            $table->dropForeign('domains_site_id_foreign');
        });
    }
}
