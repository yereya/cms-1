<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqnessToSiteIdSlugAndTableTogeterInSiteContentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sites')
            ->table('site_content_types',function(Blueprint $table){
            $table->unique(array('site_id','slug','table'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sites')
            ->table('site_content_types',function(Blueprint $table){
            $table->dropUnique('site_content_types_site_id_slug_table_unique');
        });
    }
}
