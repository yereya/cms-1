<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SitesBaseTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Create sites table
         */
        Schema::connection('sites')->create('sites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->boolean('is_public')->index()->default(1);
            $table->integer('publisher_id')->unsigned();
            $table->string('db_host', 255);
            $table->string('db_user', 255);
            $table->string('db_pass', 1024);
            $table->string('db_name', 255);
            $table->timestamps();
            $table->softDeletes();
        });

        /**
         * Create site widgets - widgets
         */
        Schema::connection('sites')->create('site_widgets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('controller', 100);
            $table->string('view')->nullable();
        });

        /**
         * Create site widget templates
         */
        Schema::connection('sites')->create('site_widget_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->integer('widget_id')->unsigned();
            $table->boolean('published')->nullable();
            $table->string('file_name', 100)->nullable();
            $table->string('description')->nullable();
            $table->string('name', 100)->nullable();
            $table->text('blade');
            $table->string('scss', 2048)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        /**
         * Create site widget fields
         */
        Schema::connection('sites')->create('site_widget_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('widget_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->string('name')->index();
            $table->string('title', 100);
            $table->string('default', 30)->nullable();
            $table->text('help');
            $table->string('permission')->index();
        });

        /**
         * Create site widget field types
         */
        Schema::connection('sites')->create('site_widget_field_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('controller', 100);
        });

        /**
         * Create site settings defaults
         */
        Schema::connection('sites')->create('site_settings_defaults', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type', 100);
            $table->string('key', 100);
            $table->string('value')->nullable();
            $table->integer('field_type_id')->unsigned();
            $table->string('options_query', 2048)->nullable();
            $table->integer('priority')->default(1);
            $table->timestamps();
        });

        /**
         * Create site settings
         */
        Schema::connection('sites')->create('site_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->string('name');
            $table->string('type', 100);
            $table->string('key', 100);
            $table->string('value', 2500)->nullable();
            $table->integer('field_type_id')->unsigned();
            $table->string('options_query', 2048)->nullable();
            $table->integer('priority')->default(100);
            $table->timestamps();
            $table->unique(['type', 'key']);
        });

        /**
         * Create site navigation
         */
        Schema::connection('sites')->create('site_navigation', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100)->nullable();
            $table->string('menu_icon', 100)->nullable();
            $table->string('route', 100)->nullable();
            $table->string('route_params')->nullable();
            $table->string('permission_name', 100)->nullable();
            $table->string('children_query', 1000)->nullable();
            $table->integer('order')->default(50)->unsigned();
            $table->integer('parent_id')->unsigned();
        });

        /**
         * Create site domains
         */
        Schema::connection('sites')->create('site_domains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->string('domain', 100)->unique();
            $table->boolean('domain_dev')->default(0);
            $table->timestamps();
        });

        /**
         * Create site devices
         */
        Schema::connection('sites')->create('site_devices', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('group', ['web', 'tablet', 'mobile'])->nullable()->index();
            $table->string('name')->unique();
            $table->text('user_agent');
        });

        /**
         * Create site content types
         */
        Schema::connection('sites')->create('site_content_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned()->index();
            $table->string('name');
            $table->string('slug', 64);
            $table->string('table');
            $table->boolean('has_comments')->index();
            $table->boolean('has_likes')->index();
            $table->unique('table');
            $table->unique('slug');
            $table->index('site_id');
        });

        /**
         * Create site content type fields
         */
        Schema::connection('sites')->create('site_content_type_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id')->unsigned()->index();
            $table->string('name');
            $table->string('display_name');
            $table->string('type');
            $table->tinyInteger('is_required')->default(0);
            $table->string('default');
            $table->tinyInteger('priority')->nullable();
            $table->enum('index', ['index', 'unique', 'full_text'])->nullable();
            $table->string('metadata', 8192)->nullable();
            $table->integer('field_group_id')->unsigned()->nullable();
            $table->timestamps();
        });

        /**
         * Create site content type field groups
         */
        Schema::connection('sites')->create('site_content_type_field_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->unsigned();
            $table->integer('content_type_id')->unsigned();
            $table->string('name', 100)->nullable();
            $table->integer('priority')->default(50)->unsigned();
            $table->integer('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sites')->dropIfExists('site_content_type_field_groups');
        Schema::connection('sites')->dropIfExists('site_content_type_fields');
        Schema::connection('sites')->dropIfExists('site_content_types');
        Schema::connection('sites')->dropIfExists('site_devices');
        Schema::connection('sites')->dropIfExists('site_domains');
        Schema::connection('sites')->dropIfExists('site_navigation');
        Schema::connection('sites')->dropIfExists('site_settings');
        Schema::connection('sites')->dropIfExists('site_settings_defaults');
        Schema::connection('sites')->dropIfExists('site_widget_field_types');
        Schema::connection('sites')->dropIfExists('site_widget_fields');
        Schema::connection('sites')->dropIfExists('site_widget_templates');
        Schema::connection('sites')->dropIfExists('site_widgets');
        Schema::connection('sites')->dropIfExists('sites');
    }
}
