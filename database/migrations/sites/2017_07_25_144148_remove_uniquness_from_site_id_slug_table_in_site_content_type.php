<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqunessFromSiteIdSlugTableInSiteContentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('sites')
            ->table('site_content_types',function(Blueprint $table){
            $table->dropUnique('site_content_types_slug_unique');
            $table->dropUnique('site_content_types_table_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('sites')
            ->table('site_content_types',function(Blueprint $table){
            $table->unique('slug');
            $table->unique('table');
        });
    }
}
