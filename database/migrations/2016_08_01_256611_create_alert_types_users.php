<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateAlertTypeUser
 */
class CreateAlertTypesUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alert_type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->boolean('viewed')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('alerts_users');
    }
}