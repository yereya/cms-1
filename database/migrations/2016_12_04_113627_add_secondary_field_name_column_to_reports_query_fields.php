<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSecondaryFieldNameColumnToReportsQueryFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_query_fields', function (Blueprint $table) {
            $table->string('secondary_field_name', 100)->after('field_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_query_fields', function (Blueprint $table) {
            $table->dropColumn('secondary_field_name');
        });
    }
}
