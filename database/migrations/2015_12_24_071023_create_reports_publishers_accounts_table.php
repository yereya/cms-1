<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class CreateReportsPublishersAccountsTable
 */
class CreateReportsPublishersAccountsTable extends Migration {

    public function up()
    {
        Schema::create('reports_publishers_accounts', function(Blueprint $table) {
            $table->increments('id');
            $table->enum('publisher', ['adwords', 'bing'])->default('adwords');
            $table->enum('type', ['Display', 'Search', 'Mcc', 'Inactive']);
            $table->string('account_name', 70)->nullable();
            $table->bigInteger('account_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('reports_publishers_accounts');
    }
}