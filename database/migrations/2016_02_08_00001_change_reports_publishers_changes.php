<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeReportsPublishersChanges extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('reports_publishers_accounts', function (Blueprint $table) {
            DB::statement("ALTER TABLE `reports_publishers_changes`
                ADD `reason` TEXT NULL AFTER `comment`,
                ADD `expected_results` TEXT NULL AFTER `reason`,
                ADD `follow_up_date` TIMESTAMP NULL AFTER `expected_results`
                ADD `follow_up_state` TINYINT NULL AFTER `follow_up_date`");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports_publishers_accounts', function (Blueprint $table) {
            DB::statement("ALTER TABLE `reports_publishers_changes`
                DROP `reason`,
                DROP `expected_results`,
                DROP `follow_up_date`
                DROP `follow_up_state`");
        });
    }
}
