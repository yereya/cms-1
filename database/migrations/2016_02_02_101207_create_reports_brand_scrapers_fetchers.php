<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsBrandScrapersFetchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports_brand_scrapers_fetchers', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('scraper_id')->unsigned();
            $table->text('url');
            $table->string('name');
            $table->enum('method', ['GET', 'POST', 'HEAD', 'PUT', 'DELETE', 'OPTIONS', 'CONNECT'])->default('GET');
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reports_brand_scrapers_fetchers');
    }
}
