<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RefactorUserRecordFeedbackFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $table = 'user_action_feedback';
        Schema::table($table, function (Blueprint $table) {
            $table->renameColumn('dynamic_id', 'record_id');
            $table->text('meta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table = 'user_action_feedback';
        Schema::table($table, function (Blueprint $table) {
            $table->renameColumn('record_id', 'dynamic_id');
            $table->removeColumn('meta');
        });
    }
}
