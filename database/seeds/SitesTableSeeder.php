<?php

use App\Entities\Models\Sites\Site as CMS_Site;
use App\Entities\Models\Sites\SiteDomain as CMS_Domain;
use Illuminate\Database\Seeder;

class SitesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(CMS_Site::class, 2)->create()->each(function ($s) {
            $s->domains()->saveMany(factory(CMS_Domain::class, 3)->make());
        });

    }
}
