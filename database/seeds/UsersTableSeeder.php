<?php

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Users\AccessControl\Permission;
use App\Entities\Models\Users\AccessControl\Role;
use App\Entities\Models\Users\Notifications\Notification;
use App\Entities\Models\Users\Notifications\Rule as NotificationsRule;
use App\Entities\Models\Users\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'test@test.com',
            'username' => 'test',
            'first_name' => 'test',
            'last_name' => 'test',
            'password' => 'test'
        ]);

        $users = factory(User::class, 2)->create();

        $permissions = factory(Permission::class, 3)->create();

        $roles = factory(Role::class, 2)->create()->each(function ($role) use ($permissions) {
            $role->permissions()->sync($permissions);
        });

        factory(Site::class)->make()->all()->each(function ($site) use ($users, $roles, $permissions) {
            $users->each(function ($user) use ($roles, $permissions, $site) {
                $user->roles()->attach($roles->pluck('id')->toArray(), ['site_id' => $site->id]);
                $user->specialPermissions()->attach($permissions->pluck('id')->toArray(), ['site_id' => $site->id]);
            });
        });

        factory(NotificationsRule::class, 2)->create()->each(function ($rule) use ($users) {
            $users->each(function ($user) use ($rule) {
                factory(Notification::class, 3)->create([
                    'user_id' => $user->id,
                    'user_notification_rule_id' => $rule->id
                ]);

                $user->notificationRules()->attach($rule->id);
            });
        });
    }
}
