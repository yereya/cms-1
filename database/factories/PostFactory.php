<?php

use App\Entities\Models\Sites\Like;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Site;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Like::class, function (Faker $faker) {
    $site = Site::find(5);
    SiteConnectionLib::setSite($site);

    return [];
});

$factory->define(Post::class, function (Faker $faker) {
    $site = Site::find(5);
    SiteConnectionLib::setSite($site);


    return [
        'slug' => $faker->slug,
        'name' => $faker->name,
    ];
});