//Disabling elixir OS notifications
process.env.DISABLE_NOTIFIER = true;

var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {
    // Bootstrap
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/bootstrap.scss'
    ], './public/assets/global/css/bootstrap.css');

    // Global
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/global/components.scss'
    ], './public/assets/global/css/components.css');
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/global/components-md.scss'
    ], './public/assets/global/css/components-md.css');
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/global/components-rounded.scss'
    ], './public/assets/global/css/components-rounded.css');
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/global/plugins.scss'
    ], './public/assets/global/css/plugins.css');
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/global/plugins-md.scss'
    ], './public/assets/global/css/plugins-md.css');

    // Layout
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/layouts/layout/layout.scss'
    ], './public/assets/layout/css/layout.css');
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/layouts/layout/themes/darkblue.scss'
    ], './public/assets/layout/css/darkblue.css');

    // Pages
    mix.sass([
        './resources/assets/sass/metronic_4.5.1/pages/login-4.scss'
    ], './public/assets/pages/css/login.css');

    // TrafficPoint Theme Customizations
    mix.sass('./resources/assets/sass/cms/theme.scss', './public/assets/main.css');

    // Global
    mix.copy('./resources/assets/other/metronic_4.5.1/global/img', './public/assets/global/img');
    mix.copy('./resources/assets/other/metronic_4.5.1/global/scripts/app.js', './public/assets/global/scripts/main.js');
    mix.copy('./resources/assets/other/metronic_4.5.1/global/scripts/datatable.js', './public/assets/global/scripts/datatable.js');

    // Layout
    mix.copy('./resources/assets/other/metronic_4.5.1/layouts/layout/img', './public/assets/global/layout/img');
    mix.copy('./resources/assets/other/metronic_4.5.1/layouts/global/scripts/quick-sidebar.js', './public/assets/layout/scripts/quick-sidebar.js');
    mix.copy('./resources/assets/other/metronic_4.5.1/layouts/layout/scripts/layout.js', './public/assets/layout/scripts/layout.js');
    mix.copy('./resources/assets/other/metronic_4.5.1/layouts/layout/scripts/demo.js', './public/assets/layout/scripts/demo.js');

    // Pages
    mix.copy('./resources/assets/other/metronic_4.5.1/pages/img', './public/assets/pages/img');
    mix.copy('./resources/assets/other/metronic_4.5.1/pages/media/bg', './public/assets/pages/media/bg');
    mix.copy('./resources/assets/other/metronic_4.5.1/pages/scripts/login.js', './public/assets/pages/scripts/login.js');
});

elixir(function(mix) {
    mix.browserSync({
        proxy: 'http://dev.cms.trafficpointltd.com/' // if using localhost:3000, make it a proxy for
    });
});