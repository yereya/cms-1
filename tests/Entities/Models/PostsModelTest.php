<?php

use App\Entities\Models\Sites\Post;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class PostsModelTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * testValidateLikesAmount
     * Make sure the method Likes Sum
     */
    public function testValidateLikesSum()
    {
        $post = factory(Post::class, 1)
            ->create()
            ->first();

        $post->likes()->create(['like' => 2]);


        $this->assertGreaterThan(1, $post->likesSum);
    }
}
