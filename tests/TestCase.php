<?php

namespace Tests;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Users\AccessControl\Role;
use App\Entities\Models\Users\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use SiteConnectionLib;

abstract class TestCase extends BaseTestCase
{

    protected $baseUrl = 'http://dev.cms.trafficpointltd.com/';

    protected $site_connection = null;
    protected $super_admin_user;
    protected $ppc_user;
    protected $admin_role;
    protected $ppc_role;

    use CreatesApplication;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
    }


    /**
     * setSiteConnection
     * This is require
     */
    public function setSiteConnection()
    {
        $site = Site::find(5);
        SiteConnectionLib::setSite($site);
    }

    protected function setUsers()
    {
        $this->super_admin_user = User::find(2); // TODO Create a test user (vadim)
        $this->ppc_user         = User::find(14); // TODO Create a test user (maayan)
    }

    protected function setRoles()
    {
        $this->admin_role = Role::find(1); // super admin
        $this->ppc_role   = Role::find(5); // Marketing PPC
    }
}