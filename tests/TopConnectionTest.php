<?php

use App\Entities\Models\Sites\Site;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class TopConnectionTest extends TestCase
{
    use DatabaseTransactions;

    public function testConnectivityToPlayRightDB()
    {
        $this->setSiteConnection();

        $this->assertTrue(!!SiteConnectionLib::getSite());
    }

    public function testPlayRightPostsTableExistance()
    {
        $this->setSiteConnection();

        $post = \App\Entities\Models\Sites\Post::first();

        $this->assertTrue(!!$post);
    }

}
