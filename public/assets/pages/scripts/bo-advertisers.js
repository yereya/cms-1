/**
 * Created by kostya on 3/2/2016.
 */
var advertisersHandler = (function() {
    function events() {
        $('.advertiser_save_btn').on('click', function(e) {
            e.preventDefault();
            var form = $(this).parents('form');

            console.log(form.serialize());

            $('#ajax-modal').modal('hide');
        });
    }

    return {
        init: function() {
            events()
        }
    }
})();

jQuery(document).ready(function () {
    advertisersHandler.init();
});