var BuilderAPI = function () {
    var Builder = {
        site: null, // Holds "App\Entities\Models\Sites\Site" instance
        data: null, // Holds "App\Entities\Models\Sites\Templates\Template" instance
        drag: null, // Holds "dragula" instance

        widgetsData: {}, // Contains all widgets data instances used

        // What fillable fields may be used for each element in forms
        fillable: {
            row: ['html_element_id', 'html_class', 'html_container_class', 'html_wrapper_class', 'order', 'total_size', 'locked', 'active', 'inherited', 'template_id', 'ab_tests_ids', 'ab_tests_action'],
            column: ['html_element_id', 'html_class', 'row_id', 'order', 'size', 'locked', 'template_id', 'inherited']
        },

        // CSS selectors for each element.
        // Keep everything here for easy access
        selectors: {
            editor: '.editor',
            contextMenu: '#contextMenu',

            hierarchy: '.hierarchy',
            hierarchyElement: '.hierarchy .element',

            container: '.container',
            row: '.builder-row',
            column: '.builder-column',
            widget: '.builder-widget',

            save: '.save',
            move: '.actions .action.move',

            templates: {
                row: '#row-template',
                column: '#column-template',
                widget: '#widget-template',

                contextMenu: '#context-menu-template',

                hierarchy: '#hierarchy-template',
                hierarchyElement: '#hierarchy-element-template'
            }
        },

        /*  Where everything is located.

        For example:
        {
            "rows":{
                "12":{
                    ...,
                    "columns":{
                        "15":{
                            ...,
                            "widgetsData":{
                                "23":{...},
                                "24":{...}
                            },
                            "rows":{
                                "13":{
                                     "columns":{
                                        "16":{...}
                                     }
                                }
                            }
                        }
                    }
                }
            }
        }

        Would resolve to:
        {
            rows: {
                12: null // Null because it is root
                13: 15
            },
            columns: {
                15: 12
                16: 13
            },
            widgetsData: {
                23: 15
                24: 15
            }
        }
         */
        maps: {
            rows: {},
            columns: {},
            items: {},
            widgetsData: {}
        },

        // The Handlebars templates for each element
        templates: {
            row: null,
            column: null,
            widget: null,
            container: null
        },

        /**
         * Set Handlebars templates
         */
        setTemplates: function () {
            Handlebars.registerHelper("debug", function () {
                console.debug("Current Context");
                console.debug("====================");
                console.debug(this);

                console.debug("Arguments");
                console.debug("====================");
                console.debug(arguments);
            });

            Handlebars.registerHelper('escapeHTML', function (object) {
                var entityMap = {
                    '&': '&amp;',
                    '<': '&lt;',
                    '>': '&gt;',
                    '"': '&quot;',
                    "'": '&#39;',
                    '/': '&#x2F;',
                    '`': '&#x60;',
                    '=': '&#x3D;'
                };

                return encodeURIComponent(String(object).replace(/[&<>"'`=\/]/g, function fromEntityMap(s) {
                    return entityMap[s];
                }));
            });

            Handlebars.registerHelper('equals', function (v1, v2, options) {
                if (v1 === v2) {
                    return options.fn(this);
                }
                return options.inverse(this);
            });

            this.templates.row = Handlebars.compile($(this.selectors.templates.row).html());
            this.templates.column = Handlebars.compile($(this.selectors.templates.column).html());
            this.templates.widget = Handlebars.compile($(this.selectors.templates.widget).html());
            this.templates.contextMenu = Handlebars.compile($(this.selectors.templates.contextMenu).html());
            this.templates.hierarchy = Handlebars.compile($(this.selectors.templates.hierarchy).html());
            this.templates.hierarchyElement = Handlebars.registerPartial('hierarchyElement', $(this.selectors.templates.hierarchyElement).html());
        },

        /**
         * Init Drag & Drop
         */
        initDragAndDrop: function () {
            // Init the drag & drop
            //noinspection JSUnresolvedFunction
            this.drag = dragula({
                revertOnSpill: true,

                /**
                 * Whether or not the clicked element may be moved
                 *
                 * @param el
                 * @param source
                 * @param handle
                 * @param sibling
                 * @returns {boolean}
                 */
                moves: function (el, source, handle, sibling) {
                    // Allow moving rows
                    if ($(el).is(Builder.selectors.row)) {
                        // We want to move only by the handler
                        if ($(handle).is(Builder.selectors.row + ' > ' + Builder.selectors.move) || $(handle).parent().is(Builder.selectors.row + ' > ' + Builder.selectors.move)) {
                            // If the row is locked, or its sibling, you shouldn't move the row
                            return !$(el).data('locked') && !$(sibling).data('locked');
                        }

                        return false;
                    }

                    // Allow moving widgets
                    if ($(el).is(Builder.selectors.widget)) {
                        // If widget have locked don't move (if this parent template widget)
                        if ($(el).data('locked')) {
                            return false;
                        }
                        // We want to move only by the handler
                        if ($(handle).is(Builder.selectors.widget + ' > ' + Builder.selectors.move) || $(handle).parent().is(Builder.selectors.widget + ' > ' + Builder.selectors.move)) {
                            // If the column is locked, you shouldn't move the widget
                            return !$(el).parents(Builder.selectors.column).data('locked');
                        }

                        return false;
                    }
                },

                /**
                 * Whether or not you can drop the dragged element
                 *
                 * @param el
                 * @param target
                 * @param source
                 * @param sibling
                 * @returns {*}
                 */
                accepts: function (el, target, source, sibling) {
                    // Rows can be moved into the editor / other columns (nested)
                    if ($(el).is(Builder.selectors.row)) {
                        return ($(target).is(Builder.selectors.editor) && !$(sibling).data('locked')) // Drop to the editor (First Level), sibling row not locked
                            || ($(target).is(Builder.selectors.column) && !$(target).data('locked')) && !$(sibling).data('locked'); // Drop to a column (Nested Level), target column not locked
                    }

                    // Widgets can be moved into columns only
                    if ($(el).is(Builder.selectors.widget)) {
                        return $(target).is(Builder.selectors.column) && !$(target).data('locked') && !$(sibling).data('locked'); // Drop to a column (Any level, doesn't matter), column not locked, sibling not locked
                    }

                    // The rest is disabled...
                    return false;
                }
            });

            // What to do when element is over a container (editor / column)
            this.drag.on('shadow', function (el, container) {
                $(Builder.selectors.editor).find(' > .empty').show();
                $(Builder.selectors.editor).find(Builder.selectors.column).find(' > .empty').show();

                $(container).find(' > .empty').hide();
            });

            // What to do when element is dropped into a container
            this.drag.on('drop', function (el, target, source, sibling) {
                sibling = $(sibling).is(Builder.selectors.row + ',' + Builder.selectors.widget) ? sibling : null;

                Builder.handleItemOrder(el, target, source, sibling);
                Builder.buildGrid();
            });

            this.drag.on('cancel', function () {
                $(Builder.selectors.editor).find(Builder.selectors.column).find(' > .empty').show();
                $(Builder.selectors.editor).find(' > .empty').show();
            });
        },

        /**
         * Move Rulers on Mouse Move
         */
        moveRulersOnMouseMove: function () {
            $('body').on('mousemove', function (event) {
                var rulerX = parseInt($('.rulers .ruler-x').css('left').replace('px'));
                var rulerY = parseInt($('.rulers .ruler-y').css('top').replace('px'));

                var eventX = event.offsetX + rulerX;
                var eventY = event.offsetY + rulerY;

                if (eventX && eventY) {
                    $('#mouse-x-pos').css('left', eventX + 'px');
                    $('#mouse-y-pos').css('top', eventY + 'px');
                }
            });
        },

        /**
         * Toggle iframe Breakpoint
         */
        toggleIframeBreakpoint: function () {
            $('.breakpoints button').on('click', function () {
                var $this = $(this),
                    $iframe = $('iframe', window.parent.document.body);

                $iframe.removeClass();
                $iframe.addClass($(this).data('size'));

                $this.parent().find('button').removeClass('active');
                $this.addClass('active');
            });
        },

        /**
         * Get Data from the server, used on first load
         */
        getRemoteItems: function () {
            App.blockUI();

            $.ajax({
                url: routes.getBuilderItems,
                method: 'GET',
                success: function (response) {
                    Builder.site = response.site;
                    Builder.data = response.template;

                    toastr.success('Template loaded.');

                    // After everything is loaded, let's build the editor
                    Builder.buildGrid();
                    Builder.setEvents();
                    Builder.loadAssets();
                },

                error: function (xhr, status, error) {
                    toastr.warning('Check the console for details.', 'Error fetching the template');
                    console.error(xhr, status, error);
                },

                complete: function () {
                    App.unblockUI();
                }
            });
        },

        /**
         * Refresh Data from the server, used to discard current changes
         */
        refreshRemoteItems: function () {
            if (!confirm('Are you sure?')) {
                return;
            }
            App.blockUI();

            $.ajax({
                url: routes.getBuilderItems,
                method: 'GET',
                success: function (response) {
                    Builder.site = response.site;
                    Builder.data = response.template;

                    // After everything is loaded, let's build the editor
                    Builder.buildGrid();
                    Builder.loadAssets();
                },

                error: function (xhr, status, error) {
                    toastr.warning('Check the console for details.', 'Error fetching the template');
                    console.error(xhr, status, error);
                },

                complete: function () {
                    App.unblockUI();
                }
            });
        },

        /**
         * Store Data
         * Send all the local data to the server to save it
         */
        storeRemoteItems: function () {
            if (!confirm('Are you sure?')) {
                return;
            }
            App.blockUI();
            Builder.removeUnnecessaryDataForStore(Builder.data.grid);

            $.ajax({
                url: routes.storeBuilderItems,
                data: {
                    _token: App.csrf,
                    grid: Builder.data.grid
                },
                method: 'POST',
                success: function (response) {
                    Builder.site = response.site;
                    Builder.data = response.template;

                    toastr.success('Template changes saved.');

                    // After everything is loaded, let's build the editor
                    Builder.buildGrid();
                    Builder.loadAssets();
                },

                error: function (xhr, status, error) {
                    toastr.warning('Check the console for details.', 'Error saving the template');
                    console.error(xhr, status, error);
                },

                complete: function () {
                    App.unblockUI();
                }
            })
        },

        /**
         * Remove Unnecessary Data For Store
         *
         * Removes attributes used only in the builder but not in the server.
         * This is done to reduce the amount of data sent to the server.
         */
        removeUnnecessaryDataForStore: function (rows, isItem) {
            if(isItem) {
                rows = [rows.instance];
            }

            $.each(rows, function (r) {
                if(!isItem) {
                    delete rows[r].data;
                    delete rows[r].html;
                    delete rows[r].sortedColumns;
                }

                $.each(rows[r].columns, function (c) {
                    delete rows[r].columns[c].data;
                    delete rows[r].columns[c].row_data;
                    delete rows[r].columns[c].html;
                    delete rows[r].columns[c].sortedItems;

                    $.each(rows[r].columns[c].items, function (i, item) {
                        if(item.element_type === 'row') {
                            Builder.removeUnnecessaryDataForStore(item, true);
                        }
                    });
                });
            })
        },

        /**
         * Build Grid
         * Loops over all the rows / column / widgets data and builds the HTML to work with.
         * Also adds the editor and columns as dragging container (you can drop into them elements)
         */
        buildGrid: function () {
            if (!this.data) return;

            this.data.grid = !$.isArray(this.data.grid) ? this.data.grid : {};

            // Throw it to the editor
            $(this.selectors.editor).html(this.buildGridRows(this.data.grid))
                .promise()
                .done(function () {
                    $('.tooltips').tooltip();

                    $('#ajax-modal').modal('hide');
                });

            var dragContainers = $(this.selectors.editor).toArray(),
                dragColumns = $(this.selectors.editor).find(this.selectors.row + ' ' + this.selectors.column).toArray();

            // Update the dragging containers with the newly loaded containers
            this.drag.containers = dragContainers.concat(dragColumns);
        },

        /**
         * Builds rows into the container
         *
         * @param rows
         * @param parent_item
         *
         * @returns string|null
         */
        buildGridRows: function (rows, parent_item) {
            if (!rows) return null;

            var sorted = Object.values(rows).sort(function (a, b) {
                return a.order - b.order;
            });

            var html = '';
            $.each(sorted, function (i, row) {
                if (row.deleted) return;

                if (row.inherited) {
                    row.locked = true;
                }

                // Reset rows order to prevent issues
                rows[row.id].order = i;

                // Store parent item. if not exists, mock it for consistency
                rows[row.id].item = parent_item ? parent_item : {
                        id: row.id,
                        order: row.order,
                        created: typeof row.created == 'undefined' ? false : row.created
                    };
                rows[row.id].isItem = !!parent_item;

                // Update the mapping (now we know where to find it)
                Builder.maps.rows[row.id] = parent_item ? parent_item.id : null;

                // Reset the objects to prevent issues
                row.columns = (typeof row.columns != 'undefined' && !$.isArray(row.columns)) ? row.columns : {};

                // Store additional information for other elements to use
                row.data = Builder.getRowDataByFillable(row);
                row.html = Builder.buildGridColumns(row);

                // Create the html from the template with columns for this row
                html += Builder.templates.row(row);
            });

            return html;
        },

        /**
         * Get Row Data By Fillable
         * Used not to send any unnecessary data to the server on forms
         *
         * @param row
         *
         * @return string
         */
        getRowDataByFillable: function (row) {
            var data = {
                id: row.id
            };
            for (var i = 0; i < this.fillable.row.length; i++) {
                data[this.fillable.row[i]] = row[this.fillable.row[i]];
            }

            return JSON.stringify(data);
        },

        /**
         * Builds columns into rows
         *
         * @param row
         *
         * @returns string|null
         */
        buildGridColumns: function (row) {
            if (!row.columns) return null;

            // Sort the columns by the defined order
            row.sortedColumns = Object.values(row.columns).sort(function (a, b) {
                return a.order - b.order;
            });

            var html = '';
            $.each(row.sortedColumns, function (i, column) {
                if (column.deleted) return;

                // Reset columns order to prevent issues
                row.columns[column.id].order = i;

                // Update the mapping (now we know where to find it)
                Builder.maps.columns[column.id] = row.id;

                // Reset the objects to prevent issues
                column.items = !$.isArray(column.items) ? column.items : {};

                // Store additional information for other elements to use
                column.row_id = row.id;
                column.row_data = row.data;
                column.data = Builder.getColumnDataByFillable(row, column);
                column.html = Builder.buildGridItems(row, column);

                // Create the html from the template
                html += Builder.templates.column(column);
            });

            return html;
        },

        /**
         * Get Column Data By Fillable
         * Used not to send any unnecessary data to the server on forms
         *
         * @param row
         * @param column
         *
         * @returns string
         */
        getColumnDataByFillable: function (row, column) {
            var data = {
                id: column.id,
                row_total_size: row.total_size
            };
            for (var i = 0; i < this.fillable.column.length; i++) {
                data[this.fillable.column[i]] = column[this.fillable.column[i]];
            }

            return JSON.stringify(data);
        },

        /**
         * Builds items into columns
         *
         * @param row
         * @param column
         *
         * @returns string|null
         */
        buildGridItems: function (row, column) {
            if (typeof column.items == 'undefined') return null;

            // Sort the items by the defined order
            column.sortedItems = Object.values(column.items).filter(Boolean).sort(function (a, b) {
                return a.order - b.order;
            });

            var html = '';
            $.each(column.sortedItems, function (i, item) {
                if (item.deleted) return;

                if (item.inherited) {
                    item.locked = true;
                }

                // Reset item order to prevent issues
                column.items[item.id].order = i;

                // Update the mapping (now we know where to find it)
                Builder.maps.items[item.id] = column.id;

                if (item.element_type == 'row') {
                    // Normalize the row element to match the regular row
                    var rowTemp = {};

                    rowTemp[item.element_id] = $.extend({}, item.instance, {
                        row: row,
                        column: column,
                        item: item
                    });

                    // Use existing rows building, but use only 1 row at a time
                    html += Builder.buildGridRows(rowTemp, item);
                } else if (item.element_type == 'widget') {
                    ['show_on_mobile','show_on_tablet','show_on_desktop'].forEach(function (value){
                        if (typeof item[value] === 'undefined'){
                            // When new widgets are created, set the default to true.
                            item[value] = 1;
                        }else{
                            // Parse the int value just in case the server returns json numbers as string.
                            item[value] = parseInt(item[value]);
                        }
                    });
                    html += Builder.buildColumnWidget(item, row, column);
                }
            });

            return html;
        },

        /**
         * Builds widget into column
         *
         * @param item
         * @param row
         * @param column
         * @returns {*}
         */
        buildColumnWidget: function (item, row, column) {
            var data_id = item.element_id;

            // If the object has "instance" object, this means the data is fresh. Then let's update the local data
            if (typeof item.instance != 'undefined') {
                // Update the local data
                Builder.widgetsData[data_id] = item.instance;

                // Reset the column widget data so we won't do this anymore
                delete column.items[item.id].instance;
            }

            // Update the mapping (now we know where to find it)
            Builder.maps.widgetsData[item.element_id] = item.id;

            // Updates with new HTML if there is none
            if (!Builder.widgetsData[data_id].html) {
                Builder.fetchWidgetDataHTML(item, row, column);
            }

            // Create the html from the template (with what we have... we may not have HTML)
            return Builder.templates.widget($.extend({}, Builder.widgetsData[data_id], {
                row: row,
                column: column,
                item: item
            }));
        },

        /**
         * Gets the widget data html from the server by widget_id and data (local)
         *
         * @param item
         * @param row
         * @param column
         */
        fetchWidgetDataHTML: function (item, row, column) {
            $.ajax({
                url: buildAjaxUrl(Builder.widgetsData[item.element_id].widget_id, item.element_id),
                data: {
                    widget_data_id: item.element_id,
                    widget_id: Builder.widgetsData[item.element_id].widget_id
                },
                item: item,
                templateData: {
                    row: row,
                    column: column,
                    item: item
                },
                success: function (response) {
                    var item = this.item;

                    // Update the current widget data with new data
                    $.extend(Builder.widgetsData[item.element_id], response);

                    // Add additional template data
                    var template = Builder.templates.widget($.extend({}, Builder.widgetsData[item.element_id], this.templateData));

                    // Replace the current widget in the DOM with template with new data
                    $(Builder.selectors.editor).find(Builder.selectors.widget).filter(function () {
                        return $(this).data('id') == item.id;
                    }).replaceWith(template);
                }
            });

            /**
             * Build Ajax URL
             *
             * @param widgetId
             * @param widgetDataId
             * @returns string
             */
            function buildAjaxUrl(widgetId, widgetDataId) {
                return routes.getWidgetData
                    .replace(/:widgetId/, widgetId)
                    .replace(/:widgetDataId/, widgetDataId);
            }
        },

        /**
         * Handle context menu events
         */
        handleContextMenu: function (e) {
            e.stopPropagation();

            // return native menu if pressing control
            if (e.ctrlKey) return;

            e.preventDefault();

            var invokedOn = $(this);
            var invokedOnType = $(this).data('type');
            var contextMenu = $(Builder.selectors.contextMenu);

            buildContextMenu(invokedOn, invokedOnType);
            placeMenu(e);

            $(Builder.selectors.hierarchy).one('click', '.close-hierarchy', function () {
                buildHierarchy();
            });

            // Create a single event to close the context menu after clicking
            contextMenu.one('click', 'a', function () {
                contextMenu.hide();
                buildHierarchy();
            });

            //make sure menu closes on any click
            $(document).one('click', ':not(' + Builder.selectors.contextMenu + ')', function () {
                contextMenu.hide();
            });

            if (invokedOn.is(Builder.selectors.widget) || invokedOn.is(Builder.selectors.column) || invokedOn.is(Builder.selectors.row)) {
                buildHierarchy(invokedOn, invokedOnType);
            }

            // Gather the Ids for all the element types underneath the current item
            function collectIndices($el, type) {
                var indices = [];

                if (type == 'widget') {
                    indices['widget'] = $el.data();
                    indices['column'] = $el.closest('[data-type="column"]').data();
                    indices['row'] = $el.closest('[data-type="row"]').data();
                } else if (type == 'column') {
                    indices['column'] = $el.data();
                    indices['row'] = $el.closest('[data-type="row"]').data();
                } else if (type == 'row') {
                    indices['row'] = $el.data();
                } else {
                    console.error();
                }

                return indices;
            }

            function buildContextMenu(invokedOn, invokedOnType) {
                var indices = collectIndices(invokedOn, invokedOnType);

                $(Builder.selectors.contextMenu).html(Builder.templates.contextMenu({
                    menuItems: getMenuItemsByType(indices, invokedOnType)
                }));
            }

            function getMenuItemsByType(indices, invokedOnType) {
                var widgetItems = [],
                    columnItems = [],
                    rowItems = [];

                if (invokedOnType == 'widget') {
                    widgetItems.push({
                        "title": "Edit Widget",
                        "modal_url": routes.widgetDataCtrl + '&method=createOrEdit&widget_id=' + indices['widget']['widgetId']
                        + "&column_id=" + indices['widget']['columnId']
                        + "&data_id=" + indices['widget']['dataId'] + "&item_id=" + indices['widget']['id'],
                        'disabled': indices['widget']['inherited'] ? 'disabled' : ''
                    });
                    widgetItems.push({
                        "title": "Delete Widget",
                        "modal_url": routes.widgetDataCtrl + '&method=delete&column_id=' + indices['widget']['columnId']
                        + "&id=" + indices['widget']['id'],
                        'disabled': indices['widget']['inherited'] ? 'disabled' : ''
                    });
                    widgetItems.push({
                        "title": "Duplicate Widget",
                        "modal_url": routes.widgetDataCtrl + '&method=duplicate&widget_id=' + indices['widget']['widgetId']
                        + "&column_id=" + indices['widget']['columnId']
                        + "&data_id=" + indices['widget']['dataId'],
                        'disabled': indices['widget']['inherited'] ? 'disabled' : ''
                    });
                }

                if (invokedOnType == 'widget' || invokedOnType == 'column') {
                    widgetItems.push({
                        "title": "Add Widget",
                        "modal_url": routes.widgetDataCtrl + '&method=select&row_id=' + indices['column']['rowId']
                        + "&column_id=" + indices['column']['id'],
                    });
                    columnItems.unshift({
                        "title": "Edit Column",
                        "modal_url": routes.templateCtrl + '&method=column&id=' + indices['column']['id']
                        + "&data=" + JSON.stringify(indices['column']['data'])
                        + "&row_data=" + JSON.stringify(indices['column']['rowData']),
                        'disabled': indices['column']['inherited'] ? 'disabled' : ''
                    });
                    columnItems.push({
                        "title": "Delete Column",
                        "modal_url": routes.templateCtrl + '&method=deleteColumn&row_id=' + indices['column']['rowId']
                        + "&id=" + indices['column']['id'],
                        'disabled': indices['column']['inherited'] ? 'disabled' : ''
                    });

                    rowItems.push({
                        "title": "Add Row",
                        "modal_url": routes.templateCtrl + '&method=row&column_id=' + indices['column']['id']
                    });
                }

                if (invokedOnType == 'widget' || invokedOnType == 'column' || invokedOnType == 'row') {
                    columnItems.push({
                        "title": "Add Column",
                        "modal_url": routes.templateCtrl + '&method=column&row_data=' + JSON.stringify(indices['row']['data'])
                    });

                    rowItems.unshift({
                        "title": "Edit Row",
                        "modal_url": routes.templateCtrl + '&method=row&data=' + JSON.stringify(indices['row']['data']),
                        'disabled': indices['row']['inherited'] ? 'disabled' : ''
                    });

                    rowItems.push({
                        "title": "Duplicate Row",
                        "modal_url": routes.templateCtrl + '&method=duplicateRow&id=' + indices['row']['id'] + '&is_item=' + indices['row']['isItem'],
                        'disabled': indices['row']['inherited'] ? 'disabled' : ''
                    });

                    rowItems.push({
                        "title": "Delete Row",
                        "modal_url": routes.templateCtrl + '&method=deleteRow&id=' + indices['row']['id'] + '&is_item=' + indices['row']['isItem'],
                        'disabled': indices['row']['inherited'] ? 'disabled' : ''
                    });
                }

                if (widgetItems.length > 0) {
                    widgetItems.push({'devider': true});
                }
                if (columnItems.length > 0) {
                    columnItems.push({'devider': true});
                }

                return $.merge($.merge(widgetItems, columnItems), rowItems);
            }

            function placeMenu(e) {
                contextMenu.css({
                    display: "block",
                    left: getMenuPosition(e.clientX, 'width', 'scrollLeft'),
                    top: getMenuPosition(e.clientY, 'height', 'scrollTop')
                });
            }

            function getMenuPosition(mouse, direction, scrollDir) {
                var win = $(window)[direction](),
                    scroll = $(window)[scrollDir](),
                    menu = $(Builder.selectors.contextMenu)[direction](),
                    position = mouse + scroll;

                // opening menu would pass the side of the page
                if (mouse + menu > win && menu < mouse)
                    position -= menu;

                return position;
            }

            function buildHierarchy(invokedOn, invokedOnType) {
                var id = invokedOn ? invokedOn.data('id') : 0,
                    element;

                var elements = [];
                var path;

                if (invokedOnType === 'widget') {
                    var item = Builder.getItem(id);
                    path = Builder.getColumnPath(item.column_id);

                    elements.push(Builder.getRow(path[0].row));
                    element = Builder.widgetsData[item.element_id];
                } else if (invokedOnType === 'column') {
                    path = Builder.getColumnPath(id);

                    elements.push(Builder.getRow(path[0].row));
                    element = Builder.getColumn(id);
                } else if (invokedOnType === 'row') {
                    if(invokedOn.data('is-item')) {
                        id = Builder.getItem(id).element_id;
                    }

                    path = Builder.getRowPath(id);

                    if (path.length) {
                        elements.push(Builder.getRow(path[0].row))
                    } else {
                        elements.push(Builder.getRow(id));
                    }

                    element = Builder.getRow(id);
                }

                $(Builder.selectors.hierarchy).html(Builder.templates.hierarchy({
                    id: id,
                    type: invokedOnType,
                    element: element,
                    elements: elements,
                    widgetsData: Builder.widgetsData
                }));

                $(Builder.selectors.hierarchy).one('click', '.element > .title', function () {
                    var $element = $(this).closest('.element');

                    buildHierarchy($element, $element.data('type'));
                });
            }
        },

        /**
         * Highlights the element from the hovered hierarchy element
         */
        highlightHoveredHierarchyElement: function () {
            var $element = $(this).closest('.element'),
                id = $element.data('id'),
                selector;

            if ($element.data('type') == 'row') {
                selector = Builder.selectors.row;
            } else if ($element.data('type') == 'column') {
                selector = Builder.selectors.column;
            } else {
                selector = Builder.selectors.widget;
            }

            $(Builder.selectors.row + ',' + Builder.selectors.column + ',' + Builder.selectors.widget).removeClass('hover');
            $(selector).filter(function () {
                return $(this).data('id') == id;
            }).addClass('hover');
        },

        /**
         * Set Events
         *
         * Contains all the events to listen to for form handling and others
         */
        setEvents: function () {
            $(Builder.selectors.editor).on('modal-error', function (e, errors) {
                console.error(errors);
            });

            $(document).on('click', this.selectors.save, this.storeRemoteItems);

            $(document).on('modal-form-event-row-changed', this.handleRowForm);

            $(document).on('modal-form-event-row-deleted', this.handleRowDelete);

            $(document).on('modal-form-event-row-duplicate', this.handleRowDuplicate);

            $(document).on('modal-form-event-column-changed', this.handleColumnForm);

            $(document).on('modal-form-event-column-deleted', this.handleColumnDelete);

            $(document).on('modal-form-event-widget-deleted', this.handleWidgetDataDelete);

            $(document).on('click', '.choose-widget-data', this.handleWidgetDataChoosing);

            $(document).on('click', '.save-add-widget-data', this.handleWidgetDataSaveAdd);

            $(document).on('modal-form-event-widget-data-changed', Builder.handleWidgetDataForm);

            $(document).on('click', '.toggle-compact-view', this.toggleCompactView);

            $(document).on('contextmenu', Builder.selectors.widget + ', ' + Builder.selectors.column + ', ' + Builder.selectors.row + ', ' + Builder.selectors.hierarchyElement, this.handleContextMenu);

            $(document).on('mouseover', this.selectors.hierarchyElement + ' > .title', this.highlightHoveredHierarchyElement);

            $("#dynamic_view").change(this.toggleDynamicView);
        },

        toggleDynamicView: function(){
            if(!this.checked){
                $(".builder-widget").css('cssText','display: block !important')
            }else{
                $(".builder-widget").css('display','');
            }
        },

        /**
         * Load Assets
         *
         * Loops over the returned assets from remote and appends them to head. Removes previously added assets before that.
         */
        loadAssets: function () {
            var $head = $('head');

            $head.find('link.builder-css').remove();
            $head.find('script.builder-js').remove();

            $.each(Builder.data.assets, function (i, url) {
                $head.append(function () {
                    if (url.endsWith('.css')) {
                        return $('<link>').attr('href', url).attr('rel', 'stylesheet').addClass('builder-css');
                    } else if (url.endsWith('.js')) {
                        return $('<script>').attr('src', url).addClass('builder-js');
                    }
                })
            });
        },

        /**
         * Toggle Compact View
         */
        toggleCompactView: function () {
            $('.canvas').toggleClass('hide-widget-html')
        },

        /**
         * Get Row by ID
         *
         * @param id
         * @param value
         * @returns {*}
         */
        getRow: function (id, value) {
            return this.getElementObject('row', this.getRowPath(id), id, value);
        },

        /**
         * Get Column by ID
         *
         * @param id
         * @param value
         * @returns {*}
         */
        getColumn: function (id, value) {
            return this.getElementObject('column', this.getColumnPath(id), id, value);
        },

        /**
         * Get Item By ID
         *
         * @param id
         * @param value
         * @returns {*}
         */
        getItem: function (id, value) {
            var column = this.getColumn(this.maps.items[id]);

            // Stores the result of the value inside the object (note that it updates all references also).
            return value ? $.extend(column.items[id], value) : column.items[id];
        },

        /**
         * Get Item Parent Column By Item ID
         * @param id
         * @param value
         * @returns {*}
         */
        getItemParentColumn: function (id, value) {
            return this.getColumn(this.maps.items[id], value);
        },

        /**
         * Get row path by ID
         * Searches recursively by "this.maps" object
         *
         * @param id
         * @param path
         * @returns {*}
         */
        getRowPath: function (id, path) {
            path = path || [];

            var parent_item_id = this.maps.rows[id];

            // Check if the row is inside an item
            if (parent_item_id) {
                var column_id = this.maps.items[parent_item_id];
                var row_id = this.maps.columns[column_id];

                // If column parent row is not a root row, then we don't need it because the column is nested under an item (type of row)
                if (this.maps.rows[row_id]) {
                    path.unshift({
                        column: column_id,
                        item: parent_item_id
                    })
                } else {
                    path.unshift({
                        row: row_id,
                        column: column_id,
                        item: parent_item_id
                    });
                }

                return this.getRowPath(row_id, path);
            }

            return path;
        },

        /**
         * Get column path by ID
         * Searches recursively by "this.maps" object
         *
         * @param id
         * @param path
         * @returns {*}
         */
        getColumnPath: function (id, path) {
            path = path || [];

            var row_id = this.maps.columns[id];
            var parent_item_id = this.maps.rows[row_id];

            // Check if the row is inside an item
            if (parent_item_id) {
                var parent_column_id = this.maps.items[parent_item_id];

                path.unshift({
                    column: id,
                    item: parent_item_id
                });

                return this.getColumnPath(parent_column_id, path);
            }

            path.unshift({
                row: row_id,
                column: id
            });

            return path;
        },

        /**
         * Returns a reference the the correct row / column by provided path
         * Each column is stored in a row. Each row may be stored in a item which is inside a column
         *
         * @param type
         * @param paths
         * @param key
         * @param value
         * @returns {*}
         */
        getElementObject: function (type, paths, key, value) {
            var object = this.getNestedObjectByPath(type, paths);

            // When the element is at the root level (rows for instance), we would like to get it straight.
            // On all other cases the instance will be passed within the object
            if (type == 'row' && paths.length == 0) {
                object = key ? object[key] : object;
            }

            // The value may be a function, so if it is, try to execute it and grab the result
            value = typeof value == 'function' ? value(object) : value;

            // Stores the result of the value inside the object (note that it updates all references also).
            return value ? $.extend(object, value) : object;
        },

        /**
         * Get Nested Object By Path
         *
         * @param type
         * @param paths
         *
         * @returns object
         */
        getNestedObjectByPath: function (type, paths) {
            var object = this.data.grid;

            for (var i = 0; i < paths.length; i++) {
                var path = paths[i];

                if (type == 'row') {
                    // Start with a root row
                    if (i == 0) {
                        object = object[path.row];
                    }

                    object = object.columns[path.column];
                    object = object.items[path.item].instance;
                } else if (type == 'column') {
                    // Start with a root row
                    if (i == 0) {
                        object = object[path.row];
                    }
                    // On all other cases we expect columns to be inside items
                    else {
                        object = object.items[path.item].instance;
                    }

                    object = object.columns[path.column];
                }
            }

            return object;
        },

        /**
         * Handle row form when user saves the data
         * Stores the data locally to allow multiple modifications until saved.
         *
         * @param e
         * @param form
         */
        handleRowForm: function (e, form) {
            var row = $(form).serializeObject();

            row.locked = parseInt(row.locked);
            row.active = parseInt(row.active);

            var item_id = 'temp-' + Math.floor(Date.now() / 1000);

            if (row.id) {
                Builder.getRow(row.id, row);
            } else if (row.parent_column_id) {
                var column_id = parseInt(row.parent_column_id);

                // Set temporary ID for the row
                row.id = item_id + '-row';
                row.created = true;

                // Adds the relevant data for the item
                Builder.getColumn(column_id).items[item_id] = {
                    id: item_id,
                    column_id: column_id,
                    element_id: row.id,
                    element_type: 'row',
                    instance: row,
                    order: Object.keys(Builder.getColumn(column_id).items).length++,
                    created: true
                };
            } else {
                Builder.data.grid[item_id] = $.extend(row, {
                    id: item_id,
                    order: Object.keys(Builder.data.grid).length++,
                    created: true
                });
            }

            Builder.buildGrid();
        },

        /**
         * Handle row delete
         * Stores the data locally to allow multiple modifications until saved.
         *
         * @param e
         * @param form
         */
        handleRowDelete: function (e, form) {
            var form = $(form).serializeObject(),
                deleted;

            if (form.is_item) {
                // Delete the item holding the row
                deleted = Builder.getItem(form.id, {
                    deleted: true
                });

                // Delete the row
                Builder.getRow(deleted.element_id, {
                    deleted: true
                });
            } else {
                // Delete the row
                Builder.getRow(form.id, {
                    deleted: true
                });
            }

            Builder.buildGrid();
        },

        /**
         * Handle Row Duplicate
         *
         * @param e
         * @param form
         */
        handleRowDuplicate: function (e, form) {
            form = $(form).serializeObject();

            if (form.is_item) {
                var item = Builder.getItem(form.id);
                var newItem = duplicateItem(item);

                Builder.getItemParentColumn(form.id).items[newItem.id] = newItem;
            } else {
                var row = Builder.getRow(form.id);
                var newRow = duplicateRow(row);

                Builder.data.grid[newRow.id] = newRow;
            }

            Builder.buildGrid();

            function duplicateItem(item) {
                item = $.extend(true, {}, item);
                item.id = getTempID(item.id);
                item.created = true;

                if(item.element_type == 'row') {
                    item.element_id = getTempID(item.element_id);

                    item.instance.id = item.element_id;
                    item.instance.created = true;

                    alterColumns(item.instance);

                    Builder.maps.rows[item.element_id] = item.id;
                }

                return item;
            }

            function duplicateRow(row) {
                row = $.extend(true, {}, row);
                row.id = getTempID(row.id);
                row.created = true;

                alterColumns(row);

                Builder.maps.rows[row.id] = null;

                return row;
            }

            function getTempID(original) {
                return 'temp-' + Math.floor(Date.now()) + '-' + original;
            }

            function alterColumns(row) {
                $.each(row.columns, function (i, column) {
                    delete row.columns[i];

                    column.id = getTempID(column.id);
                    column.row_id = row.id;
                    column.created = true;

                    alterItems(column);

                    row.columns[column.id] = column;
                    Builder.maps.columns[column.id] = row.id;
                });
            }

            function alterItems(column) {
                $.each(column.items, function (i, item) {
                    delete column.items[i];

                    item.column_id = column.id;
                    item = duplicateItem(item);

                    column.items[item.id] = item;
                    Builder.maps.items[item.id] = column.id;
                });
            }
        },

        /**
         * Handle column form when user saves the data.
         * Stores the data locally to allow multiple modifications until saved.
         *
         * @param e
         * @param form
         */
        handleColumnForm: function (e, form) {
            var column = $(form).serializeObject();

            column.locked = parseInt(column.locked);

            var row = Builder.getRow(column.row_id);

            if (column.id) {
                Builder.getColumn(column.id, column);
            } else {
                if (typeof row.columns == 'undefined' || $.isArray(row.columns)) {
                    row.columns = {};
                }

                $.extend(column, {
                    id: 'temp-' + Math.floor(Date.now() / 1000),
                    order: Object.keys(row.columns).length++,
                    items: {},
                    created: true
                });

                row.columns[column.id] = column;
            }

            Builder.buildGrid();
        },

        /**
         * Handle column delete
         * Stores the data locally to allow multiple modifications until saved.
         *
         * @param e
         * @param form
         */
        handleColumnDelete: function (e, form) {
            var id = $(form).serializeObject().id,
                deletedColumn = Builder.getColumn(id, {
                    deleted: true
                });

            var columns = Builder.getRow(deletedColumn.row_id).columns;

            $.each(columns, function (i, column) {
                if (!column.deleted && deletedColumn.order <= column.order) {
                    column.order--;
                }
            });

            Builder.buildGrid();
        },

        /**
         * Handle widget data form when the user saves the data.
         *
         * // TODO: Make it store the data locally to not modify server data until saved
         *
         * @param e
         * @param form
         * @param callback
         */
        handleWidgetDataForm: function (e, form, callback) {
            var form_params = $(form).serializeObject();

            form_params._token = App.csrf;
            form_params._method = form_params.create_new == 'true' ? 'POST' : 'PUT';

            $.ajax({
                url: buildUrl(form_params.widget_id, form_params.widget_data_id),
                method: 'POST',
                data: form_params,
                success: function (response) {
                    if (response.id) {

                        // Update the core grid when a change is made.
                        for (var columns_index in Builder.data.grid){
                            var columns = Builder.data.grid[columns_index].columns;
                            for (let items_index in columns){
                                var column = columns[items_index];
                                if (response.widget_data_items && column.items[response.widget_data_items.id]){
                                    column.items[response.widget_data_items.id] = response.widget_data_items;
                                }
                            }
                        }

                        $.each(Builder.widgetsData, function (id, data) {
                            if (id == response.id) {
                                $.extend(data, response);
                            }
                        });

                        toastr.success('Widget saved and updated.');
                    } else {
                        toastr.warning('Check the console for details.', 'Error saving the widget');
                        console.error(response, form_params);
                    }

                    if (typeof callback == 'function') {
                        callback(response);
                    }

                    Builder.buildGrid();
                },
                error: function (response) {
                    toastr.warning('Check the console for details.', 'Error saving the widget');
                    console.error(response, form_params);
                }
            });

            function buildUrl(widgetId, widgetDataId) {
                return resolveRouteByFormIntention()
                    .replace(/:widgetId/, widgetId)
                    .replace(/:widgetDataId/, widgetDataId);
            }

            function resolveRouteByFormIntention() {
                return form_params.create_new == 'true' ? routes.storeWidgetData : routes.updateWidgetData;
            }
        },

        /**
         * Handle widget data delete.
         * Stores the data locally to allow multiple modifications until saved.
         *
         * @param e
         * @param form
         */
        handleWidgetDataDelete: function (e, form) {
            var data = $(form).serializeObject();

            Builder.getItem(data.id, {
                deleted: true
            });

            Builder.buildGrid();
        },

        /**
         * Handle widget data save and add
         */
        handleWidgetDataSaveAdd: function (e) {
            var $button = $(this),
                $form = $button.parents('#ajax-modal').find('form');

            $button.attr('disabled', true);

            Builder.handleWidgetDataForm.call($button[0], e, $form, function (response) {
                Builder.handleWidgetDataAppendToColumn(response.id, $button.data('column-id'), response);
            });
        },

        /**
         * Handle widget data choosing
         */
        handleWidgetDataChoosing: function () {
            var $button = $(this);

            $button.attr('disabled', true);

            Builder.handleWidgetDataAppendToColumn($button.data('id'), $button.data('column-id'), $button.data('data'));
            Builder.buildGrid();
        },

        /**
         * Handle widget data append to column
         *
         * Adds the widget data into a column
         *
         * @param data_id
         * @param column_id
         * @param data
         */
        handleWidgetDataAppendToColumn: function (data_id, column_id, data) {
            var item_id = 'temp-' + Math.floor(Date.now() / 1000);

            // Adds the relevant data for the item
            Builder.getColumn(column_id).items[item_id] = {
                id: item_id,
                column_id: column_id,
                element_id: data_id,
                element_type: 'widget',
                order: Object.keys(Builder.getColumn(column_id).items).length++,
                created: true
            };

            // Update local widget data with the newest
            if (typeof Builder.widgetsData[data_id] == 'undefined') {
                Builder.widgetsData[data_id] = data;
            }
        },

        /**
         * Handle Item Order and location when user drags a row
         *
         * @param el
         * @param target
         * @param source
         * @param sibling
         */
        handleItemOrder: function (el, target, source, sibling) {
            var id = $(el).data('id'),
                draggedItem = $(el).data('isItem') ? Builder.getItem(id) : Builder.getRow(id),
                originalOrder = draggedItem.order,
                newOrder,
                items;

            // Sometimes you'll drop on a column of the same row you are dragging.
            // This tries to find the closest column and if it wasn't found (like you are dropping to the root), it will use the root
            if ($(target).closest(el).length) {
                target = $(target).closest(el).closest(Builder.selectors.column);

                if (!target.length) {
                    target = $(Builder.selectors.editor);
                }
            }

            // Remove the item from where it was taken from
            if (target != source) {
                if ($(source).is(Builder.selectors.editor)) {
                    delete Builder.data.grid[draggedItem.id];
                } else if ($(source).is(Builder.selectors.column)) {
                    var sourceColumn = Builder.getColumn($(source).data('id'));

                    delete sourceColumn.items[draggedItem.id];
                }
            }

            // Depending if it was dropped to a column or the root, Sets the data to the proper location.
            if ($(target).is(Builder.selectors.editor)) {

                // When you move a row from a column to the root, it will be within an item.
                // That is unnecessary for the root. we should remove it and only use the instance (the row itself)
                if ($(source).is(Builder.selectors.column)) {
                    draggedItem = draggedItem.instance;
                }

                draggedItem.column_id = null;
                Builder.data.grid[draggedItem.id] = draggedItem;
                Builder.maps.items[draggedItem.id] = null;

                items = Builder.data.grid;
            } else if ($(target).is(Builder.selectors.column)) {
                var targetColumn = Builder.getColumn($(target).data('id'));

                // In some cases (like creating a new row) you'll want to move a row from the root to a column.
                // In that case you'll want to create an item to hold that element
                if ($(source).is(Builder.selectors.editor)) {
                    draggedItem = {
                        id: 'temp-' + Math.floor(Date.now()) / 1000,
                        column_id: targetColumn.id,
                        element_id: draggedItem.id,
                        element_type: 'row',
                        instance: draggedItem,
                        order: draggedItem.order,
                        created: true
                    };
                }

                draggedItem.column_id = targetColumn.id;
                targetColumn.items[draggedItem.id] = draggedItem;
                Builder.maps.items[draggedItem.id] = targetColumn.id;

                items = targetColumn.items;
            }

            // Determines what order this item should have (depending on where it's dropped).
            if (sibling) {
                var siblingId = $(sibling).data('id'),
                    siblingItem = $(sibling).data('isItem') ? Builder.getItem(siblingId) : Builder.getRow(siblingId);

                newOrder = siblingItem.order > 1 ? siblingItem.order - 1 : 1;
            } else {
                newOrder = items.length;
            }

            // Loops over the items of the target and sets the orders according to the new order
            $.each(items, function (id, item) {
                if (draggedItem.id == id) {
                    item.order = newOrder;
                } else if (originalOrder <= item.order && newOrder >= item.order) {
                    item.order--;
                } else if (newOrder <= item.order) {
                    item.order++;
                }
            });
        }
    };

    return {
        init: function () {
            Builder.setTemplates();
            Builder.initDragAndDrop();
            Builder.moveRulersOnMouseMove();
            Builder.toggleIframeBreakpoint();
            Builder.getRemoteItems();
        },

        save: function () {
            Builder.storeRemoteItems();
        },

        refresh: function () {
            Builder.refreshRemoteItems();
        },

        build: function () {
            Builder.buildGrid();
        },

        getData: function () {
            return Builder.data;
        },

        getMaps: function () {
            return Builder.maps;
        },

        getWidgetsData: function () {
            return Builder.widgetsData;
        },

        // TODO: Removes this from the API, and move to the general lib. Make sure it executes with an event listener.
        recompileScss: function (e, that) {
            e.preventDefault();

            App.blockUI();

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: $(that).attr('href'),
                data: {_token: App.csrf},
                success: function (response) {
                    if (response.status == '1') {
                        var queryString = '?reload=' + new Date().getTime();

                        $('link[rel="stylesheet"]').each(function () {
                            this.href = this.href.replace(/\?.*|$/, queryString);
                        });

                        // Update the inline CSS.
                        var inline_css = $("#inline-css");
                        inline_css.html('');
                        $.get('/api/sites/' + CMS.site_id + '/templates-css/generate/' + CMS.template_id, function (css){
                            inline_css.html(css);
                        });
                    }

                    if (response.status && response.desc.length) {
                        toastr.info(response.desc)
                    } else if (response.desc.length) {
                        return swal({
                            title: 'Error',
                            type: 'error',
                            html: response.desc
                        });
                    }
                },
                error: function (xhr, status, error) {
                    toastr.warning('Check the console for details.', 'Error refreshing CSS');
                    console.error(xhr, status, error);
                },

                complete: function () {
                    App.unblockUI();
                }
            });
        },

        // TODO: Removes this from the API, and move to the general lib. Make sure it executes with an event listener.
        uploadCss: function (e, that) {
            e.preventDefault();

            App.blockUI();

            $.ajax({
                type: 'GET',
                url: $(that).attr('href'),
                data: {_token: App.csrf},
                success: function (response) {
                    if (response.status && response.desc.length) {
                        toastr.info(response.desc)
                    } else if (response.desc.length) {
                        toastr.warning(response.desc)
                    }
                },

                error: function (xhr, status, error) {
                    toastr.warning('Check the console for details.', 'Error sending CSS');
                    console.error(xhr, status, error);
                },

                complete: function () {
                    App.unblockUI();
                }

            });
        }
    }
}();

$(function () {
    BuilderAPI.init();
});