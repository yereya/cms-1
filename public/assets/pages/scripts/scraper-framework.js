/**
 * Created by kostya on 2/1/2016.
 */

var brandFetcher = (function () {
    this.ajax_types = {
        'save': 'POST',
        'index': 'GET',
        'delete': 'DELETE',
        'update': 'PUT',
        'duplicate': 'GET'
    }

    function initEvents() {
        /** update scraper */
        $('.scraper_container').on('click', '.scraper_update_btn', function () {
            var form = $(this).parents('form');
            var url = form.attr('action');
            send('PUT', form.serialize(), url, callbackUpdateScraper, $(this));
        })

        /** clear form */
        $('.fetcher_container').on('click', '.clear_btn', function () {
            clearPortletForm($(this));
        });

        /** save fetcher */
        $('.fetcher_container').on('click', '.save_btn', function () {
            ajaxSave($(this));
        });

        /** update form */
        $('.fetcher_container').on('click', '.update_btn', function () {
            ajaxUpdate($(this));
        });

        /** clone and add new form fetcher */
        $('.fetcher_container').on('click', '.add_fetcher_btn', function () {
            clonePortletForm($(this));
        });

        /** delete fetcher form */
        $('.fetcher_container').on('click', '.delete_btn', function () {
            ajaxDelete($(this));
        });

        /** test fetcher */
        $('.fetcher_container').on('click', '.test_btn', function () {
            sendRequest($(this));
        });

        $('.processor_container, .scraper_container').on('click', '.test_all_btn', function() {
            sendPageRequest($(this));
        });

        /** hide all remove buttons (properties row) in start */
        $('.fetcher_container').find('div.btn_section_remove a').each(function () {
            var row = $(this).parents('div.clone');
            var is_empty = true;

            row.find('input[type="text"]').each(function () {
                if ($(this).val().length > 0) {
                    is_empty = false;
                }
            });

            if (is_empty) {
                $(this).hide();
            }
        });

        /** fetcher properties delete or clear */
        $('.fetcher_container').on('click', '.btn_section_remove a', function () {
            clearOrRemoveProperties($(this));
        });

        /** duplicate row properties */
        $('.fetcher_container').on('focus', '.clone .key_group, .clone .value_group', function () {
            duplicateRowProperties($(this));
        });

        $('#sortable_portlets').on('sortstop', function(event, ui) {
            if ((ui.item.index() + 1) == ui.item.find('input[name="order"]').val()) {
                return false;
            }

            var order = 1, ajax_url = null, data = [];

            var _token = $(this).find('input[name="_token"]').val();

            $(this).find('input[name="order"]').each(function() {
                $(this).val(order);

                var url = $(this).parents('form').attr('action');
                ajax_url = url.substring(0, url.indexOf('?')) + '/priority';

                var params = {};
                var id = url.substring(url.indexOf('?'));
                params['fetcher_id'] = id.replace('?', '');
                params['order'] = order;
                data.push(params);
                order++;
            });

            send('POST', {_token: _token, params: data}, ajax_url, callbackUpdateScraper, null);
        });

        $('.column').disableSelection();
    }

    /** create data array and request to controller scraper and fetchers only **/
    function sendRequest(element) {
        var portlet = element.parents('.portlet');
        var current_form = portlet.find('form');
        var container = element.parents('.fetcher_container');
        var form_url = current_form.attr('action');

        var reqex = new RegExp('[?]', 'g');
        var url = form_url.replace(reqex, '/') + '/actions';

        var forms = [];

        container.find('form').each(function (i, v) {
            var serialized = $(v).serialize();
            forms[i] = serialized;

            if ($(v).find('input[name="order"]').val() == current_form.find('input[name="order"]').val()) {
                return false;
            }
        });

        var data = {
            '_token': current_form.find('input[name="_token"]').val(),
            'debug': element.hasClass('debug_true') ? 1 : 0,
            'scraper': $('.scraper_container').find('form').serialize(),
            'fetchers': forms
        };

        send(this.ajax_types.save, data, url, callbackAfterAction, null);
    }

    /** create data array and request to controller full page (scraper, fetchers, processors) **/
    function sendPageRequest(element) {
        var form_url = null;
        var $scraper = $('.scraper_container').find('form');

        var forms = [];
        $('.fetcher_container').find('form').each(function () {
            if (form_url == null) {
                form_url = $(this).attr('action');
            }

            forms.push($(this).serialize());
        });

        if (form_url) {
            var reqex = new RegExp('[?]', 'g')
            var url = form_url.replace(reqex, '/') + '/actions';
        } else {
            var url = $scraper.attr('action') + '/fetchers/0/actions'
        }

        var data = {
            '_token': App.csrf,
            'debug': element.hasClass('debug_true') ? 1 : 0,
            'scraper': $scraper.serialize(),
            'fetchers': forms,
            'processors': brandProcessor.processors()
        };

        send(this.ajax_types.save, data, url, callbackAfterAction, null);
    }

    /** clone properties row */
    //TODO remove
    function duplicateRowProperties(element) {
        var row = element.parents('div.clone');
        var parent = element.parents('div.properties_fetcher');

        var is_empty = true;
        row.find('input[type="text"]').each(function () {
            if ($(this).val().length > 0 || jQuery(this).val() != '') {
                is_empty = false;
            }
        });

        row.find('select').each(function () {
           if (!$(this).val()) {
               is_empty = false;
           }
        });

        if (!is_empty) {
            var count_empty_fields = 0;
            parent.find('input').each(function () {
                if ($(this).val().length == 0 || jQuery(this).val() == '') {
                    count_empty_fields++;
                }
            });

            parent.find('select').each(function () {
                if (!$(this).val()) {
                    count_empty_fields++;
                }
            })

            if (count_empty_fields < 2) {
                row.find('select').each(function () {
                    $(this).select2('destroy');
                });
                var clone_row = row.clone();
                row.find('a').show();
                clone_row.find('select').prop('selected', false);
                clone_row.find('input').val('');
                clone_row.find('a').hide();
                clone_row.insertAfter(row);
                row.find('select').each(function () {
                    $(this).select2();
                });
                clone_row.find('select').each(function () {
                    $(this).select2();
                });
            }
        }
    }

    /** clear or remove row properties */
    //TODO remove
    function clearOrRemoveProperties(element) {
        var parent = element.parents('div.form');
        var row_delete = element.parents('div.clone');

        var count = 0;
        parent.find('div.btn_section_remove a').each(function () {
            count++;
        });
        if (count > 1) {
            row_delete.remove();
        } else {
            row_delete.find('input').val('');
        }
    }

    /** create new fetcher form */
    function clonePortletForm(element) {
        var parent = element.parents('div.portlet');

        $.uniform.restore(parent.find('input[type="checkbox"]'));
        parent.find('select').each(function () {
            $(this).select2('destroy');
        });
        clearPortletForm(parent.clone(), parent);
    }

    /** clear form */
    function clearPortletForm(parent, insertAfterBlock) {
        var prevOrder = insertAfterBlock.find('input[name="order"]').val();

        $('input[name="order"]').each(function() {
            console.log($(this).val());
        });

        var order = 1;
        $('input[name="order"]').each(function() {
            if (parseInt($(this).val()) > parseInt(order)) {
                order = parseInt($(this).val());
            }
        });
        order = parseInt(order) + 1;

        var form = parent.find('form');
        var url = form.attr('action').substring(0, form.attr('action').indexOf('?'));
        form.attr('action', url);
        parent.find('input[name="fetcher_id"]').attr('value', -1);
        parent.find('input').each(function () {
            if ($(this).attr('name') != '_token' && $(this).attr('name') != 'scraper_id') {
                $(this).val('');
            }
        });
        parent.find('input[name="order"]').val(order);
        //override title
        parent.find('span.caption-helper').text('');
        //override buttons
        parent.find('a.add_fetcher_btn').addClass('hidden');
        parent.find('a.clear_btn, button.clear_btn').removeClass('hidden');
        parent.find('a.save_btn, button.clear_btn').removeClass('hidden');
        parent.find('a.delete_btn, button.clear_btn').addClass('hidden');
        parent.find('a.duplicate_btn, button.clear_btn').addClass('hidden');
        parent.find('a.update_btn, button.clear_btn').addClass('hidden');
        parent.find('a.test_btn, button.test_btn').addClass('hidden');

        parent.find('.properties_fetcher').each(function() {
            $(this).find('.tb-duplicate-group').each(function(i, v) {
                if (i != 0) {
                    $(v).remove();
                }
            });
        });

        if (insertAfterBlock) {
            parent.insertAfter(insertAfterBlock);
            App.initDuplicateGroup();
            insertAfterBlock.find('select').each(function () {
                $(this).select2();
            });
            parent.find('select').each(function () {
                $(this).select2();
            });

            insertAfterBlock.find('input[type="checkbox"]').uniform();
            parent.find('input[type="checkbox"]').uniform();
        }
    }

    /** save fetcher **/
    function ajaxSave(element) {
        var parent = element.parents('div.portlet');
        var data = parent.find('form').serialize();
        var url = parent.find('form').attr('action');

        var reqex = new RegExp('[?]', 'g');
        url = url.replace(reqex, '/');

        send(this.ajax_types.save, data, url, callbackSaveFetcher, element);
    }

    /** update fetcher */
    function ajaxUpdate(element) {
        var parent = element.parents('div.portlet');
        var data = parent.find('form').serialize();
        var url = parent.find('form').attr('action');

        var reqex = new RegExp('[?]', 'g');
        url = url.replace(reqex, '/');

        send('PUT', data, url, callbackUpdatedFetcher, element);
    }

    /** delete fetcher */
    function ajaxDelete(element) {
        var parent = element.parents('div.portlet');
        var _token = parent.find('form').find('input[name="_token"]').val();
        var url = parent.find('form').attr('action');

        var reqex = new RegExp('[?]', 'g');
        url = url.replace(reqex, '/');

        send(this.ajax_types.save, {
            '_method': this.ajax_types.delete,
            '_token': _token
        }, url, callbackDeleteFetcher, element);
    }

    /** ajax function */
    function send(method, params, url, callback, element) {
        App.blockUI();

        jQuery.ajax({
            type: method ? method : 'POST',
            url: url,
            data: params,
            success: function (response) {
                App.unblockUI();
                callback(response, element)
            },
            error: function(message) {
                App.unblockUI();
                // errorCallback(message)
            }
        });
    }

    /** callback action after save new fetcher */
    function callbackSaveFetcher(response, element) {
        if (response.fetcher) {
            toastr.success('Success');
        } else {
            toastr.error(response.fetcher);
            return false;
        }
        var parent = element.parents('div.portlet')
        var form = parent.find('form.fetcher-form');
        //override title
        parent.find('span.caption-helper').text('type: ' + response.fetcher.fetcher_type + ' name: ' + response.fetcher['name']);
        //override buttons
        var action = form.attr('action');
        action += '?' + response.fetcher['id'];
        form.attr('action', action);
        form.find('input[name="fetcher_id"]').val(response.fetcher['id']);
        parent.find('a.clear_btn').addClass('hidden');
        parent.find('a.save_btn').addClass('hidden');
        parent.find('a.add_fetcher_btn').removeClass('hidden');
        parent.find('a.delete_btn').removeClass('hidden');
        parent.find('a.update_btn').removeClass('hidden');
        parent.find('a.test_btn').removeClass('hidden');
    }

    function callbackUpdateScraper(response) {
        toastr.success('Scraper updated successful!');
    }

    /** callback action after update fetcher */
    function callbackUpdatedFetcher(response, element) {
        if (response.fetcher) {
            toastr.success('Success');
        } else {
            toastr.error(response.fetcher);
            return false;
        }
    }

    /** callback action after delete fetcher */
    function callbackDeleteFetcher(response, element) {
        if (response.status) {
            toastr.success('Success');
        } else {
            toastr.error(response.desc);
            return false;
        }

        var parent = element.parents('div.portlet');
        parent.find('a.add_fetcher_btn').click();
        parent.remove();
    }

    function callbackAfterAction(response, element) {
        ajaxModal.init(response);
        ajaxModal.show();
    }

    /** if exists error in ajax print toastr error */
    function errorCallback(response) {
        toastr.error('Error, check network');
    }

    return {
        init: function () {
            initEvents();
        },
        send: function (method, params, url, callback, element) {
            send(method, params, url, callback, element);
        }
    };
})();

var brandAdvanced = (function () {
    function initEvents() {
        /** hide all remove buttons (properties row) in start */
        $('.advanced_container').find('div.btn_section_remove a').each(function () {
            var row = $(this).parents('div.clone');
            var is_empty = true;

            row.find('input[type="text"]').each(function () {
                if ($(this).val().length > 0) {
                    is_empty = false;
                }
            });

            if (is_empty) {
                $(this).hide();
            }
        });

        /** fetcher properties delete or clear */
        $('.advanced_container').on('click', '.btn_section_remove a', function () {
            clearOrRemoveProperties($(this));
        });

        /** duplicate row properties */
        $('.advanced_container').on('focus', '.clone .key_group, .clone .value_group', function () {
            duplicateRowProperties($(this));
        });

        /** save or update advanced properties */
        $('.advanced_container').on('click', '.properties_save_btn', function () {
            saveAdvancedProperties($(this));
        });
    }

    function saveAdvancedProperties(element) {
        var data = element.parents('form').serialize();
        var url = element.parents('form').attr('action');
        var reqex = new RegExp('[?]', 'g');
        url = url.replace(reqex, '/');
        brandFetcher.send('POST', data, url, callbackUpdatedAdvanced);
    }

    function callbackUpdatedAdvanced(response) {
        if (response.status) {
            toastr.success('Updated');
        } else {
            toastr.error(response.desc);
        }
    }

    /** clone properties row */
    function duplicateRowProperties(element) {
        var row = element.parents('div.clone');
        var parent = element.parents('div.portlet-body');

        var is_empty = true;
        row.find('input[type="text"]').each(function () {
            if ($(this).val().length > 0 || jQuery(this).val() != '') {
                is_empty = false;
            }
        });

        if (is_empty) {
            var count_empty_fields = 0;
            parent.find('input').each(function () {
                if ($(this).val().length == 0 || jQuery(this).val() == '') {
                    count_empty_fields++;
                }
            });

            if (count_empty_fields == 2) {
                var clone_row = row.clone();
                row.find('a').show();
                clone_row.find('input').val('');
                clone_row.find('a').hide();
                clone_row.insertAfter(row);
            }
        }
    }

    /** clear or remove row properties */
    function clearOrRemoveProperties(element) {
        var parent = element.parents('div.portlet-body');
        var row_delete = element.parents('div.clone');

        var count = 0;
        parent.find('div.btn_section_remove a').each(function () {
            count++;
        });
        if (count > 1) {
            row_delete.remove();
        } else {
            row_delete.find('input').val('');
        }
    }

    return {
        init: function () {
            initEvents();
        }
    }
})();

var brandProcessor = (function () {

    function initEvents() {
        $('.processor_container').on('click', '.new_processor_btn', function () {
            cloneProcessor();
        });

        $('.processor_container').on('click', '.save_processor_btn, .update_processor_btn', function () {
            saveProcessors($(this), true);
        });

        $('.processor_container').on('click', '.remove_processor_btn', function (e) {
            e.preventDefault();
            deleteProcessor($(this));
        });

        $('.processor_container').on('click', '.duplicate_processor_btn', function (e) {
            e.preventDefault();
            duplicateProcessor($(this));
        });
    }

    function duplicateProcessor(element) {
        var $container = element.parents('.processor_row_container');
        var $last = $('.processor_container').find('.processor_row_container').last();
        var $clone = $container.clone();
        $clone.insertAfter($last);

        let url = element.attr('href');
        var _token = App.csrf;

        brandFetcher.send('POST', {_token: _token}, url, callbackSaveProcessors, null);

        return false;
    }

    function deleteProcessor(element) {
        var $container = element.parents('.processor_row_container');
        var id = $container.data('processor_id');
        
        if (id == 0) {
            $container.remove();
            
            return false;
        }
        
        var url = element.attr('href');
        var _token = App.csrf;
        
        brandFetcher.send('DELETE', {_token: _token}, url, callbackSaveProcessors, null);
        $container.remove();
        
        return false;
    }

    function saveProcessors(element, save) {
        var $container = element.parents('.processor_container');
        var _token = App.csrf;
        var url = $container.data('processors_url');

        var data = [];
        $container.find('.processor_row_container').each(function() {
            var obj = {};
            var $row = $(this);
            obj['id'] = $row.attr('data-processor_id');
            obj['active'] = $row.find('input[name="active"]').is(':checked') ? 1 : 0;
            $row.find('a').each(function() {
                if ($(this).data('pk_column') != undefined) {
                    obj[$(this).data('pk_column')] = $(this).text();
                }
            });
            data.push(obj);
        });

        if (save) {
            brandFetcher.send('POST', {'_token': _token, 'params': data}, url, callbackSaveProcessors, null);
        } else {
            return data;
        }
    }
    function cloneProcessor() {
        var $last = $('.processor_container').find('.processor_row_container').last();
        $.uniform.restore($last.find('input[type="checkbox"]'));
        var $clone = $last.clone();
        $clone.attr('data-processor_id', 0);
        $last.find('input[type="checkbox"]').uniform();
        $clone.find('input[type="checkbox"]').uniform();
        $clone.find('a.editable').editable('setValue', null);
        $clone.find('select').prop('selectedIndex', 0);

        $clone.insertAfter($last);
    }

    function callbackSaveProcessors(response) {
        if (response.status) {
            toastr.success(response.desc);
            if (response.data) {
                var count = 0;
                $('.processor_container').find('.processor_row_container').each(function(i, v) {
                    if ($(v).attr('data-processor_id') == 0 && response.data.new) {
                        $(v).attr('data-processor_id', response.data.new[count]);
                        count++;
                    }
                });
            }

            $('.save_processor_btn').addClass('hidden');
            $('.update_processor_btn').removeClass('hidden');
            $('.test_all_btn').removeClass('hidden');
        } else {
            toastr.error(response.desc);
            return false;
        }
    }

    return {
        init: function () {
            initEvents();
        },
        processors: function () {
            return saveProcessors($('.save_processor_btn'), false);
        }
    }
})();

var ajaxModal = (function () {
    this.modal_obj = $('<div class="modal fade" id="jmodal" role="dialog"><div class="modal-dialog"><div class="modal-content">' +
        '<div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>' +
        '<h4 class="modal-title">Modal Header</h4></div><div class="modal-body">' +
        '<p>Could not load data due to response error. check console.</p></div><div class="modal-footer">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '</div></div></div></div>');

    this.default_params = {
        title: 'Modal Header',
        container: 'Scraper Test log',
        button: 'Close'
    };

    function showModel(params) {
        if (params.status) {
            if (!$('body').is('#jmodal')) {
                $('body').append(modal_obj);
            }

            if (params.data.title) {
                $('#jmodal').find('h4.modal-title').text(params.data.title);
            }

            if (params.data.body) {
                var body = $('#jmodal').find('div.modal-body').text('');

                var jj = $('<div />').addClass('jjson');
                jj.jJsonViewer(params.data.body, {expanded: true});

                jj.find('span.close').each(function() {
                    $(this).removeClass('close');
                })

                body.append(jj);
            }
        } else {
            toastr.error('Error! Check network');
        }
    }

    function openLog(log_id) {
        var debug = $('.log_container').data('log_url');
        var href = debug.replace('log_id', log_id);

        var redirect = window.open(href, '', {target: '_blank'});
        redirect.location;

        return false;
    }

    return {
        init: function (params) {

            App.unblockUI();

            if (params.status) {
                openLog(params.data.logId);
            } else {
                toastr.error('Error! Check network');
            }
        },
        show: function () {
            $('#jmodal').modal();
        }
    }
})();

var brandPage = (function () {
    function toggleCollapse(element) {
        var portlet = $(element).parents('div.portlet');
        var portletActions = portlet.find('div.actions');

        if (portletActions.is(':visible')) {
            portletActions.hide();
            portlet.find('div.portlet-body').css({'display': 'none'});
        } else {
            portletActions.show();
            portlet.find('div.portlet-body').css({'display': 'block'});
        }

    }

    return {
        init: function() {
            // Close all opened portals
            $('.page-content a.expand').each(function() {
                toggleCollapse(this);
            });

            // Set the collapse event
            $('.page-content').on('click', '.expand, .collapse', function() {
                toggleCollapse(this);
            });
        }
    }
})();

var translatorDocumentation = (function () {
    var selectors = {
        'translators': '.properties_fetcher select[name^="translator[key]"]',
        'help-block': '.properties_fetcher .help-block'
    };

    var domTranslators;

    function getTranslatorResources () {

        return document.querySelectorAll(selectors.translators);
    }

    function attachOnChangeEvents () {
        domTranslators = getTranslatorResources();
        domTranslators.forEach(function (domTranslator) {
            $(domTranslator).on("change", function (obj) {
                var helpSpan = $(domTranslator).closest('.row').find('.help-block');
                helpSpan.html(translators_docs[domTranslator.value]);
            });
        });
    }

    function init () {
        domTranslators = getTranslatorResources();
        attachOnChangeEvents();
    }

    return {
        init: function () {
            init();
        }
    };
})();


jQuery(document).ready(function () {
    brandFetcher.init();
    brandAdvanced.init();
    brandProcessor.init();
    brandPage.init();
    translatorDocumentation.init();
});

