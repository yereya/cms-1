define('pageSpeed', ['jquery'], function ($) {
    /**
     *
     * @type {{btn: string, form: string}}
     */
    const selectors = {
        btn: '.page-speed-refresh-btn',
        form: '.page-speed-form'
    };

    /**
     *  Initialize Process
     */
    function init() {
        initEvents();
    }

    /**
     *  Initialize events
     */
    function initEvents() {
        let btn = $(selectors.btn);
        btn.on('click', function(event){
            "use strict";
            refreshScores(event);
        });


    }

    /**
     * Refresh table scores
     *
     *  1. make ajax request
     *  2. change button text (until process finish).
     *  3. after response made reload table data.
     *
     * @param event
     */
    function refreshScores(event) {
        event.preventDefault();
        const $target = $(event.target);

        this.ajax = () => {
            const $form = $(selectors.form);
            "use strict";
            const setting = {
                method: $form.attr('method'),
                url: $form.attr('action'),
                timeout: 0,
                beforeSend: function () {
                    $target.text('Refreshing...');
                    $target.attr('disabled', 'disabled');
                },
                complete: function () {
                    $target.text('Updated....');
                    setTimeout(function () {
                        location.reload();
                    },2000);
                }
            };

            $.ajax(setting);

        };


        this.ajax();
    }

    init();
});
