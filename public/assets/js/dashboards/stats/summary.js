/**
 * Summary widget - dashboard usage.
 */
define('summaryStats', ['generalHelpers'], function (helpers) {


    var cost = '.cost>.number span';
    var revenue = '.revenue .number>span';
    var profit = '.profit .number>span';
    var ratio = {
        selectors: {
            wrapper: '.period .ratio',
            percent: '.period .ratio .percent',
            diff: '.period .ratio .diff',
            arrow: '.fa'
        }
    };
    var $forms = $('.stats-form');
    var refreshButton = $('.refresh-stats');

    /**
     * Initialize Process
     */
    function init() {
        initStats();
        initEvents();
    }

    /**
     * Init All Events
     */
    function initEvents() {
        refreshButton.on('click', function (event) {
            event.preventDefault();
            initStats(true);
        })
    }

    /**
     * Init Stats
     * @param update
     */
    function initStats(update) {
        $forms.each(function (key, form) {
            requestToDashStats($(form), update);
        })
    }

    /**
     * Request To Dash Stats
     * @param $form
     * @param update
     */
    function requestToDashStats($form, update) {
        this.resolve = function (stats, $form) {
            $form.find(cost).text(stats.cost ? '$' + stats.cost : 'N/A');
            $form.find(revenue).text(stats.revenue ? '$' + stats.revenue : 'N/A');
            $form.find(profit).text(stats.profit ? '$' + stats.profit : 'N/A');
            stats.ratio.percent = Math.round(parseFloat(stats.ratio.percent));
            var diff = '';

            if (typeof stats.ratio.percent !== 'undefined') {
                if (helpers.isNegative(stats.ratio.percent)) {
                    stats.ratio.percent = (stats.ratio.percent.toString()).replace('-', '');
                    $form.find(ratio.selectors.wrapper).addClass('negative');
                }
            }
            if (stats.ratio.percent != 0) {
                ratio.value = stats.ratio.percent + '%';
                $form.find(ratio.selectors.arrow).show();
            } else {
                ratio.value = 'N/A';
                $form.find(ratio.selectors.arrow).hide();
            }

            diff = Math.round(stats.ratio.diff);
            console.log(stats.ratio.diff);
            $form.find(ratio.selectors.percent).text(ratio.value);
            $form.find(ratio.selectors.diff).text(diff);
            App.unblockUI($form.closest('.portlet'));
        }

        this.ajax = function ($form) {
            var that = this;
            var formParams = $form.serialize();
            var $filters = $('.' + $form.data('controllers'));

            if (update) {
                var filterData = $filters.serializeArray();
                var formArray = $form.serializeArray();
                formParams = $.merge(formArray, filterData);
            }
            var setting = {
                method: $form.attr('method'),
                url: $form.attr('action'),
                data: formParams,
                beforeSend: function () {
                    App.blockUI({target: $form.closest('.portlet')});
                },
                complete: function (stats) {
                    if (helpers.isJson(stats.responseText)) {
                        var stats = JSON.parse(stats.responseText);
                        that.resolve(stats, $form);
                    }
                    else {
                        App.unblockUI($form.closest('.portlet'));
                        console.warn('no data passed');
                    }
                }
            }
            $.ajax(setting)
        }

        //init on load
        this.ajax($form, update);
    }

    init();
})