/**
 *
 */
define('chartConfiguration', function() {
  /**
   *
   * @returns{{type: string, data: {labels: Array, datasets: [* , * , *]}, options: {responsive: boolean, tooltips: {mode: string, intersect: boolean}, hover: {mode: string, intersect: boolean}, scales: {xAxes: [*], yAxes: [*]}}}}
   * {{
      * type: string,
      * data: {labels: Array, datasets: [null,null,null]},
      * options: {responsive: boolean, tooltips: {mode: string, intersect: boolean},
      * hover: {mode: string, intersect: boolean},
      * scales: {xAxes: [null], yAxes: [null]}}}}
   */
  function getDailyStats(target) {
    var datasets;
    var config = {
      type: 'line',
      data: {
        labels: [],
        datasets: [],
      },
      options: {
        responsive: true,
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true,
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Days',
              },
              ticks: {
                maxTicksLimit: 15,
              },
            }],
          yAxes: [
            {
              display: true,
              responsive: true,
              scaleLabel: {
                display: true,
                labelString: 'Value',
              },
              ticks: {
                beginAtZero: true,
              },
            }],
        },
      },
    };
    switch (target) {

      case 'clicks':
        datasets = [
          {
            label: 'Clicks',
            backgroundColor: 'rgba(96,144,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            lineTension: 0,
            data: [],
            fill: false,
          },
        ];
        break;

      case 'sales_count':
        datasets = [
          {
            label: 'Sales',
            backgroundColor: 'rgba(96,144,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            lineTension: 0,
            data: [],
            fill: false,
          },
        ];
        break;

      case 'cts':
        datasets = [
          {
            label: 'CTS',
            backgroundColor: 'rgba(96,144,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            lineTension: 0,
            data: [],
            fill: false,
          },
        ];
        break;

      case 'site_ctr':
        datasets = [
          {
            label: 'Site_CTR',
            backgroundColor: 'rgba(96,144,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            lineTension: 0,
            data: [],
            fill: false,
          },
        ];
        break;

      case 'avg_position':
        datasets = [
          {
            label: 'Avg_Position',
            backgroundColor: 'rgba(96,144,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            lineTension: 0,
            data: [],
            fill: false,
          },
        ];
        break;

      case 'cpc':
        datasets = [
          {
            label: 'CPC',
            backgroundColor: 'rgba(100,20,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            lineTension: 0,
            data: [],
            fill: false,
          },
        ];
        break;

      case 'click_out_unique':
        datasets = [
          {
            label: 'Click_Out_Unique',
            backgroundColor: 'rgba(67,20,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            lineTension: 0,
            data: [],
            fill: false,
          },
        ];
        break;

      default:
        datasets = [
          {
            label: 'Cost',
            backgroundColor: 'rgba(255,116,86,1)',
            borderColor: 'rgba(255,116,86,1)',
            lineTension: 0,
            data: [],
            fill: false,
          },
          {
            label: 'Revenue',
            data: [],
            backgroundColor: 'rgba(96,144,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            lineTension: 0,
            fill: false,
          },
          {
            label: 'Profit',
            data: [],
            backgroundColor: 'rgba(52,190,0,1)',
            borderColor: 'rgba(52,190,0,1)',
            lineTension: 0,
            fill: false,
          },
        ];
        break;
    }

    config['data']['datasets'] = datasets;

    return config;
  }

  /**
   *
   * @returns {{type: string, data: {labels: Array, datasets: [null,null]}, options: {responsive: boolean, tooltips: {mode: string, intersect: boolean}, hover: {mode: string, intersect: boolean}, scales: {xAxes: [null], yAxes: [null]}}}}
   */
  function getAdvertisersTargets() {
    return {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          {
            label: 'Profit',
            data: [],
            backgroundColor: 'rgba(96,144,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            fill: false,
          },
          {
            label: 'Conversion',
            data: [],
            backgroundColor: 'rgba(52,190,0,1)',
            pointBackgroundColor: [],
            borderColor: 'rgba(52,190,0,1)',
            fill: false,
          },
          {
            label: 'Cpa',
            data: [],
            backgroundColor: 'rgba(241, 196, 15, 1)',
            pointBackgroundColor: [],
            borderColor: 'rgba(241, 196, 15, 1)',
            fill: false,
          },
          // {
          //     label: "TrendLine",
          //     data: [],
          //     backgroundColor: 'rgba(200,30,1,1)',
          //     pointBackgroundColor: [],
          //     borderColor: 'rgba(200,30,1,1)',
          //     fill: false,
          //
          // }
        ],
      },
      options: {

        responsive: true,
        tooltips: {
          mode: 'index',
          intersect: true,
          callbacks: {
            label: function(tooltipItem, data) {
              var label = data.datasets[tooltipItem.datasetIndex].label || '';

              if (label) {
                label += ': ';
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label + '%';
            },
          },
        },
        hover: {
          mode: 'nearest',
          intersect: true,
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Days',
              },
              ticks: {
                maxTicksLimit: 15,
              },
            }],
          yAxes: [
            {
              display: true,
              responsive: true,
              position: 'left',
              scaleLabel: {
                display: true,
                labelString: 'Target ratio',
              },
              ticks: {
                beginAtZero: true,
                callback: function(value, index, values) {
                  if (value < 1 && value > 0) {
                    return parseFloat(value).toFixed(1) + '%';
                  }
                  return value + '%';
                },
              },
              gridLines: {
                zeroLineWidth: 2,
              },
            }],
        },
      },

    };
  }

  function getMorningRoutine() {
    return {
      type: 'line',
      data: {
        labels: [],
        datasets: [
          {
            label: 'Adv 1',
            data: [],
            backgroundColor: 'rgba(96,144,242,1)',
            borderColor: 'rgba(96,144,242,1)',
            steppedLine: 'after',
            fill: false,
          },
          {
            label: 'Adv 2',
            data: [],
            backgroundColor: 'rgba(52,190,0,1)',
            pointBackgroundColor: [],
            borderColor: 'rgba(52,190,0,1)',
            fill: false,
          },

          {
            label: 'Overall',
            data: [],
            backgroundColor: 'rgba(200,30,1,1)',
            pointBackgroundColor: [],
            borderColor: 'rgba(200,30,1,1)',
            fill: false,

          },
        ],
      },
      options: {
        responsive: true,
        tooltips: {
          mode: 'index',
          intersect: false,
        },
        hover: {
          mode: 'nearest',
          intersect: true,
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Days',
              },
              ticks: {
                maxTicksLimit: 15,
              },
            }],
          yAxes: [
            {
              display: true,
              responsive: true,
              scaleLabel: {
                display: true,
                labelString: 'Profit',
              },
              ticks: {
                beginAtZero: true,
              },
              gridLines: {
                zeroLineWidth: 2,
              },
            }],
        },
      },

    };

  }

  return {
    dailyStats: function(target) {
      return getDailyStats(target);
    },
    advertisersTargets: function() {
      return getAdvertisersTargets();
    },
    morningRoutine: function() {
      return getMorningRoutine();

    },
  };

});
