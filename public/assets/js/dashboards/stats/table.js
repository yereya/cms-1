/**
 * Table widget - dashboard usage.
 */
define('tableStats', ['jquery', 'generalHelpers', 'select2'],
    function($, helpers) {
      /**
       *
       * @type {{forms: * | jQuery | HTMLElement, refreshButton: * | jQuery | HTMLElement, category: * | jQuery | HTMLElement, table: {summary: * | jQuery | HTMLElement, alerts: {budgets: * | jQuery | HTMLElement, campaigns: * | jQuery | HTMLElement, accounts: * | jQuery | HTMLElement}, rows: * | jQuery | HTMLElement, columns: * | jQuery | HTMLElement}}}
       *
       */
      var $selectors = {
        filters: {
          fieldsList: $('.fieldsSelector select'),
          displayValues: $('.display-values-wrapper input'),
        },
        forms: {
          stats: $('.table-stats-form'),
          resolveState: $('.table-resolve-state-form'),
        },
        refreshButton: $('.refresh-stats'),
        category: $('[name="category"]'),
        table: {
          wrapper: $('.table-responsive'),
          targets: $('.targets .table-responsive'),
          summary: $('.summary .table-responsive'),
          managerTarget: $('.manager-target .table-responsive'),
          pageSpeed: $('.page-speed .table-responsive'),
          alerts: {
            global: $('.table-stats'),
            budgets: $('.alerts .budgets .table-responsive'),
            campaigns: $('.alerts .campaigns .table-responsive'),
            accounts: $('.alerts .accounts .table-responsive'),
          },
          rows: '.data_row',
          columns: $('.column'),
        },
        search: {
          button: $('.searchInput'),
          wrapper: $('.searchBar'),
        },
      };

      var selectors = {
        columns: '.column',
        columnWrapper: '.data_row',
        headers: '.headers_row',
      };

      /**
       *  Init Stats
       */
      function init() {
        initStats();
        initEvents();
      }

      /**
       *  List of all events that load on initialization.
       */
      function initEvents() {

        /*Update stats according to filters*/
        $selectors.refreshButton.on('click', updateStats);

        /*Update alerts actions*/
        $selectors.table.alerts.global.on('change', '.action', saveActionState);

        /*Search in table*/
        $selectors.search.button.on('keyup', filterStats);

        /*Update table field list */
        $selectors.filters.fieldsList.on('change', updateFieldList);

        /*Update display values*/
        $selectors.filters.displayValues.on('change', updateStats);
      }

      /**
       * Stats Initialization
       *
       * @param event
       * @param update
       */
      function initStats(event, update) {
        $selectors.forms.stats.each(function($key, form) {
          var $form = $(form);
          requestToTable($form, update);
        });
      }

      /**
       *
       * Save action state
       *
       * @param elem
       */
      function saveActionState(elem) {
        var $wrapper = $(elem).closest(selectors.columnWrapper);
        var $form = $($selectors.forms.resolveState);
        var $stateElem = resolveStateTag($wrapper);

        /*get props that need to be updated*/
        var id = $wrapper.find('.record-id').attr('record_id');
        var state = $stateElem.val();

        //set the props to the form
        $form.find('[name=record_id]').val(id);
        $form.find('[name=state]').val(state);

        saveAlertState($form);

        /**
         *
         * @param $wrapper
         * @returns {*}
         */
        function resolveStateTag($wrapper) {

          return $wrapper.find('input').length > 0
              ? $wrapper.find('input')
              : $wrapper.find(':selected');
        }

        /**
         *
         * @param $form
         */
        function saveAlertState($form) {
          this.ajax = function() {
            var tableParams = $form.serializeArray();
            var that = this;

            var settings = {
              method: $form.attr('method'),
              url: $form.attr('action'),
              data: tableParams,
              beforeSend: function() {
                App.blockUI({target: $form.closest('.portlet')});
              },
              complete: function(stats) {
                App.unblockUI($form.closest(' .portlet'));

                if (helpers.isJson(stats.responseText)) {
                  var stats = JSON.parse(stats.responseText);

                  that.resolve(stats);
                }
                else {
                  console.warn('no data passed');
                }
              },
            };

            $.ajax(settings);
          };

          this.resolve = function() {
            toastr.success('feedback stored successfully');
          };

          this.ajax();
        }
      }

      /**
       * Filter results by search field
       *
       * @param target
       */
      function filterStats(event, target) {

        // Declare variables
        var $input, filter, $table, $trs, $tr;
        $input = target ? target : $(event.currentTarget);
        filter = $input.val().toLowerCase();
        $table = $input.closest('.table-stats').find('.table-responsive');
        $trs = $table.find('.data_row');

        $trs.each(function(key, tr) {
          $tr = $(tr);

          if ($tr.text().toLowerCase().indexOf(filter) === -1) {
            $tr.hide();

            return;
          }

          $tr.show();
        });
      }

      /**
       *  This part is one of the optional filter you can add in table:
       *
       *  In order to use it:
       *   - developer need to pass filter object inside table blade = (partials.containers.dashboards.stats.table)
       *   example:
       *   'filters'=> [
       'fieldSelection' => true, ==> this one is for this specific method
       'searchBar' => true
       ];
       *
       * @description
       *  - get the relevant title.
       *  - find fields columns.
       *  - hide or show them.
       *  - save list to local storage.
       *
       * @param e
       */
      function updateFieldList(e) {
        var selected = $(e.target);
        var $tag = $(event.target);
        var title = resolveFieldByElemType($tag);

        var fieldName = $(selectors.headers).
            find('[data-title="' + title + '"]').
            attr('data-field');
        var fieldColumns = selectors.columns + '[data-field="' + fieldName +
            '"], ' + selectors.headers + ' [data-field="' + fieldName + '"]';
        $(fieldColumns).toggle();

        saveFieldsToLocalStorage(selected);

        /**
         *
         * @param $tag
         * @returns {*}
         */
        function resolveFieldByElemType($tag) {
          return $tag.prop('tagName') === 'SPAN'
              ? $tag.parent().attr('title')
              : $tag.text();
        }

        /**
         *
         * @param selected
         */
        function saveFieldsToLocalStorage(selected) {
          var fieldsArray = [];
          selected.find('option:selected').each(function(key, record) {
            var fieldName = $(record).attr('data-field').toLowerCase();

            if ($.inArray(fieldName, fieldsArray) === -1) {
              fieldsArray.push(fieldName);
            }
          });
          localStorage.setItem('fields', JSON.stringify(fieldsArray));
        }
      }

      /**
       * Update Stats according to the chosen filters
       *
       * @param event
       */
      function updateStats(event) {
        event.preventDefault();
        initStats(event, true);
      }

      /**
       * Create table header's
       *
       * @param $table
       * @param stats
       * @param savedFields
       * @param fieldSelectionFeature
       */
      function createHeaders(
          $table, stats, savedFields, fieldSelectionFeature) {
        var headers = stats.headers;
        var $tr = $('<tr></tr>').addClass('headers_row');

        $.each(headers, function(key, header) {
          var $th = $('<th>');
          var className = helpers.toSlug(header.label);
          header['key'] = header['key'].toLowerCase();

          $th.addClass(className + '_header');
          $th.text(header['label']);
          $th.attr('data-index', key);
          $th.attr('data-field', header['key']);
          $th.attr('data-title', header['label']);

          if (fieldSelectionFeature
              && savedFields.length
              && $.inArray(header['key'], savedFields) === -1)

            $th.hide();

          $tr.append($th);
        });
        $table.append($tr);
      }

      /**
       * Insert value according to attributes
       *
       * @param $td
       * @param block
       */
      function insertTextByAttributes($td, block) {
        var attributes = block && block['attributes']
            ? block['attributes']
            : undefined;
        var record = block && block['value'] !== null
            ? (block['value'] === 0
                ? (block['value']).toString()
                : block['value']) : undefined;
        var ratio = block && block['ratio'] !== null
            ? (block['ratio'] === 0)
                ? (block['ratio']).toString()
                : block['ratio'] : undefined;

        var ratio_is_number = jQuery.isNumeric(ratio);
        var ratio = ratio_is_number ? ratio : 'NA';
        var cpaField = ($td.attr('data-field')).indexOf('cpa') !== -1;
        let isPpcTargetDashboard = attributes['dashboard'] ===
            'ppc-target';
        let isActualField = (attributes['description']) &&
            (attributes['description'] === 'actual-record');

        /*check if there's attribute prop */
        if (typeof attributes !== 'undefined') {
          resolveClass();
          resolveFieldAttributes();
          resolveValue();
        }

        function resolveEmptyValue() {
          var emptyAttr = attributes['empty'];
          if (emptyAttr !== undefined) {
            $td.text('');
          }
        }

        /**
         * Set basic value to general fields
         * @returns {*}
         */
        function resolveBasic() {
          let field = $td;

          let description = attributes['description'] || null;
          let validActualRecordState = description && description ===
              'actual-record' && record !== '';

          if (validActualRecordState) {
            field = $td.find('.notification');
          }

          return field.text(record);
        }

        /**
         *  Resolve
         */
        function resolveRatio() {
          if (ratio !== undefined && !cpaField) {
            $td.find('.notification').append('<div class="ratio">');
            let $ratioField = $td.find('.ratio');
              if ($.isNumeric(ratio) && ratio != 0) {
                  return $ratioField.text(`(${Math.round(ratio)}%)`);
              }
              $ratioField.text(`(${ratio})`);
          }
        }

        function resolveValue() {

          if (record !== undefined) {
            resolveBasic();
            resolveRatio();
            resolveSuffix();

            return true;
          }
          resolveEmptyValue();
        }

        /**
         * Add classes according to attributes
         */
        function resolveClass() {
          setGeneral();
          setFontColorAccordingToPageSpeedScore();
          setFontColorAccordingToMorningRoutineRatio();
          setFontColorAccordingToYesterdayScore();
          setBgColorAccordingToPpcTargetVsActualRatio();

          /**
           *  resolve the relevant color according to flag attribute
           *  2 = blue
           *  1 = red
           *  0 = no color
           */
          function setFontColorAccordingToPageSpeedScore() {

            if (attributes['mobile']) {
              var flag = attributes['mobile'];
              switch (flag) {
                case '2':
                  $td.addClass('blue');
                  return;
                case '1':
                  $td.addClass('red');
                  return;
              }
            }
            else if (attributes['desktop']) {
              var flag = attributes['desktop'];
              switch (flag) {
                case '1':
                  $td.addClass('red');
                  return;
                case '2':
                  $td.addClass('blue');
                  return;
              }
            }
          }

          /**
           * Resolve Background color according to ratio result
           *
           * @return {boolean}
           */
          function setBgColorAccordingToPpcTargetVsActualRatio() {
              if (!isPpcTargetDashboard || !ratio) {
                  return false;
              }

              $td.find('>div').removeClass('bg-red bg-yellow bg-green');

              if (ratio > 100) {
                  if (cpaField) {
                      return $td.find('> div').addClass('bg-red');
                  }
                  $td.find('> div').addClass('bg-green');
              }
              else if (ratio >= 80 && ratio <= 100) {
                  if (cpaField) {
                      return $td.find('>div').addClass('bg-green');
                  }
                  $td.find('> div').addClass('bg-yellow');
              }
              else {
                  if (cpaField) {
                      return $td.find('> div').addClass('bg-green');
                  }
                  $td.find('>div').addClass('bg-red');
              }
          }

          /**
           *
           */
          function setFontColorAccordingToMorningRoutineRatio() {
            /*assign font color according to the color*/
            if (attributes['ratio']) {
              var ratio = attributes['ratio'];

              if (ratio > 120) {
                if (attributes['class'] === 'cpa') {
                  $td.addClass('red');
                  return;
                }
                $td.addClass('blue');
              }
              else if (ratio < 80) {
                if (attributes['class'] === 'cpa') {
                  $td.addClass('blue');
                  return;
                }

                $td.addClass('red');
              }
            }
          }

          function setFontColorAccordingToYesterdayScore(attributes, $td) {

            if (attributes && attributes['ratio']) {
              /*assign font color according to yesterday's score*/
              let ratio = attributes['ratio'];
              if (ratio > 120) {
                if (attributes['class'] === 'cpa') {
                  $td.addClass('red');
                  return;
                }
                $td.addClass('blue');
              }
              else if (ratio < 80) {
                if (attributes['class'] === 'cpa') {
                  $td.addClass('blue');
                  return;
                }

                $td.addClass('red');
              }
            }
          }

          /**
           * set General classes
           */
          function setGeneral() {
            if (attributes['type'] === 'subject') {
              $td.addClass('subject');
            }

            //add class if it exist else add global class
            if (attributes['class'] !== undefined) {
              var blockClass = attributes['class'];
              $td.addClass(blockClass);

              if (blockClass.search('actual-field') !== -1) {
                $td.append('<div>');
                $td.find('div').addClass('notification');
              }
            }
            $td.addClass('column');
          }
        }

        /**
         * Set Suffix attribute (%,$)
         */
        function resolveSuffix() {
          let suffix = attributes['suffix'];
          let decimal = attributes['decimal'] ? attributes['decimal'] : 0;
          let recordWithSuffix;
          let _record = $td.text();

          if (isPpcTargetDashboard && suffix) {
            return wherePpcTarget();
          }

          whereOtherDashboards();

          /**
           *
           * @returns {string}
           */
          function whereOtherDashboards() {
            let val;
            if (suffix !== undefined) {

              if (suffix === '%') {
                val = getWithPercentage();
              } else {
                val = getWithCurrency();
              }

              $td.text(val);
            }
          }

          /**
           *
           * @returns {boolean}
           */
          function wherePpcTarget() {

            if (isPpcTargetDashboard) {

              if (suffix === '%') {
                record = getWithPercentage();
              } else {
                record = getWithCurrency();
              }

              /*ACTUAL FIELD*/
              if (isActualField) {
                let $notification = $td.find('.notification');
                $td.find('.ratio').text('');

                /*SET THE RECORD WITH PREFIX */
                $notification.text(record);

                /*SET THE RATIO*/
                if (!cpaField) {
                  let $ratio = $('<div>').addClass('ratio');
                    ratio = (ratio < 1) && (ratio > 0) ? ratio.toFixed(2) : ratio;
                  $ratio.text(`(${ratio}%)`);

                    if (ratio == 0 || ratio == 'NA') {
                        $ratio.text(`(${ratio})`)
                    }

                  $notification.append($ratio);
                }

                return true;
              }

              /*TARGET FIELD*/
              $td.text(record);
            }
          }

          /**
           *
           * @returns {string}
           */
          function getWithPercentage(target = null) {

            if (target) {
              _record = target;
            }
            /*if there's no decimal point*/
            let recordWithSuffix = _record + suffix;

            if (parseFloat(_record) == 0) {
              return 0;
            }

            /*  display the prefix with selected decimal number*/
            if (parseFloat(_record) % 1 !== 0) {
              recordWithSuffix = parseFloat(_record * 100).toFixed(decimal) +
                  suffix;
            }

            return recordWithSuffix;
          }

          /**
           *
           * @returns {string}
           */
          function getWithCurrency(target = null) {

            if (target) {
              _record = target;
            }
            recordWithSuffix = parseFloat(_record).toLocaleString('en-US',
                {style: 'currency', currency: 'USD'}).slice(0, -3);

            return recordWithSuffix;
          }
        }

        /*Attach attribute according to the data prop*/
        function resolveFieldAttributes() {
          if (attributes['data']) {
            if (Array.isArray(attributes['data'])) {
              attributes['data'].map(($val, $key) => {
                $td.attr($key, $val);
              });

              return true;
            }
            $td.attr(attributes['data']['key'], block['value']);
          }
        }
      }

      /**
       * Create table raw's
       *
       * @param $table
       * @param stats
       * @param savedFields
       * @param fieldSelectionFeature
       */
      function createRows($table, stats, savedFields, fieldSelectionFeature) {
        var rows = $.isArray(stats.data) && stats.data.length > 1
            ? stats.data
            : stats.data[0];

        $.each(rows, function(key, subject) {
          var morningRoutineTable = subject['advertiser_name'] !== undefined;

          if (morningRoutineTable) {
            try {
              var advertiser_name = subject['advertiser_name']['value'];
              advertiser_name = advertiser_name ? advertiser_name.replace(
                  /(-|\.| |_)/g, '_').toLowerCase() : '';

            } catch (e) {
              console.log(rows);
              console.log(e);
            }
          }

          //Build table row
          var $tr = $('<tr>').addClass('data_row');

          if (morningRoutineTable) {
            $tr.attr('data-advertiser', advertiser_name);
          }

          //Iterate over all data  - columns
          $.each(subject, function(field, block) {

            var $td = $('<td>');
            field = field.toLowerCase();
            $td.attr('data-field', field);

            if (fieldSelectionFeature && savedFields.length &&
                $.inArray(field, savedFields) === -1)
              $td.hide();

            insertTextByAttributes($td, block);

            $tr.append($td);
            $table.append($tr);
          });
        });
      }

      /**
       * Ajax request to table
       *
       *  1. Make ajax request.
       *  2. Load the table
       *
       * @param $form
       * @param update
       */
      function requestToTable($form, update) {
        this.create = function(stats, tableParams) {
          var $table = $('<table>').addClass('table stripped');
          var alertSection = tableParams['section'] === 'alerts';
          var $selector = $selectors.table[tableParams['section']];
          var $domFields = $selectors.filters.fieldsList;
          var savedFields = localStorage.getItem('fields')
              ? JSON.parse(localStorage.getItem('fields'))
              : [];
          var fieldSelectionFeature = haveFieldSelectFeature(tableParams);
          var searchTableFeature = haveSearchFilter(tableParams);

          if (fieldSelectionFeature) {
            resolveFieldList($domFields, stats.headers, savedFields);
          }

          if (searchTableFeature) {
            $($selectors.search.wrapper).removeClass('hidden');
          }

          createHeaders($table, stats, savedFields, fieldSelectionFeature);
          createRows($table, stats, savedFields, fieldSelectionFeature);

          /*declare after creating rows*/
          var actionButton = $table.find('.action').length > 0;

          /*if needed add action buttons*/
          if (actionButton) {
            attachSelectorsBySection();
          }

          /*if alertSection needed change selector*/
          if (alertSection) {
            $selector = $selectors.table[tableParams['section']][tableParams['table_name']];
          }

          $selector.prepend($table);
          filterStats(null, $selectors.search.button);

          /**
           *
           * @returns {boolean}
           */
          function haveFieldSelectFeature(tableParams) {
            return tableParams['field_selection'] !== undefined;
          }

          /**
           *
           * @param tableParams
           * @returns {boolean}
           */
          function haveSearchFilter(tableParams) {
            return tableParams['searchBar'] !== undefined;
          }

          /**
           *
           * @param $domFields
           * @param headers
           * @param savedFields
           */
          function resolveFieldList($domFields, headers, savedFields) {

            $.each(headers, function(key, header) {
              var $option = $('<option>');
              header['key'] = header['key'].toLowerCase();

              $option.attr('data-field', header['key']);
              $option.attr('selected', 'selected');
              $option.text(header['label']);
              $option.val(key);

              /*
                - if there's fields that saved from the last session
                - If there's field that doesn't  in the that list - remove them from the selected fields
             */
              if (savedFields.length &&
                  $.inArray(header['key'], savedFields) === -1) {
                $option.removeAttr('selected');
              }

              $domFields.append($option);
            });

            $domFields.select2();

            $('.fieldSelectorWrapper').removeClass('hidden');
          }

          /**
           * Attach selector buttons
           */
          function attachSelectorsBySection() {
            if (tableParams['table_name'] === 'budgets') {
              return buildMultiSelect($table);
            }

            return buildMultiSelectWithInput($table);

            /**
             *
             * @param $table
             */
            function buildMultiSelect($table) {
              $table.find('.action').append(
                  '<select class=\'btn btn-default\' >' +
                  '<option value=\'-1\' disabled selected>Choose Action</option>' +
                  '<option value=\'Increase Budget\'>Increase Budget</option>' +
                  '<option value=\'Decrease Budget\'>Decrease Budget</option>' +
                  '<option value=\'No Action\'>No Action</option>' +
                  '</select>');

            }

            /**
             *
             * @param $table
             */
            function buildMultiSelectWithInput($table) {
              $table.find('.action').append(
                  '<input list=\'actionList\' name=\'action\' class=\'input btn btn-default\'>' +
                  '<datalist id=\'actionList\'>' +
                  '<option value=\'Impression Increase\'>' +
                  '<option value=\'CTR Increase\'>' +
                  '<option value=\'Bid Increase\'>' +
                  '<option value=\'Fraud\'>' +
                  '</datalist>',
              );
            }
          }
        };

        this.update = function(stats, otherParams) {
          var rows = $.isArray(stats.data) && stats.data.length > 1
              ? stats.data
              : stats.data[0];

          $('.table-responsive').html('');
          this.create(stats, otherParams);
        };

        this.ajax = function($form, update) {
          var that = this;
          var $filters = $('.' + $form.data('controllers'));
          var tableParams = $form.serializeArray();
          var otherParams = {
            'section': $form.find('[name=section]').val(),
            'table_name': $form.find('[name=table_name]').val(),
            'field_selection': $form.find('[name=fieldSelection]').val(),
            'searchBar': $form.find('[name=searchBar]').val(),
            'table_description': $form.find('[name=table_description]').val(),
          };
          var $rows = $($selectors.table.rows);

          if (update) {
            var filterData = $filters.serializeArray();
            var formArray = $form.serializeArray();
            tableParams = $.merge(formArray, filterData);
          }

          var settings = {
            method: $form.attr('method'),
            url: $form.attr('action'),
            data: tableParams,
            beforeSend: function() {
              App.blockUI({target: $form.closest('.portlet')});
            },
            complete: function(stats) {
              App.unblockUI($form.closest(' .portlet'));

              if (!helpers.isJson(stats.responseText)) {
                console.warn('no data passed');
                return;
              }

              stats = JSON.parse(stats.responseText);

              if (!stats.data) {
                var noRecordFound = 'records not found';
                var noRecordAssociated = `No records at this date range for section ${otherParams.table_description}`;
                toastr.error(
                    otherParams.table_description
                        ? noRecordAssociated
                        : noRecordFound,
                );
                return;
              }

              if (!update || !$rows.length) {
                that.create(stats, otherParams);
                return;
              }

              that.update(stats, otherParams);
            },
          };

          $.ajax(settings);
        };

        this.ajax($form, update);
      }

      init();
    })
;
