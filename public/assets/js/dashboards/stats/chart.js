/**
 * Chart widget - Dashboard usage
 */
define('chartStats',
    ['chartJs', 'chartConfiguration', 'generalHelpers'],
    function(Chart, configuration, helpers) {

      var duration;
      var myChart;

      /**
       *
       * @type {{counter: number, XAxis: Array}}
       */
      var trendLine = {
        counter: 0,
        XAxis: [],
      };

      /**
       *
       * @type {{refreshButton: jQuery|HTMLElement, durationRadio: jQuery|HTMLElement, ctx: jQuery|HTMLElement, $form: jQuery|HTMLElement, targets: jQuery|HTMLElement}}
       */
      var selectors = {
        refreshButton: $('.refresh-stats'),
        durationRadio: $('[name=duration]'),
        ctx: $('#chart'),
        $form: $('.chart-form'),
        targets: $('[name=\'target\']'),
      };

      //Get chart configuration by data-attribute name data-name="
      var name = selectors.$form.data('name');
      var chartConfig = configuration[name]();

      /**
       *
       * Initialize the Chart
       *
       */
      function init() {
        formToChart(selectors.$form);
        initEvents();
      }

      /**
       * Init All Events
       */
      function initEvents() {
        selectors.refreshButton.on('click', updateChart);
        selectors.durationRadio.on('click', updateXAxisName);
        selectors.targets.on('change', updateTargets);
      }

      /**
       * update XAxisName
       */
      function updateXAxisName(event) {
        var options = {
          'daily': 'Days',
          'weekly': 'Weeks',
          'monthly': 'Months',
        };
        duration = options[($(this).val())];
      }

      /**
       * Update Chart
       */
      function updateChart() {
        formToChart(selectors.$form, true);
      }

      /**
       * Form To Chart
       * @param $form
       * @param update
       */
      function formToChart($form, update) {
        this.prepare = function(stats) {
          var target = $form.find('[name="target_type"]').val();
          var dynamicMode = $('[name=dynamic_mode]').length > 0;
          var label;
          var targetColumn;
          var options = {
            'min': stats.min,
            'ticks': stats.max,
            'stepZero': Math.floor(stats.max / 10),
            'beginAtZero': true,
          };

          if (update) {
            $.each(chartConfig.data.datasets, function(key, dataset) {
              chartConfig.data.datasets[key].data = [];
            });

            trendLine.counter = 0;
            trendLine.XAxis = [];
            if (chartConfig.data.datasets.length > 1) {
              chartConfig.data.datasets[1].pointBackgroundColor = [];
            }
            chartConfig.data.labels = [];
          }
          if (typeof duration !== 'undefined') {
            chartConfig.options.scales.xAxes[0].scaleLabel.labelString
                = helpers.capitalizeString(duration);
          }

          if (typeof  target !== 'undefined') {
            chartConfig.options.scales.yAxes[0].scaleLabel.labelString
                = helpers.capitalizeString(target);
          }

          /*Update options coordinates*/
          $.each(options, function(key, option) {
            chartConfig.options.scales.yAxes[0].ticks[key] = option;
          });

          //Dynamic mode is set when (add param
          if (dynamicMode) {
            //store the column name
            label = $('[name=dynamic_label]').val();
            targetColumn = $('[name=dynamic_column]').val();
            chartConfig.data.datasets = [];
            var randomColors = [];

            createRandomColors();

            //build dynamic graph according to the labels
            $.each(stats.labels, function(key, label) {
              chartConfig.data.datasets[key] = {
                label: label,
                data: [],
                backgroundColor: randomColors[key],
                borderColor: randomColors[key],
                fill: false,
              };
            });
          }

          /*Insert Data Into Chart*/
          $.each(stats, function(key, stat) {
            $.each(chartConfig.data.datasets, function(index, target) {
              var record;

              //dynamic graphs - advertisers
              if (dynamicMode) {
                if (stat[label]) {
                  if (target.label.toLowerCase() ===
                      stat[label].toLowerCase()) {
                    record = parseFloat(stat[targetColumn]);
                  }
                  else {
                    return true;
                  }
                }

              }
              else {
                record = parseFloat(stat[(target.label).toLowerCase()]);
              }

              if (target.label === 'Sales') {
                target.label = 'sales_count';
              }

              resolvePointColor(index, stat, target);
              chartConfig.data.datasets[index].data.push(record);
            });
            if (typeof stat.date !== 'undefined') {
              if (chartConfig.data.labels.indexOf(stat.date) === -1) {
                chartConfig.data.labels.push(stat.date);
              }
            }

          });

          if (helpers.isValueExistInArray(chartConfig.data.datasets, 'label',
              'TrendLine')) {

            /*Set trend line Axis*/
            resolveTrendLine();
          }

          this.chart();

          /**
           *  Create random colors for chart ( depending on label number)
           */
          function createRandomColors() {
            for (var i = 0; i < stats.labels.length; i++) {
              var randomColor = helpers.getRandomColor();

              if (randomColors.indexOf(randomColor) !== -1) {
                i--;
                continue;
              }

              randomColors.push(randomColor);
            }
          };
        };
        this.ajax = function($form) {
          var that = this;
          var $filters = $('.' + $form.data('controllers'));
          var filterData = $filters.serializeArray();
          var formArray = $form.serializeArray();
          var formParams = $.merge(formArray, filterData);

          var setting = {
            method: $form.attr('method'),
            url: $form.attr('action'),
            data: formParams,
            beforeSend: function() {
              App.blockUI({target: $form.closest('.portlet')});
            },
            complete: function(stats) {
              if (helpers.isJson(stats.responseText)) {
                var stats = JSON.parse(stats.responseText);
                that.prepare(stats);
              } else {
                console.warn('no data passed');
                App.unblockUI($form.closest('.portlet'));
              }
            },
          };
          $.ajax(setting);
        };
        this.chart = function() {
          if (update && typeof myChart !== 'undefined') {

            myChart.destroy();
          }
          myChart = new Chart(selectors.ctx, chartConfig);
          App.unblockUI($form.closest('.portlet'));
        };

        //Init on load
        this.ajax($form, update);
      }

      /**
       *
       * Update charts graphs
       *
       * @param event
       */
      function updateTargets() {
        var $target = $(event.target);
        selectors.$form.find('[name="target_type"]').val($target.attr('id'));
        chartConfig = configuration[name]($target.attr('id'));
        formToChart(selectors.$form, true);
      }

      /**
       *
       * Resolve points color
       *
       * @param stat
       * @param record
       */
      function resolvePointColor(index, stat, target) {
        var is_result_record = target.label === 'Result';
        var beneath_target = parseInt(stat.target) > parseInt(stat.result);
        if (typeof stat.result !== 'undefined') {
          if (is_result_record) {
            if (beneath_target) {
              chartConfig.data.datasets[index].pointBackgroundColor.push(
                  'rgba(215,62,44,0.9)');

              return;
            }
            chartConfig.data.datasets[index].pointBackgroundColor.push(
                'rgba(18, 221, 105, 0.9)');
          }
        }
      }

      /*
          Calculate Linear regression (For trend line)
      */
      function findLineByLeastSquares(values_x, values_y) {
        var sum_x = 0;
        var sum_y = 0;
        var sum_xy = 0;
        var sum_xx = 0;
        var count = 0;

        /*
         * We'll use those variables for faster read/write access.
         */
        var x = 0;
        var y = 0;
        var values_length = values_x.length;

        if (values_length !== values_y.length) {
          throw new Error(
              'The parameters values_x and values_y need to have same size!');
        }

        /*
         * Nothing to do.
         */
        if (values_length === 0) {
          return [[], []];
        }

        /*
         * Calculate the sum for each of the parts necessary.
         */
        for (var v = 0; v < values_length; v++) {
          x = values_x[v];
          y = values_y[v];
          sum_x += x;
          sum_y += y;
          sum_xx += x * x;
          sum_xy += x * y;
          count++;
        }

        /*
         * Calculate m and b for the formular:
         * y = x * m + b
         */
        var m = (count * sum_xy - sum_x * sum_y) /
            (count * sum_xx - sum_x * sum_x);
        var b = (sum_y / count) - (m * sum_x) / count;

        /*
         * We will make the x and y result line now
         */
        var result_values_x = [];
        var result_values_y = [];

        for (var v = 0; v < values_length; v++) {
          x = values_x[v];
          y = x * m + b;
          result_values_x.push(x);
          result_values_y.push(y);
        }

        return [result_values_x, result_values_y];
      }

      /**
       * Set The y Axis To Trend Line
       */
      function resolveTrendLine() {

        /*Prepare the results as the y axis*/
        trendLine.YAxis = chartConfig.data.datasets[1].data;

        /*Prepare trend line x Axis*/
        $.each(trendLine.YAxis, function(index, value) {
          trendLine.XAxis.push(index);
        });

        /*Calculate the linear regression */
        trendLine.results = findLineByLeastSquares(trendLine.XAxis,
            trendLine.YAxis);

        /*Set the results to the trendLine graph*/
        chartConfig.data.datasets[2].data = trendLine.results[1];
      }

      init();
    },
);
