define('dynamicHelpers', ['jquery'], function () {

    var helpers = {
        filterBy: function (stats, wheres) {
            var stats = stats.filter(function (stat) {
                var bool = true;
                for (var key in wheres) {
                    if (Array.isArray(wheres[key])) {
                        if (wheres[key].indexOf(stat[key]) == -1) {
                            bool = false;
                        }
                    } else {
                        if (stat[key] != wheres[key]) {
                            bool = false
                        }
                    }
                }
                return bool
            });

            return stats;
        },

        getFieldSummery: function (record, column) {
            var sum = record.reduce(function (sum, stat) {
                return sum + stat[column]
            }, 0);

            return Math.round(sum);
        },

      activateTreeItem: function(listItem, selector, multiSelection) {
            "use strict";

        if (!multiSelection) {
          listItem.closest(selector).find('li').removeClass('active');
        }
        listItem.closest('.dd-item').hasClass('active')
          ? listItem.closest('.dd-item').removeClass('active')
          : listItem.addClass('active');
        }
    };

    return helpers;
});
