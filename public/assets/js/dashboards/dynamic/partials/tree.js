define('dynamicTree',
    ['jquery', 'dynamicStates', 'treeBuilder', 'generalHelpers'],
    function ($, states, treeBuilder, helpers) {

        var selectors = {
                wrapper: '#treeWrapper',
                body: '#treeWrapper .portlet-body',
                filters: '.filters select',
                type: '#treeWrapper .listType-wrapper button',
                sortBy: {
                    wrapper: '.sort-by-buttons li'
                },
                marked: '.fa-star.favorite',
                olTree: '#tree'
            },
            devices = {
                names: {
                    'c': 'desktop',
                    'm': 'mobile',
                    't': 'tablet',
                    '': 'other',
                    'NA': 'other'
                },
                translate: function (device) {
                    var name = undefined;

                    $.each(devices.names, function (key, value) {
                        if (device === value) {
                            name = value;
                        }
                    });

                    return name;
                },

                normalize: function (key) {
                    var name = undefined;

                    $.each(devices.names, function (column, fullName) {
                        if (column == key) {
                            name = fullName
                        }
                    });

                    return name;
                }
            },
            alertTypes = {
                names: {
                    'Low CTS': 'lowCTS',
                    'High CTS': 'highCTS'
                },
                translate: function (alertType) {
                    var name = undefined;

                    $.each(alertTypes.names, function (key, value) {
                        if (alertType === value) {
                            name = value;
                        }
                    });

                    return name;
                }
            },
          treeUpdateBuffer = {},
          sortHirrarchy = ['account_name', 'campaign_name', 'ad_gourp_name', 'match_type_four', 'device', 'keyword'];



        var _tree = {
                marked: [],
                init: function () {

                    //Init Tree
                    _tree.buildByView();
                    states.records = _tree.records;
                },

                getViewType: function () {

                    var viewTypes = $(selectors.type);

                    //filter button with active property
                    var activeView = viewTypes.filter(function (key, view) {
                        return $(view).hasClass('active');
                    });

                    var type = activeView.attr('data-type');

                    return type;
                },

                /**
                 * get sorting param
                 */
                getSortingParam: function (param) {
                    var sortByButtons = $(selectors.sortBy.wrapper);
                    var activeButton = sortByButtons.filter(function (key, button) {

                        return $(button).hasClass('active');
                    });

                    return activeButton.attr('data-' + param);
                },

                /**
                 * Build tree by target view
                 */
                buildByView: function () {

                    var tree = {};

                    return buildTreeByView();

                    /**
                     * @description build tree according to view
                     */
                    function buildTreeByView() {

                        //get view type
                        var view = _tree.getViewType();

                        //get sort param
                        var sortBy = _tree.getSortingParam('sort');

                        var direction = _tree.getSortingParam('direction');

                        /*build tree by view*/
                        switch (view) {
                            case 'flat':
                                tree = buildFlat(sortBy, direction);
                                break;

                            case 'marked':
                                tree = buildMarked(sortBy, direction);
                                break;

                            case 'hierarchy':
                                tree = buildHierarchy();
                                break;
                        }

                        _tree.data = tree;

                        return tree;
                    }

                    /**
                     * @description Build Hierarchy
                     */
                    function buildHierarchy() {
                        var tree = {};

                        //level 1
                        //ALERTS -> Advertisers
                        $.map(_tree.records, function (item) {
                            var advertiserState = typeof tree[item['advertiser_id']] !== 'undefined';

                            //if advertiser doest exist create it
                            if (!advertiserState) {
                                tree[item.advertiser_id] = buildNodeItem('Advertiser', item.advertiser_name)
                            }
                        });

                        //Level 2
                        //ALERT -> Accounts
                        $.map(_tree.records, function (item) {
                            var _tree = tree[item.advertiser_id].children;
                            if (item.alert_level == 'account Level') {
                                _tree[item.account_id] = buildAlertItem(item, 'Account', item.account_name)
                            } else {
                                _tree[item.account_id] = buildNodeItem('Account', item.account_name)
                            }
                        });

                        //level 3
                        /**
                         *   alerts -> Device | Match type | Campaign
                         */
                        $.map(_tree.records, function (item) {
                            var _tree = tree[item.advertiser_id].children[item.account_id].children;

                            // ALERTS
                            if (item.alert_level == 'account+Device Level') {
                                _tree[item.device] = buildAlertItem(item, 'Device', devices.normalize(item.device));
                            }
                            else if (item.alert_level == 'account+MT Level') {
                                _tree[item.match_type_four] = buildAlertItem(item, 'Match Type', item.match_type_four)
                            }
                            else if (item.alert_level == 'Campaign Level') {
                                _tree[item.campaign_id] = buildAlertItem(item, 'Campaign', item.campaign_name)
                            }

                            // NODES
                            else if (item.alert_level == 'account+MT+Device Level') {
                                if (!_tree[item.match_type_four]) {
                                    _tree[item.match_type_four] = buildNodeItem('Match Type', item.match_type_four);
                                }
                            }
                            else if (item.alert_level == 'Campaign+MT Level') {
                                if (!_tree[item.campaign_id]) {
                                    _tree[item.campaign_id] = buildNodeItem('Campaign', item.campaign_name);
                                }
                            }
                            else if (item.alert_level == 'Campaign+Device Level') {
                                if (!_tree[item.campaign_id]) {
                                    _tree[item.campaign_id] = buildNodeItem('Campaign', item.campaign_name);
                                }
                            }
                            else if (item.alert_level == 'Campaign+MT+Device Level') {
                                if (!_tree[item.campaign_id]) {
                                    _tree[item.campaign_id] = buildNodeItem('Campaign', item.campaign_name);
                                }
                            }
                            else if (item.alert_level == 'Ad_group Level') {
                                if (!_tree[item.campaign_id]) {
                                    _tree[item.campaign_id] = buildNodeItem('Campaign', item.campaign_name);
                                }
                            }
                            else if (item.alert_level == 'Ad_group+MT Level') {
                                if (!_tree[item.campaign_id]) {
                                    _tree[item.campaign_id] = buildNodeItem('Campaign', item.campaign_name);
                                }
                            }
                            else if (item.alert_level == 'Ad_group+Device Level') {
                                if (!_tree[item.campaign_id]) {
                                    _tree[item.campaign_id] = buildNodeItem('Campaign', item.campaign_name);
                                }
                            }
                            else if (item.alert_level == 'Ad_group+MT+Device Level') {
                                if (!_tree[item.campaign_id]) {
                                    _tree[item.campaign_id] = buildNodeItem('Campaign', item.campaign_name);
                                }
                            }
                            else if (item.alert_level == 'Keyword Level') {
                                if (!_tree[item.campaign_id]) {
                                    _tree[item.campaign_id] = buildNodeItem('Campaign', item.campaign_name);
                                }
                            }
                        });

                        //level 4
                        /* ALERTS -> Account+MT+Device | Campaign+MT | Campaign+Device | Ad_group */
                        $.map(_tree.records, function (item) {

                            var _treeCampaigns = {};
                            var _treeMT = {};
                            if (item.match_type_four !== '' && item.match_type_four !== '0') {
                              _treeMT = tree[item.advertiser_id].children[item.account_id].children;
                            }

                            if (item.campaign_id !== '' && item.campaign_id !== '0' && Object.keys(_treeMT).length) {
                                _treeCampaigns = tree[item.advertiser_id].children[item.account_id].children[item.campaign_id].children;
                            }

                            // ALERTS
                            if (_treeMT && item.alert_level == 'account+MT+Device Level') {
                                _treeMT[item.device] = buildAlertItem(item, 'Device', devices.normalize(item.device))
                            }
                            else if (_treeCampaigns) {
                                if (item.alert_level == 'Campaign+MT Level') {
                                    _treeCampaigns[item.match_type_four] = buildAlertItem(item, 'Match Type', item.match_type_four)
                                }
                                else if (item.alert_level == 'Campaign+Device Level') {
                                    _treeCampaigns[item.device] = buildAlertItem(item, 'Device', devices.normalize(item.device))
                                }
                                else if (item.alert_level == 'Ad_group Level') {
                                    _treeCampaigns[item.ad_group_id] = buildAlertItem(item, 'Ad Group', item.ad_gourp_name)
                                }

                                // NODES
                                else if (item.alert_level == 'Campaign+MT+Device Level') {
                                    if (!_treeCampaigns[item.match_type_four]) {
                                        _treeCampaigns[item.match_type_four] = buildNodeItem('Match Type', item.ad_gourp_name);
                                    }
                                } else if (item.alert_level == 'Campaign+MT+Device Level') {
                                    if (!_treeCampaigns[item.match_type_four]) {
                                        _treeCampaigns[item.match_type_four] = buildNodeItem('Match Type', item.ad_gourp_name);
                                    }
                                }
                                else if (item.alert_level == 'Ad_group+MT Level') {
                                    if (!_treeCampaigns[item.ad_group_id]) {
                                        _treeCampaigns[item.ad_group_id] = buildNodeItem('Ad Group', item.ad_gourp_name);
                                    }
                                } else if (item.alert_level == 'Ad_group+Device Level') {
                                    if (!_treeCampaigns[item.ad_group_id]) {
                                        _treeCampaigns[item.ad_group_id] = buildNodeItem('Ad Group', item.ad_gourp_name);
                                    }
                                } else if (item.alert_level == 'Ad_group+MT+Device Level') {
                                    if (!_treeCampaigns[item.ad_group_id]) {
                                        _treeCampaigns[item.ad_group_id] = buildNodeItem('Ad Group', item.ad_gourp_name);
                                    }
                                }
                                else if (item.alert_level == 'Keyword Level') {
                                    if (!_treeCampaigns[item.ad_group_id]) {
                                        _treeCampaigns[item.ad_group_id] = buildNodeItem('Ad Group', item.ad_gourp_name);
                                    }
                                }
                            }
                        });

                        //level 5
                        /* ALERTS -> Campaign+MT+Device | Ad_group+MT | Ad_group+Device -> */
                        $.map(_tree.records, function (item) {


                            if (isAccountLevel(item)) {
                                return;
                            }

                            var _treeAdGroup = {};
                            var _treeMT = {};
                            if (item.match_type_four !== '' && item.match_type_four !== '0' && isCampaignLevel(item)) {
                              _treeMT = tree[item.advertiser_id].children[item.account_id].children[item.campaign_id].children[item.match_type_four].children;
                            }
                            if (item.campaign_id !== '' && item.campaign_id !== '0' && !isCampaignLevel(item) && Object.keys(_treeMT).length) {
                                _treeAdGroup = tree[item.advertiser_id].children[item.account_id].children[item.campaign_id].children[item.ad_group_id].children;
                            }

                            // Alerts
                            if (_treeMT && item.alert_level == 'Campaign+MT+Device Level') {
                                _treeMT[item.device] = buildAlertItem(item, 'Device', devices.normalize(item.device))
                            }
                            else if (_treeAdGroup) {
                                if (item.alert_level == 'Ad_group+MT Level') {
                                    _treeAdGroup[item.match_type_four] = buildAlertItem(item, 'Match Type', item.match_type_four)
                                }
                                else if (item.alert_level == 'Ad_group+Device Level') {
                                    _treeAdGroup[item.device] = buildAlertItem(item, 'Device', devices.normalize(item.device))
                                }

                                // NODES
                                else if (item.alert_level == 'Ad_group+MT+Device Level') {
                                    if (!_treeAdGroup[item.match_type_four]) {
                                        _treeAdGroup[item.match_type_four] = buildNodeItem('Match Type', item.match_type_four);
                                    }
                                }
                                else if (item.alert_level == 'Keyword Level') {
                                    if (!_treeAdGroup[item.match_type_four]) {
                                        _treeAdGroup[item.match_type_four] = buildNodeItem('Match Type', item.match_type_four)
                                    }
                                }
                            }
                        });

                        //level 6
                        /* ALERTS -> Ad_group+MT+Device */
                        $.map(_tree.records, function (item) {
                            var _treeMT;
                            if (isCampaignLevel(item) || isAccountLevel(item)) {

                                return;
                            }

                            if (item.match_type_four !== '' && item.match_type_four !== '0') {
                                _treeMT = matchNestedChildrenLeafByProp(tree[item.advertiser_id].children[item.account_id].children, [item.campaign_id, item.ad_group_id, item.match_type_four])
                                // _treeMT =
                                //     tree[item.advertiser_id].children[item.account_id].children[item.campaign_id].children[item.ad_group_id].children[item.match_type_four].children;
                            }

                            // ALERTS
                            if (item.alert_level == 'Ad_group+MT+Device Level') {
                                _treeMT[item.device] = buildAlertItem(item, 'Device', devices.normalize(item.device))
                            }

                            /*NODES*/
                            else if (item.alert_level == 'Keyword Level') {
                                if (!_treeMT[item.match_type_four]) {
                                    _treeMT[item.device] = buildNodeItem('Device', devices.normalize(item.device));
                                }
                            }
                        });

                        //level 7
                        //ALERTS -> Keyword
                        $.map(_tree.records, function (item) {
                            if (isCampaignLevel(item) || isAccountLevel(item)) {

                                return;
                            }

                            var _treeDevice;

                            if (item.match_type_four !== '' && item.match_type_four !== '0' && item.device !== '' && item.device !== '0') {
                                _treeMT = matchNestedChildrenLeafByProp(tree[item.advertiser_id].children[item.account_id].children, [item.campaign_id, item.ad_group_id, item.match_type_four, item.device])
                                // _treeDevice = tree[item.advertiser_id].children[item.account_id].children[item.campaign_id].children[item.ad_group_id].children[item.match_type_four].children[item.device].children;
                            }

                            // Alerts
                            if (item.alert_level == 'Keyword Level') {
                                _treeDevice[item.keyword_id] = buildAlertItem(item, 'Keyword', item.keywords);
                            }
                        });

                        /**
                         *
                         * Build alert item with id
                         *
                         * @param item
                         * @param type
                         * @param name
                         * @returns {*}
                         */
                        function buildAlertItem(item, type, name) {
                            return $.extend({}, item, {
                                type: type,
                                name: name,
                                children: {}
                            });
                        }

                        /**
                         *
                         * Build Path to the alert Item
                         *
                         * @param type
                         * @param name
                         * @returns {{type: *, name: *, children: {}}}
                         */
                        function buildNodeItem(type, name) {
                            return {
                                type: type,
                                name: name,
                                children: {}
                            }
                        }

                      /**
                       *
                       * @param struct
                       * @param Array nestedPropsArr
                       */
                        function matchNestedChildrenLeafByProp(struct, nestedPropsArr = []) {
                            if (!Object.keys(struct).length)
                                return {}
                            return nestedPropsArr.reduce(function (obj, curProp) {
                              if (obj == undefined)
                                return obj;

                              if (obj[curProp]) {
                                return obj[curProp].children;
                              }

                              return undefined;
                            }, {...struct}) || {}
                        }

                        /**
                         *  Check if alert in campaign level
                         * @param alert
                         * @returns {boolean}
                         */
                        function isCampaignLevel(alert) {
                            var itemLevel = alert.alert_level;

                            return itemLevel == 'Campaign+Device Level'
                                || itemLevel == 'Campaign+MT+Device Level'
                                || itemLevel == 'Campaign+MT Level'
                                || itemLevel == 'Campaign Level';
                        }

                        /**
                         * Check if alert in account level
                         *
                         * @param alert
                         * @returns {boolean}
                         */
                        function isAccountLevel(alert) {
                            var alertLevel = alert.alert_level;

                            return alertLevel == 'account Level'
                                || alertLevel == 'account+MT Level'
                                || alertLevel == 'account+MT+Device Level'
                                || alertLevel == 'account+Device Level'
                        }

                        return tree;
                    }

                    /**
                     * @description build flat view
                     */
                    function buildFlat(sortBy, direction) {
                        var tree = {};
                        var records = _tree.records;
                        var advertiserAccountGroups = [];
                        var counter = 0;
                        var sortByDefault = sortBy.match(/name/g) ? 'account_name' : 'advertiser_name';
                        var direction = direction ? direction : 'asc';


                        //first sorting for global list (advertiser_name or other associative ordering
                        records = _tree.sortTree.resolveByField(sortByDefault, records, direction);

                        //build tree after global sorting (sort the inner list according to the sortBy argument)
                        tree = buildAccordingToSort(sortBy, direction);

                        var arrayTree = [];
                        for (var key in tree) {
                            arrayTree.push(tree[key]);
                        }

                        return arrayTree;

                        /**
                         * @description build factory (according to target sorting)
                         *
                         * @param sortBy
                         * @returns {{}}
                         */
                        function buildAccordingToSort(sortBy, direction) {

                            //basic sorting ( sort outer by advertiser_name and inner sort by profit value desc)
                            if (sortBy === 'default') {
                                return buildFlatWithDefaultSorting();
                            }

                            //custom sorting according to the select box sortBy
                            return buildFlatTreeWithCustomSorting(sortBy, direction);

                            /**
                             * @description build flat with default Sorting (by group name + by
                             * profit value inside)
                             *
                             * @returns {{}}
                             */
                            function buildFlatWithDefaultSorting() {

                                var sortBy = 'profit';

                                /**
                                 *  @description Level 1 - advertiser + account
                                 */
                                $.map(records, function (item) {
                                    var advertiserAccountGroup = item.advertiser_id + item.account_id;

                                    if (advertiserAccountGroups.indexOf(advertiserAccountGroup) === -1) {
                                        tree[counter] = _tree.buildGroupItem('Advertiser+account', item.advertiser_name + '-' + item.account_name, item.advertiser_id, item.account_id);
                                        advertiserAccountGroups.push(advertiserAccountGroup);
                                        counter++;
                                    }
                                });


                                /**
                                 * @description  Level 2 - campaign + ad-group + match-type + device + keyword + alert_name + profit

                                 */
                                $.map(records, function (item) {
                                    var advertiserIdRecords = item.advertiser_id;
                                    var accountIdRecords = item.account_id;

                                    $.map(tree, function (treeItem) {
                                        var advertiserIdTree = treeItem.advertiser_id;
                                        var accountIdTree = treeItem.account_id;

                                        if (advertiserIdTree === advertiserIdRecords && accountIdRecords === accountIdTree) {
                                            treeItem.children.push(item);
                                        }
                                    });
                                });

                                /**
                                 * @description Sort inner group by default value
                                 */
                                $.map(tree, function (item) {
                                    item.children = _tree.sortTree.resolveByField(sortBy, item.children);
                                });

                                return tree;
                            }

                          /**
                           * @description Build with custom field sorting
                           *
                           *
                           * @returns {{}}
                           * @param sortBy
                           * @param direction
                           */
                            function buildFlatTreeWithCustomSorting(sortBy, direction) {

                            var sorted = _tree.sortTree.resolveByField(sortBy, records, direction);

                                var tree = {},
                                    lastAdvertiserID,
                                    lastAccountID,
                                    counter = 0;

                                //sort by custom sorting (inner and outer
                                // + show multiple groups if they doesn't on after another
                                sorted.map(function (item) {
                                    var groupName = item.advertiser_name + '-' + item.account_name;
                                    var index = Object.keys(tree).length;
                                    var firstIteration = counter === 0;

                                    //first key (so there's no point check if there's previous advertiser)
                                    if (isSameParent(lastAdvertiserID, lastAccountID, item) && !firstIteration) {
                                        tree[index - 1].children.push(item);
                                        lastAdvertiserID = item.advertiser_id;
                                        lastAccountID = item.account_id;

                                        //the advertiser+account is the same
                                    } else {
                                        tree[index] = _tree.buildGroupItem('Advertiser+account', groupName, item.advertiser_id, item.account_id);
                                        tree[index].children.push(item);
                                        lastAdvertiserID = item.advertiser_id;
                                        lastAccountID = item.account_id;
                                    }
                                    counter++;

                                    /**
                                     *
                                     *
                                     * @param lastAdvertiserID
                                     * @param lastAccountID
                                     * @param item
                                     * @returns {boolean}
                                     */
                                    function isSameParent(lastAdvertiserID, lastAccountID, item) {

                                        return lastAdvertiserID == item.advertiser_id && lastAccountID == item.account_id
                                    }
                                });

                                //sort each child
                                $.map(tree, function (item) {
                                    item.children = _tree.sortTree.resolveByField(sortBy, item.children, direction);
                                });

                                return tree;
                            }
                        }
                    }

                    /**
                     * @description build marked
                     * @param sortBy
                     */
                    function buildMarked(sortBy) {
                        tree = buildFlat(sortBy);
                        var accountID = 0,
                            advertiserID = 0,
                            counter = 0,
                            newTree = {};

                        var markedAlerts = getMarked(tree);

                        markedAlerts.map(function (group) {

                            group.children.map(function (alert) {
                                var firstIteration = counter === 0;
                                var index = Object.keys(newTree).length;
                                var groupName = alert.advertiser_name + '-' + alert.account_name;

                                if (alert.advertiser_id == advertiserID && alert.account_id == accountID && !firstIteration) {
                                    newTree[index - 1].children.push(alert);
                                    accountID = alert.account_id;
                                    advertiserID = alert.advertiser_id;
                                }
                                else {
                                    newTree[index] = _tree.buildGroupItem('Advertiser+account', groupName, alert.advertiser_id, alert.account_id);
                                    newTree[index].children.push(alert);
                                    accountID = alert.account_id;
                                    advertiserID = alert.advertiser_id;
                                }
                                counter++;
                            })
                        });

                        if ($.isEmptyObject(newTree)) {
                            return false;
                        }

                        return newTree;


                        /**
                         *
                         * @param tree
                         */
                        function getMarked(tree) {

                            //filter the flat tree
                            var markedTree = tree.filter(function (group) {
                                var markedItemCookie = helpers.getCookie('marked');
                                var markedItems = markedItemCookie ? markedItemCookie.split(',') : [];
                                markedItems = markedItems.map(function (id) {
                                    return parseInt(id);
                                });

                                var hasMarkedItems = group.children.findIndex(isMarkedItemExist) != -1;

                                if (hasMarkedItems) {

                                    group.children = group.children.filter(isMarkedItemExist);

                                    return true;
                                }

                                return false;

                                /**
                                 *
                                 * @param item
                                 * @returns {boolean}
                                 */
                                function isMarkedItemExist(item) {

                                    return markedItems.indexOf(item.id) != -1;
                                }
                            });

                            return markedTree;
                        }
                    }
                },

                /**
                 * @description build group for every alert ( ordered by advertiser id )
                 *
                 * @param type
                 * @param name
                 * @param id
                 * @returns {{advertiser_id: *, name: *, type: *, children: Array}}
                 */
                buildGroupItem: function (type, name, advertiser_id, account_id) {
                    return {
                        advertiser_id: advertiser_id,
                        account_id: account_id,
                        name: name,
                        type: type,
                        children: []
                    };
                },

                /**
                 *
                 * Resolve dynamic filters
                 *
                 * @returns {{}}
                 */
                resolveFilters: function () {
                    var filters = $(selectors.filters);
                    var where = {};

                    //resolve where filters
                    filters.each(function (name, filter) {
                        var group = $(filter).data('filter');
                        var value = $(filter).val();
                        var column = group + "_id";

                        //if value no empty or not null ( type of object);
                        if (value != '' && typeof value != 'object') {
                            where[column] = value;
                        }
                    });

                    if (Object.keys(where).length) {

                        return where;
                    }
                },

                /**
                 *
                 * refresh filters
                 *
                 * @param filters
                 * @param view
                 */
                refreshTree: function (filters, view) {
                    "use strict";

                    /**
                     *
                     * @param view
                     * @returns {*}
                     */
                    function resolveCallbackByView(view) {
                        switch (view) {

                            case 'flat':

                                return appendFlatTree;
                                break;

                            case 'marked':

                                return appendMarkedTree;
                                break;

                            default:

                                return appendHierarchyTree;
                        }
                    }

                    var callback = resolveCallbackByView(view);
                    App.blockUI({iconOnly: true, target: $(selectors.wrapper)});

                    setTimeout(function () {
                        treeBuilder.set(getTreeObject(filters), jQuery(selectors.body), callback);
                        App.unblockUI($(selectors.wrapper));
                    }, 1000);

                    /**
                     *
                     * @param filters
                     * @returns {{}|*}
                     */
                    function getTreeObject(filters) {

                        if (filters) {
                            if (filters.advertiser_id && filters.account_id && filters.campaign_id) {

                                return _tree.data[filters.advertiser_id].children[filters.account_id].children[filters.campaign_id].children;
                            }
                            else if (filters.advertiser_id && filters.account_id) {

                                return _tree.data[filters.advertiser_id].children[filters.account_id].children;
                            }
                            else if (filters.advertiser_id) {

                                return _tree.data[filters.advertiser_id].children;
                            }
                        }

                        return _tree.data;
                    }

                    /**
                     *
                     * @param item
                     * @returns {{itemClass: string, innerText: string}}
                     */
                    function appendHierarchyTree(item) {
                        var isAlert = (typeof item.id !== 'undefined');

                        var alertIcon = isAlert ? '<span class="fa fa-bell-o"></span>' : '';

                        return {
                            itemClass: isAlert ? 'alert-item' : '',
                            innerText:
                            '<div class="alert-item-wrapper" data-id="' + item.id + '">' +
                            '<span class="font-grey-salt" data-alert_type="' + item.alert_level + '">' +
                            item.type +
                            ':</span> ' +
                            item.name +
                            '</div>' +
                            alertIcon
                        }
                    }

                    /**
                     *
                     * @param item
                     */
                    function appendMarkedTree(item) {

                        return appendFlatTree(item);
                    }

                    /**
                     *
                     * @param item
                     * @returns {{itemClass: string, innerText: string}}
                     */
                    function appendFlatTree(item) {
                        var isAlert = (typeof item.id !== 'undefined');
                        var text = '';
                        var markedAlerts = helpers.getCookie('marked');
                        var markedAlerts = markedAlerts ? markedAlerts.split(',') : [];

                        if (isAlert) {
                            var alert_id_state = $.inArray(String(item.id), markedAlerts) != -1;
                            text = "<span class='fa fa-star-o marked'></span>";

                            if (alert_id_state) {
                                text = "<span class='fa fa-star favorite marked'></span>";
                            }

                            text += '<span class="font-grey-salt alert-level-wrapper" data-alert_level="' + item.alert_level + '">';
                            text += resolvePath(item) + '</span> ';
                        } else {
                            text = '<span class="font-grey-salt" ></span> ' + item.name;
                        }

                        return {
                            itemClass: isAlert ? 'alert-item flatOrMarked' : '',
                            innerText:
                            '<div class="alert-item-wrapper" data-id="' + item.id + '">' +
                            text + '</div>'
                        };

                        function resolvePath(item) {
                            var baseLevel;
                            var icons;
                            var variables = resolveVariables(item);
                            var accountLevel = '';

                            //In case it account level concat account name to it;
                            if (item.alert_level.match(/account/g)) {
                                accountLevel = variables.levels.account + ' ';
                            }

                            baseLevel =
                                "<span class='base-levels'>" +
                                "<span class='tokens'>"
                                + accountLevel
                                + variables.levels.campaign
                                + variables.levels.adGroup
                                + variables.helpers.separator.space
                                + variables.helpers.separator.pipeline
                                + "</span> " +
                                "<span class='values'>" +
                                +variables.levels.profit
                                + variables.helpers.separator.comma
                                + variables.levels.alertDays
                                + "</span> </span>";

                            icons = "<span class='icons'>" +
                                variables.icons.mt +
                                variables.helpers.separator.space +
                                variables.icons.device +
                                variables.helpers.separator.space +
                                variables.icons.keyword +
                                variables.helpers.separator.space +
                                variables.icons.alert +
                                "</span>";

                            return baseLevel + icons;


                            function resolveVariables(item) {
                                var mtClassColor = item.match_type_four ? resolveColorClassByParam('mt', item.match_type_four) : '',
                                    alertTypeClassColor = item.alert_type ? resolveColorClassByParam('alert_type', item.alert_type) : '',
                                    deviceTypeClassColor = item.device ? resolveColorClassByParam('device', devices.normalize(item.device)) : '',
                                    alertSign = alertTypeClassColor == 'green' ? 'up' : 'down';

                                var mtIcon = item.match_type_four && item.match_type_four != '0'
                                    ? '<button'
                                    + ' class="match-type btn btn-xs tooltipster ' + mtClassColor + '" '
                                    + ' data-attr="' + item.match_type_four + '" '
                                    + ' data-tooltip-content="#tooltip-match-type">'
                                    + getMatchTypeAcronym(item.match_type_four)
                                    + '</button>'
                                    + '<span id="tooltip-match-type" class="hidden">'
                                    + item.match_type_four
                                    + '</span>'
                                    : '',

                                    deviceIcon = item.device && item.device != '0'
                                        ? '<button class="device btn btn-xs ' + deviceTypeClassColor + '">' +
                                        '<i class="device fa fa-' + devices.normalize(item.device) + '"></i></button>'
                                        : '',

                                    keywordIcon = item.keywords != '0' && item.keywords
                                        ? '<button class="keyword btn btn-xs purple-plum" data-attr="' + item.keywords + '">KW</button>'
                                        : '',

                                    alertIcon = '<button class="alert_type btn btn-xs ' + alertTypeClassColor + '">' +
                                        '<i class="fa fa-arrow-' + alertSign + '"></i></button>',
                                    space = " ",
                                    pipeline = ' | ',
                                    profit = item.profit ? Math.round(parseInt(item.profit)) : '',
                                    alertDays = item.alert_days ? item.alert_days : '',
                                    accountName = item.account_name && item.account_name != '0' ? item.account_name : '',
                                    campaignName = item.campaign_name && item.campaign_name != '0' ? item.campaign_name : '',
                                  adGroupName = item.ad_gourp_name && item.ad_gourp_name != '0' ? pipeline + item.ad_gourp_name : '',
                                  token = item.token && item.token != '0' ? `${pipeline}${item.token}` : '';

                                return {
                                    icons: {
                                        alert: alertIcon,
                                        mt: mtIcon,
                                        device: deviceIcon,
                                        keyword: keywordIcon
                                    },
                                    levels: {
                                        profit: profit,
                                        alertDays: alertDays,
                                        account: accountName,
                                        campaign: campaignName,
                                      adGroup: adGroupName,
                                      token: token
                                    },
                                    helpers: {
                                        separator: {
                                            space: space,
                                            pipeline: pipeline,
                                            comma: ', '
                                        }
                                    }
                                };

                                /**
                                 *
                                 * @param type
                                 * @returns {*}
                                 */
                                function getMatchTypeAcronym(type) {
                                    var typeToAcronym = {
                                        'bmm': 'BM',
                                        'broad': 'BR',
                                        'exact': 'EX',
                                        'phrase': 'PH'
                                    };

                                    return typeToAcronym[type];
                                }
                            }

                            /**
                             * resolve
                             *
                             * @param type
                             * @param param
                             * @returns {*}
                             */
                            function resolveColorClassByParam(type, param) {
                                var typeToClass = {
                                    'device': {
                                        'desktop': 'bg-blue',
                                        'mobile': 'bg-purple-studio',
                                        'tablet': 'bg-yellow-casablanca',
                                        'other': 'bg-grey-mint'
                                    },
                                    'mt': {
                                        'exact': 'dark',
                                        'phrase': 'yellow-saffron',
                                        'bmm': 'red-pink',
                                        'broad': 'yellow'
                                    },
                                    'alert_type': {'High_CTS': 'green', 'Low_CTS': 'red'}
                                };

                                var options = typeToClass[type];

                                return options[param];
                            }

                        }
                    }
                },

                /**
                 * Move to next item
                 */
                nextItem: function () {
                    "use strict";

                    var allItems = _tree.getAllAlerts();

                    var currentIdx = _tree.findActiveIndex(allItems);

                    if (currentIdx === (allItems.length - 1)) {
                        allItems[0].click();
                    } else {
                        allItems[++currentIdx].click();
                    }
                },

                /**
                 * Move to previous item
                 */
                prevItem: function () {
                    "use strict";

                    var allItems = _tree.getAllAlerts();

                    var currentIdx = _tree.findActiveIndex(allItems);

                    if (currentIdx === 0) {
                        allItems[allItems.length - 1].click();
                    } else {
                        allItems[--currentIdx].click();
                    }
                },

                /**
                 * Find active index
                 *
                 * @param allItems
                 * @returns {boolean}
                 */
                findActiveIndex: function (allItems) {
                    "use strict";

                    var currentIdx = false;
                    $.each(allItems, function (idx, item) {
                        if ($(item).closest('li').hasClass('active')) {
                            currentIdx = idx;

                            return false;
                        }
                    });

                    return currentIdx;
                },

                /**
                 *
                 * Get all alerts
                 *
                 * @returns {*|jQuery|HTMLElement}
                 */
                getAllAlerts: function () {
                    "use strict";

                    return $(selectors.body + ' li > .alert-item');
                },

                /**
                 *
                 * get item
                 *
                 * @param attributes
                 * @returns {*|jQuery|HTMLElement}
                 */
                item: function (attributes) {
                    var li = $('<li>', {
                        class: 'dd-item' + attributes['level'],
                        'item-id': attributes['record'].id
                    });
                    var buttonCollapse = $('<button>', {class: 'collapse'});
                    var buttonExpend = $('<button>', {class: 'expend'});
                    var handle = $('<div>', {class: 'dd-handle'});
                    li.append(buttonCollapse, buttonExpend, handle);

                    return li
                },

                /**
                 *
                 *
                 * @returns {*|jQuery|HTMLElement}
                 */
                wrapper: function () {

                    return $('<ol>', {class: 'dd-list'});
                },

                /**
                 *
                 * @param attributes
                 * @returns {*}
                 */
                list: function (attributes) {

                    return this.wrapper().append(this.item(attributes))
                },


                append: function () {
                    if (!treeUpdateBuffer) {
                        clearTimeout(treeUpdateBuffer);
                    }

                    var view = _tree.getViewType();

                    treeUpdateBuffer = setTimeout(function () {
                        if (view === 'hierarchy') {
                            var activeFilters = _tree.resolveFilters();
                        }

                        _tree.refreshTree(activeFilters, view);
                    }, 500);
                },

                /**
                 *
                 * @param elem
                 */
                toggleMarked: function (elem) {
                    $(elem).toggleClass('fa-star favorite').toggleClass('fa-star-o');
                },

                /**
                 * save
                 */
                saveMarkedAlerts: function () {

                    _tree.marked = [];

                    $(selectors.marked).each(function (key, item) {

                        var $target = $(item);
                        var alert = $target.closest('.alert-item-wrapper');
                        var alert_id = alert.attr('data-id');

                        if ($.inArray(alert_id, _tree.marked) === -1) {
                            _tree.marked.push(alert_id);
                        }
                    });

                    helpers.setCookie('marked', _tree.marked.join(), 30);
                },

                sortTree: {
                    resolveByField: function (type, records, direction) {
                      if (type.match(/name/g) || type == 'token') {
                            return getAssociativeOrder(type, records, direction);
                        }

                        return getNumericOrder(type, records, direction);

                        /**
                         *
                         * @param field
                         * @param records
                         * @param direction
                         * @returns {*|records|{}}
                         */
                        function getAssociativeOrder(field, records, direction) {
                            records = records.sort(function (a, b) {
                              var recordA = a[field].toLowerCase();
                              var recordB = b[field].toLowerCase();

                                    if (direction) {
                                        if (direction === 'asc') {
                                          if (recordA < recordB) return -1;
                                          if (recordA > recordB) return 1;
                                          if (recordA === recordB) {
                                            sortIfChosenRecordIsTheSame(a, b, sortHirrarchy[sortHirrarchy.indexOf(field) + 1]);
                                            }
                                        }
                                    }
                                    else {
                                      if (recordA < recordB) return 1;
                                      if (recordA > recordB) return -1;
                                      if (recordA === recordB) {
                                        sortIfChosenRecordIsTheSame(a, b, sortHirrachy[sortHirrachy.indexOf(field) + 1]);
                                        }
                                    }
                                }
                            );

                            /**
                             *
                             * @param a
                             * @param b
                             * @param field
                             * @returns {number}
                             */
                            function sortIfChosenRecordIsTheSame(a, b, field) {
                              let accountA = a[field].toLowerCase();
                              let accountB = b[field].toLowerCase();

                              if (accountA > accountB) return 1;
                              else if (accountA < accountB) return -1;
                              return 0;
                            }


                          return records;
                        }

                        /**
                         *
                         * @param param
                         * @param record
                         * @param direction
                         * @returns {*}
                         */
                        function getNumericOrder(param, record, direction) {
                            record.sort(function (a, b) {

                                //order desc by default
                                if (direction && direction === 'asc') {
                                    return b[param] - a[param];
                                }

                                return a[param] - b[param];
                            });

                            return record;
                        }
                    }
                }
            }
        ;

        return _tree;
    })
;