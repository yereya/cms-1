define('dynamicFilters', ['jquery', 'dynamicAlerts', 'dynamicTree']
    , function ($) {

        var selectors = {
                alertTypes: {
                    class: '.alert_types',
                    sum: '.alert_types .sum',
                    length: '.alert_types .length'
                },
                devices: {
                    class: '.devices',
                    sum: '.devices .sum',
                    length: '.devices .length'
                },
            },
            devices = {
                names: {
                    'c': 'desktop',
                    'm': 'mobile',
                    't': 'tablet',
                    '': 'other',
                    '0': 'other',
                    'NA': 'other'
                },
                translate: function (device) {
                    var name = undefined;

                    $.each(devices.names, function (key, value) {
                        if (device === value) {
                            name = value;
                        }
                    });

                    return name;
                },

                normalize: function (key) {
                    var name = undefined;

                    $.each(devices.names, function (column, fullName) {
                        if (column == key) {
                            name = fullName
                        }
                    });

                    return name;
                }
            },
            alertTypes = {
                names: {
                    'Low_CTS': 'lowCTS',
                    'High_CTS': 'highCTS'
                },
                translate: function (alertType) {
                    var name = undefined;

                    $.each(alertTypes.names, function (key, value) {
                        if (alertType === value) {
                            name = value;
                        }
                    });

                    return name;
                }
            },
            stats = {
                alertTypes: {
                    highCTS: {
                        cost: 0,
                        length: 0
                    },
                    lowCTS: {
                        cost: 0,
                        length: 0
                    }
                },
                devices: {
                    desktop: {
                        cost: 0,
                        length: 0
                    },
                    mobile: {
                        cost: 0,
                        length: 0
                    },
                    tablet: {
                        cost: 0,
                        length: 0
                    },
                    other: {
                        cost: 0,
                        length: 0
                    }
                }
            };

        //Filters part
        var filters = {

            // Holds the filtered records
            // Used to display the final results
            filtered: [],

            /**
             * @description Init filter process
             *
             * @param target
             * @returns {*}
             */
            init: function (target) {

                if (target) {
                    target.toggleClass('active');
                }

                var filters = this.getActiveFilters();

                return this.update(filters['alertTypeFilters'], filters['deviceFilters']);
            },

            /**
             * @description Updates the filters
             *
             * @param alertTypeFilters
             * @param deviceFilters
             * @returns {Array}
             */
            update: function (alertTypeFilters, deviceFilters) {
                filters.filtered = filters.records;

                if (alertTypeFilters.length > 0 || deviceFilters.length > 0) {
                    filters.filterRecords(alertTypeFilters, deviceFilters);
                }

                this.updateFilterTabsStats(alertTypeFilters);

                this.appendFilterTabsStats();

                return filters.filtered;
            },

            /**
             * @description Will generate the records according to the passed filters
             *
             * @param alertTypeFilters
             * @param deviceFilters
             */
            filterRecords: function (alertTypeFilters, deviceFilters) {
                // Clear the old filtered content
                filters.filtered = [];

                $.each(filters.records, function (idx, record) {
                    var alertTypeName = alertTypes.names[record['alert_type']];
                    var deviceName = devices.names[record['device']] || 'NA';

                    if (alertTypeFilters.length > 0 && deviceFilters.length > 0) {
                        if (alertTypeFilters.indexOf(alertTypeName) !== -1
                            && deviceFilters.indexOf(deviceName) !== -1) {

                            filters.filtered.push(record);
                        }
                    }
                    else if (alertTypeFilters.length > 0 && alertTypeFilters.indexOf(alertTypeName) !== -1) {
                        filters.filtered.push(record);
                    }
                    else if (deviceFilters.length > 0 && deviceFilters.indexOf(deviceName) !== -1) {
                        filters.filtered.push(record);
                    }
                });
            },

            /**
             * @description clear alert stats
             */
            clearAlertStats: function () {

                // TODO write a recursive function instead of this SHIIT method
                stats = {
                    alertTypes: {
                        highCTS: {
                            cost: 0,
                            length: 0
                        },
                        lowCTS: {
                            cost: 0,
                            length: 0
                        }
                    },
                    devices: {
                        'desktop': {
                            cost: 0,
                            length: 0
                        },
                        'mobile': {
                            cost: 0,
                            length: 0
                        },
                        'tablet': {
                            cost: 0,
                            length: 0
                        },
                        'other': {
                            cost: 0,
                            length: 0
                        }
                    }
                };
            },

            /**
             * @description update Dynamic Filters
             *
             * @param alertTypeFilters
             */
            updateFilterTabsStats: function (alertTypeFilters) {

                this.clearAlertStats();

                $.each(filters.records, function (idx, record) {

                    // concat alert type stats
                    var alertTypeName = alertTypes.names[record['alert_type']];
                    var alertState = stats.alertTypes[alertTypeName];
                    if (isNaN(alertState.cost))
                        alertState.cost = 0;
                    alertState.cost += parseFloat(record.cost);
                    alertState.length++;

                    // concat device stats
                    if (hasAlertTypes()
                        && alertTypeFilters.indexOf(alertTypes.names[record['alert_type']]) !== -1
                        || !hasAlertTypes()) {

                        var deviceName = devices.names[record['device']];
                        var deviceState = stats.devices[deviceName];
                        deviceState.cost += parseFloat(record.cost);
                        deviceState.length++;
                    }
                });

                function hasAlertTypes() {
                    return alertTypeFilters.length > 0;
                }
            },

            /**
             *  @description Append filters to dom
             */
            appendFilterTabsStats: function () {
                $.each(stats.alertTypes, function (name, alertTypeStats) {
                    var tab = $('[data-filter=' + name + "]");
                    appendToDom(tab, alertTypeStats);
                });
                $.each(stats.devices, function (name, deviceStats) {
                    var tab = $('[data-filter=' + name + "]");
                    appendToDom(tab, deviceStats);
                });

                function appendToDom(element, stats) {
                    element.find('.length').html(stats.length);
                    element.find('.sum').html(getCost(stats));
                }

                function getCost(record) {
                    return Math.round(record.cost).toLocaleString('USD');
                }
            },

            /**
             *
             * @description Get active filters
             *
             * @returns {{alertTypeFilters: *, deviceFilters: *}}
             */
            getActiveFilters: function () {
                var alertTypeFilters = $.map($(selectors.alertTypes.class + ' .active'), function (el) {
                    return $(el).data('filter');
                });

                var deviceFilters = $.map($(selectors.devices.class + ' .active'), function (el) {
                    return $(el).data('filter');
                });

                return {
                    alertTypeFilters: alertTypeFilters,
                    deviceFilters: deviceFilters
                }
            }
        };

        return filters;

    });
