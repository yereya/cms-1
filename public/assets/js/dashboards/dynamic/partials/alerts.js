/**
 * Dynamic alerts module (In use with dashboard dynamic)
 */
define('dynamicAlerts', ['jquery'],
    function () {
            var alerts = {
                records: {},
                length: 0,
                sum: 0
            };

        return alerts;
    })
;
