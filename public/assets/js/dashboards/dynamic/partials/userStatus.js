define('userStatus', ['jquery'], function ($) {

    var selectors =  {
            status : '.note',
            name:'[name=user_name]',
            resolved:'[name=resolved]'
        }
        var userStatus = {
            data: {
                name: 'john doe',
                alertsNumber: 0,
                handledAlerts: 0,
                ratio: 0,
            },
            setUserName:function() {
                userStatus.data.name =  $(selectors.name).val();
            },
            setAlertsNumber: function (alerts) {
                userStatus.data.alertsNumber = alerts.records.length;
            },
            setResolvedAlerts: function (alerts) {
                userStatus.data.handledAlerts =  alerts.resolved;
            },
            setAlertToResolvedRatio: function () {
                var alerts = userStatus.data.alertsNumber;
                var handled = userStatus.data.handledAlerts;

                console.log(alerts,handled);
                userStatus.data.ratio =   parseInt(handled)/ parseInt(alerts);
            }
        }
        var build = function (alerts) {
            var callBacks = {
                'name': userStatus.setUserName,
                'alertsNumber': userStatus.setAlertsNumber,
                'handledAlerts': userStatus.setResolvedAlerts,
                'ratio': userStatus.setAlertToResolvedRatio
            };
            $.each(userStatus.data, function (k) {
                userStatus[k] = callBacks[k](alerts);
            });

            set();
        }
        var set = function () {
            var data = userStatus.data
            console.log(data);
            var options = {'length': data.alertsNumber, 'fixed': data.handledAlerts, 'percentage': data.ratio };

            $.each(options, function (subject, record) {
                $(selectors.status + ' .' + subject).html(record);
            });
        }

        function init(alerts) {
            build(alerts);
            set();
        }

        return {
            userStatus:userStatus,
            init:function (alerts) {
                return init(alerts);
            }
        }
});
