define('dynamicStates', ['jquery', 'dynamicHelpers'], function($, helpers) {

  //Holds the records stats
  var selectors = {
      alertRecords: {
        wrapper: '.alert-records',
        topContent: {
          wrapper: '.top-content-stats',
          breadcrumb: '.breadcrumbs-list',
          overview: '.breadcrumbs-list'
        },
        stats: '.stats',
        breadcrumb: '.breadcrumbs-list',
        overview: '.overview-wrapper'
      },
      listTree: {
        wrapper: '#treeWrapper',
        body: '#treeWrapper .portlet-body',
        filters: '.filters select',
        type: '#treeWrapper .listType-wrapper button'
      },
      form: {
        alertStats: '.alert-state-form',
        api: '.widget-stats-form'
      },
      itemWrapper: '.alert-item-wrapper',
      multiSelectBox: {
        wrapper: '.alert-selection',
        selectBox: '.multi'
      }
    },
    devices = {
      names: {
        'c': 'desktop',
        'm': 'mobile',
        't': 'tablet',
        '': 'other',
        'NA': 'other'
      },
      translate: function(device) {
        var name = undefined;

        $.each(devices.names, function(key, value) {
          if (device === value) {
            name = value;
          }
        });

        return name;
      },

      normalize: function(key) {
        var name = undefined;

        $.each(devices.names, function(column, fullName) {
          if (column == key) {
            name = fullName;
          }
        });

        return name;
      }
    },

    alertTypes = {
      names: {
        'Low CTS': 'lowCTS',
        'High CTS': 'highCTS'
      },
      translate: function(alertType) {
        var name = undefined;

        $.each(alertTypes.names, function(key, value) {
          if (alertType === value) {
            name = value;
          }
        });

        return name;
      }
    },

    formatToField = {
      'currency': ['cpc', 'cpa', 'cpa_history', 'profit', 'profit_history', 'cost'],
      'percent': ['cts', 'cts_history', 'site_ctr']
    };

  var states = {
    records: {},
    init: function(event) {
      var target = $(event.target);
      if (!target.hasClass('marked')) {
        var wrapper = target.closest('.dd-item');
        var id = wrapper.find(selectors.itemWrapper).attr('data-id');
        var record = helpers.filterBy(states.records, {'id': id});
        var multiSelectState = states.isMultiSelectionChecked();

        // Update to the new active item
        helpers.activateTreeItem(wrapper, selectors.listTree.body, multiSelectState);

        //update input hidden with the dynamic_id field
        states.handleDynamicIds(id, multiSelectState, target);

        //append to the dom
        states.handleAppending(record, multiSelectState);

        //show alerts
        $(selectors.alertRecords.topContent.wrapper).removeClass('collapse');
      }
    },

    clearAll: function() {
      'use strict';

      console.log('all cleared');

      var statsPortlet = $(selectors.alertRecords.stats);
      statsPortlet.find('.alert-level').html('');
      statsPortlet.find('.alert-type').html('');
      statsPortlet.find('input[name=dynamic_id], input[name=description],' +
        ' input[name=records_id], input[name=record_id]').val('');
      $(selectors.alertRecords.breadcrumb + ' li span.name').html('');
      $(selectors.alertRecords.wrapper + ' .current').html('-');
      $(selectors.alertRecords.wrapper + ' .history').html('-');
    },

    append: function(records, reset) {
      var record = records[0];
      var alertRecordsTable = $(selectors.alertRecords.wrapper);
      var alertStats = $(selectors.alertRecords.stats);
      var alertFieldToSelector = {
        alert_level: alertStats.find('.alert-level'),
        alert_type: alertStats.find('.alert-type')
      };
      var format;
      var alertRecord;

      var statToSelector = {
        cpc: alertRecordsTable.find('.cpc'),
        cpa: alertRecordsTable.find('.cpa'),
        cts: alertRecordsTable.find('.cts'),
        profit: alertRecordsTable.find('.profit'),
        site_ctr: alertRecordsTable.find('.site_ctr')
      };

      // append alert info
      $.each(alertFieldToSelector, function(alertField, selector) {
        var alertRecord = record[alertField];

        selector.html(alertRecord);

        if (alertField == 'alert_type') {
          selector.html('|' + alertRecord);
        }
      });

      // append alert stats
      $.each(statToSelector, function(stat, selector) {

        if (reset) {
          selector.find('.current').html('-');
          selector.find('.history').html('-');

          return true;
        }
        try {

          alertRecord = states.formatField(record, stat + '_history');

          var current = states.formatField(record[stat], stat);
          var history = states.formatField(record[stat + '_history'], stat);

          var zeroValue = record[stat] == 0 || record[stat + '_history'] == 0;
          var ratio = zeroValue ? 'NA' : (record[stat] / record[stat + '_history']) - 1;
          ratio = ' (' + (ratio != 'NA' ? parseFloat(ratio).toFixed(2) + '%' : ratio) + ')';

          selector.find('.current').html(current + ratio);
          selector.find('.history').html(history);
        } catch (e) {
          console.log(e);
        }
      });

      this.buildBreadCrumbs(record);
      this.buildOverview(record);

    },
    handleAppending: function(record, multiSelectionState) {
      if (!multiSelectionState) {
        return states.append(record);
      }
    },
    handleDynamicIds: function(id, multiSelectState, target) {
      let ids;
      let $records_id;
      let $records_value;

      if (!multiSelectState) {
        return $(selectors.form.alertStats).find('[name=record_id]').val(id);
      }
      $records_id = $(selectors.form.alertStats).find('[name="records_id"]');
      $records_value = $records_id.val();
      ids = $records_value
        ? $records_id.val().split(',')
        : [];

      if (ids.indexOf(id) === -1) {
        ids.push(id);
      }
      else {
        ids.splice(ids.indexOf(id), 1);
      }
      $records_id.val(ids);
    },
    save: function(e) {

      return new Promise(function(resolve, reject) {
        var $target = $(e.target);
        var $form = $($target.closest('form'));
        var formParams = $form.serializeArray();
        formParams.push({name: 'state', value: $target.val()});
        var multiSelectionState = states.isMultiSelectionChecked();
        var emptyFields = isFilled(formParams, multiSelectionState);

        if (emptyFields.length === 0) {
          var settings = {
            url: $form.attr('action'),
            method: $form.attr('method'),
            data: formParams,
            beforeSend: function() {
              App.blockUI({iconOnly: true, target: $(selectors.alertRecords.stats)});
            },
            complete: function(response) {

              App.unblockUI($(selectors.alertRecords.stats));
              //reload proccess
              if (response.responseJSON.status === 200) {

                resolve('alert record was saved successfully');
                toastr.success('alert record was saved successfully');

                states.clearAll();
                hideOverView();

                function hideOverView() {
                  $('.top-content-stats').addClass('collapse');
                }

              } else {

                // Parse errors  to the toaster
                var string = '';

                $.each(response.responseJSON, function(key, val) {

                  $.each(val, function(k, v) {
                    string += '<br>' + v;
                  });
                });

                toastr.error('please fill all required fields: ' + string);
              }
            }
          };
          $.ajax(settings);
        } else {
          toastr.error('you must fill all required fields');
        }
      });

      /**
       * @description check field filling
       * @param params
       * @returns {boolean}
       */
      function isFilled(params, multiSelection) {

        if (!multiSelection) {
          return $.grep(params, function(param) {
            return param.value === '' && (param.name == 'description' || param.name == 'record_id');
          });
        }
        return $.grep(params, function(param) {
          return param.value === '' && (param.name == 'description' || param.name == 'records_id');
        });
      }

      function prepareRecordsId() {
        let $recordsIdTag = $(selectors.form.alertStats).find('[name="records_id"]');
        let ids = $recordsIdTag.val();
        $recordsIdTag.val(`[${ids}]`);
      };
    },

    flagAlert: function(target) {
      target.addClass('active');
    },

    buildBreadCrumbs: function(record) {
      //advertiser_name | account_name | campaign_name | ad_group_name

      var alertRecordsWrapper = $(selectors.alertRecords.breadcrumb);

      var statsToSelector = {
        advertiser_name: alertRecordsWrapper.find('.advertiser .name'),
        account_name: alertRecordsWrapper.find('.account .name'),
        campaign_name: alertRecordsWrapper.find('.campaign .name'),
        ad_gourp_name: alertRecordsWrapper.find('.ad_group .name'),
        match_type_four: alertRecordsWrapper.find('.mt .name'),
        device: alertRecordsWrapper.find('.device .name'),
        keywords: alertRecordsWrapper.find('.keyword .name')
      };

      $.each(statsToSelector, function(key, param) {
        if (record[key] != '' && record[key] != 'NA') {
          try{
            param.html(record[key].toLowerCase());

            if (key == 'device') {
              param.html(devices.normalize(record[key]));
            }
            param.closest('li').removeClass('hidden');
          }catch (e) {
            console.log('Error in state - ' + e);
          }
        }
        else {
          param.closest('li').addClass('hidden');
        }
      });

      alertRecordsWrapper.find('ul').fadeIn();
    },
    buildOverview: function(record) {
      var overViewWrapper = $(selectors.alertRecords.overview);
      var fieldToSelector = {
        profit: '.profit',
        cost: '.cost',
        creation_date: '.creation_date',
        alert_days: '.alert_days',
        site_ctr: '.site_ctr'
      };

      $.each(fieldToSelector, function(field, selector) {
        var recordField = record[field];

        if (field != 'creation_date' && field != 'alert_days') {
          recordField = states.formatField(recordField, field);
        }

        overViewWrapper.find(selector + ' .amount').text(recordField);
      });
    },
    /**
     *
     * resolve Format Per Field
     *
     * @param field
     */
    resolveFormat: function(field) {
      var currentFormat;

      $.each(formatToField, function(k, format) {

        if (format.indexOf(field) != -1) {
          currentFormat = k;
        }
      });

      return currentFormat;
    },

    /**
     *
     *
     * @param recordField
     * @param format
     * @returns {*}
     */
    formatField: function(recordField, field) {
      var format = states.resolveFormat(field);

      if (format == 'currency') {
        recordField = parseFloat(recordField).toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,');
        recordField = '$' + recordField;
      } else {
        recordField = recordField * 100;
        recordField = recordField.toFixed(2);
        recordField = recordField + '%';
      }

      return recordField;
    },
    isMultiSelectionChecked() {
      return $(selectors.multiSelectBox.wrapper).find('.active').hasClass('multi');
    }
  };

  return states;
});
