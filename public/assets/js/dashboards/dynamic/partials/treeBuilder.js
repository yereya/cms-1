/**
 * Dynamic alerts module (In use with dashboard dynamic)
 */
define('treeBuilder', ['jquery','nestable2'], function ($) {
    "use strict";

    var defaultConfigs = {
        maxDepth: 10,
        noDragClass: 'dd-nodrag',
        collapsedClass: 'dd-collapsed'
    };

    // Activates nestable package
    // uses the passed configs
    function activateNestable(container, configs) {
        var _configs = $.extend({}, defaultConfigs, configs);

        container.nestable(_configs);
        container.nestable('init');
    }

    function buildNesting(tree, container, callback) {
        var newContainer = $('<ol class="dd-list"></ol>');

        $.each(tree, function (idx, item) {
            var result = callback(item);

            var newListItem =
                $('<li class="dd-item" data-id="' + idx + '">' +
                    '<div class="dd-handle dd-nodrag ' + result.itemClass + '">' +
                    result.innerText +
                    '</div></li>');

            newContainer.append(newListItem);

            if (item.children) {
                buildNesting(item.children, newListItem, callback);
            }
        });

        container.append(newContainer);
    }


    return {
        set: function (theTree, container, callback, nestableConfigs) {
            container.nestable('destroy');
            container.html('');

            if (!theTree) {
                return;
            }
            buildNesting(theTree, container, callback);
            activateNestable(container, nestableConfigs || {});
        }
    };
});
