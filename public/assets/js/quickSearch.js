/**
 * Detect typing on the search input.
 *
 * @return void
 * */
define('quickSearch', ['jquery'], function ($) {

    this.onKeyUp = function () {
        /**
         * The current search input.
         *
         * @ver object
         **/
        var input = $(this);

        /**
         * The empty state selector.
         * @ver object
         **/
        var empty_state_selector = $(input.data('empty-state'));

        /**
         * Collection of the search in elements.
         *
         * @ver object
         **/
        var search_in_selector = $(input.data('search-in'));

        /**
         * List of search terms.
         *
         * @ver array
         **/
        var search_terms = input.val().toLowerCase().split(' ');


        // Build empty state functionality
        empty_state_selector.find("ul li").click(function (){
            input.val($(this).text());
            input.trigger('keyup');
        });
        if (search_terms.length === 0){
            return search_in_selector.show();
        }

        // Perform search
        search_in_selector.each(function () {
            var matchs = 0;
            var inner_text = $(this).text().toLowerCase();

            search_terms.forEach(function (term) {
                if (inner_text.indexOf(term) !== -1) {
                    matchs++;
                }
            });

            if (matchs > 0) {

                return $(this).show();
            }

            $(this).hide();
        });

        // Display or hide the empty state.
        var visible_elements_count = $(input.data('search-in') + ':visible').length;
        if (visible_elements_count === 0){
            empty_state_selector.removeClass('hide');
        }else{
            empty_state_selector.addClass('hide');
        }

        if (visible_elements_count === 1){
            $(input.data('remove-class-on-single-result')).removeClass(input.data('class-to-remove'));
        }else{
            $(input.data('remove-class-on-single-result')).addClass(input.data('class-to-remove'));
        }

    };

    $(".quickSearch").keyup(this.onKeyUp);
});