Vue.component('loader', {
    props: ['visible'],
    template: '<div class="text-center" v-if="visible"> <img src="/assets/global/img/loader.svg" /> </div>'
});