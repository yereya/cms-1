/**
 * Require JS CMS
 */
requirejs.config({
    baseUrl: "/assets/",
    paths: {
        // Libraries
        "jquery": "libs/jquery-3.1.1.min",
      'select2': 'global/plugins/select2/js/select2.min',
        "validator": "libs/jquery-validation-1.16/jquery.validate.min",
        "jqueryWithCustomValidation": "js/jqueryWithCustomValidation",
        "jqueryWithValidator": "libs/jquery-validation-1.16/additional-methods",
        'blockUi': 'libs/jquery.blockui.min' ,
        'chartJs': 'libs/chart.js/dist/Chart',
        'generalHelpers': 'js/generalHelpers',
        'chartStats': 'js/dashboards/stats/chart',
        'summaryStats': 'js/dashboards/stats/summary',
        'tableStats': 'js/dashboards/stats/table',
        'chartConfiguration':'js/dashboards/stats/chartConfiguration',
        'quickSearch': 'js/quickSearch',
        'flagStats':'js/dashboards/stats/flag',
        'nestable2':'global/plugins/nestable2/jquery.nestable',
        'dynamicDashboard':'js/dashboards/dynamic/dynamic',
        'dynamicAlerts':'js/dashboards/dynamic/partials/alerts',
        'userStatus': 'js/dashboards/dynamic/partials/userStatus',
        'treeBuilder': 'js/dashboards/dynamic/partials/treeBuilder',
        'dynamicFilters': 'js/dashboards/dynamic/partials/filters',
        'dynamicTree': 'js/dashboards/dynamic/partials/tree',
        'dynamicStates': 'js/dashboards/dynamic/partials/states',
        'dynamicHelpers': 'js/dashboards/dynamic/partials/helpers',
        'trackingSystem': 'js/dashboards/tracking_system/trackingSystem',
        'ppcMorningRoutine':'js/dashboards/morning-routine-ppc/ppcMorningRoutine',
        'pageSpeed':'js/dashboards/page-speed/page-speed',
    },
    shim: {
        'chartJs': {
            deps: ['jquery', 'blockUi'],
            expose: ['Chart']
        },
        'treeBuilder': {
            deps: ['jquery', 'nestable2'],
            expose: ['treeBuilder']
        }
    }
});

/**
 * check if the jquery object is already declared
 * if it declared return it
 * if not load it from the requireJs module
 */
if (typeof jQuery === 'function') {
    define('jquery', function () {
        return jQuery;
    });
}

var selectors_to_module = {
    '#chart' : 'chartStats',
    '.dash-stats'  : 'summaryStats',
    '.table-stats' : 'tableStats',
    '.quickSearch' : 'quickSearch',
    '.dynamic-wrapper' : 'dynamicDashboard',
    '.tracking-system' : 'trackingSystem',
    '.ppc-morning-routine-wrapper': 'ppcMorningRoutine',
    '.page-speed':'pageSpeed'
};

var mainJs = (function () {
    return {
        init: function ($) {
            "use strict";
            $.each(selectors_to_module, function (selector, lib) {
                if ($(selector).length) {
                    requirejs([lib]);
                }
            });
        }
    }
})();


requirejs(['jquery'], function ($) {
    mainJs.init($);
});