var postRevisions = new Vue({
    el: '#post_revisions',
    data: {
        base_url: null,
        timeline: null,
        revisions: null,
        selected_revision: null,
        previous_revision: null,
        loaders: {
            timeline: true,
            previous: false
        },
        form: {
            start_date: window.cms.start_date,
            end_date: window.cms.end_date
        }
    },
    /**
     * Run when app is mounted.
     *
     * @returns {void}
     **/
    mounted: function () {
        // Build the base ajax url.
        this.base_url = window.location.origin + '/sites/' + window.cms.site.id + '/posts/' + window.cms.post.id + '/revisions';
        // Fetch revisions time-line points.
        this.getRevisions();
    },
    methods: {
        /**
         * Print an attribute of the current post
         * from the original post object or his content type.
         *
         * @param {string} key
         *
         * @returns {string}
         **/
        printFromPostByKey: function (key) {
            var pipes = [
                window.cms.post,
                window.cms.post.content
            ];

            for (var i in pipes) {
                var pipe = pipes[i];

                if (typeof pipe[key] !== 'undefined') {
                    return pipe[key];
                }
            }

            return 'Attribute was not found in the current post object.';
        },
        /**
         * Render the diff time-line.
         *
         * @param {array} items
         *
         * @returns {void}
         **/
        renderTimeline: function (items) {
            var container = document.getElementById('visualization');
            container.innerHTML = '';
            this.timeline = new vis.Timeline(container, new vis.DataSet(items), {});
            this.timeline.on('click', this.onTimelineItemClicked.bind(this));
            this.loaders.timeline = false;
        },
        /**
         * Get the revisions gor a given period.
         *
         * @returns void
         **/
        getRevisions: function () {
            // Show loader
            this.loaders.timeline = true;

            // Reset vars
            this.revisions = null;
            this.previous_revision = null;
            this.selected_revision = null;

            // Get json data from server.
            jQuery.get(this.base_url + '/json', this.form, function (revisions) {

                var map = {};

                // Group by date into map.
                revisions.forEach(function (revision) {
                    if (!map[revision.created_at]) {
                        map[revision.created_at] = [];
                    }
                    map[revision.created_at].push(revision);
                });

                // Clear data
                revisions = [];

                // Push new grouped revisions
                for (var key in map) {
                    revisions.push({id: key, revisions: map[key]});
                }

                this.revisions = revisions;

                var items = revisions.map(function (item) {
                    return {id: item.id, content: item.id, start: new Date(item.id)}
                }.bind(this));

                this.renderTimeline(items);

            }.bind(this));
        },
        /**
         * Callback for the click event of the diff time-line.
         *
         * @param {object} event
         *
         * @returns {void}
         **/
        onTimelineItemClicked: function (event) {
            this.revisions.forEach(function (revision) {
                if (revision.id == event.item) {
                    this.selected_revision = revision.revisions;
                    return this.loadPreviousRevision();
                }
            }.bind(this));
        },
        /**
         * Load the previous revisions for each of field in the selected revision.
         *
         * @returns {void}
         **/
        loadPreviousRevision: function () {
            var promises = [];
            this.loaders.previous = true;
            this.previous_revision = null;
            for (var i in this.selected_revision) {
                var selected = this.selected_revision[i];
                promises.push(this.resolvePreviousRevision(selected));
            }
            Promise.all(promises).then(function (previous_revisions) {
                this.previous_revision = {};
                previous_revisions.forEach(function (revision) {
                    this.previous_revision[revision.key] = revision;
                }.bind(this));
            }.bind(this)).catch(function (error) {
                alert('Whoops!');
            }).then(function () {
                this.loaders.previous = false;
            }.bind(this));
        },
        /**
         * Resolve the previous revision for a given revision field
         *
         * @param {object} selected
         *
         * @returns {void}
         **/
        resolvePreviousRevision: function (selected) {
            return new Promise(function (resolve) {
                jQuery.get(this.base_url + '/' + selected.key + '/before/' + selected.created_at, function (response) {
                    resolve(response);
                }.bind(this));
            }.bind(this));
        },
        /**
         * Use a given revision.
         *
         * @param {string} key
         * @param {object} revision
         *
         * @returns {void}
         **/
        useRevision: function (key, revision) {
            swal({
                title: "Are you sure?",
                text: "This action will revert the post to the selected revision field.",
                type: 'warning',
                buttonsStyling: false,
                showCancelButton: true,
                cancelButtonColor: '#d33',
                cancelButtonText: 'Cancel',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Yes, Do it!',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonClass: 'btn btn-success'
            }).then(function () {
                var update_args = {};
                update_args[revision.key] = revision.new_value;

                jQuery.post(this.base_url, {
                    update_args: update_args,
                    _token: window.cms.csrf_token,
                    revisionable_type: revision.revisionable_type
                }, function (response) {
                    if (!response.msg) {
                        return App.sweetAlert("Whoops!", "Invalid server response.", "error");
                    }
                    App.sweetAlert("👍🏻 Done!", response.msg, "success");
                }).fail(function () {
                    App.sweetAlert("Whoops!", 'No changes were made.', "warning");
                });
            }.bind(this), function (dismiss) {
                //...
            });
        },
        /**
         * Generate a diff output.
         *
         * @param {object} revision
         *
         * @returns {string}
         **/
        diff: function (revision) {
            try {
                // In case of a null value, convert into an empty string.
                revision.old_value = revision.old_value || '';
                revision.new_value = revision.new_value || '';

                var diff = JsDiff.diffChars(revision.old_value, revision.new_value);
                var output = document.createElement('div');
                diff.forEach(function (part) {
                    var color = part.added ? 'added' :
                        part.removed ? 'removed' : 'no-change animate';
                    var span = document.createElement('span');
                    span.classList = [color];
                    span.innerText = part.value;
                    output.append(span);
                });

                return output.outerHTML;
            } catch (exception) {
                console.warn('diff.exception',exception);

                return 'Diff exception has occurred.';
            }
        }
    }
});