/**
 * Code mirror module
 *
 * 1. textAreaToCodeMirror - transform text areas to code mirror element
 * 2. codeMirrorToTextArea - transform codemirror to text area
 *
 */
define('codeMirror', ['codeMirrorLib'], function (codeMirror) {
    var selectors = {
        textAreas: '.codemirror',
    };

    var textAreaWatchers = [];

    /**
     *  @description Initialize Code Mirror
     */
    function init() {
        textAreaToCodeMirror();
        initEvents();
    }

    /**
     *
     */
    function initEvents() {

    }

    /**
     * @description Transfrom textArea to code-mirror
     */
    function textAreaToCodeMirror() {
        var codeMirrorTextAreas = $(selectors.textAreas);

        /*
               Is code mirror Textarea exists
               */
        function isCodeMirrorExists() {
            return codeMirrorTextAreas.length > 0 && typeof codeMirror == 'function';
        }

        if (isCodeMirrorExists()) {
            codeMirrorTextAreas.each(function (index, textArea) {
                var language = $(textArea).data('codemirror_lang');
                language = resolveLanguage(language);

                var cm = codeMirror.fromTextArea(textArea, {
                    mode: language,
                    indentUnit: 4,
                    tabSize: 4,
                    lineNumbers: true,
                    foldGutter: {
                        rangeFinder: new codeMirror.fold.combine(codeMirror.fold.brace, codeMirror.fold.comment)
                    },
                    gutters: ["CodeMirror-foldgutter", 'CodeMirror-foldgutter']
                });

                if (window.codeMirrorEditors) {
                    window.codeMirrorEditors.push(cm);
                } else {
                    window.codeMirrorEditors = [cm];
                }

                var column = $(textArea).data('codemirror_column_wrap');

                if (typeof column != 'undefined') {
                    //Cause of some unknown bug we need run it twice - fix me if you have time
                    cm.wrapParagraphsInRange(cm.posFromIndex(0), cm.posFromIndex(cm.posFromIndex(cm.getViewport.to)), {column: column});
                    cm.wrapParagraphsInRange(cm.posFromIndex(0), cm.posFromIndex(cm.posFromIndex(cm.getViewport.to)), {column: column});
                }

                textAreaWatchers[index] = cm;

                /**
                 *
                 * resolve codeMirror language
                 *
                 * @param lang
                 * @returns {*}
                 */
                function resolveLanguage(lang) {
                    if (lang == 'html') {
                        lang = 'htmlmix';
                    } else if (lang == 'scss') {
                        lang = 'sass';
                    } else if (lang == 'blade') {
                        lang = 'php';
                    }

                    return lang;
                }
            });
        }
    }

    /**
     * @description transform code mirror to textArea
     */
    function codeMirrorToTextArea() {
        if (textAreaWatchers.length > 0) {
            textAreaWatchers.each(function (key, codeMirror) {
                codeMirror.toTextArea();
            });
        }
    }

    init();

    return {
        init: function () {
            init();
        },
        killCodeMirror: function () {
            codeMirrorToTextArea();
        }
    }
});

