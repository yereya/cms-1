/**
 *
 * Html validator module in usage with edit blade
 *
 *  - it will validate the html and check if there's some mistypes according to innerHtml compiled version.
 *  - it will validate the scss file - try to compile the scss if it fail it throw the scss error.
 *
 */
define('htmlValidator', ['jquery', 'jsDiff', 'generalHelpers']/**
     *
     */
    , function ($, jsDiff, helpers) {

        var selectors = {
            txtAreaBlade: 'textarea[name="blade"], textarea[name="custom-blade"]',
            txtAreaScss: 'textarea[name="scss"], textarea[name="custom-scss"]',
            textarea: 'textarea[data-validate]',
            submit: 'button[type="submit"]',
            diff: '.blade-diff',
            bladeDiffWrapper: '.diff-blade-wrapper',
            scssDiffWrapper: '.diff-scss-wrapper',
            replaceTextButton: '.replaceText.btn.btn-info',
            ignoreButton: '.ignoreValidation.btn.btn-warning',
            scssValidationForm: '.scss-validation-form',
            allowCloseModal: '[name="close_modal"]',
            valid: '[name="valid"]'

        }

        var conditions = {
            isCodeMirrorExist: $('.codemirror').length,
            isThereBladeTextArea: $(selectors.txtAreaBlade).length > 0,
            isThereScssTextArea: $(selectors.txtAreaScss).length > 0,
            ignoreValidation: false,
        }
        var characterToReplace = {
            '<!--{{': '</{{',
            '}}-->': '}}>',
            '{!! html': '{!! HTML',
            '=""': '',
            '"{{"': '{{',
            '!--': '/'
        };

        /**
         * Initialise
         */
        function init() {
            initEvents();
        }

        /**
         * Init All Events
         */
        function initEvents() {
            $(selectors.submit).on('click', resolveValidation);
        }

        /**
         * Resolve validation after submit
         *
         *
         * @param event
         */
        /**
         *
         * @param event
         */
        function resolveValidation(event) {
            event.preventDefault();

            var isModal = $(this).parent().is('[class*=modal]')
            var $button = $(this);
            var $form = resolveForm(isModal, $button);
            var scssFormExistence = isScssFormExist($form);
            var scssTextAreaExistence = isScssTextAreaExist(isModal);
            var bladeTextAreaExistence = isBladeTextAreaExist(isModal);

            /*If validation Ignored Submit The form*/
            if (conditions.ignoreValidation) {
                isModal ? $form.find('button').click() : $form.submit();

                return;
            }

            /*If there's union validation */
            if (scssTextAreaExistence && bladeTextAreaExistence) {

                /*Make validation blade and scss*/
                Promise
                    .all([validateScss($form), validateBlade()])
                    .then(function (message) {
                        isModal ? $form.find('button').click() : $form.submit();
                    })
                    .catch(function (message) {
                        console.log(message);
                    });
            }

            //Only blade Validation
            else {
                var validationProcess = validateBlade;
                if (scssFormExistence) {
                    validationProcess = validateScss;
                }

                /*Choose the validation needed dynamically*/
                validationProcess($form)
                    .then(function (message) {
                        isModal ? $form.find('button').click() : $form.submit();
                    })
                    .catch(function (message) {
                        console.log(message);
                    });
            }
        };


        /*
        *
        * Activate validation ignore
        *
        * */
        function submitWithoutBladeValidation() {
            conditions.ignoreValidation = true;
            toastr.warning('validation will be ignored');
        }

        /**
         * Toggle Difference tab
         *
         * @param action
         */
        function toggleDiffTab(action) {
            var action = action + 'Class';
            $(selectors.bladeDiffWrapper)[action]('hidden');
        }

        /**
         * Set Validation To your choice
         *
         * @param int
         */
        function setValidation(int) {
            $(selectors.allowCloseModal).val(int);
            $(selectors.valid).val(int);
        }

        /**
         *
         * Normalise String
         *
         *  - decode html entities
         *  - change custom characters to its right syntax (blade issues)
         *
         * @param compiled
         * @returns {*}
         */
        function normaliseString(compiled) {
            compiled = helpers.decodeHtmlEntity(compiled);
            compiled = helpers.replaceStringCharacters(compiled, characterToReplace);

            return compiled;
        }


        /**
         *
         * Validate blade
         *
         * 1 - get compiled and precompiled
         * 2 - compare between them
         * - if there difference :
         *    - print the diff between them
         *  else:
         * 4. validate pass
         *
         * @return {Promise}
         *
         */
        function validateBlade($form) {

            return new Promise(function (resolve, reject) {
                var $textArea = $(selectors.textarea);
                var preCompiled = $textArea.val();
                /*IF codemirror library exists*/
                if (conditions.isCodeMirrorExist) {
                    var textArea = resolveCodeMirrorByName($textArea.attr('name'));
                    $textArea = $(textArea.getTextArea());
                    preCompiled = textArea.getValue();
                }

                /*Compiled*/
                var compiled = $('<div>').html(preCompiled).html();
                compiled = normaliseString(compiled);


                /*Check if compiled version is the same as the text version*/
                if (compiled != preCompiled) {

                    /*Resolve difference between versions*/
                    var diff = resolveDiff(preCompiled, compiled);
                    $(selectors.diff).html(diff);

                    /*Display difference tab*/
                    toggleDiffTab('remove');

                    /*Show Button Replace With compiled*/
                    appendReplaceButton();
                    appendIgnoreButton();
                    setValidation(0);
                    reject('blade validation failed');

                } else {

                    /*Hide difference tab*/
                    toggleDiffTab('add');
                    setValidation(1);
                    resolve('blade validation succeeded');
                }

                /**
                 * Add Dynamic Buttons
                 *
                 * @param props
                 */
                function addButtonDynamically(props) {
                    var buttonExist = $(selectors.bladeDiffWrapper).find(props['class']);

                    if (!buttonExist.length) {
                        var button = $('<a>',
                            {
                                href: '#',
                                class: props['class'].replace(/\./g, ' '),
                                text: props['text']
                            });

                        $(selectors.bladeDiffWrapper).append(button);

                        button.on('click', props['callback']);
                    }
                }

                /*Append Ignore Button To Dom*/
                function appendIgnoreButton() {
                    var props = {
                        class: 'ignoreButton',
                        text:'Ignore Validation',
                        callback: submitWithoutBladeValidation
                    }

                    addButtonDynamically(props);
                }

                /*
                    Append Replace Button
                 */
                function appendReplaceButton() {
                    var props = {
                        name: 'replaceTextButton',
                        text:'Replace To Compiled Version',
                        callback: replaceContent
                    }

                    addButtonDynamically(props);
                }
            })
        }

        /**
         *
         * Replace To Compiled Content
         *
         * @param e
         */
        function replaceContent(event) {
            event.preventDefault();

            var $textArea = $(selectors.textarea);
            var preCompiled = $textArea.val();
            var compiled = $('<div>').html(preCompiled).html();
            compiled = normaliseString(compiled);
            $textArea.val(compiled);

            if (conditions.isCodeMirrorExist) {
                var textAreaName = $(selectors.textarea).attr('name');
                var codeMirror = resolveCodeMirrorByName(textAreaName);

                preCompiled = codeMirror.getValue();
                compiled = $('<div>').html(preCompiled).html();
                compiled = normaliseString(compiled);

                codeMirror.setValue(compiled);
            }
        }

        /**
         * Get Text Area By name property
         *
         * @param name
         * @return {boolean}
         */
        function resolveCodeMirrorByName(name) {
            var codeMirror = false;

            /*Iterate over all textarea's and resolve the correct one*/
            $.each(codeMirrorEditors, function (key, _codeMirror) {
                var codeMirrorTextArea = _codeMirror.getTextArea();
                var isCorrectTextArea = $(codeMirrorTextArea).attr('name') == name;

                if (isCorrectTextArea) {
                    codeMirror = _codeMirror;

                    return false;
                }
            });

            return codeMirror;
        }

        /**
         *
         * Resolve Difference Of Two Strings
         *
         * @param preCompiled
         * @param compiled
         */
        function resolveDiff(preCompiled, compiled) {
            var diff = jsDiff.diffChars(preCompiled, compiled, {ignoreWhitespace: true, ignoreCase: true});
            var output = document.createElement('div');

            diff.forEach(function (part) {
                var color = part.added ? 'added'
                    : part.removed ? 'removed' : 'no-change animate';
                var span = document.createElement('span');
                span.classList = [color];
                span.innerText = part.value;
                output.append(span);

            });

            return output.outerHTML;
        }

        /**
         * Validate scss file
         *
         * @description validate scss files
         *
         * - make ajax request with the scss you want to validate
         * - in the back end try to compile this sass after save it
         * - if there's some error roll back the changes
         * - throw the error to the user with the appropriate message.
         *
         * @param $form
         * @return {Promise}
         */
        function validateScss($form) {
            return new Promise(function (resolve, reject) {

                /* Append respond message into the DOM */
                this.resolve = function (message, codeMirrorWrapper) {
                    var scssDiffWrapper = $(selectors.scssDiffWrapper);

                    /*If there's error message it */
                    if (!message.status) {
                        setValidation(0);
                        scssDiffWrapper.find('.diff-scss').html(message.desc);
                        scssDiffWrapper.removeClass('hidden');
                        reject('error been accord: ' + message.desc);
                    } else {

                        /*Close modal set to true*/
                        setValidation(1);
                        scssDiffWrapper.addClass('hidden');

                        resolve('scss validation success!');
                    }

                    App.unblockUI(codeMirrorWrapper);
                };

                /* Make ajax request with the scss file */
                this.ajax = function () {
                    var that = this;

                    /*If there's codeMirror library */
                    if (conditions.isCodeMirrorExist) {

                        var textAreaName = $form.find(selectors.txtAreaScss).attr('name');
                        var codeMirrorScss = resolveCodeMirrorByName(textAreaName);
                        var codeMirrorWrapper = $(codeMirrorScss.getTextArea()).closest('div');
                    }

                    var formParams = $form.serializeArray();

                    formParams.map(function (param) {

                        if (param.name === 'scss' && conditions.isCodeMirrorExist) {
                            param.value = codeMirrorScss.getValue();
                        }
                        if (param.name === '_method') {
                            param.value = 'POST';
                        }

                        return param;
                    })

                    /**
                     *
                     * @type {{method: *, url: *, data, beforeSend: beforeSend, complete: complete}}
                     */
                    var setting = {
                        method: 'POST',
                        url: $form.attr('data-url_validation'),
                        data: formParams,
                        beforeSend: function () {
                            App.blockUI({target: codeMirrorWrapper});
                        },
                        complete: function (response) {
                            var message = response.responseText
                            if (helpers.isJson(response.responseText)) {
                                message = response.responseJSON;
                            }
                            that.resolve(message, codeMirrorWrapper);
                        },
                    }


                    $.ajax(setting);
                };

                this.ajax();
            });
        }

        /**
         * Is Scss Text Area Exist
         *
         * @param isModal
         * @returns {*}
         */
        function isScssTextAreaExist(isModal) {
            return isModal
                ? $(this).closest('.modal-content').find(selectors.txtAreaScss).length > 0
                : conditions.isThereScssTextArea;
        }

        /**
         * Is Blade Text Area Exist
         *
         * @param isModal
         * @returns {*}
         */
        function isBladeTextAreaExist(isModal) {
            return isModal
                ? $(this).closest('.modal-content').find(selectors.txtAreaBlade).length > 0
                : conditions.isThereBladeTextArea;
        }

        /**
         *
         * @param $form
         */
        function isScssFormExist($form) {
            $form.find(selectors.txtAreaScss).length > 0
        }

        /**
         *
         * @param isModal
         * @returns {*|jQuery}
         */
        function resolveForm(isModal, $button) {
            return isModal ? $button.closest('.modal-content').find('form') : $button.closest('form');
        }


        init();

        return {
            init: function () {
                init();
            }
        }
    });