/**
 * Require JS Template Builder
 */
requirejs.config({
    baseUrl: "/top-assets/",
    paths: {
        // Libraries
        "jquery": "libs/jquery-3.1.1.min",
        "validator": "libs/jquery-validation-1.16/jquery.validate.min",
        "jquery_with_validator": "libs/jquery-validation-1.16/additional-methods",
        "jquery_with_custom_validation": "js/jqueryWithCustomValidation",
        "bootstrap": "libs/bootstrap/javascripts/bootstrap",
        "slick": "libs/slick/slick",
        "tooltipster": "libs/tooltipster/js/tooltipster.bundle",
        'carousel': 'js/carousel',
        'comparisonTable': 'js/comparisonTable',
        'toolTips': 'js/toolTipster',
        'topScroller': 'js/scrollTop',
        'bookmark': 'js/bookmark',
        "rating": "js/rating",
        'generalHelpers': 'js/generalHelpers',
        'scrollIntoView': 'js/scrollIntoView',
        'sidr': 'libs/sidr/dist/jquery.sidr',
        'mobileSideBar': 'js/mobileSidebar',
        'blockUi': 'libs/jquery.blockui.min',
        'likes': 'js/likes',
        'quickSearch': 'js/quickSearch',

        //todo add later html validation
        // 'htmlValidator': 'js/htmlValidator',
        // 'jsDiff': 'libs/jsDiff/diff.min'
    }
});
/**
 * check if the jquery object is already declared
 * if it declared return it
 * if not load it from the requireJs module
 */
if (typeof jQuery === 'function') {
    define('jquery', function () {
        return jQuery;
    });
}
var selectors_to_module = {
    '.likes,.dislikes': 'likes',
    '.quickSearch': 'quickSearch'
};

requirejs(['topScroller', 'bookmark']);

var mainJs = (function () {
    return {
        init: function ($) {
            "use strict";
            $.each(selectors_to_module, function (selector, lib) {
                if ($(selector).length) {
                    requirejs([lib]);
                }
            });
        }
    }
})();


requirejs(['jquery'], function ($) {
    mainJs.init($);
});