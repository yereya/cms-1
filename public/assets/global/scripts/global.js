$(function ($) {
    "use strict";

    //var toastrMsgs = [{"success":"User wer wer added"}];
    //Toastr auto messages
    if (App.toastrMsgs) {
        $.each(App.toastrMsgs, function (idx, val) {
            if (val.length == 0) return true;

            var msgTheme = 'info';
            if (idx == 'error' || idx == 'success' || idx == 'warning') {
                msgTheme = idx;
            }
            toastr[msgTheme](val);
        });
    }


    //Open new tab window
    if (App.openNewTabUrl) {
        App.openNewTab(App.openNewTabUrl);
    }

    // Open new tab window
    if (App.openNewModalUrl) {
        setTimeout(function() {
            App.openAjaxModal(App.openNewModalUrl.join());
        }, 1000)
    }


    //Collapse or expend sections
    //adds or removes the "collapsed" class
    $('section .action-collapse, section .section-header').click(function (e) {
        e.stopPropagation();
        $(this).closest('section').toggleClass('collapsed');
    });


    //Adds tabbing functionality according to the uri hashtag
    location.hash && $('.nav-tabs a[href="' + location.hash + '_tab"][data-toggle="tab"]').click();
    $(window).on('hashchange', function () {
        $('.nav-tabs a[href="' + location.hash + '_tab"][data-toggle="tab"]').click();
    });

    if (!String.format) {
        String.format = function (format) {
            var args = Array.prototype.slice.call(arguments, 1);
            return format.replace(/{(\d+)}/g, function (match, number) {
                return typeof args[number] != 'undefined'
                    ? args[number]
                    : match
                    ;
            });
        };
    }

});


//Will try to parse the json string into an object
function parseToJason(json) {
    if (json) {
        try {
            return JSON.parse(json);
        } catch (e) {
            console.log('Error while parseing: ', e);
            return undefined;
        }
    }
    else {
        console.log('Error while parseing: ', 'empty string passed');
        return undefined;
    }
}