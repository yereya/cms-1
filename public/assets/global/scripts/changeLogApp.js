/**
 *
 *
 */
var ChangeLogApp = function () {
    // Portlet element
    var portletEl = $("#change_log_portlet"),

        // Action buttons element
        actionButtonsEl = portletEl.find('.actions.pull-right ul li'),

        // Filters element
        filtersEl = portletEl.find(".table-filters form"),

        // Table element
        tableSelector = "#changes_log_state_table",

        // Action level
        changeLevel = "";

    function manageChangeLevels() {
        filtersEl.on("change", "select", function () {
            updateActionLevel();
            manageActionsButtons();
        });
    }

    // cleans up the filters into an manageable array
    function filtersToArray() {
        var setFilters = [];
        $.each(filtersEl.serializeArray(), function (key, item) {
            if (item.name === undefined) {
                return true;
            }

            if (item.name.substring(item.name.length - 2) == "[]") {
                item.name = item.name.substring(0, item.name.length - 2);
            }

            setFilters[item.name] = item.value;
        });
        return setFilters;
    }

    // Updates the changeLevel global variable
    function updateActionLevel() {
        var setFilters = filtersToArray();

        changeLevel = "";
        if (setFilters.keyword) {
            changeLevel = "keyword";
        }
        else if (setFilters.ad_group) {
            changeLevel = "keyword";
        }
        else if (setFilters.campaign) {
            changeLevel = "ad_group";
        }
        else if (setFilters.account) {
            changeLevel = "campaign";
        }
    }

    // Manage (hide/show) of buttons in actions dropdown
    function manageActionsButtons() {
        actionButtonsEl.each(function () {
            var $this = $(this);

            var itemChangeLevels = $this.find("a").attr("data-change_level").split(",");

            if (changeLevel.length > 0 && $.inArray(changeLevel, itemChangeLevels) >= 0) {
                $this.removeClass("hide");
            } else {
                $this.addClass("hide");
            }
        });
    }

    // Listen to change action button
    function listenToChangeAction() {
        actionButtonsEl.on("click", function () {
            var $this = $(this);
            var tableEl = portletEl.find(tableSelector);
            var setFilters = filtersEl.serializeArray();
            var anchorHref = $this.find("a").attr("data-route");

            // Pass the action button clicked
            setFilters.push({name: "action", value: $this.find("a").attr("data-change_type")});

            // Checkboxes
            // Is the select all results checkbox selected
            var checkboxesSelections = tableEl.find("input[type='checkbox']:checked");
            if (checkboxesSelections.length == 0) {
                console.log(checkboxesSelections);
                // App.toastr.info("Missing selections", "Mark who do you want this action to work upon in the table.");
                return false;
            }

            if (checkboxesSelections.hasClass("group-checkable")) {
                setFilters.push({name: "checkbox", value: "ALL"});
            }
            else { // Otherwise return the ids of each one of the selected results
                var selected = [];
                $.each(checkboxesSelections.serializeArray(), function (idx, item) {
                    setFilters.push(item);
                });

                setFilters.push(selected);

            }

            // Open ajax modal with the settings for this command
            App.openAjaxModal(anchorHref + "&" + $.param(setFilters));
        });
    }

    // The lib api
    function handleFiltersDependencies() {
        filtersEl.on("change", ".select2", function (a, b) {
            clearSelectsFromDependents(this);
        });

        function clearSelectsFromDependents(changedSelect) {
            var $changedSelect = $(changedSelect);
            $.each(filtersEl.find(".select2"), function (select) {
                var $select = $(this);

                if ($select.hasClass("select2-hidden-accessible") && $select.val() !== null) {
                    var atts = $select.data();

                    // Iterate on the attributes to see if the select element is
                    // dependent on the changed select element
                    $.each(atts, function (att, value) {
                        if (typeof att === "string" &&
                            att.search("dependency_selector") == 0 &&
                            $changedSelect.attr("id") == value.substring(1)) {
                            $select.select2("val", -1);
                            return false;
                        }
                    });
                }
            });
        }
    }

    return {
        init: function () {
            // Do something awesome
            manageChangeLevels();
            manageActionsButtons();
            listenToChangeAction();
            handleFiltersDependencies();
        },

        // Returns the action level
        getActionLevel: function () {
            return changeLevel;
        }
    }
}();

jQuery(document).ready(function () {
    ChangeLogApp.init();
});