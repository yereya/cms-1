(function () {
    var createLockUrl = '/entity_lock/create';
    var defaultPollingInterval = 30; //seconds
    var isErrorDisplay = false;

    var LOCK_CREATED = 'LOCK_CREATED';
    var LOCK_ERROR = 'LOCK_ERROR';

    function createNewLock() {
        sendRequest(App.lock_params);
    }

    function createHardLock() {
        var lock_params = $.extend({}, App.lock_params, {hard_lock: true});
        sendRequest(lock_params);
    }

    function sendRequest(lock_params) {
        var request = $.ajax({
            dataType: "json",
            url: createLockUrl,
            data: lock_params
        });

        request.done(handleResponse);
        request.fail(handleFail);

    }

    function handleResponse(response) {
        if (response.status == LOCK_ERROR) {
            handleLockError(response)

        } else if (response.status == LOCK_CREATED) {
            if (isErrorDisplay) {
                removeError();
            }
        }
    }

    function handleLockError(response) {
        if (!isErrorDisplay) {
            swal({
                type: 'warning',
                title: 'Page is Lock',
                html: response.desc,
                showConfirmButton: false,
            });

            $('.take-over-btn').click(createHardLock);

            isErrorDisplay = true;
        }
    }


    function removeError() {
        swal.close();
        isErrorDisplay = false;
    }

    function handleFail(jqXHR, textStatus) {
        console.error('Fail from entityLock.js', jqXHR, textStatus);
    }

    function lockPolling(pollingIntervalTime) {
        setInterval(createNewLock, pollingIntervalTime);
    }

    function init() {
            if (App.lock_params) {
                var pollingIntervalTime = (App.lock_params['polling_interval'] || defaultPollingInterval) * 1000;
                delete App.lock_params['polling_interval'];

                createNewLock();
                lockPolling(pollingIntervalTime);
            }
    }

    $(document).ready(function () {
        init();
    });
})();
