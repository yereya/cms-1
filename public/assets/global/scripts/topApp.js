var topApp = function () {
    /**
     * Init content type fields
     */
    var initContentTypeFields = function () {
        //first initial
        $('.ajax-radio-button').each(function() {
            var $tag = $('#field-metadata');
            if ($(this).data('field_id') && $(this).data('route')) {
                var route = $(this).data('route') + '/' + $(this).val();
                var field = $(this).data('field_id');

                route += '?field_id=' + field;

                $tag.html('');

                insertBlockToTag(route, $tag);
            }
        });

        /**
         * Ajax radio button click event.
         **/
        $('.ajax-radio-button').on('click', function() {
            var $tag = $('#field-metadata');
            var route = $(this).attr('data-route') + '/' + $(this).val();
            var field = $(this).attr('data-field_id');

            if (field) {
                route += '?field_id=' + field;
            }

            disableDefaultCheck($(this).attr('id'));

            if (route !== null && route != '') {
                $tag.html('');

                insertBlockToTag(route, $tag);
            }
        });

        /**
         * To avoid SQL errors, check the field type
         * and disable the default text field when needed.
         *
         * @param {string} field
         *
         * @return void
         **/
        function disableDefaultCheck(field){
            var default_text_element = $("#default");

            switch(field){
                case 'field_type_text':
                case 'field_type_textarea':
                case 'field_type_wysiwyg':
                case 'field_type_image':
                case 'field_type_select':
                case 'field_type_checkbox':
                case 'field_type_multi_select':
                    default_text_element.val('');
                    default_text_element.prop('disabled',true);
                    break;
                default:
                    default_text_element.prop('disabled',false);
                    break;
            }
        }

    };

    /**
     * Init page type functionality
     */
    var initPageType = function() {

        getAjaxChoiceHtml = function (element, hasType) {
            var $element = $(element);
            var type_param = hasType ? '&type=' + $element.val() : '';
            var route = $element.data('route');
            console.log(route);
            var tag = $('#field-metadata');

            if (route != 'undefined' && route) {
                route += type_param;
                tag.html('');

                insertBlockToTag(route, tag);
            }
        };

        $('.choice_type').each(function () {
            getAjaxChoiceHtml(this, true);
        });

        $('.choice_type').on('change', function() {
            getAjaxChoiceHtml(this, true);
        });

        $('.choice_type_click').on('click', function (event) {
            event.preventDefault();

            getAjaxChoiceHtml(this, false);
        });
    };

    /**
     * Ajax - insert html block
     *
     * @param route
     * @param tag
     */
    var insertBlockToTag = function (route, tag) {
        $.get(route, function (resp, status, xhr) {
            if (resp && xhr.status === 200) {
                tag.html(resp);
            }
        });
    }

    // All types of string sanitizer
    // some code taken from string.js and underscore.js
    var string = {
        toSnake: function(str) {
            return str.trim()
                .replace(/([a-z\d])([A-Z]+)/g, '$1_$2')
                .replace(/([A-Z\d]+)([A-Z][a-z])/g,'$1_$2')
                .replace(/[-\s]+/g, '_')
                .replace(/[`~!@#$%^&*()|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
                .toLowerCase();
        },

        toHumane: function(str) { //modified from underscore.string
            return this.toSnake(str).replace(/_id$/,'').replace(/_/g, ' ').trim().capitalize()
        },

        toCamel: function(str) {
            return this.trim(str).replace(/(\-|_|\s)+(.)?/g, function(mathc, sep, c) {
                return (c ? c.toUpperCase() : '');
            });
        },

        // Trim spaces from beginning and end
        trim: function(str) {
            return str.replace(/(^\s*|\s*$)/g, '');
        },

        toSlug: function(str) {
            return this.toSnake(str).replace(/_/g, '-');
        }
    };

    // Sensitization handler and watcher
    var handleSanitizedFields = function() {

        $('input[data-sanitizer_watch]').each(function (idx, item) {
            var $item = $(item);

            $($item.attr('data-sanitizer_watch')).on('propertychange change click keyup input paste', function () {
                var cleanValue = sanitize($(this).val(), $item.attr('data-sanitizer_method'));
                $item.val(cleanValue);
            })
        });

        function sanitize(str, sensitizationMethod) {
            return topApp.string[sensitizationMethod](str);
        }
    };

    /**
     * Init wysiwyg
     *
     * @returns {boolean}
     */
    var initWysiwyg = function () {
        if (!$('textarea.wysiwyg')) {
            return false;
        }

        var $wysiwyg = $('textarea.wysiwyg');

        var elFinderBrowser = function(field_name, url, type, win) {
            tinymce.activeEditor.windowManager.open({
                file: $wysiwyg.data('route'),// use an absolute path!
                title: 'File manager',
                width: 900,
                height: 450,
                resizable: 'yes'
            }, {
                setUrl: function (url) {
                    win.document.getElementById(field_name).value = url;
                }
            });
            return false;
        }

        var tinyMceProperties = {
            selector: 'textarea.wysiwyg',
            setup: function (editor) {
                editor.on('change', function () {
                    editor.save();
                });
            },
            height: 200,
            theme: 'modern',
            plugins: [
                'advlist autolink lists image charmap print preview hr anchor pagebreak',
                'searchreplace wordcount visualblocks visualchars code fullscreen',
                'insertdatetime media nonbreaking save table contextmenu directionality',
                'emoticons template paste textcolor colorpicker textpattern imagetools tp_autocomlite_link'
            ],
            toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image tp_autocomlite_link',
            toolbar2: 'print preview media | forecolor backcolor emoticons | code | fontselect fontsizeselect',
            fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 24pt 32pt',
            font_formats: 'Roboto=Roboto;Open Sans=Open Sans;Oswald=Oswald;',
            content_css: [
                '//fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,700,700i,900,900i&amp;subset=latin-ext',
                '//fonts.googleapis.com/css?family=Open+Sans:400,700',
                '//fonts.googleapis.com/css?family=Oswald:400'
            ],
            style_formats_merge: true,
            style_formats: [
                {
                    title: 'Font Weight', items: [
                    {title: 'Regular', inline: 'span', styles: {'font-weight': '400'}},
                    {title: 'Light', inline: 'span', styles: {'font-weight': '300'}},
                    {title: 'Bold', inline: 'span', styles: {'font-weight': '700'}},
                    {title: 'Bolder', inline: 'span', styles: {'font-weight': '900'}}
                ]
                }
            ],
            route_link_list: $wysiwyg.data('route_page_list'),
            setup : function(editor)
            {
                editor.on('init', function()
                {
                    this.getDoc().body.style.fontSize = '12pt';
                    this.getDoc().body.style.fontFamily = 'Roboto';
                });
            },
            image_advtab: true,
            image_caption: true,
            image_title: true,
            relative_urls: false,
            convert_urls: false,
            remove_script_host: true,
            file_picker_types: 'image media',
            automatic_uploads: true,
            images_upload_credentials: true,
            target_list: [
                {title: 'None', value: ''},
                {title: 'Open link in same page', value: '_self'},
                {title: 'Open link in new page', value: '_blank'}
            ],
            link_title: false,
            link_anchor: false,
            relative_urls: false,
            file_browser_callback : elFinderBrowser,
            valid_elements: "*[*]"
        };

        tinymce.init(tinyMceProperties);
    };

    /**
     * Init row sortable
     * example see: resources/views/partials/containers/sites/custom-fields/containers/checkboxes.blade.php
     */
    var initSortable = function() {
        if (!$('.sortable') || typeof $.sortable != 'function') {
            return false;
        }

        $('.sortable').sortable({
            revert: true,
            start: function (event, ui) {
                $(ui.item).css('background-color', 'rgba(43, 54, 67, 0.3)');
            },
            stop: function (event, ui) {
                var $element = $(ui.item);
                $element.css('background-color', 'rgba(255, 255, 255, 1)');

                if ($element.find('input[type="radio"], input[type="checkbox"]')) {
                    var count = 0;
                    $.each($element.parent().find('input[type="radio"], input[type="checkbox"]'), function() {
                        $(this).val(count);
                        count++;
                    });
                }
            }
        });

        $('.sortable').on('mouseover', '.ui-sortable-handle', function () {
            var arrows = $(this).find('i.draggable-icon');
            arrows.show();
        });
        $('.sortable').on('mouseout', '.ui-sortable-handle', function () {
            var arrows = $(this).find('i.draggable-icon');
            arrows.hide();
        });
    }

    /**
     * Init file manager add image to preview
     */
    var initFileManagerImageModal = function() {
        //if file manager load and user click to file, found this file and get path
        var $iFrame = $('iframe.iframe-file-manager');
        if ($iFrame.length > 0) {
            $iFrame.load(function() {
                $iFrame.contents().on('click', '.elfinder-cwd-file', function (e) {
                    e.preventDefault();
                    if (!$(this).hasClass('directory')) {
                        var $this = $(this);

                        var url = $iFrame.data('find_file')
                            + '&_token=' + App.csrf
                            + '&cmd=info&targets[0]=' + $this.attr('id')
                            + '&_=' + Date.now()
                            + '&fileInfo=1'
                            + '&field_name=' + $iFrame.data('field_name');

                        $.ajax({
                            url: url,
                            success: function (response) {
                                if (response) {
                                    insertToPreview(response.field_name, response.name, response);
                                } else {
                                    toastr.error('File not found in database!');
                                }
                            }
                        });

                        var modalId = '#' + $iFrame.parents('.modal').attr('id');
                        closeModal(modalId);
                    }
                });
            });
        }

        // close file manager model
        var closeModal = function(id) {
            if (id) {
                $(id).modal('toggle');
            } else {
                $('#ajax-modal').modal('toggle');
            }
        };

        // insert file to image preview
        var insertToPreview = function(fieldName, imageName, file) {
            var imagePath = file.full_path;
            var imageClass = '.image_preview_' + fieldName;
            var $img = $(imageClass);
            var $imageUpload = $img.parent().parent();
            $img.one('load', function(){
                $imageUpload.find('.iu-toolbar-preview').attr({'href': imagePath, 'file_id': file.model.id});
                setOneImageUploadProperties($imageUpload);
                var imageName = imagePath.replace(/^.*[\\\/]/, '');
                $imageUpload.find('.image-upload-header')
                    .text(imageName)
                    .attr('title', imageName);
            });
            $img.attr('src', imagePath);
            $imageUpload.find('input.image_preview_' + fieldName).val(imagePath);
            $imageUpload.find('input.image_preview_id_' + fieldName).val(file.model.id);
            showButtonsAfterPreview($imageUpload);
        };

        var clearImageProperties = function($imageUpload) {
            $imageUpload.find('.image-upload-header').text('');
            $imageUpload.find('.iu-footer-height').text('');
            $imageUpload.find('.iu-footer-size').text('');
            $imageUpload.find('img').attr('src', '/assets/global/img/no-image.png');
        };

        // show delete button
        var showButtonsAfterPreview = function($imageUpload) {
            $imageUpload.find('.iu-toolbar-delete').show();
            $imageUpload.find('.iu-toolbar-preview').show();
        };

        // hide delete button
        var hideButtonsAfterDelete = function($imageUpload) {
            $imageUpload.find('.iu-toolbar-delete').hide();
            $imageUpload.find('.iu-toolbar-preview').hide();
        };

        // add remove event to delete button
        var eventButtonDeleteImage = function() {
            $('.iu-toolbar-delete').on('click', function(event) {
                var fieldName = $(this).data('name');
                $('.image_preview_' + fieldName).attr('src', '');
                $('.image_preview_' + fieldName).val('');
                $('.image_preview_id_' + fieldName).val('');
                $image_upload = $(event.target).closest('.image-upload');
                hideButtonsAfterDelete($image_upload);
                clearImageProperties($image_upload);
            })
        };

        // init delete event
        eventButtonDeleteImage();
    }

    /**
     * @param img
     * @param $imageUpload - jquery object of the image upload div
     * @param cb - callback
     */
    var computeImageSize = function(img, $imageUpload, cb) {
        var xhr = new XMLHttpRequest();
        xhr.open('HEAD', img.src, true);
        xhr.onreadystatechange = function(){
            if ( xhr.readyState == 4 ) {
                if ( xhr.status == 200 ) {
                    var size = xhr.getResponseHeader('Content-Length');
                    cb(true, size , img, $imageUpload);
                } else {
                    console.log( ' fail to get image size');
                }
            }
        };
        xhr.send(null);
    };

    /**
     * Write image size, width and height to footer div under image
     * @param result
     * @param size
     * @param img
     * @param $imageUpload
     */
    var writeOnImageFotter = function(result, size, img, $imageUpload) {
        if(result) {
            var imageSize = Math.round(size/1024);
            $imageUpload.find('.iu-footer-height').text(  img.width + ' x ' + img.height );
            $imageUpload.find('.iu-footer-size').text( imageSize + 'kb ');

        }
    };

    var setOneImageUploadProperties = function($imageUpload) {
        var src = $imageUpload.find('img').attr('src');
        var img = new Image();
        img.src = src;
        computeImageSize(img, $imageUpload, writeOnImageFotter)
    };

    /**
     *  Set the properties of width height and size of image to all images using image-upload widget
     */
    var setAllImageUploadProperties = function () {
        var imageUpload = $('.image-upload');
        if( imageUpload.length == 0 ) return;
        for(var i=0; i < imageUpload.length; i++ ) {
            var $imageUpload = $(imageUpload[i]);
            if(! $imageUpload.hasClass('iu-no-image')) {
                setOneImageUploadProperties($imageUpload);
            }
        }
    };

    var initElfinder = function () {
        if ($.fn.elfinder) {
            $().ready(function () {
                var elf = $('#elfinder').elfinder({
                    customData: {
                        _token: App.csrf
                    },
                    url: $('#elfinder').data('route'),
                    height: 500,
                    dialog: {
                        width: 300,  modal: true, title: 'Select a file'},
                    resizable: true,
                    commands : [
                        'labels', 'alt', 'open', 'reload', 'home', 'up', 'back', 'forward', 'quicklook',
                        'download', 'rm', 'rename', 'mkdir', 'mkfile', 'upload', 'copy',
                        'cut', 'paste', 'edit', 'extract', 'archive', 'search', 'info', 'view', 'resize', 'sort'
                    ],
                    contextmenu : {
                        // navbarfolder menu
                        navbar : ['open', '|', 'copy', 'cut', 'paste', '|', 'rm', '|', 'info'],
                        // current directory menu
                        cwd    : ['reload', 'back', '|', 'upload', 'mkdir', 'mkfile', 'paste', '|', 'sort', '|', 'info'],
                        // current directory file menu
                        files  : ['quicklook', '|', 'download', '|', 'labels', 'alt', 'copy', 'cut', 'paste', '|', 'rm', '|', 'edit', 'rename', 'resize', '|', 'archive', 'extract', '|', 'info']
                    },
                    commandsOptions: {
                        getfile: {
                            oncomplete: 'destroy'
                        }
                    },
                    getFileCallback: function (file) {
                        window.parent.processSelectedFile(file.path, $('#elfinder').data('process_selected'));
                        parent.jQuery.colorbox.close();
                    }
                }).elfinder('instance');
            });

            /**
             * Custom command alt
             */
            elFinder.prototype.commands.alt = function() {
                this.exec = function(hashes) {
                    //implement what the custom command should do here
                    var url = $('#elfinder').data('route')
                        + '&_token=' + App.csrf
                        + '&cmd=info&targets[0]=' + hashes
                        + '&_=' + Date.now()
                        + '&isAlt=1';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        success: function (response) {
                            console.log(response);
                            App.ajaxJSModal('alt-modal', response);
                        }
                    })
                }
                this.getstate = function() {
                    //return 0 to enable, -1 to disable icon access
                    return 0;
                }
            }

            /**
             * Custom command labels
             */
            elFinder.prototype.commands.labels = function() {
                this.exec = function(hashes) {
                    //implement what the custom command should do here
                    var url = $('#elfinder').data('route')
                        + '&_token=' + App.csrf
                        + '&cmd=info&targets[0]=' + hashes
                        + '&_=' + Date.now()
                        + '&isLabels=1';
                    $.ajax({
                        type: 'GET',
                        url: url,
                        success: function (response) {
                            App.ajaxJSModal('label-modal', response);
                        }
                    })
                }
                this.getstate = function() {
                    //return 0 to enable, -1 to disable icon access
                    return 0;
                }
            }
        }
    };

    var initTabAjax = function (id) {
        var $tabs = id ? $(id) : $('[data-toggle="tabajax"]');

        function isEmpty(el){
            return !$.trim(el.html())
        }

        $tabs.each(function () {
            $(this).click(function() {
                var $this = $(this);
                var route = $this.data('route');
                var tab_id = $this.data('id');
                var view = $this.data('view')
                    ? '#' + tab_id + ' .' + $this.data('view')
                    : '#' + tab_id + ' .nestable-lists';
                var isOnce = $this.data('once');
                var paramsFrom = $(this).data('tab_params');
                var idToParams = '#' + paramsFrom + ' :input, #' + paramsFrom + ' select';
                var params = $(idToParams).serialize();

                if (isOnce && !isEmpty($(view))) {
                    $(this).tab('show');
                    return false;
                }

                $(view).html('');

                $.ajax({
                    type: 'GET',
                    url: route,
                    data: params,
                    success: function (response) {
                        $(view).html(response);
                        $.each($(view + ' .row'), function () {
                            initAppMethods($(this));
                        });
                    }
                });

                $(this).tab('show');
                return false;
            });
        });

        /**
         * Init app methods to custom view
         *
         * @param dataAttr
         * @returns {boolean}
         */
        var initAppMethods = function (dataAttr) {
            if (!dataAttr.data()) {
                return false;
            }

            if ('select_to_nestable' in dataAttr.data()) {
                topApp.createNestable(dataAttr.data('select_to_nestable'));
            }

            var initAppMethods = function () {
                if ('app_nestable' in dataAttr.data()) {
                    App.nestableSortable(dataAttr.data('app_nestable'));
                }

                if ('app_select' in dataAttr.data()) {
                    App.initSelect2ToSelects();
                }
            }

            appJsLoaded(initAppMethods);
        }
    }

    /**
     * TODO - remove after upgrade to require.js
     *
     * Check if app js loaded
     *
     * @param callback
     */
    var appJsLoaded = function (callback) {
        var initMethods = function (js, callback) {
            var interval = null;

            interval = setInterval(function() {
                if ($(js).length > 0) {
                    clearInterval(interval);
                    callback();
                }
            }, 1000);
        }

        initMethods('script[src*="app.js"]', callback);
    }

    /**
     * Add functionality dynamic items to nestable list
     *
     * @param container
     */
    var createNestable = function (container) {
        var $container = $('#' + container);
        var button = $container.data('button');
        var choiceList = $container.data('choice_list')
        var nestableList = $container.data('nestable_list');
        var route = $container.data('ajax_url') ? $container.data('ajax_url') : undefined;
        var input = $container.data('save_input') ? $container.data('save_input') : undefined;
        var itemContainer = $container.data('with_container') ? $container.data('with_container') : undefined;

        // disable submit button if nothing not choice
        var submitDisabledEvent = function () {
            $(button).prop('disabled', true);

            $(choiceList).on('change', function () {
                if ($(choiceList).find('option:selected').length > 0) {
                    $(button).prop('disabled', false);
                } else {
                    $(button).prop('disabled', true);
                }
            });
        }

        // return object to insert {id, value}
        var getListObject = function () {
            if (!route && choiceList) {
                return {id: $(choiceList).val(), value: $(choiceList).find('option:selected').text().trim()};
            } else {
                //TODO ajax get object - currently not use
            }
        }

        // remove item
        var removeEvent = function () {
            $(nestableList).on('click', '.remove_item', function () {
                $(this).parents('li').remove();
                saveToInput();
            })
        }

        var showMoreEvent = function () {
            $(nestableList).on('click', '.show_more', function () {
                var $this = $(this);
                var $li = $this.parents('li');
                var $container = $li.find('.with_container');
                var $child = $this.children().first();

                if ($child.hasClass('fa-minus')) {
                    $child.removeClass('fa-minus').addClass('fa-plus');
                    $container.addClass('hidden').hide('fast');
                } else {
                    $child.removeClass('fa-plus').addClass('fa-minus');
                    $container.removeClass('hidden').show(600);
                }
            })
        }

        // insert item to nestable list
        var insertItemToList = function (item) {
            var itemContainerContent = null;
            var li = $('<li />')
                .addClass('dd-item dd3-item')
                .attr('data-id', item.id)
                .attr('item_id', item.id)
                .attr('id', 'parent_' + item.id);

            var div = $('<div />')
                .addClass('dd3-content')
                .addClass('ui-sortable-handle')
                .text(item.value);

            var hamburgerDiv = $('<div class="dd-handle dd3-handle ui-sortable-handle">Drag</div>');

            var actionsContainer = $('<div />')
                .addClass('pull-right')
                .addClass('control_buttons');

            if (itemContainer) {
                var collapse = $('<a href="javascript:;" title="Show More" class="show_more"><i class="fa fa-plus"></i></a>');
                actionsContainer.append(collapse);

                itemContainerContent = $container.find('.with_container.template').first();
                var clone = itemContainerContent.clone();
                clone.removeClass('template');

                clone.find('input, select').each(function (index, tag) {
                    var $tag = $(tag);
                    var oldName = $tag.attr('name');
                    var newName = 'tab[_' + item.id + '][' + oldName + ']';
                    $tag.attr('name', newName);
                });

                li.append(clone);
            } else {
                var del = $('<a href="javascript:;" title="Remove from list" class="tooltips remove_item"><i class="fa fa-trash"></i></a>');
                actionsContainer.append(del);
            }

            $(nestableList).append(li.prepend(div.append(actionsContainer)).append(hamburgerDiv));
            $(nestableList).nestedSortable();

            $(choiceList).select2('val', -1);

            saveToInput();
        }

        // add item event
        var addSubmitEvent = function () {
            $(button).on('click', function (e) {
                e.preventDefault();

                var list = getListObject();
                insertItemToList(list);
            });
        }

        var saveToInput = function () {
            if (input) {
                var $list = $(nestableList).find('li');
                var toHierarchy = [];


                $list.each(function () {
                    var params, $this = $(this);
                    params = {
                        tab_name: $this.find('.item_tab_name').val(),
                        default: $this.find('.item_tab_default').is(':checked') ? 1 : 0,
                        widget_data_id: $this.attr('item_id')
                    }

                    toHierarchy.push(params);
                })

                var $input = $('input[name="' + input + '"]');
                $input.val(JSON.stringify(toHierarchy));
            }
        }

        var submitForm = function () {
            var form = $container.parents('form');

            if (form) {
                $(form).submit(function (e) {
                    saveToInput();
                })
            }
        }

        // register events
        var registerEvents = function () {
            submitDisabledEvent();
            removeEvent();
            showMoreEvent();
            addSubmitEvent();
            saveToInput();
            submitForm();
        }

        registerEvents();
    }

    return {
        init: function () {
            initContentTypeFields();
            initWysiwyg();
            handleSanitizedFields();
            initSortable();
            initFileManagerImageModal();
            setAllImageUploadProperties();
            initPageType();
            initElfinder();
        },

        string: string,

        wysiwyg: function () {
            initWysiwyg();
        },

        sortable: function () {
            initSortable();
        },

        fileManagerEvent: function () {
            initFileManagerImageModal();
        },

        pageType: function (selector) {
            initPageType(selector);
        },

        sanitized: function () {
            handleSanitizedFields();
        },

        tabAjax: function (id) {
            initTabAjax(id);
        },

        createNestable: function (container) {
            createNestable(container);
        }
    }
}();

$(document).ready(function () {
    topApp.init();
});


