/**
 Core script to handle the entire theme and core functions
 **/
var App = function () {

    // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

    var resizeHandlers = [];

    var assetsPath = '/assets/';

    var globalImgPath = 'global/img/';

    var globalPluginsPath = 'global/plugins/';

    var globalCssPath = 'global/css/';

    // theme layout color set

    var brandColors = {
        'blue': '#89C4F4',
        'red': '#F3565D',
        'green': '#1bbc9b',
        'purple': '#9b59b6',
        'grey': '#95a5a6',
        'yellow': '#F8CB00'
    };

    var xhrDataTablePool = [];

    // initializes main settings
    var handleInit = function () {

        initEditableOptions();

        if ($('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !!navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !!navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !!navigator.userAgent.match(/MSIE 10.0/);

        if (isIE10) {
            $('html').addClass('ie10'); // detect IE10 version
        }

        if (isIE10 || isIE9 || isIE8) {
            $('html').addClass('ie'); // detect IE10 version
        }
    };

    var pageContainer = $('.page-content');

    // Will activate all the scripts related to forms
    function initFormFields() {
        initDependedGroup();
    }

    function initEditableOptions() {
        if ($().editable) {
            $.fn.editable.defaults.mode = 'popup';
            $.fn.editable.defaults.placement = 'top';
            $.fn.editable.defaults.inputclass = "form-control";
        }
    }

    /**
     * depended group
     * if checkbox checked - input enabled else input disabled
     * work with checkbox and radio buttons
     */
    function initDependedGroup() {
        $('.depended-group').each(function () {
            var $group = $(this);
            var selector = $group.data('dependency_selector');
            if (selector) {
                var is_checkbox = $group.find(selector).is(':checkbox');
                var is_radio = $group.find(selector).is(':radio');

                if (is_checkbox) {
                    checkboxGroup($group, selector);
                } else if (is_radio) {
                    radioGroup($group, selector);
                }
            }
        });

        function radioGroup($group, selector) {
            var radio = $group.find(selector);
            if (radio.prop('checked', false)) {
                disableInput($group, selector);
            }

            checkbox.on('change', function () {
                if ($(this).is(':checked')) {
                    enableInput($group, selector);
                } else {
                    disableInput($group, selector);
                }
            });
        }

        function checkboxGroup($group, selector) {
            var checkbox = $group.find(selector);
            if (!checkbox.is(':checked')) {
                disableInput($group, selector);
            }

            checkbox.on('change', function () {
                if ($(this).is(':checked')) {
                    enableInput($group, selector);
                } else {
                    disableInput($group, selector);
                }
            });
        }

        function disableInput($group, selector) {
            $group.find('input').each(function () {
                if (!$(this).hasClass(selector)) {
                    $(this).hide();
                }
            });
        }

        function enableInput($group, selector) {
            $group.find('input').each(function () {
                if (!$(this).hasClass(selector)) {
                    $(this).show();
                }
            });
        }
    }

    /** init row with multi select (ajax version) **/
    function initMultiSelect2Boxs() {
        $('.multi-select-ajax').on('change', 'select', function () {
            var $currentSelect = $(this); //current select
            var url = $(this).data('url_search'); //url to search results
            var $nextSelect = $('.' + $(this).data('show_select_class')); //element select to connected results
            var currentValue = $(this).val(); //current value

            //if found url and element to connect new results else do nothing
            if (url && $nextSelect) {
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {_token: App.csrf, value: currentValue},
                    success: function (response) {
                        if (response.status == 'success') {
                            insertToSelect(response.data, $nextSelect);
                        }
                    }
                });
            }
        });

        //connect new options to select
        function insertToSelect($options, $select) {
            $select.select2('destroy'); //destroy select2
            $select.find('options').remove(); //remove all old options
            $.each($options, function () {
                var option = "<option value='" + this.id + "'>" + this.name + "</option>";
                $select.append(option); //add new option
            });
            initSelect2Element($select); //re-init select2
        }
    }

    /** Init event listener for select collapse **/
    function initSelect2Collapse() {
        $('select[data-toggle="collapse"]').on('change', function () {
            var target = $(this).data('target') ? $(this).data('target') : $(this).val();
            var state = !!$(this).val();

            $(target).toggle(state);
        }).change();
    }

    /**
     * Activate duplicated group
     */
    function initDuplicateGroup() {
        var containers = $('.tb-duplicate-group').parent();

        containers.each(function () {
            var container = $(this);
            container.on('click', '.remove-row-btn', function () {
                removeGroup($(this).parents('.tb-duplicate-group'));
            });

            container.on('click', '.clone-row-btn', function () {
                cloneGroup($(this).parents('.tb-duplicate-group'));
            });

            container.on('change', 'select', function (e) {
                var $this = $(this);
                if ($this.hasClass('where_operator')) {
                    return;
                }
                var selectedOptionValue = $this.val();
                var currentRow = $this.parents('div.form-group'); //current row;

                var operatorValue = currentRow.find('.where_operator').val();
                var where_column_name = currentRow.find('.select_where_column_name');
                var where_column_data_attrs = where_column_name.data();
                var fieldType = 'field_' + $('option:selected', where_column_name).attr('type');

                // if the operator is 5 - "LIKE" or 6 - "NOT LIKE"
                if (operatorValue == 5 || operatorValue == 6) {
                    fieldType = 'field_input';
                }

                if (!$this.hasClass('select2-multiple') && $.isNumeric(selectedOptionValue)) {
                    $this.find('option').removeAttr('selected'); //remove all selected in select box
                    $this.find('option[value=' + selectedOptionValue + ']').attr('selected', 'selected');  //add current selected to select box
                    $this.val(selectedOptionValue); //add current selected id to select2
                }

                /** parse data attributes, and check if has data-field_. If exists show or hide elements **/
                for (var attr in where_column_data_attrs) {
                    if (attr.indexOf('field_') == 0) {
                        var field = currentRow.find($this.data(attr));
                        if (attr.indexOf(fieldType) == 0 && selectedOptionValue) {
                            field.attr('data-depend_select2_field', selectedOptionValue);
                            field.parent().parent().show();
                        } else {
                            if (field.hasClass('select2')) {
                                field.val(null).trigger("change");
                            } else {
                                field.val('');
                            }
                            field.parent().parent().hide();
                        }
                    }
                }
            });
        });

        function removeGroup(element) {
            var duplicateGroups = element.parent().find('.tb-duplicate-group');

            if (duplicateGroups.length > 1) {
                element.find('select').each(function () {
                    $(this).select2('destroy');
                });
                element.remove();
            } else {
                element.find('input').val('');
                element.find('select').each(function () {
                    $(this).select2('val', -1);
                });
            }
        }

        function cloneGroup(element) {
            //destroy all select2
            element.find('select').each(function () {
                $(this).select2('destroy');
            });
            //clone this element
            var clone = element.clone();
            //increase index name
            //in this a place, before init select2
            this.increaseNameToDuplicate(clone, 'where');
            this.increaseNameToDuplicate(clone, 'having');
            this.increaseNameToDuplicate(clone, 'archive');
            //add select2
            element.find('select').each(function () {
                if ($(this).hasClass('select2-multiple')) {
                    $(this).find('option').each(function (key, option) {
                        $(option).attr('selected', 'selected');
                    });
                }

                initSelect2Element($(this));
            });

            clone.find('select').each(function () {
                $('option', $(this)).each(function () {
                    $(this).removeAttr('selected');
                });
                if ($(this).data('value')) {
                    $(this).attr('data-value', -1);
                }
                initSelect2Element($(this));
                $(this).select2("val", "");
            });

            clone.find('input').val('');
            clone.insertAfter(element).hide().fadeIn();

            //hide input or select fields in reports pages
            if (clone.find('select').data('field_input') || clone.find('select').data('field_select')) {
                var input_class = clone.find('select').data('field_input');
                var select_class = clone.find('select').data('field_select');

                clone.find(select_class).attr('data-depend_select2_field', 'column');
                clone.find(select_class).find('option').each(function () {
                    $(this).remove();
                });

                clone.find(input_class).parent().parent().hide();
                clone.find(select_class).parent().parent().hide();
            }

            //clone radio buttons
            cloneRadioButtons(element, clone);
            //clone checkbox
            cloneCheckboxButtons(element, clone);
        }

        /**
         * clone radio buttons
         *
         * @param original - original element
         * @param clone - clone element
         */
        var cloneRadioButtons = function (original, clone) {
            //TODO - BUG - this part should also run when removing column - check also checkbox case
            if (original.find('input[type="radio"]')) {
                var original_row = original.find('input[type="radio"]');
                var found_id = original_row.attr('id'); //original id

                var count = 0;
                // get radio button rows
                $.each(original.parent().find('input[type="radio"]'), function () {
                    $(this).val(count);
                    count++;
                });

                // change id in input clone radio button
                var clone_row = clone.find('input[type="radio"]');
                clone_row.attr('id', found_id + '_' + count);

                // find and change for in label current radio button
                $.each(clone.find('label'), function () {
                    if ($(this).attr('for') == found_id) {
                        $(this).attr('for', found_id + '_' + count);
                    }
                });

                // remove attribute checked
                clone_row.prop('checked', false);
            }
        }

        /**
         * clone checkbox
         *
         * @param original - original element
         * @param clone - clone element
         */
        var cloneCheckboxButtons = function (original, clone) {
            if (original.find('input[type="checkbox"]')) {
                var original_row = original.find('input[type="checkbox"]');

                var count = 0;
                // get radio button rows
                $.each(original.parent().find('input[type="checkbox"]'), function () {
                    $(this).val(count);
                    count++;
                });

                // change id in input clone radio button
                var clone_row = clone.find('input[type="checkbox"]');
                // insert value
                // clone_row.val(count);

                // remove attribute checked
                clone_row.prop('checked', false);
            }
        }
    }

    /**
     * Auto increase name to next index
     * example:
     * current where[0]
     * next where[1]
     *
     * @param element
     * @param nameAttr
     */
    this.increaseNameToDuplicate = function (element, nameAttr) {
        var style = 'background: red; color: white; display: block;';
        if (element == undefined || nameAttr == undefined) {
            console.log('%c One or more parameters is undefined', style);
            return false;
        }

        var changed = 0;
        $.each(element.find('select, input'), function () {
            var name = $(this).attr('name');
            if (name != undefined && name.indexOf(nameAttr) != -1) {
                var indexStart = name.indexOf('[') + 1;
                var indexEnd = name.indexOf(']');
                var currentArrayIndex = parseInt(name.substring(indexStart, indexEnd));

                if (!isNaN(currentArrayIndex)) {
                    var newIndex = currentArrayIndex + 1;
                    var newName = name.substring(0, indexStart) + newIndex + name.substring(indexEnd, name.length);
                    $(this).attr('name', newName);
                    changed++;
                }
            }
        });

        if (changed == 0) {
            console.log('%c Not changed nothing, check maybe in file.blade.php forget add index to name \'name[index]\'', style);
        }
    };

    function datatablesAddRefreshButton() {
        $('<span id="refresh-wrapper" class="btn btn-sm default"><i class="icon-refresh"></i></span>').prependTo('.pagination-panel');
    }

    function datatablesAddRefreshButtonHoverAnimation() {
        $("#refresh-wrapper .icon-refresh").on('mouseover mouseleave', function (e) {
            $(this).toggleClass('fa-spin', e.type == 'mouseover');
        });
    }

    function refreshDatatableWithAnimation() {
        //refresh datatable on clicking the refresh button
        $("#refresh-wrapper").on('click', function (e) {
            e.preventDefault();

            //show spinning animation on button click
            var $refreshIcon = $("#refresh-wrapper").find('.icon-refresh');

            //the datatable
            var $dataTable = $('table.data-table');

            $refreshIcon.addClass('fa-spin');

            if ($.fn.DataTable.isDataTable($dataTable)) {
                //re-draw the datatable
                $.fn.dataTable.Api($dataTable).draw();
            }

        });
    }

    /**
     * init all datatables in page
     *
     * Search all datatables in page - pointer class data-table
     * Search all containers filter with datatables - pointer class tp-server-side
     *
     * Three types datatablse:
     * 1) with class tp-server-side-datatable - server side datatables
     *      filters, shows, paging
     * 2) static datatable - building in html
     * 3) with class tp-server-side (class in container div)
     *      this table created after press submit button
     */
    function initDatatables() {
        //Search datatabels
        $.each($('table.data-table'), function () {
            var $table = $(this);

            if ($table.hasClass('tp-server-side-datatable')) {
                //server side datatables
                datatableServerSide($table);
            } else {
                //static datatabels
                App.initDatatable($table);
            }
        });

        //search filter containers
        $('.tp-server-side').each(function () {
            buildDatatables($(this));
        });

        $('body').on('click', 'thead > tr > th > label.mt-checkbox', function () {
            toggleDatatableCheckBoxes(this)
        });

        datatablesAddRefreshButton();
        datatablesAddRefreshButtonHoverAnimation();
        refreshDatatableWithAnimation();
    }


    function toggleDatatableCheckBoxes(el) {

        // list of subject checkboxes
        var subjectCheckboxes = $(el).closest('table').find('tbody input[type="checkbox"]'),

            // The new state to set boolean
            // Checked / Unchecked
            newState = $(el).find('div > span').hasClass('checked');

        // Tick the checkboxes
        subjectCheckboxes.prop("checked", newState);
        $.uniform.update();
    }


    /**
     * Build static table after submit filters
     *
     * @param element - this container element, not table
     */
    function buildDatatables(element) {
        //find table id
        var tableId = element.data('datatable_table');

        var cols = [];
        var results = [];
        var headers = [];
        var sort = [];
        var summary = {};

        //add event to submit button
        element.on('submit', function (e) {
            e.preventDefault();
            //run request ajax and build datatable
            runAjax($(this));
        });

        //event download file csv
        element.find('.download_file').on('click', function (e) {
            e.preventDefault();

            var required = true;
            var fieldName = [];
            //check if form have required fields
            $('form').find('select, input').each(function () {
                if ($(this).prop('required')) {
                    if (!$(this).val()) {
                        fieldName.push($(this).attr('name'));
                        required = false;
                    }
                }
            });

            //if all required fields not null
            if (required) {
                runAjax(element, true);
            } else {
                for (var i = 0; i < fieldName.length; i++) {
                    toastr.error('Field ' + fieldName[i] + ' is required');
                }
            }
        });

        function runAjax(element, csv) {
            var method = 'POST',
                action,
                form,
                $element = element;

            if (csv) {
                $element.append('<input class="csv" value="1" name="csv"/>');
            }

            //check if this form or not
            if ($element.prop("tagName") == 'FORM') {
                method = $element.attr('method');
                action = $element.attr('action');
                form = $element.serializeArray();
            } else {
                method = $element.data('server_side_method');
                action = $element.data('server_side_url');
                form = function () {
                    var data = [];
                    $element.each('input, select', function () {
                        var name = $(this).attr('name');
                        var value = $(this).val();
                        var field = {name: value};
                        data.push(field);
                    });

                    return data;
                }
            }

            if (csv) {
                $element.find('.csv').remove();
            }

            App.blockUI();
            $.ajax({
                type: method,
                url: action,
                data: form,
                success: function (response) {
                    App.unblockUI();
                    if (response.status) {
                        parseData(response.data, response.tooltips, response.sort, response.summary);
                    } else if (csv) {
                        window.location = response;
                    } else {
                        toastr.error(response.message);
                    }

                },
                error: function () {
                    App.unblockUI();
                }
            });
        }

        /**
         * Parse json data - get columns name only
         *
         * @param json
         * @param tooltips
         * @param order
         * @param summary_data
         */
        function parseData(json, tooltips, order, summary_data) {
            cols = [];
            results = json;
            headers = tooltips;
            sort = [];
            summary = summary_data;

            if (json.length > 0) {
                $.each(json[0], function (key, value) {
                    cols.push({
                        'mDataProp': key, 'sTitle': key, 'sType': ""
                    })
                });
            }

            if (order) {
                $.each(cols, function (key, value) {
                    var title = value.sTitle;
                    if (order[title]) {
                        sort.push([key, order[title]]);
                    }
                });
            }

            initDT();
        }

        /**
         * create new parameters for datatable - reports
         * and init new datatable
         */
        function initDT() {
            if (cols.length > 0) {
                var properties = {
                    "bDestroy": true, //can destroy
                    "bFilter": true, //search box
                    "aaData": results, //data
                    "bSort": true, //label sort
                    "aoColumns": cols, //columns name
                    "pageLength": 50, //page size
                    "columnDefs": [
                        {"type": "num-html"}
                    ],
                    "order": sort, //sorting
                    "initComplete": function (settings) {
                        $('#report thead th').each(function () {
                            var $td = $(this);
                            var title = $td.text();
                            $td.attr('title', headers[title.toLowerCase()]);
                        });

                        /* Apply the tooltips */
                        $('#report thead th[title]').tooltip({"container": 'body'});
                    },
                    "footerCallback": function (row, data, start, end, display) {
                        if (summary) {
                            var api = this.api();
                            api.columns().flatten().each(function (value, index) {
                                // if column has a summary value then insert it to the relevant
                                // footer td in the tfoot tag
                                var column = api.column(index);
                                var title = $(column.header()).text();
                                if (summary[title]) {
                                    $(column.footer()).html(summary[title]);
                                }
                            });
                        }
                    }
                };
            }

            if ($.fn.DataTable.isDataTable($(tableId))) {
                //clear datatable before insert new data
                $(tableId).dataTable().fnDestroy();
                $(tableId).empty();
            }

            // if there's summary_data then we have too manually append the tfoot, otherwise
            // DataTable plugin will not recognise it and ignore our options
            if (summary) {
                var tds = cols.map(function (obj, index) {
                    return index === 0 ? '<td>Total</td>' : '<td></td>';
                });

                if (tds && tds.length) {
                    tds.reduce(function (a, b) {
                        return a + b
                    });
                    $(tableId).append('<tfoot class="total-footer"><tr role="row">' + tds + '</tr></tfoot>');
                }
            }

            /** init new datatable */
            if (cols.length > 0) {
                App.initDatatable(tableId, properties);

                //sets up numeric sorting of links
                $(tableId).bSort = function () {
                    var x = parseFloat(x);
                    var y = parseFloat(y);
                    return ((x < y || isNaN(y) ) ? -1 : ((x > y || isNaN(x)) ? 1 : 0));
                };
            }
        }
    }

    /**
     * function selected all text in container and put to clipboard
     * @param el document.getElement
     */
    function copyToClipboard(el) {
        var body = document.body, range, sel;
        //select all
        if (document.createRange && window.getSelection) {
            range = document.createRange();
            sel = window.getSelection();
            sel.removeAllRanges();
            try {
                range.selectNodeContents(el);
                sel.addRange(range);
            } catch (e) {
                range.selectNode(el);
                sel.addRange(range);
            }
        } else if (body.createTextRange) {
            range = body.createTextRange();
            range.moveToElementText(el);
            range.select();
        }
        document.execCommand('copy'); //insert to clipboard
        window.getSelection().removeAllRanges(); //deselect all

        toastr.success('Table copy to clipboard successful!');
    }

    function initDatePickers() {
        if ($.fn.timepicker != undefined) {
            $('.timepicker').timepicker({
                showMeridian: false
            });
        }
        if ($.fn.datetimepicker != undefined) {
            $('.datetimepicker').datetimepicker({
                format: 'yyyy-mm-dd hh:ii',
                autoclose: true,
                todayBtn: true
            });
        }
        if ($.fn.datepicker != undefined) {
            $('.date-picker').datepicker({
                format: "yyyy-mm-dd",
                autoclose: true
            });
        }
    }

    function initDateRangePickers() {
        //TimePicker activation
        if ($.fn.daterangepicker != undefined && $('.date-range-picker input').length > 0) {
            var start = moment().subtract(1, 'days'),
                end = moment(),
                insertRange = function (start, end, rangeName) {
                    $('.date-range-picker input[type=text]').val(start.format('YYYY-MM-DD') + ' ' + end.format('YYYY-MM-DD'));
                    $('.date-range-picker input[type=hidden]').val(rangeName);
                };

            $('.date-range-picker').daterangepicker({
                startDate: start,
                endDate: start,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(7, 'days'), moment().subtract(1, 'days')],
                    'Last 14 Days': [moment().subtract(14, 'days'), moment().subtract(1, 'days')],
                    'This Month': [moment().startOf('month'), moment()],
                    'Last 30 days': [moment().subtract(30, 'days'), moment().subtract(1, 'days')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    'This Year': [moment().startOf('year'), moment()],
                }
            }, insertRange);

            // If no default value passed
            // set default
            if ($('.date-range-picker input').val().length == 0) {
                insertRange(start, start);
            }
        }
    }

    /**
     * Datatable server side
     *
     * 1) get columns, search in html code thead
     * 2) get filter container, if exists
     * 3) TODO - create dynamic length, info show and pagination, after init table only
     * 4) add change event to container, if changed refresh datatable, after init table only
     * 5) add new parameters to datatable with ajax container
     * 6) init datatable
     */
    function datatableServerSide($table) {
        var isFirstRun = true;
        var theadCols = $table.find('thead th');
        var filtersContainer = '.' + $table.data('filter_container'); // filter contrainer in top
        var columns = []; // datatable columns
        var timeout; // ajax search, timeout keyup

        $.each(theadCols, function () {
            var name = $.trim($(this).text());
            name = name.toLowerCase();
            name = name.split(' ').join('_');
            columns.push(name);
        });

        $(filtersContainer).on('submit', function (e) {
            e.preventDefault();
            refreshDatatable();
        });

        $('.download_file').on('submit', function (e) {
            e.preventDefault();
            App.blockUI();

            $.ajax({
                type: 'POST',
                url: $table.data('route_datatable'),
                data: {
                    '_token': App.csrf,
                    'csv': true,
                    'method': $table.data('route_method'),
                    'cols': columns,
                    'filters': findFilters(filtersContainer, isFirstRun)
                },
                success: function (response) {
                    App.unblockUI();
                    window.location = response;
                },
                error: function () {
                    console.log('%c Error create download file', 'background: red; color: white; display: block;');
                    App.unblockUI();
                }
            });
        });

        //refresh datatable if datatable exist
        function refreshDatatable() {
            if ($.fn.DataTable.isDataTable($table)) {
                $.fn.dataTable.Api($table).draw();
            }
        }

        var properties = {
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "paging": true, //Enable or disable table pagination.
            "ajax": { //Load data for the table's content from an Ajax source
                type: 'POST',
                url: $table.data('route_datatable'),
                data: function (data) {
                    data._token = App.csrf;
                    data.method = $table.data('route_method');
                    data.cols = columns;
                    data.filters = findFilters(filtersContainer);
                    return data;
                },
                beforeSend: function (jqXHR) {
                    //Show full-page loader if blocking loader is not specified
                    xhrDataTablePool.push(jqXHR);
                },
                complete: function (jqXHR) {
                    //Hide full-page loader
                    var i = xhrDataTablePool.indexOf(jqXHR);   //  get index for current connection completed
                    if (i > -1) xhrDataTablePool.splice(i, 1); //  removes from list by index
                },
                dataSrc: function (json) {
                    if (json.data.length > 0) {
                        var colIndex = Array.apply(null, Array(json.data[0].length)).map(Number.prototype.valueOf, 0);

                        $.each(json.data, function () {
                            $(this).each(function (i, v) {
                                colIndex[i] = (v != null) ? colIndex[i] + 1 : colIndex[i];
                            });
                        });

                        $.each(colIndex, function (i, v) {
                            try {
                                $.fn.DataTable.Api($table).column(i).visible(v == 0 ? false : true);
                            } catch (error) {
                                console.log('%c' + error, 'background: #222; color: #bada55');
                            }
                        });
                    }

                    return json.data;
                }
            }
        };

        App.initDatatable($table, properties);
    }

    // Toggle Able checkboxes
    // Will handle the uri feature of any property_toggle asset
    function initToggleAbleCheckBoxes() {
        $('body').on('click', 'a[data-property_toggle]', function (e) {
            e.preventDefault();

            makeToggleableCall($(this))
        });
    }

    // Handle the Portlet Nav bar
    function handlePortletNavs() {
        $('.portlet-nav li.nav-item a.label').click(function (e) {
            var navItem = $(this).closest('li');
            var allItems = navItem.closest('ul').children();

            // Close all if the current item isn't opened already
            if (!navItem.hasClass('open')) {
                allItems.removeClass('open').trigger('closePortletNav');
                navItem.addClass('open').find('.content').focus();
            } else {
                navItem.removeClass('open').trigger('closePortletNav');
            }
        });

        // Close the popup when clicked anywhere else on the screen
        $('.portlet-nav li.nav-item .content').on('blur', function (e) {
            var navItem = $(this).closest('li');
            var allItems = navItem.closest('ul').children();

            // If we just hangout an extra tick, we'll find out which element got focus
            // this will allow us to decide if we want to close the window or not
            setTimeout(function () {
                if ($(document.activeElement).is('body')) {
                    allItems.filter('.open').removeClass('open').trigger('closePortletNav');
                }
            }, 1);
        })
    }

    function handlePortletReportBookmarks() {
        if ($('#view_bookmark').length) {

            // Catches the select bookmark event and activates the relevent filters
            $('#select_bookmark').on('change', function () {
                var $this = $(this);

                if ($this.val() != $this.attr('data-selected_filter')) {
                    $this.closest('form').submit();
                }
            })
        }
    }

    function handleNotifications(first_poll, error_counter) {

        // if no errors are passed then default value is 0
        error_counter = error_counter ? error_counter : 0;

        //if there where too many errors, stop the xhr requests
        if (error_counter >= 3) {
            return toastr.error('Too many notifications errors');
        }

        // check if we on page that contains notifications
        if ($('#header_notification_bar').length) {
            first_poll = (first_poll === true);
            var notifications_id = [];
            // get the ids of unread notifications
            $('#header_notification_bar .dropdown-menu-list a').each(function () {
                notifications_id.push($(this).attr('data-notification-id'));
            });

            $.ajax({
                type: 'GET',
                url: '/reports/alerts/notifications-list',
                data: {
                    ids: notifications_id,
                    'first_poll': first_poll
                },
                success: function (response) {
                    if (response !== '' && response.status !== 0) {
                        $('#header_notification_bar').empty();
                        $('#header_notification_bar').append(response);
                    }
                    handleNotifications();
                },
                error: function () {
                    error_counter++;
                    handleNotifications(false, error_counter);
                }
            });
        }
    }

    function makeToggleableCall(el) {
        var url = el.data('property_toggle');

        App.blockUI({"animate": true});
        $.getJSON(url)
            .done(function (response) {
                if (response) {
                    el.find('.toggle_checkbox').attr('data-toggle_state', function () {
                        return (response.toggleState == 1) ? 'checked' : '';
                    });

                    if (response.desc) {
                        toastr.success((response.desc.length > 0) ? response.desc : 'Value changed');
                    }
                }
            })
            .fail(function (res) {
                console.log('Ajax button error:', res);
            })
            .always(function () {
                el.find('i.fa-refresh.fa-spin').remove();
                App.unblockUI();
            })
    }

    /**
     * Toggle all toggleable options to "active" state
     *
     * @param selector
     * @param activate
     */
    function toggleAllCheckBoxes(selector, activate) {

        // Reset default to true
        activate = typeof activate !== 'undefined' ? activate : true;
        var toggleState = !Boolean(activate) ? 'checked' : '';

        $(selector + ' a[data-property_toggle]').each(function () {
            var $item = $(this);
            if ($item.find('span').attr('data-toggle_state') == toggleState) {
                makeToggleableCall($item);
            }
        });
    }

    /**
     * reset table select all checkboxes, called whenever table is refreshed
     * @param table
     */
    function refreshTableCheckboxes(table) {
        $(table).find('.group-checkable').removeAttr('checked');
        $.uniform.update('.mt-checkbox div span input[type="checkbox"]');
    }

    /**
     * refresh filters in datatable filter container
     * if container changed
     *
     * @param _class - container class
     * @param isFirstRun - boolean
     * @returns {{}} - object
     */
    function findFilters(_class, isFirstRun) {

        var filters = isFirstRun ? getFilterParamsFromUrl() : {};

        $(_class).find('input, select').each(function () {
            var name = $(this).attr('name');
            var value = $(this).val();
            if (name != undefined && name != '_token' && value !== null) {
                if (name.indexOf['['] || name.indexOf(']')) {
                    name = name.replace('[', '');
                    name = name.replace(']', '');
                }

                filters[name] = value;
            }
        });

        setUrlState(filters);

        return filters;
    }

    /**
     * Save Url State
     *
     * @param filters
     */
    function setUrlState(urlParams) {
        var url = buildUrlWithParams(urlParams);
        window.history.pushState(urlParams, 'title', url);
    }

    /**
     * Build Url With Params
     *
     * @param filters
     * @returns {string}
     */
    function buildUrlWithParams(params) {

        var ret = [];
        for (var key in params) {
            ret.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
        }

        return window.location.pathname + ret.length ? ('?' + ret.join('&')) : '';
    }

    /**
     * Get Filters Params From Url
     *
     * @returns {{}} - object
     */
    function getFilterParamsFromUrl() {

        var filters = {};
        var url_params = window.location.href.substring(window.location.href.indexOf('?') + 1, window.location.href.length);

        url_params = url_params.split('&');

        $.each(url_params, function (i, v) {
            var values = v.split('=');
            if (values.length == 2) {
                filters[values[0]] = values[1];
            }
        });

        return filters;
    }

    /**
     * init select2
     */
    function initSelects2() {
        if ($.fn.select2 != undefined) {
            buildSelect2AjaxParams();

            $('.select2').each(function () {
                initSelect2Element($(this));
            });
        }
    }

    /**
     * build select2 params
     * to be sent using ajax call
     */
    function buildSelect2AjaxParams(el) {
        // Bail if al is not a valid element
        if (el == undefined || el.length == 0) {
            return {};
        }

        var selectParams = {};

        el.each(function () {
            $.each(this.attributes, function () {
                if (this.name.startsWith('data-')) {
                    if (this.name.indexOf('data-dependency_selector') != -1) {
                        if (selectParams.dependency == undefined) {
                            selectParams.dependency = [];
                        }

                        selectParams.dependency.push({
                            value: $(this.value).val(),
                            selector: this.value
                        });
                    }
                    else if(this.name.indexOf('data-param-selector') != -1){
                        if (selectParams.param == undefined) {
                            selectParams.param = [];
                        }

                        selectParams.param.push({
                            value: $(this.value).val(),
                            selector: this.value
                        });
                    }
                    else {
                        selectParams[this.name.substring(5)] = this.value;
                    }
                }
            });
        });

        return selectParams;
    }

    /**
     * Init Select2 Element
     * Will accept an element and activates it
     *
     * events:
     * select2:select
     * select2:close
     * select2:open
     * select2:unselect
     *
     * @param element
     */
    function initSelect2Element(element) {
        var allowClear = !element.data('force-selection');

        if (element.hasClass('select2-ajax')) {
            initSelect2AjaxToSelect(element);
        } else {
            var params = {
                placeholder: ".."
            };

            params['allowClear'] = allowClear;
            params.closeOnSelect = !element.hasClass('select2-multiple');
            element.select2(params);
        }

        // When nothing is checked lets reset the select
        if (element.find('option[selected="selected"]').length == 0) {
            if (allowClear) {
                element.select2('val', -1);
            }

        } else if (!element.hasClass('select2-multiple')) {
            try {
                element.select2('val', element.find('option[selected="selected"]').val());
            } catch (err) {
                console.log(err);
            }
        }

        // adds sortable to select2 multi select
        if (element.hasClass('select2-multiple') && element.hasClass('sortable')) {
            $('ul.select2-selection__rendered').sortable({
                containment: 'parent',
                update: function () {
                    var parent = element.parent('div');
                    changeOrders($(this), parent.find('select.select2'));
                }
            });
        }

        /**
         * Init tags input to multi select
         */
        if (element.hasClass('select2-multiple') && element.data('role') == 'tagsinput') {
            var parent = element.parent('div');

            //append sortable options and css style
            var appendTagsInput = function () {
                var ul = parent.find('ul.select2-selection__rendered');
                ul.sortable({
                    containment: 'parent',
                    update: function () {
                        var parent = element.parent('div');
                        changeOrders($(this), parent.find('select.select2'));
                    }
                });

                var li = ul.find('li.select2-selection__choice');
                li.addClass('btn').addClass('green-sharp');
                var x = ul.find('span.select2-selection__choice__remove');
                x.addClass('font-white');
            }

            //set interval - if select2 loading to this select
            var select2InitInterval = setInterval(function () {
                if (parent.find('ul.select2-selection__rendered')) {
                    clearInterval(select2InitInterval);
                    appendTagsInput();
                }
            }, 1000);
        }

        // sort select by changes
        function changeOrders(element, select) {
            //get all select2 elements
            var li = element.find('li.select2-selection__choice');
            //get all select options
            var options = select.find('option');
            //remove all select options
            select.find('option').remove();

            //insert to select options by order
            var insertOptionHtml = function (title) {
                $.each(options, function () {
                    var option = $(this);
                    if (option.html() == title) {
                        select.append(option);
                        //remove inserted option from options array
                        options.splice($.inArray(this, options), 1);
                    }
                });
            }

            //insert new options
            li.each(function () {
                var title = $(this).attr('title');
                insertOptionHtml(title);
            });

            //insert not choice options
            $.each(options, function () {
                select.append($(this));
            })
        }

        //if select have class .select2-ajax init ajax
        function initSelect2AjaxToSelect(element) {
            // Pulls the value name for when the value id is passed in the view
            // this is a requirement when using select2 ajax
            if (element.data('value')) {
                var $option = $('<option/>');
                var params = buildSelect2AjaxParams(element);
                params.getCurrentValue = true;
                $.ajax({
                    url: element.data('api_url'),
                    method: 'GET',
                    data: params,
                    success: function (response) {
                        if (response.status) {
                            if (Array.isArray(response.data)) { //to multi select
                                var values = []; //values to multi select
                                $.each(response.data, function (i, v) { //add options to select
                                    $option = $('<option/>');
                                    $option.text(v.text).val(v.id);
                                    element.append($option);
                                    element.val(v.id).trigger('change');
                                    values[i] = v.id;
                                });

                                //add options to multi select
                                element.select2('val', values);
                            } else { //simple select
                                if (response && response.data && response.data[0] && response.data[0].text) {
                                    $option.text(response.data[0].text).val(element.data('value'));
                                    element.append($option);
                                    element.val(element.data('value')).trigger('change');
                                }
                            }

                            filterBreadCrumbs();
                        }
                    }
                });
            }

            // Init the ajax search
            var select2Params = {
                placeholder: "..",
                allowClear: true,
                ajax: {
                    url: element.data('api_url'),
                    dataType: 'json',
                    data: function (params) {
                        var data = buildSelect2AjaxParams(element);

                        // Used to trigger an alert when the required
                        // dependencies are not met
                        var dependencyFlag = true;

                        data.query = params.term;

                        // Validate dependencies
                        // If there are any
                        if (data.dependency) {
                            var dependencyErrors = [];
                            $.each(data.dependency, function (key, item) {
                                if ((item.value === undefined || item.value === null) && dependencyFlag) {
                                    // Stop the ajax call and through an alert
                                    var selectorName = $(item.selector).closest('.form-group')
                                        .find('label').html().trim();
                                    //
                                    dependencyErrors.push(selectorName ? selectorName : item.selector);
                                }
                            });

                            // there are items inside the dependencyErrors array so lets toast a message
                            // and reset the flag
                            if (dependencyErrors.length) {
                                toastr.info('Required fields: <br>' + dependencyErrors.join('<br>'));
                                data = false;
                                dependencyFlag = false;
                            }
                        }

                        return data;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.data, function (item) {
                                return {
                                    text: item.text,
                                    id: item.id
                                };
                            })
                        };
                    }
                },
                minimumInputLength: function () {
                    return element.data('min_input');
                }
            };
            select2Params.closeOnSelect = !element.hasClass('select2-multiple');

            element.select2(select2Params);
        }
    }

    /** encode string **/
    function encodedStr(string) {
        var entity = string.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
            return '&#' + i.charCodeAt(0) + ';';
        });
        return entity;
    }

    // runs callback functions set by App.addResponsiveHandler().
    var _runResizeHandlers = function () {
        // reinitialize other subscribed elements
        for (var i = 0; i < resizeHandlers.length; i++) {
            var each = resizeHandlers[i];
            each.call();
        }
    };

    function openNewTab(url) {
        var redirect = window.open(url, '', {target: '_blank'});
        redirect.location;
    }

    function openAjaxModal(url) {
        $("#ajax-modal").modal({remote: url});
    }

    // handle the layout reinitialization on window resize
    var handleOnResize = function () {
        var resize;
        if (isIE8) {
            var currheight;
            $(window).resize(function () {
                if (currheight == document.documentElement.clientHeight) {
                    return; //quite event since only body resized not window.
                }
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    _runResizeHandlers();
                }, 50); // wait 50ms until window resize finishes.
                currheight = document.documentElement.clientHeight; // store last body client height
            });
        } else {
            $(window).resize(function () {
                if (resize) {
                    clearTimeout(resize);
                }
                resize = setTimeout(function () {
                    _runResizeHandlers();
                }, 50); // wait 50ms until window resize finishes.
            });
        }
    };

    // Handles portlet tools & actions
    var handlePortletTools = function () {
        // handle portlet remove
        $('body').on('click', '.portlet > .portlet-title > .tools > a.remove', function (e) {
            e.preventDefault();
            var portlet = $(this).closest(".portlet");

            if ($('body').hasClass('page-portlet-fullscreen')) {
                $('body').removeClass('page-portlet-fullscreen');
            }

            portlet.find('.portlet-title .fullscreen').tooltip('destroy');
            portlet.find('.portlet-title > .tools > .reload').tooltip('destroy');
            portlet.find('.portlet-title > .tools > .remove').tooltip('destroy');
            portlet.find('.portlet-title > .tools > .config').tooltip('destroy');
            portlet.find('.portlet-title > .tools > .collapse, .portlet > .portlet-title > .tools > .expand').tooltip('destroy');

            portlet.remove();
        });

        // handle portlet fullscreen
        $('body').on('click', '.portlet > .portlet-title .fullscreen', function (e) {
            e.preventDefault();
            var portlet = $(this).closest(".portlet");
            if (portlet.hasClass('portlet-fullscreen')) {
                $(this).removeClass('on');
                portlet.removeClass('portlet-fullscreen');
                $('body').removeClass('page-portlet-fullscreen');
                portlet.children('.portlet-body').css('height', 'auto');
            } else {
                var height = App.getViewPort().height -
                    portlet.children('.portlet-title').outerHeight() -
                    parseInt(portlet.children('.portlet-body').css('padding-top')) -
                    parseInt(portlet.children('.portlet-body').css('padding-bottom'));

                $(this).addClass('on');
                portlet.addClass('portlet-fullscreen');
                $('body').addClass('page-portlet-fullscreen');
                portlet.children('.portlet-body').css('height', height);
            }
        });

        $('body').on('click', '.portlet > .portlet-title > .tools > a.reload', function (e) {
            e.preventDefault();
            var el = $(this).closest(".portlet").children(".portlet-body");
            var url = $(this).attr("data-url");
            var error = $(this).attr("data-error-display");
            if (url) {
                App.blockUI({
                    target: el,
                    animate: true,
                    overlayColor: 'none'
                });
                $.ajax({
                    type: "GET",
                    cache: false,
                    url: url,
                    dataType: "html",
                    success: function (res) {
                        App.unblockUI(el);
                        el.html(res);
                        App.initAjax(); // reinitialize elements & plugins for newly loaded content
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        App.unblockUI(el);
                        var msg = 'Error on reloading the content. Please check your connection and try again.';
                        if (error == "toastr" && toastr) {
                            toastr.error(msg);
                        } else if (error == "notific8" && $.notific8) {
                            $.notific8('zindex', 11500);
                            $.notific8(msg, {
                                theme: 'ruby',
                                life: 3000
                            });
                        } else {
                            alert(msg);
                        }
                    }
                });
            } else {
                // for demo purpose
                App.blockUI({
                    target: el,
                    animate: true,
                    overlayColor: 'none'
                });
                window.setTimeout(function () {
                    App.unblockUI(el);
                }, 1000);
            }
        });

        //load ajax data on page init
        $('.portlet .portlet-title a.reload[data-load="true"]').click();

        $('body').on('click', '.portlet > .portlet-title > .tools > .collapse, .portlet .portlet-title > .tools > .expand', function (e) {
            e.preventDefault();
            var el = $(this).closest(".portlet").children(".portlet-body");
            if ($(this).hasClass("collapse")) {
                $(this).removeClass("collapse").addClass("expand");
                el.slideUp(200);
            } else {
                $(this).removeClass("expand").addClass("collapse");
                el.slideDown(200);
            }
        });
    };

    // Handles custom checkboxes & radios using jQuery Uniform plugin
    var handleUniform = function () {
        if (!$().uniform) {
            console.log('no uniform js');
            return;
        }
        var test = $("input[type=checkbox]:not(.toggle, .md-check, .md-radiobtn, .make-switch, .icheck), input[type=radio]:not(.toggle, .md-check, .md-radiobtn, .star, .make-switch, .icheck)");
        if (test.size() > 0) {
            test.each(function () {
                if ($(this).parents(".checker").size() === 0) {
                    $(this).show();
                    $(this).uniform();
                }
            });
        }

        $('input[type=file]').on('change', function () {
            var $this = $(this);
            $this.closest('.fileinput-button').find('span').text('File: ' + $this.val());
        });
    };

    // Handlesmaterial design checkboxes
    var handleMaterialDesign = function () {

        // Material design ckeckbox and radio effects
        $('body').on('click', '.md-checkbox > label, .md-radio > label', function () {
            var the = $(this);
            // find the first span which is our circle/bubble
            var el = $(this).children('span:first-child');

            // add the bubble class (we do this so it doesnt show on page load)
            el.addClass('inc');

            // clone it
            var newone = el.clone(true);

            // add the cloned version before our original
            el.before(newone);

            // remove the original so that it is ready to run on next click
            $("." + el.attr("class") + ":last", the).remove();
        });

        if ($('body').hasClass('page-md')) {
            // Material design click effect
            // credit where credit's due; http://thecodeplayer.com/walkthrough/ripple-click-effect-google-material-design
            var element, circle, d, x, y;
            $('body').on('click', 'a.btn, button.btn, input.btn, label.btn', function (e) {
                element = $(this);

                if (element.find(".md-click-circle").length == 0) {
                    element.prepend("<span class='md-click-circle'></span>");
                }

                circle = element.find(".md-click-circle");
                circle.removeClass("md-click-animate");

                if (!circle.height() && !circle.width()) {
                    d = Math.max(element.outerWidth(), element.outerHeight());
                    circle.css({height: d, width: d});
                }

                x = e.pageX - element.offset().left - circle.width() / 2;
                y = e.pageY - element.offset().top - circle.height() / 2;

                circle.css({top: y + 'px', left: x + 'px'}).addClass("md-click-animate");

                setTimeout(function () {
                    circle.remove();
                }, 1000);
            });
        }

        // Floating labels
        var handleInput = function (el) {
            if (el.val() != "") {
                el.addClass('edited');
            } else {
                el.removeClass('edited');
            }
        };

        $('body').on('keydown', '.form-md-floating-label .form-control', function (e) {
            handleInput($(this));
        });
        $('body').on('blur', '.form-md-floating-label .form-control', function (e) {
            handleInput($(this));
        });

        $('.form-md-floating-label .form-control').each(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('edited');
            }
        });
    };

    // Handles custom checkboxes & radios using jQuery iCheck plugin
    var handleiCheck = function () {
        if (!$().iCheck) {
            return;
        }

        $('.icheck').each(function () {
            var checkboxClass = $(this).attr('data-checkbox') ? $(this).attr('data-checkbox') : 'icheckbox_minimal-grey';
            var radioClass = $(this).attr('data-radio') ? $(this).attr('data-radio') : 'iradio_minimal-grey';

            if (checkboxClass.indexOf('_line') > -1 || radioClass.indexOf('_line') > -1) {
                $(this).iCheck({
                    checkboxClass: checkboxClass,
                    radioClass: radioClass,
                    insert: '<div class="icheck_line-icon"></div>' + $(this).attr("data-label")
                });
            } else {
                $(this).iCheck({
                    checkboxClass: checkboxClass,
                    radioClass: radioClass
                });
            }
        });
    };

    // Handles Bootstrap switches
    var handleBootstrapSwitch = function () {
        if (!$().bootstrapSwitch) {
            return;
        }
        $('.make-switch').bootstrapSwitch();
    };

    // Handles Bootstrap confirmations
    var handleBootstrapConfirmation = function () {
        if (!$().confirmation) {
            return;
        }
        $('[data-toggle=confirmation]').confirmation({
            container: 'body',
            btnOkClass: 'btn btn-sm btn-success',
            btnCancelClass: 'btn btn-sm btn-danger'
        });
    };

    // Handles Bootstrap Accordions.
    var handleAccordions = function () {
        $('body').on('shown.bs.collapse', '.accordion.scrollable', function (e) {
            App.scrollTo($(e.target));
        });
    };

    // Handles Bootstrap Tabs.
    var handleTabs = function () {
        //activate tab if tab id provided in the URL
        if (location.hash) {
            var tabid = encodeURI(location.hash.substr(1));
            $('a[href="#' + tabid + '"]').parents('.tab-pane:hidden').each(function () {
                var tabid = $(this).attr("id");
                $('a[href="#' + tabid + '"]').click();
            });
            $('a[href="#' + tabid + '"]').click();
        }

        if ($().tabdrop) {
            $('.tabbable-tabdrop .nav-pills, .tabbable-tabdrop .nav-tabs').tabdrop({
                text: '<i class="fa fa-ellipsis-v"></i>&nbsp;<i class="fa fa-angle-down"></i>'
            });
        }
    };

    // Handles Bootstrap Modals.
    var handleModals = function () {
        var body = $('body');
        // fix stackable modal issue: when 2 or more modals opened, closing one of modal will remove .modal-open class.
        body.on('hide.bs.modal', function () {
            if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') === false) {
                $('html').addClass('modal-open');
            } else if ($('.modal:visible').size() <= 1) {
                $('html').removeClass('modal-open');
                $('.modal-content').html(''); // clean modal contents
            }
        });

        body.on('click', '.modal button.expand-modal', function () {
            $(this).closest('.modal-dialog').toggleClass('full-width');
        });

        // fix page scrollbars issue
        body.on('show.bs.modal', '.modal', function () {
            if ($(this).hasClass("modal-scroll")) {
                body.addClass("modal-open-noscroll");
            }
        });

        // fix page scrollbars issue
        body.on('hide.bs.modal', '.modal', function () {
            body.removeClass("modal-open-noscroll");
        });

        // remove ajax content and remove cache on modal closed
        body.on('hidden.bs.modal', '.modal:not(.modal-cached)', function () {
            $('.tooltips').tooltip('hide');
            $(this).removeData('bs.modal');
        });
    };

    // Handles Bootstrap Tooltips.
    var handleTooltips = function () {
        // global tooltips
        if ($('.tooltips').length > 0) {
            $('.tooltips').tooltip();
        }

        // portlet tooltips
        $('.portlet > .portlet-title .fullscreen').tooltip({
            container: 'body',
            title: 'Fullscreen'
        });
        $('.portlet > .portlet-title > .tools > .reload').tooltip({
            container: 'body',
            title: 'Reload'
        });
        $('.portlet > .portlet-title > .tools > .remove').tooltip({
            container: 'body',
            title: 'Remove'
        });
        $('.portlet > .portlet-title > .tools > .config').tooltip({
            container: 'body',
            title: 'Settings'
        });
        $('.portlet > .portlet-title > .tools > .collapse, .portlet > .portlet-title > .tools > .expand').tooltip({
            container: 'body',
            title: 'Collapse/Expand'
        });
    };

    // Handles Bootstrap Dropdowns
    var handleDropdowns = function () {
        /*
         Hold dropdown on click
         */
        $('body').on('click', '.dropdown-menu.hold-on-click', function (e) {
            e.stopPropagation();
        });
    };

    var handleAlerts = function () {
        $('body').on('click', '[data-close="alert"]', function (e) {
            $(this).parent('.alert').hide();
            $(this).closest('.note').hide();
            e.preventDefault();
        });

        $('body').on('click', '[data-close="note"]', function (e) {
            $(this).closest('.note').hide();
            e.preventDefault();
        });

        $('body').on('click', '[data-remove="note"]', function (e) {
            $(this).closest('.note').remove();
            e.preventDefault();
        });
    };

    // Handle Hover Dropdowns
    var handleDropdownHover = function () {
        $('[data-hover="dropdown"]').not('.hover-initialized').each(function () {
            $(this).dropdownHover();
            $(this).addClass('hover-initialized');
        });
    };

    // Handle textarea autosize
    var handleTextareaAutosize = function () {
        if (typeof(autosize) == "function") {
            autosize(document.querySelector('textarea.autosizeme'));
        }
    };

    // Handles Bootstrap Popovers

    // last popep popover
    var lastPopedPopover;

    var handlePopovers = function () {
        $('.popovers').popover();

        // close last displayed popover

        $(document).on('click.bs.popover.data-api', function (e) {
            if (lastPopedPopover) {
                lastPopedPopover.popover('hide');
            }
        });
    };

    // Handles scrollable contents using jQuery SlimScroll plugin.
    var handleScrollers = function () {
        App.initSlimScroll('.scroller');
    };

    // Handles Image Preview using jQuery Fancybox plugin
    var handleFancybox = function () {
        if (!jQuery.fancybox) {
            return;
        }

        if ($(".fancybox-button").size() > 0) {
            $(".fancybox-button").fancybox({
                groupAttr: 'data-rel',
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: true,
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });
        }
    };
    // Listen to submit on form inside modal which were flagged to be sent with ajax
    // using the ajax_submit attribute of the ajax-modal partial.
    var handleModalAjaxForm = function () {
        $(".modal-content").on("submit", ".ajax-submit form", function (e) {
            e.preventDefault();
            var formData = $(this).serializeArray();
            var url = $(this).attr('action');
            var method = $(this).attr('method');
            var successCallback = $(this).parents('.ajax-submit').attr('data-success');
            var errorCallback = $(this).parents('.ajax-submit').attr('data-error');

            if (url.length < 10) return true; // Make sure this is a valid url

            $.ajax({
                url: url,
                data: formData,
                type: method,
                success: function (resp) {
                    if (successCallback && typeof window[successCallback] === 'function') {
                        window[successCallback](resp, this.data);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (errorCallback && typeof window[errorCallback] === 'function') {
                        window[errorCallback](xhr, ajaxOptions, thrownError);
                    }
                }
            })
        })
    };

    // Listen to submit on form inside modal which were flagged to trigger an event
    // using the triggers event class and event data attribute
    var handleModalTriggersEvent = function () {
        $(".modal-content").on('submit', '.triggers-event form', function (e) {
            e.preventDefault();

            var event = $(this).parents('.triggers-event').data('event');

            $(document).trigger('modal-form-event-' + event, [this]);
        });
    };

    // Init the sweet alert plugin to listen to all sweetAlert data-targets
    var handleSweetAlert = function (callback) {

        // Make sure sweet alert was preloaded
        if (typeof(swal) != 'function') {
            return;
        }

        var configurationObject = {};

        // Respond to clicks on sweetAlert data target
        $("body").on("click", "[data-target='sweetAlert']", function () {
            //close previous open alerts
            swal.close();

            // Title and type are set for both ajax and non ajax alerts
            configurationObject.title = $(this).attr('data-alert_title');
            configurationObject.type = $(this).attr('data-alert_type');

            var href = $(this).attr('data-alert_href');

            //if href is provided we need to fetch the html before poping out the alert
            if (href) {
                $.ajax({
                    method: 'GET',
                    url: href,
                    success: function (resp) {
                        configurationObject.html = resp;
                        sweetAlert(configurationObject);
                    },
                    error: function () {
                        return swal('Warning', 'Server respond with an error!', 'error');
                    }
                });
            } else {
                configurationObject.text = $(this).attr('data-alert_text');
                sweetAlert(configurationObject);
            }
        });

        callback();
    };

    // Sweet Alert 2 JQuery plugin, documentation: https://limonte.github.io/sweetalert2/
    var sweetAlert = function (configurationObject) {

        // Create the default configuration object - see documentation for further configurations
        if (!configurationObject) {
            configurationObject = {
                title: '',
                text: '',
                type: 'info'
            };
        }

        // run after the modal is open
        // add support to select 2
        configurationObject.onOpen = function () {
            if ($('.swal2-modal .select2').length > 0) {
                $('.swal2-modal .select2').select2();
            }
        };

        swal(configurationObject);
    };

    // Detect adblockers only after sweet alert is ready, fuckAdBlock plugin is initiated by default
    var detectAdBlockers = function () {

        // Function called if AdBlock is detected
        var adBlockDetected = function () {
            sweetAlert({
                type: 'warning',
                title: 'Ad Blocker Is Enabled',
                text: 'Disable AdBlock for this domain to use this CMS.'
            });
        };

        if (typeof fuckAdBlock === 'undefined') {
            adBlockDetected();
        } else {
            fuckAdBlock.onDetected(adBlockDetected);
        }
    };

    // Handles counterup plugin wrapper
    var handleCounterup = function () {
        if (!$().counterUp) {
            return;
        }

        $("[data-counter='counterup']").counterUp({
            delay: 10,
            time: 1000
        });
    };

    // Handles Editable plugin wrapper
    var handleEditables = function () {
        if (!$().editable) {
            return;
        }

        $(".editable:not(.editable-ajax)").editable();
    };

    // Fix input placeholder issue for IE8 and IE9
    var handleFixInputPlaceholderForIE = function () {
        //fix html5 placeholder attribute for ie7 & ie8
        if (isIE8 || isIE9) { // ie8 & ie9
            // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
            $('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function () {
                var input = $(this);

                if (input.val() === '' && input.attr("placeholder") !== '') {
                    input.addClass("placeholder").val(input.attr('placeholder'));
                }

                input.focus(function () {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                input.blur(function () {
                    if (input.val() === '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    };

    // Handle Select2 DropDown
    var handleSelect2 = function () {
        if ($().select2) {
            $.fn.select2.defaults.set("theme", "bootstrap");
        }
    };

    // handle group element heights
    var handleHeight = function () {
        $('[data-auto-height]').each(function () {
            var parent = $(this);
            var items = $('[data-height]', parent);
            var height = 0;
            var mode = parent.attr('data-mode');
            var offset = parseInt(parent.attr('data-offset') ? parent.attr('data-offset') : 0);

            items.each(function () {
                if ($(this).attr('data-height') == "height") {
                    $(this).css('height', '');
                } else {
                    $(this).css('min-height', '');
                }

                var height_ = (mode == 'base-height' ? $(this).outerHeight() : $(this).outerHeight(true));
                if (height_ > height) {
                    height = height_;
                }
            });

            height = height + offset;

            items.each(function () {
                if ($(this).attr('data-height') == "height") {
                    $(this).css('height', height);
                } else {
                    $(this).css('min-height', height);
                }
            });

            if (parent.attr('data-related')) {
                $(parent.attr('data-related')).css('height', parent.height());
            }
        });
    };

    // Init ajax modal functionality
    // Init ajax modal functionality
    var initAjaxModal = function () {
        var ajaxModalEl = $('#ajax-modal');

        ajaxModalEl.on('click', '.submit-button', function () {
            validateModalForm(ajaxModalEl.find('form'));
            return false;
        });

        $(document).on('click.bs.modal.data-api', '[data-open="modal"],[data-toggle="modal"]', function (e) {
            var $this = $(this);
            if ($this.is('a')) e.preventDefault(); //if click to link dont refresh page
            if ($this.is('a.stackable')) return false; //if stackable dont run ajax in this modal

            $.ajax({
                url: $this.attr('href'),
                success: function (response) {
                    var modalElement = ajaxModalEl.find('.modal-content');
                    modalElement.empty();
                    modalElement.append(response);
                }
            });
        });
    };

    /**
     * validate modal form
     *
     * @param form
     */
    var validateModalForm = function (form) {
        var $form = $(form);
        var required = $form.find('[required]');
        var isValid = true;

        $.each(required, function () {
            var $this = $(this);
            var label = $this.data('required_name');

            if ($this.prop("tagName") == 'DIV') { //check if this nestable list
                if ($this.find('li').length == 0) { //check if this nestable list not empty
                    isValid = false;
                }
            } else if (!$this.val() || $this.val() == undefined) { //check required fields not empty
                isValid = false;
            }

            if (!isValid) {
                toastr.warning('This field ' + label + ' is required');

                return false;
            }
        });

        if (isValid) {
            $form.submit();
        }
    }

    /**
     *
     */
    function submitModalForm($modal) {
        //if modal exists form - event to submit button
        $modal.on('click', '.submit-button', function (e) {
            e.preventDefault();

            var $form = $modal.find('form');

            //todo html validation code
            // var allowCloseModal = $form.find('[name=close_modal]').val() != 0;
            // var isValidationRequired = $form.find('textarea[data-validate]')
            // var isValidationPassed = $form.find('valid').val() == 1;

            var settings = {
                type: 'POST',
                url: $form.attr('action'),
                data: $form.serialize(),
                success: function (response) {
                    if (response.status) {
                        toastr.success(response.desc);
                    } else {
                        // todo html validation
                        // if (!allowCloseModal) {
                        //     $modal.find('.diff-scss').html(response.desc);
                        //
                        //     return;
                        // }
                        toastr.error(response.desc);
                    }
                },
                error: function (error) {

                }
            };

            $.ajax(settings);
            $modal.modal('toggle');

            /*todo html validation code */
            // if(!isValidationRequired
            //     || isValidationPassed && isValidationRequired){
            // }

            // if (allowCloseModal) {
            // }
        });
    }

    // Init ajax modal functionality from js
    var initJSModal = function (id, content) {
        var $container = $('.modal-container');

        var $modal = $('<div class="modal fade" id="' + id + '" role="dialog" aria-hidden="true">' +
            '<div class="modal-dialog full-width">' +
            '<div class="modal-content"></div></div></div>');

        $modal.find('.modal-content').append(content);
        $container.append($modal);
        $modal.modal();

        submitModalForm($modal);
    };

    /**
     * Object code Mirror :
     *
     * two methods :
     * 1. Init codeMirror on all textarea
     * 2. Generate new codeMirror custom properties*
     */

    var codeMirror = {
        textAreaWatchers: [],

        initTextAreas: function () {
            var codeMirrorTextAreas = $('.codemirror');
            var that = this;
            if (codeMirrorTextAreas.length > 0 && typeof CodeMirror == 'function') {

                codeMirrorTextAreas.each(function (index, textArea) {

                    var lang = $(textArea).data('codemirror_lang');

                    if (lang == 'html') {
                        lang = 'htmlmix';
                    } else if (lang == 'scss') {
                        lang = 'sass';
                    } else if (lang == 'blade') {
                        lang = 'php';
                    }

                    var cm = CodeMirror.fromTextArea(textArea, {
                        mode: lang,
                        indentUnit: 4,
                        tabSize: 4,
                        lineNumbers: true,
                        foldGutter: {
                            rangeFinder: new CodeMirror.fold.combine(CodeMirror.fold.brace, CodeMirror.fold.comment)
                        },
                        gutters: ["CodeMirror-foldgutter", 'CodeMirror-foldgutter']
                    });

                    if (window.codeMirrorEditors) {
                        window.codeMirrorEditors.push(cm);
                    } else {
                        window.codeMirrorEditors = [cm];
                    }

                    var column = $(textArea).data('codemirror_column_wrap');
                    if (typeof column != 'undefined') {
                        //Cause of some unknown bug we need run it twice - fix me if you have time
                        cm.wrapParagraphsInRange(cm.posFromIndex(0), cm.posFromIndex(cm.posFromIndex(cm.getViewport.to)), {column: column});
                        cm.wrapParagraphsInRange(cm.posFromIndex(0), cm.posFromIndex(cm.posFromIndex(cm.getViewport.to)), {column: column});
                    }

                    that.textAreaWatchers[index] = cm;
                });
            }
        },

        //todo add destroy method that remove all textareas
        destroyTextAreas: function () {
            //todo add destroy method that remove all textareas
        },

        initOnRequest: function (textarea, props) {
            if (textarea && typeof props == "function") {
                CodeMirror.fromTextArea(textarea, props);
            }
        }
    };


    //add ajax stackable modal //currency 2 levels
    var initAjaxModalStackable = function () {
        var modalContainer = null; //find modal container
        $('#ajax-modal').on('shown.bs.modal', function () {
            modalContainer = $(this).parents('.modal-container');
        });

        // stackable modal have exists button with class 'stackbale'
        $('.stackable:not(.bound)').addClass('bound').on('click', function (event) {
            event.preventDefault();

            var $this = $(this);
            var id = $this.attr('href').replace('#', '');
            var route = $this.data('route');

            if ($this.data('param')) { // if this param exists add this parameter to route
                var value = $('.' + $this.data('param')).val();
                route += '&' + $this.data('param') + '=' + value;
            }

            // build modal
            var modal = $('<div class="modal fade" id="' + id + '" tabindex="-1" role="dialog" aria-hidden="true">' +
                '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                '</div></div></div>');

            if (!modalContainer) {
                // find parent model
                $.each($('#ajax-modal'), function () {
                    if ($(this).hasClass('in')) {
                        modalContainer = $(this).parents('.modal-container');
                    }
                });
            }

            if (modalContainer !== null && modalContainer !== undefined) {
                //add new modal to modal container
                modalContainer.append(modal);
            }

            //find modal content block
            var modalContent = modal.find('.modal-content');

            //get content
            $.ajax({
                url: route,
                success: function (response) {
                    if (response.status === 0 && response.desc) {
                        $('#' + id).modal('toggle');
                        toastr.error(response.desc);
                        return false;
                    }

                    if ($(response).find('.submit-button').length > 0) {
                        App.ajaxJSModal(id, response);
                    } else {
                        modalContent.append(response);
                    }
                }
            });

            //if this modal close, remove from modal container
            $('#' + id).on('hidden.bs.modal', function () {
                $(this).remove();
            });
        })

    };

    /**
     * Error modal
     *
     * @param text
     * @param title
     */
    var ajaxModal = function (text, title) {
        //init empty modal content
        var modalContainer = $('<div id="modal_iframe"><div class="modal-header"><button type="button" class="close" data-dismiss="modal">&times;</button>' +
            '<h4 class="modal-title">Modal Header</h4></div><div class="modal-body">' +
            '<iframe id="modal_iframe_content" style="width: 100%; min-height: 300px;"></iframe>' +
            '</div><div class="modal-footer">' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button></div>');

        var default_params = {
            title: title,
            container: text,
            button: 'Close'
        };

        //remove modal content if exists
        $('#modal_iframe').remove();
        //change title
        modalContainer.find('h4.modal-title').text(default_params.title);

        //change content
        var content = $('#ajax-modal').find('.modal-content');
        var iframe = modalContainer.find('iframe');
        var body = 'data:text/html;charset=utf-8,' + encodeURI(default_params.container);
        iframe.attr({'src': body});

        //connect to modal
        content.append(modalContainer);
        //show modal
        $('#ajax-modal').modal('show');
    };

    var initAjaxErrors = function () {
        $(document).ajaxError(function (request, status, error) {
            if (status.status == 500 || status.status == 405) {
                ajaxModal(status.responseText, status.status + ' ' + status.statusText);
            }
            // server responded with unauthorized, redirect to the login page
            // instead of showing the login page as the ajax result
            if (status.status === 401) {
                window.top.location = '/auth/login';
            }
        });
    };

    //init datatable filter
    var filterDataTable = function filterDataTable($table, selectFilter) {
        if ($.fn.DataTable.isDataTable($table)) {
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var column_id = parseInt(selectFilter.data('column_id'), 10);
                    var selctedValue = selectFilter.val();
                    var column = data[column_id] || 0; // use data filter column

                    return selctedValue === null || column == selctedValue;
                }
            );

            $.fn.dataTable.Api($table).draw();
        }
    };

    //init datatable

    var initDataTableProperties = function (tableId, properties) {
        var $table = $(tableId);

        //Default params
        var params = datatablesParams($table);

        //if table has additional params - add them
        if (typeof properties !== 'undefined') {
            $.extend(params, properties);
        }

        //if table has additional attr params - add them
        if (typeof $table.data() !== 'undefined') {
            var attr = $table.data();
            if ('onchange_submit' in attr) {
                //disable header in datatable
                attr['bFilter'] = false; // show|hide search box
                attr['bInfo'] = true; // show|hide info in bottom table
                attr['bLengthChange'] = true; // show|hide show entities
            }

            if ('search' in attr) { //search box
                attr['bFilter'] = $table.data('search');
            }

            if ('info' in attr) { //info in bottom
                attr['bInfo'] = $table.data('info');
            }

            if ('length' in attr) { //length views
                attr['bLengthChange'] = $table.data('length');
            }

            if ('sort' in attr) { // sort columns
                attr['bSort'] = $table.data('sort');
            }

            if ('blocking_loader' in attr) { // show the "processing" loader is on
                attr['processing'] = true;
            }

            if ('field_order' in attr) {
                attr['rowReorder'] = true;
            }

            $.extend(params, attr);
        }

        initDatatables(params);
        initSelect2InDatatables($table);
        App.initUniform();

        //if event search used - abort previous ajax
        //in datatable server side only
        $table.on('preXhr.dt', function () {
            killDataTableAjax();
        });

        /**
         * create parameters to datatable
         * if currentParams null use default params
         * if newParams null return currentParams only
         * if newParams not null override currents and adds new if exists
         *
         * @returns {*}
         */
        function datatablesParams($table) {
            return {
                "dom": "<'row'<'col-md-9 col-sm-12'pli><'col-md-3 col-sm-12'f<'table-group-actions" +
                " pull-right'>>r>t<'row'<'col-md-8 col-sm-12'><'col-md-4 col-sm-12'>>",
                "pagingType": "bootstrap_extended",
                "language": {
                    "aria": {
                        "sortAscending": ": activate to sort column ascending",
                        "sortDescending": ": activate to sort column descending"
                    },
                    "emptyTable": "No data available in table",
                    "info": "Showing _START_ to _END_ of _TOTAL_ records",
                    "infoEmpty": "No records found",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "lengthMenu": "Show _MENU_",
                    "search": "Search:",
                    "zeroRecords": "No matching records found",
                    "emptyTable": "No data available in table",
                    "paginate": {
                        "previous": "Prev",
                        "next": "Next",
                        "last": "Last",
                        "first": "First"
                    },
                    "sEmptyTable": "No Data"
                },
                "bStateSave": false, // save datatable state(pagination, sort, etc) in cookie.
                "lengthMenu": [
                    [10, 20, 50, 100, 200, 500],
                    [10, 20, 50, 100, 200, 500] // change per page values here
                ],
                "pageLength": 10,
                "mData": 0,
                "drawCallback": function (settings) { //Function that is called every time DataTables performs a draw.

                    //datatable refresh button
                    var refreshIcon = "#refresh-wrapper .icon-refresh";

                    //remove spinning after the datatable is refreshed
                    $table.closest(".dataTables_wrapper").find(refreshIcon).removeClass("fa-spin");

                    $table.find('.tooltips').tooltip();
                    refreshTableCheckboxes($table); // reset rows checkboxes

                    //adds a scroll to tables that have the data-scrollX
                    addHorizontalScroll($table);

                    App.initUniform();
                    if ($().editable) {
                        $('.editable.editable-ajax')
                            .editable({
                                ajaxOptions: {
                                    type: "GET",
                                    dataType: "json",
                                    sourceCache: 'false'
                                },
                                validate: function (value) {
                                    if ($.trim(value) == '') {
                                        return 'This field is required';
                                    }
                                },
                                params: function (params) {
                                    params.pk_column = ($(this).data('pk_column')) || null;
                                    return params;
                                },
                                success: function (response, newValue) {
                                },
                            });
                    }
                },
                createdRow: function (row, data, dataIndex) {
                    $(row).attr('id', dataIndex);
                },
                "fnRowCallback": function (nRow) {
                    $(nRow).on('mousedown', function (e) {
                        if (e.button == 2) {
                            e.preventDefault();
                            //TODO create context menu
                            console.log('Right mouse button!');
                            return false;
                        }
                        return true;
                    });
                }
            };

            function addHorizontalScroll($table) {
                if ($table.attr('data-scrollX')) {
                    $table.parent('.dataTables_wrapper').addClass('x-scrollable');
                }
            }
        }

        //init datatable
        function initDatatables(params) {
            $table.dataTable(params);

            // if table have data attribute row-reorder - init datatable row reordering
            rowReordering();
        }

        //init datatable filter (not server side)
        //select only
        var selectFilter  = $('.filter_select_datatable'),
            select2Filter = selectFilter.closest('.filter_select2');
        if (selectFilter) {
            //if filter visible add select2 and position -1
            if (select2Filter.is(':visible')) {
                select2Filter.select2();
                select2Filter.select2('val', -1);
            }

            //add event change
            selectFilter.on('change', function () {
                filterDataTable($table, selectFilter);
            });
        }

        //init select2 in datatable
        function initSelect2InDatatables($table) {
            $table.find('.search-in-table select').select2();
        }

        //canceled ajax connections
        function killDataTableAjax() {
            $.each(xhrDataTablePool, function (i, jqXHR) {   //  cycle through list of recorded connection
                jqXHR.abort();  //  aborts connection
                xhrDataTablePool.splice(i, 1); //  removes from list by index
            });
        }

        //datatable init row reordering
        //event after drag and drop table row
        // TODO ajax and server side methods
        function rowReordering() {
            if ($table.data('field_order_url')) {
                $($table).on('row-reorder.dt', function (event, diff) {
                    var rows = [];

                    $.each(diff, function () {
                        rows.push({
                            priority: $(this.newData).html(),
                            itemId: $(this.oldData).attr('item_id')
                        });
                    });

                    $.ajax({
                        'type': 'POST',
                        'data': {
                            _token: App.csrf,
                            rows: rows
                        },
                        'url': $table.data('field_order_url'),
                        'success': function (response) {
                            toastr.success('Orders changed successful!');
                        },
                        'error': function (msg) {
                            toastr.error('Orders not saved, try again later');
                        }
                    })

                });
            }
        }
    };

    var filterBreadCrumbs = function () {
        // Report visualisation bread crumbs
        if ($('#filters_form').length) {
            this.filtersForm = $('#filters_form');
            this.breadCrumbsContainer = $('.filter-bread-crumbs');
            this.triggeringElSelectors = [
                '.select_where_column_name',
                'select[name="group_by[]"]',
                '.select_having_column_name',
                'select[name="order_by[key][]"]',
                'input[name="date_range[value]"]'
            ];
            this.clearFilterBreadCrumbs = function () {
                this.breadCrumbsContainer.empty();
            };
            this.addFilterBreadCrumb = function (name, groupName) {
                var groupLabel = '<span class="label label-default" data-group="{groupName}"> {groupName}: <span class="filter-list"></span></span> ',
                    filterLabel = ' <span class="badge badge-info"> {title} </span>';

                var breadCrumbsGroupEl = $('.filter-bread-crumbs [data-group="' + groupName + '"]');
                if (breadCrumbsGroupEl.length === 0) {
                    this.breadCrumbsContainer.append(groupLabel.replace(/{groupName}/g, groupName));
                    breadCrumbsGroupEl = $('.filter-bread-crumbs [data-group="' + groupName + '"]');
                }

                breadCrumbsGroupEl.find('.filter-list').append(filterLabel.replace('{title}', name));
            };
            this.updateFilterBreadCrumbs = function () {
                var self = this;
                self.clearFilterBreadCrumbs();
                for (var i = 0; i < self.triggeringElSelectors.length; i++) {
                    if ($(self.triggeringElSelectors[i]).is('select') && $(self.triggeringElSelectors[i]).select2('data').length) {
                        $(self.triggeringElSelectors[i]).each(function (key, option) {
                            self.addFilterBreadCrumb(
                                $(option).select2('data').length && typeof $(option).select2('data')[0] !== 'undefined'
                                    ? $(option).select2('data')[0]['text']
                                    : '',
                                $(option).closest('[data-group]').attr('data-group')
                            );
                        });
                    } else if ($(this.triggeringElSelectors[i]).is('input') && $(this.triggeringElSelectors[i]).val().length) {
                        this.addFilterBreadCrumb(
                            $(this.triggeringElSelectors[i]).val(),
                            $(this.triggeringElSelectors[i]).closest('[data-group]').attr('data-group')
                        );
                    }
                }
            };
            this.init = function () {
                this.filtersForm.on('change', this.triggeringElSelectors.join(', '), this.updateFilterBreadCrumbs.bind(this));
                this.filtersForm.on('closePortletNav', '.portlet-nav li.nav-item', this.updateFilterBreadCrumbs.bind(this));
                this.updateFilterBreadCrumbs();
            };
            return this.init();
        }
    }

    /**
     * Set size by element around iframe
     *
     * Currently args support three params:
     * args {width, height, element}
     *
     * element - class or other element to get size
     */
    var iframeWindowSize = function (args) {
        var argsObj = args || {};
        var $element = ('element' in argsObj) ? $(argsObj.element) : $('.portlet');
        var widthEnd = ('width' in argsObj) ? argsObj.width : $element.width();
        var heightEnd = ('height' in argsObj) ? argsObj.height : ($element.height() > 700 ? $element.height() : 700);

        $('.iframe-file-manager').css({
            width: widthEnd,
            height: heightEnd
        });
    }

    /*
     * Use plugin jquery.mjs.nestedSortable
     */
    function initNestedList(id) {
        var $nested = $('#' + id);
        if (!id) {
            $nested = $('.sortable');
        }

        if ($nested.length <= 0 || $nested.hasClass('nested-list')) {
            return false;
        }

        var actions = $('.form-actions');
        var $form = actions.find('form');
        var $input = $form.find('input[name="list"]');
        var submit = actions.find('button[data-form_class]');
        var attributes = $nested.parent('.nestable-lists').data();
        var settings = {};
        var type = $nested.parent('.nestable-lists').data('type');
        var inputSaveTree = undefined;
        var nestedChange = undefined;

        if (attributes) {
            inputSaveTree = attributes.save_tree;
            nestedChange = attributes.nested_change;
        }

        if (attributes) {
            $.each(attributes, function (i, v) {
                var key = topApp.string['toCamel'](i);
                settings[key] = v;
            })
        }

        var setChangedMode = function () {
            submit.prop('disabled', false);
            submit.on('click', function () {
                var serialize = $nested.nestedSortable('toHierarchy');
                if (type == 'page') {
                    $input.val(JSON.stringify(getPagesInfoById(serialize)));
                } else {
                    $input.val(JSON.stringify(serialize));
                }

                $form.submit();
            });
        };

        /**
         * Return one level object of all the pages in the following order:
         * { "the_page_id": { parent_id: 2, priority: 1}....
         *
         * @returns {{}}
         */
        var getPagesInfoById = function (pages_order) {
            var pages_by_id = {};
            iterateRecursiveOverPages(pages_by_id, pages_order, null);
            return pages_by_id;
        };

        var iterateRecursiveOverPages = function (pages_by_id, pages_order, parent) {
            var order = 1;
            for (var i = 0; i < pages_order.length; i++) {
                var page = pages_order[i];
                pages_by_id[page.id] = {
                    parent_id: parent,
                    priority: order++,
                };
                if (page.children) {
                    iterateRecursiveOverPages(pages_by_id, page.children, page.id)
                }
            }
        };

        var insertManualOrder = function (name, value) {
            if (name && value) {
                var $input = $('[name="' + name + '"]');
                $input.val(value);
            }
        };

        var defaultParams = {
            forcePlaceholderSize: true,
            handle: 'div',
            helper: 'clone',
            items: 'li', // items in parentRoot (ol)
            opacity: .6, // draggable opacity
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 60,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 4, // max level - max 6
            protectRoot: false, // if change child to parent
            isTree: true, // is tree or list
            expandOnHover: 700,
            startCollapsed: true,
            disableNesting: 'no-nest',
            // stop: setChangedMode, // after change event
            update: function () {
                var nestedId = $(this).attr('id');
                if (inputSaveTree && nestedChange && nestedId == nestedChange) {
                    var serialize = $(this).nestedSortable('toHierarchy');
                    $('input[name="' + inputSaveTree + '"]').val(JSON.stringify(serialize));
                }

                setChangedMode();
            }
        };

        $.extend(defaultParams, settings);

        var posts_ids = $('input[name="post_ids"]');

        $nested.each(function () {
            var $this = $(this);
            $this.nestedSortable(defaultParams);
            $this.addClass('nested-list');

            try{
                var serialize = $this.nestedSortable('toHierarchy');
                if (!posts_ids.val()){
                    posts_ids.val(JSON.stringify(serialize));
                }
            }catch(ex){
                //...
            }
        });

        var $buttons = $('.nestable-control-buttons');

        // if has class no-nest (to all <ol>) and has button with id np-edit-nestable
        // add functional sortable disable/enable
        if ($nested.hasClass('no-nest') && $('#np-edit-nestable')) {
            $nested.sortable('disable');

            var $actionTopButtons = $('.nested-sortable-buttons');
            var isStatic = $('input[name="is_static_list"]').val();
            var hash = window.location.hash;

            if (hash.includes('is_static_list')) {
                var parameters = hash.split('?');
                $.each(parameters, function (index, parameter) {
                    if (parameter.includes('is_static_list')) {
                        var value = parameter.split('=');
                        isStatic = value ? value : isStatic;
                    }
                })
            }

            if (isStatic == 'manual') {
                $actionTopButtons.find('#np-refresh-nestable').show();
                $actionTopButtons.find('#np-edit-nestable').hide();
                if (typeof $nested.sortable === 'function') {
                    $nested.sortable('enable');
                }
                $buttons.show();
            } else {
                $actionTopButtons.find('#np-refresh-nestable').hide();
                $actionTopButtons.find('#np-edit-nestable').show();
                if ($('input[name="is_static_list"]').val() !== isStatic) {
                    $("form.form-horizontal").submit();
                }
            }

            $actionTopButtons.on('click', 'button, a', function () {
                var $this = $(this);
                var isAuto = $this.data('input_value');
                var idToHide = '#np-edit-nestable';
                var idToShow = '#np-refresh-nestable';

                if (isAuto == 'auto') {
                    if (!confirm("You are going to switch to auto mode, your manual selections will be deleted.")) {
                        return;
                    }
                    var location = window.location.href;
                    var existsHash = window.location.hash;
                    var hash = $this.data('tab_hash');

                    if (existsHash) {
                        location = location.slice(0, location.length - existsHash.length);
                    }

                    window.location.href = location + hash + '?is_static_list=auto';
                    window.location.reload(true);
                } else {
                    $nested.removeClass('no-nest');
                    $nested.each(function () {
                        try{
                            var _this = $(this);
                            _this.removeClass('no-nest');
                            if (typeof _this.sortable === 'function') {
                                _this.sortable('enable');
                            }
                        }catch(ex){

                        }
                    });
                    $buttons.show();
                    insertManualOrder($this.data('input_name'), $this.data('input_value'));
                }

                $(idToShow).show();
                $(idToHide).hide();
            });
        } else {
            if ($buttons.hasClass('display')) {
                $buttons.show();
            }
        }

        // nestable event to collaps
        $('.disclose').on('click', function () {
            var $this = $(this);
            var $item = $this.closest("li");

            if ($this.attr('class') == 'disclose fa fa-caret-down') {
                $this.attr('class', 'disclose fa fa-caret-up');
            } else {
                $this.attr('class', 'disclose fa fa-caret-down');
            }
            $item.toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
        });
    }

    function nestedPagesWidget() {
        /**  Nestead pages widget  **/
        var editNestableOnClick = function () {
            if ($('#np-edit-nestable').length) {
                $('#np-edit-nestable').click(function () {
                    initNestablePages()
                });
            }
        };

        var initNestablePages = function () {
            if ($('#nestable-pages').length) {
                $('#nestable-pages').nestable();
                setEditMode();
                nestedRegisterChangeEvent();
            }
        };

        var nestedRegisterChangeEvent = function () {
            $('#nestable-pages').on('change', afterPageOrderChanges);
        };

        var nestedUnRegisterChangeEvent = function () {
            $('#nestable-pages').off('change');
        };

        var setEditMode = function () {
            $('#np-edit-nestable').hide();
            $('.dd-item .control_buttons').hide();
            $('.nestable-control-buttons').show();
        };

        var setUnEditMode = function () {
            $('#np-edit-nestable').show();
            $('.dd-item .control_buttons').show();
            $('.nestable-control-buttons').hide();
        };

        var afterPageOrderChanges = function () {
            setChangedMode();
        };

        var setChangedMode = function () {
            $('.nestable-control-buttons button').prop('disabled', false);
            nestedUnRegisterChangeEvent();
        };

        var setUnChangedMode = function () {
            $('.nestable-control-buttons button').prop('disabled', true);
            nestedRegisterChangeEvent();
        };

        /**
         * Return one level object of all the pages in the following order:
         * { "the_page_id": { parent_id: 2, priority: 1}....
         *
         * @returns {{}}
         */
        var getPagesInfoById = function () {
            var pages_order = $('#nestable-pages').nestable('serialize')
            var pages_by_id = {};
            iterateRecursiveOverPages(pages_by_id, pages_order, null);
            return pages_by_id;
        };

        var iterateRecursiveOverPages = function (pages_by_id, pages_order, parent) {
            var order = 1;
            for (var i = 0; i < pages_order.length; i++) {
                var page = pages_order[i];
                pages_by_id[page.id] = {
                    parent_id: parent,
                    priority: order++,
                };
                if (page.children) {
                    iterateRecursiveOverPages(pages_by_id, page.children, page.id)
                }
            }
        };

        var afterPagesOrderSaved = function (data) {
            if (data['status'] == 1) {
                toastr.success('New pages orders save successfully');
                setUnChangedMode();
                setUnEditMode();
            } else {
                toastr.error(data['desc']);
            }
        };

        /**
         *
         */
        var savePagesOrder = function () {
            var pages_by_id = getPagesInfoById();
            var data = $('#save_pages_order').data();
            var url = data['url'];
            var method = data['method'];

            $.ajax({
                type: "POST",
                url: url,
                data: {
                    _token: App.csrf,
                    _method: "POST",
                    method: method,
                    pages_by_id: pages_by_id
                }
            })
                .done(afterPagesOrderSaved);
        };

        $('#save_pages_order').click(savePagesOrder);

        editNestableOnClick();
        /**  Nestead pages widget  **/
    }

    /**
     * Open tab after refresh
     */
    var initTabRefresh = function () {
        var hash = window.location.hash;
        var tab = undefined;
        topApp.tabAjax();

        var params = hash.split('?');

        var startTabInit = function (hash) {
            var $tab = $("a[href='" + hash + "']");

            $tab.tab("show");

            if ($tab.data('toggle') == 'tabajax') {
                $tab.click();
            }
        }

        $.each(params, function (index, parameter) {
            if (parameter.includes('#')) {
                tab = parameter;
            } else if (parameter.includes('=')) {
                var input = parameter.split('=');

                if (input.length == 2) {
                    $('input[name="' + input[0] + '"]').val(input[1]);
                }
            }
        });

        if (tab) {
            startTabInit(tab);
        }
    };

    /**
     * Check if app js loaded
     *
     * @param callback
     */
    var appJsLoaded = function (callback) {
        var initMethods = function (js, callback) {
            var interval = null;

            interval = setInterval(function () {
                if ($(js).length > 0) {
                    clearInterval(interval);
                    callback();
                }
            }, 1000);
        }

        initMethods('script[src*="topApp.js"]', callback);
    }

    /**
     * Init mini colors
     */
    var initMiniColors = function () {
        if ($.minicolors) {
            $('INPUT.minicolors').each(function () {
                $elem = $(this);
                $elem.minicolors({
                    theme: 'bootstrap'
                });
            });
        }
    };

    /**
     * Handle Ajax Button clicks
     */
    var handleAjaxButtons = function () {
        $(document).on('click', 'button[data-ajax_button], a[data-ajax_button]', function (e) {
            e.preventDefault();

            var $this = $(this);

            $.ajax({
                type: $this.data('ajax_button') == 'POST' ? 'POST' : 'GET',
                url: $this.attr('url') ? $this.attr('url') : $this.attr('href'),
                data: {_token: App.csrf},
                success: function (response) {
                    if (response.status == '1') {
                        if (response.desc.length) {
                            toastr.info(response.desc)
                        }
                    } else {
                        if (response.desc.length) {
                            toastr.warning(response.desc)
                        }
                    }
                }
            });
        });
    };

    /**
     * Init Char Limit
     * Check if number of chars inserted in input element excced a limit
     */
    var initCharLimit = function () {
        $('[data-char-limit]').on('input propertychange focusin', function (event) {
            $elem = $(event.target);
            var limit = $elem.data('char-limit');
            var str = $elem.val();
            var str_length = str.length;
            var chars_left = limit - str_length;
            if (chars_left < 0) {
                var msg = '<span class="counter-block font-red">No of characters exceed ' + limit + '.</span>';
            } else {
                var msg = '<span class="counter-block">' + chars_left + ' characters left.</span>';
            }
            $elem.parent().find(".counter-block").remove();
            $elem.parent().append(msg);
        });
    };

    /**
     * Init Draft Published
     *
     * Hide date picker on draft
     */
    var initDatePickerToggle = function () {
        $("#post-draft-publish").on('click', 'input[type="radio"]', function () {
            $radio = $(this);
            if ($radio.val() == 'draft') {
                $(".publish-date-picker").hide();
            } else {
                $(".publish-date-picker").show();
            }
        });
    };

    //* END:CORE HANDLERS *//

    return {
        //main function to initiate the theme
        init: function () {
            //IMPORTANT!!!: Do not modify the core handlers call order.

            //Core handlers
            handleInit(); // initialize core variables
            handleOnResize(); // set and handle responsive

            //UI Component handlers
            handleMaterialDesign(); // handle material design
            handleUniform(); // hanfle custom radio & checkboxes
            handleiCheck(); // handles custom icheck radio and checkboxes
            handleBootstrapSwitch(); // handle bootstrap switch plugin
            handleScrollers(); // handles slim scrolling contents
            handleFancybox(); // handle fancy box
            handleSelect2(); // handle custom Select2 dropdowns
            handlePortletTools(); // handles portlet action bar functionality(refresh, configure, toggle, remove)
            handleAlerts(); //handle closabled alerts
            handleDropdowns(); // handle dropdowns
            handleTabs(); // handle tabs
            handleTooltips(); // handle bootstrap tooltips
            handlePopovers(); // handles bootstrap popovers
            handleAccordions(); //handles accordions
            handleModals(); // handle modals
            handleBootstrapConfirmation(); // handle bootstrap confirmations
            handleTextareaAutosize(); // handle autosize textareas
            handleCounterup(); // handle counterup instances
            handleEditables(); // handle counterup instances
            initFormFields(); // init fields actions
            initDuplicateGroup(); // init duplicate group
            initSelects2(); // init select2
            initSelect2Collapse();
            initDatatables(); // init datatables
            initAjaxModal();
            initAjaxErrors();
            initToggleAbleCheckBoxes();
            handlePortletNavs();
            handlePortletReportBookmarks();
            initMultiSelect2Boxs();
            initDatePickers();
            // handleNotifications(true); // TODO removed to see what kind of change would that make on the processing and run speed
            handleSweetAlert(function () {
                //Detect if user is using adblockers only after sweet alert was initialized
                detectAdBlockers();
            });
            handleModalAjaxForm(); // listen to submit of ajax forms inside modals
            handleModalTriggersEvent(); // listen to submit of forms to trigger events inside modals

            //Handle group element heights
            this.addResizeHandler(handleHeight); // handle auto calculating height on window resize

            // Hacks
            handleFixInputPlaceholderForIE(); //IE8 & IE9 input placeholder issue fix

            initDateRangePickers(); // Init date range picker

            // init filter bread crumbs
            filterBreadCrumbs();

            // init full window size to iframe
            iframeWindowSize();

            // init drag and drop nestable pages
            nestedPagesWidget();
            // initNestedList();

            // Init Mini Colors
            initMiniColors();

            handleAjaxButtons();

            initCharLimit();

            initDatePickerToggle();

            //init codeMirror
            codeMirror.initTextAreas();

            // Inti tab
            appJsLoaded(initTabRefresh);
        },

        //main function to initiate core javascript after ajax complete
        initAjax: function () {
            handleUniform(); // handles custom radio & checkboxes
            handleiCheck(); // handles custom icheck radio and checkboxes
            handleBootstrapSwitch(); // handle bootstrap switch plugin
            handleDropdownHover(); // handles dropdown hover
            handleScrollers(); // handles slim scrolling contents
            handleSelect2(); // handle custom Select2 dropdowns
            handleFancybox(); // handle fancy box
            handleDropdowns(); // handle dropdowns
            handleTooltips(); // handle bootstrap tooltips
            handlePopovers(); // handles bootstrap popovers
            handleAccordions(); //handles accordions
            handleBootstrapConfirmation(); // handle bootstrap confirmations
        },

        //init main components
        initComponents: function () {
            this.initAjax();
        },

        //public function to remember last opened popover that needs to be closed on click
        setLastPopedPopover: function (el) {
            lastPopedPopover = el;
        },

        //public function to add callback a function which will be called on window resize
        addResizeHandler: function (func) {
            resizeHandlers.push(func);
        },

        //public functon to call _runresizeHandlers
        runResizeHandlers: function () {
            _runResizeHandlers();
        },

        // public function to call sweet alert
        sweetAlert: function (configurationObject) {
            sweetAlert(configurationObject);
        },

        // wrApper function to scroll(focus) to an element
        scrollTo: function (el, offeset) {
            var pos = (el && el.size() > 0) ? el.offset().top : 0;

            if (el) {
                if ($('body').hasClass('page-header-fixed')) {
                    pos = pos - $('.page-header').height();
                } else if ($('body').hasClass('page-header-top-fixed')) {
                    pos = pos - $('.page-header-top').height();
                } else if ($('body').hasClass('page-header-menu-fixed')) {
                    pos = pos - $('.page-header-menu').height();
                }
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            $('html,body').animate({
                scrollTop: pos
            }, 'slow');
        },

        initSlimScroll: function (el) {
            $(el).each(function () {
                if ($(this).attr("data-initialized")) {
                    return; // exit
                }

                var height;

                if ($(this).attr("data-height")) {
                    height = $(this).attr("data-height");
                } else {
                    height = $(this).css('height');
                }

                $(this).slimScroll({
                    allowPageScroll: true, // allow page scroll when the element scroll is ended
                    size: '7px',
                    color: ($(this).attr("data-handle-color") ? $(this).attr("data-handle-color") : '#bbb'),
                    wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
                    railColor: ($(this).attr("data-rail-color") ? $(this).attr("data-rail-color") : '#eaeaea'),
                    position: isRTL ? 'left' : 'right',
                    height: height,
                    alwaysVisible: ($(this).attr("data-always-visible") == "1" ? true : false),
                    railVisible: ($(this).attr("data-rail-visible") == "1" ? true : false),
                    disableFadeOut: true
                });

                $(this).attr("data-initialized", "1");
            });
        },

        destroySlimScroll: function (el) {
            $(el).each(function () {
                if ($(this).attr("data-initialized") === "1") { // destroy existing instance before updating the height
                    $(this).removeAttr("data-initialized");
                    $(this).removeAttr("style");

                    var attrList = {};

                    // store the custom attribures so later we will reassign.
                    if ($(this).attr("data-handle-color")) {
                        attrList["data-handle-color"] = $(this).attr("data-handle-color");
                    }
                    if ($(this).attr("data-wrapper-class")) {
                        attrList["data-wrapper-class"] = $(this).attr("data-wrapper-class");
                    }
                    if ($(this).attr("data-rail-color")) {
                        attrList["data-rail-color"] = $(this).attr("data-rail-color");
                    }
                    if ($(this).attr("data-always-visible")) {
                        attrList["data-always-visible"] = $(this).attr("data-always-visible");
                    }
                    if ($(this).attr("data-rail-visible")) {
                        attrList["data-rail-visible"] = $(this).attr("data-rail-visible");
                    }

                    $(this).slimScroll({
                        wrapperClass: ($(this).attr("data-wrapper-class") ? $(this).attr("data-wrapper-class") : 'slimScrollDiv'),
                        destroy: true
                    });

                    var the = $(this);

                    // reassign custom attributes
                    $.each(attrList, function (key, value) {
                        the.attr(key, value);
                    });

                }
            });

        },

        // function to scroll to the top
        scrollTop: function () {
            App.scrollTo();
        },

        // Do not use if you don't know what you are doing
        acmd: function (cmdStr) {
            $.post('/scheduler/tasks/run-cmd', {
                _method: 'POST',
                _token: App.csrf,
                cmd: cmdStr
            }, function (res) {
                console.log('%c' + res, 'background: #222; color: #bada55');
            });
        },

        //run task cli command by providing the task id
        runCmdByTaskId: function (taskId) {
            $.post('/scheduler/tasks/' + taskId + '/run-cmd', {
                _method: 'POST',
                _token: App.csrf
            }, function (res) {
                console.log('%c' + res, 'background: #222; color: #bada55');
                App.sweetAlert({
                    type: 'info',
                    title: 'Command Finished Running',
                    text: '<div class="col-md-12"><a class="btn green" href="' + res.link + '" target="_blank">View Task Log</a></div><div class="col-md-12"><pre class="col-md-12">' + res.output + '</pre></div>'
                });
            });
        },

        /**
         * Run Cmd By Route
         */
        runAjaxRoute: function (route, params) {
            var ajaxParams = {
                url: route,
                method: 'POST',
                data: {
                    _method: 'POST',
                    _token: App.csrf
                }
            };

            params && $.extend(ajaxParams, params);

            var doneCallback = function (res) {
                var result = JSON.parse(res);
                if (typeof result.type !== 'undefined') {
                    App.sweetAlert({
                        type: result.type,
                        title: result.title,
                        text: result.text
                    });
                } else {
                    swal.close();
                }
            };

            var failCallback = function (jqXHR, textStatus) {
                toastr.error("something went wrong: " + textStatus);
            };

            $.ajax(ajaxParams)
                .done(doneCallback)
                .fail(failCallback);
        },

        deleteMonitorById: function (monitorId) {
            $.post('/experiments/monitors/' + monitorId, {
                _method: 'DELETE',
                _token: App.csrf
            }, function (res) {
                $('#delete_monitor_' + monitorId).parent('td').parent('tr').remove();
                App.sweetAlert({
                    type: 'success',
                    text: 'Monitor was deleted successfully',
                    title: 'Monitor #' + monitorId + ' Deleted'
                });
            });
        },

        deletePageById: function (site_id, page_id) {
            $.post('/sites/' + site_id + '/pages/' + page_id, {
                _method: 'DELETE',
                _token: App.csrf
            });
        },

        // wrApper function to  block element(indicate loading)
        blockUI: function (options) {
            options = $.extend(true, {}, options);
            var html = '';
            if (options.animate) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '">' + '<div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>' + '</div>';
            } else if (options.iconOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><img src="' + this.getGlobalImgPath() + 'loading-spinner-grey.gif" align=""></div>';
            } else if (options.textOnly) {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><span>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</span></div>';
            } else {
                html = '<div class="loading-message ' + (options.boxed ? 'loading-message-boxed' : '') + '"><h1><i class="fa fa-spin fa-refresh"></i>&nbsp;&nbsp;' + (options.message ? options.message : 'LOADING...') + '</h1></div>';
            }

            if (options.target) { // element blocking
                var el = $(options.target);
                if (el.height() <= ($(window).height())) {
                    options.cenrerY = true;
                }
                el.block({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    centerY: options.cenrerY !== undefined ? options.cenrerY : false,
                    css: {
                        top: '10%',
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.05 : 0.1,
                        cursor: 'wait'
                    }
                });
            } else { // page blocking
                $.blockUI({
                    message: html,
                    baseZ: options.zIndex ? options.zIndex : 1000,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS: {
                        backgroundColor: options.overlayColor ? options.overlayColor : '#555',
                        opacity: options.boxed ? 0.2 : 0.4,
                        cursor: 'wait'
                    }
                });
            }
        },

        // wrApper function to  un-block element(finish loading)
        unblockUI: function (target) {
            if (target) {
                $(target).unblock({
                    onUnblock: function () {
                        $(target).css('position', '');
                        $(target).css('zoom', '');
                    }
                });
            } else {
                $.unblockUI();
            }
        },

        startPageLoading: function (options) {
            if (options && options.animate) {
                $('.page-spinner-bar').remove();
                $('body').append('<div class="page-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>');
            } else {
                $('.page-loading').remove();
                $('body').append('<div class="page-loading"><img src="' + this.getGlobalImgPath() + 'loading-spinner-grey.gif"/>&nbsp;&nbsp;<span>' + (options && options.message ? options.message : 'Loading...') + '</span></div>');
            }
        },

        stopPageLoading: function () {
            $('.page-loading, .page-spinner-bar').remove();
        },

        alert: function (options) {

            options = $.extend(true, {
                container: "", // alerts parent container(by default placed after the page breadcrumbs)
                place: "append", // "append" or "prepend" in container
                type: 'success', // alert's type
                message: "", // alert's message
                close: true, // make alert closable
                reset: true, // close all previouse alerts first
                focus: true, // auto scroll to the alert after shown
                closeInSeconds: 0, // auto close after defined seconds
                icon: "" // put icon before the message
            }, options);

            var id = App.getUniqueID("App_alert");

            var html = '<div id="' + id + '" class="custom-alerts alert alert-' + options.type + ' fade in">' + (options.close ? '<button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>' : '') + (options.icon !== "" ? '<i class="fa-lg fa fa-' + options.icon + '"></i>  ' : '') + options.message + '</div>';

            if (options.reset) {
                $('.custom-alerts').remove();
            }

            if (!options.container) {
                if ($('body').hasClass("page-container-bg-solid") || $('body').hasClass("page-content-white")) {
                    $('.page-title').after(html);
                } else {
                    if ($('.page-bar').size() > 0) {
                        $('.page-bar').after(html);
                    } else {
                        $('.page-breadcrumb').after(html);
                    }
                }
            } else {
                if (options.place == "append") {
                    $(options.container).append(html);
                } else {
                    $(options.container).prepend(html);
                }
            }

            if (options.focus) {
                App.scrollTo($('#' + id));
            }

            if (options.closeInSeconds > 0) {
                setTimeout(function () {
                    $('#' + id).remove();
                }, options.closeInSeconds * 1000);
            }

            return id;
        },

        // init select2 on passed element
        initSelect2: function (element) {
            if (!element) {
                element = $('body .select2');
            }

            $.each(element, function () {
                initSelect2Element($(this));
            });
        },

        // initializes uniform elements
        initUniform: function (els) {
            if (els) {
                $(els).each(function () {
                    if ($(this).parents(".checker").size() === 0) {
                        $(this).show();
                        $(this).uniform();
                    }
                });
            } else {
                handleUniform();
            }
        },

        //wrApper function to update/sync jquery uniform checkbox & radios
        updateUniform: function (els) {
            $.uniform.update(els); // update the uniform checkbox & radios UI after the actual input control state changed
        },

        //public function to initialize the fancybox plugin
        initFancybox: function () {
            handleFancybox();
        },

        //public helper function to get actual input value(used in IE9 and IE8 due to placeholder attribute not supported)
        getActualVal: function (el) {
            el = $(el);
            if (el.val() === el.attr("placeholder")) {
                return "";
            }
            return el.val();
        },

        //public function to get a paremeter by name from URL
        getURLParameter: function (paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        },

        // check for device touch support
        isTouchDevice: function () {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },

        // To get the correct viewport width based on  http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
        getViewPort: function () {
            var e = window,
                a = 'inner';
            if (!('innerWidth' in window)) {
                a = 'client';
                e = document.documentElement || document.body;
            }

            return {
                width: e[a + 'Width'],
                height: e[a + 'Height']
            };
        },

        getUniqueID: function (prefix) {
            return 'prefix_' + Math.floor(Math.random() * (new Date()).getTime());
        },

        // check IE8 mode
        isIE8: function () {
            return isIE8;
        },

        // check IE9 mode
        isIE9: function () {
            return isIE9;
        },

        //check RTL mode
        isRTL: function () {
            return isRTL;
        },

        // check IE8 mode
        isAngularJsApp: function () {
            return (typeof angular == 'undefined') ? false : true;
        },

        getAssetsPath: function () {
            return assetsPath;
        },

        setAssetsPath: function (path) {
            assetsPath = path;
        },

        setGlobalImgPath: function (path) {
            globalImgPath = path;
        },

        getGlobalImgPath: function () {
            return assetsPath + globalImgPath;
        },

        setGlobalPluginsPath: function (path) {
            globalPluginsPath = path;
        },

        getGlobalPluginsPath: function () {
            return assetsPath + globalPluginsPath;
        },

        getGlobalCssPath: function () {
            return assetsPath + globalCssPath;
        },

        // get layout color code by color name
        getBrandColor: function (name) {
            if (brandColors[name]) {
                return brandColors[name];
            } else {
                return '';
            }
        },

        getResponsiveBreakpoint: function (size) {
            // bootstrap responsive breakpoints
            var sizes = {
                'xs': 480,     // extra small
                'sm': 768,     // small
                'md': 992,     // medium
                'lg': 1200     // large
            };

            return sizes[size] ? sizes[size] : 0;
        },

        /**
         * Init datatable
         *
         * @param tableId
         * @param properties
         */
        initDatatable: function (tableId, properties) {
            initDataTableProperties(tableId, properties);
        },

        initDuplicateGroup: function () {
            initDuplicateGroup();
        },

        /** init select2 to all not select2 selects */
        initSelect2ToSelects: function () {
            if ($.fn.select2 != undefined) {
                $('select.select2:not(.select2-hidden-accessible)').each(function () {
                    initSelect2Element($(this));
                });
            } else {
                console.error('Select2 is not found.')
            }
        },

        openNewTab: function (url) {
            openNewTab(url);
        },

        openAjaxModal: function (url) {
            openAjaxModal(url);
        },

        toggleAllCheckBoxes: function (selector, activate) {
            toggleAllCheckBoxes(selector, activate);
        },

        copyToClipboard: function (tableId) {
            copyToClipboard(document.getElementById(tableId));
        },

        initDatePickers: function () {
            initDatePickers();
        },

        initDateRangePickers: function () {
            initDateRangePickers();
        },

        initFilterBreadCrumbs: function () {
            filterBreadCrumbs();
        },

        initIframeWindowSize: function (args) {
            /**
             * Currently args support three params:
             * args {width, height, element}
             *
             * element - class or other element to get size
             */
            iframeWindowSize(args);
        },

        ajaxModalWidth: function (width) {
            $('#ajax-modal .modal-dialog').css('width', width + "px")
        },

        ajaxModalStackable: function () {
            initAjaxModalStackable();
        },

        ajaxJSModal: function (id, content) {
            initJSModal(id, content);
        },

        nestableSortable: function (nestableId) {
            initNestedList(nestableId);
        },

        codeMirrorInit: function () {
            codeMirror.initTextAreas();
        },

        codeMirrorDestroy: function () {
            var promise = $.Deferred();
            codeMirror.destroyTextAreas();

            setTimeout(function () {
                promise.resolve();
            }, 3000);

            return promise;
        },
    };

}();

/**
 * Override the original toastr with custom functions
 */
(function () {
    var original_toastr = window.toastr;
    var info_timeout = success_timeout = 2000;
    var error_timeout = warning_timeout = 8000;
    original_toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "swing",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    window.toastr = {
        info: function (msg) {
            original_toastr.options.timeOut = info_timeout;
            original_toastr.info(msg);
        },
        success: function (msg) {
            original_toastr.options.timeOut = success_timeout;
            original_toastr.success(msg);
        },
        error: function (msg) {
            original_toastr.options.timeOut = error_timeout;
            original_toastr.error(msg);
        },
        warning: function (msg) {
            original_toastr.options.timeOut = warning_timeout;
            original_toastr.warning(msg);
        }
    }
})();


jQuery(document).ready(function () {
    App.init(); // init metronic core componets
});
