function menuItems() {
    var _newItemNumber = 10000;
    var savedSelectedMenuId;
    var isChanged = false;

    var initNestableMenuItems = function () {
        if ($('#mi-nestable-menu-items').length) {
            $('#mi-nestable-menu-items').nestable();
            registerEvent();
        }
    };

    var registerEvent = function () {
        //Add items
        $('#mi-btn-add-page-to-menu').click(addPageToMenu);
        $('#mi-btn-add-link-to-menu').click(addLinkToMenu);

        //Menus box
        savedSelectedMenuId = $('#mi-menu-select').val();
        $('#mi-menu-select').on('change', onMenuChange);

        //Save button
        $('#mi-save-menu-items-order').click(saveMenuItemsOrder);

        //Inner items box
        $('#mi-nestable-menu-items')
            .on('click', '.mi-toggle-link-box', toggleLinkBox)
            .on('click', '.mi-delete-menu-item', deleteMenuItem);

        // register nestabel change after it first is has been correctly
        setTimeout(function(){
            $('#mi-nestable-menu-items')
                .on('change', afterChanged);
        },700);

    };

    var onMenuChange = function (event) {
        var selectedMenuId = $(event.target).val();
        if (selectedMenuId != savedSelectedMenuId) {
            var newLocation = window.location.href.replace(/menus\/\d*\/items/, 'menus/' + selectedMenuId + '/items');
            window.location.href = newLocation;
        }
    };

    var deleteMenuItem = function (event) {
        event.preventDefault();
        var $menuItemToDelete = $(event.target).closest('li');
        $menuItemToDelete.hide();
        //add delete hidden input to all the siblings to be remove at the server
        $menuItemToDelete.find('.mi-link-content').each(function (index, elem) {
            var $elem = $(elem);
            var $input = $elem.find('input').first();
            $deleteInput = $input.clone();
            var newNameAttribute = $deleteInput.attr('name').replace(/\[.*]/, '[delete]')
            $deleteInput
                .attr('name', newNameAttribute)
                .attr('type', 'hidden')
                .val('delete');
            $input.after($deleteInput);
        });
    };

    var toggleLinkBox = function (event) {
        event.preventDefault();
        var $li = $(event.target).closest('li');
        var $toggleLinkBox = $li.find('.control_buttons.mi-toggle-link-box');
        $toggleLinkBox
            .toggleClass('fa-caret-down')
            .toggleClass('fa-caret-up');
        $li.find('.mi-link-content').first().toggle();
    };

    var getNewItemNumber = function () {
        return _newItemNumber++;
    };

    var saveMenuItemsOrder = function () {
        var linksInfoById = getLinksInfoById();
        var linksData = $("#mi-nestable-menu-items-form").serializeObject();
        for (var i = 0; i < linksInfoById.length; i++) {
            var linkInfo = linksInfoById[i];
            var id = linkInfo.id;
            var linkId = "link_" + id;
            if (linksData[linkId]) {
                $.extend(linkInfo, linksData[linkId]);
            }
        }
        ;

        $form = $("#mi-nestable-menu-items-form");
        $.ajax({
            type: "POST",
            url: $form.attr('action'),
            data: {
                _token: App.csrf,
                _method: "POST",
                links_by_id: linksInfoById

            },
        })
            .done(afterMenuItemsSaved);
    };

    var afterMenuItemsSaved = function (data) {
        if (data.status == 0) {
            toastr.warning(data.desc);
            return;
        }
        window.onbeforeunload = null;
        toastr.success("Your changes has been saved");
        location.reload();
    };

    /**
     * Convert tree like nested items to array with
     *
     * @returns {{}}
     */
    var getLinksInfoById = function () {
        var items_order = $('#mi-nestable-menu-items').nestable('serialize');
        var save_link_array = [];
        iterateRecursiveOverNestable(save_link_array, items_order, null);
        return save_link_array;
    };

    var iterateRecursiveOverNestable = function (save_link_array, items_order, parent) {
        var order = 1;
        for (var i = 0; i < items_order.length; i++) {
            var item = items_order[i];
            save_link_array.push({
                id: item.id,
                parent_id: parent,
                priority: order++,
            });
            if (item.children) {
                iterateRecursiveOverNestable(save_link_array, item.children, item.id)
            }
        }
    };

    var addPageToMenu = function () {
        var $selectedOption = $('#mi-select-page  option:selected');
        var pageName = $selectedOption.attr('name');
        var pageId = $selectedOption.val();
        var slug = $selectedOption.attr('slug');
        if (isNaN(pageId)) {
            toastr.warning('Please choose page');
            return;
        }
        addNewMenuItem(pageName, pageId, slug);
    };

    var addLinkToMenu = function () {
        var linkName = $('.mi-link-name').val().trim();
        var linkUrl = $('.mi-link-url').val().trim();
        if (linkName.length === 0 || linkUrl.length === 0) {
            toastr.warning('Please fill all fields');
            return;
        }
        addNewMenuItem(linkName, null, linkUrl);
        $('.mi-link-name').val('');
        $('.mi-link-url').val('');
    };

    var afterChanged = function () {
        if (isChanged) return;
        $('#mi-nestable-menu-items').off('change');
        $('#mi-empty-message-box').hide();
        console.log('onbeforeunload');
        window.onbeforeunload = function() {
            return 'You have unsaved changes!';
        };

        isChanged = true;
    };

    var addNewMenuItem = function (text, page_id, url) {
        var localId = getNewItemNumber();
        $li = $('.mi-link-content-template li').clone();
        //remove old select 2 objects
        $li.find('select').removeClass('select2-hidden-accessible');
        $li.find('.select2.select2-container').remove();
        $li.find('[name="link_[name]"]').val(text);
        if (url) {
            $li.find('[name="link_[url]"]').val(url).attr('readonly','readonly');
        } else {
            $li.find('[name="link_[url]"]').closest('.form-group').remove();
        }
        if (page_id) {
            $li.find('[name="link_[post_id]"]').val(page_id);
        } else {
            $li.find('[name="link_[post_id]"]').closest('.form-group').remove();
        }
        $li.find('[name]').each(function (index, elem) {
            $(elem).attr('name', $(elem).attr('name').replace('link_', 'link_' + localId));
        });
        $li
            .data('id', localId)
            .find('.mi-link-name')
            .text(text);
        $('#mi-nestable-menu-items ol').first().append($li);
        $li.find('select').first().select2();
        afterChanged();
    };

    /** bootstrap **/
    initNestableMenuItems();
};


jQuery(document).ready(function () {
    menuItems();
});

