define('dynamicList', ['jquery','generalHelpers','rating'], function ($,helpers) {
    var showedCounts = 0;

    var defaultCssSettings = {
        display: 'table-row'
    };

    var filterables = {
        filters: [],
        sorters: []
    };

    var selectors = {
        tableContainer: '.brands-table',
        sortListContainer: '.sort-filter ul ul.sorter',
        sortListItems: '.sort-filter li li[data-action="sort"]',
        filterListContainer: '.sort-filter ul ul.filter',
        filterListItems: '.sort-filter li li[data-action="filter"]',
        selectedTagsContainer: '.selected-filters',
        loadMoreButton: '.load-more',
        clearAllButton: '#clear-all-filters',
        defaultContainer: '.container',
        lastBonus: {
            container: '.last-bonus',
            bonus: '.cell.bonus',
            button: '.last-bonus .btn'
        }
    };

    // filter table rows
    function filter($container, byDataAttrs) {
        var $fullItemList = $container.find('.table-row');
        var countVisiblePost = $container.find('[data-load_count]').data('load_count');

        if ($container.data('filter_remove')) {
            $container.data('filter_remove', false);
            byDataAttrs = [];
        }

        // display all items
        $fullItemList.each(function () {
            var $row = $(this);

            if (byDataAttrs.length == 0 && countVisiblePost > 0) {
                countVisiblePost--;
                $row.removeClass('hidden');
            } else if (byDataAttrs.length == 0 && countVisiblePost == 0) {
                $row.addClass('hidden');
            } else if (byDataAttrs.length > 0) {
                $row.removeClass('hidden');
            }

            $row.removeClass('filtered');
        })

        // hide every row that does not have all of the chosen data attributes
        $.each(byDataAttrs, function (index, value) {
            var activeFilterAttr = value;
            $fullItemList.each(function () {
                var $row = $(this);
                // the row does not have the current data attribute
                if (!$row.data(activeFilterAttr)) {
                    $row.addClass('hidden');
                    $row.addClass('filtered');
                }
            });
        });
    }

    function getFilteredItems($itemList) {
        var count = 0;

        // count filtered rows
        $itemList.each(function () {
            if ($(this).hasClass('filtered')) {
                count++;
            }
        });

        return count;
    }

    function getVisibleItems($itemList) {
        var count = 0;

        // count visible rows
        $itemList.each(function () {
            if ($(this).is(':visible')) {
                count++;
            }
        });

        return count;
    }

    // sort table rows
    function sortList($container, byDataAttr) {
        var sortAttribute = byDataAttr == undefined ? 'order_index' : byDataAttr;
        var sortType = sortAttribute == 'order_index' ? 'asc' : 'desc';
        var dataAttr = 'data-' + (sortAttribute);
        var $itemList = $container.find('[' + dataAttr + ']');
        var countVisible = 0;
        var classContainer = $itemList.length > 0 ? $($itemList[0]).data('container') : undefined;
        var container = $container.find(classContainer);

        // sort by value - asc
        $itemList.detach().sort(function (a, b) {
            var aValue = $(a).data(sortAttribute);
            var bValue = $(b).data(sortAttribute);
            return sortType == 'asc' ? aValue - bValue : bValue - aValue;
        });

        // count visible row
        countVisible = getVisibleItems($itemList);

        if (container) {
            // empty container
            var $container = $(container);

            // insert new list
            $itemList.each(function (index, row) {
                var $row = $(row);
                if (index < countVisible) {
                    $row.removeClass('hidden');
                } else {
                    $row.addClass('hidden');
                }

                $(container).append($row);
            });
        } else {
            console.error([{
                'js': 'dynamicList',
                'method': 'filter',
                'error': 'container not found, missing data attribute - data-container'
            }]);
        }
    }

    /**
     * Show hidden posts
     */
    function showMore(button) {
        var $showButton = $(button);
        var current_list = $(button).parents('div').eq(2);
        var hidden = current_list.find('.table-row.hidden');
        var $load_count = current_list.find('[data-load_count]');

        if (typeof $load_count.data('load_count') !== 'undefined') {
            var loadCount = $load_count.data('load_count') - 1;
            var hiddenItemsCount = hidden.length;

            hidden.each(function (index, row) {
                if (index <= loadCount) {
                    $(row).removeClass('hidden');
                }
            });

            if (hiddenItemsCount - loadCount <= 1) {
                $showButton.hide();
            }
        }
    }

    function showLoadingAnimation($loaderAnimation) {
        var $targetWrapper = $(selectors.tableContainer);

        $loaderAnimation.fadeIn(500);
        $targetWrapper.fadeOut(500);
    }

    function terminateLoadingAnimation($loaderAnimation) {
        var $targetWrapper = $(selectors.tableContainer);

        $loaderAnimation.fadeOut(500);
        $targetWrapper.fadeIn(500);
    }

    // init this s&f
    function updateFilterables() {
        filterables.filters = $(selectors.filterListItems);
        filterables.sorters = $(selectors.sortListItems);
    }

    // toggle the list filter icon
    function toggleFilter($filter) {
        $filter.toggleClass('active');
    }

    function updateFilterTags() {
        var $selectedTagsContainer = $(selectors.selectedTagsContainer);
        var $activeFilters = $(selectors.filterListContainer).find('.active');

        // clear the tags container
        $selectedTagsContainer.html('');

        //  iterate over the active filters and append active filters to tags
        $activeFilters.each(function () {
            var $activeFilter = $(this);
            var $tagHtml = getTagHtml($activeFilter.find('.filter-name').html(), $activeFilter.data('field'));

            $selectedTagsContainer.append($tagHtml);
        });
    }

    function initShowMore() {
        var $containers = $('.container');

        $containers.each(function () {
            var $container = $(this);
            updateShowMore($container);
        });
    }

    function updateShowMore($container) {
        var $tableRows = $container.find('.table-row.hidden');
        var button = $container.find('.load-more');
        var $showMoreButton = $(button);
        var filteredCount = getFilteredItems($tableRows);

        if ($tableRows.length - filteredCount > 0) {
            $showMoreButton.show();
        } else {
            $showMoreButton.hide();
        }
    }

    function updateTable($filter, isContainer) {
        var $container = isContainer ? $filter : $filter.parents(this.defaultContainer);

        if ($container.length === 0){
            $container = $(selectors.tableContainer);
        }

        var dataFields = [];
        var $activeFilters = $container.find(selectors.filterListContainer).find('.active');
        var $activeSorter = $container.find(selectors.sortListContainer).find('.active');
        var $loadingAnimation = $('.loading');

        var table_container = $(selectors.tableContainer);
        var disable_animation = table_container.length && table_container.attr('data-no-animation') == 1;

        if (!disable_animation) {
            showLoadingAnimation($loadingAnimation);
        }

        $activeFilters.each(function () {
            var $this = $(this);
            var dataField = $this.data('field');

            dataFields.push(dataField);
        });

        sortList($container, $activeSorter.data('field'));
        filter($container, dataFields);

        updateShowMore($container);
        terminateLoadingAnimation($loadingAnimation);
    }

    // toggle only one sorter to be active
    function toggleSorter($sorter) {
        var $container = $sorter.parents('.container');
        $container.find('.sorters')
            .find('.active')
            .not($sorter)
            .removeClass('active');
        $sorter.toggleClass('active');
    }

    //removes filter from tags and list
    function removeFilter($filter) {
        var $container = $filter.parents('.container');

        var filterField = $filter.parents('.filter').data('field');
        var selectorString = '[data-field="' + filterField + '"]';
        var $filterListItem = $container.find(selectors.filterListContainer).find(selectorString);
        var $selectedTagFilter = $container.find(selectors.selectedTagsContainer).find(selectorString);

        $selectedTagFilter.remove();
        toggleFilter($filterListItem);
    }

    function loadMoreRows(button) {
        var current_list = $(button).parents('div').eq(2);
        var hidden = current_list.find('.table-row.hidden').length;

        if (hidden > 0) {
            showMore(button);
        } else {
            $(button).hide();
            $(selectors.tableContainer).addClass('full-table-display');
        }
    }

    function attachEventListeners() {

        filterables.filters.on('click touch', function (e) {
            e.preventDefault();
            var $filter = $(this);
            var $container = $filter.parents('.container');
            var $activeFilters = $container.find(selectors.filterListContainer);

            toggleFilter($filter);

            if ($activeFilters.find('.active').length == 0) {
                $container.data('filter_remove', true);
            }

            updateFilterTags($filter);
            updateTable($filter);
        });

        filterables.sorters.on('click touch', function (e) {
            e.preventDefault();
            var $sorter = $(this);

            toggleSorter($sorter);
            updateTable($sorter);
        });

        $(selectors.selectedTagsContainer).on('click touch', '.remove', function (e) {
            e.preventDefault();
            var $filter = $(this);
            var $container = $filter.parents('.container');
            var $activeFilters = $container.find(selectors.selectedTagsContainer);

            removeFilter($filter);

            if ($activeFilters.find('div').length == 0) {
                $container.data('filter_remove', true);
            }

            updateTable($container, true);
        });

        $(selectors.loadMoreButton).on('click touch', function (e) {
            e.preventDefault();
            loadMoreRows(this);
        });

        $(selectors.lastBonus.button).on('click', function (e) {
            e.preventDefault();
            var $bonus = $(selectors.lastBonus.bonus);
            hideLastBonusContent($bonus);
            setTimeout(function () {
                showLastBonusContent($bonus);
            }, 2000);
            var time = loadLastBonusTime();
            document.cookie = "lastBonusTimeRefresh=" + time;
        });

        $(selectors.clearAllButton).click(function (){
            filterables.filters = [];
            $(selectors.filterListContainer).find('.active').removeClass('active');
            updateTable($(selectors.tableContainer));
        });

    }

    function getTagHtml(label, fieldName) {
        return $('<div class="filter" data-field="' + fieldName + '">' +
            '<span class="name">' + label + '</span>' +
            '<span class="remove">' +
            '<i class="icon-no"></i>' +
            '</span>' +
            '</div>');
    }

    /**
     * Check if url have query
     */
    function urlQuery() {
        if (location.search.substr(1)) {
            var queryParams = parseUrlQuery();

            if (queryParams.filter) {
                attachUrlSearch(queryParams, filterables.filters);
            }

            if (queryParams.order) {
                attachUrlSearch(queryParams, filterables.sorters);
            }
        }
    }

    /**
     * Find and run click event on filter or order
     *
     * @param queryParams
     * @param elements
     */
    function attachUrlSearch(queryParams, elements) {
        $.each(elements, function () {
            if (queryParams[$(this).data('field')]) {
                $(this).click();
            }
        });
    }

    /**
     * Parse url query
     *
     * @returns {{}}
     */
    function parseUrlQuery() {
        var queryParams = {};
        location.search.substr(1).split("&").forEach(function (item) {
            queryParams[item.split("=")[0]] = item.split("=")[1]
        });

        return queryParams;
    }


    /**
     * Support old dynamic list behavior.
     *
     * @return void
     **/
    function oldDynamicListSupport(){
        if (typeof window.overwrite_selectors === 'object') {
            for (var key in window.overwrite_selectors) {
                if (window.overwrite_selectors[key]) {
                    selectors[key] = window.overwrite_selectors[key];
                }
            }
        }

        var dropdown_filter_element = $("#filter_dropdown");
        if (dropdown_filter_element.length > 0){

            var sort_dropdown = $(".open-sort");

            sort_dropdown.click(function (){
                var sort_dropdown_content = sort_dropdown.find(".dropdown-filter");
                if (sort_dropdown_content.is(":visible")){
                    sort_dropdown_content.hide();
                }else{
                    sort_dropdown_content.show();
                }
            });

            var filters = $(".dropdown-wrapper");

            filters.hide();

            dropdown_filter_element.click(function (){
                if (filters.is(":visible")){
                    filters.slideUp();
                }else{
                    filters.slideDown();
                }
            });

            $(".filters-close").click(function (){
                dropdown_filter_element.click();
            })
        }
    }

    function init() {
        oldDynamicListSupport();
        updateFilterables();
        attachEventListeners();
        //init url query
        urlQuery();
        initShowMore();
        loadLastBonusTime(-15);
    }

    init();

    /**
     *
     * Load Last Bonus Time:
     * - if delay passed - it will decrease it from time
     *
     * @param selector
     * @param delay
     */
    function loadLastBonusTime(delay) {
        this.date = new Date();
        this.selectors = {
            wrapper: '.last-bonus .time',
        };
        this.init = function (delay) {
            if (helpers.getCookie('lastBonusTimeRefresh') && delay) {
                this.time = helpers.getCookie('lastBonusTimeRefresh');
                this.set(this.time);
                return;
            }
            this.time = this.generateTime(this.date, delay);
            this.time = this.time.toLocaleString(
                'en-US', {
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric',
                    hour12: true
                }
            );
            this.set(this.time);
        }
        this.generateTime = function (date, minDelay) {
            minDelay = !minDelay ? 0 : minDelay;
            return new Date(date.getTime() + minDelay * 60000);
        }
        this.set = function (time) {
            $(this.selectors.wrapper).text(time);
            $(selectors.lastBonus.button).removeClass('hidden');
        }

        //initialize on load
        this.init(delay);

        return this.time;
    }

    /**
     * Hide Last Bonus Content
     *
     * @param $bonus
     */
    function hideLastBonusContent($bonus) {
        $bonus.find('.content').fadeOut();
        $bonus.find('.loading').fadeIn();
    }

    /**
     * Show Last Bonus Content
     *
     * @param $bonus
     */
    function showLastBonusContent($bonus) {
        $bonus.find('.loading').fadeOut();
        setTimeout(function () {
            $bonus.find('.content').fadeIn();
        }, 400);
    }

    return {
        init: function () {
            init();
        }
    };
});