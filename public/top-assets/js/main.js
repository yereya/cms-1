var libs = {
    "jquery": "libs/jquery-3.1.1.min",
    "validator": "libs/jquery-validation-1.16/jquery.validate.min",
    "jquery_with_validator": "libs/jquery-validation-1.16/additional-methods.min",
    "bootstrap": "libs/bootstrap/javascripts/bootstrap.min",
    "slick": "libs/slick/slick.min",
    "tooltipster": "libs/tooltipster/js/tooltipster.bundle.min",
    "magnific-popup": "libs/magnific-popup/jquery.magnific-popup.min",
    "jsCookie": "libs/js-cookie/js.cookie.min",
    'sidr': "libs/sidr/dist/jquery.sidr.min",
    'affix': 'libs/bootstrap/javascripts/bootstrap/affix',
    'sticky': 'libs/sticky-header.min',
    'lightbox':'libs/light-box/dist/ekko-lightbox'
};

var js = {
    "jquery_with_custom_validation": "js/min/jqueryWithCustomValidation",
    "toolTips": "js/min/toolTipster",
    "carousel": "js/min/carousel",
    "comparisonTable": "js/min/comparisonTable",
    "topScroller": "js/min/scrollTop",
    "popup": "js/min/tp-popup",
    "bookmark": "js/min/bookmark",
    "dynamicList": "js/min/dynamicList",
    "tabbing": "js/min/tabbing",
    "comments": "js/min/comments",
    "ajax-forms": "js/min/ajax-forms",
    "cookieConsent": "js/min/cookie-consent",
    "animateTrigger": "js/min/animateTrigger",
    "rating": "js/min/rating",
    'generalHelpers': 'js/min/generalHelpers',
    'scrollIntoView': 'js/min/scrollIntoView',
    'expandText': 'js/min/expandText',
    'mobileSideBar': 'js/min/mobileSidebar',
    'stickyHeader': 'js/min/stickyHeader',
    'feed': 'js/min/feed',
    'likes': 'js/min/likes',
    'goBack': 'js/min/goBack',
    'parallax':'js/min/parallax',
    'general-rating':'js/min/general-rating',
    'lightBox':'js/lightBox',
    'countdownJs': 'js/countdown'
};

if (typeof Top === 'object' && Top.env && Top.env === 'local') {
    for (var key in js) {
        js[key] = js[key].replace('js/min/','js/');
    }
}

var paths = {};

for (var key in libs){
    paths[key] = libs[key];
}

for (var key in js){
    paths[key] = js[key];
}

requirejs.config({
    baseUrl: '/top-assets/',
    paths: paths
});

if (typeof jQuery === 'function') {
    define('jquery', function () {
        return jQuery;
    });
}

var selector_lib_map = {
    '.mobile-side-bar': 'mobileSideBar',
    '.slider-wrapper': 'carousel',
    '#comparison': 'comparisonTable',
    '.tooltipster': 'toolTips',
    '.scroll-to-top': 'topScroller',
    '.add_to_bookmarks': 'bookmark',
    '.brands-table': 'dynamicList',
    '.tabbing': 'tabbing',
    '.comment-box-form': 'comments',
    '[data-stick="stick"]': 'stickyHeader',
    '#cookie-consent': 'cookieConsent',
    '[data-animate]': 'animateTrigger',
    'a[href^="\\#"]': 'scrollIntoView',
    '[data-expand]': 'expandText',
    '.widget_feed_wrapper': 'feed',
    '.likes': 'likes',
    '.goBackButton': 'goBack',
    '[data-current_rating]': 'rating',
    '.parallax_ends_here': 'parallax',
    '.light_box':'lightBox',
    '.rating-input': 'general-rating',
    '[data-countdown]': 'countdownJs',
    '[data-toggle="collapse"]': 'bootstrap'
};

var ajax_forms_selectors = {
    '.newsletter-box-form': 'newsletter',
    '.contact-us-box-form': 'contact-us'
};

var pushNotification = function () {
    //https://pushcrew.com
    (function (p, u, s, h) {
        p._pcq = p._pcq || [];
        p._pcq.push(['_currentTime', Date.now()]);
        s = u.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = 'https://cdn.pushcrew.com/js/3aefadf8bdec1cc1c87f1a477ec293b5.js';
        h = u.getElementsByTagName('script')[0];
        h.parentNode.insertBefore(s, h);
    })(window, document);
}

var mainJs = (function () {
    return {
        init: function ($) {
            "use strict";

            $.each(selector_lib_map, function (selector, lib) {
                if ($(selector).length) {
                    requirejs([lib]);
                }
            });

            $.each(ajax_forms_selectors, function (selector, type) {
                if ($(selector).length) {
                    requirejs(['ajax-forms'], function (ajax_form) {
                        ajax_form.init(selector, type);
                    })
                }
            });

            if (typeof tp_popup_list != 'undefined') {
                requirejs(['popup']);
            }

            if (typeof Top === 'object' && Top.disable_push_notification_request != 0) {
                pushNotification();
            }
        }
    }
})();

requirejs(['jquery'], function ($) {
    window.jQuery = $;
    mainJs.init($);
});