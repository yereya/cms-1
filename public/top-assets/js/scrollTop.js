define('topScroller', ['jquery'], function ($) {
    var scrollToTopBtn;
    var defaultSettings = {
        minimalButtonDisplayPos: 100,
        scrollToPosition: 0,
        button: '.scroll-to-top'
    };

    function init(userSettings) {
        var settings = $.extend(defaultSettings, userSettings);
        scrollToTopBtn = $(settings.button);

        handleScrollButton();
        handleEvents(settings);
    }

    function handleEvents(settings) {
        scrollToTopBtn.on('click touch', function () {
            scrollTo(settings.scrollToPosition);
        });
    }

    function scrollTo(pos) {
        pos = (typeof pos === 'number' ? pos : 0);

        $('html,body').animate({
            scrollTop: pos
        }, 'slow');
    }

    function getViewPortTop() {

        return window.pageYOffset
    }

    function validateScrollDisplay() {
        if (getViewPortTop() < defaultSettings.minimalButtonDisplayPos) {
            scrollToTopBtn.fadeOut();
        } else {
            scrollToTopBtn.fadeIn();
        }
    }

    function handleScrollButton() {
        $(window).on('scroll', validateScrollDisplay);
    }

    init();

    return {
        init: function (settings) {
            init(settings);
        }
    }
});