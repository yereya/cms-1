/**
 *  Lightbox Effect module
 */
define('lightBox', ['jquery','bootstrap','lightbox'], function ($) {

    /**
     *  Initialize all
     */
    function init() {
        initEvents();
    }

    /**
     *  Init all events
     */
    function initEvents() {
        $('body').on('click',
            '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    }

    init();
});
