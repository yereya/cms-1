/**
 *  Material Design Module
 */
define('materialDesign', ['jquery'], function ($) {

    /**
     *
     * @type {{inputs: string}}
     */
    var selectors = {
        inputs:'.form-md-floating-label .form-control',
    }

    /**
     * Initialize on load
     */
    function init() {
        $(selectors.inputs).each(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('edited');
            }
        });
        initEvents();
    }

    /*
        Trigger Events
     */
    function initEvents() {
        var $body = $('body');
        var selector = getEffectSelector($body);

        /**
         * Get specific selector per Page Size
         * @param $body
         * @returns {string}
         */
        function getEffectSelector($body) {
            return $body.hasClass('page-md')
                ? '.md-checkbox > label, .md-radio > label'
                : 'a.btn, button.btn, input.btn, label.btn';
        }

        $body.on('click', selector, resolveEffects);
        $body.on('keydown', selectors.inputs, function (e) {
            handleInput($(this));
        });
        $body.on('blur', selectors.inputs, function (e) {
            handleInput($(this));
        });
    }

    /**
     *
     * Resolve effects
     *
     * @param e
     */
    function resolveEffects(e) {

        var element, circle, d, x, y;
        var hasClassPageMd = $('body').hasClass('page-md');

        if (hasClassPageMd) {
            resolveMediumPageEffect(this);

            return;
        }

        return resolveGeneralEffects(this);

        /**
         *
         * @param element
         */
        function resolveGeneralEffects(element) {
            var that = $(element);
            // find the first span which is our circle/bubble
            var el = that.children('span:first-child');

            // add the bubble class (we do this so it doesnt show on page load)
            el.addClass('inc');

            // clone it
            var newone = el.clone(true);

            // add the cloned version before our original
            el.before(newone);

            // remove the original so that it is ready to run on next click
            $("." + el.attr("class") + ":last", that).remove();
        }

        /**
         *
         * @param element
         */
        function resolveMediumPageEffect(that) {
            element = $(that);

            if (element.find(".md-click-circle").length == 0) {
                element.prepend("<span class='md-click-circle'></span>");
            }

            circle = element.find(".md-click-circle");
            circle.removeClass("md-click-animate");

            if (!circle.height() && !circle.width()) {
                d = Math.max(element.outerWidth(), element.outerHeight());
                circle.css({height: d, width: d});
            }

            x = e.pageX - element.offset().left - circle.width() / 2;
            y = e.pageY - element.offset().top - circle.height() / 2;

            circle.css({top: y + 'px', left: x + 'px'}).addClass("md-click-animate");

            setTimeout(function () {
                circle.remove();
            }, 1000);

        }
    }

    // Floating labels
    function handleInput() {
        if (el.val() != "") {
            el.addClass('edited');
        } else {
            el.removeClass('edited');
        }
    }

    init();
});
