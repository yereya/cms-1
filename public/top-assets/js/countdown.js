/**
 * Add data-countdown, data-end_date and data-start_date attributes to the countdown wrapper element
 * Add days/hours/minutes/seconds classes to your clock elements
 */

define('countdownJs',['jquery'], function ($) {
    var countables = {};
    var timer = 0;
    var selectors = {
        deadline: 'end_date',
        startDate: 'start_date',
        container: '[data-countdown]',
        timeElements: {
            'days': '.days',
            'hours': '.hours',
            'minutes': '.minutes',
            'seconds': '.seconds'
        }
    };

    var divisors = {
        days: (1000 * 60 * 60 * 24),
        hours: (1000 * 60 * 60),
        minutes: (1000 * 60),
        seconds: 1000
    };

    var remainders = {
        days: (1000* 1000 * 60 * 60 * 24),
        hours: (1000 * 60 * 60 * 24),
        minutes: (1000 * 60 * 60),
        seconds: (1000 * 60)
    }

    function runClockTermination(clock, countdown) {
        clearInterval(clock);
        if (typeof alterClock === 'function') {
            try {
                alterClock(countdown);
            } catch (err) {
                console.log(err);
            }
        }
    }

    function updateRemainingTime(countDownDate, clock, countdown, index) {
        countables['timer']['countdown' + index] += 1000;
        var difference  = countDownDate - countables['timer']['countdown' + index];
        if (difference <= 0) {
            runClockTermination(clock, countdown);
        }

        for (var timeUnit in countables['countdown' + index]) {
            var updatedTime = Math.floor((difference % remainders[timeUnit])/ (divisors[timeUnit]));
            updatedTime     = ('0' + updatedTime).slice(-2);
            countables['countdown' + index][timeUnit].text(updatedTime);
        }
    }

    function attachCountdownEvents() {
        timer             = (new Date($(selectors.container).data(selectors.startDate))).getTime();
        var countDownClocks    = $(selectors.container);
        countables['timer'] = {};

        $.each(countDownClocks, function(index, countdown) {
            var countDownContainer = $(countdown);
            var countDownDate = (new Date(countDownContainer.data(selectors.deadline))).getTime();
            // initializes countable variables for the current clock
            countables['countdown' + index] = {};
            countables['timer']['countdown' + index] = timer;

            for (var timeUnit in selectors.timeElements) {
                countables['countdown' + index][timeUnit] = countDownContainer.find(selectors.timeElements[timeUnit]);
            }

            (function (countdownVar, i) {
                var clock = setInterval(function () {
                    updateRemainingTime(countDownDate, clock, countdownVar, i);
                }, 1000);
            })(countdown, index);
        });
    }

    function init() {
        attachCountdownEvents();
    }

    init();
});