define('rating', ['jquery', 'generalHelpers'], function ($, helper) {

    var form = '.rating-api-form';
    var starsRatingSelector = '.tp-rating';
    var ratingInputSelector = '.comment-box-rating input[name="rating"]';
    var ratingDescription = ['Poor', 'Average', 'Good', 'Great', 'Excellent'];
    var rates = helper.getCookie('postsRating') ? JSON.parse(helper.getCookie('postsRating')) : [];

    /**
     * 5 star rating library
     *
     * @description The rating component.
     * @param {HTMLElement} el The HTMl element to build the rating widget on
     * @param {string} currentRating The current rating value
     * @param {string} maxRating The max rating for the widget
     * @param {Function} callback The optional callback to run after set rating
     * @return {Object} Some public methods
     */
    function rating(el, currentRating, maxRating, callback) {

        /**
         * stars
         *
         * @description The collection of stars in the rating.
         * @type {Array}
         */
        var stars = [];

        /**
         * Generic build for all stars
         */
        function init() {

            if (!el) {
                throw Error('No element supplied.');
            }
            if (!maxRating) {
                maxRating = 5;
            }
            if (!currentRating) {
                currentRating = 0;
            }
            if (currentRating < 0 || currentRating > maxRating) {
                throw Error('Current rating is out of bounds.');
            }

            for (var i = 0; i < maxRating; i++) {
                var star = $('<li>');
                var icomoon = $('<i>');
                var roundedCurrentRating = Math.floor(parseFloat(currentRating));
                star.addClass('tp-rating__item');
                star.attr({
                    'data-description': ratingDescription[i],
                    'data-index': i
                });
                icomoon.addClass('stars')
                if (i < currentRating) {
                    if (i == roundedCurrentRating && currentRating > roundedCurrentRating) {
                        star.addClass('half');
                    } else {
                        star.addClass('active');
                    }

                }
                el.append(star);
                star.append(icomoon);
                stars.push(star);

                var wrapper = $(star).closest('ul');
                var post_id = wrapper.data('post_id');
                var $scoreSpan = $(star).closest('.rating').find('.score');
                if (rates.indexOf(post_id) == -1) {
                    attachEvents(star);
                } else {
                    $scoreSpan.text('Thank You!');
                }
            }
        }

        /**
         * Attach Star Events
         *
         * @description Attaches events to each star in the collection. Returns
         *   nothing.
         * @param {HTMLElement} star The star element
         */
        function attachEvents(star) {
            click(star);
            mouseOver(star);
            mouseOut(star);
        }

        /**
         * unAttach events
         *
         * @param stars
         */
        function unAttachEvents(stars, events) {
            stars.off(events);
        }

        /**
         * Set Cookies
         *
         * @param post_id
         * @param expiring
         */
        function setCookies(post_id, expiring) {
            rates.push(post_id);
            helper.setCookie('postsRating', JSON.stringify(rates), expiring);
        }

        /**
         * Listen To Element Click
         *
         * @description The click event for the star. Returns nothing.
         * @param {HTMLElement} star The star element
         */
        function click(star) {
            $(star).on('click', function (e) {
                e.preventDefault();

                var rate = parseInt(star.data('index')) + 1;
                var starWrapper = $(star).closest('ul');
                var stars = starWrapper.find('li');
                var post_id = starWrapper.data('post_id');

                set(rate, true);
                save(star);
                unAttachEvents(stars, 'click mouseover mouseout');
                setCookies(post_id, 30);
            });
        }

        /**
         * Star Mouse Over
         *
         * @description The mouseover event for the star. Returns nothing.
         * @param {HTMLElement} star The star element
         */
        function mouseOver(star) {
            $(star).on('mouseover', function (e) {
                iterate(stars, function (item, index) {
                    if (index <= parseInt(star.data('index'))) {
                        item.addClass('active');
                    } else {
                        item.removeClass('active half');
                    }
                });
                setDescription(star);
            });
        }

        /**
         * Star Mouse Out
         *
         * @description The mouseout event for the star. Returns nothing.
         * @param {HTMLElement} star The star element
         */
        function mouseOut(star) {
            $(star).on('mouseout', function (e) {
                if (stars.indexOf(e.relatedTarget) === -1) {
                    set(null, false);
                    unsetDescription(star);
                }
            });
        }

        /**
         * Set Rating
         *
         * @description Sets and updates the currentRating of the widget, and runs
         *   the callback if supplied. Returns nothing.
         * @param {jQuery} value The number to set the rating to
         * @param {Boolean} doCallback A boolean to determine whether to run the
         *   callback or not
         */
        function set(value, doCallback) {
            if (value && value < 0 || value > maxRating) {
                return;
            }
            if (doCallback === undefined) {
                doCallback = true;
            }

            currentRating = value || currentRating;

            iterate(stars, function (star, index) {
                if (index < currentRating) {

                    star.addClass('active');
                } else {
                    star.removeClass('active half');
                }
            });

            if (callback && doCallback) {
                callback(get());
            }
        }

        /**
         * Get Rating
         *
         * @description Gets the current rating.
         * @return {string} The current rating
         */
        function get() {
            return currentRating;
        }

        /**
         * Set Rate Description
         *
         * @param star
         */
        function setDescription(star) {
            var $scoreSpan = $(star).closest('.rating').find('.overall');
            var $descriptionSpan = $(star).closest('.rating').find('.description');
            var $description = $(star).data('description');
            if ($description) {
                $scoreSpan.addClass('hidden');
                $descriptionSpan.text($description).removeClass('hidden');
            }
        }

        /**
         * Unset Description
         *
         * @description unSet Rate Description
         *
         * @param star
         */
        function unsetDescription(star) {
            var $star = $(star);
            var $scoreSpan = $star.closest('.rating').find('.overall');
            var $descriptionSpan = $star.closest('.rating').find('.description');
            var $description = $star.data('description');
            $scoreSpan.removeClass('hidden');
            $descriptionSpan.text($description).addClass('hidden');
        }

        /**
         * Iterate
         *
         * @description A simple iterator used to loop over the stars collection.
         *   Returns nothing.
         * @param {Array} collection The collection to be iterated
         * @param {Function} callback The callback to run on items in the collection
         */
        function iterate(collection, callback) {
            for (var i = 0; i < collection.length; i++) {
                var item = collection[i];
                callback(item, i);
            }
        }

        /**
         * Save Rating
         *
         * @param star
         */
        function save(star) {
            this.prepare = function () {
                var $form = $(form);
                var $starUl = $(star);
                var post_id = $starUl.closest('ul').data('post_id');
                var rate = $starUl.data('index') + 1;
                $form.find('[name="post_id"]').val(post_id);
                $form.find('[name="rate"]').val(rate);

                this.ajax($form);
            }

            this.ajax = function ($form) {
                var that = this;
                var rateItParams = $form.serialize();
                var setting = {
                    url: $form.attr('action'),
                    method: $form.attr('method'),
                    data: rateItParams,
                    beforeSend: function () {
                    },
                    complete: function (confirm) {
                        if (confirm) {
                            that.set(confirm.responseText);
                        }
                    }
                }
                $.ajax(setting);
            }

            this.set = function () {
                var $scoreSpan = $(star).closest('.rating').find('.score');
                var avarage = $(star).closest('ul').data('current_rating');
                $scoreSpan.text('Thank You!');
                set(avarage, false);
            }

            this.prepare();
        }

        init();
    }


    /**
     * Init Rating
     *
     * @description Initial rating generate with data-configurations
     */
    function init() {
        $(starsRatingSelector).each(function (key, value) {
            var element = $(value);
            var currentRating = element.data('current_rating');
            var maxRating = element.data('max_rating');
            var callback = function (ratingValue) {
                $(ratingInputSelector).val(ratingValue);
            };

            rating(element, currentRating, maxRating, callback);
        });
    }

    init();
});



