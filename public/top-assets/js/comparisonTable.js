define('comparisonTable', ['jquery'], function ($) {

    var $comparison = $('#comparison');
    var isMobile = $('body').hasClass('mobile');
    var brandSelectedCount = isMobile ? 2 : 5;

    var logos = $comparison.find('.comparison-logos');
    var table = $comparison.find('.comparison-table');

    /**
     * init comparison
     */
    function init () {
        table.hide();

        $.each(logos.find('.brand-logo-container'), function () {
            $(this).addClass('not-selected');

            $(this).on('click', comparisonSelected);
        });

        var index = 1;
        $.each(table.find('.comparison-column'), function () {
            $(this).attr('index', index);
            index++;
            $(this).hide();
        })
    }

    function comparisonSelected() {
        if ($(this).hasClass('not-selected')) {
            if (!table.is(':visible')) {
                table.show();
                hideTablePlaceHolder();
            }
            if (brandSelectedCount >= 1) {
                $(this).removeClass('not-selected');
                brandSelectedCount--;
                showColumn(this);
            }
        } else {
            $(this).addClass('not-selected');
            brandSelectedCount++;
            hideColumn(this);

            if (brandSelectedCount == 5 || ( isMobile && brandSelectedCount == 2 ) ) {
                table.hide();
            }
        }
    }

    function showColumn(element) {
        var index = -1;
        var showOrderElement = function () {
            var brand_id = $(element).attr('brand_id');
            var column = $comparison.find('.comparison-column#brand_id_' + brand_id);
            column.css('order', parseInt(index + 1));
            column.fadeIn(600);
        }

        // reorder();
        showOrderElement();
    }

    function hideColumn(element) {
        var brand_id = $(element).attr('brand_id');
        var column = $comparison.find('.comparison-column#brand_id_' + brand_id);
        column.fadeOut(600);
    }

    /**
     * Hide any content under this div after table is displayed
     */
    function hideTablePlaceHolder() {
        $('.comparison-table-placeholder').hide();
    }

    init();

    return {
        init: function() {
            init();
        }
    }
});