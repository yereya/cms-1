define('bookmark', ['jquery'], function($) {
    function init() {
        $('.add_to_bookmarks').click(function (event) {
            event.preventDefault();
            var bookmarkURL = window.location.href;
            var bookmarkTitle = document.title;

            if ('addToHomescreen' in window && window.addToHomescreen.isCompatible) {
                addToHomescreen({autostart: false, startDelay: 0}).show(true);
            } else if (window.sidebar && window.sidebar.addPanel) {
                window.sidebar.addPanel(bookmarkTitle, bookmarkURL, '');
            } else if ((window.opera && window.print)) {
                $(this).attr({
                    href: bookmarkURL,
                    title: bookmarkTitle,
                    rel: 'sidebar'
                }).off(event);

                return true;
            } else if (window.external && ('AddFavorite' in window.external)) {
                window.external.AddFavorite(bookmarkURL, bookmarkTitle);
            } else {
                alert('Please press ' + (/Mac/i.test(navigator.userAgent) ? 'CMD' : 'CTRL') + ' + D to add this page to your favorites.');
            }

            return false;
        });
    }

    init();

    return {
        init: function () {
            init();
        }
    }
});