define('mobileSideBar', ['jquery', 'sidr'], /**
 *
 * @param $
 */
function ($) {

    /**
     * Basic Variables
     *
     * @type {*}
     */
    var $hamburger = $('.hamburger');
    var targetNavBar = $hamburger.data('target') || 'sidr';

    /**
     * Basic sidr-settings
     *
     * @type {{name: string, timing: string, source: string, speed: number}}
     */
    var settings = {
        name: targetNavBar,
        timing: 'ease-in-out',
        source: '.' + targetNavBar,
        speed: 500
    }

    /**
     * Initialize the sidr on load
     *
     * @param {{name: string, timing: string, source: string, speed: number}} settings
     * @param {string} setting.name - name of the wrapper class
     * @param {string} setting.timing - string delay on opening sidebar
     * @param {string} setting.source - source selector for sidebar
     * @param {number} setting.speed - speed of sidebar opening
     *
     * @return void|null
     */
    function init(settings) {
        $hamburger.attr('href', '.' + targetNavBar);
        $hamburger.sidr(settings);
        initEvents();
    }

    /**
     * Init Events
     *
     * @description In All Events
     * - trigger event by click
     * - change arrows sign according to the nav-bar situation
     * - show or hide ul sub-nav bar
     *
     * @return null|void
     */
    function initEvents() {
        var $navLinks = $('.sidr-inner>ul>li>a');
        var $body = $('body');
        var $window = $(window);

        $navLinks.on('click', toggleItems);
        $window.on('resize', closeSidr);
        $body.on('click touchstart',closeIfNotNavParent);
    }

    /**
     * Close sidr nav-bar
     */
    function closeSidr() {
        $.sidr('close', targetNavBar);
    }

    /*
        Close if not nav parent
     */
    function closeIfNotNavParent(e) {
        var $target = $(e.target);
        var mobileSideBarState = $target.parents('#mobile-side-bar').length;

        if(!mobileSideBarState){
            closeSidr();
        }
    }

    /**
     * Toggle Items
     * @param event
     */
    function toggleItems(event) {
        if($(this).siblings().is('ul')) {
            event.preventDefault();
            var $target = $(event.target);
            var $navItem = $target.closest('li');
            var $wrapper = $target.closest('ul');
            removeActiveExcept($wrapper,$navItem);
            toggleActive($navItem);
        }
    }

    /**
     * Toggle Active Class
     *
     * @param $link
     */
    function toggleActive($link) {
        $link.toggleClass('active');
    }

    /**
     * Close all beside specific element
     *
     * @param $wrapper
     * @param $navItem
     */
    function removeActiveExcept($wrapper,$exceptItem) {
        $wrapper.find('li').not($exceptItem).removeClass('active');
    }

    init(settings);

});