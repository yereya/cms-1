define('cookieConsent', ['jquery', 'jsCookie'], function ($, Cookies) {
    var selectors = {
        wrapper: '#cookie-consent'
    };
    var cookieName = 'cookie_consent';
    var closeAfterSeconds = 30;

    function toggleDisplay() {
        $(selectors.wrapper).slideToggle(1000);
    }

    function setHideEvent() {
        $(selectors.wrapper).click(function () {
            toggleDisplay();
        });
    }

    function hideOnTimeout() {
        setTimeout(function() {
            if ($(selectors.wrapper).is(":visible")) {
                toggleDisplay();
            }
        }, closeAfterSeconds * 1000)
    }

    function createConsentCookie() {
        Cookies.set(cookieName, 'true');
    }

    function init() {

        if (typeof Cookies.get(cookieName) == 'undefined') {
            createConsentCookie();
            toggleDisplay();
            setHideEvent();
            hideOnTimeout();
        }
    }

    init();
});