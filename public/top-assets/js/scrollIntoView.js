define('scrollIntoView', ['jquery', 'generalHelpers'], function ($, generalHelper) {

    var $hrefLinks = $('a[href^="\\#"]');

    /**
     * Scroll Into View
     *  - data-target
     *  - data-margin
     */
    function scrollIntoViewByTarget() {
        $hrefLinks.on('click', function (e) {
            e.preventDefault();
            var $href = $(e.target);
            $href = $href.is('a') ? $href : $href.closest('a');
            var $target = $($href.data('target'));
            var $marginFromTop = $href.data('margin') ? parseInt($href.data('margin')) : 20;

            generalHelper.scrollIntoView($target, $marginFromTop);
        })
    }

    function init() {
        scrollIntoViewByTarget();
    }

    init();
});
