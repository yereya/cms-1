define('likes', ['jquery', 'generalHelpers'], function ($, helper) {

        var selectors = {
            wrapper: ".likes, .dislikes",
            sum: '.count',
            form: '.post-likes-form'
        }
        var postLikes = helper.getCookie('postLikes') ? JSON.parse(helper.getCookie('postLikes')) : [];

        /**
         *  Initialize Script
         */
        function init() {
            $(selectors.wrapper).each(function () {
                var target = $(this);
                var post_id = target.data('post_id');
                if (postLikes.indexOf(post_id) === -1) {
                    attachEvents(target, post_id);
                    return;
                }
                disable(selectors.wrapper);
            })
        }

        /**
         * Attach Events
         */
        function attachEvents(target) {
            target.on('click', click);
        }

        /**
         * Event Click
         */
        function click() {
            var post_id = $(this).data('post_id');
            var $form = $(selectors.form);
            set(this);
            prepare($form, this);
            save($form, this);
            setCookie(post_id, 30);
            disable(this);
        }

        /**
         * Prepare Form With Like Props
         *
         * @param $form
         * @param target
         */
        function prepare($form, target) {
            var $target = $(target);
            var type = $target.data('type');
            var post_id = $target.data('post_id');

            $form.find('[name="type"]').val(type);
            $form.find('[name="post_id"]').val(post_id);
        }

        /**
         * Save Like In Post Id
         *
         * @param form
         */
        function save($form, target) {

            var setting = {
                url: $form.attr('action'),
                method: $form.attr('method'),
                data: $form.serialize() + '&_token=' + Top.csrf,
                complete: function (response) {
                    var response = (helper.isJson(response.responseText)) ? JSON.parse(response.responseText) : '';
                    if (typeof response != 'undefined' && typeof response.likes != 'undefined') {
                        var likes = response.likes;
                        set(target, likes);
                    }
                }
            }
            $.ajax(setting);

        }

    /**
     * Set new Like
     *
     * @param target
     */
    function set(target, likes) {
        var wrapper = $(target);
        var sum = wrapper.find(selectors.sum);
        var sumUpdated = (sum.text()) + 1;
        if (likes && sumUpdated != likes) {
            sum.text(parseInt(likes));
            return;
        }
        sum.text(parseInt(sum.text()) + 1);
        }

        /**
         *  add New Post_id to PostLikes
         *
         * @param post_id
         * @param expiring
         */
        function setCookie(post_id, expiring) {
            postLikes.push(post_id);
            helper.setCookie('postLikes', JSON.stringify(postLikes), expiring);
        }


    /**
     * Disable Like Rating
     *
     * @param $wrapper
     */
    function disable(wrapper) {
        $(wrapper).addClass('disabled');
        detachEvent(wrapper);
    }


        /**
         * UnAttach Event
         *
         * @param likes
         */
        function detachEvent(likes) {
            $(likes).off('click');
        }

        init();
    }
);
