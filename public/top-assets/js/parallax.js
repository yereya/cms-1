/**
 * Parallax module
 *
 *  - this module requires css style - (.parallax)
 *
 * @description will evaluate when class = parallax_ends_here will be set
 *
 *  - will get the div bottom position
 *  - will set the height of the parallax according to it
 *  - will append the element to body
 *
 */
define('parallax', ['jquery'], function ($) {
    var selectors = {
        'endPoint': '.parallax_ends_here',
        'page_wrapper':'.main-content-wrapper'
    };

    /**
     * Initialize the process
     *
     * - Find the end point selector
     * - Prepare the parallax div for appending
     * - Prepaid the div to the start of the body
     *
     */
    function init() {
        var $endPoint = $(selectors.endPoint);
        var parallax = prepare($endPoint);
        $('body').prepend(parallax);
    }

    /**
     * Prepare the new parallax div for appending
     *
     * @param height
     * @returns {*|jQuery|HTMLElement}
     */
    function prepare(end_point) {
        var height = end_point.offset().top + end_point.outerHeight(true) - 100;
        var image_url = getImageUrl();
        var element = $('<div>');

        element.addClass('parallax');
        element.css('height', height);
        element.css('background-image',image_url);

        return element;
    }

    /**
     * Get background image from main-wrapper
     *
     * @returns {*|jQuery}
     */
    function getImageUrl() {
        var image = $(selectors.page_wrapper).css('background-image');
        $(selectors.page_wrapper).css('background-image','none');

        return image;
    }

    init();
});