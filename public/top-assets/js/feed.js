define('feed', ['jquery', 'slick'], function ($) {
    /**
     * Feed object.
     *
     * @param feedWrapper
     **/
    var Feed = function (feedWrapper) {

        /**
         * @ver Element
         **/
        var wrap;

        this.constructor = function () {
            wrap = feedWrapper.find(".ticker");
            feedWrapper.find(".back-arrow").click(this.prevButtonClicked);
            this.loadFeed();
        };

        /**
         * Handle top right arrow click event.
         **/
        this.prevButtonClicked = function () {
            wrap.slick('slickPrev');
        };

        /**
         * Make a get request to fetch feed data.
         **/
        this.loadFeed = function(){
            $.get("/api/feed/sport", {feed_id: wrap.data('feed-id')}).then(function (slides) {
                slides.forEach(function (slide) {
                    var div = $("<div>");
                    div.append($("<strong>", {text: slide.text}));
                    div.append($("<span>", {text: slide.date_time}));
                    wrap.append(div)
                });
                this.setupSlider(slides.length-1);
            }.bind(this));
        };

        /**
         * Make a slick slider.
         *
         * @param numberOfSlides
         **/
        this.setupSlider = function(numberOfSlides){
            var slides_to_show = parseInt(wrap.data('slides-to-show'));
            if (slides_to_show === 0){
                slides_to_show = (numberOfSlides > 7) ? 7 : numberOfSlides;
            }

            wrap.slick({
                prevArrow: '',
                nextArrow: '',
                infinite: true,
                autoplay: true,
                slidesToScroll: 1,
                slidesToShow: slides_to_show,
                autoplaySpeed: parseInt(wrap.data('scroll-delay')) * 1000,
                variableWidth: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        };

        this.constructor();
    };

    // Loop each widget ticker and create a new ticker instance.
    $('.widget_feed_wrapper').each(function () {
        new Feed($(this));
    });
});