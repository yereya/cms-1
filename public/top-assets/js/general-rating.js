define('general-rating', ['jquery'], function ($) {
    (function(selector) {
        $(selector).each(function () {
            var input = $(this);
            var ul = $("<ul>", {class: 'rating'});

            var create_item = function (i) {
                return $("<li>", {class: 'tp-rating__item', 'data-index': i}).append($("<i>", {class: 'stars'}));
            };

            for (var i = 1; i < 6; i++) {
                ul.append(create_item(i));
            }

            var noevents = input.attr('data-noevents');

            if (noevents !== 'true') {
                ul.find("li").click(function () {
                    var index = $(this).attr('data-index');

                    input.val(index);
                    activate_by_index(index);
                });

                ul.find("li").mousemove(function () {
                    var index = $(this).attr('data-index');
                    activate_by_index(index);
                });
            }

            var activate_by_index = function (index) {
                index = parseInt(index);
                ul.find("li").removeClass('active');
                ul.find("li").each(function () {
                    if (parseInt($(this).attr('data-index')) <= index) {
                        $(this).addClass('active');
                    }
                })
            };

            input.attr("type", "hidden");
            $(input.parent('div')).append(ul);

            var index = input.val();
            if (index !== '') {
                activate_by_index(index);
            }
        })
    }('.rating-input'));
});