define('generalHelpers', ['jquery'], function ($) {

    /**
     * Scroll Into View
     * @param selector - element to scroll into
     * @param marginFromTop - number that will be removed from scroll to create gap in top of the scroll
     */
    function scrollIntoView($element, marginFromTop) {
        if ($element.length) {
            var heightFromTheTop = ($element.offset().top) - marginFromTop;
            $('body,html').animate(
                {scrollTop: heightFromTheTop}
                , 1000);
        } else {
            console.log('no element found with the target specified');
        }
    }

    /**
     * Get Cookie By Name
     * @param name
     * @returns {*}
     */
    function getCookie(name) {
        var value = "; " + document.cookie;
        var parts = value.split("; " + name + "=");
        if (parts.length == 2) return parts.pop().split(";").shift();
    }

    /**
     * Check If str is json
     *
     * @param str
     * @returns {boolean}
     */
    function isJson(jsonString) {
        try {
            var o = JSON.parse(jsonString);

            // Handle non-exception-throwing cases:
            // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
            // but... JSON.parse(null) returns null, and typeof null === "object",
            // so we must check for that, too. Thankfully, null is falsey, so this suffices:
            if (o && typeof o === "object") {

                return true;
            }
        }
        catch (e) {

            return false;
        }

        return false;
    };

    /**
     * Check if number is negative
     * @param number
     * @returns {boolean}
     */
    function isNegative(number) {
        return number < 0
    }

    /**
     *
     * Set New Cookie By :
     *
     * @param name - cookie name
     * @param value = cookie value
     * @param expireDays = expired days
     */
    function setCookie(name, value, expireDays) {
        var date = new Date();
        date.setTime(date.getTime() + (expireDays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + date.toUTCString();
        document.cookie = name + "=" + value + ";" + expires + ";path=/";
    }

    /**
     * Count words
     *
     * @param str
     * @param separator
     */
    function countWords(str, separator) {
        return str.split(separator).length;
    }

    /**
     * Capitalize string
     *
     * @param string
     */
    function capitalizeString(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    /**
     * To slug
     *
     * @param str
     * @returns {string}
     */
    function toSlug(string) {
        string = string.toLowerCase();
        string = string.replace(/\(|\)/g, '');
        return string.replace(/ /g, '_');
    }

    return {
        scrollIntoView: function ($element, marginFromTop) {
            if ((typeof $element == 'object') && (typeof marginFromTop == "number")) {
                scrollIntoView($element, marginFromTop);
            }
        },
        isJson: function (str) {
            if (typeof str != 'undefined' && typeof str == 'string') {
                return isJson(str);
            } else {
                throw 'isJson must get param - str. ';
            }
        },
        isNegative: function (number) {
            if (typeof number != 'undefined' && typeof number == 'number') {
                return isNegative(number)
            } else {
                throw 'isNegative must get param - number.';
            }
        },
        getCookie: function (name) {
            if (typeof name != 'undefined' && typeof name == 'string') {

                return getCookie(name);
            } else {

                throw 'getCookie must get param - name.'
            }
        },
        setCookie: function (name, value, expireDays) {
            if (typeof name != 'undefined' && typeof value != 'undefined' && typeof expireDays != 'undefined') {
                setCookie(name, value, expireDays);
            } else {

                throw 'setCookie must get all 3 params - name,value,expireDays.';
            }
        },
        countWords: function (string, separator) {
            if (typeof string != 'undefined' && typeof string == 'string') {
                return countWords(string, separator);
            }
        },
        capitalizeString: function (string) {
            if(typeof string == 'string'){
                return capitalizeString(string);
            }
        },
        toSlug:function (string) {
            if(typeof string == 'string'){
                return toSlug(string);
            }
        }
    }
});
