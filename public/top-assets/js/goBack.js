define('goBack', ['jquery'], function ($) {
    $(".goBackButton").click(function (){
        if (window.history.length > 1){
            return window.history.back();
        }
        var port = window.location.port;
        if (port){
            port = ':' + port;
        }
        window.location.href = window.location.protocol + '//' + window.location.hostname + port;
    });
});