define('ajax-forms', ['jquery_with_custom_validation'], function ($) {

    var formSuccessHtmlSelector = '.success-message-template';
    var fieldsFailHtmlSelector = '.fields-fail-message-template';
    var generalFailHtmlSelector = '.general-fail-message-template';
    var messageBoxSelector = '.message-box';
    var subscribe = '.subscribe';
    var errors ='div[class$=error]';

    var reUseAbleMessages = {
        name: {
            required: "Please enter your name",
            minlength: "Name should be longer than two chars",
        },
        email: {
            required: "Please enter your email",
            customEmailValidation: "Please enter a valid email address."
        },
        message: {
            required: "Please enter your message",
            minlength: "Message should be longer than 50 chars"
        },
        accept_term: {
            required: "Please accept Terms & Conditions",
        },
        hiddenRecaptcha: "Please enter captcha"
    };

    /**
     *
     * @param state
     *
     */
    function showRelevantSubmitText(state) {
        var button = $(subscribe);
        state = button.data(state)
        button.text(state);
    }

    /**
     *
     * @param error
     * @param $error_element
     * @param errors
     */
    function showErrorsWithDelay(error, $error_element,errors) {
        showRelevantSubmitText('process');
        setTimeout(function () {
            if ($error_element.length) {
                $error_element.html(error.html());
            } else {
                error.insertAfter(element);
            }
            showRelevantSubmitText('ready');
            errors.show();
        }, 1000);


    }

    /**
     *
     * @param error
     * @param element
     * @param error_prefix
     */
    var generalErrorPlacement = function (error, element, error_prefix) {
        var error_element = error_prefix + element.attr("name") + '_error';
        var $error_element = $('.' + error_element);
        var $errors = $(errors);
        if($errors.length){
          $errors.hide();
        }
        showErrorsWithDelay(error,$error_element,$errors);
    };


    var recaptchaRule = {
        required: function () {
            if (grecaptcha.getResponse() == '') {
                return true;
            } else {
                return false;
            }
        }
    };

    var form_params_by_type = {
        'newsletter': {
            url: '/api/newsletter',
            validationObject: {
                rules: {
                    name: {
                        required: true,
                        minlength: 3
                    },
                    email: {
                        required: {
                            depends: function () {
                                $(this).val($.trim($(this).val()));
                                return true;
                            }
                        },
                        customEmailValidation: true
                    },
                    newsletter_tc: {
                        required: true
                    }
                },
                messages: {
                    name: reUseAbleMessages.name,
                    email: reUseAbleMessages.email,
                    newsletter_tc: reUseAbleMessages.accept_term,
                    hiddenRecaptcha: reUseAbleMessages.hiddenRecaptcha,
                },
                errorPlacement: function (error, element) {
                    var error_prefix = 'newsletter_';
                    generalErrorPlacement(error, element, error_prefix);

                },
                success: function (error) {
                },
            },
        },
        'contact-us': {
            url: '/api/contact-us',
            validationObject: {
                rules: {
                    name: {
                        required: true,
                        minlength: 3
                    },
                    message: {
                        required: true,
                        minlength: 50
                    },
                    email: {
                        required: {
                            depends: function () {
                                $(this).val($.trim($(this).val()));
                                return true;
                            }
                        },
                        customEmailValidation: true
                    },
                },
                messages: {
                    message: reUseAbleMessages.message,
                    name: reUseAbleMessages.name,
                    email: reUseAbleMessages.email,
                    hiddenRecaptcha: reUseAbleMessages.hiddenRecaptcha,
                }
            },
        }
    };

    /**
     * Register Form
     *
     * @param formType
     */
    function registerForm(formSelector, formType) {
        var formParams = form_params_by_type[formType];
        var mainValidationObject = {
            ignore: ".ignore_validation", // selector to ignore validation must be here even if not used for validation of hidden fields
            submitHandler: function (form) {
                send(formSelector, formParams);

                return false;
            }
        };

        var validationObject = $.extend(mainValidationObject, formParams.validationObject);
        $(formSelector).validate(validationObject);
    }

    /**
     * Send Comment
     */
    function send(formSelector, formParams) {
        var params = $(formSelector).serialize();
        params += '&_token=' + Top.csrf;

        $.post(formParams.url, params)
            .done(function (data) {
                if (data.status == '1') {
                    var successHtml = $(formSelector + ' ' + formSuccessHtmlSelector).html();
                    $(formSelector).replaceWith(successHtml);
                } else {
                    addFailMessage(formSelector);
                }
                updateText();
            })
            .fail(function (jqXhr) {
                if (jqXhr.status === 422) {
                    addFailMessage(formSelector, fieldsFailHtmlSelector);

                } else {
                    addFailMessage(formSelector, generalFailHtmlSelector);
                }
            });
    }

    /**
     *
     * @param formSelector
     * @param messageSelector
     */
    function addFailMessage(formSelector, messageSelector) {
        var failHtml = $(formSelector + ' ' + messageSelector).html();
        $(formSelector + ' ' + messageBoxSelector).html(failHtml);
    }

    return {
        init: function (formSelector, formType) {
            registerForm(formSelector, formType);
        }
    }
});
