define('popup', ['jquery', 'magnific-popup'], function ($) {

    // The Popup will listen to click event on this css selector for setting optin cookie if asked
    var POPUP_OPT_IN_SELECTOR  = ".popup-optin";

    /**
     * @type {boolean}
     */
    var page_scroll_event_started = false;

    /**
     * @type {boolean}
     */
    var page_scroll_listeners = [];

    /**
     *
     * @type {boolean}
     */
    var inactivity_event_started = false;

    /**
     *
     * @type {Array}
     */
    var inactivity_listeners = [];

    /**
     * Start
     */
    function start() {
        $.each(tp_popup_list, function (index, params) {
            switch (params['appear_event']) {
                case 'on_page_load':
                    var popupInstance = new Popup(params);
                    popupInstance.onPageLoad();
                    break;
                case 'on_mouse_exit':
                    onMouseExit({
                        delay: 5000, // 5 seconds delay before detection mouse exit.
                        threshold: 8,
                        callback: function(isFirstTime){
                            if (!isFirstTime){
                                return;
                            }
                            var popupInstance = new Popup(params);
                            popupInstance.openPopup();
                        }
                    })
                    break;
                case 'scroll_page':
                    var popupInstance = new Popup(params);
                    popupInstance.onScrollPage();
                    break;
                case 'x_seconds_of_inactivity':
                    var popupInstance = new Popup(params);
                    popupInstance.onXSecondsOfInactivity();
                    break;
            }
        });
    }

    /**
     * Start Listen On Page Scroll
     */
    function startListenOnPageScroll() {
        if (page_scroll_event_started) {
            return;
        }

        function onScrollEvent(percentage) {
            $.each(page_scroll_listeners, function (index, params) {
                if (percentage > params['param']) {
                    params['popup'].openPopup(params);
                    page_scroll_listeners.splice(index, 1);
                }
            });
        }

        $(window).scroll(function (e) {
            var scrollTop = $(window).scrollTop();
            var docHeight = $(document).height();
            var winHeight = $(window).height();
            var scrollPercent = (scrollTop) / (docHeight - winHeight);
            var scrollPercentRounded = Math.round(scrollPercent * 100);
            onScrollEvent(scrollPercentRounded)
        });
    }

    /**
     * Detect when a visitor mouse it out of the browser.
     *
     * @param Object options
     *
     * @return void
     **/
    function onMouseExit(options){
        if (typeof options !== 'object' ||
            typeof options.callback !== 'function'){
            throw 'Invalid options.';
        }
        var firstTime = true;
        var maxPosition = {x:0,y:0};
        var onMoseMove = function(event){
            var x = event.pageX - window.pageXOffset,
                y = event.pageY - window.pageYOffset,
                threshold = options.threshold || 40;

            if (x > maxPosition.x){
                maxPosition.x = x;
            }

            if (y > maxPosition.y){
                maxPosition.y = y;
            }

            if (x <= threshold && maxPosition.x > threshold ||
                y <= threshold && maxPosition.y > threshold ||
                x >= (window.innerWidth-threshold) && maxPosition.x > threshold ||
                y >= (window.innerHeight-threshold) && maxPosition.y > threshold) {
                options.callback(firstTime);
                firstTime = false;
            }
        }
        setTimeout(function (){
            document.onmousemove = onMoseMove;
        },options.delay || 0);
    }

    /**
     * On In Activity
     */
    function onInActivity() {

        if (inactivity_event_started) {

            return;
        }

        var idleTime = 0;

        //Increment the idle time counter every minute.
        var idleInterval = setInterval(timerIncrement, 1000); // 1 second

        //Zero the idle timer on mouse movement.
        $(this).mousemove(function (e) {
            idleTime = 0;
        });
        $(this).keypress(function (e) {
            idleTime = 0;
        });

        function timerIncrement() {
            idleTime = idleTime + 1;
            $.each(inactivity_listeners, function (index, params) {
                if (idleTime > params['param']) {
                    params['popup'].openPopup(params);
                    inactivity_listeners.splice(index, 1);
                    if (inactivity_listeners.length == 0) {
                        clearInterval(idleInterval);
                    }
                }
            });
        }
    }

    /**
     * Popup Class
     *
     * @param params
     * @constructor
     */
    function Popup(params) {

        this.params = params;

    };

    Popup.prototype = {

        /**
         * On Page Load
         */
        onPageLoad: function () {
            if (this.isCookieExists()) {

                return;
            }

            if (this.params['param']) {
                this.openAfter()
            } else {
                this.openPopup();
            }
        },

        /**
         * Open After
         */
        openAfter: function () {
            var self = this;
            setTimeout(function () {
                self.openPopup()
            }, this.params['param'] * 1000);
        },

        /**
         * Open Popup
         */
        openPopup: function () {
            var self = this;
            $.magnificPopup.open({
                items: {
                    src: this.params['url']
                },
                type: 'ajax',
                closeOnContentClick: false,
                closeOnBgClick: false,
                showCloseBtn: true,
                enableEscapeKey: false,
                callbacks: {
                    ajaxContentAdded: function () {
                        self.afterPopupDisplay();
                    },

                    close: function () {
                        self.setCloseCookie();
                    }
                }
            });

        },

        /**
         *  After Popup Display
         */
        afterPopupDisplay: function() {
            $(POPUP_OPT_IN_SELECTOR).click(function() {
                this.setConversionCookie();
            });

            var css_params = {};
            if(this.params['height']) {
                css_params['height'] = this.params['height'];
            }

            if(this.params['width']) {
                css_params['width'] = this.params['width'];
            }

            $(".popup-wrapper").css(css_params);

        },

        /**
         * On Scroll Page
         */
        onScrollPage: function () {
            if (this.isCookieExists()) {

                return;
            }

            if (this.params['param'] && this.params['url']) {
                this.params['popup'] = this;
                page_scroll_listeners.push(this.params);
                startListenOnPageScroll();
            }
        },

        /**
         * On X Seconds Of Inactivity
         */
        onXSecondsOfInactivity: function () {
            if (this.isCookieExists()) {

                return;
            }

            if (this.params['param']) {
                this.params['popup'] = this;
                inactivity_listeners.push(this.params);
                onInActivity();
            }
        },

        /**
         * Is Cookie Exists
         * @returns {boolean}
         */
        isCookieExists: function () {
            if (this.getCloseCookie() || this.getConversionCookie()) {

                return true;
            }
        },
        /**
         * Get Conversion Cookie Name
         */
        getConversionCookieName: function () {

            return this.params['cookie_name'] + '_optin';
        },

        /**
         * Get Close Cookie Name
         */
        getCloseCookieName: function () {

            return this.params['cookie_name'] + '_close';
        },

        /**
         * Get Conversion Cookie
         */
        getConversionCookie: function () {

            return this.getCookie(this.getConversionCookieName());
        },

        /**
         * Get Close Cookie
         */
        getCloseCookie: function () {

            return this.getCookie(this.getCloseCookieName());
        },

        /**
         * Set Conversion Cookie
         */
        setConversionCookie: function () {
            if (this.params['cookie_name'] && this.params['cookie_time_on_conversion']) {
                var cookie_name = this.getConversionCookieName();
                var days = this.params['cookie_time_on_conversion'];

                this.setCookie(cookie_name, true, days);
            }
        },

        /**
         * Set Close Cookie
         */
        setCloseCookie: function () {
            if (this.params['cookie_name'] && this.params['cookie_time_on_close']) {
                var cookie_name = this.getCloseCookieName();
                var days = this.params['cookie_time_on_close'];

                this.setCookie(cookie_name, true, days);
            }
        },
        /**
         * Get Cookie
         */
        getCookie: function (cookie_name) {
            var name = cookie_name + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },

        /**
         * Set Cookie
         */
        setCookie: function (cookie_name, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toUTCString();
            document.cookie = cookie_name + "=" + cvalue + ";" + expires + ";path=/";
        }
    };

    /**
     * Start after it load
     */
    start();
});

