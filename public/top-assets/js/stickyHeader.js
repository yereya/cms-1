/**
 * Sticky Header
 *
 * to add stick need two parameters
 *
 * data-stick="stick"
 * data-stick_container="class or id or tag"
 * data-stick_hide="true"
 */

define('stickyHeader', ['jquery', 'sticky'], function ($) {
    var
        //stick data attribute
        stickMeClass = $('[data-stick="stick"]'),
        //is block visible
        isVisible = false,
        //stick events
        stickEvents = {
            //scroll with stick
            sticking: 'sticking',
            //start scroll with stick
            stickyBegin: 'sticky-begin',
            //up to top page
            topReached: 'top-reached',
            //down to bottom page
            bottomReached: 'bottom-reached'
        };

    /**
     * Init stick header
     */
    var initStick = function () {
        var topOffset = 0,
            width = 0;

        stickMeClass.each(function () {
            var $element = $(this);
            if ($element.offset().top > topOffset) {
                topOffset = $element.offset().top;
            }

            if ($element.css('width') !== '100%') {
                width = $element.css('width');
            }
        });

        stickMeClass.css({
            'width': width,
            'z-index': '10000'
        });

        //stick properties
        stickMeClass.stickMe({
            transitionDuration: 300,
            shadow: false,
            shadowOpacity: 0.3,
            animate: false,
            triggerAtCenter: false,
            transitionStyle: 'fade',
            topOffset: topOffset,
            body: '.main-content-wrapper'
        });
    }

    /**
     * Init events
     *
     * event sticking - if use scroll about sticky element
     */
    var initEvents = function () {
        stickMeClass.on(stickEvents.sticking, function () {
            var stickElement = $(this),
                parent,
                parentBottom,
                stickElementBottom;
            parent = $(stickElement.data('stick_container'));

            if (!parent) {
                parent = stickElement.parent();
            }

            parentBottom = parent.offset().top + parent.height();

            stickElementBottom = stickElement.offset().top + stickElement.height();

            if (stickElement.css('position') == 'relative') {
                stickElement.css('display', '');
            }

            if ((parentBottom <= stickElementBottom) && !stickElement.hasClass('invisible') && parent.is(':visible')) {
                stickElement.addClass("invisible");
            } else if ((parentBottom > stickElementBottom) && stickElement.hasClass('invisible')) {
                stickElement.removeClass("invisible");
            } else if (stickElement.hasClass('hidden') && stickElement.data('stick_hide')) {
                stickElement.hide();
            }
        });
    }

    /**
     * init
     */
    var init = function () {
        MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

        var observer = new MutationObserver(function () {
            if (stickMeClass.is(':visible') && !isVisible) {
                isVisible = true;
                initStick();
                initEvents();
            }
        });

        observer.observe(document, {
            attributes: true,
            subtree: true,
        });
    }

    /**
     *
     */
    init();
})