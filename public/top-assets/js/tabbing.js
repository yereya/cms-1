define('tabbing', ['jquery'], function($) {
    var $container,
        $tabsButtons;

    function switchTab() {
        var $activeButton = $(this);
        setActiveButton($activeButton);
        var tabsContainer = $container.find('[data-tab_content]');
        tabsContainer.each(function() {
           var $tabContainer = $(this);
           if ($tabContainer.data('tab_content') == $activeButton.data('tab')) {

               $tabContainer.fadeIn(700).removeClass('hidden');
           } else {
               $tabContainer.addClass('hidden');
           }
        });
    }

    /**
     * Set Active Button
     * @param $activeButton
     */
    function setActiveButton($activeButton) {
        $tabsButtons.removeClass('active');
        $activeButton.addClass('active');
    }
    /*
     * Add event click to tabs buttons
     */
    function registerTabsButton() {
        $tabsButtons.on('click', switchTab);
    }

    function setFirstButtonActive() {
        $tabsButtons.first().addClass('active');

        var tabsContainer = $container.find('[data-tab_content]');
        tabsContainer.each(function() {
            var $tabContainer = $(this);
            if ($tabContainer.data('tab_content') == $tabsButtons.first().data('tab')) {
                $tabContainer.fadeIn(700).removeClass('hidden');
            } else {
                $tabContainer.addClass('hidden');
            }
        });
    }

    function init() {
        $container = $('.tabbing');
        $tabsButtons = $container.find('[data-tab]');
        setFirstButtonActive();
        registerTabsButton();
    };

    init();

    return {
        init: function() {
            init();
        }
    }
});