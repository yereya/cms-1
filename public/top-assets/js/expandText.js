define('expandText', ['jquery'], function ($) {
    var selectors = {
        expandables: '[data-expand]',
        expandButtonsContainer: '.expand-button',
        expandLink: '.expand',
        collapseLink: '.collapse'
    };

    function init() {
        var $selectors = {
            containers: $(selectors.expandButtonsContainer),
            expandables: $(selectors.expandables),
            window: $(window)
        }
        setEvents($selectors);
    }

    function setEvents($selectors) {

        $selectors.window.on('resize', function () {
            toggleVisibility($selectors);
        });
        $selectors.expandables.on('click', selectors.expandButtonsContainer, function (e) {
            e.preventDefault();

            var expandable = $(this).closest(selectors.expandables);
            toggleExpand(expandable);
        });
    }

    function toggleVisibility($selectors) {
        var isMobile = $selectors.window.innerWidth() < 768;

        if (isMobile) {
            $selectors.containers.removeClass('hidden');
        } else {
            $selectors.containers.addClass('hidden');
        }
    }

    function toggleExpand(el) {
        var el = $(el);

        // Collapsing
        if (el.attr('data-expand') == 'open') {
            doCollapse(el);
        } else { // Expanding
            doExpand(el);
        }
    }

    function doCollapse(el) {
        var initialHeight = el.attr('data-expand_height');
        el.attr('data-expand', '');
        el.animate({height: initialHeight}, "slow");

        el.find(selectors.expandLink).removeClass('hidden');
        el.find(selectors.collapseLink).addClass('hidden');
    }

    function doExpand(el) {
        var currentHeight = el.height();
        el.attr('data-expand', 'open').css('height', 'auto');
        var newHeight = el.height() + $(selectors.expandButtonsContainer).height();
        el.css('height', currentHeight);
        el.animate({height: newHeight}, "slow");

        el.find(selectors.collapseLink).removeClass('hidden');
        el.find(selectors.expandLink).addClass('hidden');
    }

    init();

});