define('toolTips', ['jquery', 'tooltipster'], function ($) {

    function init() {
        $('.tooltipster').tooltipster({
            theme: ['tooltipster-shadow', 'tooltipster-custom'],
            maxWidth: 300,
            interactive: true,
            delay: 300,
            animation: 'fade',
            trigger:"custom",
            triggerOpen: {
                click: true,
                mouseenter: true,
                touchstart: true
            },
            triggerClose: {
                mouseleave: true,
                touchleave: true
            },
            delayTouch: [0,1500],
            functionInit: function(instance, helper){
                var $origin = $(helper.origin),
                    dataOptions = $origin.attr('data-tooltipster');

                if(dataOptions){

                    dataOptions = JSON.parse(dataOptions);

                    $.each(dataOptions, function(name, option){
                        instance.option(name, option);
                    });
                }
            }
        });
    }

    init();

    return {
        init: function() {
            init();
        }
    }
});