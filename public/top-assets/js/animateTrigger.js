define('animateTrigger', ['jquery'], function ($) {
    // TODO: Surround this library with an init function
    var selector = 'animate';
    var activeClass = 'animated';
    var defaultLeaveTimeout = 1000;

    $('[data-' + selector + ']').each(function () {
        var $element = $(this);

        var event = $element.data('animate-event') ? $element.data('animate-event') : 'mouseenter';

        $element.on(event, function () {
            $element
                .addClass(activeClass)
                .addClass($element.data(selector));

            var timeout = $element.data('animate-leave-timeout') ? $element.data('animate-leave-timeout') : defaultLeaveTimeout;

            setTimeout(function () {
                $element
                    .removeClass(activeClass)
                    .removeClass($element.data(selector));
            }, timeout);
        });
    });
});