define('comments', ['jquery_with_custom_validation', 'generalHelpers', 'rating'], function ($, helpers) {

    /**
     *
     * @type {{url: string, form: string, successHtml: string, failHtml: string, massageBox: string, commentType: string, minimumCharacters: string}}
     */
    var selectors = {
        url: '/api/comments',
        form: '.comment-box-form',
        successHtml: '.comment-box-successful',
        failHtml: '.comment-box-fail',
        massageBox: '.comment-box-message',
        commentType: '.comment-box-form input[name="type"]',
        minimumCharacters: '.minimum-characters',
        recaptcha:'.g-recaptcha'
    }

    //Rules for validation
    var rules_by_type = {
        like_dislike: {
            author: {
                required: true,
                minlength: 3
            },
            email: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    },
                },
                customEmailValidation: true
            },
            like: {
                require_from_group: [1, ".like-dislike-group"]
            },
            dislike: {
                require_from_group: [1, ".like-dislike-group"]
            },
            declare: {
                required: true
            }
        },
        reply: {
            author: {
                required: true,
                minlength: 3
            },
            email: {
                required: {
                    depends: function () {
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                customEmailValidation: true
            },
            message: {
                required: true,
                minlength: 50
            },
            agree: {
                required: true
            }
        }
    };

    //Messages for error validation
    var reUseAbleMessages = {
        author: {
            required: "Please enter your name",
            minlength: "Name should be longer than two chars"
        },
        email: {
            required: "Please enter your email",
            customEmailValidation: "Please enter a valid email address."
        },
        like: {
            require_from_group: "Please enter at least one of these comments"
        },
        dislike: {
            require_from_group: "Please enter at least one of these comments"
        },
        declare: {
            required: "Please accept Terms & Conditions"
        },
        message: {
            required: "Please enter at least 50 chars"
        },
        agree: {
            required: "required field"
        }
    };

    /**
     * @description init on page loading
     *              1 set replay message listener
     *              2. hide questions on focus
     *              3. set validation rules
     *
     */
    function init() {
        attachEvents();
        var type = $(selectors.commentType).val();
        $(selectors.form).validate({
            rules: rules_by_type[type],
            errorPlacement: function (error, element) {

                var error_prefix = 'comment-';
                generalErrorPlacement(error, element, error_prefix);
            },

            //in order to hide errors after type correct string, need to add this method
            success: function (error) {
            },
            messages: {
                author: reUseAbleMessages.author,
                email: reUseAbleMessages.email,
                like: reUseAbleMessages.like,
                dislike: reUseAbleMessages.dislike,
                declare: reUseAbleMessages.declare
            },
            submitHandler: function () {
                $(selectors.recaptcha).click();

                return false;
            }
        });
    }

    /**
     * Initialization
     *
     * @description initialize Events
     */
    function attachEvents() {
        setIdAndFocusIntoForm();
        hideReviewQuestionsOnFocus();
        resolveMinimumCharsByKeyUp();
    }


    /**
     *  @description  Set value of parent to hidden field and focus into first input
     */
    function setIdAndFocusIntoForm() {
        var $replayButton = $('.reply-button');
        var $parentId = $('[name=parent_id]');
        var $fieldToFocus = $('[name=author]');
        var $elementToScrollInto = $('.comment-box-stars-rating');

        $replayButton.on('click', function (event) {
            event.preventDefault();
            var $button = $(event.target);
            var id = getId($button);
            setParentId($parentId, id);
            focusIntoField($fieldToFocus);
            scrollIntoView($elementToScrollInto, 30);
        })
    }

    /**
     * @description Hide pros cons Comment on focus or click
     */
    function hideReviewQuestionsOnFocus() {
        var promptLikeNDislike = '.prompt-like, .prompt-dislike';
        var reviewQuestionSelector = '.review-question';

        $('body').on('click focus', promptLikeNDislike, function (event) {
            var $target = $(event.target);
            var $parent = $target.closest('.like-dislike');

            $parent.find(reviewQuestionSelector).fadeOut();
            if (event.type == "click") {
                focusIntoField($parent.find('.like-dislike-group'));
            }
        })
    }

    /**
     * @description send ajax request to api and update the database with the new comment
     */
    function sendComment() {
        var params = $(selectors.form).serialize();
        params += '&_token=' + Top.csrf + '&post_id=' + Top.post_id;
        $.post(selectors.url, params)
            .done(function (data) {
                if (data.status == '1') {
                    var successHtml = $(selectors.successHtml).html();
                    $(selectors.form).html(successHtml);
                } else {
                    addFailMessage();
                }
            })
            .fail(function (jqXhr) {
                if (jqXhr.status === 422) {
                    addFailMessage();
                }
            });
    }

    /**
     *  @description Set Fail message after finish submitting
     */
    function addFailMessage() {
        var failHtml = $(selectors.failHtml).html();
        $(selectors.massageBox).html(failHtml);
    }

    /**
     * @description set comment parent id into hidden field
     * @param $input
     * @param commentId
     */
    function setParentId($input, commentId) {
        $input.val(commentId);
    }

    /**
     * @description get comment id from data-comment_id
     * @param $button
     * @return string
     */
    function getId($button) {
        return $button.data('comment_id');
    }

    /**
     * @description Focus into input
     * @param $field
     */
    function focusIntoField($field) {
        $field.focus();
    }

    /**
     * @description  Scroll into specific field and and spacing at the top
     * @param $elementToScrollInto
     * @param marginTop
     */
    function scrollIntoView($elementToScrollInto, marginTop) {
        var heightFromTheTop = ($elementToScrollInto.offset().top) - marginTop;
        $('body,html').animate(
            {scrollTop: heightFromTheTop}
            , 1000);
    }

    /**
     *
     * @description Generate Error Placements
     *
     * @param error
     * @param element
     * @param error_prefix
     */
    function generalErrorPlacement(error, element, error_prefix) {
        var error_element = error_prefix + element.attr("name") + '-error';
        var $error_element = $('.' + error_element);

        if ($error_element.length) {
            $error_element.html(error.html());

        } else {
            if (element.attr('type') == 'checkbox') {
                element.addClass('error');
                return;
            }
            error.insertAfter(element);
        }
    }

    /**
     * Resolve Chars Number on KeyUp
     * @description display  the min chars for the message while typing
     */
    function resolveMinimumCharsByKeyUp() {
        var $selector = $(selectors.minimumCharacters);
        var textAreaSelector = $($selector.data('textarea_selector'));
        var minChars = $selector.data('minimum-characters');
        $selector.text(minChars);

        textAreaSelector.on('keyup', function () {
            var content = $(this);
            var charsNumber = helpers.countWords(content.val(), '');
            console.log(charsNumber);
            if (charsNumber <= minChars) {
                $selector.text(minChars - charsNumber);
            }
        })
    }

    /**
     * Recaptcha callback
     */
    function recaptchaCallback() {
        sendComment();
    }

    /**
     * expose recaptcha callback
     */
    window.recaptchaCallback = function () {
        recaptchaCallback();
    }

    init();

});