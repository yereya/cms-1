<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AssignConversion extends Event
{
    use SerializesModels;

    private $accounts;
    private $logger;

    /**
     * Create a new event instance.
     * @param $accounts
     * @param $logger
     */
    public function __construct($accounts, $logger)
    {
        $this->accounts = $accounts;
        $this->logger = $logger;
    }

    /**
     * Get Conversions array
     *
     * @return mixed
     */
    public function getAccounts() {
        return $this->accounts;
    }

    /**
     * Get Logger
     *
     * @return mixed
     */
    public function getLogger() {
        return $this->logger;
    }

    /**
     * Get conversion events
     *
     * @return array
     */
    public function getConversionEvents() {
        return [
            [
                'type' => 'event',
                'event' => 'lead'
            ],
            [
                'type' => 'event',
                'event' => 'call'
            ],
            [
                'type' => 'click',
                'event' => 'canceled_sale'
            ],
            [
                'type' => 'click',
                'event' => 'canceled_lead'
            ],
            [
                'type' => 'click',
                'event' => 'adjustment'
            ],
            [
                'type' => 'event',
                'event' => 'sale'
            ],
            [
                'type' => 'click',
                'event' => 'click_out'
            ],
            [
                'type' => 'click',
                'event' => 'install'
            ]
        ];
    }
}
