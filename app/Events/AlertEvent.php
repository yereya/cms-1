<?php

namespace App\Events;

/**
 * Class AlertEvent
 * @package App\Events
 */
class AlertEvent extends Event
{
    public $query_id;
    public $result;

    /**
     * AlertEvent constructor.
     * @param $query_id
     * @param $result
     */
    public function __construct($query_id, $result)
    {
        $this->query_id = $query_id;
        $this->result = $result;
    }
}
