<?php

namespace App\Events;

use App\Entities\Models\Sites\Media;
use Illuminate\Queue\SerializesModels;

class MediaUpdate extends Event
{
    use SerializesModels;

    public $media;
    public $action;

    /**
     * Create a new event instance.
     *
     * @param Media  $media
     * @param string $action
     */
    public function __construct(Media $media, string $action)
    {
        $this->media = $media;
        $this->action = $action;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
