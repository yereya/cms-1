<?php

namespace App\Events;

use App\Entities\Models\Sites\DynamicLists\DynamicList;
use App\Events\Event;
use App\Libraries\Events\ModelEventActions;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DynamicListEvent extends Event
{
    use SerializesModels;

    /**
     * @var DynamicList
     */
    public $model;

    /**
     * @var
     */
    public $action;

    /**
     * Create a new event instance.
     *
     * @param DynamicList $model
     */
    public function __construct(DynamicList $model, $action)
    {
        $this->model = $model;
        $this->action = $action;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
