<?php

namespace App\Events;

use App\Entities\Models\Sites\Widgets\SiteWidgetTemplate;
use Illuminate\Queue\SerializesModels;

class BladeEvent extends Event
{
    use SerializesModels;

    /**
     * @var SiteWidgetTemplate
     */
    public $model;

    /**
     * @var
     */
    public $action;

    /**
     * Create a new event instance.
     *
     * @param SiteWidgetTemplate $site_widget_template
     * @param $action
     */
    public function __construct(SiteWidgetTemplate $site_widget_template, $action)
    {
        $this->model = $site_widget_template;
        $this->action = $action;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
