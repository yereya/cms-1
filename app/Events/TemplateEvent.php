<?php

namespace App\Events;

use App\Entities\Models\Sites\Site;
use Illuminate\Queue\SerializesModels;
use App\Entities\Models\Sites\Templates\Template;

class TemplateEvent extends Event
{
    use SerializesModels;

    /**
     * @ver Site
     */
    public $site;

    /**
     * @ver string
     */
    public $action;

    /**
     * @ver Template
     */
    public $template;

    /**
     * Create a new event instance.
     *
     * @param Template $template
     * @param string   $action
     * @param Site     $site
     */
    public function __construct(Template $template, string $action, Site $site)
    {
        $this->site   = $site;
        $this->action = $action;
        $this->model  = $template;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
