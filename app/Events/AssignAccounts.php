<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class AssignAccounts extends Event
{
    use SerializesModels;

    private $accounts;
    private $logger;

    /**
     * Create a new event instance.
     *
     * @param $rows - accounts rows
     * @param $logger
     */
    public function __construct($rows, $logger)
    {
        $this->accounts = $rows;
        $this->logger = $logger;
    }

    /**
     * Get Logger
     *
     * @return mixed
     */
    public function getLogger() {
        return $this->logger;
    }

    /**
     * Get new account data
     *
     * @return mixed
     */
    public function getAccounts()
    {
        return $this->accounts;
    }
}
