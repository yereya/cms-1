<?php

namespace App\Events;

use App\Entities\Models\Sites\Templates\WidgetData;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WidgetDataEvent extends Event
{
    use SerializesModels;

    /**
     * @var WidgetData
     */
    public $model;

    /**
     * @var
     */
    public $action;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(WidgetData $model, $action)
    {
        $this->model = $model;
        $this->action = $action;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
