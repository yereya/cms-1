<?php

namespace App\Listeners;

use App\Events\AssignConversion;
use DB;

class EventAssignConversion
{
    /**
     * @var string - database name
     */
    protected $connection;

    /**
     * @var string - table name
     */
    protected $table_name;

    /**
     * Create the event listener.
     */
    public function __construct()
    {
        $this->connection = 'bo';
        $this->table_name = 'conversions';
    }

    /**
     * Handle the event.
     *
     * @param  AssignConversion $event
     * @return void
     */
    public function handle(AssignConversion $event)
    {
        if (!$event->getAccounts()) {
            return null;
        }

        $records = $this->buildConversionRecords($event->getAccounts(), $event->getConversionEvents());

        try {
            DB::connection($this->connection)->table($this->table_name)->insert($records);
            $event->getLogger()->info(['Assign conversions to new accounts', count($records)]);
        } catch (\Exception $e) {
            $event->getLogger()->error(['Assign conversions to new account failed', $e->getMessage()]);
        }
    }

    /**
     * Create conversions records
     *
     * @param $accounts
     * @param $conversion_events
     * @return array
     */
    private function buildConversionRecords($accounts, $conversion_events)
    {
        $values = [];
        $track_id = time();
        foreach ($accounts as $account) {
            foreach ($conversion_events as $event) {
                $values[] = [
                    'track_id' => $track_id * -1,
                    'source_account_id' => $account['source_account_id'],
                    'type' => $event['type'],
                    'event' => $event['event'] == 'click_out' ? 'clickout' : $event['event'],
                    'price' => 0,
                    'timestamp' => date('Y-m-d H:i:s', time()),
                    'publisher_id' => 95
                ];

                $track_id++;
            }
        }

        return $values;
    }
}