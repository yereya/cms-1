<?php

namespace App\Listeners;

use App\Events\BladeEvent;
use App\Libraries\Cache\CacheClearer;
use App\Libraries\Events\ModelEventActions;

class BladeEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BladeEvent  $event
     * @return void
     */
    public function handle(BladeEvent $event)
    {
        if (in_array($event->action, [ModelEventActions::UPDATED, ModelEventActions::DELETED])) {
            (new CacheClearer())->clearFromEvent('blade', $event->model->id);
        }
    }
}
