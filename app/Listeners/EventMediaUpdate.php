<?php

namespace App\Listeners;

use App\Entities\Models\Log;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use App\Events\MediaUpdate;
use App\Libraries\Media\MediaUploader;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventMediaUpdate
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MediaUpdate $event
     *
     * @return void
     */
    public function handle(MediaUpdate $event)
    {
        \Log::info('media_update', [$event->action, $event->media->id]);

        $result = $this->updateSite($event);

        \Log::info('media_update', [json_encode($result)]);
    }

    /**
     * @param MediaUpdate $event
     *
     * @return mixed
     */
    public function updateSite(MediaUpdate $event)
    {
        $domain = (new SiteDomainRepo)->getBySite($event->media->site_id);

        $client = new MediaUploader($domain->domain, 'media/upload');
        $result = $client->upload($event->media->id, $event->action, $this->getCmsMediaUrl($event->media->site_id));

        return $result;
    }

    private function getCmsMediaUrl($site_id) {
        return url('/') . "/top-assets/sites/$site_id/images";
    }
}
