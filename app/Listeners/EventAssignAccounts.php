<?php

namespace App\Listeners;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\BlockQuery;
use App\Entities\Models\Users\AccessControl\SiteUserRole;
use App\Events\AssignAccounts;
use Cache;
use DB;

class EventAssignAccounts
{
    /**
     * @var string - permissions name
     */
    private $permission_name;

    /**
     * @var string
     */
    private $permission_type;

    /**
     * @var - permitted users
     */
    private $permitted_users;

    /**
     * @var - table name
     */
    private $database;
    private $table_account_report;
    private $table_assign_account;
    private $table_account_report_field;

    /**
     * @var - Collection accounts|null
     */
    private $accounts;

    /**
     * @var - TpLogger logger
     */
    private $logger;

    private $role_id = 14;

    /**
     * Create the event listener.
     */
    public function __construct()
    {
        $this->permission_name = 'users.auto.account.permissions';
        $this->permission_type = 'view';
        $this->permitted_users = [];
        $this->database = 'bo';
        $this->table_assign_account = 'user_assigned_accounts';
        $this->table_account_report = 'account_reports';
        $this->table_account_report_field = 'account_report_fields';
    }

    /**
     * Handle the event.
     *
     * @param  AssignAccounts $event
     * @return void
     */
    public function handle(AssignAccounts $event)
    {
        if (!$event->getAccounts()) {
            return null;
        }

        $this->accounts = $this->getAccountsCollection($event->getAccounts());
        $this->logger = $event->getLogger();
        $this->buildPermittedUsers();
        $this->assignAccounts();
        $this->addReports();
        $this->addNetworkNetRevenue();

        $this->createBlockerQueries();
    }

    /**
     * Create blocker queries only to google accounts
     */
    private function createBlockerQueries() {
        foreach ($this->accounts as $account) {
            if ($account->publisher_id == 95 || $account->publisher_id == 163) {
                $block = $this->createEmptyBlockQuery($account);
                if ($block) {
                    $this->createBlockQuery($block, $account);
                    $this->createReleaseQuery($block->id, $account);
                    $this->createSecondReleaseQuery($block->id, $account);
                }
            }
        }
    }

    /**
     * Get Accounts from table
     *
     * @param $accounts_rows
     * @return mixed
     */
    private function getAccountsCollection($accounts_rows)
    {
        $source_ids = [];
        foreach ($accounts_rows as $row) {
            $source_ids[] = $row['source_account_id'];
        }

        return Account::whereIn('source_account_id', array_values($source_ids))->get();
    }

    /**
     * Add record Network Net Revenue
     */
    private function addNetworkNetRevenue() {
        $records = $this->buildNetworkNetRevenueRecords();

        try {
            DB::connection($this->database)->table($this->table_account_report_field)->insert($records);
            $this->logger->info(['Add network net revenue records to new accounts', json_encode($this->accounts->pluck('id')->filter())]);
            Cache::forget('networks_report_2'); //remove old cache
        } catch (\Exception $e) {
            $this->logger->error(['Adds network net revenue records to new accounts fail', $e->getMessage()]);
        }
    }

    /**
     * Assigns reports to new Accounts
     */
    private function addReports()
    {
        $records = $this->buildReportRecords();

        try {
            DB::connection($this->database)->table($this->table_account_report)->insert($records);
            $this->logger->info(['Assign reports to new accounts', json_encode($this->accounts->pluck('id')->filter())]);
        } catch (\Exception $e) {
            $this->logger->error(['adds reports to new accounts fail', $e->getMessage()]);
        }
    }

    /**
     * Assigns Account to permitted users
     */
    private function assignAccounts()
    {
        $records = $this->buildAccountRecords();

        try {
            DB::table($this->table_assign_account)->insert($records);
            $this->logger->info(['Assign permitted users to new accounts', json_encode($this->accounts->pluck('id')->filter())]);
        } catch (\Exception $e) {
            $this->logger->error(['adds user permissions to new accounts fail', $e->getMessage()]);
        }
    }

    /**
     * Build records to ASSIGN_USER table
     *
     * @return array
     */
    private function buildAccountRecords()
    {
        $records = [];
        foreach ($this->accounts as $account) {
            foreach ($this->permitted_users as $user_id) {
                $records[] = [
                    'user_id' => $user_id,
                    'account_id' => $account->id
                ];
            }
        }

        return $records;
    }

    /**
     * Build Reports Records
     *
     * @return array
     */
    private function buildReportRecords()
    {
        $records = [];
        foreach ($this->accounts as $account) {
            foreach ($this->getReports() as $id => $name) {
                $records[] = [
                    'account_id' => $account->id,
                    'report_id' => $id
                ];
            }
        }

        return $records;
    }

    /**
     * Build Network Net Revenue Records
     *
     * @return array
     */
    private function buildNetworkNetRevenueRecords()
    {
        $records = [];
        foreach ($this->accounts as $account) {
            foreach ($this->getReportsToNetworkNetRevenue() as $id => $report) {
                $records[] = [
                    'account_id' => $account->id,
                    'report_id' => $id,
                    'field' => 'network_net_revenue',
                    'source_field' => 'cost',
                    'operator' => 'multiplication',
                    'value_type' => 'value',
                    'value' => -1
                ];
            }
        }

        return $records;
    }

    /**
     * Build Permitted Users
     */
    private function buildPermittedUsers()
    {
        $this->permitted_users = SiteUserRole::where('role_id', $this->role_id)
            ->get()->pluck('user_id')->filter();
    }

    /**
     * Create empty record to block query
     *
     * @param $account
     *
     * @return static
     */
    private function createEmptyBlockQuery($account) {
        $block = BlockQuery::create([
            'account_id' => $account->id,
            'type' => 'ip block',
            'query' => '',
            'name' => $account->account_name . ' Block ALL',
            'last_run' => date('Y-m-d H:i:s')
        ]);

        return $block;
    }

    /**
     * Insert query to block
     *
     * @param $block
     * @param $account
     *
     * @return mixed
     */
    private function createBlockQuery($block, $account) {
        $query = "SELECT t.tokenIp AS ip
            FROM dwh.dwh_fact_tracks t 
            left join bo.blocked_ips ips on ips.ip = t.tokenIp and ips.block_query_id = $block->id
            WHERE t.click_in = 1 
            AND t.gclid IS NOT NULL 
            AND t.advertiser_id = $account->advertiser_id
            AND t.timestamp >= (select date_sub(abq.last_run, interval 3 minute) from bo.account_block_queries abq where abq.id = $block->id)
            AND ((ips.ip is null) Or (t.timestamp > ips.updated_at AND ips.status='released' and count != -1))
            group by t.tokenIp";

        $block->query = $query;
        $block->save();

        return $block->id;
    }

    /**
     *
     *
     * @param $block_query_id
     * @param $account
     *
     * @return mixed
     */
    private function createReleaseQuery($block_query_id, $account) {
        $query = "select t.tokenIP as ip 
            from dwh.dwh_fact_tracks t 
            inner join bo.blocked_ips bip on t.tokenIP = bip.ip and bip.block_query_id = $block_query_id 
            and bip.count < 4
            where t.click_out >= 1 and t.gclid is not null and t.advertiser_id = $account->advertiser_id
            and TIMESTAMPDIFF(MINUTE, bip.updated_at, now()) < 60 
            and TIMESTAMPDIFF(MINUTE, bip.updated_at, now()) >= 0  
            and bip.status = 'blocked' 
            and t.timestamp >= (select date_sub(abq.last_run, interval 3 minute) 
            from bo.account_block_queries abq where abq.id = $block_query_id)
            group by t.tokenIP";

        $release = BlockQuery::create([
            'account_id' => $account->id,
            'type' => 'ip release',
            'query' => $query,
            'name' => $account->account_name . ' Release blocked IP after 10 mins if identified click _out eve',
            'last_run' => date('Y-m-d H:i:s')
        ]);

        return $release->id;
    }

    private function createSecondReleaseQuery($block_query_id, $account) {
        $query = "select ip from bo.blocked_ips 
            where block_query_id = $block_query_id and status = 'blocked' and 
            (
	          (count = 1 and TIMESTAMPDIFF(MINUTE, updated_at, now()) >= 360) Or
              (count = 2 and TIMESTAMPDIFF(MINUTE, updated_at, now()) >= 1080) Or
              (count = 3 and TIMESTAMPDIFF(MINUTE, updated_at, now()) >= 1640) Or
              (count >= 4 and TIMESTAMPDIFF(MINUTE, updated_at, now()) >= 20160) 
            )
            group by ip";

        $release = BlockQuery::create([
            'account_id' => $account->id,
            'type' => 'ip release',
            'query' => $query,
            'name' => $account->account_name . ' Release IP\'s after 24 hrs (first query)',
            'last_run' => date('Y-m-d H:i:s')
        ]);

        return $release->id;
    }

    /**
     * Reports id and name
     *
     * @return array
     */
    private function getReports()
    {
        return [
            7 => 'CLICK_PERFORMANCE_REPORT',
            //6 => 'ADGROUP_PERFORMANCE_REPORT',
            10 => 'GENDER_PERFORMANCE_REPORT',
            12 => 'BUDGET_PERFORMANCE_REPORT',
            13 => 'GEO_PERFORMANCE_REPORT',
            15 => 'SEARCH_QUERY_PERFORMANCE_REPORT',
            17 => 'SEARCH_QUERY_CONVERSION_PERFORMANCE_REPORT',
            16 => 'AD_PERFORMANCE_REPORT',
            //20 => 'CHANGE_LOG_KEYWORDS_REPORT',
            //21 => 'CHANGE_LOG_AD_REPORT',
            //18 => 'CONVERSION_UPLOAD',
            23 => 'CAMPAIGN_PERFORMANCE_REPORT',
            27 => 'SHARE_OF_VOICE_REPORT',
            29 => 'GENDER_CONVERSION_PERFORMANCE_REPORT',
            32 => 'AGE_PERFORMANCE_REPORT',
            33 => 'AGE_CONVERSION_PERFORMANCE_REPORT',
            34 => 'GEO_CONVERSION_PERFORMANCE_REPORT'
        ];
    }

    /**
     * Reports to network net revenue
     *
     * @return array
     */
    private function getReportsToNetworkNetRevenue()
    {
        return [
            1 => 'KEYWORDS_PERFORMANCE_REPORT',
            2 => 'DISPLAY_AD_PERFORMANCE_REPORT'
        ];
    }
}
