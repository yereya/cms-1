<?php

namespace App\Listeners;

use App\Events\WidgetDataEvent;
use App\Libraries\Cache\CacheClearer;
use App\Libraries\Events\ModelEventActions;

class WidgetDataEventListener
{
    /**
     * Handle the event.
     *
     * @param  WidgetDataEvent  $event
     * @return void
     */
    public function handle(WidgetDataEvent $event)
    {
        if (in_array($event->action, [ModelEventActions::UPDATED, ModelEventActions::DELETED])) {
            (new CacheClearer())->clearFromEvent('widget-data', $event->model->id);
        }
    }
}
