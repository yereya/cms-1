<?php

namespace App\Listeners;

use App\Events\TemplateEvent;
use App\Libraries\Cache\CacheClearer;
use App\Libraries\Events\ModelEventActions;

class TemplateEventListener
{

    /**
     * Handle the event.
     *
     * @param  TemplateEvent $event
     *
     * @return void
     */
    public function handle(TemplateEvent $event)
    {
        if (in_array($event->action, [ModelEventActions::UPDATED, ModelEventActions::DELETED])) {
            (new CacheClearer())->clearFromEvent('template', $event->model->id);
        }
    }
}
