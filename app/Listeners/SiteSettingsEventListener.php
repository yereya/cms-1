<?php

namespace App\Listeners;

use App\Events\SiteSettingsEvent;
use App\Libraries\Cache\CacheClearer;
use App\Libraries\Events\ModelEventActions;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SiteSettingsEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SiteSettingsEvent  $event
     * @return void
     */
    public function handle(SiteSettingsEvent $event)
    {
        if (in_array($event->action, [ModelEventActions::UPDATED, ModelEventActions::DELETED])) {
            (new CacheClearer())->clearFromEvent('site-settings', $event->model->id, $event->model->site);
        }
    }
}
