<?php

namespace App\Listeners;

use App\Events\DynamicListEvent;
use App\Libraries\Cache\CacheClearer;
use App\Libraries\Events\ModelEventActions;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DynamicListEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DynamicListEvent $event
     * @return void
     */
    public function handle(DynamicListEvent $event)
    {
        if (in_array($event->action, [ModelEventActions::UPDATED, ModelEventActions::DELETED])) {
            (new CacheClearer())->clearFromEvent('dynamic-list', $event->model->id);
        }
    }
}
