<?php

namespace App\Listeners;

use App\Events\PostEvent;
use App\Libraries\Cache\CacheClearer;
use App\Libraries\Events\ModelEventActions;

class PostEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PostEvent  $event
     * @return void
     */
    public function handle(PostEvent $event)
    {
        if (in_array($event->action, [ModelEventActions::UPDATED, ModelEventActions::DELETED])) {
            (new CacheClearer())->clearFromEvent('post', $event->model->id);
        }
    }
}
