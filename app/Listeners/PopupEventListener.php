<?php

namespace App\Listeners;

use App\Events\PopupEvent;
use App\Libraries\Cache\CacheClearer;
use App\Libraries\Events\ModelEventActions;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PopupEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PopupEvent  $event
     * @return void
     */
    public function handle(PopupEvent $event)
    {
        if (in_array($event->action, [
            ModelEventActions::CREATED,
            ModelEventActions::UPDATED,
            ModelEventActions::DELETED
        ])) {
            (new CacheClearer())->clearFromEvent('popup', $event->model->id);
        }
    }
}
