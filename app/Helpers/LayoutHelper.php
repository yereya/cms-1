<?php

if (!function_exists('getSiteModel')) {
    /**
     * Returns the model entity
     *
     * @return null|object
     */
    function getSiteModel()
    {
        $site = Route::getCurrentRoute()->getParameter('site') ?? config('app.cms_site_id');
        if (isset($site)) {
            return is_object($site) ? $site : \App\Entities\Models\Sites\Site::find($site);
        }

        return null;
    }
}
