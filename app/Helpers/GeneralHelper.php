<?php


use App\Entities\Repositories\Sites\SiteRepo;

if (!function_exists('object2Array')) {
    /**
     * Transforms object into array including ancestors
     * by using json encode decode
     *
     * @param $obj
     *
     * @return mixed
     */
    function object2Array($obj)
    {
        return json_decode(json_encode($obj), true);
    }
}


if (!function_exists('array2Object')) {
    /**
     * Transforms array into object including ancestors
     * by using json encode decode
     *
     * @param $obj
     *
     * @return mixed
     */
    function array2Object($obj)
    {
        return json_decode(json_encode($obj), false);
    }
}


if (!function_exists('insertOnDuplicateKeyUpdate')) {
    /**
     * @param \Illuminate\Database\Query\Builder $builder
     * @param array $values
     * @param array $exclude_columns_from_updating - this columns from being updated (they will still be inserted)
     *
     * @return int
     */
    function insertOnDuplicateKeyUpdate(Illuminate\Database\Query\Builder $builder, array $values, array $exclude_columns_from_updating = [])
    {
        if (empty($values)) {
            return 0;
        }

        // Case where $data is not an array of arrays.
        if (!isset($values[0])) {
            $values = [$values];
        }

        // find the key columns
        list($first_row) = $values;
        if (empty($exclude_columns_from_updating)) {
            $update_columns =  array_keys($first_row);
        } else {
            $update_columns = array_diff(array_keys($first_row), $exclude_columns_from_updating);
        }

        // Build sql query
        $grammar = $builder->getGrammar();
        $sql  = $grammar->compileInsert($builder, $values);

        $sql .= ' ON DUPLICATE KEY UPDATE ';
        $sql .= implode(',', array_map(function ($val) {
            return sprintf('`%s` = VALUES(`%s`)', $val, $val);
        }, $update_columns));

        // Build the bindings of the values
        // later to be used in the statement 
        $bindings = [];
        foreach ($values as $record) {
            foreach ($record as $value) {
                $bindings[] = $value;
            }
        }

        return $builder->getConnection()->affectingStatement($sql, $bindings);
    }
}


if (!function_exists('array2HtmlAttributes')) {
    /**
     * Will return a string with html attributes
     *
     * @param array $array
     *
     * @return array
     */
    function array2HtmlAttributes($array)
    {
        $_attr = ' ';

        if (!$array OR !count($array)) {
            return '';
        }

        foreach ($array as $key => $val) {
            $_attr .= $key . '="' . $val . '" ';
        }

        return $_attr;
    }
}

if (!function_exists('array2IdNameObject')) {
    function array2IdNameObject($array)
    {
        $newArray = [];
        foreach ($array as $key => $value) {
            $newObject       = new stdClass();
            $newObject->id   = $key;
            $newObject->name = $value;
            $newArray[$key]  = $newObject;
        }

        return $newArray;
    }
}


if (!function_exists('getBreadcrumbs')) {
    /**
     * Will take the value of each $keyName in the passed $array
     * and set as the key for each record in the array
     *
     * @return array
     */
    function getBreadcrumbs()
    {
        //Gets the controller that called this view
        //pulls the $breadcrumbs_excludes if exists
        $route = \Route::getCurrentRoute()->getAction();
        if (isset($route['controller'])) {
            $controller           = substr($route['controller'], 0, strpos($route['controller'], '@'));
            $breadcrumbs_excludes = isset(app($controller)->breadcrumbs_excludes) ? app($controller)->breadcrumbs_excludes : null;
        }

        $route          = [];
        $_segments      = '';
        $route_segments = \Request::segments();
        if (isset($route_segments)) {
            foreach ($route_segments as $route_segment) {
                $_segments .= '/' . $route_segment;

                if (!is_numeric($route_segment) && !inArrayR(strtolower($route_segment), $breadcrumbs_excludes)) {
                    $route[] = [
                        'href'  => $_segments,
                        'title' => ucwords(str_replace(['-', '_'], ' ', $route_segment))
                    ];
                }
            }
        }

        return $route;
    }
}


if (!function_exists('array2HtmlTags')) {
    /**
     *
     * @return array
     */
    function array2HtmlTags($array)
    {
        return "<span class=\"badge badge-info\">" . implode('</span> <span class="badge badge-info">',
            $array) . "</span>";
    }
}


if (!function_exists('inArrayR')) {
    /**
     * Recursive version of in_array to work on multidimensional array
     *
     * @args string §
     * @args array $haystack
     * @args bool $strict
     * @return bool
     */
    function inArrayR($needle, $haystack, $strict = false)
    {
        if (!is_array($haystack)) {
            return false;
        }
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && inArrayR($needle, $item,
                        $strict))
            ) {
                return true;
            }
        }

        return false;
    }
}


if (!function_exists('arraySearchKey')) {
    /**
     * Recursive search for key inside an array and returns the value
     *
     * @param $needle_key
     * @param $haystack
     *
     * @return bool|string
     */
    function arraySearchKey($needle_key, $haystack)
    {
        if (empty($haystack)) {
            return false;
        }

        $result = false;
        array_walk_recursive($haystack, function ($item, $key) use ($needle_key, &$result) {
            if ($result === false && $key == $needle_key) {
                $result = $item;
            }
        });

        return $result;
    }
}


if (!function_exists('arrayCookieQuerySearch')) {
    /**
     * Array Cookie Query Search
     * Searches for cookie by its name ($query_key)
     *
     * Possible structure of cookie:
     * JSESSIONID=ygr9od6pnjr9;Path=/
     *
     * Example of possible $haystack:
     *"response_headers" => array:8 [
            "Content-Type" => array:1 [
                0 => "text/html; charset=utf-8"
            ]
            "Expires" => array:1 [
                0 => "Thu, 01 Jan 1970 00:00:00 GMT"
            ]
            "Set-Cookie" => array:4 [
                0 => "JSESSIONID=ygr9od6pnjr9;Path=/"
                1 => "csrf=cae4de40-12b8-4c1c-85f0-2448758d4bc5;Path=/;Domain=.onlinedatingcash.com"
                2 => "DATE_AFF="impression%3DY%2Cclick%3DY%2Caffiliate_id%3D21202867%2Ccampaign_id%3D12848%2Ccreative_id%3D-1%2Cbanner_id%3D-1%2Clanding_page_id%3D-1%2Ckeyword_id%3D36649%2Ckeyword%3D%2Csubaffiliate_id%3D-1%2Csubaffiliateuserid%3D-1%2Creferrer_id%3D-1%2Csite_id%3D4%2Cis_legacy_campaign%3DN%2Cclick_list%3D%2Cimpression_list%3D%2Creferrer_campaign_id%3D-1%2Ctracking_params%3D";Path=/;Domain=.onlinedatingcash.com;Expires=Wed, 06-Jul-16 14:05:17 GMT"
                3 => "AFFILIATES=aff1; path=/"
            ]
            "Last-Modified" => array:1 [
                0 => "Wed, 29 Jun 2016 14:05:17 GMT"
            ]
            "Cache-Control" => array:1 [
                0 => "no-store,no-cache,max-age=-1"
            ]
            "Pragma" => array:1 [
                0 => "no-cache"
            ]
            "Transfer-Encoding" => array:1 [
                0 => "chunked"
            ]
            "Server" => array:1 [
                0 => "Jetty(6.1.18)"
            ]
        ]
     *
     * @param $query_key
     * @param $haystack
     * @param $delimiter
     *
     * @return bool|string
     */
    function arrayCookieQuerySearch($query_key, $haystack, $delimiter = ';')
    {
        $cookie_val = false;
        array_walk_recursive($haystack, function ($item, $key) use ($query_key, &$cookie_val, $delimiter) {

            // possible item: "csrf=cae4de40-12b8-4c1c-85f0-2448758d4bc5;Path=/;Domain=.onlinedatingcash.com"
            if (strpos($item, $delimiter) !== false) {
                $cookie_blocks = explode($delimiter, $item);
            } else {
                $cookie_blocks = [$item];
            }

            foreach ($cookie_blocks as $cookie_block) {
                if ($cookie_block && (strpos($cookie_block, $query_key) !== false)) {
                    $cookie_val = explode('=', $cookie_block)[1];
                }
            }

        });

        return $cookie_val;
    }
}


if (!function_exists('arrayKeyValue2ArrayKey')) {
    /**
     * Will take the value of each $keyName in the passed $array
     * and set as the key for each record in the array
     *
     * @param array|collection $collection
     * @param string $keyName
     * @param string $only_this_value
     *
     * @return array
     */
    function arrayKeyValue2ArrayKey($collection, $keyName = 'key', $only_this_value = null)
    {
        if (!$collection) {
            return $collection;
        }

        $newArray = [];
        foreach ($collection as $item) { // ($i = 0; $i < count($array); $i++) {

            $newArray[$item[$keyName]] = $only_this_value ? $item[$only_this_value] : $item;
        }

        return $newArray;
    }
}


if (!function_exists('back_url')) {
    /**
     * Version URL::previous() of Redirect::back() to form back button
     * if controller have action index redirect to index
     * if not have action index use laravel back methods
     *
     * Why I need use this method and not laravel back method?
     * Because, if you need use request validation form
     * and you form not valid, laravel saved previous url same current url.
     * This method checked if controller have action index and redirect to index
     *
     * @param array $params
     *
     * @return string
     */
    function back_url(array $params = [])
    {
        $current = Route::getCurrentRoute()->getAction();
        $as = $current['as'];
        $index = substr($as, 0, strripos($as, ".")) . '.index';
        if (Route::getRoutes()->hasNamedRoute($index)) {
            //check if route string with default params, if with params create route with params
            if (strpos($index, '{') !== false) {
                $params = Route::getCurrentRoute()->parameters();
            }

            return URL::route($index, $params);
        } else {
            return \URL::previous();
        }
    }
}

if (!function_exists('filterAndSetParams')) {
    /**
     * Combine passed params with our default params.
     *
     * The pairs should be considered to be all of the attributes which are
     * supported by the caller and given as a list. The returned attributes will
     * only contain the attributes in the $pairs list.
     *
     * If the $atts list has unsupported attributes, then they will be ignored and
     * removed from the final returned list.
     *
     *
     * @param array $pairs  Entire list of supported attributes and their defaults.
     * @param array $params User defined attributes in shortcode tag.
     *
     * @return array Combined and filtered attribute list.
     */
    function filterAndSetParams($pairs, $params)
    {
        $params = (array)$params;
        $out    = [];

        foreach ($pairs as $name => $default) {
            if (array_key_exists($name, $params)) {
                $out[$name] = $params[$name];
            } else {
                $out[$name] = $default;
            }
        }

        return $out;
    }
}

if (!function_exists('endKey')) {
    /**
     * Returns the key at the end of the array
     *
     * @param array $array
     *
     * @return string
     */
    function endKey($array)
    {
        end($array);

        return key($array);
    }
}

if (!function_exists('htmlAttsToArray')) {
    /**
     * Turns html attributes string into an array
     *
     * @param string $string
     *
     * @return array
     */
    function htmlAttsToArray($string)
    {
        $pattern = '/(\\w+)\s*=\\s*("[^"]*"|\'[^\']*\'|[^"\'\\s>]*)/';
        preg_match_all($pattern, $string, $matches, PREG_SET_ORDER);

        $_atts = [];
        foreach ($matches as $match) {
            if (($match[2][0] == '"' || $match[2][0] == "'") && $match[2][0] == $match[2][strlen($match[2]) - 1]) {
                $match[2] = substr($match[2], 1, -1);
            }
            $name  = strtolower($match[1]);
            $value = html_entity_decode($match[2]);
            switch ($name) {
                case 'class':
                    $_atts[$name] = preg_split('/\s+/', trim($value));
                    break;
                case 'style':
                    // parse CSS property declarations
                    break;
                default:
                    $_atts[$name] = $value;
            }
        }

        return $_atts;
    }
}

if (!function_exists('sqlDateFormat')) {
    /**
     * returns date in SQL format
     *
     * @param string $date_string
     * @param bool $seconds
     *
     * @return string
     */
    function sqlDateFormat($date_string = "now", $seconds = true)
    {
        $format = $seconds ? 'Y-m-d H:i:s' : 'Y-m-d H:i';

        return date($format, strtotime($date_string));
    }
}

if (!function_exists('formatDate')) {
    /**
     * convert any valid date string to a different date format
     *
     * @param $date_string
     * @param $date_format
     *
     * @return string
     */
    function formatDate($date_string = 'now', $date_format = 'Y-m-d H:i:s')
    {
        return date($date_format, strtotime($date_string));
    }
}

if (!function_exists('percentToDecimalFraction')) {
    /**
     * Convert percent to decimal fraction
     *
     * @param $percent
     *
     * @return float
     */
    function percentToDecimalFraction($percent)
    {
        return 1 + $percent / 100;
    }
}

if (!function_exists('decimalFractionToPercent')) {
    /**
     * Convert decimal fraction to percent
     *
     * @param $decimal_fraction
     *
     * @return float
     */
    function decimalFractionToPercent($decimal_fraction)
    {
        return ($decimal_fraction - 1) * 100;
    }
}

if (!function_exists('clearAttributesArray')) {
    function clearAttributesArray($keys, $values)
    {
        if (!is_array($keys) || !is_array($values)) {
            return null;
        }

        $_result = [];
        for ($i = 0; $i < count($keys) && $i < count($values); $i++) {
            if (!empty($keys[$i])) {
                $_result[$keys[$i]] = $values[$i];
            }
        }

        return $_result;
    }
}

if (!function_exists('getTimeZones')) {
    /**
     * Get all timezones php array to format GMT h:i Region/City
     * example GMT +03:00 Europe/Moscow
     *
     * @return array
     */
    function getTimeZones()
    {
        $zones = [];
        $time  = time();
        $unique_time = [];
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $date_format = date('P', $time);
            if (!in_array($date_format, $unique_time)) {
                $origin_dtz = new DateTimeZone($zone);
                $unique_time[] = date('P', $time);
                $zones[$key]['id'] = $zone;
                $zones[$key]['time'] = $origin_dtz->getOffset(new DateTime('now', $origin_dtz));
                $zones[$key]['text'] = 'GMT ' . $date_format . ' ' . $zone;
            }
        }

        usort($zones, 'sortHelperByTime');
        return $zones;
    }

    function sortHelperByTime($a, $b)
    {
        return $a['time'] - $b['time'];
    }
}

if (!function_exists('timeZoneName')) {
    /**
     * Return only time zone name
     *
     * @return array [id: time_zone_name, text: time_zone_name, time: time]
     */
    function timeZoneName()
    {
        $zones = [];
        $time = time();
        foreach (timezone_identifiers_list() as $key => $zone) {
            date_default_timezone_set($zone);
            $zones[$key]['id'] = $zone;
            $zones[$key]['text'] = $zone;
            $zones[$key]['time'] = $date_format = date('P', $time);
        }

        return $zones;
    }
}

if (!function_exists('convertTimeZoneNameToTimeZoneTime')) {
    /**
     * Convert time zone name to time zone time
     *
     * Example:
     * request: Pacific/Honolulu
     * response: -10:00
     *
     * @param $time_zone_name
     *
     * @return string
     */
    function convertTimeZoneNameToTimeZoneTime($time_zone_name) {
        $dateTime = new DateTime();
        $dateTime->setTimeZone(new DateTimeZone($time_zone_name));
        $seconds = $dateTime->getOffset();
        $flag = strpos($seconds, '-') !== false ? '-' : null;
        $seconds = intval(str_replace('-', '', $seconds));
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        return (strlen($hours) == 1 ? $flag . '0' . $hours : $hours) . ':' . (strlen($minutes) == 1 ? $minutes . '0' : $minutes);
    }
}

if (!function_exists('generateUUID')) {
    /**
     * Generate a globally unique identifier (GUID)
     *
     * @return array
     */
    function generateUUID()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }

        $data    = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }

}

if (!function_exists('createBase64UUID')) {
    /**
     * Create Base 64 UUID v4
     * Generated as short as possible unique ID
     * with seconds 22 chars
     * without seconds 18 chars
     *
     * @param bool $add_seconds
     *
     * @return array
     */
    function createBase64UUID($add_seconds = true)
    {
        $uuid = generateUUID();

        // For extra uniqueness add the seconds since 2014
        if ($add_seconds) {
            $uuid .= strtotime(2014) - time(); //seconds since 2014
        }
        $byteString = "";

        // Remove the dashes from the string
        $uuid = str_replace("-", "", $uuid);

        // Remove the opening and closing brackets
        $uuid = substr($uuid, 1, strlen($uuid) - 2);

        // Read the UUID string byte by byte
        for ($i = 0; $i < strlen($uuid); $i += 2) {
            // Get two hexadecimal characters
            $s = substr($uuid, $i, 2);
            // Convert them to a byte
            $d = hexdec($s);
            // Convert it to a single character
            $c = chr($d);
            // Append it to the byte string
            $byteString = $byteString . $c;
        }

        // Convert the byte string to a base64 string
        $b64uuid = base64_encode($byteString);
        // Replace the "/" and "+" since they are reserved characters
        $b64uuid = str_replace("/", "_", $b64uuid);
        $b64uuid = str_replace("+", "-", $b64uuid);
        // Remove the trailing "=="
        $b64uuid = substr($b64uuid, 0, strlen($b64uuid) - 2);

        return $b64uuid;
    }

}

if (!function_exists('breakCliCommandIntoParams')) {
    /**
     * Creates a valid call string to be used in Artisan::call()
     *
     * @param $command
     *
     * @return array
     * @throws Exepetion
     */
    function breakCliCommandIntoParams($command)
    {
        if (count($command) == 0) {
            throw new \Exepetion('Invalid cli command string');
        }

        $command     = trim($command);
        $command_arr = explode(' ', $command);

        $params = [];
        for ($i = 1; $i < count($command_arr); $i++) {
            $_cli_param = explode('=', $command_arr[$i]);
            if (count($_cli_param) == 1) {
                if ($i == 1) {
                    $params['request'] = $_cli_param[0];
                } else {
                    $params[$_cli_param[0]] = true;
                }
            } elseif (count($_cli_param) == 2) {
                $params[$_cli_param[0]] = $_cli_param[1];
            }
        }

        return [
            'command' => $command_arr[0],
            'params'  => $params,
        ];
    }
}


if (!function_exists('arrayRemoveFields')) {
    /**
     * Array Remove Fields
     *
     * @param array $array
     * @param $fields
     */
    function arrayRemoveFields(array &$array, $fields)
    {
        foreach ((array) $fields as $field) {
            array_pull($array, $field);
        }
    }
}


if (!function_exists('arrayIntersectingFields')) {
    /**
     * Array filters all according to their intersection
     * This means that it will only leave fields inside the passed array which exists
     * in the fields array
     *
     * @param array $array
     * @param array $fields
     *
     * @return array
     */
    function arrayIntersectingFields(array &$array, array $fields = [])
    {
        return array_filter($array, function($key) use ($fields) {
            return in_array($key, $fields);
        }, ARRAY_FILTER_USE_KEY);
    }
}


if (!function_exists('getCurrentController')) {
    /**
     * Get Current Controller
     *
     * @return mixed
     */
    function getCurrentController()
    {
        $route           = \Route::getCurrentRoute()->getActionName();
        $route_segment   = explode('\\', $route);
        $controller      = $route_segment[endKey($route_segment)];
        $controller_name = explode('@', $controller);
        $_controller     = [
            'name'   => $controller_name[0],
            'method' => isset($controller_name[1]) ? $controller_name[1] : null,
        ];

        return $_controller;
    }
}


if (!function_exists('isJson')) {
    /**
     * Is Json
     *
     * @param $string
     * @param bool $assoc
     *
     * @return bool
     */
    function isJson($string, $assoc = true)
    {
        // For best performance, check first char to be [ or {
        if (!in_array(substr($string, 0, 1), ['[', '{'])) {
            return false;
        }

        try {
            $decoded = json_decode($string, $assoc);
        } catch (\Exception $e) {
            return false;
        }

        // Check for json errors
        if (json_last_error() != JSON_ERROR_NONE) {
            return false;
        }

        // Validate the decoded result
        if (!is_object($decoded) && !is_array($decoded)) {
            return false;
        }

        return true;
    }
}


if (!function_exists('arrayToString')) {
    /**
     * Array To String
     * Flattens any array into a long string
     *
     * @param $array
     * @param bool $include_keys
     * @param string $separator
     *
     * @return string
     */
    function arrayToString($array, $include_keys = false, $separator = ' - ')
    {
        $res = '';

        if (is_array($array)) {
            array_walk_recursive($array, function ($item, $key) use (&$res, $include_keys, $separator) {
                if (!is_array($item)) {
                    if ($include_keys) {
                        $res = ' ' . $key . ': ' . $item . $separator . $res;
                    } else {
                        $res = $item . $separator . $res;
                    }
                }
            });
            $res = substr($res, 0, -3);
        } else {
            $res = $array;
        }

        return $res;
    }
}

if (!function_exists('array2table')) {
    /**
     * Array To Table
     *
     * @param $array
     * @param bool $recursive
     * @param array $args
     *
     * @return string
     */
    function array2table($array, $recursive = false, $args = [], $html_tables = false)
    {
        if (!defined('OPEN_TABLE_HTML')) {
            define('OPEN_TABLE_HTML', $html_tables ? "<table border='1' cellpadding='1' cellspacing='0' style='border: 1px solid #eee; font-family: Arial,Helvetica,sans-serif; color: #333; '>" : "<div class='tp-table'>");
            define('CLOSE_TABLE_HTML', $html_tables ? "</table>" : "</div>");
            define('OPEN_ROW_HTML', $html_tables ? "<tr>" : "<div class='tp-table-row'>");
            define('CLOSE_ROW_HTML', $html_tables ? "</tr>" : "</div>");
            define('OPEN_HEADER_CELL_HTML', $html_tables ? "<td bgcolor='#ececec' style='border: 1px solid #eee; background-color: #e2e3ff; padding: 3px 2px;'>" : "<div class='tp-table-cell'>");
            define('CLOSE_HEADER_CELL_HTML', $html_tables ? "</td>" : "</div>");
            define('OPEN_CELL_HTML', $html_tables ? "<td style='border: 1px solid #eee; padding: 3px 2px; '>" : "<div class='tp-table-cell'>");
            define('CLOSE_CELL_HTML', $html_tables ? "</td>" : "</div>");
        }

        $args = array_merge(['null' => '<div style="color: #eaeaea; font-size: 40%; text-align: center;">NULL</div>'], (array) $args);

        // Sanity check
        if (empty($array) || !is_array($array)) {
            return '';
        }

        if (!isset($array[key($array)]) || !is_array($array[key($array)])) {
            $array = [$array];
        }

        // Start the table
        $table = OPEN_TABLE_HTML;

        // The header
        // Take the keys from the first row as the headings
        $header   = '';
        $is_assoc = false;
        $header .= OPEN_ROW_HTML;
        foreach ((array)array_keys($array[key($array)]) as $heading) {
            if (is_string($heading)) {
                $is_assoc = true;
            }
            $header .= OPEN_HEADER_CELL_HTML . $heading . CLOSE_HEADER_CELL_HTML;
        }
        $header .= CLOSE_ROW_HTML;
        if ($is_assoc) {
            $table .= $header;
        }

        // The body
        foreach ((array)$array as $row) {
            $table .= OPEN_ROW_HTML;
            foreach ((array)$row as $cell) {
                $table .= OPEN_CELL_HTML;

                // Cast objects
                if (is_object($cell)) {
                    $cell = (array)$cell;
                }

                if ($recursive === true && is_array($cell) && !empty($cell)) {
                    // Recursive mode
                    $table .= "" . array2table($cell, true, $args) . "";
                } else {
                    $table .= (strlen(arrayToString($cell)) > 0) ?
                        arrayToString($cell, false, '<br>') : $args['null'];
                }

                $table .= CLOSE_CELL_HTML;
            }

            $table .= CLOSE_ROW_HTML;
        }

        $table .= CLOSE_TABLE_HTML;
        return $table;
    }
}


if (!function_exists('isArrayAssoc')) {
    /**
     * Is Array Assoc
     *
     * @param $array
     *
     * @return bool
     */
    function isArrayAssoc($array)
    {
        if (count(array_filter($array, 'is_scalar')) > 0) {
            return true;
        }
        return false;
//        return count(array_filter(array_keys(array_keys($array)), 'is_string')) > 0;
    }
}


if (!function_exists('transposeArray')) {
    /**
     * Transpose Array
     *
     * @param $array
     * @param $out
     * @param array $indices
     */
    function transposeArray($array, &$out, $indices = [])
    {
        if (is_array($array)) {
            foreach ($array as $key => $val) {
                //push onto the stack of indices
                $temp   = $indices;
                $temp[] = $key;
                transposeArray($val, $out, $temp);
            }
        } else {
            //go through the stack in reverse - make the new array
            $ref = &$out;
            foreach ((array)array_reverse($indices) as $idx) {
                $ref = &$ref[$idx];
            }
            $ref = $array;
        }
    }
}

if (!function_exists('snakeToWords')) {
    /**
     * Turns snake case to regular word with first char as capital case
     *
     * @param $string
     * @param bool $first_char_capital
     *
     * @return int
     */
    function snakeToWords($string, $first_char_capital = true)
    {
        $words = str_replace('_', ' ', snake_case($string));

        //remove dashes if they exist
        $words = str_replace('-', '', $words);

        if ($first_char_capital) {
            return ucwords($words);
        } else {
            return ucfirst($words);
        }
    }
}

if (!function_exists('snakeCase')) {
    /**
     * Returns snake case, takes into account a "-" sign
     *
     * @param $string
     * @return int
     * @internal param bool $first_char_capital
     *
     */
    function snakeCase($string)
    {
        $words = str_replace('-', '', snake_case($string));
        //remove dashes if they exist
        return $words;
    }
}

if (!function_exists('weekDays')) {
    /**
     * Return week days
     *
     * @param string $format
     *
     * @return array|\Illuminate\Support\Collection
     * @throws Exception
     */
    function weekDays($format = 'array')
    {
        $week_days = [
            'Sunday',
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
        ];

        switch ($format) {
            case 'array' :
                return $week_days;

            case 'collection' :
                return new \Illuminate\Support\Collection($week_days);

            case 'select2' :
                return (new \Illuminate\Support\Collection($week_days))->map(function($item, $idx) {
                        return ['id' => $idx, 'text' => $item];
                    });

            default:
                throw new Exception('Unsupported format request');
        }
    }
}

if (!function_exists('countArrayDimensions')) {
    /**
     * Count Array Dimensions
     *
     * @param $array
     *
     * @return int
     */
    function countArrayDimensions($array)
    {
        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = countArrayDimensions($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;
    }
}

if (!function_exists('cleanArray')) {
    /**
     * Remove from array $needle rows
     *
     * @param $array
     * @param null $needle
     * @return array
     */
    function cleanArray(&$array, $needle = null) {
        $temp_array = [];
        foreach ($array as $child => $value) {
            if ($value != $needle) {
                $temp_array[$child] = $value;
            }
        }

        return $array = $temp_array;
    }
}
if (!function_exists('isClosure')) {
    /**
     * Checks if the variable is a closure or not
     *
     * @param $suspected_closure
     *
     * @return bool
     */
    function isClosure($suspected_closure) {
        return $suspected_closure instanceof \Closure;
    }
}

if (!function_exists('sanitizeFileName')) {
    /**
     * cleans filename to become url compatible, inspired by wordpress
     *
     * @param $filename
     *
     * @return string
     */
    function sanitizeFileName($filename)
    {
        $special_chars = array("?", "[", "]", "/", "\\", "=", "<", ">", ":", ";", ",", "'", "\"", "&", "$", "#", "*", "(", ")", "|", "~", "`", "!", "{", "}", "%", "+", chr(0));

        // Filters the list of characters to remove from a filename.
        $filename = preg_replace("#\x{00a0}#siu", ' ', $filename);
        $filename = str_replace($special_chars, '', $filename);
        $filename = str_replace(array('%20', '+'), '-', $filename);
        $filename = preg_replace('/[\r\n\t -]+/', '-', $filename);
        $filename = trim($filename, '.-_');
        return $filename;
    }
}

if (!function_exists('findInCollection')) {
    /**
     * Check if there is a item in a collection by given key and value
     *
     * @param Illuminate\Support\Collection $collection collection in which search is to be made
     * @param string $key                               name of key to be checked
     * @param string $value                             value of key to be checkied
     *
     * @return boolean|object false if not found, object if it is found
     */
    function findInCollection(Illuminate\Support\Collection $collection, $key, $value)
    {
        foreach ($collection as $item) {
            if (isset($item->$key) && $item->$key == $value) {
                return $item;
            }
        }

        return false;
    }
}

if (!function_exists('insertSitesToSession')) {
    /**
     * Insert new site to session
     */
    function insertSitesToSession() {
        $user = Auth::user();
        if (!$user) {

            return;
        }
        $sites = (new SiteRepo())->getPermittedSites($user);
        session(['sites' => $sites]);
    }
}


if (!function_exists('translateDevices')) {

    /**
     * Translate Devices Names
     *
     * @param $device
     *
     * @return string
     */
    function translateDevices($device)
    {
        switch ($device) {
            case 'c':
                return 'Computer';
            case 't':
                return 'Tablet';
            case 'm':
            case 'p':
                return 'Mobile';
            case 'value':
                return 'new value';
            default :
                return 'Unknown';
        }
    }

    /**
     * Convert object values to utf8
     *
     * @param $object
     *
     * @return array
     */
     function convertObjectToUtf8($object){
         array_walk_recursive($object, function ($value) {
             if (is_string($value) || is_numeric($value) || is_null($value))
                 return utf8_decode(utf8_encode($value ?? ""));
             return $value;
         });
         return $object;
    }
}


if (!function_exists('bindParamArray')) {
    /**
     * Bind array params for sql query
     * @param $prefix - prefix for temp array values to bind. (prefix1, prefix2 etc...)
     * @param $values - values to bind for (prefix1, prefix2 etc...)
     * @param &$bindArray - reference for binding array to attached with the sql query.
     *
     * @return
     */
    function bindParamArray($prefix, $values, &$bindArray)
    {
        $str = "";
        foreach($values as $index => $value){
            $str .= ":".$prefix.$index.",";
            $bindArray[$prefix.$index] = $value;
        }
        return rtrim($str,",");
    }
}

