<?php

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteSettingDefault;
use App\Entities\Repositories\Sites\SettingsRepo;
use App\Facades\SiteConnectionLib;
use Illuminate\Database\Eloquent\Collection;

if (!function_exists('deviceType')) {
    /**
     * Get the current visitor device type.
     * d = Desktop, t = Tablet, m = Mobile.
     *
     * @return string
     */
    function deviceType()
    {
        $agent = new Jenssegers\Agent\Agent;

        if ($agent->isTablet()) {
            return 't';
        }

        if ($agent->isMobile()) {
            return 'm';
        }

        return 'd';
    }
}

if (!function_exists('siteAssetPath')) {
    /**
     * Get the current site asset path.
     *
     * @param string $path
     *
     * @return string
     */
    function siteAssetsPath($path = '')
    {
        $site = SiteConnectionLib::getSite();
        return 'top-assets' . DIRECTORY_SEPARATOR . 'sites' . DIRECTORY_SEPARATOR . $site->id . DIRECTORY_SEPARATOR . $path;
    }
}

if (!function_exists('minify')) {
    /**
     * HTML minification function.
     *
     * @param string $buffer
     *
     * @return string
     */
    function MinifyHTML($buffer)
    {
        $search  = [
            '/\>[^\S ]+/s',     // strip whitespaces after tags, except space
            '/[^\S ]+\</s',     // strip whitespaces before tags, except space
            '/(\s)+/s',         // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/' // Remove HTML comments
        ];
        $replace = [
            '>',
            '<',
            '\\1',
            ''
        ];
        $buffer  = preg_replace($search, $replace, $buffer);

        return $buffer;
    }
}

if (!function_exists('getRemoteInlineCSS')) {
    function getRemoteInlineCSS($site_id, $template_id)
    {
        $client  = app(GuzzleHttp\Client::class);
        $cms_url = env('CMS_URL', 'http://dev.cms.trafficpointltd.com/');
        $url     = sprintf('%s/api/sites/%s/templates-css/generate/%s', $cms_url, $site_id, $template_id);

        try {
            $response = $client->request('GET', $url);
            $css      = (string)$response->getBody();

            return $css;
        } catch (\Exception $ex) {

            return '/* Invalid cms response. */';
        }
    }
}

if (!function_exists('inlineFileContent')) {
    function inlineFileContent($site, $filename)
    {
        $placeholder = 'top-assets/sites/%s/css/%s';
        $file        = public_path(sprintf($placeholder, $site->id, $filename));
        if (File::exists($file)) {

            return File::get($file);
        }

        return '/* '.$filename.' was not found. */';
    }
}

if (!function_exists('adminBar')) {
    function adminBar()
    {
        $allowed_ips = explode(',', env('MAINTENANCE_ALLOWED_IPS', ''));

        if (!in_array(getUserIPAddress(), $allowed_ips)) {
            return;
        }

        print view('admin-bar');
    }
}

if (!function_exists('getUserIPAddress')) {
    function getUserIPAddress()
    {
        $request = request();

        if ($request->server->has('HTTP_CF_CONNECTING_IP')) {
            return $request->server->get('HTTP_CF_CONNECTING_IP');
        }

        return $request->getClientIp();
    }
}

if (!function_exists('isPage')) {
    function isPage($post)
    {
        return (new \App\Entities\Repositories\Sites\PostRepo())->isPage($post);
    }
}

if (!function_exists('mainCSS')) {
    function mainCSS($print = false,$site_id = 5)
    {
        $lastModified = false;
        $mainCssFile  = siteAssetsPath('css/main.css');

        if (file_exists($mainCssFile)) {
            if ($print) {
                return str_replace('../images', './top-assets/sites/'.$site_id.'/images',
                    str_replace('./icomoon/', './top-assets/sites/'.$site_id.'/css/icomoon/', File::get($mainCssFile)));
            }
            $lastModified = File::lastModified($mainCssFile);
        }

        if ($lastModified) {
            $mainCssFile .= '?lm=' . $lastModified;
        }

        return asset($mainCssFile);
    }
}

if (!function_exists('getSiteFromRouteParameter')) {
    /**
     * Get Site From Route Parameter
     *
     * @return object|string
     * @throws Exception
     */
    function getSiteFromRouteParameter()
    {
        if (!Route::current() && !Request::get('site')) {
            return null;
        }

        if (Route::current()->getParameter('site')) {
            $site = Route::current()->getParameter('site');
        } else {
            if (Request::get('site')) {
                $site = Request::get('site');
            } else {
                $site = config('app.cms_site_id');
            }
        }

        if (is_null($site)) {
            throw new Exception('Parameter site not found, please check route');
        }

        if (is_a($site, Site::class)) {
            return $site;
        }

        return Site::find($site);
    }
}

if (!function_exists('getTemplateFromRouteParameter')) {
    /**
     * Get Site From Route Parameter
     *
     * @return object|string
     * @throws Exception
     */
    function getTemplateFromRouteParameter()
    {
        $template = Route::current()->getParameter('template');
        if (is_null($template)) {
            throw new Exception('Parameter template not found, please check route');
        }

        if (is_a($template, \App\Entities\Models\Sites\Templates\Template::class)) {
            return $template;
        }

        return \App\Entities\Models\Sites\Templates\Template::find($template);
    }
}

if (!function_exists('getSiteSectionId')) {
    /**
     * Get Site Section Id
     *
     * @return null|integer
     */
    function getSiteSectionId()
    {
        return session('site.section_id');
    }
}

if (!function_exists('getBaseUrl')) {
    /**
     * Get site home url or create it
     *
     * @return mixed
     */
    function getBaseUrl()
    {

        return config('APP_URL', URL::to('/'));
    }
}

if (!function_exists('isJson')) {
    /**
     * Is Json
     *
     * @param      $string
     * @param bool $assoc
     *
     * @return bool
     */
    function isJson($string, $assoc = true)
    {
        // For best performance, check first char to be [ or {
        if (!in_array(substr($string, 0, 1), ['[', '{'])) {
            return false;
        }

        try {
            $decoded = json_decode($string, $assoc);
        } catch (\Exception $e) {
            return false;
        }

        // Check for json errors
        if (json_last_error() != JSON_ERROR_NONE) {
            return false;
        }

        // Validate the decoded result
        if (!is_object($decoded) && !is_array($decoded)) {
            return false;
        }

        return true;
    }
}

if (!function_exists('limitWordsByCharCount')) {
    /**
     * Returns the limited string
     *
     * @param $article_string
     * @param $char_indicator
     *
     * @return string
     */
    function limitWordsByCharCount($article_string, $char_indicator = 30)
    {

        if (strlen($article_string) > $char_indicator) {
            $string = wordwrap($article_string, $char_indicator);
            $i      = strpos($string, "\n");
            if ($i) {
                $string = substr($string, 0, $i);
            }

            return $string;
        } else {

            return $article_string;
        }
    }
}

if (!function_exists('calculateByFactor')) {

    function calculateByFactor($original = null, $multiplier = 1)
    {

        if ($original) {

            return intval($original * $multiplier);
        }

        return null;
    }
}

if (!function_exists('fullImagePathById')) {
    /**
     * Helper
     * Get image full path by image id
     *
     * @param $image_id
     *
     * @return null
     */
    function fullImagePathById($image_id)
    {
        if (is_null($image_id) || empty($image_id) || $image_id == 0) {

            return null;
        }

        $image = \App\Entities\Models\Sites\Media::find($image_id);

        if ($image) {

            return $image->getFullMediaPath();
        } else {

            return null;
        }
    }
}

if (!function_exists('fullRetinaImagePath')) {
    /**
     * Convert path to retina path if exists
     *
     * @param $path_1x          - base path
     * @param string $dimension - default @2x | @3x
     * @return null|string      - full path or null if not exists
     */
    function fullRetinaImagePath($path_1x, $dimension = '@2x') {
        if (empty($path_1x)) {
            return null;
        }

        $extension_file = substr($path_1x, strrpos($path_1x, '.') + 1);
        $path_without_extension = substr($path_1x, 0, strlen($path_1x) - strlen($extension_file) - 1);

        $path = $path_without_extension . $dimension . '.' . $extension_file;

        $local_path = substr($path, strlen(url('/')));
        $local_path = substr($local_path, 0, strpos($local_path, '?'));

        return File::exists(public_path() . $local_path)
            ? $path
            : '';
    }
}

if (!function_exists('getStringBeforeSeparator')) {

    /**
     * Helper
     * Returns a string value that comes before separator
     * Default: ' '
     *
     * @param $str
     * @param $separator
     *
     * @return string
     */
    function getStringBeforeSeparator($str, $separator = ' ')
    {
        return substr($str, 0, strrpos($str, $separator));
    }

}

if (!function_exists('toSlug')) {

    /**
     * Convert string to slug
     * page slug - separator => -
     * name slug - separator => _
     *
     * @param        $str
     * @param string $separator
     *
     * @return bool|mixed|null|string
     */
    function toSlug($str, $separator = '-')
    {
        // replace non letter or digits by -
        $str = preg_replace('~[^\pL\d]+~u', $separator, $str);

        // transliterate
        $str = iconv('utf-8', 'us-ascii//TRANSLIT', $str);

        // remove unwanted characters
        $str = preg_replace('~[^-\w]+~', '', $str);

        // trim
        $str = trim($str, $separator);

        // remove duplicate -
        $str = preg_replace('~-+~', $separator, $str);

        // lowercase
        $str = strtolower($str);

        if (empty($str)) {
            return null;
        }

        return $str;
    }
}

if (!function_exists('topMediaPath')) {
    /**
     * topMediaPath
     *
     * @param null   $file_name
     * @param string $extension
     *
     * @return string
     */
    function topMediaPath($file_name = null, $extension = 'png')
    {
        $site = SiteConnectionLib::getSite();

        //TODO replace the site->name with site->id
        $path = getBaseUrl() . DIRECTORY_SEPARATOR . 'top-assets' . DIRECTORY_SEPARATOR . snake_case($site->name,
                '_') . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

        if ($file_name) {
            $path .= toSlug($file_name, '_') . '.' . $extension;
        }

        return $path;
    }
}

if (!function_exists('snakeToWords')) {
    /**
     * Turns snake case to regular word with first char as capital case
     *
     * @param      $string
     * @param bool $first_char_capital
     *
     * @return int
     */
    function snakeToWords($string, $first_char_capital = true)
    {
        $words = str_replace('_', ' ', snake_case($string));

        //remove dashes if they exist
        $words = str_replace('-', '', $words);

        if ($first_char_capital) {
            return ucwords($words);
        } else {
            return ucfirst($words);
        }
    }
}

if (!function_exists('formatDate')) {
    /**
     * convert any valid date string to a different date format
     *
     * @param $date_string
     * @param $date_format
     *
     * @return string
     */
    function formatDate($date_string = 'now', $date_format = 'Y-m-d H:i:s')
    {
        return date($date_format, strtotime($date_string));
    }
}

if (!function_exists('snakeCase')) {
    /**
     * Returns snake case, takes into account a "-" sign
     *
     * @param $string
     *
     * @return int
     * @internal param bool $first_char_capital
     */
    function snakeCase($string)
    {
        $words = str_replace('-', '', snake_case($string));
        //remove dashes if they exist
        return $words;
    }
}

if (!function_exists('getFileUpdateTime')) {
    /**
     * returns asset last modification epoch time
     *
     * @param $asset
     *
     * @return int
     */
    function getFileUpdateTime($asset)
    {
        if (file_exists($asset)) {

            return filemtime($asset);
        } else {

            return '';
        }
    }
}

if (!function_exists('requireJsAsset')) {
    /**
     * Builds requirejs async request to bring in assets
     *
     * @param string $assets
     *
     * @return string
     */
    function requireJsAsset($assets)
    {
        if (env('APP_NAME') == 'cms') {
            $assets = json_encode($assets);

            return "<script>(function(){require([$assets])})()</script>";
        }
    }
}

if (!function_exists('toLowerCleanSequence')) {
    /**
     * Replaces any character which is not an alpha numeric
     *
     * @param $unfiltered_str
     *
     * @return string
     */
    function toLowerCleanSequence($unfiltered_str)
    {
        $filtered_str = '';

        if (strlen($unfiltered_str)) {
            $clean_str    = preg_replace("/[^A-Za-z0-9]/", '', $unfiltered_str);
            $filtered_str .= strtolower($clean_str);
        }

        return $filtered_str;
    }
}

if (!function_exists('getSiteSetting')) {

    function getSiteSetting($key)
    {
        $site          = SiteConnectionLib::getSite();
        $settings_repo = new SettingsRepo();
        $settings      = $settings_repo->getSetting($site->id, SiteSettingDefault::TYPE_SITE, $key);

        return $settings->value ?? null;
    }

}

if (!function_exists('getTemplateFromParameter')) {
    /**
     * @param $parameter
     *
     * @return mixed
     * @throws Exception
     */
    function getTemplateFromParameter($parameter)
    {
        if (is_null($parameter)) {
            throw new Exception('Parameter template not found, please check route');
        }

        return \App\Entities\Models\Sites\Templates\Template::find($parameter);
    }
}

if (!function_exists('object2Array')) {
    /**
     * Transforms object into array including ancestors
     * by using json encode decode
     *
     * @param $obj
     *
     * @return mixed
     */
    function object2Array($obj)
    {
        return json_decode(json_encode($obj), true);
    }
}

if (!function_exists('currency2Sign')) {
    /**
     * Translates the currency string to it's sign
     *
     * @param $currency_string
     *
     * @return String
     */
    function currency2Sign($currency_string)
    {
        switch (strtolower($currency_string)) {
            case 'usd' :
                return '$';
            case 'gbp' :
                return '�';
            case 'eur' :
                return '�';

            default:
                return $currency_string;
        }
    }
}

if (!function_exists('getUrlPath')) {
    /**
     * Breaks apart the url path to an array
     * TODO in the future we might want to break the url params as well
     *
     * @return array
     */
    function getUrlPath()
    {
        $url_path = Request::path() ?? '';

        return explode('/', $url_path);
    }
}

if (!function_exists('sortSelectByList')) {
    /**
     * Return collection by list sort
     *
     * @param $sortable
     * @param $list
     *
     * @return Collection
     */
    function sortSelectByList($sortable, $list)
    {
        $result = new Collection();

        foreach ($list as $item) {
            $result->push(['key' => $item, 'value' => $sortable->get($item)]);
            $sortable->forget($item);
        }

        foreach ($sortable as $key => $item) {
            $result->push(['key' => $key, 'value' => $item]);
        }

        return $result->pluck('value', 'key');
    }
}

if (!function_exists('isCurrentURL')) {
    /**
     * Check if a given url is the current visitor url.
     *
     * @param String $url
     *
     * @return array
     */
    function isCurrentURL($url)
    {
        return request()->fullUrlIs(strtok(strtok(url((string) $url), '?'), '#'));
    }
}

if (!function_exists('formatCurrency')) {

    /**
     * Format Currency
     * 1. add thousands comma
     * 2. set the number of decimal points
     *
     * @param     $value
     * @param int $decimals
     *
     * @return string|void
     */
    function formatCurrency($value, $decimals = 2)
    {
        if (!is_numeric($value)) {
            return $value;
        }

        return number_format($value, $decimals);
    }
}

if (!function_exists('htmlAttsToArray')) {
    /**
     * Turns html attributes string into an array
     *
     * @param string $string
     *
     * @return array
     */
    function htmlAttsToArray($string)
    {
        $pattern = '/(\\w+)\s*=\\s*("[^"]*"|\'[^\']*\'|[^"\'\\s>]*)/';
        preg_match_all($pattern, $string, $matches, PREG_SET_ORDER);

        $_atts = [];
        foreach ($matches as $match) {
            if (($match[2][0] == '"' || $match[2][0] == "'") && $match[2][0] == $match[2][strlen($match[2]) - 1]) {
                $match[2] = substr($match[2], 1, -1);
            }
            $name  = strtolower($match[1]);
            $value = html_entity_decode($match[2]);
            switch ($name) {
                case 'class':
                    $_atts[$name] = preg_split('/\s+/', trim($value));
                    break;
                case 'style':
                    // parse CSS property declarations
                    break;
                default:
                    $_atts[$name] = $value;
            }
        }

        return $_atts;
    }
}

if (!function_exists('filterAndSetParams')) {
    /**
     * Combine passed params with our default params.
     * The pairs should be considered to be all of the attributes which are
     * supported by the caller and given as a list. The returned attributes will
     * only contain the attributes in the $pairs list.
     * If the $atts list has unsupported attributes, then they will be ignored and
     * removed from the final returned list.
     *
     * @param array $pairs  Entire list of supported attributes and their defaults.
     * @param array $params User defined attributes in shortcode tag.
     *
     * @return array Combined and filtered attribute list.
     */
    function filterAndSetParams($pairs, $params)
    {
        $params = (array)$params;
        $out    = [];

        foreach ($pairs as $name => $default) {
            if (array_key_exists($name, $params)) {
                $out[$name] = $params[$name];
            } else {
                $out[$name] = $default;
            }
        }

        return $out;
    }
}

if (!function_exists('arraySearchKey')) {
    /**
     * Recursive search for key inside an array and returns the value
     *
     * @param $needle_key
     * @param $haystack
     *
     * @return bool|string
     */
    function arraySearchKey($needle_key, $haystack)
    {
        $result = false;
        array_walk_recursive($haystack, function ($item, $key) use ($needle_key, &$result) {
            if ($result === false && $key == $needle_key) {
                $result = $item;
            }
        });

        return $result;
    }
}

if (!function_exists('arrayCookieQuerySearch')) {
    /**
     * Array Cookie Query Search
     * Searches for cookie by its name ($query_key)
     *
     * @param $query_key
     * @param $haystack
     * @param $delimiter
     *
     * @return bool|string
     */
    function arrayCookieQuerySearch($query_key, $haystack, $delimiter = ';')
    {
        $cookie_val = false;
        array_walk_recursive($haystack, function ($item, $key) use ($query_key, &$cookie_val, $delimiter) {

            // possible item: "csrf=cae4de40-12b8-4c1c-85f0-2448758d4bc5;Path=/;Domain=.onlinedatingcash.com"
            if (strpos($item, $delimiter) !== false) {
                $cookie_blocks = explode($delimiter, $item);
            } else {
                $cookie_blocks = [$item];
            }

            foreach ($cookie_blocks as $cookie_block) {
                if ($cookie_block && (strpos($cookie_block, $query_key) !== false)) {
                    $cookie_val = explode('=', $cookie_block)[1];
                }
            }

        });

        return $cookie_val;
    }
}

if (!function_exists('getRobotMetaTagContent')) {

    /**
     * Get Robot MetaTag Content Depending on the params
     *
     * @param $post
     *
     * @return string
     */
    function getRobotMetaTagContent($post)
    {
        if (!empty($post)) {
            $index  = $post->robots_index ? 'INDEX' : 'NOINDEX';
            $follow = $post->robots_follow ? 'FOLLOW' : 'NOFOLLOW';

            return $index . ', ' . $follow;
        }
    }
}

if (!function_exists('getWithTracking')) {

    /**
     * Translate Devices Names
     *
     * @param $attributes
     *
     * @return string
     */
    function getWithTracking($attributes)
    {
        if (empty($attributes['link'])) {
            return '#';
        }

        // Check if there already are params inside the link or not
        $link = $attributes['link'];
        if (strpos($link, '?') === false) {
            $link .= '?';
        } else {
            $link .= '&';
        }

        // Sanitize all attributes
        unset($attributes['link']);
        foreach ($attributes as $key => $att_val) {
            $attributes[$key] = strtolower(sanitizeFileName($att_val));
        }

        $link .= http_build_query($attributes);

        return $link;
    }
}

if (!function_exists('sanitizeFileName')) {
    /**
     * cleans filename to become url compatible, inspired by wordpress
     *
     * @param $filename
     *
     * @return string
     */
    function sanitizeFileName($filename)
    {
        $special_chars = [
            "?",
            "[",
            "]",
            "/",
            "\\",
            "=",
            "<",
            ">",
            ":",
            ";",
            ",",
            "'",
            "\"",
            "&",
            "$",
            "#",
            "*",
            "(",
            ")",
            "|",
            "~",
            "`",
            "!",
            "{",
            "}",
            "%",
            "+",
            chr(0)
        ];

        // Filters the list of characters to remove from a filename.
        $filename = preg_replace("#\x{00a0}#siu", ' ', $filename);
        $filename = str_replace($special_chars, '', $filename);
        $filename = str_replace(['%20', '+'], '-', $filename);
        $filename = preg_replace('/[\r\n\t -]+/', '-', $filename);
        $filename = trim($filename, '.-_');
        return $filename;
    }
}