<?php

namespace App\Providers;

use App\Libraries\ShortCodes\Factory;
use Illuminate\Support\ServiceProvider;
use App\Libraries\ShortCodes\ShortCodesLib;
use App\Libraries\ShortCodes\Compilers\ShortcodeCompiler;
use App\Libraries\Widgets\ShortCodes as WidgetsShortCodes;

class ShortcodesServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerShortcodeDriver();
        $this->registerShortcodeCompiler();
        $this->registerView();
    }


    /**
     * Register short-code driver.
     */
    public function registerShortcodeDriver()
    {
        $this->app->singleton('shortcode.driver', function () {
            return new ShortCodesLib(new WidgetsShortCodes);
        });
    }

    /**
     * Register short-code compiler
     */
    public function registerShortcodeCompiler()
    {
        $this->app->singleton('shortcode.compiler', function ($app) {
            return new ShortcodeCompiler($app['shortcode.driver']);
        });
    }

    /**
     * Register Laravel view
     */
    public function registerView()
    {
        $this->app->singleton('view', function ($app) {
            // Next we need to grab the engine resolver instance that will be used by the
            // environment. The resolver will be used by an environment to get each of
            // the various engine implementations such as plain PHP or Blade engine.
            $finder = $app['view.finder'];
            $resolver = $app['view.engine.resolver'];

            $env = new Factory($resolver, $finder, $app['events'], $app['shortcode.compiler']);
            // We will also set the container instance on this view environment since the
            // view composers may be classes registered in the container, which allows
            // for great testable, flexible composers for the application developer.
            $env->setContainer($app);
            $env->share('app', $app);
            return $env;
        });
    }
}
