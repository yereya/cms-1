<?php

namespace App\Providers;

use App\Events\PostEvent;
use App\Events\BladeEvent;
use App\Events\PopupEvent;
use App\Events\MediaUpdate;
use App\Events\WidgetDataEvent;
use App\Events\SiteSettingsEvent;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Media;
use App\Entities\Models\Sites\Popup;
use App\Entities\Models\Sites\SiteSetting;
use App\Libraries\Events\ModelEventActions;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\SiteWidgetTemplate;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

/**
 * Class EventServiceProvider
 *
 * @package App\Providers
 */
class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\AssignAccounts'    => [
            'App\Listeners\EventAssignAccounts'
        ],
        'App\Events\AssignConversion'  => [
            'App\Listeners\EventAssignConversion'
        ],
        'App\Events\AlertEvent'        => [
            'App\Listeners\AlertListener'
        ],
        'App\Events\MediaUpdate'       => [
            'App\Listeners\EventMediaUpdate'
        ],
        'App\Events\PostEvent'         => [
            'App\Listeners\PostEventListener'
        ],
        'App\Events\TemplateEvent'     => [
            'App\Listeners\TemplateEventListener'
        ],
        'App\Events\DynamicListEvent'  => [
            'App\Listeners\DynamicListEventListener'
        ],
        'App\Events\WidgetDataEvent'   => [
            'App\Listeners\WidgetDataEventListener'
        ],
        'App\Events\SiteSettingsEvent' => [
            'App\Listeners\SiteSettingsEventListener'
        ],
        'App\Events\PopupEvent'        => [
            'App\Listeners\PopupEventListener'
        ],
        'App\Events\BladeEvent'        => [
            'App\Listeners\BladeEventListener'
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Media::created(function ($item) {
            event(new MediaUpdate($item, ModelEventActions::CREATED));
        });

        Media::updated(function ($item) {
            event(new MediaUpdate($item, ModelEventActions::UPDATED));
        });

        Media::deleted(function ($item) {
            event(new MediaUpdate($item, ModelEventActions::DELETED));
        });

        Post::updated(function ($post) {
            event(new PostEvent($post, ModelEventActions::UPDATED));
        });

        Post::deleted(function ($post) {
            event(new PostEvent($post, ModelEventActions::DELETED));
        });

        SiteWidgetTemplate::updated(function ($site_widget_template) {
            event(new BladeEvent($site_widget_template, ModelEventActions::UPDATED));
        });

        SiteWidgetTemplate::deleted(function ($site_widget_template) {
            event(new BladeEvent($site_widget_template, ModelEventActions::DELETED));
        });

        WidgetData::updated(function ($widget_data) {
            event(new WidgetDataEvent($widget_data, ModelEventActions::UPDATED));
        });

        WidgetData::deleted(function ($widget_data) {
            event(new WidgetDataEvent($widget_data, ModelEventActions::DELETED));
        });

        Popup::updated(function ($popup) {
            event(new PopupEvent($popup, ModelEventActions::UPDATED));
        });

        Popup::created(function ($popup) {
            event(new PopupEvent($popup, ModelEventActions::CREATED));
        });

        Popup::deleted(function ($popup) {
            event(new PopupEvent($popup, ModelEventActions::DELETED));
        });

        SiteSetting::updated(function ($site_settings) {
            event(new SiteSettingsEvent($site_settings, ModelEventActions::UPDATED));
        });

        SiteSetting::deleted(function ($site_settings) {
            event(new SiteSettingsEvent($site_settings, ModelEventActions::DELETED));
        });
    }
}
