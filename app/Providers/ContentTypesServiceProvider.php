<?php namespace App\Providers;

use App\Libraries\ShortCodes\ShortCodesLib;
use App\Libraries\ShortCodes\ShortCodes as ShortCodes;
use App\Libraries\ShortCodes\Contracts\ShortCodesLib as ShortCodesLibContract;
use App\Libraries\ShortCodes\Contracts\ShortCodes as ShortCodesContract;
use Assets;
use Illuminate\Support\ServiceProvider;

/**
 * Class ContentTypesServiceProvider
 *
 * @package App\Providers
 */
class ContentTypesServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
