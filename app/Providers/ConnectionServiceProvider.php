<?php

namespace App\Providers;

use App;
use App\Entities\Models\Sites\Site;
use App\Libraries\SiteConnection\SiteConnectionLib;
use Illuminate\Support\ServiceProvider;

class ConnectionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('SiteConnectionLib', function ($app) {
            $site = getSiteFromRouteParameter();

            $site = (!$site || $site->name == 'Cms') ? null : $site;

            $site_connection = new SiteConnectionLib($site);

            return $site_connection;
        });

    }
}
