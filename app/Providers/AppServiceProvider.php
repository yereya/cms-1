<?php

namespace App\Providers;

use Route;
use Assets;
use Illuminate\Support\ServiceProvider;
use App\Libraries\Widgets\BladeExtensions;
use App\Libraries\ShortCodes\ShortCodesLib;
use App\Entities\Models\Sites\Templates\Row;
use App\Entities\Models\Sites\Templates\WidgetData;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Libraries\ShortCodes\ShortCodes as ShortCodes;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use App\Libraries\ShortCodes\Contracts\ShortCodes as ShortCodesContract;
use App\Libraries\ShortCodes\Contracts\ShortCodesLib as ShortCodesLibContract;

/**
 * Class AppServiceProvider
 *
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    public $assets_collections = '../resources/assets/collections/*.{json}';

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::singularResourceParameters(false);

        Relation::morphMap([
            'row'    => Row::class,
            'widget' => WidgetData::class,
        ]);

        (new BladeExtensions())->register();

        $this->registerAssets();
    }

    /**
     * Register Application Assets
     */
    public function registerAssets()
    {
        // Reset the assets paths
        Assets::config([
            'js_dir'  => '',
            'css_dir' => ''
        ]);

        // Register all the collections
        foreach (glob($this->assets_collections, GLOB_BRACE) as $file) {
            $collection = [];

            foreach (json_decode(file_get_contents($file), true) as $asset) {
                if (is_array($asset) && isset($asset['type'])) {
                    $type         = $asset['type'] == 'css' ? '&.css' : '&.js';
                    $collection[] = $asset['url'] . $type;
                } else {
                    $collection[] = $asset;
                }
            }

            $file_name = explode('/', str_replace('.json', '', $file));
            $file_name = $file_name[count($file_name) - 1];

            Assets::registerCollection($file_name, $collection);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (in_array($this->app->environment(), ['local', 'develop'])) {

            /**
             * Loader for registering facades.
             */
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();

            /*
             * Load third party local providers
             */
            if (class_exists('\Barryvdh\Debugbar\ServiceProvider')) {
                $this->app->register(\Barryvdh\Debugbar\ServiceProvider::class);
            }
            if (class_exists('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider')) {
                $this->app->register(IdeHelperServiceProvider::class);
            }

            /*
             * Load third party local aliases
             */
            $loader->alias('Debugbar', \Barryvdh\Debugbar\Facade::class);
        }

        if (in_array($this->app->environment(), ['production', 'staging'])) {
            // Register providers for production ...
        }

        $this->app->bind(ShortCodesLibContract::class, ShortCodesLib::class);
        $this->app->bind(ShortCodesContract::class, ShortCodes::class);
    }
}
