<?php

namespace app\Providers;


use Barryvdh\Elfinder\ElfinderServiceProvider;
use Illuminate\Routing\Router;

class FileManagerServiceProvider extends ElfinderServiceProvider
{
    public function boot(Router $router)
    {
        $viewPath = __DIR__.'/../resources/views';
        $this->loadViewsFrom($viewPath, 'elfinder');
        $this->publishes([
            $viewPath => base_path('resources/views/vendor/elfinder'),
        ], 'views');

        if (!defined('ELFINDER_IMG_PARENT_URL')) {
            define('ELFINDER_IMG_PARENT_URL', $this->app['url']->asset('packages/barryvdh/elfinder'));
        }

        $config = $this->app['config']->get('elfinder.route', []);
        $config['namespace'] = 'App\Http\Controllers\Sites';

        $router->group($config, function($router)
        {
            $router->get('/', 'FileManagerController@showIndex');
            $router->any('connector', ['as' => 'elfinder.connector', 'uses' => 'FileManagerController@showConnector']);
            $router->get('popup/{input_id}', ['as' => 'elfinder.popup', 'uses' => 'FileManagerController@showPopup']);
            $router->get('filepicker/{input_id}', ['as' => 'elfinder.filepicker', 'uses' => 'FileManagerController@showFilePicker']);
            $router->get('tinymce', ['as' => 'elfinder.tinymce', 'uses' => 'FileManagerController@showTinyMCE']);
            $router->get('tinymce4', ['as' => 'elfinder.tinymce4', 'uses' => 'FileManagerController@showTinyMCE4']);
            $router->get('ckeditor', ['as' => 'elfinder.ckeditor', 'uses' => 'FileManagerController@showCKeditor4']);
            $router->post('find_file', ['as' => 'elfinder.find_file', 'uses' => 'FileManagerController@getFileId']);
            $router->get('ajax-modal', ['as' => 'elfinder.ajax-modal', 'uses' => 'FileManagerController@ajaxModal']);
            $router->post('file-update', ['as' => 'elfinder.file.update', 'uses' => 'FileManagerController@updateFileInfo']);
            $router->post('labels-update', ['as' => 'elfinder.labels', 'uses' => 'FileManagerController@updateFileLabels']);
        });
    }
}