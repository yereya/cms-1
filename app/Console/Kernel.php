<?php

namespace App\Console;


use App\Console\Commands\CheckAlerts;
use App\Console\Commands\Email;
use App\Console\Commands\Experiments;
use App\Console\Commands\HolsteredQueries;
use App\Console\Commands\LPValidator;
use App\Console\Commands\Maintenance\ConvertWidgetDataTable;
use App\Console\Commands\Maintenance\CopyBladeToDb;
use App\Console\Commands\Maintenance\PostReplace;
use App\Console\Commands\Parser\SyncDataToBo;
use App\Console\Commands\TracksMonitor;
use App\Console\Commands\TracksValidationAlert;
use App\Console\Commands\Maintenance\UpgradePosts;
use App\Console\Commands\Parser\Migrate;
use App\Console\Commands\NullProcess;
use App\Console\Commands\Parser\Logs;
use App\Console\Commands\SchedulerTasks;
use App\Console\Commands\Scraper;
use App\Console\Commands\Blocker;
use App\Console\Commands\SiteMigrate;
use App\Console\Commands\CheckSitesPageSpeed;
use App\Console\Commands\UploadAnalyticsConversions;
use App\Console\Commands\FacebookConversionUpload;
use App\Console\Commands\CompareBrandPrices;
use App\Console\Commands\CopyHierarchy;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

/**
 * Class Kernel
 *
 * @package App\Console
 */
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Scraper::class,
        Blocker::class,
        SchedulerTasks::class,
        HolsteredQueries::class,
        Experiments::class,
        Email::class,
        NullProcess::class,
        LPValidator::class,
        SiteMigrate::class,
        ConvertWidgetDataTable::class,
        UpgradePosts::class,
        CopyBladeToDb::class,
        PostReplace::class,
        Migrate::class,
        SyncDataToBo::class,
        Logs::class,
        CheckAlerts::class,
        CheckSitesPageSpeed::class,
        TracksValidationAlert::class,
        UploadAnalyticsConversions::class,
        FacebookConversionUpload::class,
        TracksMonitor::class,
        CompareBrandPrices::class,
        CopyHierarchy::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //
    }
}
