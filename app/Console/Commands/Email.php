<?php namespace App\Console\Commands;

use App\Console\Command;
use App\Libraries\Mail\MailLib;

/**
 * Class SchedulerTasks
 *
 * @package App\Console\Commands
 */
class Email extends Command
{

    /**
     * @var string $signature
     */
    protected $signature = 'email
                            {request : The name of the desired request to run}
                            {--criteria= : Pass emails criteria (ex: UNSEEN,ALL,FROM)}
                            {--limit= : The number of emails to fetch}
                            {--label= : The folder/label name to pull emails from}';

    /**
     * @var string $description
     */
    protected $description = 'Trigger email actions.';


    /**
     * Execute the console command
     *
     * @return array|null
     */
    public function fire()
    {
        if ($this->argument('request') == 'fetch') {

            // Clear empty values
            $attributes = filterAndSetParams([
                'criteria' => null,
                'limit' => null,
                'label' => null,
            ], $this->option());
            $attributes['criteria'] = str_replace('-', ' ', $attributes['criteria']);

            $emails = MailLib::fetch($attributes);

            dd($emails);
        }
    }

}