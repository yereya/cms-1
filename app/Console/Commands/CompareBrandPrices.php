<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\PriceComparison\PriceComparison;

class CompareBrandPrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comparePrices
                             {--email= : Email report}
                             {--sheet_name= : Sheet Name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Compare Our prices with brand's prices";

    private $priceComparison;
    private $emails;

    /**
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
//        $email               = $this->option('email') ? $this->option('email') : false;
        $this->priceComparison = new PriceComparison();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emails = $this->option('email') ? $this->option('email') : false;
        $sheet_name = $this->option('sheet_name') ? $this->option('sheet_name') : false;
        $this->priceComparison->run($emails,$sheet_name);

    }
}
