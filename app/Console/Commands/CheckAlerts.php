<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\PartnersAlerts\PartnersAlerts;

class CheckAlerts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alerts:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the partneralerts script';

    private $partners_alerts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->partners_alerts = new PartnersAlerts();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->partners_alerts->run();
    }
}
