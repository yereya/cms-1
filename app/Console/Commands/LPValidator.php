<?php namespace App\Console\Commands;

use App\Libraries\LPValidator\LPValidatorLib;
use App\Console\Command;

/**
 * Class LPValidator
 *
 * @package App\Console\Commands
 */
class LPValidator extends Command
{

    private $select_lp_query = '51';

    /**
     * @var string $signature
     */
    protected $signature = 'lp_validator
                            {--lp_id= : Comma delimited list of landing page ids}
                            {--query_id= : query id}
                            {--email= : Email report}';

    /**
     * @var string $description
     */
    protected $description = 'Trigger landing pages validation';


    /**
     * Execute the console command
     *
     * @return array|null
     */
    public function fire()
    {
        $lp_ids_arr = $this->option('lp_id') ? explode(',', $this->option('lp_id')) : [];
        $query_id   = $this->option('query_id') ?: $this->select_lp_query;

       $failed_urls = LPValidatorLib::validate($lp_ids_arr, $query_id, ['email' => $this->option('email')]);
        dd($failed_urls);
    }

}