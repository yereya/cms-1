<?php namespace App\Console\Commands;


use App\Console\Command;

/**
 * Class NullProcess
 *
 * @package App\Console\Commands
 */
class NullProcess extends Command
{

    /**
     * @var string $signature
     */
    protected $signature = 'null';

    /**
     * @var string $description
     */
    protected $description = 'Trigger null process, used for testing.';


    /**
     * Execute the console command
     *
     * @return array|null
     */
    public function fire()
    {
        dd("Null Process yeah !!");
    }

}