<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\TracksValidationAlert\TracksValidationAlert as TracksValidator;

class TracksValidationAlert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tracks:validate
                            {--email_by_permission : Set email sending by account permission } 
                            {--email= : Email report}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Validate the difference between dwh fact tracks and out tracks for the last 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email               = $this->option('email') ? $this->option('email') : false;
        $email_by_permission = $this->option('email_by_permission') ? true : false;

        // Create a params array which contains both arguments & options
        $params    = array_merge($this->argument(), $this->option());

        $result = TracksValidator::fetch([
            'email'               => $email,
            'email_by_permission' => $email_by_permission
        ]);

        var_dump($result);
    }
}
