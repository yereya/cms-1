<?php

namespace App\Console\Commands;

use App\Entities\Models\Sites\Site;
use App\Facades\SiteConnectionLib;
use Illuminate\Console\Command;
use League\Flysystem\Exception;

class SiteMigrate extends Command
{
    /**
     * The path for Artisan migrations
     *
     * @var string
     */
    private $path = 'app/Libraries/SiteConnection/migrations';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:site
                    {db_name : The name of the DB in sites table for the requested Site.}
                    {file? : Dummy run for specific file .},
                    {--rollback :  Rollback the last database migration}
                    {--force :  Force the operation to run when in production.}
                    {--pretend : Make a dummy run without executing the queries. Mainly used to verify the generated queries.}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Run migrations for a chosen site \n" .
         "Examples: \n".
         "Site up migration --\n".
         "-- artisan migrate:site tpsite_playright\n".
         "Site down migration\n".
         "-- artisan migrate:site tpsite_playright --rollback\n".
         "Show migration file sql query. Doesn't run anything on db\n".
         "-- artisan migrate:site tpsite_playright 2016_08_02_1_create_site_tables\n"
    ;


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $site = $this->getSite();

        $connection = SiteConnectionLib::setSite($site)->getConnection();

       if($this->argument('file')) {
          $this->showSqlForOneMigration();

           return;
       }


        if ($this->option('rollback')) {
            $this->call('migrate:rollback', [
                '--database' => $connection->getName(),
                '--force'    => $this->option('force'),
                '--pretend'  => $this->option('pretend'),
            ]);

        } else {
            $this->call('migrate', [
                '--database' => $connection->getName(),
                '--path'     => $this->path,
                '--force'    => $this->option('force'),
                '--pretend'  => $this->option('pretend'),
            ]);

        }
    }

    /**
     * Get Site
     *
     * @return mixed
     * @throws Exception
     */
    private function getSite()
    {
        $db_name = $this->argument('db_name');
        $site    = Site::where('db_name', $db_name)->first();

        if (!$site) {
            throw new Exception("Didn't find site with database name $db_name");
        }

        return $site;
    }


    /**
     * Show Sql For One Migration
     */
    private function showSqlForOneMigration()
    {
        $laravel = $this->getLaravel();
        $migrator = $laravel['migrator'];

        $file = $this->argument('file');
        $full_path = $laravel->basePath() . '/' . $this->path. '/' . $file . '.php';
        require_once($full_path);
        $migration_list = [$file];

        $migrator->runMigrationList($migration_list, ['pretend' => true]);

        foreach ($migrator->getNotes() as $note) {
            $this->output->writeln($note);
        }
    }
}
