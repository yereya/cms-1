<?php namespace App\Console\Commands;

use App;
use App\Console\Command;
use App\Libraries\CommandQueueLib;
use App\Libraries\Scraper\Scraper as ScraperLib;
use Exception;

/**
 * Class Scraper
 *
 * @package App\Console\Commands
 */
class Scraper extends Command
{

    /**
     * @var string $signature
     */
    protected $signature = 'scraper
                            {request : The name of the desired request to run}
                            {--provider= : Specify a provider to run the request on. if not set, all the providers will be used}
                            {--date-range-type=today_yesterday : Set date range for the request}
                            {--date-range-start= : If date range type is set to "custom_date", specify starting date in UTC, support string yesterday and today}
                            {--date-range-end= : If date range type is set to "custom_date", specify ending date in UTC, support string yesterday and today}
                            {--id= : Comma delimited list of tasks ids}
                            {--account_id= : Local account id}
                            {--update : If this parameter added then it will update (not create) bing conversion (specifiably for bing conversion upload) }
                            {--days_to_run_at= : Set numeric days(1-31) on which the script will run each month, separated by comma}
                            {--limit= : Set limit}';

    /**
     * @var string $description
     */
    protected $description = 'Executes the requests scraper to fetch new data';

    /**
     * Execute the console command
     */
    public function fire()
    {
        try{
            $scraper = new ScraperLib();

            // Create a params array which contains both arguments & options
            $params = array_merge($this->argument(), $this->option());
            if (!App::environment('local')) {
                $file_path = CommandQueueLib::start('scraper', $params);
            }

            $this->line('Started executing scraper. Start: ' . date('Y-m-d H:i:s'));
            $this->fireRequests($scraper);
            $this->line('Finished executing scraper. Finish: ' . date('Y-m-d H:i:s') . PHP_EOL);

            if (!App::environment('local')) {
                CommandQueueLib::end($file_path);
            }
        }catch (\Exception $e){
            if (!App::environment('local')) {
                CommandQueueLib::end($file_path);
            }
            throw new Exception($e);
        }


    }


    /**
     * Fire Requests
     *
     * @param ScraperLib $scraper
     *
     * @throws App\Libraries\Scraper\Exceptions\RequestNotExistsException
     */
    private function fireRequests(ScraperLib $scraper)
    {
        $options                     = $this->option();
        $options['date-range-start'] = ($options['date-range-start']) ? $this->parseToDate($options['date-range-start']) : null;
        $options['date-range-end']   = ($options['date-range-end']) ? $this->parseToDate($options['date-range-end']) : null;

        // Output the custom date range
        if (isset($options['date-range-start'])) {
            $this->line('  Range: [' . $options['date-range-start'] . ' - ' . $options['date-range-end'] . ']');
        }

        $scraper->setOptions($options);
        $scraper->runRequest($this->argument('request'));
    }
}
