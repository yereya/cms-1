<?php namespace App\Console\Commands;

use App\Libraries\Experiments\ExperimentLib;
use App\Console\Command;

/**
 * Class SchedulerTasks
 *
 * @package App\Console\Commands
 */
class Experiments extends Command
{

    /**
     * @var string $signature
     */
    protected $signature = 'experiment
                            {request : The name of the desired request to run}
                            {--id= : Comma delimited list of queries id}';

    /**
     * @var string $description
     */
    protected $description = 'Trigger experiment.';

    /**
     * Execute the console command
     */
    public function fire()
    {
        if ($this->argument('request') == 'trigger') {
            ExperimentLib::trigger($this->option('id'));
        } elseif ($this->argument('request') == 'do_scheduled') {
            ExperimentLib::doScheduledTasks();
        }
    }

}