<?php

namespace App\Console\Commands\Maintenance;

use App\Entities\Models\Sites\ContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteSeoSettings;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Facades\SiteConnectionLib;
use Carbon\Carbon;
use Illuminate\Console\Command;
use League\Flysystem\Exception;

class UpgradePosts extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maintenance:upgrade-posts
                        {db_name : database name you want change}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "maintenance:upgrade-posts" .
    "example: artisan maintenance:upgrade-posts tpsite_playright\n";


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $site       = $this->getSite();
        $connection = SiteConnectionLib::setSite($site)->getConnection();

        $this->checkFirstTimeRun();

        $this->fillPostsTable($site);
        $this->copyPagesTable($site);
        $this->updateWidgetData($site);
    }

    /**
     * Get Site
     *
     * @return mixed
     * @throws Exception
     */
    private function getSite()
    {
        $db_name = $this->argument('db_name');
        $site    = Site::where('db_name', $db_name)->first();

        if (!$site) {
            throw new Exception("Didn't find site with database name $db_name");
        }

        return $site;
    }

    /**
     * @param $site
     */
    private function copyPagesTable($site)
    {
        $site_content_type = SiteContentType::where('table', 'pages')->first();

        if (!$site_content_type) {
            $site_content_type               = new SiteContentType();
            $site_content_type->id           = 1;
            $site_content_type->site_id      = 5;
            $site_content_type->name         = 'Pages';
            $site_content_type->table        = 'pages';
            $site_content_type->has_comments = false;
            $site_content_type->has_likes    = false;
            $site_content_type->save();
        }

        $content_type_data = new ContentType();
        $content_type_data->setTable('pages');
        $pages = $content_type_data->newQuery()->get(['*']);

        foreach ($pages as $page) {
            $seo_settings = SiteSeoSettings::where('post_id', $page->id)
                ->where('post_table_name', 'pages')
                ->first();

            $seo_settings = $seo_settings ? $seo_settings->toArray() : [];

            $post                  = new Post($seo_settings);
            $post->slug            = $page->slug;
            $post->name            = $page->name;
            $post->priority        = $page->priority;
            $post->active          = 1;
            $post->section_id      = null;
            $post->created_at      = $page->created_at ?? Carbon::now();
            $post->updated_at      = $page->updated_at ?? Carbon::now();
            $post->deleted_at      = $page->deleted_at;
            $post->content_type_id = $site_content_type->id;

            if ($post->published) {
                $post->published_at = $page->created_at ?? Carbon::now();
            } else {
                $post->published_at = null;
            }

            $post->save();

            $page->post_id = $post->id;
            $page->setTable('pages');
            $page->save();
        }

        $keyd_pages = $pages->keyBy('id');

        //update parent ids
        foreach ($pages as $page) {
            if ($page->parent_id) {
                $parent_page     = $keyd_pages[$page->parent_id];
                $post            = Post::withTrashed()->where('id', $page->post_id)->first();
                $post->parent_id = $parent_page->post_id;
                $post->save();
            }
        }
    }

    /**
     * @param $site
     */
    private function fillPostsTable($site)
    {
        $site_content_types = $site->contentTypes;

        foreach ($site_content_types as $site_content_type) {
            $table             = $site_content_type->full_table;
            $content_type_data = new ContentType();
            $content_type_data->setTable($table);
            $content_type_rows = $content_type_data->newQuery()->get(['*']);

            foreach ($content_type_rows as $content_type_row) {

                $seo_settings = SiteSeoSettings::where('post_id', $content_type_row->id)
                    ->where('post_table_name', $table)
                    ->first();

                $seo_settings = $seo_settings ? $seo_settings->toArray() : [];

                $post               = new Post($seo_settings);
                $post->slug         = $content_type_row->slug;
                $post->name         = $content_type_row->title;
                $post->active       = $content_type_row->active;
                $post->section_id   = $content_type_row->section_id;
                $post->published_at = $content_type_row->published_at;
                $post->created_at   = $content_type_row->created_at;
                $post->updated_at   = $content_type_row->updated_at;
                $post->deleted_at   = $content_type_row->deleted_at;

                $post->content_type_id = $site_content_type->id;
                $post->save();

                $content_type_row->post_id = $post->id;

                $content_type_row->setTable($table);
                $content_type_row->save();
            }
        }
    }

    /**
     * Update widget Data
     */
    private function updateWidgetData()
    {
        $widget_data = WidgetData::whereNotNull('post_id')
            ->whereNotNull('content_type_id')
            ->orderBy('content_type_id')
            ->get();

        foreach ($widget_data as $widget_data_row) {
            $content_type_data = $this->getContentTypeData($widget_data_row);

            if (!$content_type_data) {
                continue;
            }

            $widget_data_row->post_id = $content_type_data->post_id;
            $widget_data_row->save();
        }
    }

    /**
     * @param $site_content_type
     * @param $widget_data_row
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getContentTypeData($widget_data_row)
    {
        $site_content_type = SiteContentType::find($widget_data_row->content_type_id);
        $model             = new ContentType();
        $model->setTable($site_content_type->full_table);
        $content_type_data = $model->newQuery()
            ->find($widget_data_row->post_id);

        return $content_type_data;
    }

    /**
     * Check First Time Run
     *
     * @throws Exception
     */
    private function checkFirstTimeRun()
    {
        if (Post::count()) {

            throw new Exception('Table posts is not empty');
        }
    }

}
