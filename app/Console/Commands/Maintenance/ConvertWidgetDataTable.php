<?php

namespace App\Console\Commands\Maintenance;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Facades\SiteConnectionLib;
use Illuminate\Console\Command;
use League\Flysystem\Exception;

class ConvertWidgetDataTable extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maintenance:convert-widget-data
                        {db_name : database name you want change}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Convert data coloumn of widget data table \n" .
         "extract column widget_template_id, content_type_id, post_id, menu_id to there own column\n".
         "example: artisan maintenance:convert-widget-data tpsite_playright\n"
    ;


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $site = $this->getSite();
        $connection = SiteConnectionLib::setSite($site)->getConnection();

        $widget_data_rows = WidgetData::all();
        foreach ($widget_data_rows as $row)
        {
            $data_attributes = $row->data;
            if(isset($data_attributes->content_type_id)) {
                $row->content_type_id = $data_attributes->content_type_id;
                unset($data_attributes->content_type_id);
            }

            if(isset($data_attributes->widget_template_id)) {
                $row->widget_template_id = $data_attributes->widget_template_id;
                unset($data_attributes->widget_template_id);
            }

            if(isset($data_attributes->post_id)) {
                $row->post_id = $data_attributes->post_id;
                unset($data_attributes->post_id);
            }
            if(isset($data_attributes->menu_id)) {
                $row->menu_id = $data_attributes->menu_id;
                unset($data_attributes->menu_id);
            }

            $row->data = $data_attributes;
            $row->save();
        }

    }

    /**
     * Get Site
     *
     * @return mixed
     * @throws Exception
     */
    private function getSite()
    {
        $db_name = $this->argument('db_name');
        $site    = Site::where('db_name', $db_name)->first();

        if (!$site) {
            throw new Exception("Didn't find site with database name $db_name");
        }

        return $site;
    }
}
