<?php

namespace App\Console\Commands\Maintenance;

use App\Entities\Models\Sites\ContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteSeoSettings;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Repositories\Sites\ContentTypeRepo;
use App\Facades\SiteConnectionLib;
use Carbon\Carbon;
use Illuminate\Console\Command;
use League\Flysystem\Exception;

class PostReplace extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maintenance:posts-replace
                        {--db= : database name you want change}
                        {--search= : item to search for}
                        {--replace= : item to replace}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "maintenance:upgrade-posts" .
    'example: artisan maintenance:posts-replace --db=playright --search=\"value to search for\" --replace=\"new value\"\n';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $content_type_repo = (new ContentTypeRepo());
        SiteConnectionLib::setSite($this->getSite());

        $posts = Post::get();

        $posts_changed = 0;
        $total_changes = 0;
        $posts->each(function ($post) use (&$posts_changed, &$total_changes, $content_type_repo) {

            $flag = false;
            foreach ($post->content->getAttributes() as $key => $val) {

                if (strpos($val, $this->option('search'))) {
                    $post->content->{$key} = str_replace($this->option('search'), $this->option('replace'), $val);
                    $total_changes++;
                    $flag = true;
                    dump($post->id . ' - ' . $val);
                }
            }

            if ($flag) {
                $posts_changed++;
                $content_type_repo->saveModel($post->content, $post);
            }
        });

        $this->info('=======Stats=======');
        $this->info("DB: {$this->option('db')}");
        $this->info("Search: \"{$this->option('search')}\"");
        $this->info("Replace: \"{$this->option('replace')}\"");
        $this->info("Posts changed: {$posts_changed}");
        $this->info("Total changed: {$total_changes}");
        $this->info('===================');
    }

    /**
     * Get Site
     *
     * @return mixed
     * @throws Exception
     */
    private function getSite()
    {
        $db_name = $this->option('db');
        $site    = Site::where('db_name', $db_name)->first();

        if (!$site) {
            throw new Exception("Didn't find site with database name $db_name");
        }

        return $site;
    }
}
