<?php

namespace App\Console\Commands\Maintenance;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Widgets\SiteWidgetTemplate;
use App\Entities\Models\Sites\Widgets\WidgetTemplate;
use App\Facades\SiteConnectionLib;
use App\Libraries\SiteWidgetTemplates\SiteWidgetTemplatesLib;
use Illuminate\Console\Command;

class CopyBladeToDb extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maintenance:copy-blade-to-db';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Copy all widgets blade to cms.site_widget_templates \n";


    /**
     * Execute the console command.
     */
    public function handle()
    {

        $this->copyFilesToDb();

        $this->createFilesFromDb();
    }

    /**
     * Copy Files to Db
     */
    private function copyFilesToDb()
    {
        $site = Site::where('db_name', 'tpsite_playright')->first();
        $connection = SiteConnectionLib::setSite($site)->getConnection();

        $site_widget_templates = SiteWidgetTemplate::with('widget')->get();

        foreach ($site_widget_templates as $site_widget_template) {
            $filename = base_path() . "/app/Libraries/Widgets/Partials/Templates/{$site_widget_template->widget->controller}/{$site_widget_template->file_name}.blade.php";
            if (file_exists($filename)) {
                $content = file_get_contents($filename);

                //remove header and footer includes from templates
                $content = preg_replace("/@include\\('WidgetsLib::Assets.widget-wrapper',\\s*\\['part'\\s*=>\\s*'open'\\]\\).*\n+/", '', $content);
                $content = preg_replace("/@include\\('WidgetsLib::Assets.widget-wrapper',\\s*\\['part'\\s*=>\\s*'close'\\]\\).*(\n|$)/", '', $content);

                if (strpos($content, 'WidgetsLib::Assets.widget-wrapper') !== false) {
                    echo "@@ widget-wrapper " . $filename . PHP_EOL;
                }

                if (strpos($content, 'include') !== false) {
                    echo "With include - " . $filename . PHP_EOL;
                }

                $site_widget_template->blade = $content;

                $widget_template = WidgetTemplate::where('site_widget_template_id', $site_widget_template->id)->first();
                if ($widget_template) {
                    $site_widget_template->scss = $widget_template->scss;
                }

                $site_widget_template->site_id = 5;
                $site_widget_template->published = true;

                $site_widget_template->save();

            } else {
                echo "NOT EXISTS - " . $filename . PHP_EOL;
            }
        }
    }

    private function createFilesFromDb()
    {
        $site = Site::find(5);
        $widgets_view_path = config('view.widgets_view_path') . '/' . $site->id;
        $templates_lib = new SiteWidgetTemplatesLib($site, $widgets_view_path);
        $templates_lib->createAllWidgetTemplates($site);
    }

}
