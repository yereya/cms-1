<?php

namespace App\Console\Commands;

use App\Console\Command;
use App\Libraries\PageSpeed\PageSpeed;

/**
 * Class CheckSitesPageSpeed
 * @package App\Console\Commands
 */
class CheckSitesPageSpeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:sites:page_speed
                               {--emails= : Email recipient}';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pageSpeed = new PageSpeed($this->options());
        $pageSpeed->saveFromRequest();

        return $pageSpeed->notifyUserByMailAccordingToScoreDifference();
    }
}
