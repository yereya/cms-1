<?php

namespace App\Console\Commands\Parser;

use App\Libraries\PerconaMigration\OutCmsMigration;
use Illuminate\Console\Command;
use Log;

class SyncDataToBo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:SyncDataToBo
                            { --database= : Available DataBases are [OUT,CMS] }';

    /**
    * @var string
    */
    private $database;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize tables from OUT or/and CMS to BO';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {

        parent::__construct();

        $this->database = new OutCmsMigration();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->database->run($this->option('database'));
    }
}
