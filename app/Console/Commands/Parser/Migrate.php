<?php

namespace App\Console\Commands\Parser;

use App\Libraries\PerconaMigration\OutMigration;
use Illuminate\Console\Command;

class Migrate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:migrate 
                            { --scraper= : Scraper name - current only [scraper_bo, scraper_tracks]}
                            { --to_date=null : Set date limit }
                            ';

    /**
     * @var OutMigration
     */
    private $out;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate data from db out to mrr';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->out = new OutMigration();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->out->run($this->option('scraper'),$this->option('to_date'));
    }
}
