<?php

namespace App\Console\Commands\Parser;

use App\Libraries\PerconaMigration\LogMigrations;
use Illuminate\Console\Command;

class Logs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:logs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync table cms.logs with table dwh.logs';

    private $log_migrations;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->log_migrations = new LogMigrations();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->log_migrations->run();
    }
}
