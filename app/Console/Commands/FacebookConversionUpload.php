<?php

namespace App\Console\Commands;

use App\Libraries\FacebookConversions\FacebookConversions;
use Illuminate\Console\Command;

class FacebookConversionUpload extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'facebook_conversions:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the upload Facebook conversions upload';

    private $upload_conversions;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->upload_conversions = new FacebookConversions();
        $this->upload_conversions->run();
    }
}
