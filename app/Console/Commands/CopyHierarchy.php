<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CopyHierarchy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy_hierarchy
                             {--from_advertiser= : Advertiser to copy from}
                             {--to_advertiser= : To which advertiser to copy}
                             {--name_extension= : The  extension which will be added to the names}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Duplicate existing hierarchy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Payroll
        $old_adv = $this->option()['from_advertiser'];
        $new_adv = $this->option()['to_advertiser'];
        $name_extension = $this->option()['name_extension'];

        $out_connection = \DB::connection('putin');
        $brands = $out_connection->table('brands')->select('*')->whereIn('advertiser_id',array($old_adv))->get();
        try{
            foreach ($brands as $brand){
                $new_brand = (array)$brand;
                $new_brand['mongodb_id'] = \Uuid::generate(1)->string;

                $new_brand['name'] = $brand->name . ' - ' . $name_extension;
                $new_brand['advertiser_id'] = $new_adv;
                unset($new_brand['createdAt']);
                unset($new_brand['updatedAt']);
                unset($new_brand['deleted_at']);
                unset($new_brand['id']);
                try{
                    $out_connection->table('brands')->insert($new_brand);

                }catch (\Exception $e){
                    $temp = $e;
                }
                $campaigns = $out_connection->table('campaigns')->select('*')->whereIn('brand_id',array($brand->id))->get()->toArray();
                foreach ($campaigns as $campaign){

                    $new_campagin = (array)$campaign;
                    $new_brand['brand_id'] = $out_connection->table('brands')->select('*')->whereIn('mongodb_id',array($new_brand['mongodb_id']))->first()->id;
                    $new_campagin['brand_id'] = $new_brand['brand_id'];
                    $new_campagin['brand_mongodb_id'] = $new_brand['mongodb_id'];
                    $new_campagin['mongodb_id'] = \Uuid::generate(1)->string;
                    $new_campagin['name'] =  $new_campagin['name'] . ' - ' . $name_extension;
                    unset($new_campagin['createdAt']);
                    unset($new_campagin['updatedAt']);
                    unset($new_campagin['id']);
                    $out_connection->table('campaigns')->insert($new_campagin);

                    // For each campaign create placements
                    $new_campagin['id'] = $out_connection->table('campaigns')->select('*')->whereIn('mongodb_id',array($new_campagin['mongodb_id']))->first()->id;
                    $placements = $out_connection->table('placements')->select('*')->whereIn('campaign_id',array($campaign->id))->get()->toArray();
                    foreach ($placements as $placement){
                        $new_placement = (array)$placement;
                        $new_placement['campaign_id'] = $new_campagin['id'];
                        $new_placement['mongodb_id'] = \Uuid::generate(1)->string;
                        $new_placement['campaign_mongodb_id'] = $new_campagin['mongodb_id'];
                        $new_placement['name'] = $new_placement['name']  . ' - ' . $name_extension;
                        unset($new_placement['createdAt']);
                        unset($new_placement['updatedAt']);
                        unset($new_placement['id']);

                        $out_connection->table('placements')->insert($new_placement);
                    }
                    // For each campaign create landing page
                    $landing_pages = $out_connection->table('landing_pages')->select('*')->whereIn('campaign_id',array($campaign->id))->get()->toArray();
                    foreach ($landing_pages as $landing_page) {
                        $new_landing_page = (array)$landing_page;
                        $new_landing_page['campaign_id'] = $new_campagin['id'];
                        $new_landing_page['mongodb_id'] = \Uuid::generate(1)->string;
                        $new_landing_page['campaign_mongodb_id'] = $new_campagin['mongodb_id'];
                        $new_landing_page['name'] = $new_landing_page['name'] . ' - ' . $name_extension;
                        unset($new_landing_page['createdAt']);
                        unset($new_landing_page['updatedAt']);
                        unset($new_landing_page['id']);

                        $out_connection->table('landing_pages')->insert($new_landing_page);
                    }
                }
            }
        }catch (\Exception $e){
            throw new \Exception('$e');
        }
    }
}
