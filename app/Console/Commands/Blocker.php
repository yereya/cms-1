<?php namespace App\Console\Commands;

use App\Console\Command;
use App\Libraries\Blocker\Blocker as BlockerLib;

/**
 * Class Blocker
 *
 * @package App\Console\Commands
 */
class Blocker extends Command
{

    /**
     * @var string $signature
     */
    protected $signature = 'blocker
                            {--id= : The ID of the block query you wish to use. otherwise, all will be used}
                            {--refresh : Should the blocked IDs be refreshed}
                            {--account_id= : Set the account ID for which you wish to refresh the IP}
                            {--ip= : Set the IP you wish to refresh}
                            {--release : Get IP from google}
                            {--rm : All IPs to release}';

    /**
     * @var string $description
     */
    protected $description = 'Executes the blocker';

    /**
     * Execute the console command
     */
    public function fire()
    {
        $blocker = new BlockerLib();

        $this->line('Started executing blocker');
        if($this->option('refresh')) {
            $blocker->refresh($this->option('account_id'), $this->option('ip'));
        } elseif ($this->option('release')) {
            if ($this->option('rm') && !$this->option('account_id')) {
                $this->line('Error console command, need index account_id.');
            } else {
                $blocker->releaseIps($this->option('id'), $this->option('account_id'), $this->option('rm'));
            }
        } else {
            $blocker->run($this->option('id'));
        }
        $this->line('Finished executing blocker');

    }

}