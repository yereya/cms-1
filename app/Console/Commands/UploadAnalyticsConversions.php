<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Libraries\GoogleAnalytics\GoogleAnalytics;

class UploadAnalyticsConversions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'google_analytics:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Runs the upload google analytics script';

    private $upload_conversions;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->upload_conversions = new GoogleAnalytics();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->upload_conversions->run();
    }
}
