<?php namespace App\Console\Commands;

use App\Libraries\Scheduler\SchedulerTasksLib;
use Illuminate\Console\Command;

/**
 * Class SchedulerTasks
 *
 * @package App\Console\Commands
 */
class SchedulerTasks extends Command
{

    /**
     * @var string $signature
     */
    protected $signature = 'scheduler';

    /**
     * @var string $description
     */
    protected $description = 'Runs the queued tasks.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        (new SchedulerTasksLib())->fireCron();
    }

}