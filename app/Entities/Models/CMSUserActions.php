<?php

namespace App\Entities\Models;

use App\Entities\Models\Bo\Report;
use Illuminate\Database\Eloquent\Model;

class CMSUserActions extends Model
{
    //id, user_id, user_name, cms_users_action_id, report_id, report_name, run_duration, sql_query, created_at, updated_at
    protected $connection = 'cms';
    protected $table = 'report_requests_logs';

    protected $dates = ['created_at', 'updated_at'];
    protected $fillable = ["user_id", "user_name", "report_name", "run_duration", "cms_users_action_id", "report_id", "sql_query", 'report_date_from', 'report_date_to'];
}
