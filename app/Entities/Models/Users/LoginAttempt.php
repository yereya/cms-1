<?php namespace App\Entities\Models\Users;

use App\Entities\Models\Model;

/**
 * App\Entities\Models\Users\LoginAttempt
 *
 * @property-read User $user
 * @property integer   $id
 * @property integer   $user_id
 * @property string    $username
 * @property string    $ip
 * @property string    $timestamp
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\LoginAttempt whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\LoginAttempt whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\LoginAttempt whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\LoginAttempt whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\LoginAttempt whereTimestamp($value)
 */
class LoginAttempt extends Model
{

    public $timestamps = false;
    protected $table = 'user_login_attempts';

    /**
     * User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}