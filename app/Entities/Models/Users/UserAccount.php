<?php

namespace App\Entities\Models\Users;

use App\Entities\Models\Reports\Publishers\Account;
use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    /**
     * @var string
     */
    protected $table = 'reports_publishers_accounts_users';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'account_id'];

    /**
     * @var bool - not columns updated_at and created_at in table
     */
    public $timestamps = false;

    /**
     * Users
     * many to many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany(User::class);
    }

    /**
     * Accounts
     * many to many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function accounts() {
        return $this->belongsToMany(Account::class);
    }

}
