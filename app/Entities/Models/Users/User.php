<?php namespace App\Entities\Models\Users;

use App\Entities\Models\Alerts\AlertLog;
use App\Entities\Models\Bo\Account;
use App\Entities\Models\Model;
use App\Entities\Models\Reports\Publishers\Change;
use App\Entities\Models\Sites\Site;
use App\Http\Traits\CanResetPasswordTrait;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;

/**
 * App\Entities\Models\Users\User
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, AuthorizableContract
{
    use Authorizable;
    use Authenticatable;
    use CanResetPasswordTrait;
    use SoftDeletes;
    use Notifiable;

    protected $connection = 'cms';
    protected $dates = ['deleted_at'];
    protected $hidden = ['password'];
    protected $fillable = [
        'username',
        'email',
        'first_name',
        'last_name',
        'password',
        'status',
        'password_expiry_date',
        'role',
        'permitted_ips',
        'remember_token',
        'thumbnail'
    ];

    /**
     * returns the table name for this model
     *
     * @return string
     */
    public static function getTableName()
    {
        return with(new self)->getTable();
    }

    /**
     * Set Password Attribute
     * Mutator to catch the password set and hash it with random password salt defined for the specific user
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        if ($password == '********') {
            return;
        }

        $this->attributes['password'] = password_hash($password, PASSWORD_BCRYPT);
    }

    /**
     * Sites
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sites()
    {
        return $this->belongsToMany(Site::class);
    }

    /**
     * Login Attempts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function loginAttempts()
    {
        return $this->hasMany(LoginAttempt::class);
    }

    /**
     * Special Permissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function specialPermissions()
    {
        return $this->belongsToMany(AccessControl\Permission::class, 'site_user_permission');
    }

    /**
     * Notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notifications\Notification::class);
    }

    /**
     * Notification Rules
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function notificationRules()
    {
        return $this->belongsToMany(Notifications\Rule::class, 'user_notification_rule_user');
    }

    /**
     * Reports changes logs
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function change()
    {
        return $this->hasMany(Change::class, 'reports_publishers_changes');
    }

    /**
     * Assign the given role to the user.
     *
     * @param string|AccessControl\Role $role
     *
     * @return AccessControl\Role
     */
    public function assignRole($role)
    {
        $this->roles()->save($this->getStoredRole($role));
    }

    /**
     * Roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(AccessControl\Role::class, 'site_user_role');
    }

    /**
     * Get Stored Role
     *
     * @param $role
     *
     * @return mixed
     */
    protected function getStoredRole($role)
    {
        if (is_string($role)) {
            return app(AccessControl\Role::class)->findByName($role);
        }

        return $role;
    }

    /**
     * Revoke the given role from the user.
     *
     * @param string|AccessControl\Role $role
     *
     * @return mixed
     */
    public function removeRole($role)
    {
        $this->roles()->detach($this->getStoredRole($role));
    }

    /**
     * Determine if the user has any of the given role(s).
     *
     * @param string|AccessControl\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasAnyRole($roles)
    {
        return $this->hasRole($roles);
    }

    /**
     * Determine if the user has (one of) the given role(s).
     *
     * @param string|AccessControl\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasRole($roles)
    {
        $user_roles = session('user_roles');
        if (is_string($roles)) {
            return $user_roles->contains('name', $roles);
        }

        if ($roles instanceof AccessControl\Role) {
            return $user_roles->contains('id', $roles->id);
        }

        return (bool)!!$roles->intersect($user_roles)->count();
    }

    /**
     * Determine if the user has all of the given role(s).
     *
     * @param string|AccessControl\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasAllRoles($roles)
    {
        if (is_string($roles)) {
            return $this->roles->contains('name', $roles);
        }

        if ($roles instanceof AccessControl\Role) {
            return $this->roles->contains('id', $roles->id);
        }

        $roles = collect()->make($roles)->map(function ($role) {
            return $role instanceof AccessControl\Role ? $role->name : $role;
        });

        return $roles->intersect($this->roles->pluck('name')) == $roles;
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param AccessControl\Permission $permission
     *
     * @return bool
     */
    public function hasPermissionTo($permission)
    {
        if (is_string($permission)) {
            $permission = app(AccessControl\Permission::class)->findByName($permission);
        }

        return $this->hasSpecialPermission($permission) || $this->hasPermissionViaRole($permission);
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param AccessControl\Permission $permission
     *
     * @return bool
     */
    public function hasPermissionToNew($permission)
    {

        return $this->hasSpecialPermissionNew($permission) || $this->hasPermissionViaRoleNew($permission);
    }

    protected function hasSpecialPermissionNew($permission)
    {

        $special_permissions = session('user_special_permissions');

        return $special_permissions ? $special_permissions->contains('id', $permission->id) : false;
    }
    /**
     * Determine if the user has has the given permission.
     *
     * @param AccessControl\Permission $permission
     *
     * @return bool
     */
    protected function hasSpecialPermission(AccessControl\Permission $permission)
    {
        if (is_string($permission)) {
            $permission = app(AccessControl\Permission::class)->findByName($permission);
        }

        $special_permissions = session('user_special_permissions');

        return $special_permissions ? $special_permissions->contains('id', $permission->id) : false;
    }

    /**
     * Determine if the user has, via roles, has the given permission.
     *
     * @param AccessControl\Permission $permission
     *
     * @return bool
     */
    protected function hasPermissionViaRole(AccessControl\Permission $permission)
    {
        if (is_string($permission)) {
            $permission = app(AccessControl\Permission::class)->findByName($permission);
        }
        return $this->hasRole($permission->roles);
    }

    protected function hasPermissionViaRoleNew($permission)
    {

        $roles_allowed_for_permission = \DB::connection('cms')->table('user_permission_role')
            ->select('*')
            ->where('permission_id','=',$permission->id)->get()->toArray();

        $user_roles = session('user_roles');
        if(!$user_roles){
            $user_roles = $this->roles()->get();
            if($user_roles)
                session(['user_roles' => $user_roles]);
        }
        if($user_roles) {
            $user_roles_unique = array_unique(array_column($user_roles->toArray(), 'id'));

            if (is_string($roles_allowed_for_permission)) {
                return $user_roles->contains('name', $roles_allowed_for_permission);
            }

            if ($roles_allowed_for_permission instanceof AccessControl\Role) {
                return $user_roles->contains('id', $roles_allowed_for_permission->id);
            }

            if (is_array($roles_allowed_for_permission)) {
                $roles_allowed_for_permission = array_column($roles_allowed_for_permission, 'role_id');
                return array_intersect($user_roles_unique, $roles_allowed_for_permission) ? true : false;
            }
        }

        return false;

    }
    /**
     * Alert Logs
     * relationship with alerts_log via pivot alerts_users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function alertLogs()
    {
        return $this->belongsToMany(AlertLog::class, 'alerts_users')
            ->withPivot('viewed', 'reminder_date')
            ->orderBy('viewed', 'ASC');
    }

    /**
     * Accounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function accounts()
    {
        return $this->belongsToMany(
            Account::class,
            "user_assigned_accounts",
            "user_id",
            "account_id"
        );
    }

    /**
     * @param $query QueryBuilder
     * @return mixed
     */
    public function scopeIsActive($query)
    {
        return $query->where('status', '=', '1');
    }
}
