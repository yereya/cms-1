<?php namespace App\Entities\Models\Users\Notifications;

use App\Entities\Models\Model;
use App\Entities\Models\Users\User;

/**
 * App\Entities\Models\Users\Notifications\Notification
 *
 * @property-read User      $user
 * @property-read Rule      $rule
 * @property integer        $id
 * @property integer        $user_id
 * @property integer        $user_notification_rule_id
 * @property string         $location
 * @property boolean        $is_viewed
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Models\Users\Notifications\Notification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Models\Users\Notifications\Notification whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\Notifications\Notification
 *         whereUserNotificationRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\Notifications\Notification
 *         whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\Notifications\Notification
 *         whereIsViewed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\Notifications\Notification
 *         whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\Notifications\Notification
 *         whereUpdatedAt($value)
 */
class Notification extends Model
{

    protected $table = 'user_notifications';

    /**
     * User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Rule
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function rule()
    {
        return $this->hasOne(Rule::class);
    }

}