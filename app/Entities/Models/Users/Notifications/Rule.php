<?php namespace App\Entities\Models\Users\Notifications;

use App\Entities\Models\Model;
use App\Entities\Models\Users\User;

/**
 * App\Entities\Models\Users\Notifications\Rule
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|User[] $users
 * @property integer                                              $id
 * @property string                                               $type
 * @property string                                               $message
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\Notifications\Rule whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\Notifications\Rule whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\Notifications\Rule whereMessage($value)
 */
class Rule extends Model
{

    protected $table = 'user_notification_rules';
    public $timestamps = false;

    /**
     * Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

}