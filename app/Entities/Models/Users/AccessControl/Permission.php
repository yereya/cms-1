<?php namespace App\Entities\Models\Users\AccessControl;

use App\Entities\Models\Model;
use DB;

/**
 * App\Entities\Models\Users\AccessControl\Permission
 *
 * @property-read Role $roles
 * @property integer   $id
 * @property string    $type
 * @property string    $name
 * @property string    $description
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Models\Users\AccessControl\Permission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Models\Users\AccessControl\Permission whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Models\Users\AccessControl\Permission whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\AccessControl\Permission
 *         whereDescription($value)
 */
class Permission extends Model
{
    use RefreshesPermissionCacheTrait;

    public $timestamps = false;

    protected $fillable = ['type', 'name', 'description', 'roles_id'];
    
    protected $table = 'user_permissions';

    /**
     * Get Roles Id Attribute
     *
     * Returns the list of IDs for assigned roles to selected permissions
     *
     * @return array
     */
    public function getRolesIdAttribute()
    {
        return $this->roles()->pluck('id');
    }

    /**
     * Roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_permission_role');
    }

    /**
     * Set Roles Id Attribute
     *
     * Syncs defined roles with selected permissions
     *
     * @param array $roles
     */
    public function setRolesIdAttribute(array $roles)
    {
        $this->roles()->sync($roles);
    }

    /**
     * Find a permission by its name.
     *
     * @param string $name
     *
     * @throws \Exception
     * @return Permission
     */
    public static function findByName($name)
    {
        $permission = static::select(DB::raw('CONCAT(name, ".", type) as name'))->having('name', '=', $name)->first();

        if (!$permission) {
            throw new \Exception('permission does not exists');
        }

        return $permission;
    }

}