<?php

namespace App\Entities\Models\Users\AccessControl;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Users\User;
use Illuminate\Database\Eloquent\Model;

class AssignedAccount extends Model
{
    protected $table = 'user_assigned_accounts';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'account_id'];

    /**
     * Users
     * many to many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users() {
        return $this->belongsToMany(User::class);
    }

    /**
     * Accounts
     * many to many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function accounts() {
        return $this->belongsToMany(Account::class);
    }
}
