<?php namespace App\Entities\Models\Users\AccessControl;

use App\Entities\Models\Model;
use App\Entities\Models\Users\User;

class SiteUserRole extends Model
{
    use RefreshesPermissionCacheTrait;

    public $timestamps = false;

    protected $fillable = ['site_id', 'user_id', 'role_id'];

    /**
     * @var string $table
     */
    protected $table = 'site_user_role';


    /**
     * Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class, 'id', 'user_id');
    }


    /**
     * Roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->hasMany(Role::class, 'id', 'role_id');
    }

}