<?php namespace App\Entities\Models\Users\AccessControl;

use App\Policies\PermissionPolicy;

trait RefreshesPermissionCacheTrait
{
    public static function bootRefreshesPermissionCache()
    {
        static::created(function ($model) {
            $model->forgetCachedPermissions();
        });

        static::updated(function ($model) {
            $model->forgetCachedPermissions();
        });

        static::deleted(function ($model) {
            $model->forgetCachedPermissions();
        });
    }

    /**
     *  Forget the cached permissions.
     */
    public function forgetCachedPermissions()
    {
        app(PermissionPolicy::class)->forgetCachedPermissions();
    }
}
