<?php

namespace App\Entities\Models\Users\AccessControl;

use App\Entities\Models\Sites\SitesModel;
use Illuminate\Database\Eloquent\Model;
use User;

class SiteUserPermission extends Model
{
    protected $table = 'site_user_permission';

    /**
     * Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Permissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions()
    {
        return $this->hasMany(Permission::class, 'id', 'permission_id');
    }
}
