<?php namespace App\Entities\Models\Users\AccessControl;

use App\Entities\Models\Model;
use App\Entities\Models\Users\User;

/**
 * App\Entities\Models\Users\AccessControl\Role
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|User[]       $users
 * @property-read \Illuminate\Database\Eloquent\Collection|Permission[] $permissions
 * @property integer                                                    $id
 * @property string                                                     $name
 * @property string                                                     $description
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\AccessControl\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\AccessControl\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Users\AccessControl\Role whereDescription($value)
 */
class Role extends Model
{
    use RefreshesPermissionCacheTrait;

    public $timestamps = false;

    protected $fillable = ['name', 'description', 'permissions_id'];

    protected $table = 'user_roles';

    /**
     * Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Permissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'user_permission_role');
    }

    /**
     * Get Permissions Id Attribute
     *
     * Returns the list of IDs for assigned permissions to selected roles
     *
     * @return array
     */
    public function getPermissionsIdAttribute()
    {
        return $this->permissions()->pluck('id');
    }

    /**
     * Set Permissions Id Attribute
     *
     * Syncs defined permissions with selected roles
     *
     * @param array $permissions
     */
    public function setPermissionsIdAttribute(array $permissions)
    {
        $this->permissions()->sync($permissions);
    }

    /**
     * returns the table name for this model
     *
     * @return string
     */
    public static function getTableName()
    {
        return with(new self)->getTable();
    }

    /**
     * Find a role by its name.
     *
     * @param string $name
     *
     * @return Role
     * @throws \Exception
     */
    public static function findByName($name)
    {
        $role = static::where('name', $name)->first();

        if (!$role) {
            throw new \Exception('role does not exists');
        }

        return $role;
    }

    /**
     * Grant the given permission to a role.
     *
     * @param  $permission
     *
     * @return HasPermissions
     */
    public function givePermissionTo($permission)
    {
        $this->permissions()->save($this->getStoredPermission($permission));

        $this->forgetCachedPermissions();

        return $this;
    }

}