<?php namespace App\Entities\Models;

use App\Entities\Models\Model;

/**
 * Class ReportKeyword
 *
 * @package App\Entities\Models
 */
class FieldsQueryParam extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'fields_query_params';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'type',
        'display_name',
        'system_id',
        'cli_command',
        'selector',
        'options_query',
        'db',
        'input_type',
        'attributes',
    ];


    /**
     * Build Cli Command - Builds the Artisan Cli command for the current field query param
     *
     * @param FieldsQueryParam $model
     * @return null
     */
    public static function buildCliCommand(FieldsQueryParam $model)
    {
        $attributes = json_decode($model->attributes['attributes'], true);

        return isset($attributes['cli_command']) ? $attributes['cli_command'] : NULL;
    }
}