<?php

namespace App\Entities\Models;

use App\Entities\Models\Model;

/**
 * Class SitesPageSpeedScore
 * @package App\Entities\Models
 */
class SitesPageSpeedScore extends Model
{
    /**
     * @var string
     */
    protected $connection = 'cms';
    /**
     * @var string
     */
    protected $table = "sites_page_speed_scores";
    /**
     * @var
     */
    protected $fillable = ['site_url', 'desktop_score', 'mobile_score', 'updated_at', 'created_at'];

}
