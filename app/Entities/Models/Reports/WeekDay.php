<?php namespace App\Entities\Models\Reports;

use App\Entities\Models\Model;

// TODO: Is this class even necessary? Looks like it should be removed

/**
 * Class WeekDay
 *
 * @package App\Entities\Models\ChangesLogs
 */
class WeekDay extends Model
{
    /**
     * Names of days of the week.
     *
     * @var array
     */
    protected $days = array(
        0 => 'Sunday',
        1 => 'Monday',
        2 => 'Tuesday',
        3 => 'Wednesday',
        4 => 'Thursday',
        5 => 'Friday',
        6 => 'Saturday',
    );

    public $timestamps = false;

    protected $table = 'reports_week_days';


    public function changesLogs()
    {
        return $this->belongsToMany('Reports\ChangesLogs\ChangesLog', 'reports_changes_days');
    }


    public static function getTableName()
    {
        return with(new self)->getTable();
    }


    public function getDayName($day_id, $string_case = null)
    {
        if (is_null($string_case))
            return $this->days[$day_id];
        else
            return strtolower($this->days[$day_id]);
    }


    public function getDayId($day_name)
    {
        foreach ($this->days as $key => $day) {
            if (strtolower($day) == strtolower($day_name)) {
                return $day;
            }
        }
    }
}