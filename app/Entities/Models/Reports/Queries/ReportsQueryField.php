<?php

namespace App\Entities\Models\Reports\Queries;

use Illuminate\Database\Eloquent\Model;

class ReportsQueryField extends Model
{
    /**
     * @var string
     */
    protected $table = 'reports_query_fields';
}
