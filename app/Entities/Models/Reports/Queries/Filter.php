<?php namespace App\Entities\Models\Reports\Queries;

use App\Entities\Models\Model;

/**
 * Class FilterUser
 */
class Filter extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['query_id', 'name', 'order'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'reports_query_filters';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function properties() {
        return $this->hasMany(FilterProperty::class, 'filter_id', 'id')->orderBy('order', 'desc');
    }
}