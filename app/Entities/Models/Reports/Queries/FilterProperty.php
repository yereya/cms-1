<?php namespace App\Entities\Models\Reports\Queries;

use App\Entities\Models\Model;

/**
 * Class FilterProperties
 */
class FilterProperty extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['filter_id', 'query_field_id', 'operator', 'value', 'order'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'reports_query_filters_properties';


    public function field() {
        return $this->belongsTo(ReportsQueryField::class, 'query_field_id', 'id');
    }
}