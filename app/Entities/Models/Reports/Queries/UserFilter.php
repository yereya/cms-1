<?php namespace App\Entities\Models\Reports\Queries;

use Illuminate\Database\Eloquent\Model;

class UserFilter extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'filter_id'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'reports_query_user_filter';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function filters() {
        return $this->hasMany(Filter::class, 'filter_id', 'id');
    }
}
