<?php

namespace App\Entities\Models\Reports;

use Illuminate\Database\Eloquent\Model;

class Query extends Model
{
    protected $table = 'reports_queries';
}
