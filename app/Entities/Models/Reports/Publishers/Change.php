<?php namespace App\Entities\Models\Reports\Publishers;

use App\Entities\Models\Model;
use App\Entities\Models\Users\User;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ChangeLog
 *
 * @package App\Entities\Models\Reports\Publishers
 */
class Change extends Model
{
    use SoftDeletes;

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'reports_publishers_changes';

    /**
     * @var array $dates
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
    protected $fillable = ['user_id', 'change_level', 'account_id', 'campaign_id', 'ad_group_id', 'keyword_id', 'comment', 'reason', 'expected_results', 'follow_up_date', 'follow_up_state', 'bid', 'bid_multiplier', 'budget', 'date_time_changed', 'time_from', 'time_until', 'mobile_bid_adjustment'];
//    protected $primaryKey = 'id';

    /**
     * @var array $states
     */
    protected $states = [
//        '0' => "&nbsp;",
        '0' => "Paused",
        '1' => "Active",
    ];

    /**
     * Account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    /**
     * Account
     * get accounts by field account_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function accountByAccountId() {
        return $this->belongsTo(Account::class, 'account_id', 'account_id');
    }

    /**
     * Keywords
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function keyword()
    {
        return $this->belongsTo(Keyword::class, null, 'keyword_id');
    }

    /**
     * Keywords
     * get keywords by field campaign_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function keywordByCampaignId() {
        return $this->belongsTo(Keyword::class, 'campaign_id', 'campaign_id');
    }

    /**
     * Days
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function days()
    {
        return $this->hasMany(ChangeDay::class);
    }

    /**
     * Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * returns the table name for this model
     *
     * TODO: why is this here? consider removing it
     *
     * @return string
     */
    public static function getTableName()
    {
        return with(new self)->getTable();
    }

    /**
     * Translates the state from the index to the
     *
     * TODO: This function can be simplified
     *
     * @param $state_idx
     * @return null
     */
    public function getState($state_idx)
    {
        $options = array_keys($this->states);
        if (is_null($state_idx) || !in_array($state_idx, $options))
            return null;
        else
            return $this->states[$state_idx];
    }

    /**
     * Translates the state from the index to the
     *
     * TODO: Remove this function and use getState instead
     *
     * @param $state_idx
     * @return null
     */
    public function scopeGetState($state_idx)
    {
        $options = array_keys($this->states);
        if (!in_array($state_idx, $options))
            return null;
        else
            return $this->states[$state_idx];
    }

    /**
     * Get States Array
     *
     * @return array
     */
    public static function getStates()
    {
        return with(new self)->states;
    }

    /**
     * Get States Array
     *
     * @return array
     */
    public static function getStateId($id)
    {
        return with(new self)->states[$id];
    }

    /**
     * Get change logs related to campaigns only
     *
     * @param $query
     * @param $account_id
     * @param null $user_id
     * @return mixed
     */
    public function scopeGetCampaigns($query, $account_id, $user_id = null)
    {
        $sql_campaign = "(SELECT campaign
            FROM ". Keyword::getTableName()." AS k
            WHERE {$this->table}.campaign_id = k.campaign_id
            LIMIT 1) AS campaign";

        $changes = $query->select($this->table .'.*', 'u.first_name', 'u.last_name', 'a.publisher', DB::raw($sql_campaign))
            ->leftJoin(Account::getTableName() .' AS a', $this->table .'.account_id', '=',  'a.account_id')
            ->leftJoin(User::getTableName() .' AS u', $this->table .'.user_id', '=',  'u.id')
            ->where($this->table .'.account_id', '=', $account_id)
            ->whereChangeLevel('campaign')
            ->get();

        $changes = $this->translateCollectionStateId2Text($changes);

        return $changes;
    }


    /**
     * Get change logs related to Ad Groups only
     *
     * @param $query
     * @param $account_id
     * @param null $user_id
     * @return mixed
     */
    public function scopeGetAdGroups($query, $account_id, $user_id = null)
    {
        $sql_ad_group = "(SELECT ad_group
            FROM ". Keyword::getTableName() ." AS k
            WHERE k.ad_group_id = {$this->table}.ad_group_id
            LIMIT 1) AS ad_group";

        $sql_campaign = "(SELECT campaign
            FROM ". Keyword::getTableName()." AS k
            WHERE {$this->table}.campaign_id = k.campaign_id
            LIMIT 1) AS campaign";

        $changes = $query->select($this->table .'.*', 'u.first_name', 'u.last_name', 'a.publisher', DB::raw($sql_ad_group), DB::raw($sql_campaign))
            ->leftJoin(Account::getTableName() .' AS a', $this->table .'.account_id', '=',  'a.account_id')
            ->leftJoin(User::getTableName() .' AS u', $this->table .'.user_id', '=',  'u.id')
            ->where($this->table .'.account_id', '=', $account_id)
            ->whereChangeLevel('ad_group')
            ->get();
        $changes = $this->translateCollectionStateId2Text($changes);

        return $changes;
    }


    /**
     * Get change logs related to keywords only
     *
     * @param $query
     * @param $account_id
     * @param null $user_id
     * @return mixed
     */
    public function scopeGetKeywords($query, $account_id, $user_id = null)
    {
        $changes = $query->select($this->table .'.*', 'k.keyword', 'u.first_name', 'u.last_name', 'a.publisher', 'k.campaign')
            ->leftJoin(Account::getTableName() .' AS a', $this->table .'.account_id', '=',  'a.account_id')
            ->leftJoin(Keyword::getTableName() .' AS k', $this->table .'.keyword_id', '=',  'k.keyword_id')
            ->leftJoin(User::getTableName() .' AS u', $this->table .'.user_id', '=',  'u.id')
            ->where($this->table .'.account_id', '=', $account_id)
            ->whereChangeLevel('keyword')
            ->get();
        $changes = $this->translateCollectionStateId2Text($changes);

        return $changes;
    }


    /**
     * Get change logs related to keywords only
     *
     * TODO: Change this function to return Query Builder instead of collection
     *
     * @param $query
     * @param $account_id
     * @param null $user_id
     * @return mixed
     */
    public function scopeGetScheduling($query, $account_id, $user_id = null)
    {
        $changes = $query->select($this->table .'.*', 'u.first_name', 'u.last_name', 'a.publisher', 'k.campaign')
            ->leftJoin(Keyword::getTableName() .' AS k', $this->table .'.campaign_id', '=',  'k.campaign_id')
            ->leftJoin(Account::getTableName() .' AS a', $this->table .'.account_id', '=',  'a.account_id')
            ->leftJoin(User::getTableName() .' AS u', $this->table .'.user_id', '=',  'u.id')
            ->where($this->table .'.account_id', '=', $account_id)
            ->whereChangeLevel('scheduling')
            ->get();

        $changes = $this->translateCollectionStateId2Text($changes);
        $changes = $this->getSchedulingDays($changes);

        return $changes;
    }


    public function getSchedulingDays($changes)
    {
        $change_ids = [];
        foreach ($changes as $item) {
            $change_ids[] = $item->id;
        }

        $days = ChangeDay::whereIn('change_id', $change_ids)->get();

        //Turns the value of id to be the key in the assoc array
        $changes = $changes->keyBy('id');

        //Add the days as an array to the changes collection
        foreach ($days as $day) {
            $changes[$day->change_id]->days_list .= empty($changes[$day->change_id]->days_list) ? $day->day : ','. $day->day;
        }

        return $changes;
    }


    /**
     * Translates the state to it's value for collections
     *
     * @param $collection
     * @param string $key
     * @return mixed
     */
    private function translateCollectionStateId2Text($collection, $key = 'state')
    {
        foreach ($collection as $item) {
            $item->$key = $this->getState($item->$key);
        }

        return $collection;
    }
}