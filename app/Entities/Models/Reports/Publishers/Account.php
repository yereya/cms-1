<?php namespace App\Entities\Models\Reports\Publishers;

use App\Entities\Models\Model;

/**
 * Class Account
 *
 * @package App\Entities\Models\Reports\Publishers
 */
class Account extends Model
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'reports_publishers_accounts';

    /**
     * @var array $fillable
     */
    protected $fillable = ['publisher', 'type', 'account_name', 'account_id'];

    protected $account_types = [
        ['id' => 0, 'text' => 'Display'],
        ['id' => 1, 'text' => 'Search'],
        ['id' => 2, 'text' => 'Mcc'],
        ['id' => 3, 'text' => 'Inactive']
    ];

    /**
     * Advertisers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function advertisers()
    {
        return $this->belongsTo(Advertiser::class, 'advertiser_id');
    }


    /**
     * Get Table Name
     *
     * TODO: Why this is here? consider removing this. if it's necesery maybe this should be moved to Model (parent)
     *
     * @return string
     */
    public static function getTableName()
    {
        return with(new self)->getTable();
    }


    public static function getAccountTypes()
    {
        return with(new self)->account_types;
    }


    public static function getAccountTypeName($id)
    {
        return with(new self)->account_types[$id]['text'];
    }
}