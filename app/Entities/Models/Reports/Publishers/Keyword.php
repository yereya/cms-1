<?php namespace App\Entities\Models\Reports\Publishers;

use App\Entities\Models\Model;
use DB;

/**
 * Class ReportKeyword
 *
 * @package App\Entities\Models\Reports
 */
class Keyword extends Model
{

//    public $connection = 'mrr';

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'reports_publishers_keywords';

    /**
     * @var array $fillable
     *
     * TODO: This list is probably not updated
     */
    protected $fillable = [
        'keyword_id',
        'keyword',
        'campaign_id',
        'campaign',
        'ad_group_id',
        'ad_group',
        'match_type',
        'time_zone',
        'cost',
        'account_id',
        'currency',
        'keyword_state',
        'campaign_state'
    ];

    /**
     * Get Table Name
     *
     * TODO: This function again? consider removing it
     *
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new self)->getTable();
    }


    /**
     * Scope Get Ad Group
     *
     * TODO: a scope should not return a collection, but a Query Builder instance
     *
     * @return array|static[]
     */
    public function scopeGetAdGroup()
    {
        return DB::table($this->table)
            ->select('ad_group_id as id', 'ad_group as text')
            ->groupBy('ad_group_id')
            ->get();
    }


    /**
     * Scope Search Keyword
     *
     * @param $query
     * @param $term
     * @param $params
     *
     * @return array|static[]
     */
    public function scopeSearchSelect2Formatted($query, $term, $params = [])
    {
        $query = DB::table($this->table)
            ->select('keyword_id as id', DB::raw('CONCAT(keyword, " [", match_type, "]") as text'))
            ->where('keyword', 'like', '%' . $term . '%');

        if (isset($params['account_id']))
            $query->where('account_id', '=', $params['account_id']);

        if (isset($params['campaign_id']))
            $query->where('campaign_id', '=', $params['campaign_id']);

        return $query;
    }
}