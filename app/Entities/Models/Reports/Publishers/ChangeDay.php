<?php namespace App\Entities\Models\Reports\Publishers;

use App\Entities\Models\Model;

/**
 * Class ChangeDay
 *
 * @package App\Entities\Models\ChangesLogs
 */
class ChangeDay extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'reports_publishers_change_days';

    public $timestamps = false;


    public static function getTableName()
    {
        return with(new self)->getTable();
    }

}