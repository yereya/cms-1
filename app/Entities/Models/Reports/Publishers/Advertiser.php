<?php namespace App\Entities\Models\Reports\Publishers;

use App\Entities\Models\Model;

/**
 * Class Advertisers
 *
 * @package App\Entities\Models\Reports\Publishers
 */
class Advertiser extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    protected $connection = 'bo';
    /**
     * @var string $table
     */
    protected $table = 'advertisers';

    /**
     * @var array $fillable
     *
     * TODO: This list is probably not updated
     */
    protected $fillable = [
        'id',
        'name',
        'status',
        'type',
        'timezone',
    ];
}