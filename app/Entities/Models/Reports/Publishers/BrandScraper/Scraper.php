<?php

namespace App\Entities\Models\Reports\Publishers\BrandScraper;

use App\Entities\Models\Reports\Publishers\Advertiser;
use App\Entities\Models\Sites\Site;
use Illuminate\Database\Eloquent\Model;

class Scraper extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'reports_brand_scrapers';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'advertiser_id',
        'scraper_name',
        'repeat_delay',
        'currency',
        'username',
        'password',
        'active',
        'days_ago',
        'run_time_start',
        'run_time_stop',
        'trxid',
        'ioid',
        'amount',
        'commission_amount',
        'save_to_adserver',
        'save_to_new_adserver'
    ];

    /**
     * @var array $currency_type
     */
    protected $currency_type = [
        ['id' => 0, 'text' => 'USD'],
        ['id' => 1, 'text' => 'EUR'],
        ['id' => 2, 'text' => 'GBP']
    ];

    /**
     * Advertisers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function advertisers()
    {
        return $this->belongsTo(Advertiser::class, 'advertiser_id', 'mongodb_id');
    }

    /**
     * Fetchers
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function fetchers() {
        return $this->hasMany(ScraperFetcher::class, 'scraper_id', 'id')->with('fetcherProperties');
    }

    /**
     * Advanced Properties
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function advancedProperties() {
        return $this->hasMany(ScraperAdvanceProperties::class, 'scraper_id', 'id');
    }

    /**
     * Processors
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function processors() {
        return $this->hasMany(ScraperProcessor::class, 'scraper_id', 'id');
    }
}
