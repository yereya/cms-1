<?php

namespace App\Entities\Models\Reports\Publishers\BrandScraper;

use Illuminate\Database\Eloquent\Model;

class ScraperFetcher extends Model
{

    /**
     * constant to type tables properties
     */
    const SCRAPER_PROPERTIES_TYPE_HEADER = 2;
    const SCRAPER_PROPERTIES_TYPE_FORM_PARAMS = 1;
    const SCRAPER_PROPERTIES_TYPE_TRANSLATOR = 3;
    const SCRAPER_PROPERTIES_TYPE_QUERY = 4;
    const SCRAPER_PROPERTIES_TYPE_SPREADSHEET_INFO = 5;

    /**
     * @var string $table
     */
    protected $table = 'reports_brand_scrapers_fetchers';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'scraper_id',
        'url',
        'method',
        'active',
        'fetcher_type',
        'fetcher_sleep',
        'priority',
        'concat_n_days_results',
        'use_credentials_as_auth',
        'protocol_1_0',
        'ssl_certificate_verification',
        'disable_redirects',
        'disable_http_errors',
        'type',
        'mail_query',
        'mail_query_type'
    ];

    /**
     * @var array $currency_type
     */
    protected $method_type = [
        ['id' => 0, 'text' => 'GET'],
        ['id' => 1, 'text' => 'POST'],
        ['id' => 2, 'text' => 'HEAD'],
        ['id' => 2, 'text' => 'PUT'],
        ['id' => 2, 'text' => 'DELETE'],
        ['id' => 2, 'text' => 'OPTIONS'],
        ['id' => 2, 'text' => 'CONNECT']
    ];

    public static $translators = [
        ['ONE' => 'ONE', 'TWO' => 'TWO', 'THREE' => 'THREE']
    ];

    /**
     * Scraper
     * one to many
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function scrapers()
    {
        return $this->belongsTo(Scraper::class, 'scraper_id');
    }

    /**
     * ScraperFetcherProperties
     * many to one
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function fetcherProperties() {
        return $this->hasMany(ScraperFetcherProperties::class, 'fetcher_id', 'id');
    }

    /**
     * Builds and returns an array of all available property types
     *
     * @return array
     */
    public static function getPropertyTypes() {
        return [
            'header' => self::SCRAPER_PROPERTIES_TYPE_HEADER,
            'body' => self::SCRAPER_PROPERTIES_TYPE_FORM_PARAMS,
            'translator' => self::SCRAPER_PROPERTIES_TYPE_TRANSLATOR,
            'query' => self::SCRAPER_PROPERTIES_TYPE_QUERY,
            'spreadsheet_info' => self::SCRAPER_PROPERTIES_TYPE_SPREADSHEET_INFO
        ];
    }

    /**
     * Returns the id of the propery name that is passed
     *
     * @param $name
     * @return null
     */
    public static function getPropertyType($name) {
        $types = self::getPropertyTypes();

        return (isset($types[$name])) ? $types[$name] : null;
    }

}
