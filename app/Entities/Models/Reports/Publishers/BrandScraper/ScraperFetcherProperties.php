<?php

namespace App\Entities\Models\Reports\Publishers\BrandScraper;

use Illuminate\Database\Eloquent\Model;

class ScraperFetcherProperties extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'reports_brand_fetchers_properties';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'fetcher_id',
        'key',
        'value',
        'type_field'
    ];

    /**
     * ScraperFetcher
     * one to many
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function scrapers()
    {
        return $this->belongsTo(ScraperFetcher::class, 'fetcher_id');
    }
}
