<?php

namespace App\Entities\Models\Reports\Publishers\BrandScraper;

use Illuminate\Database\Eloquent\Model;

class ScraperProcessor extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'reports_brand_scraper_processor';

    protected $fillable = [
        'scraper_id',
        'date',
        'token',
        'event',
        'trx_id',
        'trx_id',
        'io_id',
        'commission_amount',
        'amount',
        'currency',
        'rule',
        'active'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Scraper
     * one to many
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function scrapers()
    {
        return $this->belongsTo(Scraper::class, 'scraper_id');
    }
}
