<?php

namespace App\Entities\Models\Reports\Publishers\BrandScraper;

use Illuminate\Database\Eloquent\Model;

class ScraperAdvanceProperties extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'reports_brand_advance_properties';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'scraper_id',
        'key',
        'value',
        'type_field'
    ];

    /**
     * ScraperFetcher
     * one to many
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function scrapers()
    {
        return $this->belongsTo(ScraperFetcher::class, 'fetcher_id');
    }
}
