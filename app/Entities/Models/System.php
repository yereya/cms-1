<?php namespace App\Entities\Models;

use App\Entities\Models\Model;

/**
 * Class ReportKeyword
 * @package App\Entities\Models
 */
class System extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'systems';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'controller',
        'namespace',
        'route',
        'menu_icon',
        'menu_item_order',
        'permission_name',
        'show_in_side_menu',
        'parent_id',
    ];

    public function parent()
    {
        return $this->hasOne(System::class, 'id', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(System::class, 'parent_id', 'id');
    }

    public static function navigationTree()
    {
        return static::with(['children' => function ($query) {
            $query->where('show_in_side_menu', 1)
                ->where('show_in_side_menu', 1)
                ->orderBy('menu_item_order', 'asc');

            $query->with(['children' => function ($query) {
                $query->where('show_in_side_menu', 1)
                    ->where('show_in_side_menu', 1)
                    ->orderBy('menu_item_order', 'asc');
            }]);
        }])
            ->where('parent_id', '')
            ->where('show_in_side_menu', 1)
            ->orderBy('parent_id', 'asc')
            ->orderBy('menu_item_order', 'asc')
            ->get();
    }


}