<?php namespace App\Entities\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

/**
 * Class Model
 *
 * @package App\Entities\Models
 */
class Model extends BaseModel
{
    /**
     * The "booting" method of the model.
     */
    protected static function boot()
    {
        parent::boot();

        // Makes null all the attributes that should be null when they are empty. For example nullable foreign keys
        static::saving(function ($model) {
            if(property_exists($model, 'nullable')) {
                foreach ((array) $model->nullable as $attribute) {
                    $model->$attribute = empty($model->$attribute) ? null : $model->$attribute;
                }
            }

            return true;
        });
    }

    /**
     * Returns the table name
     *
     * @return mixed
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }
}