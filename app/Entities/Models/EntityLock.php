<?php

namespace App\Entities\Models;

use App\Entities\Models\Users\User;

class EntityLock extends Model
{
    /**
     * user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
