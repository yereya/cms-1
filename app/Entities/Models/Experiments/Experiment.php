<?php namespace App\Entities\Models\Experiments;

use App\Entities\Models\Model;
use App\Entities\Models\System;
use DB;

/**
 * Class Experiment
 *
 * @package App\Entities\Models\Experiments
 */
class Experiment extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'experiments';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'system_id',
    ];


    /**
     * Systems
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function systems() {
        return $this->belongsTo(System::class, 'system_id', 'id');
    }


    public function tasks()
    {
        return $this->belongsToMany(Tasks::class, 'experiment_triggers', null, 'task_id', 'task_id')->withPivot('minutes_offset');
    }
}