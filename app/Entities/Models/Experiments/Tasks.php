<?php namespace App\Entities\Models\Experiments;

use App\Entities\Models\Model;
use App\Entities\Models\System;
use DB;

/**
 * Class Holstered Query
 *
 * @package App\Entities\Models\Reports
 */
class Tasks extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'experiment_tasks';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'system_id',
        'system_task_id',
        'params',
        'command',
    ];


    /**
     * Systems
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function systems() {
        return $this->belongsTo(System::class, 'system_id', 'id');
    }


    /**
     * Systems
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function experiments() {
        return $this->belongsToMany(Experiment::class, 'experiment_id', 'id');
    }
}