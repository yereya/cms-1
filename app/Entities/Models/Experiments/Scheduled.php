<?php namespace App\Entities\Models\Experiments;

use App\Entities\Models\Model;
use App\Entities\Models\System;
use DB;

/**
 * Class Holstered Query
 *
 * @package App\Entities\Models\Reports
 */
class Scheduled extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'experiment_scheduled';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'system_id',
        'task_id',
        'params',
        'run_time',
    ];


    /**
     * Systems
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function systems() {
        return $this->belongsTo(System::class, 'system_id', 'id');
    }


    /**
     * Systems
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tasks() {
        return $this->belongsTo(Tasks::class, 'tasks_id', 'id');
    }
}