<?php namespace App\Entities\Models\Experiments;

use App\Entities\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Monitor Model
 *
 * @package App\Entities\Models\Experiments
 */
class Monitor extends Model
{

    use SoftDeletes;

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'experiment_monitors';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name', 'department', 'category', 'target', 'date_range', 'segmentation_level', 'calculate_kpi', 'table',
        'data_calculation', 'filter', 'aggregation_type', 'unique_key', 'sending_monitor', 'run_time',
        'advertiser', 'recommendation', 'new_advertiser', 'definition', 'report_column',
        'comment', 'problem', 'query_id'
    ];

}