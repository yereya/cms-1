<?php namespace App\Entities\Models\Experiments;

use App\Entities\Models\Model;
use App\Entities\Models\System;
use DB;

/**
 * Class Holstered Query
 *
 * @package App\Entities\Models\Reports
 */
class Trigger extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'experiment_triggers';


    public function triggerable()
    {
        return $this->morphTo();
    }
}