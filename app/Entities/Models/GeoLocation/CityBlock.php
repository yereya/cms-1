<?php namespace App\Entities\Models\GeoLocation;

use App\Entities\Models\Model;

class CityBlock extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'geo_location_city_blocks';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'network',
        'network_base',
        'network_subnet',
        'range_start',
        'range_end',
        'geoname_id',
        'registered_country_geoname_id',
        'represented_country_geoname_id',
        'is_anonymous_proxy',
        'is_satellite_provider',
        'postal_code',
        'latitude',
        'longitude',
        'accuracy_radius'
    ];

    /**
     * City
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'geoname_id');
    }
}