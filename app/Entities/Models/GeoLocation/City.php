<?php namespace App\Entities\Models\GeoLocation;

use App\Entities\Models\Model;

class City extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'geo_location_cities';

    /**
     * @var string $primaryKey
     */
    protected $primaryKey = 'geoname_id';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'geoname_id',
        'locale_code',
        'continent_code',
        'continent_name',
        'country_iso_code',
        'country_name',
        'subdivision_1_iso_code',
        'subdivision_1_name',
        'subdivision_2_iso_code',
        'subdivision_2_name',
        'city_name',
        'metro_code',
        'time_zone'
    ];
}