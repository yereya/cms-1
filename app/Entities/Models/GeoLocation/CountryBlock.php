<?php namespace App\Entities\Models\GeoLocation;

use App\Entities\Models\Model;

class CountryBlock extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'geo_location_country_blocks';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'network',
        'network_base',
        'network_subnet',
        'range_start',
        'range_end',
        'geoname_id',
        'registered_country_geoname_id',
        'represented_country_geoname_id',
        'is_anonymous_proxy',
        'is_satellite_provider'
    ];

    /**
     * Country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'geoname_id');
    }
}