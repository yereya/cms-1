<?php namespace App\Entities\Models\GeoLocation;

use App\Entities\Models\Model;

class Country extends Model
{
    /**
     * @var string $table
     */
    protected $table = 'geo_location_countries';

    /**
     * @var string $primaryKey
     */
    protected $primaryKey = 'geoname_id';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'geoname_id',
        'locale_code',
        'continent_code',
        'continent_name',
        'country_iso_code',
        'country_name'
    ];
}