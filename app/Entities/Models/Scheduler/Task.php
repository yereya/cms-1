<?php namespace App\Entities\Models\Scheduler;

use App\Entities\Models\FieldsQueryParam;
use App\Entities\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Entities\Models\Scheduler\Task
 *
 */
class Task extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $table = 'scheduler_tasks';
    protected $fillable = [
        'field_query_params_id',
        'task_id',
        'task_name',
        'cli_custom_command',
        'cli_command_params',
        'minutes_from',
        'minutes_until',
        'hours_from',
        'hours_until',
        'days_from',
        'days_until',
        'day_of_week',
        'repeat_delay',
        'run_time_start',
        'run_time_stop',
        'active',
        'log_id',
        'description',


    ];

    public function fieldsQueryParams() {
        return $this->belongsTo(FieldsQueryParam::class, 'field_query_params_id', 'id')->select(['id', 'display_name']);
    }

}

