<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Bo\Changes\Type;
use App\Entities\Models\Model;

class Log extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'changes_log';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'advertiser_id',
        'publisher_id',
        'account_id',
        'campaign_id',
        'ad_group',
        'match_type',
        'keyword_id',
        'device',
        'bid',
        'status',
        'value_old',
        'value_new',
        'comment',
        'reason',
        'expected_results',
        'follow_up_date',
        'follow_up_state',
        'change_type_id',
        'initiator_user_id',
    ];

    /**
     * Types
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(Type::class, 'change_type_id', 'id');
    }

}