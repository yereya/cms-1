<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class AdGroup extends Model
{
    protected $connection = 'bo';
    protected $table = 'account_campaign_ad_groups';
    protected $fillable = [
        'publisher_id', 'account_id', 'campaign_id', 'ad_group_source_id', 'ad_group_name', 'status'
    ];
}
