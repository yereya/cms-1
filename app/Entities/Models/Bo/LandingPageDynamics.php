<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LandingPageDynamics extends Model
{
    /**
     * @var string - connect to db
     */
    protected $connection = 'bo';

    /**
     * @var string - connect to table
     */
    protected $table = 'out_landingpages_values';

    /**
     * @var array - fields
     */
    protected $fillable = [
        'landingpage_id', 'key', 'value'
    ];
}
