<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignLps extends Model
{
    use SoftDeletes;

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'out_landingpages';

    protected $fillable = [
        'name', 'mongodb_id', 'url', 'token', 'token_name', 'default', 'weight', 'campId_mogodb_id'
    ];

    protected $dates = ['deleted_at'];

    protected $softDelete = true;
}
