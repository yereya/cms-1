<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class ChangesLog extends Model
{
    protected $connection = 'bo';
    
    protected $table = 'changes_scraper';

    protected $fillable = [
        'publisher_id', 'publisher_name', 'account_id', 'account_name', 'keyword_id',
        'keyword', 'campaign_id', 'campaign_name', 'ad_group_id', 'ad_group_name',
        'match_type', 'time_zone', 'cost', 'currency', 'max_cpc', 'keyword_state',
        'campaign_state', 'ad_group_state', 'mobile_adjustment_bid'
    ];
}
