<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;

class Report extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string $table
     */
    protected $table = 'reports';

    public function accountReport() {
        return $this->hasMany(AccountReport::class);
    }
}