<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class ReportFieldSource extends Model
{
    /**
     * @var bool
     */
    public $timestamps = false;
    
    /**
     * @var string
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'report_field_sources';

    /**
     * @var array
     */
    protected $fillable = ['report_id', 'publisher_id', 'source_name', 'status'];
}
