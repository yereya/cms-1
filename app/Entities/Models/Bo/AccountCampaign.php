<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class AccountCampaign extends Model
{
    protected $connection = 'bo';

    protected $table = 'account_campaigns';

    protected $fillable = ['account_id', 'campaign_id', 'campaign_name', 'status', 'source_account_id', 'start_date'];

    /**
     * Accounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
