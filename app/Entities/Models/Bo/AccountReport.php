<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;

class AccountReport extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string $table
     */
    protected $table = 'account_reports';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['account_id', 'report_id'];

    /**
     * Account
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account() {
        return $this->belongsTo(Account::class, 'account_id', 'id');
    }

    /**
     * Reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function report()
    {
        return $this->belongsTo(Report::class);
    }
}