<?php


namespace App\Entities\Models\Bo;


use App\Entities\Models\Model;

class BudgetManagerAlert extends Model
{
    protected $connection = 'alert';
    protected $table = 'budget_controller';

    /**
     * @var array $fillable
     */
    protected $fillable = ['id', 'budget_name', 'brand_group_id', 'brand_group_name', 'company_id', 'company_name', 'advertiser_id',
        'advertiser_name', 'landingpage', 'LP_rule', 'landingpage_name', 'page', 'page_rule', 'country', 'country_rule',
        'device', 'device_rule', 'event', 'event_amount', 'budget_base_amount', 'currency', 'from_date', 'to_date', 'restrict', 'created_at'];


}

