<?php namespace App\Entities\Models\Bo;

use AccountAnalytics;
use App\Entities\Models\Bo\Publishers\Publisher;
use App\Entities\Models\Model;
use App\Entities\Models\Users\User;
use App\Entities\Repositories\Users\UserRepo;

class Account extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'accounts';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'advertiser_id', 'publisher_id', 'name', 'source_account_id',
        'source_id', 'status', 'lead', 'call', 'canceled_sale','canceled_lead','adjustment',
        'sale', 'click_out', 'install', 'type', 'url', 'is_media', 'chanel', 'currency',
        'mcc', 'time_zone', 'target_roi', 'days_to_reduce', 'time_zone_location',
        'sale_group', 'product_name'
    ];

    /**
     * Advertiser
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advertiser()
    {
        return $this->belongsTo(Advertiser::class);
    }

    /**
     * Publisher
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }

    /**
     * Campaigns
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function campaigns()
    {
        return $this->hasMany(AccountCampaign::class);
    }

    /**
     * Reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reports()
    {
        return $this->hasMany(AccountReport::class);
    }

    public function fields() {
        return $this->hasMany(AccountReportField::class, 'account_id', 'id');
    }

    public function reportsA()
    {
        $account_reports = $this->belongsTo(AccountReport::class, 'id', NULL, 'account_reports');

        return $account_reports->getResults()->belongsTo(Report::class, 'id', NULL, 'reports');
    }

    /**
     * Report Fields
     *
     * @param null $report_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function reportFields($report_id = null)
    {
        $query = $this->hasManyThrough(AccountReportField::class, AccountReport::class);

        if($report_id) {
            $query->where('account_reports.report_id', $report_id);
        }

        return $query;
    }

    /**
     * Block Queries
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function blockQueries()
    {
        return $this->hasMany(BlockQuery::class);
    }

    /**
     * Blocked Ips
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function blockedIps()
    {
        return $this->hasManyThrough(BlockedIp::class, BlockQuery::class);
    }

    /**
     * Users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(
            User::class,
            "user_assigned_accounts",
            "account_id",
            "user_id");
    }

    public function scopeNotNullSaleGroup($query){

        return $query->where('sale_group','!=','null');
    }

    /**
     * Active filter
     * @param $query
     */
    public function scopeActive($query){
//        $query->whereRaw('`status`="active"');
    }

    /**
     * Retrieves the corresponding analytics account
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function analyticsAccount()
    {
        return $this->hasOne(
            AccountAnalytics::class,
            'source_account_id'
        );
    }
}
