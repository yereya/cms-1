<?php


namespace App\Entities\Models\Bo;


use App\Entities\Models\Model;

class BayesianData extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = "bayesian_results";

    protected $fillable = [

    ];

}
