<?php

namespace App\Entities\Models\Bo\Publishers;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    /**
     * @var string - database name
     */
    protected $connection = 'bo';

    /**
     * @var string - table name
     */
    protected $table = 'out_medias';

    /**
     * @var array
     */
    protected $fillable = ['media_name', 'mongodb_id', 'type', 'status', 'publisher_id', 'url'];
}
