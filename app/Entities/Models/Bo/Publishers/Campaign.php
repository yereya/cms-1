<?php

namespace App\Entities\Models\Bo\Publishers;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * @var string - database name
     */
    protected $connection = 'bo';

    /**
     * @var string - table name
     */
    protected $table = 'out_campaigns';
}
