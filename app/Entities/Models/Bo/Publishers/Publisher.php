<?php

namespace App\Entities\Models\Bo\Publishers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
    use SoftDeletes;
    /**
     * @var string
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'publishers';

    /**
     * @var array
     */
    protected $fillable = [
        'mongodb_id', 'publisher_name', 'status', 'type'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
}
