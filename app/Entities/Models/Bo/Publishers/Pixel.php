<?php

namespace App\Entities\Models\Bo\Publishers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pixel extends Model
{
    use SoftDeletes;
    
    /**
     * @var string
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'publisher_account_pixels';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'url'
    ];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
}
