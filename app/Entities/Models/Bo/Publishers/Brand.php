<?php

namespace App\Entities\Models\Bo\Publishers;

use App\Entities\Models\Bo\Company;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    /**
     * @var string - database name
     */
    protected $connection = 'bo';

    /**
     * @var string - table name
     */
    protected $table = 'out_brands';


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
