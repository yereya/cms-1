<?php

namespace App\Entities\Models\Bo\Publishers;

use Illuminate\Database\Eloquent\Model;

class Placement extends Model
{
    /**
     * @var string
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'out_placements';

    /**
     * @var array
     */
    protected $fillable = [
        'placement_name', 'type', 'other_type', 'media_id', 'publisher_id', 'mongodb_id', 'account_id',
        'pubId_mongodb_id', 'mediaId_mongodb_id', 'campaign_id', 'campaign_mongodb_id'
    ];
}
