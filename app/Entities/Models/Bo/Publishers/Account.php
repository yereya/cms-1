<?php

namespace App\Entities\Models\Bo\Publishers;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * @var string
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'publisher_accounts';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'status', 'lead','call', 'canceled_sale', 'canceled_lead', 'sale', 'click_out', 'install', 'adjustment'
    ];
}
