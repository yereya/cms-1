<?php

namespace App\Entities\Models\Bo\Publishers;

use Illuminate\Database\Eloquent\Model;

class PublisherAccountParams extends Model
{
    protected $connection = 'bo';
    
    protected $table = 'publisher_account_params';

    public $timestamps = false;
    
    protected $fillable = [
        'publisher_account_id', 'account_id', 'source_account_id'
    ];
}
