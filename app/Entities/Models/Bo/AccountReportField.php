<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AccountReportField extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string $table
     */
    protected $table = 'account_report_fields';

    protected $fillable = ['account_id', 'report_id', 'field', 'source_field', 'operator', 'value_type', 'value'];

    static $operators = [
        'addition' => 'addition',
        'substraction' => 'substraction',
        'multiplication' => 'multiplication',
        'division' => 'division'
    ];

    static $value_types = [
        'value' => 'value',
        'field' => 'field'
    ];

    /**
     * Return field name
     *
     * @return array
     */
    final public static function getFieldName() {
        return [
            'network_net_revenue' => 'network_net_revenue'
        ];
    }

    /**
     * Return source field name
     *
     * @return array
     */
    final public static function getSourceField() {
        return [
            'cost' => 'cost'
        ];
    }

    /**
     * Report
     *
     * @return BelongsTo
     */
    public function report()
    {
        $account_reports = $this->belongsTo(AccountReport::class, 'report_id', null, 'account_reports');
        if($account_report = $account_reports->first()) {
            return $account_report->belongsTo(Report::class);
        }

        return $account_reports;
    }
}