<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class AdvertisersBrands extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'out_brands';

    protected $fillable = [
        'name', 'price_model', 'advertiser_id', 'budget_type', 'status', 'budget', 'budget_unlimited',
        'daily_cap', 'daily_cap_unlimited', 'date_to', 'date_from', 'mongodb_id'
    ];
}
