<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;

class BlockQuery extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'account_block_queries';

    /**
     * @var array $fillable
     */
    protected $fillable = ['account_id', 'type', 'name', 'query', 'status'];

    /**
     * @var array $types
     */
    public static $types = [
        'ip block',
        'ip release',
        'display block',
        'display release'
    ];

    /**
     * Touch Last Run
     *
     * @return $this
     */
    public function touchLastRun()
    {
        $this->last_run = $this->freshTimestamp();
        $this->timestamps = false;
        $this->save();

        return $this;
    }

    /**
     * Account
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo(Account::class);
    }

}