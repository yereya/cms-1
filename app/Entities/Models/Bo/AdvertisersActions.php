<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class AdvertisersActions extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    protected $fillable = ['advertiser_id', 'name', 'status', 'unique'];

    /**
     * @var string
     */
    protected $table = 'out_actions';

    /**
     * @var array
     */
    public static $parameters = ['currency', 'amount', 'commission_amount', 'trxId', 'brandId'];

    /**
     * @var array
     */
    public static $types = ['install', 'lead', 'sale'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function advertisersActionParams() {
        return $this->hasMany(AdvertisersActionParams::class, 'action_id', 'id');
    }
}
