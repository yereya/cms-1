<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class Mcc extends Model
{
    protected $table = 'mcc_config';

    protected $connection = 'bo';
    
    public $timestamps = false;
}
