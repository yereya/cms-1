<?php


namespace App\Entities\Models\Bo;


use App\Entities\Models\Model;

class Companies extends Model
{

    protected $connection = 'bo';

    protected $table = 'companies';

    /**
     * @var array
     */
    protected $fillable = ['name', 'product_description', 'SAP_id'];

}