<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class AdvertisersActionParams extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'out_action_params';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advertisersActions() {
        return $this->belongsTo(AdvertisersActions::class, 'action_id');
    }
}
