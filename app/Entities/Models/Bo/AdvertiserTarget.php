<?php namespace App\Entities\Models\Bo;
use App\Entities\Models\Model;
use Carbon\Carbon;

class AdvertiserTarget extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'advertisers_targets';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'advertiser_id',
        'date',
        'advertiser_name',
        'sales',
        'target_income',
        'target_cost',
        'target_profit',
        'product'
    ];


    /**
     * @param $value
     */
    public function setMonthAttribute($value)
    {
        $date = Carbon::parse($value);
        $date->day = 1;

        $this->attributes['date'] = $date->toDateString();
    }
}