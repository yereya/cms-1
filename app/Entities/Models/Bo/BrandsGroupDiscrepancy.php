<?php


namespace App\Entities\Models\Bo;


use App\Entities\Models\Model;

class BrandsGroupDiscrepancy extends Model
{
    protected $connection = 'bo';
    protected $table = 'brands_group_discrepancy';

    /**
     * @var array $fillable
     */
    protected $fillable = ['id','advertiser_id','advertiser_name','brand_group_name','brand_group_id','company_name',
                           'company_id','owner','reviewer','currency', 'date',
                           'base_commission_amount', 'owner_comment', 'reviewer_comment',
                           'status','invoice_group', 'currency_rate','business_group','diff', 'payment', 'day_of_payment','payment_terms', 'invoice_flag'];

    public static function getPossibleStatuses($form_select = false){
        $type = \DB::connection('bo')->select((\DB::raw('SHOW COLUMNS FROM brands_group_discrepancy WHERE Field = "status"')))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $values = array();
        foreach(explode(',', $matches[1]) as $value){
            $values[trim($value, "'")] = trim($value, "'");
        }

        return $form_select ? array_diff($values, array('New')) :  $values ; // Remove 'New' Status from form select box
    }
}

