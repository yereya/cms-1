<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;

class BlockedIp extends Model
{

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var array $fillable
     */
    protected $fillable = ['block_query_id', 'account_id', 'status', 'ip', 'count', 'error'];

    /**
     * Query
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function blockQuery()
    {
        return $this->belongsTo(BlockQuery::class);
    }

}