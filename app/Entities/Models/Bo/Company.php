<?php


namespace App\Entities\Models\Bo;


use App\Entities\Models\Bo\Publishers\Brand;
use App\Entities\Models\Model;

/**
 * Class Company
 *
 * @package app\Entities\Models\Bo
 */
class Company extends Model
{
    /**
     * @var string
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'company';

    /**
     * @var array
     */
    protected $fillable = ['name', 'product', 'bank', 'SAP_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function brand(){
        return $this->hasOne(Brand::class);
    }
}