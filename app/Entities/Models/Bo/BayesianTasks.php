<?php


namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;

class BayesianTasks extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = "bayesian_tasks";

    protected $fillable = [
        'TEST_NAME', 'AB_TESTING_SALE', 'AB_TESTING_LEAD', 'AB_TESTING_CTR', 'LEARN_PRIOR', 'DAYS_FOR_PRIOR',
        'AB_TESTING_RPM', 'EXPIRAMENT_TYPE', 'ADVERTISER_NAME', 'PAGE_NAME', 'DATES_TO_OMIT', 'DATES_TO_OMIT_PRIOR',
        'START_DATE', 'END_DATE', 'LINEUP_LABEL_A', 'LINEUP_LABEL_B', 'TEST_DESCRIPTION','task_status', 'RUN_ONCE'
    ];

}


