<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;

class AdvertiserAnalytics extends Model {
    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'advertiser_analytics';

    protected $fillable = [
        'advertiser_id',
        'ga_tracking_id',
        'ga_client_id'
    ];

    /**
     * Retrieves the connected Advertiser
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advertiser()
    {
        return $this->belongsTo(Advertiser::class, 'advertiser_id');
    }
}
