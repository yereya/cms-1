<?php


namespace App\Entities\Models\Bo;


use App\Entities\Models\Model;

class BrandGroup extends Model
{
    protected $connection = 'bo';
    protected $table = 'brands_groups';

}