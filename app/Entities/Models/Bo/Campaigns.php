<?php

namespace App\Entities\Models\Bo;

use Illuminate\Database\Eloquent\Model;

class Campaigns extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'out_campaigns';

    protected $fillable = [
        'name', 'status', 'brand_id', 'pricing_model', 'pricing_sum', 'pricing_currency', 'time_zone'
    ];
}
