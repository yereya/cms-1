<?php namespace App\Entities\Models\Bo;

use App\Entities\Models\Model;

class Conversion extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string
     */
    protected $table = 'conversions';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'track_id',
        'source_account_id',
        'type',
        'event',
        'price',
        'timestamp',
        'clkid',
        'error',
        'status',
        'response',
        'publisher_id'
    ];

    /**
     * Filter by Publisher Id
     *
     * @param $query
     * @param $publisher
     */
    public function scopeWherePublisherIs($query, $publisher)
    {
        $name_to_id = ['google' => 95, 'bing' => 97,'facebook' => 98, 'outbrain' => 161, 'taboola' => 162, 'googleGNG' => 163, 'bingGNG' => 164];
        $query->where('publisher_id', $name_to_id[$publisher]);
    }

    public function scopeWhereStatus($query, $idx)
    {
        $query->where('status', $idx);
    }
}
