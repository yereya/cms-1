<?php

namespace App\Entities\Models;

class ScssFile extends Model
{
    /**
     * @ver string
     */
    protected $table = 'scss_files';

    /**
     * @ver array
     */
    protected $fillable = [
        'name',
        'content',
        'priority'
    ];
    
}
