<?php namespace App\Entities\Models\Alerts;

use App\Entities\Models\Model;

/**
 * Class AlertsMonitorsLog
 *
 * @package App\Entities\Models\Alerts
 */
class AlertMonitorsLog extends Model
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'alerts_users';

    /**
     * @var array $fillable
     */
    protected $fillable = ['client_name', 'cost'];

    /**
     * AlertTypes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function alertType()
    {
        return $this->belongsTo(AlertType::class);
    }

}