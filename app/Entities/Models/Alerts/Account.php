<?php


namespace app\Entities\Models\Alerts;


use App\Entities\Models\Model;

class Account extends Model
{
    protected $connection = 'alert';
    protected $table = 'account';
}