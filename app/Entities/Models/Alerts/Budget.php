<?php


namespace App\Entities\Models\Alerts;


use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Model;

/**
 * Class Budget
 *
 * @package App\Entities\Models\Alerts
 */
class Budget extends Model
{
    /**
     * @var string
     */
    protected $connection = 'alert';

    protected $table = 'budget';

    public function advertiser(){

        return $this->belongsTo(Advertiser::class,'advertiser_name','name');
    }
}