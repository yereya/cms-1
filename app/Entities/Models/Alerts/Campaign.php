<?php


namespace App\Entities\Models\Alerts;


use App\Entities\Models\Model;

/**
 * Class Campaign
 *
 * @package App\Entities\Models\Alerts
 */
class Campaign extends Model
{
    /**
     * @var $connection string
     */
    protected $connection = 'alert';

    /**
     * @var $table string
     */
    protected $table = 'campaign';


}