<?php


namespace App\Entities\Models\Alerts;


use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Model;

/**
 * Class ChangesLog
 *
 * @package App\Entities\Models\Alerts
 */
class ChangesLog extends Model
{


    /**
     * @var string
     */
    protected $connection = 'alert';

    protected $table = 'changes_log';

    /**
     * @var array $fillable
     */
    protected $fillable = ['id', 'advertiser_name','advertiser_id','department','event','event_date','importance'];

    public function advertiser(){

        return $this->belongsTo(Advertiser::class,'advertiser_name','name');
    }
}
