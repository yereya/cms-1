<?php namespace App\Entities\Models\Alerts;

use App\Entities\Models\Model;

/**
 * Class AlertLogs
 *
 * @package App\Entities\Models\Alerts
 */
class AlertLog extends Model
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'alerts_log';

    /**
     * @var array $fillable
     */
    protected $fillable = ['title', 'desc', 'error_types'];

    protected $error_types = [
        ['id' => 0, 'text' => 'Info'],
        ['id' => 1, 'text' => 'Notice'],
        ['id' => 1, 'text' => 'Warning'],
        ['id' => 2, 'text' => 'Critical']
    ];

    /**
     * AlertTypes
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function alertType()
    {
        return $this->belongsTo(AlertType::class);
    }

    /**
     * alertsUsers
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function Users()
    {
        return $this->belongsToMany(User::class, 'alerts_users', 'alert_log_id', 'user_id')
            ->withPivot('viewed', 'reminder_date');
    }

    /**
     * Get Error Types
     *
     * @return mixed
     */
    public static function getErrorTypes()
    {
        return with(new self)->error_types;
    }

}