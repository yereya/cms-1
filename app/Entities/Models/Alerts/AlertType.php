<?php namespace App\Entities\Models\Alerts;

use App\Entities\Models\Model;
use App\Entities\Models\System;
use App\Entities\Models\Users\User;

/**
 * Class AlertTypes
 * @package App\Entities\Models\Alerts
 */
class AlertType extends Model
{

    public $timestamps = false;

    /**
     * @var array $fillable
     */
    protected $fillable = ['permission', 'icon'];

    /**
     * System
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function system()
    {
        return $this->belongsTo(System::class);
    }

    /**
     * alert logs
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function alertLogs()
    {
        return $this->hasMany(AlertLog::class);
    }

}