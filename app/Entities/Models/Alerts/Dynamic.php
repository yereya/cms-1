<?php


namespace App\Entities\Models\Alerts;


use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Dwh\DwhFactDailyStats;
use App\Entities\Models\Model;
use App\Entities\Repositories\UserActionFeedbackRepo;
use PhpParser\Builder;

/**
 * Class Dynamic
 *
 * @package App\Entities\Models\Alerts
 */
class Dynamic extends Model
{
    /**
     * @var string
     */
    protected $connection = 'alert';
    /**
     * @var string
     */
    protected $table = 'dynamic_new';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function advertisers()
    {
        return $this->hasOne(Advertiser::class, 'id', 'advertiser_id');
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active_alert', 1);
    }

    /**
     * @param Builder $query
     * @param $level
     *
     * @return mixed
     */
    public function scopeOnLevel($query, $level)
    {
        return $query->where('alert_type', $level);
    }

    /**
     *
     * @param Builder $query
     *
     * @return mixed
     */
    public function scopeWhereNotResolved($query)
    {
        $resolved_alert_ids = (new UserActionFeedbackRepo())->getResolvedAlertIds();

        if (count($resolved_alert_ids)) {
            $query->whereNotIn('dynamic_new.id', $resolved_alert_ids);
        }

        return $query;
    }
}