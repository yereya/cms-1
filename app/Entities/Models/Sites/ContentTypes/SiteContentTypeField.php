<?php namespace App\Entities\Models\Sites\ContentTypes;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\DynamicLists\DynamicList;
use App\Entities\Models\Sites\SitesModel;

/**
 * Class Site Content Type Field
 *
*@package App\Entities\Models\Sites
 */
class SiteContentTypeField extends SitesModel
{
    /**
     * @var string $table
     */
    protected $table = 'site_content_type_fields';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'type_id',
        'name',
        'display_name',
        'is_required',
        'priority',
        'type',
        'default',
        'index',
        'metadata',
        'field_group_id'
    ];

    /**
     * @var array
     */
    protected $with = ['fieldGroup'];

    /**
     * @var array
     */
    protected $casts = [
        'metadata' => 'object'
    ];

    /**
     * Site
     *
*@return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(SiteContentType::class);
    }

    /**
     * Field Group
     *
*@return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fieldGroup()
    {
        return $this->belongsTo(SiteContentTypeFieldGroup::class, 'field_group_id');
    }

    /**
     * @param $value
     */
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = toSlug($value, '_');
    }

    /**
     * Dynamic List
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function dynamicLists()
    {
        return $this->belongsToMany(DynamicList::class);
    }
}