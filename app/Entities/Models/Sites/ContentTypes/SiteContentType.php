<?php namespace App\Entities\Models\Sites\ContentTypes;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\Group;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SitesModel;
use App\Entities\Repositories\Sites\SiteContentTypeRepo;

/**
 * Class Site Content Type
 *
 * @package App\Entities\Models\Sites
 */
class SiteContentType extends SitesModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'site_content_types';

    /**
     * @var array $fillable
     */
    protected $fillable = ['site_id', 'name', 'table', 'has_likes', 'has_comments','slug', 'is_router'];

    /**
     * Get Full Table Attribute
     *
     * @return string
     */
    public function getFullTableAttribute()
    {
        $content_type_prefix = 'content_type_';
        $page_content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();

        // This is used for when the content type is not a generic content type
        // but instead it is a "page"
        if ($page_content_type_id
            && $this->attributes['id'] == $page_content_type_id) {
            $content_type_prefix = '';
        }

        return $content_type_prefix . $this->attributes['table'];
    }

    /**
     * Site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }

    /**
     * Fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fields()
    {
        return $this->hasMany(SiteContentTypeField::class, 'type_id')->orderBy('priority', 'asc');
    }

    /**
     * groups
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->belongsToMany(Group::class, 'content_type_groups', 'type_id', 'group_id');
    }

    /**
     * Posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class, 'content_type_id', 'id');
    }

    /**
     * Check If Is Page
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopePage($query){
        return $query->where('is_router',1);
    }
}