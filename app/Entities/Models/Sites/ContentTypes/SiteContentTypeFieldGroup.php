<?php namespace App\Entities\Models\Sites\ContentTypes;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\SitesModel;

/**
 * Class Site Content Type Field Group
 *
*@package App\Entities\Models\Sites
 */
class SiteContentTypeFieldGroup extends SitesModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'site_content_type_field_groups';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'site_id',
        'name',
        'content_type_id',
        'priority',
        'parent_id'
    ];

    /**
     * Field
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fields()
    {
        return $this->hasMany(SiteContentTypeField::class, 'field_group_id')->orderBy('priority', 'asc');
    }

    /**
     * Children Recursive
     * This relationship came from pages and use only by pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    /**
     * Children
     * This relationship came from pages and use only by pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function children()
    {
        return $this->hasMany(SiteContentTypeFieldGroup::class, 'parent_id');
    }

    /**
     * Parent Recursive
     * This relationship came from pages and use only by pages
     *
     * @return mixed
     */
    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    /**
     * Parent
     * This relationship came from pages and use only by pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function parent()
    {
        return $this->belongsTo(SiteContentTypeFieldGroup::class, 'parent_id');
    }
}