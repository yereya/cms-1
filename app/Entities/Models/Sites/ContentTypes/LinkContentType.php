<?php

namespace App\Entities\Models\Sites\ContentTypes;

use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\SiteModel;

class LinkContentType extends SiteModel
{
    public $timestamps = false;

    protected $table = 'link_content_type';
    protected $fillable = ['post_id', 'field_id', 'connect_to_post_id', 'priority'];

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }
}