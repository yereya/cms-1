<?php namespace App\Entities\Models\Sites;


/**
 * Class Site AB Test Rule
 *
 * @package App\Entities\Models\Sites
 */
class ABTestRule extends SiteModel
{


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ab_test_rules';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = ['key', 'value'];

    /**
     * Test
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function test()
    {
        return $this->belongsTo(ABTest::class);
    }
}