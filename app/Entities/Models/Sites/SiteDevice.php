<?php namespace App\Entities\Models\Sites;

/**
 * Class Site Device
 *
 * @package App\Entities\Models\Sites
 */
class SiteDevice extends SitesModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'site_devices';

    /**
     * @var array $fillable
     */
    protected $fillable = ['group', 'name', 'user_agent'];
}