<?php namespace App\Entities\Models\Sites;

use App\Entities\Models\Model;

/**
 * Class Site Domain
 *
 * @package App\Entities\Models\Sites
 */
class SiteNavigation extends SitesModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'site_navigation';

    /**
     * @var array $fillable
     */
    protected $fillable = ['title', 'menu_icon', 'route', 'route_params', 'permission_name', 'children_query', 'order', 'parent_id'];

    /**
     * Navigation Tree
     *
     * @return Collection
     */
    public static function navigationTree()
    {
        return static::with(['children' => function ($query) {
            $query->orderBy('order', 'asc');

            $query->with(['children' => function ($query) {
                $query->orderBy('order', 'asc');
            }]);
        }])
            ->whereNull('parent_id')
            ->orderBy('order', 'asc')
            ->get();
    }

    /**
     * Parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function parent()
    {
        return $this->hasOne(SiteNavigation::class, 'id', 'parent_id');
    }

    /**
     * Children
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function children()
    {
        return $this->hasMany(SiteNavigation::class, 'parent_id', 'id');
    }
}