<?php namespace App\Entities\Models\Sites;

use App\Entities\Models\Sites\DynamicLists\DynamicList;


/**
 * Class Site Device
 *
 * @package App\Entities\Models\Sites
 */
class Label extends SiteModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'labels';

    /**
     * @var array $fillable
     */
    protected $fillable = ['name', 'parent_id', 'icon', 'priority'];

    /**
     * Get As Tree
     * Return pages as nested tree.
     * Root pages are pages with parent_id null.
     * Page connect to a parent by parent_id.
     * Each group of pages under specific page (i.e tree node) are ordered by priority column.
     *
     * @param int $label_id
     *
     * @return Collection
     */
    public static function getAsTree($label_id = null)
    {
        return static::where('parent_id', $label_id)
            ->with('childrenRecursive')
            ->orderBy('priority', 'asc')
            ->get();
    }

    /**
     * posts
     */
    public function posts()
    {
        return $this->belongsToMany(
            Post::class,
            'post_label',
            'label_id',
            'post_id'
        );
    }


    /**
     * parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Label::class, 'parent_id');
    }

    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    /**
     * children
     *
     * @return mixed
     */
    public function children()
    {
        return $this->hasMany(Label::class, 'parent_id')
            ->orderBy('priority', 'asc');
    }

    /**
     * Media Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function media()
    {
        return $this->belongsToMany(Media::class, 'label_media');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function dynamicLists()
    {
        return $this->belongsToMany(DynamicList::class);
    }

}