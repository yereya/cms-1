<?php

namespace App\Entities\Models\Sites;


/**
 * Class Page
 *
 * @package App\Entities\Models\Sites
 */
class Like extends SiteModel
{
    public $timestamps = true;

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'post_id',
        'user_id',
        'session_id',
    ];

    /**
     * post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}

