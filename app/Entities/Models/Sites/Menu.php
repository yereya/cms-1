<?php

namespace App\Entities\Models\Sites;


class Menu extends SiteModel
{
    public $timestamps = false;

    /**
     * @var array $fillable
     */
    protected $fillable = ['name'];

    /**
     * Items
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function items()
    {
        return $this->hasMany(MenuItem::class, 'menu_id');
    }
}
