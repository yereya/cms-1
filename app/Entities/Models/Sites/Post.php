<?php

namespace App\Entities\Models\Sites;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Venturecraft\Revisionable\RevisionableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Entities\Repositories\Sites\LikesRepo;
use App\Entities\Repositories\Sites\CommentRepo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Entities\Models\Sites\DynamicLists\DynamicList;
use App\Entities\Repositories\Sites\LinkContentTypeRepo;
use App\Entities\Repositories\Sites\SiteContentTypeRepo;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;

/**
 * Class Post
 *
 * @package App\Entities\Models\Sites
 * @property int            $id
 * @property \Carbon\Carbon $deleted_at
 */
class Post extends SiteModel
{
    use SoftDeletes, RevisionableTrait;

    /**
     * @ver bool
     */
    protected $revisionEnabled = true;

    /**
     * @ver bool
     */
    protected $revisionCleanup = true;

    /**
     * @ver integer
     */
    protected $historyLimit = 500;

    /**
     * @ver array
     */
    protected $dontKeepRevisionOf = [
        'updated_at',
        'created_at',
        'deleted_at'
    ];

    /**
     * @ver array
     */
    const PUBLISHED_DISPLAY_STATUS = [
        'draft'          => 'Draft',
        'published'      => 'Published',
        'future_publish' => 'Future publish'
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    protected $fillable = [
        'slug',
        'name',
        'active',
        'section_id',
        'content_type_id',
        'parent_id',
        'priority',
        'published_at',
        'show_on_sitemap',
        'seo_post_title',
        'description',
        'keywords',
        'og_image',
        'og_title',
        'og_type',
        'og_url',
        'og_description',
        'robots_index',
        'robots_follow',
        'robots_custom',
        'permalink',
        'canonical_url',
        'published'
    ];

    /**
     * @ver array
     */
    protected $casts = [
        'active'          => 'boolean',
        'show_on_sitemap' => 'boolean',
    ];

    /**
     * Post content type relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contentType()
    {
        return $this->belongsTo(SiteContentType::class);
    }

    /**
     * Check if the current post is draft.
     *
     * @return bool
     */
    public function getIsDraftAttribute()
    {
        return empty($this->published_at);
    }

    /**
     * Get Published Attribute
     *
     * @return string
     */
    public function getPublishedStatusAttribute()
    {
        if (empty($this->published_at)) {

            return 'draft';
        }

        $published_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->published_at);
        if ($published_at->lte(Carbon::now())) {

            return 'published';
        } else {

            return 'future_publish';
        }
    }

    /**
     * Get Published Display Status Attribute
     *
     * @return string
     */
    public function getPublishedDisplayStatusAttribute()
    {
        return self::PUBLISHED_DISPLAY_STATUS[$this->published_status];
    }

    /**
     * Set Published Attribute
     *
     * @return void
     */
    public function setPublishedAttribute($value)
    {
        if ($value) {
            if (!$this->published_at) {
                $this->published_at = Carbon::now();
            }
        } else {
            $this->published_at = null;
        }
    }

    /**
     * Dynamically build the current post content type relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function content()
    {
        $local_key    = 'id';
        $foreign_key  = 'post_id';
        $table        = $this->contentType->full_table;
        $content_type = $this->resolveContentType($table);

        return new HasOne($content_type->newQuery(), $this, $table . '.' . $foreign_key, $local_key);
    }

    /**
     * Resolve the current post content type object.
     *
     * @param mixed $table
     *
     * @return ContentType
     */
    public function resolveContentType($table = false): ContentType
    {
        if (!$table) {
            $table = $this->contentType->full_table;
        }

        $content_type = new ContentType();
        $content_type->setTable($table);

        return $content_type;
    }

    /**
     * section
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Section::class);
    }

    /**
     * labels
     */
    public function labels()
    {
        return $this->belongsToMany(Label::class, 'post_label', 'post_id', 'label_id');
    }

    /**
     * Children Recursive
     * This relationship came from pages and use only by pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    /**
     * Children
     * This relationship came from pages and use only by pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function children()
    {
        return $this->hasMany(Post::class, 'parent_id')->orderBy('priority', 'asc');
    }

    /**
     * Parent Recursive
     * This relationship came from pages and use only by pages
     *
     * @return mixed
     */
    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }

    /**
     * Parent
     * This relationship came from pages and use only by pages
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Post::class, 'parent_id');
    }

    /**
     * page
     *
     * @return HasOne
     */
    public function page()
    {
        return $this->hasOne(Page::class);
    }

    /**
     * Delete Page
     */
    public function delete()
    {
        if ($this->isAPage()) {
            $page = Page::where('post_id', $this->id)->first();
            if($page){
                $page->deleteMenuItems();
            }
        }

        parent::delete();
    }

    /**
     * Is A Page
     *
     * @return bool
     */
    public function isAPage()
    {
        $page_content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();

        if ($this->content_type_id == $page_content_type_id) {

            return true;
        }

        return false;
    }

    /**
     * Set Slug Attribute
     * slug Mutator
     *
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = toSlug($value);
    }

    /**
     * Likes Count
     *
     * @return mixed
     */
    public function likesCount()
    {
        return (new LikesRepo())->getPostCount($this->id);
    }

    /**
     * Comments Count
     *
     * @return mixed
     */
    public function commentsCount()
    {
        return (new CommentRepo())->getPostCount($this->id);
    }

    /**
     * archiveDesktopTemplates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function archiveContentTypes()
    {
        return $this->hasMany(PageContentType::class, 'post_id', 'id');
    }

    /**
     * Popups
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function popups()
    {
        return $this->belongsToMany(Popup::class, 'popup_post');
    }

    /**
     * Linked Posts
     *
     * @param string $field_1
     * @param string $field_2
     *
     * @return Collection|null
     */
    public function linkedPosts($field_1 = null, $field_2 = null)
    {
        //Todo use array in function to get fields_names and change in all places
        $field_names = func_get_args();
        $fields      = SiteContentTypeField::where('type_id', $this->content_type_id)
            ->whereIn('name', $field_names)
            ->get();

        return (new LinkContentTypeRepo)->getLinkedPosts($this, $fields);
    }

    /**
     * Site Content Type
     *
     * @return HasOne
     */
    public function siteContentType()
    {
        return $this->hasOne(SiteContentType::class, 'content_type_id', 'id');
    }

    /**
     * Get Redirect Link
     *
     * @return string
     */
    public function getRedirectLink($slug = null)
    {
        $url = '/redirect/' . $this->siteContentType->slug . '/' . $this->slug;
        if ($slug) {
            $url .= '?slug=' . $slug;
        }

        return $url;
    }

    /**
     * Post Redirect Urls
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function redirectUrls()
    {
        return $this->hasMany(PostRedirectUrl::class, 'post_id', 'id');
    }

    /**
     * Dynamic List
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function dynamicLists()
    {
        return $this->belongsToMany(DynamicList::class);
    }

    /**
     * Get Display Name from Content Type Field
     *
     * @param string $field_name
     *
     * @return string field display name
     */
    public function getFieldDisplayName(string $field_name)
    {
        return $this->contentType->fields()
            ->where('name', $field_name)
            ->pluck('display_name')
            ->first();
    }

    /**
     * Local Scope published posts
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopePublished($query)
    {
        return $query->whereNotNull('published_at')
            ->whereDate('published_at', '<=', Carbon::now());

        if (!$allow_drafts) {
            return $query;
        }

        return $query->orWhereNull('published_at');
    }

    /**
     * Local Scope published posts
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeNotDeleted($query)
    {
        return $query->whereNull('deleted_at');
    }

    /**
     * Check label existence by label name
     *
     * @param $label_name
     *
     * @return bool
     */
    public function hasLabel(string $label_name)
    {

        return !empty($this->labels
            ->where('name', $label_name)
            ->first());
    }


    /**
     * Rating
     *
     * @return HasOne
     */
    public function rating()
    {
        return $this->hasOne(
            PostRating::class,
            'post_id',
            'id'
        );
    }

    /**
     * Get Average Attribute
     */
    public function getAverageRateAttribute()
    {
        $post_rating = $this->rating;

        if (!empty($post_rating)) {
            $rates  = [];
            $voters = 0;
            for ($i = 1; $i < 6; $i++) {
                $rate    = 'rate_' . $i;
                $rates[] = $post_rating->{$rate} * $i;
                $voters  += $post_rating->{$rate};
            }
            $sum_votes = array_sum($rates);

            return $sum_votes / $voters;
        }

        return [];
    }

    /**
     * Likes
     *
     * @return HasOne
     */
    public function likes()
    {
        return $this->hasOne(
            PostLike::class,
            'post_id',
            'id'
        );
    }


    /**
     * Get Sum of likes
     *
     * @return integer
     */
    public function getLikesSumAttribute()
    {
        $model = $this->likes;
        if (!empty($model)) {

            return $model->like;
        }

        return 0;
    }
}