<?php namespace App\Entities\Models\Sites;

use File;
use App\Entities\Models\ScssFile;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Users\AccessControl\SiteUserPermission;
use App\Entities\Models\Users\AccessControl\SiteUserRole;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Entities\Models\Sites\Site
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Domain[] $domains
 * @property integer                                                $id
 * @property string                                                 $name
 * @property boolean                                                $is_public
 * @property string                                                 $db_host
 * @property string                                                 $db_user
 * @property string                                                 $db_pass
 * @property string                                                 $db_name
 * @property \Carbon\Carbon                                         $created_at
 * @property \Carbon\Carbon                                         $updated_at
 * @property \Carbon\Carbon                                         $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereIsPublic($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereDbHost($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereDbUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereDbPass($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereDbName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Site whereDeletedAt($value)
 */
class Site extends SitesModel
{
    use SoftDeletes;

    /**
     * @var array $dates
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array $fillable
     */
    protected $fillable = ['name', 'is_public', 'db_host', 'db_user', 'db_pass', 'db_name', 'publisher_id'];

    /**
     * @var array $hidden
     */
    protected $hidden = ['db_host', 'db_user', 'db_pass', 'db_name'];

    /**
     * returns the table name for this model
     *
     * @return string
     */
    public static function getTableName()
    {
        return with(new self)->getTable();
    }

    /**
     * Returns decrypted DB Pass
     *
     * @return string
     */
    public function getDbPassAttribute()
    {
        return decrypt($this->attributes['db_pass']);
    }

    /**
     * Encrypts DB Pass
     */
    public function setDbPassAttribute($value)
    {
        $this->attributes['db_pass'] = encrypt($value);
    }

    /**
     * Sections
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function pages()
    {
        return (new Page([], $this))->newQuery();
    }

    /**
     * Sections
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function sections()
    {
        return (new Section([], $this))->newQuery();
    }

    /**
     * Content Types
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contentTypes()
    {
        return $this->hasMany(SiteContentType::class);
    }

    /**
     * Media
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function media()
    {
        return (new Media([], $this))->newQuery();
    }

    /**
     * Domains
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function domains()
    {
        return $this->hasMany(SiteDomain::class);
    }

    /**
     * Templates
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function templates()
    {
        return (new Template([], $this))->newQuery();
    }

    /**
     * Migrations
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function migrations()
    {
        return $this->belongsToMany(SiteMigration::class, 'site_migration_site', null, 'migration_id')
                    ->withPivot(['batch']);
    }

    /**
     * Groups
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function members()
    {
        return (new Member([], $this))->newQuery();
    }

    /**
     * Groups
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function memberGroups()
    {
        return (new MemberGroup([], $this))->newQuery();
    }

    /**
     * Content Authors
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contentAuthors()
    {
        return $this->hasMany(ContentAuthor::class);
    }

    /**
     * Site User Permissions
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteUserPermissions()
    {
        return $this->hasMany(SiteUserPermission::class);
    }


    /**
     * Site User Roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteUserRoles()
    {
        return $this->hasMany(SiteUserRole::class);
    }


    /**
     * AB Tests
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function abTests()
    {
        return (new ABTest([], $this))->newQuery();
    }

    /**
     * Site's SCSS files.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function scssFiles(){
        return $this->hasMany(ScssFile::class);
    }

    /**
     * Get all sass content of the current site.
     *
     * @return string
     */
    public function getAllScss(){
        $content = '';
        $custom_style_file = resource_path('assets/sass/top/sites/' . $this->id . '/style.scss');
        if (File::exists($custom_style_file)){
            $content .= File::get($custom_style_file);
        }

        foreach($this->scssFiles()->orderBy('priority')->get() as $file){
            $content .= "\n /*@@@ file-> {$file->name} */\n" . $file->content;
        }
        return $content;
    }

    /**
     * Get ScssFiles
     *
     * @param $id
     *
     * @return mixed
     */
    public function getScssFilesById($id)
    {
        $model = $this->find($id);

        return $model->scssFiles()
            ->orderBy('priority')
            ->get();
    }
}