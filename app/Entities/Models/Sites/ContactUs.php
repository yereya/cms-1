<?php

namespace App\Entities\Models\Sites;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Page
 *
 * @package App\Entities\Models\Sites
 */
class ContactUs extends SiteModel
{
    use SoftDeletes;

    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * @var string $table
     */
    protected $table = 'contact_us';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'email',
        'message'
    ];
}

