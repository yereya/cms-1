<?php namespace App\Entities\Models\Sites\Templates;

use App\Entities\Models\Sites\ABTest;
use App\Entities\Models\Sites\DynamicLists\DynamicList;
use App\Entities\Models\Sites\SiteModel;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Entities\Models\Sites\Widgets\SiteWidgetTemplate;
use App\Entities\Models\Sites\Widgets\WidgetDataPost;
use App\Libraries\Widgets\WidgetBuilder;

class WidgetData extends SiteModel
{
    const PLACEHOLDER_CLASS  = 'PLACEHOLDER_CLASS';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'widget_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'widget_id',
        'name',
        'widget_template_id',
        'content_type_id',
        'dynamic_list_id',
        'element_type',
        'menu_id',
        'data',
        'ab_tests_action',
        'scss',
        'active'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'object',
        'ab_tests' => 'object'
    ];

    /**
     * Returns built HTML for a widget
     *
     * @return string
     */
    public function getHtmlAttribute()
    {
        return (new WidgetBuilder($this, $this->widget))->getHtml();
    }

    /**
     * @return string
     */
    public function getClassAttribute()
    {
        return 'widget_data_' . $this->id;
    }

    /**
     * Widget
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function widget()
    {
        return $this->belongsTo(SiteWidget::class, 'widget_id');
    }

    /**
     * Templates
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function templates()
    {
        return $this->belongsToMany(SiteWidgetTemplate::class, 'template_row_column_items', 'element_id', 'template_id')
            ->withPivot('element_id', 'template_id');
    }

    /**
     * Template Row Column Items
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function templateRowColumnItem()
    {
        return $this->belongsTo(Item::class, 'id', 'element_id');
    }

    /**
     * Widget Data Items
     *
     * @return mixed
     */
    public function widgetDataItems()
    {
        return $this->belongsTo(Item::class, 'id', 'element_id')
            ->where('element_type', 'widget');
    }

    /**
     * Widget Data Template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function widgetDataTemplate()
    {
        return $this->belongsTo(SiteWidgetTemplate::class, 'widget_template_id', 'id');
    }

    /**
     * Posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(WidgetDataPost::class, 'widget_data_id', 'id');
    }

    /**
     * Dynamic List
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dynamicList()
    {
        return $this->belongsTo(DynamicList::class);
    }

    /**
     * AB Tests
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function abTests()
    {
        return $this->belongsToMany(ABTest::class, 'ab_test_widget_data', 'data_id', 'test_id');
    }

    /**
     * Site Widget Templates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function siteWidgetTemplates()
    {
        return $this->hasMany(SiteWidgetTemplate::class);
    }

    /**
     * All widgets connected to current widget - to Group Widget or Tabbing Widget
     *
     * @return $this
     */
    public function groups() {
        return $this->belongsToMany(WidgetData::class, 'group_widget_data', 'group_widget_id', 'widget_data_id')
            ->withPivot(['tab_name', 'priority', 'by_default']);
    }
}