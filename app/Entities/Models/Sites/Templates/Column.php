<?php namespace App\Entities\Models\Sites\Templates;

use App\Entities\Models\Sites\SiteModel;


class Column extends SiteModel
{


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template_row_columns';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_id',
        'html_element_id',
        'html_class',
        'order',
        'size',
        'locked',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'locked' => 'boolean'
    ];
}