<?php namespace App\Entities\Models\Sites\Templates;

use App\Entities\Models\Sites\SiteModel;
use Illuminate\Support\Collection;

class Template extends SiteModel
{


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'name',
        'active',
        'priority',
        'scss',
        'custom_html_head',
        'custom_html_body'
    ];

    /**
     * Attributes that should be stored as NULL instead of empty string in the DB.
     *
     * @var array $nullable
     */
    protected $nullable = ['parent_id'];

    /**
     * Get As Tree
     * Return templates as nested tree.
     *
     * @param null $template_id
     *
     * @return Collection
     */
    public static function getAsTree($template_id = null)
    {
        return static::where('parent_id', $template_id)
            ->with('childrenRecursive')
            ->orderBy('priority', 'asc')
            ->get();
    }

    /**
     * Children Recursive
     *
     * @return mixed
     */
    public function childrenRecursive()
    {
        return $this->children()
            ->with('childrenRecursive');
    }

    /**
     * Children
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')
            ->orderBy('priority', 'asc')
            ->orderBy('id', 'asc');
    }

    /**
     * Parent Recursive
     *
     * @return mixed
     */
    public function parentRecursive()
    {
        return $this->parent()
            ->with('parentRecursive');
    }

    /**
     * Each template may get working areas to work in
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(self::class)
            ->orderBy('priority');
    }

    /**
     * Each template is divided to rows
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rows()
    {
        return $this->hasMany(Row::class)
            ->orderBy('order');
    }

    /**
     * getClassAttribute
     *
     * @return string
     */
    public function getClassAttribute()
    {
        return 'tpl_' . $this->id;
    }
}