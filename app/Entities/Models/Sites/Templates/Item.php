<?php namespace App\Entities\Models\Sites\Templates;

use App\Entities\Models\Sites\SiteModel;
use Jenssegers\Agent\Agent;


class Item extends SiteModel
{
    /**
     * @ver array
     */
    public static $agentData;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'template_row_column_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'template_id',
        'column_id',
        'element_type',
        'element_id',
        'order',
        'show_on_desktop',
        'show_on_tablet',
        'show_on_mobile',
        'is_live'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        if (!self::$agentData){

            $agent = new Agent();

            self::$agentData = [
                'isTablet' => $agent->isTablet(),
                'isMobile' => $agent->isMobile(),
                'isDesktop' => $agent->isDesktop()
            ];

            if (!self::$agentData['isTablet'] && !self::$agentData['isMobile'] && !self::$agentData['isDesktop']){
                self::$agentData = [
                    'isTablet' => false,
                    'isMobile' => false,
                    'isDesktop' => true
                ];
            }

        }
    }

    /**
     * Check if the current item should be rendered on the current visitor device.
     * This function is used to render the website dynamically.
     *
     * @return boolean
     */
    public function shouldRender()
    {
        if (self::$agentData['isTablet'] && $this->show_on_tablet == 1) {

            return true;
        }

        if (self::$agentData['isMobile'] && !self::$agentData['isTablet'] && $this->show_on_mobile == 1) {

            return true;
        }

        if (self::$agentData['isDesktop'] && $this->show_on_desktop == 1) {

            return true;
        }

        return false;
    }
}