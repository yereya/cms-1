<?php namespace App\Entities\Models\Sites;

use App\Entities\Models\GeoLocation\City;
use App\Entities\Models\GeoLocation\Country;


/**
 * Class Site AB Test Rule
 *
 * @package App\Entities\Models\Sites
 */
class ABTestLocationRule extends SiteModel
{


    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ab_test_location_rules';

    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = ['country_iso_code', 'city_id'];

    /**
     * Test
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function test()
    {
        return $this->belongsTo(ABTest::class);
    }

    /**
     * Country
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_iso_code', 'country_iso_code');
    }

    /**
     * City
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'geoname_id');
    }
}