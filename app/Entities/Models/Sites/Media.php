<?php namespace App\Entities\Models\Sites;


use File;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Site Media
 *
 * @package App\Entities\Models\Sites
 */
class Media extends SiteModel
{
    use SoftDeletes;


    /**
     * @var array $fillable
     */
    protected $fillable = [
        'folder',
        'site_id',
        'site_name',
        'path',
        'thumb',
        'filename',
        'title',
        'alt',
        'metadata',
        'width',
        'height',
        'size',
        'mime'
    ];


    /**
     * @var string
     */
    protected $table = 'media';

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /*
     * Return full path to media file if not found return null
     */
    public function getFullMediaPath() {
        $timestamp = strtotime($this->updated_at);

        return $this->path
            ? url(
                config('media.directory')
                . $this->site_id
                . DIRECTORY_SEPARATOR
                . 'images'
                . $this->path
                . '?' . $timestamp)
            : null;
    }

    /**
     * Labels Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function labels() {
        return $this->belongsToMany(Label::class, 'label_media');
    }
}