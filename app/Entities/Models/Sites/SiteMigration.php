<?php namespace App\Entities\Models\Sites;

use App\Entities\Models\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class Migration
 *
 * @package App\Entities\Models\Sites
 */
class SiteMigration extends Model
{
    /**
     * @var string $table
     */
    public $table = 'site_migrations';

    /**
     * @var array $fillable
     */
    protected $fillable = ['name', 'sql_up', 'sql_down', 'status', 'order'];

    /**
     * Scope Active
     *
     * @param Builder $query
     */
    public function scopeActive(Builder $query)
    {
        $query->whereStatus(true);
    }

    /**
     * Sites
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sites()
    {
        return $this->belongsToMany(Site::class, SitesModel::DB_SITES_PREFIX . 'site_migration_site', 'migration_id')->withPivot(['batch']);
    }

}