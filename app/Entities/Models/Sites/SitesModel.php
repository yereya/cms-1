<?php namespace App\Entities\Models\Sites;

use App\Entities\Models\Model;

class SitesModel extends Model {
    const DB_SITES_PREFIX = 'sites.';
    const DB_SITES_CONNECTION = 'sites';

    protected $connection = 'sites';
}