<?php

namespace App\Entities\Models\Sites\DynamicLists;


use App\Entities\Models\Sites\SiteModel;

/**
 * Class DynamicListFilter
 *
 * @package App\Entities\Models\Sites\DynamicLists
 */
class DynamicListFilter extends SiteModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dynamic_list_filters';


    protected $fillable = [
        'type',
        'field_id',
        'operator',
        'value'
    ];

    /**
     * Post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(
            Post::class,
            'dynamic_list_post'
        )
            ->withPivot(['order']);
    }

    /**
     * Label
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function labels()
    {
        return $this->belongsToMany(
            Label::class,
            'dynamic_list_label');
    }

    /**
     * Dynamic List
     *
     * @return mixed
     */
    public function dynamicList()
    {
        return $this->belongTo(DynamicList::class);
    }

    /**
     * Content Type Fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contentTypeFields()
    {
        return $this->belongsTo(
            SiteContentTypeField::class,
            'field_id');
    }
}