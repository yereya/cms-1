<?php

namespace App\Entities\Models\Sites\DynamicLists;

use App\Entities\Models\Sites\SiteModel;

class DynamicListPost extends SiteModel
{
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string
     */
    protected $table = 'dynamic_list_post';
}