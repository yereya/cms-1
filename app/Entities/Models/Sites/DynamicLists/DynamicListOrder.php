<?php

namespace App\Entities\Models\Sites\DynamicLists;


use App\Entities\Models\Sites\SiteModel;

class DynamicListOrder extends SiteModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dynamic_list_orders';

    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = [
        'dynamic_list_id',
        'type',
        'field_id',
        'direction'
    ];

    /**
     * Posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(
            Post::class,
            'dynamic_list_post')
            ->withPivot(['order']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function labels()
    {
        return $this->belongsToMany(
            Label::class,
            'dynamic_list_label');
    }

    /**
     * Dynamic List.
     *
     * @return mixed
     */
    public function dynamicList()
    {
        return $this->belongTo(DynamicList::class);
    }

    /**
     * ContentTypeFields
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contentTypeFields()
    {
        return $this->belongsTo(
            SiteContentTypeField::class,
            'field_id');
    }
}