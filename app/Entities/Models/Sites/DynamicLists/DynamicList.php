<?php namespace App\Entities\Models\Sites\DynamicLists;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\Label;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\SiteModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DynamicList
 *
 * @package App\Entities\Models\Sites\DynamicLists
 */
class DynamicList extends SiteModel
{
    use SoftDeletes;


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_static_list',
        'query_offset',
        'query_limit',
        'content_type_id',
        'name'
    ];

    /**
     * Posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class)
            ->withPivot(['order']);
    }

    /**
     * Labels
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function labels()
    {
        return $this->belongsToMany(Label::class);
    }


    /**
     * Filter  Fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function filters()
    {
        return $this->belongsToMany(
            SiteContentTypeField::class,
            $this->getConnection()->getDatabaseName() . '.' . 'dynamic_list_filters',
            'dynamic_list_id',
            'field_id'

        )->withPivot(
            'type',
            'field_id',
            'operator',
            'value'
        )->withTimestamps();
    }

    /**
     * Order Fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function orders()
    {
        return $this->belongsToMany(
            SiteContentTypeField::class,
            $this->getConnection()->getDatabaseName() . '.' . 'dynamic_list_orders',
            'dynamic_list_id',
            'field_id'
        )->withPivot(
            'type',
            'direction'
        )->withTimestamps();
    }

    /**
     * Content Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contentType()
    {
        return $this->belongsTo(SiteContentType::class);
    }

}