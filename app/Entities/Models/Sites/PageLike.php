<?php

namespace App\Entities\Models\Sites;


class PageLike extends SiteModel
{


    /**
     * @var array
     */
    protected $fillable = [
        'page_id',
        'user_id',
        'session_id'
    ];

    /**
     * Page
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id');
    }

}


