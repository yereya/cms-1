<?php namespace App\Entities\Models\Sites;

/**
 * Class Site Domain
 *
 * @package App\Entities\Models\Sites
 */
class SiteDomain extends SitesModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'site_domains';

    /**
     * @var array $fillable
     */
    protected $fillable = ['site_id', 'domain','domain_dev'];

    /**
     * Site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}