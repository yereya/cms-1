<?php

namespace App\Entities\Models\Sites;


use App\Entities\Repositories\Sites\PageRepo;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuItem extends SiteModel
{
    public $timestamps = false;
    use SoftDeletes;

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'parent_id',
        'menu_id',
        'post_id',
        'name',
        'class',
        'url',
        'priority',
        'title',
        'target',
        'custom_html',
        'attributes'
    ];

    /**
     * Get Tree
     *  Return pages as nested tree.
     *  Root pages are pages with parent_id null.
     *  Page connect to a parent by parent_id.
     *  Each group of pages under specific page (i.e tree node) are ordered by priority column.
     *
     * @param $menu_id
     *
     * @return Collection
     */
    public static function getTree($menu_id)
    {
        return static::where('menu_id', $menu_id)
            ->where('parent_id', null)
            ->with('childrenRecursive')
            ->orderBy('priority', 'asc')
            ->get();
    }

    /**
     * Boot
     */
    protected static function boot()
    {
        parent::boot();

        self::deleting(function ($item) {
            $item->children()->delete();
        });
    }

    /**
     * Children Recursive
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    /**
     * Children
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function children()
    {
        return $this->hasMany(MenuItem::class, 'parent_id')->orderBy('priority', 'asc');
    }

    /**
     * Parent
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(MenuItem::class, 'parent_id');
    }

    /**
     * Menu
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Get Href Attribute
     *
     * @return string
     */
    public function getHrefAttribute()
    {
        if ($this->attributes['url'] ?? false) {
            return $this->attributes['url'];
        } else {
            if ($this->attributes['post_id'] ?? false) {
                return (new PageRepo())->getUrl($this->attributes['post_id']);
            } else {
                return '';
            }
        }
    }
}
