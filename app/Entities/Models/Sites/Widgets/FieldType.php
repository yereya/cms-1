<?php namespace App\Entities\Models\Sites\Widgets;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\SitesModel;

/**
 * App\Entities\Models\Widgets\FieldType

 *
 * @property integer $id
 * @property string  $name
 * @property string  $controller
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\FieldType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\FieldType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\FieldType
 *         whereController($value)
 */
class FieldType extends SitesModel
{
    const TYPE_TEXT = 'text';
    const TYPE_TEXT_AREA = 'textarea';
    const TYPE_WYSIWYG = 'wysiwyg';
    const TYPE_NUMBER = 'number';
    const TYPE_SELECT = 'select';
    const TYPE_ARRAY = 'array';
    const TYPE_CHECKBOX = 'checkbox';
    const TYPE_MULTI_SELECT = 'multi-select';
    const TYPE_MEDIA = 'media';

    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'site_widget_field_types';
}