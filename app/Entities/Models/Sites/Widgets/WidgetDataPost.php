<?php

namespace App\Entities\Models\Sites\Widgets;

use App\Entities\Models\Sites\SiteModel;


class WidgetDataPost extends SiteModel
{
    /**
     * @var bool created_at & updated_at
     */
    public $timestamps = false;

    /**
     * @var string $table name
     */
    protected $table = 'widget_data_post';
    /**
     * @var array fillable fields
     */
    protected $fillable = ['dynamic_list_id', 'content_type_id', 'post_id', 'order'];
}
