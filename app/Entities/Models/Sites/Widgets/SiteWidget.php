<?php namespace App\Entities\Models\Sites\Widgets;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\SitesModel;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Sites\Templates\WidgetData;

/**
 * App\Entities\Models\Widgets\Widget

 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Field[] $fields
 * @property integer                                               $id
 * @property string                                                $name
 * @property string                                                $controller
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\SiteWidget whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\SiteWidget whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Entities\Models\Sites\Widgets\SiteWidget
 *         whereController($value)
 */
class SiteWidget extends SitesModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'site_widgets';

    /**
     * @var array $fillable
     */
    protected $fillable = ['name', 'controller'];

    /**
     * Fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fields()
    {
        return $this->hasMany(Field::class, 'widget_id');
    }

    /**
     * Data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function widgetData()
    {
        return $this->hasMany(WidgetData::class, 'widget_id');
    }

    /**
     * Templates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function templates()
    {
        return $this->hasMany(Template::class, 'id', 'widget_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function widget_template()
    {
        return $this->hasOne(SiteWidgetTemplate::class, 'id', 'widget_id');
    }

    /**
     * getViewNamespaceAttribute
     *
     * @return string
     */
    public function getViewNamespaceAttribute()
    {
        return 'Widget' . $this->controller;
    }
}