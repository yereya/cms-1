<?php namespace App\Entities\Models\Sites\Widgets;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\SitesModel;
use App\Entities\Models\Sites\Templates\WidgetData;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * App\Entities\Models\Widgets\SiteWidgetTemplate
 */
class SiteWidgetTemplate extends SitesModel
{

    use SoftDeletes;
    use RevisionableTrait;

    /**
     * This param used by Revision package
     *
     * @var bool
     */
    protected $revisionEnabled = true;

    /**
     * Define which column to revision
     * This param used by Revision package
     *
     * @var array
     */
    protected $keepRevisionOf = ['scss', 'blade'];

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * @var string $table
     */
    protected $table = 'site_widget_templates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['site_id','name', 'file_name', 'blade', 'scss', 'widget_id', 'description', 'published'];

    /**
     * Widget
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function widget()
    {
        return $this->hasOne(SiteWidget::class, 'id', 'widget_id');
    }


    public function widgetData()
    {
        return $this->hasMany(WidgetData::class, 'widget_template_id');
    }

    /**
     * Get Class Attribute
     *
     * @return string
     */
    public function getClassAttribute()
    {
        return 'widget_tpl_' . $this->id;
    }

   /**
    * @param $query
    * @param $site
    *
    * @return mixed
    */
    public function scopeWhereSite($query,$site){
        return $query->where('site_id',$site->id);
    }
}