<?php namespace App\Entities\Models\Sites;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Site AB Test
 *
 * @package App\Entities\Models\Sites
 */
class ABTest extends SiteModel
{

    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ab_tests';

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type', 'relation'];

    /**
     * Rules
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rules()
    {
        return $this->hasMany(ABTestRule::class, 'test_id');
    }

    /**
     * Location Rules
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locationRules()
    {
        return $this->hasMany(ABTestLocationRule::class, 'test_id');
    }
}