<?php

namespace App\Entities\Models\Sites;

use App\Http\Traits\HasGravatar;

/**
 * Class Page
 *
 * @package App\Entities\Models\Sites
 */
class Comment extends SiteModel
{
    use HasGravatar;

    const STATUS_UNREAD = 0;
    const STATUS_APPROVED = 1;
    const STATUS_UNAPPROVED = 2;

    const VISIBILITY_HIDDEN = 0;
    const VISIBILITY_VISIBLE = 1;

    const STATUS_OPTIONS = [
        '0' => 'Unread',
        '1' => 'Approved',
        '2' => 'UnApproved'
    ];

    const STATUS_OPTIONS_CLASSES = [
        '0' => 'label-info',
        '1' => 'label-danger',
        '2' => 'label-success',
    ];

    const VISIBILITY_OPTIONS = [
        '0' => 'Hidden',
        '1' => 'Visible',
    ];

    const VISIBILITY_OPTIONS_CLASSES = [
        '0' => 'label-danger',
        '1' => 'label-success',
    ];
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'comments';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'site_id',
        'post_id',
        'user_id',
        'parent_id',
        'type',
        'email',
        'author',
        'content',
        'status',
        'visible',
        'notify_me',
        'rating_overall',
        'rating_1',
        'rating_2',
        'rating_3',
        'rating_4',
        'rating_5',
        'rating_6'
    ];

    protected $casts = [
        'content' => 'array',
    ];

    /**
     * post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Children Recursive
     *
     * @return mixed
     */
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
    }

    /**
     * children
     *
     * @return mixed
     */
    public function children()
    {
        return $this->hasMany(Comment::class, 'parent_id')
            ->orderBy('created_at', 'asc');
    }

    /**
     * Get the overall rating value.
     *
     * @param  string  $value
     * @return void
     */
    public function setRatingAttribute($value)
    {
        return $this->attributes['rating_overall'];
    }
}

