<?php

namespace App\Entities\Models\Sites;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\Widgets\FieldType;

/**
 * Class SiteSettingDefault
 *
 * @package App\Entities\Models\Sites
 */
class SiteSettingDefault extends SitesModel
{

    /** Same types in both site_settings and site_settings_default tables **/
    const TYPE_PAGES = 'pages';
    const TYPE_TEMPLATES = 'templates';
    const TYPE_MEMBERS = 'members';
    const TYPE_CONTENT_TYPES = 'ContentTypes';

    /** Types which are differ in site_settings and site_settings_default tables **/
    const TYPE_CONTENT_TYPE = 'ContentType';
    const TYPE_CONTENT_TYPE_ITEM = 'ContentTypeItem';
    const TYPE_SITE = 'site';
    const TYPE_PAGE = 'page';
    const TYPE_TEMPLATE = 'template';
    const TYPE_MENU = 'menu';

    /**
     * @var string $table
     */
    protected $table = "site_settings_defaults";

    /**
     * Field Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function field_type()
    {
        return $this->hasOne(FieldType::class, 'id', 'field_type_id');
    }
}
