<?php


namespace App\Entities\Models\Sites;

use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class PostLike
 *
 * @package App\Entities\Models\Sites
 */
class PostLike extends SiteModel
{

    use SoftDeletes;
    /**
     * @var string
     */
    protected $primaryKey = 'post_id';
    /**
     * @var bool
     */
    public $timestamp = false;

    /**
     * @var array
     */
    protected $fillable = [
        'post_id',
        'like',
        'dislike'
    ];

}