<?php namespace App\Entities\Models\Sites;


/**
 * Class Site Domain
 *
 * @package App\Entities\Models\Sites
 */
class MemberGroup extends SiteModel
{


    /**
     * @var string $table
     */
    protected $table = 'member_groups';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'parent_id',
    ];


    /**
     * Members
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members()
    {
        return $this->belongsToMany(Member::class)
            ->withPivot('member_group_id', 'member_id');
    }

}