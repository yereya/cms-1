<?php

namespace App\Entities\Models\Sites;

/**
 * Class Page
 *
 * @package App\Entities\Models\Sites
 */
class Newsletter extends SiteModel
{
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'newsletter';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'email',
    ];
}

