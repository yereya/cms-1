<?php

namespace App\Entities\Models\Sites;


use Illuminate\Database\Eloquent\SoftDeletes;

class Ribbon extends SiteModel
{
    public $dates = ['deleted_at'];
    use SoftDeletes;

    protected $table = 'ribbons';
    protected $fillable = ['name', 'image_id', 'html_class'];

    public function posts() {
        return $this->belongsToMany(Post::class)->withPivot('dynamic_list_id');
    }
}
