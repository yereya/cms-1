<?php

namespace App\Entities\Models\Sites;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Popup
 *
 * @package App\Entities\Models\Sites
 */
class Popup extends SiteModel
{
    use SoftDeletes;

    /**
     * @ver Integer
     */
    const ACTIVE = 1;

    /**
     * @ver Integer
     */
    const NOT_ACTIVE = 0;

    /**
     * @ver array
     */
    const PARAMS_PROPERTIES = [
        'on_page_load'            => ['label' => 'Delay Time After Page Load', 'required' => false],
        'x_seconds_of_inactivity' => ['label' => 'Time Of Inactivity', 'required' => true],
        'scroll_page'             => ['label' => 'Scroll Percentage', 'required' => true]
    ];

    /**
     * @ver array
     */
    const APPEAR_EVENT_LABELS = [
        'on_page_load'            => ['text' => 'On Page Load', 'class' => 'bg-blue-steel'],
        'x_seconds_of_inactivity' => ['text' => 'After X second of inactivity', 'class' => 'bg-red-sunglo'],
        'scroll_page'             => ['text' => 'On Scroll Page', 'class' => 'bg-yellow-haze'],
        'on_mouse_exit'           => ['text' => 'On Mouse Exit', 'class' => 'bg-blue-steel']
    ];

    /**
     * @ver array
     */
    const ACTIVE_LABELS = [
        self::NOT_ACTIVE => ['text' => 'Not Active', 'class' => 'label-danger'],
        self::ACTIVE     => ['text' => 'Active', 'class' => 'label-success'],
    ];

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'popups';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'active',
        'template_id',
        'width',
        'height',
        'start_date',
        'end_date',
        'appear_event',
        'param',
        'cookie_name',
        'cookie_time_on_conversion',
        'cookie_time_on_close',
        'post_ids',
        'pivot_exclude'
    ];

    /**
     * Overwrite the save in order to sync posts.
     * Used to include or exclude popups from specific pages.
     *
     * @param array $options
     *
     * @return void
     */
    public function save(array $options = [])
    {
        if (isset($this->attributes['post_ids'])) {
            $post_ids = $this->attributes['post_ids'];
            unset($this->attributes['post_ids']);
        } else {
            $post_ids = [];
        }

        parent::save($options);

        $this->posts()->sync($post_ids);
    }

    /**
     * Check if the current popup should be display in the current time.
     *
     * @return boolean
     */
    public function shouldDisplay()
    {
        $now = Carbon::now();
        if (!$this->start_date || Carbon::createFromFormat('Y-m-d H:i:s', $this->start_date)->gt($now)) {
            if ($this->end_date
                && Carbon::createFromFormat('Y-m-d H:i:s', $this->end_date)->gt($now)
            ) {
                // The current time is bigger then the end date, don't display the popup.
                return false;
            }
            // Popup is in the display range or it has no end date.
            return true;
        }
        // Popup's start date is in the future
        return false;
    }

    /**
     * Get the boolean value of the pivot_exclude attributes.
     *
     * @return boolean
     */
    public function getPivotExcludeAttribute()
    {
        return (bool)$this->attributes['pivot_exclude'];
    }

    /**
     * Posts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class, 'popup_post');
    }
}