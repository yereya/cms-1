<?php namespace App\Entities\Models\Sites;


/**
 * Class Site Domain
 *
 * @package App\Entities\Models\Sites
 */
class Member extends SiteModel
{


    /**
     * @var string $table
     */
    protected $table = 'members';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'email',
        'name',
        'password',
        'password_salt',
        'status',
        'remember_token',
        'email_verified',
        'newsletter_approved'
    ];

    /**
     * @var array $hidden
     */
    protected $hidden = [
        'password',
        'password_salt',
        'remember_token'
    ];


    /**
     * Member Groups
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function memberGroups()
    {
        return $this->belongsToMany(MemberGroup::class, 'member_group_members', 'member_id', 'member_group_id')
            ->withPivot('member_group_id', 'member_id');
    }

}