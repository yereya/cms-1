<?php namespace App\Entities\Models\Sites;


/**
 * Class Site Section
 *
 * @package App\Entities\Models\Sites
 */
class Section extends SiteModel
{
    /**
     * @var bool $timestamps
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'sections';

    /**
     * @var array $fillable
     */
    protected $fillable = ['name'];
}