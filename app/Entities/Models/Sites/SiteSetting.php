<?php

namespace App\Entities\Models\Sites;

use App\Entities\Models\Model;
use App\Entities\Models\Sites\Widgets\FieldType;

/**
 * Class SiteSetting
 *
 * @package App\Entities\Models\Sites
 */
class SiteSetting extends SitesModel
{
    const HEADER_RAW_CODE = 'header_raw_code';
    const FOOTER_RAW_CODE = 'footer_raw_code';
    const SCROLL_TO_TOP = 'scroll_to_top';
    const BODY_RAW_CODE = 'body_raw_code';

    /**
     * Hold the list of options in case it is a select
     *
     * @var array $list
     */
    public $list = [];

    /**
     * @var array $fillable
     */
    protected $fillable = ['site_id', 'name', 'type', 'key', 'value', 'field_type_id', 'options_query', 'priority'];

    /**
     * Get Page Type
     *
     * @param $page_id
     *
     * @return string
     */
    public static function getPageType($page_id)
    {
        return SiteSettingDefault::TYPE_PAGE . '.' . $page_id;
    }

    /**
     * Get Content Type Type
     *
     * @param $content_type_id
     *
     * @return string
     */
    public static function getContentTypeType($content_type_id)
    {
        return SiteSettingDefault::TYPE_CONTENT_TYPE . '.' . $content_type_id;
    }


    /**
     * Get Template Type
     *
     * @param $template_id
     *
     * @return string
     */
    public static function getTemplateType($template_id)
    {
        return SiteSettingDefault::TYPE_TEMPLATE . '.' . $template_id;
    }

    /**
     * Get Menu Type
     *
     * @param $menu_id
     *
     * @return string
     */
    public static function getMenuType($menu_id)
    {
        return SiteSettingDefault::TYPE_MENU . '.' . $menu_id;
    }

    /**
     * Get Content Type Item Type
     *
     * @param $content_type_id
     * @param $item_id
     *
     * @return string
     */
    public static function getContentTypeItemType($content_type_id, $item_id)
    {
        return SiteSettingDefault::TYPE_CONTENT_TYPE_ITEM . '.' . $content_type_id . '.' . $item_id;
    }

    /**
     * Field Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\hasOne
     */
    public function field_type()
    {
        return $this->hasOne(FieldType::class, 'id', 'field_type_id');
    }

    /**
     * Site
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
