<?php namespace App\Entities\Models\Sites;

use App\Entities\Models\Sites\Templates\Template;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * Class Page
 *
 * @package App\Entities\Models\Sites
 */
class Page extends SiteModel
{
    use SoftDeletes;

    const pageTypes = [
        ['id' => 'static', 'text' => 'Static Page'],
        ['id' => 'archive', 'text' => 'Archive Page'],
        ['id' => 'redirect', 'text' => 'Redirect Page']
    ];
    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'pages';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'template_id',
        'template_mobile_id',
        'type',
        'url_type',
        'url_redirect',
        'redirect_page_short_code',
        'url_query',
        'priority',
        'post_id',
        'content_type_id',
        'is_homepage'
    ];

    /**
     * Template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function template()
    {
        return $this->belongsTo(Template::class);
    }

    /**  
     * Mobile Template
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mobileTemplate()
    {
        return $this->belongsTo(Template::class, 'template_mobile_id', 'id');
    }

    /**
     * Delete Menu Items
     */
    public function deleteMenuItems()
    {
        $this->menuItems()->get()
            ->each(function ($item) {
                $item->delete();
            });
    }

    /**
     * Menu Items
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function menuItems()
    {
        return $this->hasMany(MenuItem::class, 'post_id', 'post_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }

    /**
     * Page Content Type
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pageContentTypes()
    {
        return $this->hasMany(PageContentType::class, 'post_id', 'post_id');
    }

    /**
     * Is home page
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeHomePage($query)
    {
        return $query->where('is_homepage', 1);
    }
}
