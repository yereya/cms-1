<?php

namespace App\Entities\Models\Sites;


/**
 * Class PostRedirectUrl
 *
 * @package App\Entities\Models\Sites
 */
class PostRedirectUrl extends SiteModel
{
    public $timestamps = false;
    protected $fillable = [
        'post_id',
        'slug',
        'url',
        'is_default'
    ];
}