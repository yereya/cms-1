<?php namespace App\Entities\Models\Sites;

use App\Entities\Models\Model;
use SiteConnectionLib;

/**
 * Class Model
 *
 * @package App\Entities\Models
 */
class SiteModel extends Model
{
    public function __construct(array $attributes = [])
    {
        // Resolves the connection for site
        $this->resolveSiteConnection();

        parent::__construct($attributes);
    }

    /**
     * resolveSiteConnection
     */
    public function resolveSiteConnection()
    {
        $site_connection_name = SiteConnectionLib::getConnectionName();
        $this->setConnection($site_connection_name);
    }
}