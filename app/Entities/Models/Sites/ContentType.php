<?php

namespace App\Entities\Models\Sites;

use Venturecraft\Revisionable\RevisionableTrait;
use App\Entities\Models\Sites\DynamicLists\DynamicList;


/**
* Class Site Page
*
* @package App\Entities\Models\Sites
*/
class ContentType extends SiteModel
{
    use RevisionableTrait;

    /**
     * @ver bool
     */
    protected $revisionEnabled = true;

    /**
     * @ver bool
     */
    protected $revisionCleanup = true;

    /**
     * @ver integer
     */
    protected $historyLimit = 500;

    /**
     * @ver array
     */
    protected $dontKeepRevisionOf = [
        'updated_at',
        'created_at'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array $fillable
     */
    protected $fillable = [];

    /**
     * author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(ContentAuthor::class, "content_author_id");
    }

    /**
     *  Dynamic List
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function dynamicList()
    {
        return $this->hasOne(DynamicList::class);
    }
}


