<?php

namespace App\Entities\Models\Sites;


use App\Entities\Models\Model;

class SiteSeoSettings extends SitesModel
{
    public $timestamps = false;

    protected $table = 'seo_settings';
    /**
     * @var array $fillable
     */
    protected $fillable = [

        'show_on_sitemap',
        'seo_post_title',
        'description',
        'keywords',
        'og_image',
        'og_title',
        'og_type',
        'og_url',
        'og_description',
        'robots_index',
        'robots_follow',
        'robots_custom',
        'permalink',
        'canonical_url'
    ];

}
