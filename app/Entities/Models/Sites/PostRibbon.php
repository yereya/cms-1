<?php

namespace App\Entities\Models\Sites;


class PostRibbon extends SiteModel
{
    public $timestamps = false;

    protected $table = 'post_ribbon';
    protected $fillable = ['post_id', 'dynamic_list_id', 'ribbon_id'];

    public function post() {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }

    public function ribbon() {
        return $this->belongsTo(Ribbon::class, 'ribbon_id', 'id');
    }
}