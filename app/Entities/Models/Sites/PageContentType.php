<?php namespace App\Entities\Models\Sites;

use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;


/**
 * Class Site Page
 *
 * @package App\Entities\Models\Sites
 */
class PageContentType extends SiteModel
{
    public $timestamps = false;

    /**
     * @var string $table
     */
    protected $table = 'page_content_type';
    /**
     * @var array $fillable
     */
    protected $fillable = [
        'post_id',
        'template_id',
        'template_mobile_id',
        'content_type_id',
        'priority'
    ];

    /**
     * Get the current content type label.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function labels(){
        return $this->hasOne(Label::class);
    }

    /**
     * desktopTemplate
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function desktopTemplate()
    {
        return $this->belongsTo(Template::class, 'template_id');
    }

    /**
     * mobileTemplate
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mobileTemplate()
    {
        return $this->belongsTo(Template::class, 'template_mobile_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contentType()
    {
        return $this->belongsTo(SiteContentType::class, 'content_type_id', 'id');
    }
}
