<?php namespace App\Entities\Models;

use App\Entities\Models\Model;
use App\Entities\Models\Users\User;
use DB;

/**
 * Class Log
 * @package App\Entities\Models
 */
class Log extends Model
{

    /**
     * @var bool $timestamps
     */
    public $timestamps = true;

    /**
     * @var string $table
     */
    protected $table = 'logs';

    /**
     * @var array $fillable
     */
    protected $fillable = [
        'entry_record_id',
        'system_id',
        'task_id',
        'class_name',
        'function_name',
        'message',
        'error_type',
        'run_duration',
        'uuid_entry_record_id',
        'parent_id'
    ];

    protected $errors = [
        'info',
        'notice',
        'warning',
        'error',
        'debug'
    ];

    /**
     * Systems
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function systems() {
        return $this->belongsTo(System::class, 'system_id', 'id');
    }


    /**
     * Users
     *
     * @return mixed
     */
    public function users() {
        return $this->belongsTo(User::class, 'initiator_user_id', 'id')->select('id', 'username');
    }


    /**
     * Get Error Types
     * 
     * @return mixed
     */
    public static function getErrorTypes()
    {
        return with(new self)->errors;
    }
}