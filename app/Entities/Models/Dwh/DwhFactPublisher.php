<?php namespace App\Entities\Models\Dwh;

use App\Entities\Models\Model;

/**
 * Class DwhFactPublisher
 *
 * @package App\Entities\Models\Dwh
 */
class DwhFactPublisher extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'dwh';

    protected $table = 'dwh_fact_publishers';

    public $timestamps = false;

    protected $fillable = [
        'source_report_uid',
        'report_id',
        'report_name',
        'publisher_id',
        'publisher_name',
        'account_id',
        'account_name',
        'campaign_id',
        'campaign_name',
        'ad_group_id',
        'ad_group_name',
        'keyword_name',
        'keyword_status',
        'sid',
        'match_type',
        'device',
        'clicks',
        'impressions',
        'cost',
        'max_cpc',
        'avg_position',
        'quality_score',
        'stats_date_tz',
        'advertiser_net_revenue',
        'publisher_revenue',
        'network_net_revenue',
        'creation_date',
        'update_date',
        'advertiser_id',
    ];
}
