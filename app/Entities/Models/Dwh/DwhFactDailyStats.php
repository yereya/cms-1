<?php


namespace App\Entities\Models\Dwh;


use App\Entities\Models\Model;

/**
 * Class DwhFactDailyStats
 *
 * @package App\Entities\Models\Dwh
 */
class DwhFactDailyStats extends Model
{
    /**
     * @var string
     */
    protected $connection = 'dwh';

    /**
     * @var string
     */
    protected $table = 'dash_fact_dailyStats';




}