<?php namespace App\Entities\Models\Dwh;

use App\Entities\Models\Model;

/**
 * Class DwhFactTracks
 *
 * @package App\Entities\Models\Dwh
 */
class DwhFactTracks extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'dwh';

    protected $table = 'dwh_fact_tracks';

    protected $casts = [ 'id' => 'string' ];

    public $timestamps = false;
}

