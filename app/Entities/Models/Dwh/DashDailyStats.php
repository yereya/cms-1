<?php


namespace App\Entities\Models\Dwh;


use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Model;

/**
 * Class DashDailyStats
 *
 * @package App\Entities\Models\Dwh
 */
class DashDailyStats extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'dwh';

    /**
     * @var string
     */
    protected $table = "dash_dailyStats";

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function advertiser()
    {
        return $this->belongsTo(Advertiser::class, 'advertiser_id', 'id');
    }
}