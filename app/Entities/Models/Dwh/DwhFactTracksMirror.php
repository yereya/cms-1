<?php


namespace App\Entities\Models\Dwh;

use App\Entities\Models\Model;

class DwhFactTracksMirror extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'dwh';

    protected $table = 'dwh_fact_tracks_mirror';

    public $timestamps = false;
}


