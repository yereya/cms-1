<?php


namespace App\Entities\Models\Dwh;


use App\Entities\Models\Model;

class brands_group_discrepancies_vw  extends Model
{

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'dwh';

    /**
     * @var string
     */
    protected $table = "brands_group_discrepancy_vw";


}
