<?php

namespace App\Entities\Models\Mrr;

use Illuminate\Database\Eloquent\Model;

class MrrChangeLogState extends Model
{
    /**
     * @var string - connection name
     */
    protected $connection = 'mrr';

    /**
     * @var string - table name
     */
    protected $table = 'mrr_change_log_state';

    protected $fillable = [
        'report_id',
        'report_name',
        'advertiser_id',
        'advertiser_name',
        'publisher_id',
        'publisher_name',
        'account_id',
        'account_name',
        'campaign_id',
        'campaign_name',
        'campaign_status',
        'campaign_budget',
        'campaign_device_bid_adjustment',
        'device',
        'ad_group_id',
        'ad_group_name',
        'ad_group_status',
        'ad_group_bid',
        'ad_group_device_bid_adjustment',
        'match_type',
        'keyword_id',
        'keyword_name',
        'keyword_status',
        'keyword_bid',
        'ad',
        'created_at',
        'updated_at'
    ];
}
