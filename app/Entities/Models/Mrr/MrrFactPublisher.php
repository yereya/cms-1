<?php namespace App\Entities\Models\Mrr;

use App\Entities\Models\Model;

/**
 * Class DwhFactPublisher
 *
 * @package App\Entities\Models\Dwh
 */
class MrrFactPublisher extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    protected $table = 'mrr_fact_publishers';

    public $timestamps = false;

    protected $fillable = [
        'source_report_uid',
        'report_id',
        'report_name',
        'publisher_id',
        'publisher_name',
        'account_id',
        'account_name',
        'campaign_id',
        'campaign_name',
        'campaign_status',
        'ad_group_id',
        'ad_group_name',
        'ad_group_status',
        'keyword_name',
        'keyword_status',
        'sid',
        'match_type',
        'destination_url',
        'description_line_1',
        'description_line_2',
        'device',
        'ad',
        'clicks',
        'impressions',
        'cost',
        'currency',
        'max_cpc',
        'avg_position',
        'quality_score',
        'stats_date_tz',
        'advertiser_net_revenue',
        'publisher_revenue',
        'network_net_revenue',
        'creation_date',
        'update_date'
    ];


    /**
     * @param $query
     * @param $id
     * @return mixed
     */
    public function ScopeWhereAccount($query, $id)
    {
        return $query->where('account_id', $id);
    }

    /**
     * @param $query
     * @param $range
     * @return mixed
     */
    public function ScopeWhereDates($query, $range)
    {
        $from = $range['from'];
        $to = $range['to'];

        return $query->whereBetween('stats_date_tz', [$from, $to]);
    }

}
