<?php


namespace App\Entities\Models\Mrr;


use App\Entities\Models\Model;

/**
 * Class MrrFactCalls
 *
 * @package App\Entities\Models\Mrr
 */
class MrrFactCalls extends Model
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    protected $table = 'mrr_fact_calls';

    public $timestamps = false;

    protected $fillable = [
        'call_start_time',
        'transaction_id',
        'transaction_type',
        'order_id',
        'media_type',
        'source',
        'promo_number_id',
        'city',
        'region',
        'qualified_regions',
        'repeat_caller',
        'caller_id',
        'phone_type',
        'recording',
        'corrected_at',
        'call_record_id',
        'transfer_type',
        'notes',
        'verified_zip_code',
        'end_of_call_reason',
        'destination_phone_number',
        'advertiser_id',
        'advertiser_name',
        'advertiser_campaign_id',
        'advertiser_campaign',
        'call_result',
        'duration',
        'affiliate_id',
        'affiliate_name',
        'call_result_description_detail',
        'repeat_calling_phone_number',
        'mobile',
        'complete_call_id',
        'transfer_from_type',
        'hangup_cause',
        'signal_source',
        'revenue',
        'sale_amoun',
        'corrects_transaction_id',
        'original_order_id',
        'invoca_id',
        'calling_page',
        'gclid',
        'utm_source',
        'utm_medium',
        'utm_campaign',
        'utm_content',
        'vertical',
        'advertiser_campaign_name',
        'affiliate_commissions_ranking',
        'advertiser_payin_localized',
        'affiliate_payout_localized',
        'impression_token',
        'ads_token',
        'created_at',
        'updated_at'
    ];


}