<?php

namespace App\Entities\Models\Mrr;

use Illuminate\Database\Eloquent\Model;

class ClickReport extends Model
{
    protected $connection = 'mrr';

    protected $table = 'mrr_click_report';

    public $timestamps = false;

    protected $fillable = [
        'publisher_id', 'publisher_name',
        'account_id', 'account', 'ad_type', 'ad_group_id', 'ad_group_name', 'ad_group_status',
        'network', 'network_with_search_partners', 'aoi_most_specific_target_id', 'campaign_id',
        'campaign_location_target', 'campaign_name', 'campaign_status', 'city', 'click_type',
        'country_territory', 'ad_id', 'keyword_id', 'keyword_placement', 'date', 'device',
        'most_specific_location_target_physical_location', 'month_of_year', 'page', 'region',
        'top_vs_other', 'google_click_id'
    ];
}
