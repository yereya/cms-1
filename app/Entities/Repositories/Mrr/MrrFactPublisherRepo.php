<?php


namespace App\Entities\Repositories\Mrr;


use App\Entities\Models\Mrr\MrrFactPublisher;
use App\Entities\Repositories\Repository;

/**
 * Class MrrFactPublisherRepo
 * @package App\Entities\Repositories\Mrr
 */
class MrrFactPublisherRepo extends Repository
{
    /**
     *id
     */
    const MODEL = MrrFactPublisher::class;

    /**
     * @param $attr
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByDateAndAccount($attr)
    {
        $range = $attr['ranges'] ?: false;
        $id = $attr['account_id'] ?: false;

        $query = $this->query();
        $query->whereAccount($id);
        $query->whereDates($range);

        return $query->get() ?: collect();
    }
}