<?php

namespace App\Entities\Repositories\Reports\Publishers\BrandPerformance;

use App\Http\Traits\ModelQueryBuilderTrait;
use App\Libraries\Bo\OutInterfaces\ScraperLib;
use App\Libraries\ShortCodes\ShortCodesLib;
use App\Libraries\TpLogger;
use DB;
use App\Entities\Models\Log;
use Psr\Log\InvalidArgumentException;

use App\Entities\Models\Reports\Publishers\BrandScraper\Scraper;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperAdvanceProperties;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperFetcher;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperFetcherProperties;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperProcessor;
use Validator;


class ScraperRepo
{
    use ModelQueryBuilderTrait;

    /**
     * constants
     */
    private $const_method_pop = 'pop';
    private $const_method_push = 'push';
    private $ignore_scraper_keys = ['created_at', 'updated_at', 'deleted_at'];
    private $igonre_fetcher_keys = ['created_at', 'updated_at', 'deleted_at'];

    private $path_lib = '/Libraries/Scraper/Requests/BrandsPerformance/Providers/Fetcher/Translators';
    private $path = null;

    /**
     * @var array - where params
     */
    private $params = [];

    /**
     * @var string - constant pop or push
     */
    private $method;

    /**
     * @var null | array with all scrapers data
     */
    private $scraper_data;

    /**
     * @var
     */
    private $scraper_selected_data;

    /**
     * @var int : current scraper
     */
    private $index_scraper = 0;

    /**
     * @var int : current fetcher
     */
    private $index_fetcher = 0;

    /**
     * @var int : current processor
     */
    private $index_processor = 0;

    /**
     * @var int : current translator
     */
    private $index_translator = 0;

    /**
     * @var array
     */
    private $available_translators = [];

    /**
     * ScraperParams constructor.
     */
    public function __construct() {
        $this->query = DB::table((new Scraper)->getTable());
        $this->scraper_data = null;
        $this->method = $this->const_method_pop;
        $this->path = app_path() . $this->path_lib;
    }

    /**
     * return all translators names/class/methods
     *
     * @return array
     */
    public function getAvailableTranslators() {
        if (count($this->available_translators))
            return $this->available_translators;

        $scans = array_diff(scandir($this->path), array('..', '.'));
        $path = 'App' . $this->path_lib . '\\';
        $folder_path = app_path() . $this->path_lib . '\\';
        $folder_path = str_replace('\\', '/', $folder_path);

        $this->available_translators = [];
        foreach ($scans as $scan) {
            if (!is_dir($folder_path . $scan)) {
                $full_path = $path . substr($scan, 0, strpos($scan, '.'));
                $full_path = str_replace('/', '\\', $full_path);

                try {
                    $public = new \ReflectionClass($full_path);
                    $methods_in_class = $public->getMethods(\ReflectionProperty::IS_PUBLIC);

                    foreach ($methods_in_class as $method) {
                        if (strpos($method, '__') === false) {
                            $class_name = substr($scan, 0, strpos($scan, '.'));
                            $params_str = '';

                            // Slice first param since it is an automatically-parsed param
                            foreach (array_slice($method->getParameters(), 1) as $param) {
                                $default = $param->isOptional() ? $param->getDefaultValue() : 'required*';
                                $params_str .= $param->getName() . '(' . (is_array($default) ? 'Array' : $default) . '), ';
                            }

                            $this->available_translators[] = [
                                'class' => $class_name,
                                'method' => $method->name,
                                'name' => $class_name .'@'. $method->name,
                                'namespace' => $method->class,
                                'params'    => substr($params_str, 0, -2)
                            ];
                        }
                    }
                } catch (\ReflectionException $e) {
                    $this->available_translators[] = ['error' => $e->getMessage()];
                }
            }
        }

        return $this->available_translators;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function with(array $params) {
        $this->method = $this->const_method_push;
        $this->params = $params;
        return $this;
    }

    /**
     * return results
     * @return array
     */
    public function get() {

        //Checks if the data is taken from db or UI
        //TODO REMOVE THIS, both should act the same
        if ($this->method == $this->const_method_pop) {
            return $this->normalizeDataFromBackend();
        }

        return  $this->normalizeDataFromFrontend();
    }

    /**
     * will get the current scrapper
     * when $field is passed it will return only the specific field (example: "id")
     * It will not return the fetchers only the scraper arguments and the advanced arguments
     *
     * @param null $field
     * @return null
     */
    public function getScraper($field = null) {
        if (is_null($this->scraper_data)) {
            $this->get();
        }

        if (is_null($field) && isset($this->scraper_data[$this->index_scraper])) {
            return [
                'id' => $this->scraper_data[$this->index_scraper]['id'],
                'properties' => $this->scraper_data[$this->index_scraper]['properties']
            ];
        }

        if (isset($this->scraper_data[$this->index_scraper][$field])) {
            return $this->scraper_data[$this->index_scraper][$field];
        } else {
            return isset($this->scraper_data[$this->index_scraper]['properties'][$field]) ?
                $this->scraper_data[$this->index_scraper]['properties'][$field]:
                null;
        }
    }

    /**
     * jump to next scraper
     * return the id of the scraper (not index)
     */
    public function nextScraper() {
        if (!is_null($this->scraper_data) && isset($this->scraper_data[$this->index_scraper + 1])) {

            $this->index_scraper++;
            $this->index_fetcher = 0;
            $this->index_processor = 0;
            $this->index_translator = 0;

            return $this->scraper_data[$this->index_scraper]['id'];
        }

        return null;
    }

    /**
     * return a count scrapers
     */
    public function countScrapers() {
        return count($this->scraper_data);
    }

    /**
     * return a count fetchers
     */
    public function countFetchers() {
        return $this->scraper_data[$this->index_scraper]['fetchers'] ?
            count($this->scraper_data[$this->index_scraper]['fetchers']) : 0;
    }

    /**
     * return a count translators
     */
    public function countTranslators() {
        return isset($this->scraper_data[$this->index_scraper]['fetchers'][$this->index_fetcher]['translators']) ?
            count($this->scraper_data[$this->index_scraper]['fetchers'][$this->index_fetcher]['translators']) : 0;
    }

    /**
     * return a count processors
     */
    public function countProcessors() {
        return isset($this->scraper_data[$this->index_scraper]['processors']) ?
            count($this->scraper_data[$this->index_scraper]['processors']) : 0;
    }

    /**
     * returns the current fetcher
     * it will only returns it's attributes not include translators.
     */
    public function getFetcher() {
        if (is_null($this->scraper_data)) {
            $this->get();
        }

        return [
            'id' => $this->scraper_data[$this->index_scraper]['fetchers'][$this->index_fetcher]['id'],
            'properties' => $this->scraper_data[$this->index_scraper]['fetchers'][$this->index_fetcher]['properties']
        ];
    }

    /**
     * jump to next fetcher
     * return the id of the fetcher (not index)
     */
    public function nextFetcher() {

        if (isset($this->scraper_data[$this->index_scraper]['fetchers'][$this->index_fetcher +1])) {
            $this->index_fetcher++;
            $this->index_translator = 0;

            return $this->scraper_data[$this->index_scraper]['fetchers'][$this->index_fetcher]['id'];
        }

        return null;
    }

    /**
     * returns current translator for the fetcher we are on
     */
    public function getTranslator() {
        if (is_null($this->scraper_data)) {
            $this->get();
        }

        $_idx = 0;
        foreach ($this->scraper_data[$this->index_scraper]['fetchers'][$this->index_fetcher]['translators'] as $key => $translator) {
            if ($this->index_translator == $_idx) {
                return [$key => $translator];
            }
            $_idx++;
        }

        return null;
    }

    /**
     * Returns current translator index
     *
     * @return int
     */
    public function getTranslatorIndex()
    {
        return $this->index_translator;
    }

    /**
     * jump to next translator
     * returns the  index  id  of  it (since we do not have ids for translators)  translator
     */
    public function nextTranslator()
    {
        if (isset(array_keys($this->scraper_data[$this->index_scraper]['fetchers'][$this->index_fetcher]['translators'])[$this->index_translator +1])) {
            $this->index_translator++;

            return $this->index_translator;
        }

        return null;
    }

    /**
     * Gives the user an option to rewind translators
     */
    public function rewindTranslators()
    {
        return $this->index_translator = 0;
    }

    /**
     * same as all others
     */
    public function getProcessor() {
        if (is_null($this->scraper_data)) {
            $this->get();
        }

        return isset($this->scraper_data[$this->index_scraper]['processors'][$this->index_processor]) ?
            $this->scraper_data[$this->index_scraper]['processors'][$this->index_processor] :
            null;
    }

    /**
     * same as all others
     */
    public function nextProcessor()
    {
        if (isset($this->scraper_data[$this->index_scraper]['processors'][$this->index_processor +1])) {
            $this->index_processor++;

            return $this->scraper_data[$this->index_scraper]['processors'][$this->index_processor]['id'];
        }

        return null;
    }


    /**
     * Saves the results to the db and returns the amount successfully saved
     *
     * @param $items
     * @param bool $save_to_adserver
     * @return int
     * @throws \Exception
     */
    public function saveResults($items, $save_to_adserver = false)
    {
        $rows_saved = 0;
        $parsed = [];
        $save_to_new_adserver = $this->scraper_data[$this->index_scraper]['properties']['save_to_new_adserver'] ?? false;
        $filtered_track_results = $items;
        $tp_logger = TpLogger::getInstance();

        if ($save_to_new_adserver) {
            $filtered_track_results = $this->filterExistingTrackTokens($items, $tp_logger);
            $tp_logger->info(['Tokens filtered out from items:' => collect($items)->pluck('trx_id')->except(collect($filtered_track_results)->pluck('trx_id')->toArray())->toArray()]);
            if ($filtered_track_results === false) {
                $tp_logger->error(['Error filtering tokens. Could not complete action.']);
                // Failed to return a response
                return 0;
            }
        }

        foreach ($filtered_track_results as $item) {
            $unix_timestamp = strtotime($item['date']);

            // Validates the current item's attributes
            if ($unix_timestamp === false) {
                $tp_logger->warning(["failed row: " => ["failed row: " => $item, "Invalid columns: " => 'date']]);

                continue;
            }

            $parsed = [
                'scraper_id' => $this->scraper_data[$this->index_scraper]['id'],
                'advertiser_id' => $this->scraper_data[$this->index_scraper]['properties']['advertiser_id'],

                'date' => date("Y-m-d H:i:s v", $unix_timestamp),
                'token' => $item['token'],
                'event' => $item['event'],
                'trx_id' => $item['trx_id'],
                'io_id' => $item['io_id'],
                'commission_amount' => $item['commission_amount'],
                'amount' => $item['amount'],
                'currency' => $item['currency'],
            ];

            if ($save_to_adserver) {
                $rows_saved += $this->saveResultsToAdserver($item + ['new_connection' => $save_to_new_adserver]);
            }
        }

        return $rows_saved;
    }

    /**
     * Decides which run time is earliest between the two:
     * 1. last log run time
     * 2. edge of the date span from any of the current scraper's fetcher's properties if exists
     *
     * @return false|string
     */
    private function determineEarliestRunTimeDate()
    {
        $scraper_id = $this->getScraper()['id'] ?? 2;

        // Gets the last valid log run time for the current scraper
        $earliest_date = null;
        // Race between the log earliest date and the shortcodes span
        $this->raceEarliestDateValue($earliest_date, $scraper_id);

        return $earliest_date;
    }

    /**
     * Decides if any shortcode edge date span value is earlier than the given date
     *
     * @param $earliest_date
     * @param $scraper_id
     */
    private function raceEarliestDateValue(&$earliest_date, $scraper_id)
    {
        $fetcher_properties_values = ScraperFetcherProperties::whereIn('fetcher_id',
            ScraperFetcher::where('scraper_id', $scraper_id)->get()->pluck('id')->toArray()
        )->get()->pluck('value')->toArray();

        $sc_date_strings_arr = array_filter($fetcher_properties_values, function ($fetcher_value) {
            return preg_match('/span=/', $fetcher_value);
        });

        if (empty($sc_date_strings_arr)) {
            return;
        }

        $dates_arr = [];
        foreach ($sc_date_strings_arr as $sc_date_str) {
            // Matches the date format that is being specified in the code
            preg_match('/format=(\\"|\')(.*?)(\\"|\')/', $sc_date_str, $match);
            // The index "2" refers to the second matching group we specified above
            $format = $match[2] ?? 'Y-m-d';
            preg_match('/\[[^\]]+span=(\\"|\')(.*?)(\\"|\')\]/', $sc_date_str, $match);
            $matched_date = $match[0] ?? '[date]';
            $compiled_date = $this->compileShortCode($matched_date);
            // converts shortcode span to unix timestamp
            $_date_from_format = date_create_from_format($format, $compiled_date);
            if ($_date_from_format === false) {
                continue;
            }
            $compiled_unix_timestamp = $_date_from_format->format('U');
            $dates_arr[$compiled_unix_timestamp] = true;
        }


        if (empty($dates_arr)) {
            $earliest_date = null;
        } else {
            // update max_span to the earliest date between $compiled_unix_timestamp and $max_span
            $min_span_timestamp = min(array_keys($dates_arr));
            $earliest_date = date('Y-m-d H:i:s', $min_span_timestamp);
        }
    }

    /**
     * Returns a string of the last valid run time
     *
     * @param $scraper_id
     * @return false|string
     */
    private function extractLastValidLogRunTime($scraper_id)
    {
        return Log::query()->where('task_id', '=', $scraper_id)
            ->where('error_type', '=', 'info')
            ->where('updated_at', '<', date('Y-m-d H:i:s', strtotime('-2 minutes')))
            ->orderBy('updated_at', 'desc')->limit(1)
            ->get()->first()->toArray()['updated_at'] ?? date('Y-m-d H:i:s', strtotime('-10 days'));
    }

    /**
     * Compiles the shortcodes lib value
     *
     * @param $short_code_str
     * @return array|bool|mixed
     */
    private function compileShortCode($short_code_str)
    {
        $sc_lib = new ShortCodesLib();
        $shortcode_result = $sc_lib->setShortCodes($short_code_str) ?? false;

        return $shortcode_result;
    }

    /**
     * Filter the existing track tokens out of the $items array
     *
     * @param $items
     * @param $tp_logger
     * @return mixed
     */
    private function filterExistingTrackTokens($items, $tp_logger)
    {
        $earliest_date = $this->determineEarliestRunTimeDate();
        $tokens_arr = collect($items)->unique('trx_id')->pluck('trx_id')->toArray();

        $filter_response = (new ScraperLib())->retrieveFilteredTokens($tokens_arr, $earliest_date);
        // Handle response error
        if (!$filter_response['status']) {
            $tp_logger->error(['Error returned from new out filtering action:' => $filter_response['message']]);
            return false;
        }

        $tp_logger->info(['lastRunTime sent to new out:' => $earliest_date]);

        $filtered_items = collect($items)->whereIn('trx_id', $filter_response['data'])->unique('trx_id')->toArray();

        return $filtered_items;
    }

    /**
     * validates the result set using a set of rules
     *
     * @param $rules
     * @param $result_set
     * @return string
     */
    private function validateResultsFormat($rules, $result_set)
    {
        $validator = Validator::make($result_set, $rules);
        $invalid_columns = implode(',', array_keys($validator->invalid()));

        if ($validator->fails()) {
            $tp_logger = TpLogger::getInstance();
            $tp_logger->warning(["failed row: " => ["failed row: " => $result_set, "Invalid columns: " => $invalid_columns]]);

            return false;
        }

        return true;
    }


    /**
     * Save Results To Ad Server
     * Legacy feature
     *
     * @param $item
     *
     * @return bool
     */
    private function saveResultsToAdserver($item)
    {
        $_now = \DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        $legacy_parse = [
            'trackInfo' => [
                'tokenId' => "",
                'track_type' => "event",
                'date' => $item['date'],
                'timestamp' => $_now->format("Y-m-d\TH:i:s.u\Z"),
            ],
            'params' => [
                'commission_amount' => $item['commission_amount'],
                'currency' => $item['currency'],
                'amount' => $item['amount'],
                'ioId' => $item['io_id'],
            ],
            'advertiserName' => NULL,
            'trxId' => $item['trx_id'],
            'eventName' => strtolower($item['event']),
            'source_token' => $item['token'],
        ];

        if (!empty($item['new_connection'])) {
            $response = (new ScraperLib())->create(['data' => json_encode($legacy_parse)]);

            // Checks which response data was retrieved
            if (!empty($response['data'])) {
                return $response['data'] ? 1 : 0;
            }

            // Failed to return a response
            return 0;
        }


        return DB::connection('out')->table('queue')->insert(['data' => json_encode($legacy_parse)]);
    }


    /**
     * normalize data from database
     * 
     * @return array
     */
    private function normalizeDataFromBackend() {
        $scrapers = $this->query->get();

        $result = [];
        foreach ($scrapers as $scraper) {
            $scr['id'] = $scraper->id;
            $scr['properties'] = $this->toKeyValueScraper($scraper);

            $fetchers = ScraperFetcher::where('scraper_id', $scraper->id)->orderBy('priority')->get()->toArray();

            foreach ($fetchers as $key => $fetcher) {
                $scr['fetchers'][$key]['id'] = $fetcher['id'];
                $scr['fetchers'][$key]['properties'] = $this->buildFetcherProperties($fetcher);
                $scr['fetchers'][$key]['translators'] = $this->buildTranslatorsPropertiesFromDB($fetcher['id']);
            }
            $scr['processors'] = $this->buildProcessorsParams($scraper->id);

            $result[] = $scr;
        }

        return $this->scraper_data = $result;
    }

    /**
     * Builds a proper processors array (from database)
     *
     * @param $scraper_id
     * @return array
     */
    private function buildProcessorsParams($scraper_id)
    {
        $result = [];
        $processors = ScraperProcessor::where('scraper_id', $scraper_id)->get()->toArray();

        if (empty($processors)) {
            return $result;
        }

        $idx = 0;
        foreach ($processors as $processor) {
            foreach ($processor as $key => $val) {
                if (in_array($key, ['id', 'scraper_id', 'active', 'rule'])) {
                    $result[$idx][$key] = $val;
                } else {
                    $result[$idx]['properties'][$key] = $val;
                }
            }
            $idx++;
        }

        return $result;
    }

    /**
     * Builds a proper processors array (from frontend form)
     *
     * @param $scraper_id
     * @return array
     */
    private function buildProcessorsParamsFront($scraper_id) {
        $result = [];

        if (!isset($this->params['processors'])) {
            return $result;
        }

        $processors = $this->params['processors'];

        $idx = 0;
        foreach ($processors as $processor) {
                $result[] = [
                    'id' => 0,
                    'scraper_id' => $scraper_id,
                    'properties' => [
                        'date' => isset($processor['date']) ? $processor['date'] : null,
                        'token' => isset($processor['token']) ? $processor['token'] : null,
                        'event' => isset($processor['event']) ? $processor['event'] : null,
                        'trx_id' => isset($processor['trx_id']) ? $processor['trx_id'] : null,
                        'io_id' => isset($processor['io_id']) ? $processor['io_id'] : null,
                        'commission_amount' => isset($processor['commission_amount']) ? $processor['commission_amount'] : null,
                        'amount' => isset($processor['amount']) ? $processor['amount'] : null,
                        'currency' => 'USD'
                    ],
                    'rule' => 'Empty',
                    'active' => false
                ];

            foreach ($processor as $key => $val) {
                if (in_array($key, ['id', 'scraper_id', 'active', 'rule'])) {
                    $result[$idx][$key] = $val;
                } else {
                    if (!is_null($val) && $val != '') {
                        $result[$idx]['properties'][$key] = $val;
                    }
                }
            }
            $idx++;
        }

        return $result;
    }

    /**
     * get data from controller FetcherController::actions
     *
     * @return array
     */
    private function normalizeDataFromFrontend() {
        $ignored_client_fields = ['id', '_method', '_token', 'properties'];
        if (!isset($this->params['scraper']['id']) or is_null($this->params['scraper']['id'])) {
            throw new InvalidArgumentException('Scraper Id could not be found.');
        }

        if (!isset($this->params['fetchers']) or is_null($this->params['fetchers'])) {
            throw new InvalidArgumentException('Could not find fetchers.');
        }

        foreach ($this->params['scraper'] as $key => $value) {
            if ($key != 'id') {
                if (!in_array($key, $ignored_client_fields)) {
                    $results['properties'][$key] = $value;
                } elseif ($key == 'properties') {
                    $prop = clearAttributesArray($value['key'], $value['value']);
                    if ($prop) {
                        foreach ($prop as $k => $v) {
                            $results['properties'][$k] = $v;
                        }
                    }
                }
            } else {
                $results[$key] = $value;
            }
        }
//        dd($results, $this->params['scraper']);

        foreach ($this->params['fetchers'] as $key => $fetcher) {
            $results['fetchers'][$key] = $this->buildFetcherPropertiesFromForm($fetcher);
            $results['fetchers'][$key]['id'] = isset($fetcher_array['fetcher_id']) ? $fetcher['fetcher_id'] : rand(100000, 200000);
        }

        $results['processors'] = $this->buildProcessorsParamsFront($this->params['scraper']['id']);

        return $this->scraper_data = [$results];
    }

    /**
     * convert scraper object to array key value
     * only to database
     * @param $data
     * @return null
     */
    private function toKeyValueScraper($data) {
        $properties = null;

        foreach ($data as $key => $value) {
            if (!in_array($key, $this->ignore_scraper_keys)) {
                $properties[$key] = $value;
            }
        }

        $advanced = ScraperAdvanceProperties::where('scraper_id', isset($data->id) ? $data->id : $data['id'])->get()->toArray();
        foreach ($advanced as $ad) {
            $properties[$ad['key']] = $ad['value'];
        }

        return $properties;
    }


    /**
     * convert fetcher object to array key value
     * only to database
     * @param $data
     * @return null
     */
    private function buildFetcherProperties($data) {
        $properties = [];
        $properties['form_params'] = [];
        $properties['headers'] = [];
        $properties['spreadsheet_info'] = [];

        foreach ($data as $key => $value) {
            if (!in_array($key, $this->igonre_fetcher_keys)) {
                $properties[$key] = $value;
            }
        }

        $form_params = ScraperFetcherProperties::where('fetcher_id', $data['id'])
            ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_FORM_PARAMS)->get()->toArray();

        foreach ($form_params as $item) {
            $properties['form_params'][$item['key']] = $item['value'];
        }

        $header = ScraperFetcherProperties::where('fetcher_id', $data['id'])
            ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_HEADER)->get()->toArray();

        foreach ($header as $item) {
            $properties['headers'][$item['key']] = $item['value'];
        }

        $query = ScraperFetcherProperties::where('fetcher_id', $data['id'])
            ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_QUERY)->get()->toArray();

        foreach ($query as $item) {
            $properties['query'][$item['key']] = $item['value'];
        }

        $spreadsheet = ScraperFetcherProperties::where('fetcher_id', $data['id'])
            ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_SPREADSHEET_INFO)->get()->toArray();

        foreach ($spreadsheet as $item) {
            $properties['spreadsheet_info'][$item['key']] = $item['value'];
        }

        return $properties;
    }

    /**
     * Builds the fetcher's translator properties
     *
     * @param $fetcher_id
     * @return array|null
     */
    private function buildTranslatorsPropertiesFromDB($fetcher_id)
    {
        $properties = null;

        $translators = ScraperFetcherProperties::where('fetcher_id', $fetcher_id)
            ->where('type_field', ScraperFetcher::SCRAPER_PROPERTIES_TYPE_TRANSLATOR)->get()->toArray();

        foreach ($translators as $item) {
            $properties[$item['key']] = $item['value'];
        }

        return $properties;
    }

    /**
     * convert fetcher string to array key value
     * received from a form submission
     *
     * @param $form_params
     * @return null
     */
    private function buildFetcherPropertiesFromForm($form_params)
    {
        $results['id'] = isset($form_params['fetcher_id']) ? $form_params['fetcher_id'] : $form_params['id'];
        $results['order'] = isset($form_params['order']) ? $form_params['order'] : $form_params['priority'];

        $results['properties']['headers'] = clearAttributesArray(@$form_params['header']['key'], @$form_params['header']['value']);
        $results['properties']['form_params'] = clearAttributesArray(@$form_params['body']['key'], @$form_params['body']['value']);
        $results['properties']['query'] = clearAttributesArray(@$form_params['query']['key'], @$form_params['query']['value']);
        $results['properties']['spreadsheet_info'] = clearAttributesArray(@$form_params['spreadsheet_info']['key'], @$form_params['spreadsheet_info']['value']);
        $results['translators'] = clearAttributesArray(@$form_params['translator']['key'], @$form_params['translator']['value']);

        foreach ($form_params as $key => $value) {
            if (!is_array($value)) {
                $results['properties'][$key] = $value;
            }
        }

        return $results;
    }
}