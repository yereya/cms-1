<?php

namespace App\Entities\Repositories\Reports;

use App;
use App\Entities\Models\Reports\Queries\Filter;
use App\Entities\Models\Reports\Queries\FilterProperty;
use App\Entities\Models\Reports\Queries\UserFilter;
use Illuminate\Database\ConnectionResolver;
use Illuminate\Database\Eloquent\Collection;


class FiltersRepo
{

    private static $ajax_modal_route = 'reports.filters.ajax-modal';


    /**
     * Save
     *
     * @param array $params
     * @param $user_id
     *
     * @return mixed
     * @throws \Exception
     */
    public static function save(array $params, $user_id)
    {
        $filter_id = self::createFilter($user_id, $params['query_id'], $params['filter_name']);

        if (!$filter_id) {
            throw new \Exception('Filter could not be created.');
        }

        self::saveFilterProperties($filter_id, $params);

        return $filter_id;
    }


    /**
     * Get Saved Filter Fields
     *
     * @param $filter_id
     *
     * @return null|Collection
     */
    public static function getSavedFilterFields($filter_id)
    {
        if (is_null($filter_id)) {
            return null;
        }

        $filter_fields = Filter::whereId($filter_id)
            ->with('properties.field')->first();
        $property_fields = $filter_fields->properties;

        $field_params['where'] = $property_fields->filter(function ($item) {
            return isset($item->field->clause_type) && $item->field->clause_type == 'where';
        });
        $field_params['having'] = $property_fields->filter(function ($item) {
            return isset($item->field->clause_type) && $item->field->clause_type == 'having';
        });
        $field_params['select'] = $property_fields->filter(function ($item) {
            return isset($item->field->clause_type) && $item->field->clause_type == 'select';
        })->each(function ($item) {
            //return collection by query_field_id
            $item->id = $item->query_field_id;
        });
        $field_params['order_by'] = $property_fields->filter(function ($item) {
            return isset($item->field->clause_type) && $item->field->clause_type == 'order_by';
        });
        $field_params['group_by'] = $property_fields->filter(function ($item) {
            return isset($item->field->clause_type) && $item->field->clause_type == 'group_by';
        });
        $field_params['variable'] = $property_fields->filter(function ($item) {
            return isset($item->field->clause_type) && $item->field->clause_type == 'variable';
        });

        return $field_params;
    }


    /**
     * Get Saved Params
     * Will build the params used for the dropdown-buttons.blade.php view
     *
     * @param $user_id
     * @param $report_query_id
     *
     * @return array
     */
    public static function getSavedFilters($user_id, $report_query_id)
    {
        $user_filters = UserFilter::whereUserId($user_id);

        return Filter::whereIn('id', $user_filters->pluck('filter_id'))
            ->whereQueryId($report_query_id)
            ->get();
    }


    /**
     * @param $filter_id
     *
     * @return mixed
     */
    public static function deleteFilter($filter_id)
    {
        self::deleteFilterProperties($filter_id);
        return Filter::where('id', $filter_id)->delete();
    }


    /**
     * Create Filter
     *
     * @param $user_id
     * @param $report_query_id
     * @param $filter_name
     *
     * @return mixed
     * @internal param $params
     */
    private static function createFilter($user_id, $report_query_id, $filter_name)
    {
        $new_filter = [
            'query_id' => $report_query_id,
            'name' => $filter_name,
        ];

        $new_filter = new Filter($new_filter);

        if (!$new_filter->save()) {
            App::abort(500, 'Error');
        }

        // Create the user filter relationship
        $user_filter = new UserFilter();
        $user_filter->fill([
            'user_id' => $user_id,
            'filter_id' => $new_filter->id
        ]);
        $user_filter->save();

        return $new_filter->id;
    }


    /**
     * Save Filter Properties
     *
     * @param $filter_id
     * @param $properties
     *
     * @return bool
     */
    private static function saveFilterProperties($filter_id, $properties)
    {
        self::deleteFilterProperties($filter_id);

        $new_properties = [];

        foreach ($properties as $key => $property) {
            // Make sure we only go to the properties which have data in them
            // which means they are arrays
            if (!is_array($property)) {
                continue;
            }

            switch ($key) {
                case 'date_range' :
                    foreach ((array)$property as $three_property_filter) {
                        if (is_numeric($three_property_filter[0])) {
                            $new_properties[] = self::buildDateRangeRecord($filter_id, $three_property_filter);
                        }
                    }
                    break;

                case 'where' :
                case 'having' :
                    $order = 100;
                    foreach ((array)$property as $three_property_filter) {
                        if ($three_property_filter) {
                            $new_properties[] = [
                                'filter_id' => $filter_id,
                                'query_field_id' => $three_property_filter['column'],
                                'operator' => $three_property_filter['operator'],
                                'value' => $three_property_filter['value'],
                                'order' => $order
                            ];

                            $order += 10;
                        }
                    }
                    break;

                case 'order_by' :
                    $order = 100;
                    foreach ((array)$property as $two_property_filter) {
                        if (is_numeric($two_property_filter[0])) {
                            $new_properties[] = [
                                'filter_id' => $filter_id,
                                'query_field_id' => $two_property_filter[0],
                                'operator' => null,
                                'value' => isset($two_property_filter[1]) ? $two_property_filter[1] : null,
                                'order' => $order
                            ];

                            $order += 10;
                        }
                    }
                    break;

                case 'group_by' :
                case 'select' :
                    $order = 100;
                    foreach ((array)$property as $two_property_filter) {
                        if (is_numeric($two_property_filter[0])) {
                            $new_properties[] = [
                                'filter_id' => $filter_id,
                                'query_field_id' => $two_property_filter[0],
                                'operator' => null,
                                'value' => null,
                                'order' => $order
                            ];

                            $order += 10;
                        }
                    }
                    break;
            }
        }

        if (count($new_properties)) {
            return FilterProperty::insert($new_properties);
        }

        return false;
    }


    /**
     * Builds the Rate Range Record
     * Adds shortcodes when required
     *
     * @param $filter_id
     * @param $properties
     *
     * @return mixed
     */
    private static function buildDateRangeRecord($filter_id, $properties)
    {
        $new_record['filter_id'] = $filter_id;
        $new_record['query_field_id'] = $properties[0];
        $new_record['operator'] = null;
        $new_record['order'] = null;

        // If the 2nd property is not passed
        // used the direct value right away
        if (!isset($properties[2])) {
            $new_record['value'] = isset($properties[1]) ? $properties[1] : null;
            return $new_record;
        }

        switch ($properties[2]) {
            case 'Today' :
                $new_record['value'] = '[date] [date]';
                break;

            case 'Last 7 Days' :
                $new_record['value'] = '[date span="-7 day"] [date span="yesterday"]';
                break;

            case 'Last 14 Days' :
                $new_record['value'] = '[date span="-14 day"] [date span="yesterday"]';
                break;

            case 'This Month' :
                $new_record['value'] = '[date span="first day of this month"] [date]';
                break;

            case 'Last 30 days' :
                $new_record['value'] = '[date span="-30 day"] [date span="yesterday"]';
                break;

            case 'Last Month' :
                $new_record['value'] = '[date span="first day of -1 month"] [date span="last day of -1 month"]';
                break;

            case 'Last 3 Months' :
                $new_record['value'] = '[date span="first day of -3 month"] [date span="last day of -1 month"]';
                break;

            case 'This Year' :
                $new_record['value'] = '[date span="first day of this year"] [date]';
                break;

            default :
                $new_record['value'] = isset($properties[1]) ? $properties[1] : null;
        }

        return $new_record;
    }


    /**
     * @param $filter_id
     *
     * @return mixed
     */
    private
    static function deleteFilterProperties($filter_id)
    {
        return FilterProperty::where('filter_id', $filter_id)->delete();
    }


}