<?php

namespace App\Entities\Repositories;

use App\Entities\Models\Sites\SiteSetting;
use App\Entities\Repositories\Sites\SiteSettingDefaultRepo;
use App\Facades\SiteConnectionLib;

class SiteSettingRepo extends Repository
{
    const MODEL = SiteSetting::class;

    /**
     * @var
     */
    private $site;

    /**
     * SiteSettingsRepo constructor.
     */
    public function __construct()
    {
        $this->site = SiteConnectionLib::getSite();
    }

    /**
     * Get Page Defaults
     *
     * @param $key
     *
     * @return mixed
     */
    public function getByKey($key)
    {
        return $this->getByKeys([$key])
            ->keyBy('key')
            ->first();
    }

    /**
     * Get By Keys
     *
     * @param array $keys
     *
     * @return mixed
     */
    public function getByKeys(array $keys)
    {
        return $this->query()
            ->where('site_id', $this->site->id)
            ->whereIn('key', $keys)
            ->get();
    }

    /**
     * Insert New Default Settings
     *
     * @param $site_id
     * @param $type
     * @param $default_settings
     */
    public function insertNewDefaultSettings($site_id, $type, $default_settings)
    {
        foreach ($default_settings as $param) {
            $new_setting          = $this->model()
                ->fill($param->getAttributes());
            $new_setting->site_id = $site_id;
            $new_setting->type    = $type;
            $new_setting->save();
        }
    }

    /**
     * @param $site
     */
    public function setDefaultSettings($site)
    {
        $default = (new SiteSettingDefaultRepo())->getAll();

        $default->each(function ($item) use ($site) {
            $model          = $this->resolveModel($site, $item);
            $model->site_id = $site->id;
            $model->fill($item->toArray());
            $model->save();
        });
    }

    /**
     * Resolve Model
     *
     * @param $site
     * @param $item
     *
     * @return \Illuminate\Database\Eloquent\Model|null|static
     */
    private function resolveModel($site,$item)
    {
        $model = $this->query()
            ->where('site_id', $site->id)
            ->where('name', $item->name);

        return $model->first() ?? $this->model();
    }
}