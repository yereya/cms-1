<?php

namespace App\Entities\Repositories\Users;

use App\Entities\Models\Users\User;
use App\Entities\Repositories\Repository;

/**
 * Class UserRepo
 *
 * @package App\Entities\Repositories\Users
 */
class UserRepo extends Repository
{
    /**
     *
     */
    const MODEL = User::class;

    /**
     * Get User Accounts
     *
     * @param null $id
     *
     * @return mixed
     */
    public function getActiveAccounts($id = null)
    {
        $user_id = $id ?? auth()->user()->id;
        $user    = $this->model()->find($user_id);

        return $user->accounts()
            ->active()
            ->pluck('name','source_account_id');
    }

    /**
     * @param $accounts_id
     *
     * @return \Illuminate\Support\Collection
     */
    public function getEmailsByAccountsId($accounts_id)
    {
        $query  = $this->query();
        $emails = $query->whereHas('accounts', function ($query) use ($accounts_id) {
            $query->from('bo.accounts')
                ->whereIn('account_id', $accounts_id);
        })->pluck('email');

        return $emails;
    }

    /**
     * @param $user
     * @param $service
     *
     * @return array|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Query\Builder[]|\Illuminate\Support\Collection
     */
    public function getUserPermissionsByService($user, $service)
    {
        $model  = $this->query();
        $email  = $user->email;
        $result = $model
            ->selectRaw('sur.site_id, up.type, up.name')
            ->from('users')
            ->join('site_user_role As sur', 'users.id', '=', 'sur.user_id')
            ->join('user_permission_role As upr', 'sur.role_id', '=', 'upr.role_id')
            ->join('user_permissions As up', 'upr.permission_id', '=', 'up.id')
            ->where('users.email', $email)
            ->where('up.name', 'LIKE', $service . '.%')
            ->get();

        if (!count($result)) {
            return collect();
        }

        return $result;
    }

    /**
     * @param $email
     *
     * @return mixed
     */
    public function getRoleByEmail($email)
    {
        $query = $this->query();

        return $query
            ->where('email', $email)
            ->pluck('role')
            ->first();
    }

    /**
     * @param null $id
     *
     * @return
     */
    public function getAdvertiserIds($id = null){
        $user_id = $id ?? auth()->user()->id;
        $user    = $this->model()->find($user_id);

        return $user->accounts()->distinct('advertiser_id')->pluck('advertiser_id') ?? collect();
    }


}