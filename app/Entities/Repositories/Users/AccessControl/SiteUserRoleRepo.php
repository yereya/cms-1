<?php


namespace App\Entities\Repositories\Users\AccessControl;


use App\Entities\Models\Sites\Site;
use App\Entities\Models\Users\AccessControl\SiteUserRole;
use App\Entities\Models\Users\User;
use App\Entities\Repositories\Repository;

class SiteUserRoleRepo extends Repository
{
    const  MODEL = SiteUserRole::class;

    /**
     * Set User Permission
     *
     * @param Site $site
     *
     * @return SiteUserRole
     */
    public function set(Site $site): SiteUserRole
    {
        $user = auth()->user();
        $role = $user->roles()->first()->id;

        $params = [
            "site_id" => $site->id,
            "user_id" => $user->id,
            "role_id" => $role
        ];

        $model = $this->model();
        $model->fill($params);
        $model->save();

        return $model;
    }

    /**
     * @param User $user
     * @param Site $site
     *
     * @return bool
     */
    public function hasRole(User $user, Site $site)
    {
        $model = $this->query()
            ->where('user_id', $user->id)
            ->where('site_id', $site->id);

        return count($model->first()) ? true : false;
    }
}