<?php

namespace App\Entities\Repositories\Users\AccessControl;

use App\Entities\Models\Users\AccessControl\Role;
use App\Entities\Repositories\Repository;

class RoleRepo extends Repository
{

    const MODEL = Role::class;


    /**
     * Find Role By Name
     *
     * @param string $name
     *
     * @return mixed
     */
    public function findByName(string $name){
        return $this->model()->findByName($name);
    }

}