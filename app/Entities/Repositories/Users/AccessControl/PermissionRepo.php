<?php


namespace App\Entities\Repositories\Users\AccessControl;

use App\Entities\Models\Users\AccessControl\Permission;
use App\Entities\Repositories\Users\UserRepo;
use Illuminate\Config\Repository;


/**
 * Class PermissionRepo
 *
 * @package app\Entities\Repositories
 */
class PermissionRepo extends Repository
{

    /**
     *
     */
    const MODEL = Permission::class;


    /**
     * @param $service
     *
     * @return array
     */
    public function getAllServicePermissionsForUser($service)
    {
        $user           = auth()->user();
        $userPermission = (new UserRepo())->getUserPermissionsByService($user, $service);
        $permissions    = $this->getGroupedPermissionsBySiteId($userPermission);

        return $permissions;
    }

    /**
     * @param $permissions
     *
     * @return array|\Illuminate\Support\Collection
     */
    private function getGroupedPermissionsBySiteId($permissions)
    {
        $grouped_permissions = collect();

        if (!count($permissions)) {
            return collect();
        }

        $permissions->each(function ($record) use ($grouped_permissions) {
            if (!$grouped_permissions->has($record->site_id)) {
                $grouped_permissions->put($record->site_id, collect());
            }
            $grouped_permissions[$record->site_id]->put($record->name . '.' . $record->type, true);
        });

        return $grouped_permissions->toArray();
    }
}