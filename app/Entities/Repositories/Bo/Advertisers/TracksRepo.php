<?php namespace App\Entities\Repositories\Bo\Advertisers;


use App\Entities\Models\Dwh\DwhFactTracks;
use App\Entities\Models\Dwh\DwhFactTracksMirror;
use App\Libraries\RecordManager\ConnectionManagerFactory;
use App\Libraries\RecordManager\Records\AdvertiserTracksConnection;
use App\Libraries\RecordManager\Records\BaseConnection;
use App\Libraries\TpLogger;
use DB;
use Doctrine\DBAL\Connection;
use Illuminate\Support\Facades\Session;

/**
 * Class TracksRepo
 * @package App\Entities\Repositories\Bo\Advertisers
 */
class TracksRepo
{
    private static $out_tracks_table = 'tracks';
    private static $out_queue_table = 'queue';
    private static $dwh_fact_tracks_table = 'dwh_fact_tracks';
    private static $dwh_fact_tracks_mirror_table = 'dwh_fact_tracks_mirror';
    private static $dwh_deleted_table = 'dwh_deleted_tracks_rows';
    private static $dwh_high_water_marks = 'high_water_mark_tbl';
    private static $logger;
    private static $connection_handler = 'AdvertiserTracks';
    private static $connection_indexes = [
        'out' => 0,
        'putin' => 1
    ];

    private static $exclude_columns_from_updating = [
        'created_at',
        'updated_at',
        'src_updated',
        'src_created'
    ];

    private static $connection_params = [
        'io_id' => ['ioId', 'brand_id'],
        'amount' => ['amount', 'amount'],
        'currency' => ['currency', 'orig_currency'],
        'base_commission_amount' => ['base_commission_amount', 'base_commission_amount'],
        'commission_amount' => ['commission_amount', 'commission_amount'],
        'timestamp' => ['timestamp', 'timestamp'],
        'page' => ['page' => 'page'],
        'sid' => ['sid', 'sid'],
        'network' => ['network', 'network'],
        'device' => ['display_device','display_device']
    ];

    private static $dwh_deleted_rows_fields = [

        'track_id',
        'id',
        'timestamp',
        'source_track',
        'event',
        'internal',
        'base_commission_amount',
        'commission_amount',
        'currency',
        'amount',
        'sid',
        'device',
        'keyword',
        'placement',
        'network',
        'ad_id',
        'clkid',
        'tokenIP',
        'action',
        'dynamic_list',
        'page',
        'position',
        'query_string',
        'dynamicp',
//        'adposition',
//        'match_type',
        'match_type_four',
        'direct_in_click',
        'direct_out_click',
        'click_in',
        'click_out',
        'click_out_unique',
        'lead',
        'sale',
        'sale_unique',
        'install',
        'cancel_sale',
        'adjustment',
        'canceled_lead',
//        'vwr_viewerIndex',
        'vwr_viewerId',
        'vwr_browser',
        'vwr_device',
        'vwr_os',
        'vwr_country',

//        'vwr_ip',
        'campaign_id',
        'campaign_name',
        'brand_id',
        'brand_name',
        'advertiser_id',
        'advertiser_name',
        'landingpage_id',
        'landingpage_name',
        'placement_id',
        'placement_name',
        'stats_date_tz',
        'stats_time_tz',
        'source_click_date_tz',
        'source_click_dateonly_tz',
        'created_at',
        'updated_at',

        'pub_ad_group_id',
        'pub_campaign_id',
        'pub_target_id',
        'pub_top_vs_other',
        'pub_account_id',
        'pub_account_name',
        'pub_ad_group_name',
        'pub_campaign_name',

        'record_status',
        'gid_var',
        's_advertiser_id',
        'clkout_brand_unq',
        'browser_version',

        'src_created',
        'src_updated',
        'betterment_validation',
        'click_type',
        'page_version',
        'list_id',
        'impression_token',
        'test_id',
        'test_description',
        'lineup_id',
        'lineup_label'

    ];

    private static $high_water_mark_table_names = 'dash_dailyStats';

    private static $dwh_fact_tracks_fields = [
        'track_id',
        'id',
        'timestamp',
        'source_track',
        'type',
        'event',
        'pid',
        'lpid',
        'campaignId',
        'internal',
        'base_commission_amount',
        'commission_amount',
        'currency',
        'base_amount',
        'amount',
        'sid',
        'device',
        'keyword',
        'placement',
        'network',
        'ad_id',
        'gclid',
        'msclkid',
        'clkid',
        'tokenIP',
        'source_click_date',
        'action',
        'dynamic_list',
        'page',
        'position',
        'query_string',
        'dynamicp',
//        'adposition',
        'match_type',
        'match_type_four',
        'src_created',
        'src_updated',
        'direct_in_click',
        'direct_out_click',
        'click_in',
        'click_out',
        'click_out_unique',
        'lead',
        'sale',
        'sale_unique',
        'install',
        'cancel_sale',
        'canceled_lead',
        'adjustment',
//        'vwr_viewerIndex',
        'vwr_viewerId',
        'vwr_browser',
        'vwr_device',
        'vwr_os',
        'vwr_country',
        'vwr_city',
//        'vwr_ip',
        'campaign_id',
        'campaign_name',
        'brand_id',
        'brand_name',
        'advertiser_id',
        'advertiser_name',
        'landingpage_id',
        'landingpage_name',
        'placement_id',
        'placement_name',
        'stats_date_tz',
        'stats_time_tz',
        'source_click_date_tz',
        'source_click_dateonly_tz',
        'updated_at',
        'created_at',
        'pub_ad_group_id',
        'pub_campaign_id',
        'pub_target_id',
        'pub_top_vs_other',
        'pub_account_id',
        'pub_account_name',
        'pub_ad_group_name',
        'pub_campaign_name',
        'pub_date',
        'record_status',
        'gid_var',
        'ga_token',
        'optout',
        'popup',
        'banner',
        's_advertiser_id',
        'clkout_brand_unq',
        'browser_version'
    ];

    /**
     * @var Connection $out_connection
     */
    private static $out_connection;

    /**
     * @var Connection $dwh_connection
     */
    private static $dwh_connection;

    /**
     * connection for update/insert/delete
     * @var BaseConnection
     */
    protected static $connection;

    /**
     * connection hinter
     * @var int
     */
    protected static $connection_idx;

    /**
     * Insert
     *
     * @param $items
     *
     * @return integer
     */
    public static function insert($items)
    {
        self::$logger = TpLogger::getInstance();
        self::$logger->info(['Valid tracks to be add', $items]);

        $saved = [];

        try {
            self::$connection = self::getConnectionManager();
            self::$connection_idx = self::$connection_indexes[self::$connection->getConnection()->getName()];
            foreach ((array)$items as $key => $item) {
                if (self::insertSingle($item)) {
                    $saved[] = $item;
                }
            }
            self::$connection->getConnection()->disconnect();
        } catch (\Exception $e) {
            self::$logger->error($e);
        }

        self::$logger->info(['Successfully added '. count($saved) .' tracks', $saved]);

        return count($saved);
    }


    /**
     * Update
     *
     * @param $items
     *
     * @return int
     */
    public static function update($items)
    {
        self::$logger = TpLogger::getInstance();
        self::$logger->info(['Valid tracks to be updated', $items]);

        $saved = [];

        try {
            self::$dwh_connection = DB::connection('dwh');
            self::$connection = self::getConnectionManager()->getConnection();
            self::$connection_idx = self::$connection_indexes[self::$connection->getName()];
            foreach ((array)$items as $item) {
                if (self::updateSingleItem($item)) {
                    $saved[] = $item;
                }
            }
            self::$connection->disconnect();
            self::$dwh_connection->disconnect();
        } catch (\Exception $e) {
            self::$logger->error($e);
            self::$connection->disconnect();
            self::$dwh_connection->disconnect();
        }

        // Log the saving process
        self::$logger->info(['Successfully updated '. count($saved) .' tracks', $saved]);

        return count($saved);
    }


    /**
     * Delete
     *
     * @param array $tokens
     *
     * @return int
     * @throws \Exception
     */
    public static function delete(array $tokens)
    {

        self::$logger = TpLogger::getInstance();
        self::$logger->info(['Valid tracks to be deleted', $tokens]);

        $connection = self::getConnectionManager()->getConnection();
        $connection_idx = self::$connection_indexes[$connection->getName()];

        $logger = self::$logger;

        //DWH connection for dwh_fact_tracks
        $builder_dwh = DwhFactTracks::whereIn('id',$tokens);
        $selected_tracks_filtered_by_fields = $builder_dwh->select(self::$dwh_deleted_rows_fields);

        //DWH connection for dwh_deleted_rows_tmp
        $dwh_connection = DB::connection('dwh');
        $delete_tbl = $dwh_connection->table(self::$dwh_deleted_table);
        self::$logger->info('Before  add tracks to deleted table');
        try{
            //insert all resulting track records into dwh_deleted_tracks_rows in dwh
            $selected_tracks_filtered_by_fields->each(function($track_record)use($delete_tbl,$logger){
                $result_array = $track_record->toArray();
                self::$logger->info('step 1 for tracks id : ' . $result_array['track_id']);
                $result_array['tracks_updated'] = $result_array['src_updated'];
                $result_array['tracks_created'] = $result_array['src_created'];
                $result_array['isDeleted'] = 1;
                $result_array['source'] = 'out';
                self::$logger->info('step 2 for tracks id : ' . $result_array['track_id']);
                $result_array = array_diff_key($result_array,array_flip(self::$exclude_columns_from_updating));
                self::$logger->info('step 3 for tracks id : ' . $result_array['track_id']);
                try{
                    if($result_array['track_id'] && $delete_tbl->select()->where('track_id','=',$result_array['track_id'])->get()->isEmpty()){
                        $delete_tbl->insert($result_array);
                    }
                }catch (\Exception $e){
                    self::$logger->info('Error inside delete tbl select: ' . $e);
                }
                self::$logger->info('step 4 for tracks id : ' . $result_array['track_id']);
            });
        }catch (\Exception $e){
            self::$logger->info('Error inside general delete : ' . $e);
        }
        self::$logger->info('After add tracks to deleted table');
        $dwh_connection->transaction(function () use ($dwh_connection,$tokens,$logger){

            // Delete from DWH
            $dwh_connection->table('dwh_fact_tracks')->whereIn('id', $tokens)->delete();
            // Delete from DWH_Mirror
            $dwh_connection->table('dwh_fact_tracks_mirror')->whereIn('id', $tokens)->delete();
        });

        self::$logger->info('Deleted from table dwh.dwh_fact_tracks and dwh.dwh_fact_tracks_mirror.');

        // We create the query this way because of laravel's inability to support db links
        $sql = sprintf("DELETE FROM `%s` WHERE `%s` IN ('%s')", self::$out_tracks_table, ['id', 'token'][$connection_idx], implode("', '", $tokens));
        $mrr = $connection->statement(\DB::raw($sql));
        if ($mrr) {
            self::$logger->info('Deleting from table mrr.tracks_dblink returned: successful');
        } else {
            self::$logger->error('Deleting from table mrr.tracks_dblink returned: failed');
        }

        $connection->disconnect();
        $dwh_connection->disconnect();


        return true;
    }


    /**
     * Get Tracks By Token
     *
     * @param array $tokens
     *
     * @return array|static[]
     */
    public static function getTracksByToken(array $tokens)
    {
        return \DB::connection('dwh')
            ->table(self::$dwh_fact_tracks_table)
            ->select('id as token_old',
                'event as event_old',
                'commission_amount as commission_amount_old',
                'base_commission_amount as base_commission_amount_old',
                'amount as amount_old',
                'timestamp as date_old',
                'currency as currency_old',
                'page as page_old',
                'sid as sid_old',
                'network as network_old',
                'device as display_device')
            ->whereIn('id', $tokens)
            ->get();
    }


    /**
     * Inserts a single track
     *
     * @param $item
     *
     * @return bool|void
     * @throws \Exception
     */
    private static function insertSingle($item)
    {
        $_item = filterAndSetParams([
            'commission_amount'      => 0,
            'base_commission_amount' => 0,
            'currency'               => 'USD',
            'trx_id'                 => null,
            'amount'                 => 0,
            'io_id'                  => '',
            'event'                  => null,
            'token'                  => null,
            'date'                   => null,
            'device'                 => null,
        ], $item);

        if (!self::validateInsertTrack($_item)) {
            return false;
        }

        $_now   = \DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
        $parsed = [
            'trackInfo'      => [
                'tokenId'    => "",
                'track_type' => "event",
                'date'       => date('Y-m-d 12:i:s', strtotime($item['date'])),
                'timestamp'  => $_now->format("Y-m-d\TH:i:s.u\Z"),
            ],
            'params'         => [
                'commission_amount'      => $_item['commission_amount'],
                'base_commission_amount' => $_item['base_commission_amount'],
                'currency'               => $_item['currency'],
                'amount'                 => $_item['amount'],
                'ioId'                   => $_item['io_id'],
                'device'                 => $_item['device'],
            ],
            'advertiserName' => null,
            'trxId'          => $_item['trx_id'],
            'eventName'      => strtolower($_item['event']),
            'source_token'   => $_item['token'],
        ];

        $insertion_clause = ['data' => json_encode($parsed)];

        return self::$connection_idx ?
            (self::$connection->triggerScraperEvent($insertion_clause)) :
            (self::$connection->getConnection()->table([self::$out_queue_table, 'tracks'][self::$connection_idx])->insert($insertion_clause));
    }

    /**
     * Returns the relevant connection
     *
     * @return AdvertiserTracksConnection
     * @throws \Exception
     */
    private static function getConnectionManager() : AdvertiserTracksConnection
    {
        $connection_manager = ConnectionManagerFactory::create(self::$connection_handler);
        $connection_manager->determineConnectionByInput(Session::get('connection_input'));

        return $connection_manager;
    }


    /**
     * Update a single track
     *
     * @param $item
     *
     * @return bool|array
     * @throws \Exception
     */
    private static function updateSingleItem($item)
    {
        $_item = filterAndSetParams([
            'token'                  => null, // Required
            'date'                   => null,
            'commission_amount'      => null,
            'base_commission_amount' => null,
            'currency'               => null,
            'amount'                 => null,
            'page'                   => null,
            'sid'                    => null,
            'network'                => null,
            'device'                 => null,
        ], $item);

        if (!self::validateUpdateTrack($_item)) {
            return false;
        }

        // DWH Track to extract
        $track = DwhFactTracks::where('id', $item['token']);

        // DWH connection for dwh_deleted_rows_tmp
        $delete_tbl = self::$dwh_connection->table(self::$dwh_deleted_table);

        // Rename fields
        // done because this is how it is named in dwh_fact_tracks
        if (isset($item['date'])) {
            if (!is_null($item['date'])) {
                $_item['timestamp'] = date('Y-m-d 12:i:s', strtotime($item['date']));
                if (!empty($track)) {
                    $_filtered_item = $track->select(self::$dwh_deleted_rows_fields)->first()->toArray();
                    $_filtered_item['tracks_updated'] = $_filtered_item['updated_at'];
                    $_filtered_item['tracks_created'] = $_filtered_item['created_at'];
                    $_filtered_item['isDeleted'] = 0;
                    $_filtered_item = array_diff_key($_filtered_item,array_flip(self::$exclude_columns_from_updating));

                    self::$logger->info(['Adding to delete_tbl before updating:', $_filtered_item ]);
                    $delete_tbl->insert($_filtered_item );
                }
            }
            unset($_item['date']);
        }

        // Change token to id
        // done because this is how it is named in dwh_fact_tracks
        $token_id = $_item['token'];
        unset($_item['token']);

        $_item['updateDate'] = sqlDateFormat();
        $differentiated_arr = self::$connection_params + ['updateDate' => ['updateDate', 'updatedAt']];

        // Build update array
        $_update = [];
        foreach ($_item as $key => $value) {
            $altered_value = $value;
            if ($key == 'currency') {
                $altered_value = empty($altered_value) ? 'USD' : $altered_value;
            }
            if (!is_null($value)) {
                $altered_key = $differentiated_arr[$key][self::$connection_idx] ?? $key;
                $_update[$altered_key] = $altered_value;
            }
        }

        // Create an sql query
        // We create the query this way because of laravel's inability to support db links
        $sql = "UPDATE `".self::$out_tracks_table."` SET ";
        foreach ($_update as $key => $update_property) {
            $sql .= sprintf("`%s` = '%s',", $key, $update_property);
        }
        $sql = substr($sql, 0, -1); // removed the last comma
        $sql .= sprintf(" WHERE %s = '%s'", ['id', 'token'][self::$connection_idx], $token_id);

        $mrr = self::$connection->statement(\DB::raw($sql));
        self::$logger->info(['Update item state: ', $mrr, $_item]);

        return true;
    }


    /**
     * Validate Insert Track
     *
     * @param $item
     *
     * @return array|bool
     */
    private static function validateInsertTrack($item)
    {
        $rules = [
            'event'                  => "required",
            'token'                  => "required_without:io_id",
            'currency'               => "required|alpha",
            'date'                   => "required",
            'trx_id'                 => "required",
            'amount'                 => "numeric",
            'commission_amount'      => "numeric",
            'base_commission_amount' => "numeric",
            'device'                 => "alpha",
            'io_id'                  => "required_without:token",
        ];


        $item = self::getValidFields($item);

        // Validate the passed params
        $validator = \Validator::make($item, $rules);
        if ($validator->fails()) {
            self::$logger->warning([$item, $validator->messages()->all()]);
            return false;
        }

        // Validate the date
        if (($date_str = strtotime($item['date'])) === false) {
            self::$logger->warning([$item, "Could not parse date " . $item['date']]);
            return false;
        }

        return true;
    }


    /**
     * Validate Update Track
     *
     * @param $item
     *
     * @return array|bool
     */
    private static function validateUpdateTrack($item)
    {
        $rules = [
            'token'                  => "required",
            'currency'               => "alpha|required_without_all:amount,commission_amount,date,base_commission_amount",
            'amount'                 => "numeric|required_without_all:currency,commission_amount,date,base_commission_amount",
            'commission_amount'      => "numeric|required_without_all:amount,currency,date,base_commission_amount",
            'base_commission_amount' => "numeric|required_without_all:amount,currency,date,commission_amount",
            'date'                   => "required_without_all:amount,currency,commission_amount,base_commission_amount"
        ];

        $item = self::getValidFields($item);

        // Validate the passed params
        $validator = \Validator::make($item, $rules);
        if ($validator->fails()) {
            self::$logger->warning([$item, $validator->messages()->all()]);
            return false;
        }

        return true;
    }


    /**
     * Get only filled params
     *
     * @param $item
     *
     * @return array
     */
    private static function getValidFields($item)
    {
        return array_filter($item,function($param){

            return !is_null($param);
        });
    }
}

