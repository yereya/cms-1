<?php

namespace App\Entities\Repositories\Bo\Advertisers;

use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Bo\AdvertiserTarget;
use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Dwh\DashDailyStatsRepo;
use App\Entities\Repositories\Repository;
use App\Http\Traits\DashboardTrait;
use DB;
use Illuminate\Support\Collection;

/**
 * Class TargetsRepo Repository of advertisers targets
 *
 * @package App\Entities\Repositories\Bo\Advertisers
 */
class TargetsRepo extends Repository
{
    use DashboardTrait;

    const SALES = 'sales';
    const CPA = 'cpa';
    const PROFIT = 'profit';
    const COST = 'cost';
    const DAYS_OF_MONTH = 'days_of_month';
    const TILL_NOW = 'till-now';
    const INCOME = 'income';
    const YESTERDAY = 'yesterday';
    const TWO_WEEKS_AGO = 'two_weeks_ago';
    const DAILY_TARGETS = 'daily_targets';
    const DISTANCE_FROM_TARGET = 'distance_from_target';
    const QUARTERS = [1 => '03', 2 => '06', 3 => '09', 4 => '12'];

    /**
     * Model for advertiser target
     */
    const MODEL = AdvertiserTarget::class;


    /**
     * @var $logger
     */
    protected static $logger;

    /**
     * Get advertisers
     * - get active advertisers according to user account
     * - filter by them
     *
     * @return mixed
     */
    public function getAdvertisers()
    {
        $accounts_repo = (new AccountsRepo());
        $advertisers_id = request()->input('advertisers') ?? $accounts_repo->getAdvertisersId();

        $query = $this->query()
            ->selectRaw('DISTINCT advertiser_name, advertiser_id');

        if (count($advertisers_id)) {
            $query->whereIn('advertiser_id', $advertisers_id);
        }

        return $query;
    }

    /**
     * Get verticals
     */
    public function getVerticals()
    {
        $product_name = $this->query()
            ->pluck('product_name')
            ->unique()
            ->toArray();
        $query = (new DashDailyStatsRepo())->getVerticals();

        return $query->whereIn('product_name', $product_name);
    }

    /**
     * Get Business Groups
     */
    public function getBusinessGroups()
    {
        $advertisers_id = $this->getAdvertisers()->pluck('advertiser_id')->toArray(); // Extract Advertisers' Ids

        # Get unique Business Groups
        $business_group = Advertiser::
        whereIn('id',$advertisers_id)
            ->distinct()
            ->get(['business_group'])
            ->pluck('business_group','business_group')
            ->toArray();

        return $business_group;
    }

    /**
     * Get monthly targets
     *
     * @return array
     */
    public function getMonthlyTarget()
    {
        $query = $this->query()
            ->select(
                DB::raw('sum(sales) sales'),
                DB::raw('sum(income) income'),
                DB::raw('sum(cost) cost'),
                DB::raw('date')
            )
            ->whereRaw('YEAR(date) = YEAR(CURRENT_DATE)')
            ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE)');


        $this->addGlobalFilters($query);

        $result = $query->first();

        return $result ? $result->toArray() : [];
    }


    /**
     * Create daily targets from monthly
     *
     * @param $monthly_targets
     *
     * @return array
     */
    protected function createDailyFromMonthly($monthly_targets)
    {
        $daily_targets = [];
        $days_of_month = date('t', strtotime($monthly_targets['date']));
        $monthly_sales = $monthly_targets[self::SALES];
        $monthly_cost = $monthly_targets[self::COST];
        $monthly_profit = $monthly_targets[self::INCOME] - $monthly_cost;

        $sales = $monthly_sales > 0 ? round($monthly_sales / $days_of_month) : 0;
        $cpa = $monthly_cost > 0 && $monthly_sales > 0 ? $monthly_cost / $monthly_sales : 0;
        $profit = $monthly_profit > 0 ? round(($monthly_profit) / $days_of_month) : 0;

        $daily_targets[self::SALES] = $sales;
        $daily_targets[self::CPA] = $cpa < 1 ? (float)(ceil($cpa * 10) / 10) : round($cpa, 1);
        $daily_targets[self::PROFIT] = $profit;
        $daily_targets['params'][self::DAYS_OF_MONTH] = $days_of_month;

        return $daily_targets;
    }

    /**
     * Get daily targets
     *
     * @return array
     */
    public function getDailyTargets()
    {
        $monthly_targets = $this->getMonthlyTarget();

        return $this->createDailyFromMonthly($monthly_targets);
    }

    /**
     * Get difference between targets
     * Run over all stats:
     *  1. multiply the daily-stat with the days of the month.
     *  2. evaluate the difference between from monthly target.
     *  3. divide the difference with the days  from start of the month.
     *  4. round the stats
     *
     * @return array
     */
    public function getDifferenceFromTarget()
    {
        $diff_from_target = [];
        $days_from_start_month = date('j');
        $stats = [self::CPA, self::SALES, self::PROFIT];
        $dash_daily_repo = (new DashDailyStatsRepo());
        $till_now = $dash_daily_repo->getStatsTillNow(self::TILL_NOW);
        $monthly_target = $this->resolveMonthlyTargets();
        $days_of_month = date('t', strtotime($monthly_target['date']));

        foreach ($stats as $key => $stat) {
            $is_cpa = ($stat == self::CPA);

            if ($is_cpa) {

                $cost_difference = ((double)$monthly_target[self::COST] - (double)$till_now[self::COST]);
                $sales_difference = ((int)($monthly_target[self::SALES]) - (int)$till_now[self::SALES]);
                $difference_no_zero = $cost_difference != 0 && $sales_difference != 0;
                $diff_from_target[self::CPA] = $difference_no_zero ? round($cost_difference / $sales_difference, 1) : 0;
                continue;
            }

            $monthly_difference = (double)$monthly_target[$stat] - (double)$till_now[$stat];

            /*add 1 to the days of the month - so when the date is at the end of the month it still be one day*/
            $daily_difference = ($monthly_difference / (($days_of_month + 1) - $days_from_start_month));
            $diff_from_target[$stat] = round($daily_difference);
        }

        return $diff_from_target;
    }

    /**
     * Get Stats Target
     *  - choose columns according to the target.
     * assign filters :
     *  -  dates according to date_range filter
     *  -  advertisers and verticals
     *  -  group all query by date
     *  -  generate results by duration - [days , weeks , months]
     *
     * @return Collection
     */
    public function getStatsTarget()
    {
        $daily_stats_repo = new DashDailyStatsRepo();

        $query = $this->query();
        $this->selectPpcTargets($query);
        $daily_stats_repo->whereTargetActualDates($query);
        $this->addGlobalFilters($query);
        $query->groupBy('date');

        $results = $query->get();

        if (count($results)) {
            return $this->generateTargetDays($results);
        }
    }


    /**
     * Query Concatenation according to date passed
     * Wheres dates
     *
     * @param $query
     */
    private function whereDates($query)
    {
        $from_date = request()->input('from') ?? date('Y-m-d', strtotime('-1 month'));
        $to_date = request()->input('to') ?? date('Y-m-d');
        $date_range = request()->input('date_range');

        if (!empty($date_range)) {
            $date_range = explode(' ', $date_range['value']);
            $from_date = date('Y-m-01', strtotime($date_range[0]));
            $to_date = date('Y-m-01', strtotime($date_range[1]));
        }
        $query->whereBetween("date", [$from_date, $to_date]);
    }

    /**
     * Build stats For Chart
     * 1. divide monthly target to daily.
     * 2. create daily records.
     *
     * @param $stats
     *
     * @return Collection
     */
    private function generateTargetDays($stats)
    {
        $targets_stats = collect();

        $stats->map(/**
         * @param $stat
         * @param $idx
         */
            function ($stat, $idx) use ($targets_stats) {
                $fields = collect(['conversion', 'cost', 'profit']);
                $records = [];

                /* 1 - prepare records*/
                $fields->each(function ($field) use ($stat, &$records) {
                    $record = $is_under_one = $stat[$field] < 1 && $stat[$field] > 0
                        ? $stat[$field] / $stat->days_of_month
                        : round($stat[$field] / $stat->days_of_month);

                    $records[$field] = $record;
                });

                /*Resolve days of month*/
                //todo check why we need to check date if it passed
                $year_month = date('Y-m', strtotime($stat->date));
                $start_end_date = $this->resolveStartEndDate();
                $start_from_middle = date('n', strtotime($start_end_date['start_date']['date'])) > 1 && $idx == 0;
                $range = $start_from_middle ?
                    ((int)date('j', strtotime($start_end_date['start_date']['date'])))
                    : 1;
                $days = $start_from_middle ? ($range + $this->resolveDaysPassed()) + 1 : $this->resolveDaysPassed();

                /*GENERATE DAYS */
                for ($day = $range; $day < $days; $day++) {
                    $period = $year_month . '-' . ($day);
                    $date = date('Y-m-d', strtotime($period));
                    $records['date'] = $date;
                    $targets_stats->push($records);
                }
            });

        /*Filter the records inside the time range*/
        $targets_stats = $targets_stats->filter(function ($stat) {
            $date = $this->getTheTimeStampOfStartAndEndDate();
            $current_date = strtotime($stat['date']);

            return $current_date >= $date['from'] && $current_date <= $date['to'];
        });

        //reset keys after filtering
        $targets_stats = $targets_stats->values();

        return $targets_stats;
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    private function selectTarget($query)
    {
        $target = request()->input('target') ?? self::SALES;
        $options = [
            self::PROFIT => 'date, (sum(income))-(sum(cost)) as profit, DAY(LAST_DAY(DATE)) AS days_of_month',
            self::CPA => 'date, (sum(cost))/(sum(sales)) as cpa, DAY(LAST_DAY(DATE)) AS days_of_month',
            self::SALES => 'date, sum(sales) as sales, DAY(LAST_DAY(DATE)) AS days_of_month'
        ];

        $query->selectRaw($options[$target]);
    }


    /**
     * @param $results
     *
     * @return mixed
     */
    private function generateTargetMonths($results)
    {
        $target_name = request()->input('target');
        $daily_targets = $this->generateTargetDays($results);
        $monthly_targets = collect();

        $daily_targets->each(function ($record, $index)
        use ($target_name, $daily_targets, $monthly_targets) {

            $params = [
                'index' => $index,
                'daily_targets' => $daily_targets,
                'year_month' => date('Y-m-01', strtotime($record['date'])),
                'in_same_month' => $monthly_targets->where('date', date('Y-m-01', strtotime($record['date'])))
                    ->first(),
                'iteration_in_middle' => $index != 0 && $index + 1 != count($daily_targets),
                'target_name' => $target_name
            ];
            $monthly = $this->prepareMonthly($record, $params);

            if (!$monthly) {

                return true;
            }
            // the record is unique
            // the iteration is first of last
            if (!$params['in_same_month'] || !$params['iteration_in_middle']) {
                $monthly_targets->put($params['year_month'], $monthly);

                return true;
            }
        });

        return $monthly_targets;
    }

    /**
     * @param $results
     *
     * @return array|mixed
     */
    private function generateByDuration($results)
    {
        $duration = request()->input('duration');

        switch ($duration) {

            case 'monthly':
                return $this->generateTargetMonths($results);

            case 'weekly':
                return $this->generateTargetWeeks($results);

            default:
                return $this->generateTargetDays($results);
        }
    }

    /**
     * Generate Targets Week
     *
     * @param $targets
     *
     * @return \Illuminate\Support\Collection
     */
    private function generateTargetWeeks($targets): Collection
    {
        $target_name = request()->input('target');
        $daily_targets = $this->generateTargetDays($targets);
        $weekly_targets = collect();
        $last_month = collect();

        $daily_targets->each(function ($record, $index)
        use ($weekly_targets, $last_month, $daily_targets, $target_name) {
            $params = [
                'index' => $index,
                'daily_targets' => $daily_targets,
                'last_month' => $last_month,
                'target_name' => $target_name
            ];
            $weekly = $this->prepareWeekly($record, $params);
            $year_week = date('Y-W', strtotime($record['date']));
            $existed_record = $weekly_targets->where('date', $year_week)->first();
            $iteration_in_middle = ($index != 0) && (($index + 1) != count($daily_targets));
            $date_at_start_of_month =
                date('Y-m-d', strtotime($record['date'])) ==
                date('Y-m-01', strtotime($record['date']));

            /*skip the iteration if weekly record return empty array*/
            if (!count($weekly)) {

                return true;
            }

            /*put new record if it doesn't exist*/
            if ((!$existed_record)) {
                $weekly_targets->put($weekly['date'], $weekly);

            }

            /*if the key in start of month update the key with the relevant record*/
            if ($date_at_start_of_month || !$iteration_in_middle) {
                $weekly_targets->forget($weekly['date']);
                $weekly_targets->put($weekly['date'], $weekly);
            }
        });

        return $weekly_targets;
    }

    /**
     * Resolve monthly targets
     *
     * @return array
     */
    private function resolveMonthlyTargets(): array
    {
        $stats = $this->getMonthlyTarget();

        $stats['profit'] = $stats[self::INCOME] - $stats[self::COST];
        $stats['sales'] = $stats[self::SALES];

        return $stats;
    }

    /**
     * Prepare weekly targets
     *
     * @param $date
     * @param $params
     *
     * @return array
     */
    private function prepareWeekly($date, $params): array
    {
        $dates = $this->prepareWeeklyDates($date);
        $condition = $this->prepareWeeklyConditions($params, $dates);

        if ($condition['is_cpa_target']) {

            return [
                'date' => $dates['year_week'],
                'target' => $dates['target']
            ];
        }

        /*
        * Add record of last month to temp collection:
        *
        * Check if:
        *  - the date is at the end of the month (example -> 31-05-2017).
       */
        if ($condition['week_at_end_of_month']) {
            $params['last_month']->put('record', $dates);

            return [];
        }

        /*
         * Set the weekly targets between months
         * Check if:
         *
         * - not empty last month record.
         * - there's key of last month week (data was assigned) in the previous iteration.
         *
        */
        if ($condition['not_empty_last_month'] && $condition['have_last_month_records']) {

            $last_month = $params['last_month']->get('record');
            $first_day_of_month = date('Y-m-d', strtotime($last_month['last_day_month'] . '+ 1 day'));
            $week_at_start_of_month = $dates['base_date'] == $first_day_of_month;

            if ($week_at_start_of_month) {

                return $this->resolveMiddleWeek($dates, $last_month);
            }
        }

        /*
         * If: the date range that chosen is:
         *
         *  - week at start of month.
         *  - a week between two months.
         *  - the first record of the iteration. (don't have last_month record).
         * Or
         *  - last record of the iteration.
         *  - a week between the next month.
         * Then
         *  - get the days of week.
         *  - get the target by multiply the number of days that passed.
         *
         */
        if (($condition['middle_of_the_week'] && !$condition['iteration_at_middle'])

        ) {
            $week_days = ($dates['week_days']);
            $days = $condition['first_iteration'] ? 7 - $week_days : $week_days;
            $target = $days * $date['target'];

            return [
                'date' => $dates['year_week'],
                'target' => $target
            ];
        }

        /* Return default record target
         * Multiply by 7 for week
         */
        return [
            'date' => $dates['year_week'],
            'target' => $dates['target'] * 7
        ];
    }

    /**
     * Resolve middle week target
     * Check if:
     * - the date is the first in the month.
     * (example -> 01-06-2017)
     *
     * @param $dates
     * @param $last_month
     *
     * @return array
     */
    private function resolveMiddleWeek($dates, $last_month)
    {
        /*days start from zero (so need to increment by 1)*/
        $last_month_week_days = $last_month['week_days'] + 1;
        $current_month_week_days = 7 - $last_month_week_days;

        /*get the sum of targets*/
        $last_month_targets = $last_month['target'] * $last_month_week_days;
        $current_month_targets = $dates['target'] * $current_month_week_days;
        $week_target = $last_month_targets + $current_month_targets;

        return [
            'date' => $dates['year_week'],
            'target' => $week_target
        ];
    }

    /**
     * Prepare conditions for inner usage
     *
     * @param $params
     * @param $dates
     *
     * @return array
     */
    private function prepareWeeklyConditions($params, $dates)
    {
        return [
            'week_at_start_of_month' => $dates['base_date'] == date('Y-m-01', strtotime($dates['base_date'])),
            'week_at_end_of_month' => $dates['base_date'] == $dates['last_day_month'],
            'have_last_month_records' => $params['last_month']->has('record'),
            'not_empty_last_month' => count($params['last_month']),
            'middle_of_the_week' => $dates['week_days'] > 0,
            'iteration_at_middle' => ($params['index'] + 1 != count($params['daily_targets'])) &&
                $params['index'] != 0,
            'first_iteration' => $params['index'] == 0,
            'is_cpa_target' => $params['target_name'] == 'cpa'
        ];
    }

    /**
     * Prepare dates for inner usages
     *
     * @param $date
     *
     * @return array
     */
    private function prepareWeeklyDates($date)
    {
        return [
            'base_date' => $date['date'],
            'week_number' => date('W', strtotime($date['date'])),
            'week_days' => date('w', strtotime($date['date'])),
            'month' => date('m', strtotime($date['date'])),
            'last_day_month' => date('Y-m-t', strtotime($date['date'])),
            'year_week' => date('Y-W', strtotime($date['date'])),
            'target' => $date['target']
        ];
    }

    /**
     * Prepare Monthly Records for chart usage
     *
     * @param $record
     * @param $params
     *
     * @return array
     */
    private function prepareMonthly($record, $params)
    {
        $dates = $this->prepareMonthlyDates($record);
        $condition = $this->prepareMonthlyConditions($params);

        if ($condition['is_cpa_target']) {

            return [
                'date' => $dates['year_month'],
                'days_of_month' => $dates['days_in_month'],
                'target' => $record['target']
            ];
        }

        if (!$condition['iteration_in_middle'] && $dates['middle_of_month']) {
            $days = $condition['first_iteration'] ? $dates['days_till_end_month'] : $dates['days_pass_in_month'];
            $target = $record['target'] * (int)$days;

            return [
                'date' => $dates['year_month'],
                'days_of_month' => $dates['days_in_month'],
                'target' => $target
            ];
        }

        return [
            'date' => $dates['year_month'],
            'days_of_month' => $dates['days_in_month'],
            'target' => $record['target'] * $dates['days_in_month']
        ];
    }

    /**
     * Assign the right target column according to request target
     *
     * @param $result
     * @param $targets
     *
     * @return int
     */
    public function resolveTargetByDuration($result, $targets)
    {
        $duration = request()->input('duration');
        $is_weekly = $duration == 'weekly';

        if (!$is_weekly) {
            $date = date('Y-m-d', strtotime($result->date));

            return !empty($date) && !empty($targets[$date]) ? (int)$targets[$date]['target'] : 0;
        }

        $date = explode('-', $result->date);
        $date[1] = $date[1] < 10 && !preg_match('/[0]/', $date[1]) ? 0 . $date[1] : $date[1];
        $date = implode('-', $date);

        return !empty($date) && !empty($targets[$date]) ? (int)$targets[$date]['target'] : 0;
    }

    /**
     * @param $record
     *
     * @return array
     */
    private function prepareMonthlyDates($record): array
    {
        return [
            'days_till_end_month' =>
                date('d', (strtotime(date('Y-m-t', strtotime($record['date']))) - strtotime($record['date']))),
            'days_pass_in_month' => date('j', strtotime($record['date'])),
            'middle_of_month' => date('Y-m-01') != date('Y-m-d', strtotime($record['date'])) &&
                date('Y-m-t', strtotime($record['date'])) != date('Y-m-d', strtotime($record['date'])),
            'year_month' => date('Y-m-01', strtotime($record['date'])),
            'days_in_month' => date('t', strtotime($record['date']))
        ];
    }

    /**
     * @param $params
     *
     * @return array
     */
    private function prepareMonthlyConditions($params): array
    {
        return [
            'iteration_in_middle' => $params['index'] != 0 && $params['index'] + 1 != count($params['daily_targets']),
            'first_iteration' => $params['index'] == 0,
            'is_cpa_target' => $params['target_name'] == 'cpa'
        ];
    }

    /**
     *  Get Quarterly target
     *
     * @param float $quarter
     *
     * @return array|\Illuminate\Support\Collection|static[]
     * @internal param float $quarters
     * @internal param $quarter
     */
    public function getQuarterlyTargets(float $quarter)
    {
        $sub_query = $this->query();
        $this->getInnerQuery($sub_query);

        $query = DB::table(DB::raw("({$sub_query->toSql()}) as sub"))
            ->mergeBindings($sub_query->getQuery())
            ->selectRaw('sum(sales) sales, sum(cost) cost, (sum(income) - sum(cost)) profit')
            ->where('quarter', $quarter);

        $this->addGlobalFilters($query);

        $result = $query->first();

        return $result ?? collect();
    }

    /**
     * Get inner quarterly query
     *
     * @param $query
     *
     * @return mixed
     */
    public function getInnerQuery($query)
    {

        $model = $this->model();
        $table = $model->getConnectionName() . '.' . $model->getTable();

        $sub = $query
            ->from($table)
            ->selectRaw('QUARTER(DATE) as quarter, sales, cost, income, date, advertiser_id');

        return $sub;
    }

    /**
     * Resolve the daily target needed to be accomplished in order to reach the quarter target
     *  - Get the quarter of this month.
     *  - Get the targets for the quarter.
     *  - Get the results for the quarter.
     *  - iterate the re
     */
    public function getQuarterDifferenceByTargets(): array
    {
        $daily_stats_repo = (new DashDailyStatsRepo());
        $current_month = date('m', time());
        $quarter = (int)ceil($current_month / 3);
        $stats = [self::SALES, self::CPA, self::PROFIT];
        $diff_from_quarter_target = [];


        $current_date = date_create(date('d-m-Y'));
        $last_day_of_quarter = date_create(date('t-' . self::QUARTERS[$quarter] . '-Y'));
        $days_till_end_of_quarter = (date_diff($last_day_of_quarter, $current_date))->days;

        $quarter_targets = $this->getQuarterlyTargets($quarter);
        $till_now = $daily_stats_repo->getQuarterlyResultsTillNow($quarter);

        foreach ($stats as $key => $stat) {
            $is_cpa = ($stat == self::CPA);

            if ($is_cpa) {
                $cost_difference =
                    ((double)$quarter_targets->{self::COST} - (double)$till_now->{self::COST});
                $sales_difference = ((double)($quarter_targets->{self::SALES}) - (double)
                    $till_now->{self::SALES});
                $difference_no_zero = $cost_difference != 0 && $sales_difference != 0;
                $diff_from_quarter_target[self::CPA] = $difference_no_zero ? round($cost_difference /
                    $sales_difference,
                    1) : 0;
                continue;
            }

            $monthly_difference = (double)$quarter_targets->{$stat} - (double)$till_now->{$stat};
            $no_zero_values = $monthly_difference != 0 && $days_till_end_of_quarter != 0;
            $daily_difference = $no_zero_values
                ? $monthly_difference / $days_till_end_of_quarter
                : ($monthly_difference != 0 && $days_till_end_of_quarter == 0)
                    ? $monthly_difference
                    : 0;
            $diff_from_quarter_target[$stat] = round($daily_difference);
        }

        return $diff_from_quarter_target;
    }

    /**
     * Add global filters :
     * - advertiser
     * - verticals
     *
     * @param $query
     */
    private function addGlobalFilters($query)
    {

        $this->whereAdvertisers($query);
        $this->whereVerticals($query);
        $this->whereDevices($query);
        $this->whereAccounts($query);
        $this->whereSaleGroup($query);
        $this->whereBusinessGroup($query);
        $this->whereTrafficSource($query);

    }

    /**
     * @return array
     */
    public function getAdvertiserStatsTarget(): array
    {

        $query = $this->query();
        $query->select(
            DB::raw('advertiser_name'),
            DB::raw('advertiser_id'),
            DB::raw('sum(sales) sales'),
            DB::raw('sum(cost) cost'),
            DB::raw('sum(income) - sum(cost) profit'),
            DB::raw('date')
        )
            ->whereRaw('YEAR(date) = YEAR(CURRENT_DATE)')
            ->whereRaw('MONTH(date) = MONTH(CURRENT_DATE)');

        $query->groupBy('advertiser_name')
            ->orderBy('advertiser_name');

        $this->addGlobalFilters($query);

        $result = $query->get();


        $targets = [];
        foreach ($result as $index => $row) {
            $targets[] = [
                'advertiser_name' => $row->advertiser_name,
                'advertiser_id' => $row->advertiser_id,
                'cpa_target' => $row->sales > 0 ? round($row->cost / $row->sales, 1) : 0,
                'cost_target' => $row->cost,
                'sales_target' => $row->sales,
                'profit_target' => round($row->profit, 0),
                'date' => $row->date
            ];
        }
        return $targets;

    }

    /**
     * @return array
     */
    public function getAdvertisersDifferenceFromTarget($targets): array
    {

        $diff_from_target = [];
        $days_from_start_month = date('j');
        $dash_daily_repo = (new DashDailyStatsRepo());
        $till_now = $dash_daily_repo->getAdvertisersStatsTillNow();
        $monthly_target = $targets;
        if (empty($monthly_target)) {
            return $diff_from_target;
        }
        $days_of_month = date('t', strtotime($monthly_target[0]['date']));
        $days_left = ($days_of_month + 1) - $days_from_start_month;
        foreach ($till_now as $now) {
            foreach ($monthly_target as $index => $target) {
                if ($now->advertiser_id == $target['advertiser_id']) {
                    $cost_dif = ($target['cost_target'] - $now->cost);
                    $leads_dif = ($target['sales_target'] - $now->sales);
                    $diff_from_target[] = [
                        'advertiser_name' => $now->advertiser_name,
                        'advertiser_id' => $now->advertiser_id,
                        'cpa' => $leads_dif != 0 ? round(($cost_dif / $leads_dif) / $days_left, 1) : 0,
                        'sales' => round(($target['sales_target'] - $now->sales) / $days_left, 1),
                        'cost' => round(($target['cost_target'] - $now->cost) / $days_left, 1),
                        'profit' => round(($target['profit_target'] - $now->profit) / $days_left, 1)
                    ];
                }

            }
        }

        return $diff_from_target;
    }


    /**
     * @param $targets
     * @param $stats
     * @param $dist_from_target
     * @return array
     */
    public function unifyAdvertisersStats($targets, $stats, $dist_from_target): array
    {
        $rows = [];
        $targets_advertisers = array_column($targets, 'advertiser_id');
        $distance_advertisers = array_column($dist_from_target, 'advertiser_id');


        if (empty($targets) || empty($stats)) {
            $rows[] = [
                'advertiser_name' => "N/A",
                'cpa' => "N/A",
                'cpa_target' => "N/A",
                'cpa_distance' => "N/A",
                'sales' => "N/A",
                'sales_target' => "N/A",
                'sales_distance' => "N/A",
                'profit' => "N/A",
                'profit_target' => "N/A",
                'profit_distance' => "N/A",
                'roi' => "N/A"
            ];
            return $rows;
        }

        foreach ($stats as $index => $stat) {
            $advertiser = $stat['advertiser_id'];
            $rows[$index]['advertiser_name'] = $stat['advertiser_name'];
            $rows[$index]['cpa'] = $stat['sales'] != 0 ? round($stat['cost'] / $stat['sales'], 1) : 0.0;
            $rows[$index]['sales'] = $stat['sales'];
            $rows[$index]['profit'] = $stat['profit'];
            $rows[$index]['roi'] = $stat['cost'] != 0 ? (string)round(($stat['revenue'] / $stat['cost']), 3) : 0;

            /** Checks if the advertiser_name (from dailyStats) is in the targets array
             *   if FOUND, gets the index, else returns FALSE
             */

            $target_index = array_search($advertiser, $targets_advertisers);
            if (is_int($target_index)) {
                $rows[$index]['cpa_target'] = $targets[$target_index]['cpa_target'];
                $rows[$index]['sales_target'] = $targets[$target_index]['sales_target'];
                $rows[$index]['profit_target'] = $targets[$target_index]['profit_target'];
            } else {
                $rows[$index]['cpa_target'] = "N/A";
                $rows[$index]['sales_target'] = "N/A";
                $rows[$index]['profit_target'] = "N/A";
            }

            $distance_index = array_search($advertiser, $distance_advertisers);
            if (is_int($distance_index)) {
                $rows[$index]['cpa_distance'] = $dist_from_target[$distance_index]['cpa'];
                $rows[$index]['sales_distance'] = $dist_from_target[$distance_index]['sales'];
                $rows[$index]['profit_distance'] = $dist_from_target[$distance_index]['profit'];
            } else {
                $rows[$index]['cpa_distance'] = "N/A";
                $rows[$index]['sales_distance'] = "N/A";
                $rows[$index]['profit_distance'] = "N/A";
            }

        }

        return $rows;
    }

    /**
     * @param $display_value
     * @return array|Collection
     */
    public function getDailyAvgTargets($display_value)
    {
        $monthly_targets = $this->getMonthlyTargetWithAdvertiser();

        if (\count($monthly_targets)) {
            $fields = ['t_cost', 't_revenue', 't_conversions', 't_profit'];

            $results = $this->getRecordsFromMonthlyAccordingToDisplayValue($monthly_targets, $display_value, $fields);

            foreach ($results as $actual_result){
                if(!$actual_result['t_revenue'] || !$actual_result['t_conversions']){
                    $actual_result['t_epa'] = 0;
                }else{
                    $actual_result['t_epa'] = $actual_result['t_revenue'] / $actual_result['t_conversions'];
                }
                if(!$actual_result['t_revenue'] || !$actual_result['t_cost']){
                    $actual_result['t_roi'] = 0;
                }else{
                    $actual_result['t_roi'] =($actual_result['t_revenue']/$actual_result['t_cost'] )  ;
                }
            }
            return $results;
        }
        return collect();
    }


    /**
     * @return Collection
     */
    private function getMonthlyTargetWithAdvertiser(): Collection
    {
        $daily_stats_repo = new DashDailyStatsRepo();
        $query = $this->query()
            ->select(
                'advertiser_id',
                DB::raw('DATE_FORMAT(DATE,"%Y-%m-%d") date'),
                DB::raw('DAY(LAST_DAY(date)) as days_in_month'),
                DB::raw('advertiser_name as advertiser'),
                DB::raw('sum(cost) t_cost'),
                DB::raw('sum(income) t_revenue'),
                DB::raw('(SUM(income) - SUM(cost)) t_profit'),
                DB::raw('sum(sales) t_conversions')
            );
        $daily_stats_repo->whereTargetActualDates($query);
        $this->addGlobalFilters($query);

        $query->groupBy(['advertiser', 'date']);
        $query->orderBy('advertiser');
        $query->orderBy('date');


        $result = $query->get();

        return $result;
    }


    /**
     * @param $display_value
     * @return mixed
     */
    public function getTillNowTargets($display_value)
    {
        /*Get data from db*/
        $monthly_targets = $this->getMonthlyTargetWithAdvertiser();

        if ($monthly_targets) {
            $fields = ['t_cost', 't_revenue', 't_conversions', 't_profit'];

            $results =  $this->getRecordsFromMonthlyAccordingToDisplayValue($monthly_targets, $display_value, $fields);

            foreach ($results as $actual_result){
                if(!$actual_result['t_revenue'] || !$actual_result['t_conversions']){
                    $actual_result['t_epa'] = 0;
                }else{
                    $actual_result['t_epa'] = $actual_result['t_revenue'] / $actual_result['t_conversions'];
                }
                if(!$actual_result['t_revenue'] || !$actual_result['t_cost']){
                    $actual_result['t_roi'] = 0;
                }else{
                    $actual_result['t_roi'] =( $actual_result['t_revenue']/$actual_result['t_cost'])  ;
                }
            }
            return $results;
        }
    }

    private function selectPpcTargets($query)
    {
        $query->selectRaw('date, ROUND(SUM(sales)) conversion, ROUND(SUM(cost)) cost, ROUND(SUM(income)-SUM(cost)) as profit, DAY(LAST_DAY(date)) days_of_month');
    }
}
