<?php namespace App\Entities\Repositories\Bo\Advertisers;


use App\Libraries\TpLogger;
use Carbon\Carbon;

/**
 * Class TracksRepo
 *
 * @package App\Entities\Repositories\Bo\Advertisers
 */
class CampaignsRepo
{
    private static $logger;
    private static $connection = 'bo';
    private static $table = 'account_campaigns';


    /**
     * Update
     *
     * @param $items
     *
     * @return int
     */
    public static function update($items)
    {
        self::$logger = TpLogger::getInstance();
        self::$logger->info(['Valid tracks to be updated', $items]);

        $saved = [];

        try {
            foreach ($items as $item) {
                if (self::updateSingleItem($item)) {
                    $saved[] = $item;
                }
            }
        } catch (\Exception $e) {
            self::$logger->error($e);
        }

        // Log the saving process
        self::$logger->info(['Successfully updated ' . count($saved) . ' tracks', $saved]);

        return count($saved);
    }

    /**
     * Get Tracks By Token
     *
     * @param array $campaign_ids
     *
     * @return array|static[]
     */
    public static function getByAccountIds(array $campaign_ids)
    {
        return self::query()
            ->select('campaign_id as campaign_id_old',
                'segment as segment_old',
                'country as country_old',
                'target_roi as target_roi_old',
                'campaign_quality as campaign_quality_old')
            ->whereIn('campaign_id', $campaign_ids)
            ->get();
    }

    /**
     * Update a single track
     *
     * @param $item
     *
     * @return bool|array
     */
    private static function updateSingleItem($item)
    {
        $_item = filterAndSetParams([
            'campaign_id'       => null, // Required
            'target_roi'        => null, // Required
            'country'           => null,
            'segment'           => null,
            'campaign_quality'  => null,
            'updated_at'  => Carbon::now()->toDateTimeString(),
        ], $item);

        if (!self::validateUpdate($_item)) {
            return false;
        }

        // Build update array
        $_update = [];
        foreach (array_except($_item, ['campaign_id']) as $key => $value) {
            if (!is_null($value)) {
                $_update[$key] = $value;
            }
        }

        // Save
        $query_result = self::query()
            ->where('campaign_id', $_item['campaign_id'])
            ->update($_update);

        self::$logger->info(['Update item state: ', $query_result, $_item]);

        return $query_result;
    }


    /**
     * Validate Insert Track
     *
     * @param $item
     *
     * @return array|bool
     */
    private static function validateUpdate($item)
    {
        $rules = [
            'campaign_id'        => "required",
            'target_roi'         => "numeric",
            'country'            => "string",
            'segment'            => "string",
            'campaign_quality'   => "string",
        ];

        $item = self::getValidFields($item);

        // Validate the passed params
        $validator = \Validator::make($item, $rules);
        if ($validator->fails()) {
            self::$logger->warning([$item, $validator->messages()->all()]);
            return false;
        }

        return true;
    }

    /**
     * query
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected static function query()
    {
        return \DB::connection(self::$connection)
            ->table(self::$table);
    }

    /**
     * Filter only filled params
     *
     * @param $item
     *
     * @return array
     */
    private static function getValidFields($item)
    {
        return array_filter($item,function($param){

            return !is_null($param);
        });
    }

}