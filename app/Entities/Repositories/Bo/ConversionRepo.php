<?php


namespace App\Entities\Repositories\Bo;


use App\Entities\Models\Bo\Conversion;
use App\Entities\Repositories\Repository;
use App\Http\Traits\DashboardTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class ConversionRepo
 *
 * @package App\Entities\Repositories\Bo
 */
class ConversionRepo extends Repository
{
    use DashboardTrait;
    /**
     * @var Conversion MODEL
     */
    const MODEL = Conversion::class;

    /**
     * @param $provider
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByProvider($provider)
    {
        $query = $this->query();
        $query->wherePublisherIs($provider);
        $this->whereDates($query);
        $query->whereStatus(0);
        $query->whereNull('error');
        $query->limit(200);

        $results = $query->get();
        return $results;

        //return $results->groupBy('source_account_id');
    }

    /**
     * @param $provider
     *
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getByProviderWithoutClickout($provider)
    {
        $query = $this->query();
        $query->wherePublisherIs($provider);
        $this->whereDates($query);
        $query->whereStatus(0);
        $query->where('event','!=','clickout');
        $query->whereNull('error');
        $query->limit(200);

        $results = $query->get();

        return $results->groupBy('source_account_id');
    }

    /**
     * Resolve dates according to options param
     *
     * @param  $query Builder
     */
    private function whereDates($query)
    {
        $dates = [
            'from' => date('Y-m-d', strtotime('-1 day ')) . ' 00:00:00',
            'to'   => date('Y-m-d') . ' 23:59:59'
        ];

        if (!empty($dates['date-range-start'])) {
            $dates['from'] = $dates['date-range-start'] . ' 00:00:00';
            $dates['to']   = $dates['date-range-end'] . ' 23:59:59';
        }

        $query->whereBetween('created_at', $dates);
    }

    /**
     * @param      $model
     * @param null $error
     *
     * @return mixed
     */
    public function signModel($model, $error = null)
    {
        $model->status   = 1;
        $model->response = 'Success';

        if ($error) {
            $model->error    = $error;
            $model->response = null;
        }

        return $model->save();
    }

    /**
     * @param $model
     * @param $error
     *
     * @return mixed
     */
    public function updateLog($model, $error)
    {
        $model->error = $error;

        return $model->save();
    }


    /**
     * @param      Collection $model
     * @param null            $errors
     *
     * @return
     */
    public function _save($model, $errors = null)
    {
        $this->_fill($model, $errors);

        return $model->save();
    }

    /**
     * @param $model
     * @param $response
     *
     * @return Collection
     */
    private function _fill($model, $response)
    {
        /** @var Collection $model */
        $model->status   = 1;
        $model->response = 'Success';

        if ($response) {
            $model->response = null;
            $model->error    = $response[0]->ErrorCode;
        }

        return $model;
    }
}
