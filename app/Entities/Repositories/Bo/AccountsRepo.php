<?php


namespace App\Entities\Repositories\Bo;


use App\Entities\Models\Bo\Account;
use App\Entities\Repositories\Repository;
use App\Entities\Repositories\Users\UserRepo;

/**
 * Class AccountsRepo
 *
 * @package App\Entities\Repositories\Bo
 */
class AccountsRepo extends Repository
{
    const MODEL = Account::class;

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getActive()
    {
        return $this->query()
            ->where('status', 'active')
            ->pluck('source_account_id');
    }

    /**
     * Get Active User Accounts
     *
     * @return mixed
     */
    public function getActiveUserAccounts()
    {
        $activeAccounts = session('user_account_access') ? session('user_account_access') : auth()->user()->accounts->toArray();

        if (is_array($activeAccounts)) {

            return $this->query()->whereIn('id', $activeAccounts)->get();
        } else {

            return collect();
        }
    }

    /**
     * Get Active User Accounts Without ORM
     *
     * @return mixed
     */
    public function getActiveUserAccountsWithoutORM()
    {
        if(session('user_account_access')){
            $assigned_accounts = session('user_account_access');
        }else{
            $user_id = auth()->user()->getAuthIdentifier();
            $assigned_accounts = \DB::connection('bo')->table('user_assigned_accounts')->where('user_id','=',$user_id)->get()->pluck('account_id')->toArray();
        }
        if (is_array($assigned_accounts)) {

            return $this->query()->whereIn('id', $assigned_accounts)->get();
        } else {

            return collect();
        }
    }

    /**
     * Active Account Scope
     * That Get User Active Accounts
     *
     * @param $model
     *
     * @return mixed
     */
    protected function activeAccountsScope($model)
    {
        $user_id = auth()->user()->id;

        return $model->active()
            ->whereHas('users', function ($query) use ($user_id) {
                $query->where('id', $user_id);
            });
    }

    /**
     * Get Active Accounts With Advertiser Relationship
     *
     * @return mixed
     */
    public function getActiveAccountsWithAdvertiser()
    {
        return $this->activeAccountsScope($this->model())
            ->with('advertiser')
            ->get();
    }


    /**
     * Get Advertiser Id
     *  - get distinct advertiser ids from active accounts
     *
     * @return array
     */
    public function getAdvertisersId()
    {
        $accounts       = $this->getActiveAccountsWithAdvertiser();
        $advertisers_id = $accounts->pluck('advertiser_id')->unique()->toArray();

        return $advertisers_id;
    }

    /**
     * @return mixed
     */
    public function getUniqueActiveUserAccountsNames()
    {
        $accounts = $this->getActiveUserAccounts()
            ->pluck('product_name')
            ->unique();

        $accounts = $accounts->map(function ($account) {
            if ($account == 'Website biulder') {
                return 'WebsiteBuilders';
            }
            return $account;
        });

        return $accounts;
    }

    /**
     * Get Sale Group List
     *
     * @return mixed
     */
    public function getSalesGroup()
    {
        return $this->query()
            ->notNullSaleGroup()
            ->get()
            ->unique('sale_group');
    }


    /**
     * Get Advertiser Ids that have specific type of sale group
     *
     * @param string $sale_group
     *
     * @return \Illuminate\Support\Collection
     */
    public function getAdvertiserIdsBySaleGroup(string $sale_group)
    {
        return $this->query()
            ->where('sale_group', $sale_group)
            ->get()
            ->pluck('advertiser_id');
    }

    /**
     * @param $advertisers_name
     *
     * @return TYPE_NAME|\Illuminate\Database\Eloquent\Builder
     */
    public function getByAdvertiserName($advertisers_name)
    {
        $query = $this->query();

        $query->whereHas('advertiser', function ($query) use ($advertisers_name) {
            if (count($advertisers_name) > 1) {
                return $query->whereIn('name', $advertisers_name);
            }

            $advertiser_name = $advertisers_name;
            $query->where('name', $advertiser_name);
        });

        return $query;
    }


    /**
     * @param $accounts_name
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getByName($accounts_name)
    {
        $query = $this->query();

        if (count($accounts_name) > 1) {
            return $query->whereIn('name', $accounts_name);
        }

        $account_name = $accounts_name;
        $query->where('name', $account_name);

        return $query;
    }

    /**
     * @param $ids
     *
     * @return mixed
     */
    public function getByAdvertiserId($ids)
    {
        $query = $this->query();

        $query->whereHas('advertiser', function ($query) use ($ids) {
            if (count($ids) > 1) {
                return $query->whereIn('id', $ids);
            }

            $id = $ids;
            $query->where('id', $id);

        });

        return $query;
    }

    public function getByProvider($provider)
    {
        $provider_to_publisher = ['bing' => 97, 'google' => 95];

        $query = $this->query();

        return $query->wherePublisherId($provider_to_publisher[$provider])
            ->pluck('source_account_id');
    }
}
