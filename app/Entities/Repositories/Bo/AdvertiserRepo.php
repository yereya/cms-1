<?php


namespace App\Entities\Repositories\Bo;


use App\Entities\Models\Bo\Advertiser;
use App\Entities\Repositories\Repository;

class AdvertiserRepo extends Repository
{
    const MODEL = Advertiser::class;


    /**
     * @param $ids
     *
     * @return \Illuminate\Support\Collection
     */
    public function getUserAdvertisersByIds($ids)
    {
        return $this->model()
            ->whereIn('id', $ids)
            ->pluck('name', 'id');
    }
    /**
     * @param $business_group
     *
     * @return \Illuminate\Support\Collection
     */
    public function getUserAdvertisersByBusinessGroup($business_group)
    {
        return $this->model()
            ->whereIn('business_group', $business_group)
            ->pluck('id', 'name');
    }
}