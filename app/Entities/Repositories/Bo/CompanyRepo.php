<?php


namespace app\Entities\Repositories\Bo;


use App\Entities\Models\Bo\Company;
use App\Entities\Repositories\Repository;

/**
 * Class CompanyRepo
 *
 * @package app\Entities\Repositories\Bo
 */
class CompanyRepo extends Repository
{
    /**
     * @var Company MODEL
     */
    const MODEL = Company::class;

    /**
     * Get SelectBox List
     */
    public function getList()
    {
        $query  = $this->query();
        $result = $query->pluck('name', 'id');

        return $result;
    }
}