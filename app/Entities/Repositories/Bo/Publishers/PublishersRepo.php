<?php namespace App\Entities\Repositories\Bo\Publishers;


use App\Entities\Models\Bo\Account;
use App\Entities\Models\Mrr\MrrFactPublisher;
use App\Libraries\TpLogger;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class TracksRepo
 * @package App\Entities\Repositories\Bo\Advertisers
 */
class PublishersRepo
{
    protected static $logger;

    /**
     * Insert
     *
     * @param $items
     *
     * @return integer
     */
    public static function insert($items)
    {
        self::$logger = TpLogger::getInstance();
        self::$logger->info(['Valid Publishers to be add', $items]);

        // Fetch publisher_id / publisher_name / account_name
        $all_accounts = @array_unique(@array_pluck($items, 'account_id'));
        $account      = new Collection();
        if (count($all_accounts)) {
            $account = Account::whereIn('source_account_id', $all_accounts)
                ->with('publisher')
                ->get()
                ->keyBy('source_account_id');

            self::$logger->error(['Found accounts', (array)$account]);
        } else {
            self::$logger->error('No account_id were passed.');
        }

        $saved = [];
        foreach ((array)$items as $key => $item) {
            try {

                // Validate that the source_report_id was found in the bo accounts table
                if (!isset($account[$item['account_id']])) {
                    throw new \Exception('Could not find the passed source_account_id');
                }

                // Fill the auto filled params
                $item['publisher_id']   = $account[$item['account_id']]->publisher_id;
                $item['publisher_name'] = $account[$item['account_id']]->publisher->publisher_name;
                $item['account_name']   = $account[$item['account_id']]->name;

                if (self::insertSingle($item)) {
                    $saved[] = $item;
                }
            } catch (\Exception $e) {
                self::$logger->error($e);
            }
        }

        // Log the saving process
        self::$logger->info(['Successfully added ' . count($saved) . ' publishers', $saved]);

        return count($saved);
    }


    /**
     * Inserts a single track
     *
     * @param $item
     *
     * @return bool|void
     */
    private static function insertSingle($item)
    {
        $_item = filterAndSetParams([
            // Required fields
            'sid'                 => null,
            'source_report_uid'   => null,
            'account_id'          => null,
            'network_net_revenue' => null,
            'stats_date_tz'       => null,
            'device'              => null,
            'cost'                => null,

            // Optional fields
            'account_name'        => '',
            'publisher_id'        => '',
            'publisher_name'      => '',
            'campaign_id'         => '',
            'campaign_name'       => '',
            'keyword_name'        => '',
            'match_type'          => '',
            'ad_group_id'         => '',
            'ad_group_name'       => '',
            'clicks'              => 0,
            'impressions'         => 0,

        ], $item);

        if (!self::validateInsertTrack($_item)) {
            return false;
        }

        $_item['stats_date_tz'] = date('Y-m-d 12:i:s', strtotime($item['stats_date_tz']));

        return MrrFactPublisher::insert($_item);
    }


    /**
     * Validate Insert Track
     *
     * @param $item
     *
     * @return array|bool
     */
    private static function validateInsertTrack($item)
    {
        $rules = [
            // Required fields
            'sid'                 => "required",
            'source_report_uid'   => "required|numeric",
            'account_id'          => "required",
            'network_net_revenue' => "required",
            'stats_date_tz'       => "required|date",
            'device'              => "required",
            'cost'                => "required",

            // Optional fields
            'campaign_id'         => "numeric",
            'match_type'          => "alpha",
            'ad_group_id'         => "numeric",
            'clicks'              => "numeric",
            'impressions'         => "numeric",
        ];

        // Validate the passed params
        $validator = \Validator::make($item, $rules);
        if ($validator->fails()) {
            self::$logger->warning([$item, $validator->messages()->all()]);
            return false;
        }

        // Validate the date
        if (($date_str = strtotime($item['stats_date_tz'])) === false) {
            self::$logger->warning([$item, "Could not parse date " . $item['date']]);
            return false;
        }

        return true;
    }
}