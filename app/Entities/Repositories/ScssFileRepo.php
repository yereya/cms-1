<?php


namespace App\Entities\Repositories;


use App\Entities\Models\ScssFile;

/**
 * Class ScssFileRepo
 *
 * @package App\Entities\Repositories
 */
class ScssFileRepo extends Repository
{
    /**
     * Scss File Model
     */
    const MODEL = ScssFile::class;

    /**
     * Update Files Priority
     *
     * @param $ids
     *
     * @return bool
     */
    public function updatePriorities($ids)
    {
        $index    = 1;
        $statuses = [];

        foreach ($ids as $id => $priority) {
            $model           = $this->find($id);
            $model->priority = $index;
            $status          = $model->save();
            array_push($statuses, $status);
            $index++;
        }
        $updated = in_array(!null, $statuses);

        return $updated;
    }
}