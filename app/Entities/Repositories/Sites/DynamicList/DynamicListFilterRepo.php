<?php

namespace App\Entities\Repositories\Sites\DynamicList;

use App\Entities\Models\Sites\DynamicLists\DynamicListFilter;
use App\Entities\Repositories\Repository;

/**
 * Class DynamicListFilterRepo
 *
 * @package App\Entities\Repositories\Sites\DynamicList
 */
class DynamicListFilterRepo extends Repository
{
    /**
     * Filter model association.
     */
    const MODEL = DynamicListFilter::class;

    public function getFullTableName()
    {
        $site_model = $this->model()->resolveSiteConnection();
        if ($site_model) {
            return $site_model;
        }
        return null;
    }

    /**
     * Get By Dynamic List Id
     *
     * @param $dynamic_list_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getByDynamicListId($dynamic_list_id)
    {
        return $this->query()
            ->where('dynamic_list_id', $dynamic_list_id)
            ->get();
    }

}