<?php

namespace App\Entities\Repositories\Sites\DynamicList;


use App\Entities\Models\Sites\DynamicLists\DynamicList;
use App\Entities\Repositories\Repository;
use App\Entities\Repositories\Sites\SiteContentTypeFieldRepo;
use App\Http\Requests\Request;
use App\Libraries\SiteConnection\Exceptions\DynamicListException;

/**
 * Class DynamicListRepo
 *
 * @package App\Entities\Repositories\Sites\DynamicList
 */
class DynamicListRepo extends Repository
{

    /**
     * Dynamic List Model
     */
    const MODEL = DynamicList::class;

    /**
     * Filters Relation.
     */
    const FILTERS_RELATION = 'filters';

    /**
     * Orders Relation.
     */
    const ORDERS_RELATION = 'orders';

    /**
     * Labels Relation.
     */
    const LABEL_RELATION = 'labels';

    /**
     * Post Relations.
     */
    const POSTS_RELATION = 'posts';


    /**
     * @var DynamicList $current_model
     */
    protected $current_model;

    /**
     * Create Dynamic List Record from Request.
     *
     * @param $request
     *
     * @return bool
     * @throws DynamicListException
     */
    public function createFromRequest(Request $request)
    {
        /*Save Dynamic list Table*/
        $dynamic_list_columns = $request->all();
        $this->current_model  = $this->model();
        $this->current_model
            ->fill($dynamic_list_columns)
            ->save();

        if ($request->get('is_static_list') == 'manual') {
            $this->createPosts($request);
            $this->current_model->update(['is_static_list' => 1]);
        } else {
            $this->createLabels($request);
            $this->createFilters($request);
            $this->createOrders($request);
            $this->current_model->update(['is_static_list' => 0]);
        }

        return $this->current_model;
    }

    /**
     * Create data to dynamic_list_posts.
     *
     * @param Request $request
     *
     * @internal param $posts
     */
    private function createPosts(Request $request)
    {
        $posts = json_decode($request->get('post_ids_nestable'));

        if (!is_array($posts)) {

            return;
        }

        foreach ($posts as $key => $val) {
            $this->current_model->posts()->attach($val->id, ['order' => $key]);
        }
    }

    /**
     * Add label into dynamic list model.
     *
     * @param Request $request
     *
     * @throws DynamicListException
     */
    private function createLabels(Request $request)
    {
        $labels = $request->get('labels');

        $this->current_model->labels()->sync($labels['id'] ?? []);
    }

    /**
     * Create filters to dynamic_list_filters.
     *
     * @param Request $request
     *
     * @return array|void
     */
    private function createFilters(Request $request)
    {
        $filters = $request->get('filters');

        if (!is_array($filters)) {
            return;
        }

        $this->storeFilters($this->prepareFilters($filters));
    }

    /**
     * Prepare Orders Request
     *
     * @param array $filters
     *
     * @return mixed
     * @internal param array $orders
     * @internal param array $filters
     */
    private function prepareFilters(array $filters)
    {
        $content_type_field_repo = new SiteContentTypeFieldRepo();
        $filters_to_loop_over    = $filters['operator'];
        $parsed_filters = [];

        foreach ($filters_to_loop_over as $index => $field) {
            if (isset($filters['field_id'][$index])) {
                $field_type = $content_type_field_repo->getTypeById($filters['field_id'][$index]);

                $parsed_filters[$index]['field_id']        = $filters['field_id'][$index];
                $parsed_filters[$index]['operator']        = $filters['operator'][$index];
                $parsed_filters[$index]['value']           = $filters['value'][$index] ?? '';
                $parsed_filters[$index]['dynamic_list_id'] = $this->current_model->id;
                $parsed_filters[$index]['type']            = $field_type ?? 'other';
            }
        }

        return $parsed_filters;
    }

    /**
     *  Store Filters.
     *
     * @param array $filters
     */
    private function storeFilters(array $filters)
    {
        $this->current_model->filters()->sync($filters);
    }

    /**
     * Create data to dynamic_list_orders.
     *
     * @param Request $request
     */
    private function createOrders(Request $request)
    {
        $orders = $request->get('orders');

        if (!is_array($orders)) {
            return;
        }

        $this->storeOrders($this->prepareOrders($orders));
    }

    /**
     * Prepare Orders Request
     *
     * @param array $orders
     *
     * @return mixed
     * @internal param array $orders
     * @internal param array $filters
     */
    private function prepareOrders(array $orders)
    {
        $content_type_field_repo = new SiteContentTypeFieldRepo();
        $orders_to_loop_over     = $orders['direction'];
        $parsed_orders = [];

        foreach ($orders_to_loop_over as $index => $val) {
            if (isset($orders['field_id'][$index])) {
                $field_type = ($content_type_field_repo->getTypeById($orders['field_id'][$index]));

                $parsed_orders[$index]['field_id']        = $orders['field_id'][$index];
                $parsed_orders[$index]['direction']       = $orders['direction'][$index];
                $parsed_orders[$index]['dynamic_list_id'] = $this->current_model->id;
                $parsed_orders[$index]['type']            = $field_type ?? 'other';
            }
        }

        return $parsed_orders;
    }

    /**
     * Store orders.
     *
     * @param array $orders
     */
    private function storeOrders(array $orders)
    {
        $this->current_model->orders()->sync($orders);
    }

    /**
     * Update process from request.
     *
     * @param DynamicList $model
     * @param Request     $request
     *
     * @return bool|null
     */
    public function updateFromRequest(DynamicList $model, Request $request)
    {
        $is_updated = null;

        $this->current_model = $model;
        $is_updated          = $this->current_model->update($request->all());

        if ($request->get('is_static_list') == 'manual') {
            $this->updatePosts($request);
            $this->current_model->update(['is_static_list' => 1]);
        } else {
            $this->updatePosts($request);
            $this->updateLabels($request);
            $this->updateOrders($request);
            $this->updateFilters($request);
            $this->current_model->update(['is_static_list' => 0]);
        }

        return $is_updated;
    }

    /**
     * Update posts in dynamic_list_posts.
     *
     * @param Request $request
     */
    private function updatePosts(Request $request)
    {
        $this->current_model->posts()->detach();

        $posts = json_decode($request->get('post_ids'));
        if (!is_array($posts)) {
            return;
        }

        foreach ($posts as $key => $val) {
            $this->current_model->posts()->attach($val->id, ['order' => $key]);
        }
    }

    /**
     * Update labels in dynamic_list_labels.
     *
     * @param Request $request
     */
    private function updateLabels(Request $request)
    {
        $labels = $request->get('labels');

        $this->current_model->labels()->sync($labels['id'] ?? []);
    }

    /**
     * Update orders in dynamic_list_orders.
     *
     * @param Request $request
     */
    private function updateOrders(Request $request)
    {
        $this->current_model->orders()->detach();

        $orders = $request->get('orders');
        if (empty($orders['field_id']) || empty($orders['direction'])) {
            return;
        }

        $this->storeOrders($this->prepareOrders($orders));
    }

    /**
     * Update filters in dynamic_list_filters.
     *
     * @param Request $request
     */
    private function updateFilters(Request $request)
    {
        $this->current_model->filters()->detach();

        $filters = $request->get('filters');

        if (empty($filters['field_id']) || empty($filters['operator']) || !isset($filters['value'])) {
            return;
        }

        $this->storeFilters($this->prepareFilters($filters));
    }

    /**
     * Return all dynamic list to select view
     *
     * @param array $where
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDynamicList (array $where)
    {
        $query = $this->query();

        $query->select('id', 'name as text');

        foreach ($where as $clause) {
            if (count($clause) == 3) {
                $query->where($clause[0], $clause[1], $clause[2]);
            }
        }

        return $query->get();
    }

    /**
     * Duplicate
     *
     * @param DynamicList $dynamic_list
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function duplicate(DynamicList $dynamic_list)
    {
        $duplicate_dynamic_list = $dynamic_list->replicate();
        $duplicate_dynamic_list->name .= ' - Duplicated';
        $duplicate_dynamic_list->save();

        $duplicate_dynamic_list->labels()->sync($dynamic_list->labels);
        $duplicate_dynamic_list->push();

        $orders = (new DynamicListOrderRepo())->getByDynamicListId($dynamic_list->id);
        $this->duplicateCollection($orders, $duplicate_dynamic_list->id);

        $filters = (new DynamicListFilterRepo())->getByDynamicListId($dynamic_list->id);
        $this->duplicateCollection($filters, $duplicate_dynamic_list->id);

        $dl_posts = (new DynamicListPostRepo())->getByDynamicListId($dynamic_list->id);
        $this->duplicateCollection($dl_posts, $duplicate_dynamic_list->id);

        return $duplicate_dynamic_list;
    }

    /**
     * Duplicate Collection
     *
     * @param $collection
     * @param $dup_dynamic_list_id
     */
    private function duplicateCollection($collection, $dup_dynamic_list_id)
    {
        foreach ($collection as $item) {
            $dup_item = $item->replicate();
            $dup_item->dynamic_list_id = $dup_dynamic_list_id;
            $dup_item->save();
        }
    }
}