<?php

namespace App\Entities\Repositories\Sites\DynamicList;

use App\Entities\Models\Sites\DynamicLists\DynamicListOrder;
use App\Entities\Repositories\Repository;

/**
 * Class DynamicListOrderRepo
 *
 * @package App\Entities\Repositories\Sites\DynamicList
 */
class DynamicListOrderRepo extends Repository
{
    /**
     * DynamicListOrder model association.
     */
    const MODEL = DynamicListOrder::class;

    /**
     * Get By Dynamic List Id
     *
     * @param $dynamic_list_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getByDynamicListId($dynamic_list_id)
    {
        return $this->query()
            ->where('dynamic_list_id', $dynamic_list_id)
            ->get();
    }
}