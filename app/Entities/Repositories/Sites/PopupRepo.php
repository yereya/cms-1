<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Popup;
use App\Entities\Models\Sites\Post;
use App\Entities\Repositories\Repository;
use Carbon\Carbon;

class PopupRepo extends Repository
{
    const MODEL = Popup::class;

    /**
     * Get Actives By Post
     *
     * @param Post $post
     *
     * @return array
     */
    public function getActivesInPage(Post $post)
    {
        $includedPopups = $post->popups()
            ->where('active', Popup::ACTIVE)
            ->where('pivot_exclude','!=','1')
            ->get();

        $excludedPopups = Popup::leftJoin('popup_post','popup_post.popup_id','=','id')
            ->where(function ($query) use ($post) {
                $query->where(function ($subQuery) use ($post){
                    $subQuery->where('popup_post.post_id','!=',$post->id)
                        ->orWhereNull('popup_post.post_id');
                });
                $query->where(function ($subQuery) use ($post){
                    $subQuery->whereIn('popup_post.popup_id',
                        Popup::where('active',Popup::ACTIVE)
                            ->where('pivot_exclude',1)
                            ->pluck('id')
                            ->toArray())
                        ->orWhereNull('popup_post.popup_id');
                });
            })
            ->groupBy('id')
            ->get();

        $popups = $includedPopups->merge($excludedPopups);

        $_this = $this;
        return $popups->filter(function ($popup){
            return $popup->shouldDisplay();
        })->map(function ($popup) use ($_this){
            return $_this->getJsParams($popup);
        })->values()->toArray();
    }

    /**
     * Duplicate
     *
     * @param Popup $popup
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function duplicate(Popup $popup)
    {
        $duplicate_popup = $popup->replicate();
        $duplicate_popup->name .= ' - Duplicated';
        $duplicate_popup->cookie_name = md5( $popup->name . time());
        $duplicate_popup->active = false;
        $duplicate_popup->save();

        return $duplicate_popup;
    }

    /**
     * Should Popup Display
     *
     * @param $popup
     *
     * @return bool
     */
    private function isActiveInPage($popup)
    {
        if (
            $popup->start_date
            && Carbon::createFromFormat('Y-m-d H:i:s', $popup->start_date)->gt(Carbon::now())
        ) {

            return false;
        } else {
            if (
                $popup->end_date
                && Carbon::createFromFormat('Y-m-d H:i:s', $popup->end_date)->lt(Carbon::now())
            ) {

                return false;
            }
        }

        return true;
    }

    /**
     * Get Popup Js Params
     *
     * @param $popups
     */
    private function getJsParams($popup)
    {
        $params         = [];
        $allowed_params = [
            'appear_event',
            'cookie_name',
            'cookie_time_on_conversion',
            'cookie_time_on_close',
            'param',
            'height',
            'width'
        ];
        foreach ($popup->toArray() as $key => $value) {
            if (!empty($value) && in_array($key, $allowed_params)) {
                $params[$key] = $value;
            }
        }

        $params['url'] = route('get-template.{template}', [$popup->template_id]);

        return $params;
    }
}