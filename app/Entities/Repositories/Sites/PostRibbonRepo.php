<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\PostRibbon;
use App\Entities\Repositories\Repository;

class PostRibbonRepo extends Repository
{
    const MODEL = PostRibbon::class;

    /**
     * Post Ribbons
     *
     * @param $dynamic_list_id
     *
     * @return array
     */
    public function postRibbon($dynamic_list_id) :array
    {
        return $dynamic_list_id
            ? $this->model()
                ->where('dynamic_list_id', $dynamic_list_id)
                ->get()
                ->pluck('post_id', 'ribbon_id')
                ->toArray()
            : [];
    }

    /**
     * Ribbons post by dynamic list id
     *
     * @param $dynamic_list_id
     *
     * @return array
     */
    public function ribbonPostByDynamicList($dynamic_list_id)
    {
        return $dynamic_list_id
            ? $this->model()
                ->where('dynamic_list_id', $dynamic_list_id)
                ->with('ribbon')
                ->get()
                ->keyBy('post_id')
            : [];
    }
}