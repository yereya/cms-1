<?php


namespace App\Entities\Repositories\Sites;


use App\Entities\Models\Sites\PageContentType;
use App\Entities\Repositories\Repository;

class PageContentTypeRepo extends Repository
{
    const MODEL = PageContentType::class;

    /**
     * Get Posts Priorities
     *
     * @param $posts
     *
     * @return mixed
     */
    public function getPostsPriority($posts)
    {
        $post_ids = $posts->pluck('id');

        return $this->query()
            ->select(['post_id', 'priority'])
            ->whereIn('post_id', $post_ids)
            ->get()
            ->keyBy('post_id');
    }

    /**
     * Get a list of pages for a given content type id.
     *
     * @param integer $id
     *
     * @return Collection
     */
    public function getPagesByContentTypeId($id)
    {
        $page_repo = new PageRepo();

        $ids = $this->model()
            ->where('content_type_id', $id)
            ->get()
            ->pluck('post_id')
            ->toArray();

        return $page_repo->model()->whereIn('post_id', $ids)->get();
    }

}