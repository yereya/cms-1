<?php


namespace App\Entities\Repositories\Sites;


use App\Entities\Models\Sites\PostLike;
use App\Entities\Repositories\Repository;
use App\Http\Requests\PostLikeRequest;

class PostLikeRepo extends Repository
{

    /**
     * Post Like Model
     */
    const MODEL = PostLike::class;


    /**
     * Save From Request
     *
     * @param $request
     *
     * @return bool
     */
    public function saveFromRequest(PostLikeRequest $request)
    {
        $model = $this->resolveModel($request->input('post_id'));
        $this->incrementLikeDislikeRecord($model, $request);

        if (!$model->exists) {
            $model->post_id = $request->input('post_id');
        }

        return $model->save();
    }

    /**
     * Increment Rating Record
     *
     * @param $model
     * @param $request
     */
    private function incrementLikeDislikeRecord($model, $request)
    {
        $like_dislike = $request->input('type');
        $model->{$like_dislike}++;
    }

    /**
     * Resolve Model :
     * - if there's model with post_id return it;
     * - else return new model
     *
     * @param $post_id
     *
     * @return \Illuminate\Database\Eloquent\Model|mixed
     */
    private function resolveModel($post_id)
    {
        $model = $this->query()
            ->where('post_id', $post_id)
            ->first();

        if (!isset($model)) {
            $model = $this->model();
        }

        return $model;
    }

}