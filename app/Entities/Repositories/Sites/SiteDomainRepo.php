<?php

namespace App\Entities\Repositories\Sites;

use Mockery\Exception;
use App\Http\Requests\Request;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteDomain;
use App\Entities\Repositories\Repository;
use App\Http\Requests\DomainGeneratorFormRequest;
use App\Http\Requests\Sites\SiteDomainsFormRequest;

class SiteDomainRepo extends Repository
{
    /**
     * @ver SiteDomain
     */
    const MODEL = SiteDomain::class;

    /**
     * Get the current domain base url.
     *
     * @param integer $site_id
     *
     * @return string
     */
    public function getBaseUrl($site_id)
    {
        $domain = $this->getBySite($site_id);

        /**
         * Get the protocol from the env file to support
         * both http and https protocols.
         */
        $protocol = env('SSL', false) ? 'https' : 'http';

        $url = $protocol . '://' . $domain->domain;

        return $url;
    }

    /**
     * Get Load All Widgets Url
     *
     * @param $site_id
     *
     * @return string
     */
    public function getLoadAllWidgetsUrl($site_id)
    {
        return $this->getBaseUrl($site_id) . '/load-widgets-templates';
    }

    /**
     * Get by Site Id
     *
     * @param $site_id
     *
     * @return mixed
     */
    public function getBySite($site_id)
    {

        return $this->query()->where('site_id', $site_id)
            ->where('domain_dev', (env('APP_ENV') == 'local'))
            ->first();
    }

    /**
     * Save From Request
     *
     * @param      $request
     * @param Site $site
     *
     * @return bool
     */
    public function saveFromRequest(Site $site, Request $request)
    {
        $this->deleteBySite($site);
        $prefixes = [null, 'dev', 'test', 'staging'];

        foreach ($prefixes as $prefix) {
            $domain            = !is_null($prefix) ? $prefix . '.' . $request->domain : $request->domain;
            $model             = $this->model();
            $model->site_id    = $site->id;
            $model->domain     = $domain;
            $model->domain_dev = $prefix == 'dev' ? 1 : 0;
            $is_added          = $model->save();

            if (!$is_added) {
                throw new Exception('There was an error while trying saving site domains');
            }
        }

        return true;
    }

    /**
     * Delete sites by a given site id.
     *
     * @param integer $site_id
     *
     * @return boolean
     */
    private function deleteBySite($site)
    {
        $models = $this->query()
            ->where('site_id', $site->id)
            ->get();

        if (count($models)) {
            foreach ($models as $model) {
                $this->delete($model);
            }
        }
    }
}