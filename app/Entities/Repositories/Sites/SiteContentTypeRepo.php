<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Repository;
use App\Facades\SiteConnectionLib;
use App\Http\Requests\Sites\SiteContentTypesFormRequest;

class SiteContentTypeRepo extends Repository
{
    const MODEL = SiteContentType::class;
    const PAGE_CONTENT_TYPE_TABLE = 'pages';

    /**
     * isPage
     * Checks if the content type is a page
     * or some other generic content type
     * // TODO need to add caching
     *
     * @param Post|Integer $content_type
     *
     * @return bool
     */
    public function isPage($content_type)
    {
        // If Post object
        if (is_int($content_type)) {
            $content_type = $this->find($content_type);
        }

        if ($content_type) {
            return $content_type->contentType->table == self::PAGE_CONTENT_TYPE_TABLE;
        }

        return false;
    }

    /**
     * getPageContentTypeId
     *
     * @return mixed
     */
    public function getPageContentTypeId()
    {
        $site = SiteConnectionLib::getSite();

        $result = $this->query()
            ->page()
            ->where('site_id', $site->id);

        $model = $result->first();

        return $model ? $model->id : $model;

    }

    /**
     * Get By Slug
     *
     * @param $slug
     * @return mixed
     */
    public function getBySlug($slug)
    {
        return $this->query()->where('slug', $slug)->first();
    }

    /**
     * Get All Exclude pages
     *
     * @param $site_id
     *
     * @return mixed
     */
    public function getBySiteId($site_id)
    {
        $content_types = $this->query()
            ->where('site_id', $site_id)
            ->get();

        return $content_types;
    }

    /**
     * Add Slug Param to request
     *
     * @param $request
     */
    public function addSlugParamToRequest(SiteContentTypesFormRequest $request)
    {
        $slug = $request->input('table');
        $request->request->add(['slug' => $slug]);
    }

    /**
     *  Add  content type => pages
     *
     * @param Site $site
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function addContentTypePages(Site $site)
    {
        $params = [
            'site_id'     => $site->id,
            'name'        => 'Pages',
            'slug'        => 'pages',
            'table'       => 'pages',
            'is_router'   => 1,
            'has_comment' => 0,
            'has_likes'   => 0
        ];

        $model = $this->model();
        $model->fill($params);
        $model->save();

        return $model;
    }
}
