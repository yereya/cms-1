<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\ContentTypes\LinkContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\Post;
use App\Entities\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;

class LinkContentTypeRepo extends Repository
{
    const MODEL = LinkContentType::class;

    /**
     * Get linked content type
     *
     * @param $fields
     * @param $post_id
     *
     * @return array
     */
    public function getContentTypeLink($fields, $post_id)
    {
        $fields = $fields->filter(function ($item) {
            return $item->type == 'content_type_link';
        });

        $connected_content_types = [];

        foreach ($fields as $field) {
            $connected_content_types[$field->name] = $this->model()
                ->where('post_id', $post_id)
                ->where('field_id', $field->id)
                //->orderBy('priority')
                ->get()
                ->pluck('connect_to_post_id')
                ->toJson();
        }

        return $connected_content_types;
    }

    /**
     * Helper to update synchronized content type link
     * Duplicate this field to linked content type
     *
     * @param $values
     * @param $field_name
     */
    public function syncContentTypeLinkUpdate($values, $field_name)
    {
        $values                  = $this->syncValues($values, 'update');
        $current_content_type_id = $values['metadata']['current_content_type_id'];
        $content_type            = SiteContentType::find($current_content_type_id);

        $sync_field = SiteContentTypeField::where('type_id', $current_content_type_id)
            ->where('name', $field_name)
            ->first();

        $old_field_name       = $sync_field->name;
        $sync_field->metadata = json_encode($values['metadata']);
        unset($values['metadata']);
        unset($values['field_group_id']);
        $sync_field->fill($values);
        $sync_field->save();
    }

    /**
     * Change values to content type link
     *
     * @param      $values
     * @param null $type
     *
     * @return mixed
     */
    private function syncValues($values, $type = null)
    {
        $content_type_id                               = $values['metadata']['current_content_type_id'];
        $current_content_type_id                       = $values['metadata']['content_type_id'];
        $values['metadata']['content_type_id']         = $content_type_id;
        $values['metadata']['current_content_type_id'] = $current_content_type_id;
        $values['type_id']                             = $current_content_type_id;

        if ($type == 'update') {
            unset($values['display_name']);
        }

        return $values;
    }

    /**
     * Helper to synchronize content type link
     * Duplicate this field to linked content type
     *
     * @param $values
     */
    public function syncContentTypeLinkStore($values)
    {
        $values     = $this->syncValues($values);
        $sync_field = new SiteContentTypeField();

        $current_content_type_id = $values['metadata']['current_content_type_id'];
        $content_type            = SiteContentType::find($current_content_type_id);
        $sync_field->metadata    = json_encode($values['metadata']);
        unset($values['metadata']);
        unset($values['field_group_id']);
        $sync_field->fill($values);
        //$sync_field->priority = (new SiteContentTypeFieldRepo())->getCalculatedFieldPriority($content_type);
        $sync_field->save();
    }

    /**
     * Remove all linked posts by field id
     *
     * @param $field_id
     */
    public function removeByFieldId($field_id)
    {
        $this->model()
            ->where('field_id', $field_id)
            ->delete();
    }

    /**
     * Find linked content type
     *
     * @param SiteContentTypeField $field
     *
     * @return mixed
     */
    public function findLinkedContentType(SiteContentTypeField $field)
    {
        $metadata = json_decode($field->metadata);

        $sync_content_type = SiteContentType::find($metadata->content_type_id);

        return $sync_content_type;
    }

    /**
     * Return Linked field
     *
     * @param SiteContentType      $sync_content_type
     * @param SiteContentTypeField $field
     *
     * @return mixed
     */
    public function findLinkedField(SiteContentType $sync_content_type, SiteContentTypeField $field)
    {
        return SiteContentTypeField::where('type_id', $sync_content_type->id)
            ->where('name', $field->name)
            ->first();
    }

    /**
     * Return all linked posts to current Post
     *
     * @param Post            $post
     * @param Collection|null $fields
     * @param bool            $allow_drafts
     *
     * @return Collection|null
     */
    public function getLinkedPosts(Post $post, Collection $fields, $allow_drafts = false)
    {
        $linked_post_ids = $this->query()
            ->where('post_id', $post->id)
            ->whereIn('field_id', $fields->pluck('id')->toArray())
            ->get()
            ->pluck('connect_to_post_id')
            ->toArray();

        $filtered_post_ids = !empty(`$allow_drafts`)
            ? Post::whereIn('id', $linked_post_ids)
                ->published($allow_drafts)
                ->get()
                ->pluck('id')
                ->toArray()
            : $linked_post_ids;

        return (new PostRepo())->getByIds($filtered_post_ids);
    }

    /**
     * Delete rows from db
     *
     * @param $post_id
     * @param $field_id
     *
     * @return string
     */
    public function removeLinksByPostIdAndFieldId($post_id, $field_id)
    {
        $this->query()
            ->where('post_id', $post_id)
            ->where('field_id', $field_id)
            ->delete();

        $this->query()
            ->where('connect_to_post_id', $post_id)
            ->where('field_id', $field_id)
            ->delete();
    }

    /**
     * Duplicate For Post
     *
     * @param Post $post
     * @param Post $duplicate_post
     */
    public function duplicateForPost(Post $post, Post $duplicate_post)
    {
        $links = $this->query()
            ->where('post_id', $post->id)
            ->get();
        foreach ($links as $link) {
            $dup_link = $link->replicate();
            $dup_link->post_id = $duplicate_post->id;
            $dup_link->save();
        }

        $links = $this->query()
            ->where('connect_to_post_id', $post->id)
            ->get();

        foreach ($links as $link) {
            $dup_link = $link->replicate();
            $dup_link->connect_to_post_id = $duplicate_post->id;
            $dup_link->save();
        }
    }
}