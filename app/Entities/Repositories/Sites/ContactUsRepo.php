<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\ContactUs;
use App\Entities\Repositories\Repository;

class ContactUsRepo extends Repository
{
    const MODEL = ContactUs::class;

    /**
     * @param \Illuminate\Database\Eloquent\Model $request
     *
     * @return  contact_us_record
     */
    public function prepareContactUsData($request)
    {
        $contact_us_record = $this->model();
        $filtered_request  = $request->except('_token', 'create_another');

        foreach ($filtered_request as $field_name => $field_value) {
            $contact_us_record->$field_name = $field_value;
        }

        return $contact_us_record;
    }

    /**
     * Save
     *
     * @param \Illuminate\Database\Eloquent\Model $request
     *
     * @return Newsletter
     */
    public function saveMessage($request)
    {
        $params     = $request->only('name', 'email', 'message');
        $contact_us = $this->model();
        $contact_us->fill($params);
        $this->save($contact_us);

        return $contact_us;
    }
}