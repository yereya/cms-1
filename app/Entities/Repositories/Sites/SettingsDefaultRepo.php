<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\SiteSettingDefault;
use App\Entities\Repositories\SiteSettingRepo;

class SettingsDefaultRepo
{
    /**
     * Set Default Setting For Type
     *
     * @param $site_id
     * @param $setting_type
     * @param $default_type
     */
    public static function setDefaultSettingForType($site_id, $setting_type, $default_type)
    {
        $default_settings = self::getByType($default_type);
        if (!$default_settings) {

            return;
        }

        (new SiteSettingRepo())->insertNewDefaultSettings($site_id, $setting_type, $default_settings);
    }


    /**
     * Get By Type
     *
     * @param $default_type
     *
     * @return mixed
     */
    public static function getByType($default_type)
    {
        $settings = SiteSettingDefault::where('type', $default_type)
            ->with('field_type')
            ->orderBy('priority')
            ->get();

        return $settings;
    }

}

