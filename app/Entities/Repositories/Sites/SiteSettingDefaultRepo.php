<?php


namespace App\Entities\Repositories\Sites;


use App\Entities\Models\Sites\SiteSettingDefault;
use App\Entities\Repositories\Repository;

class SiteSettingDefaultRepo extends Repository
{
    const MODEL = SiteSettingDefault::class;
}