<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Media;
use App\Entities\Models\Sites\SiteSetting;
use App\Entities\Models\Sites\Widgets\FieldType;
use App\Entities\Repositories\Repository;
use App\Facades\SiteConnectionLib;
use DB;

class SettingsRepo extends Repository
{
    const HOMEPAGE = 'homepage';


    const MODEL = SiteSetting::class;
    /**
     * Get Setting
     *
     * @param $site_id
     * @param $type
     * @param $key
     *
     * @return mixed
     */
    public static function getSetting($site_id, $type, $key)
    {
        $setting = self::getSettings($site_id, $type);

        return ($setting->keyBy('key'))[$key] ?? null;
    }

    /**
     * Get Settings
     *
     * @param $site_id
     * @param $type
     *
     * @return mixed
     */
    public static function getSettings($site_id, $type)
    {
        if(!is_array($type)){
            $type = [$type];
        }
        $settings = SiteSetting::where('site_id', $site_id)
            ->whereIn('type', $type)
            ->with('field_type')
            ->orderBy('priority')
            ->get();

        return $settings;
    }


    /**
     * Get Settings with Extra Params
     *
     * @param $site_id
     * @param $type
     *
     * @return mixed
     */
    public static function getSettingsWithExtraParams($site_id, $type)
    {
        $settings = self::getSettings($site_id, $type);
        $settings = self::getExtraParams($settings);

        return $settings;
    }

    /**
     * Save Settings
     *
     * @param $site_id
     * @param $type
     * @param $settings_to_update
     */
    public static function saveSettings($site_id, $type, $settings_to_update)
    {
        $db_settings = self::getSettings($site_id, $type);
        foreach ($db_settings as $param) {
            $param->value = self::getFieldValue($settings_to_update, $param->key, $param->field_type->name);
            $param->save();
        }
    }

    /**
     * Save Setting
     *
     * @param int    $site_id
     * @param string $type
     * @param string $key
     * @param mixed  $value
     * @param string $name
     * @param int    $filed_type_id
     *
     * @return bool
     */
    public static function saveSetting($site_id, $type, $key, $value = null, $name = null, $filed_type_id = 1)
    {
        $setting = SiteSetting::firstOrNew(compact('site_id', 'type', 'key'));

        if (!$setting->exists) {
            $setting->name          = $name;
            $setting->field_type_id = $filed_type_id;
        }

        $setting->value = $value;

        return $setting->save();
    }

    /**
     * Insert New Default Settings
     *
     * @param $site_id
     * @param $type
     * @param $default_settings
     */
    public static function insertNewDefaultSettings($site_id, $type, $default_settings)
    {
        foreach ($default_settings as $param) {
            $new_set          = new SiteSetting($param->getAttributes());
            $new_set->site_id = $site_id;
            $new_set->type    = $type;
            $new_set->save();
        }
    }

    /**
     * Get Extra params
     * - bring lists for select fields with query
     * - bring media object for media fields
     *
     * @param $settings
     *
     * @return mixed
     */
    private static function getExtraParams($settings)
    {
        /** @var  $param SiteSetting */
        foreach ($settings as $param) {
            $field_name = $param->field_type->name;
            if (($field_name == FieldType::TYPE_MULTI_SELECT
                || $field_name == FieldType::TYPE_SELECT)
            ) {
                if (!empty($param->options_query)) {
                    $param->list = Collect(DB::select($param->options_query))->pluck('text', 'id')->toArray();
                }
            } else {
                if ($field_name == FieldType::TYPE_MEDIA) {
                    $param->media = Media::find($param->value);
                }
            }
        }

        return $settings;
    }

    /**
     * Get Field Value
     *
     * @param $settings_to_update
     * @param $key
     * @param $field_name
     *
     * @return int|string
     */
    private static function getFieldValue($settings_to_update, $key, $field_name)
    {
        if ($field_name == FieldType::TYPE_MULTI_SELECT) {
            return json_encode($settings_to_update[$key]);
        } elseif ($field_name == FieldType::TYPE_SELECT) {
            return $settings_to_update[$key] ?? null;
        } elseif ($field_name == FieldType::TYPE_CHECKBOX) {
            return $settings_to_update[$key] ?? 0;
        } else {
            return $settings_to_update[$key];
        }
    }

    public function getByKey($key)
    {
        $site = SiteConnectionLib::getSite();

        return $this->query()
            ->where('site_id',$site->id)
            ->where('key', $key)
            ->first();
    }
}