<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Like;
use App\Entities\Repositories\Repository;

class LikesRepo extends Repository
{
    const MODEL = Like::class;

    /**
     * Add Like
     *
     * @param $user_id
     * @param $session_id
     * @param $post_id
     *
     * @return bool
     * @throws Exception
     */
    public function addLike($user_id, $session_id, $post_id)
    {
        if ($user_id) {
            $column = 'user_id';

            return $this->addByColumn($column, $user_id, $post_id);

        } elseif ($session_id) {
            $column = 'session_id';

            return $this->addByColumn($column, $session_id, $post_id);

        } else {

            throw new \Exception("user_id or session_id must be given");
        }
    }

    /**
     * UnLike
     *
     * @param $user_id
     * @param $session_id
     * @param $post_id
     *
     * @throws Exception
     */
    public function unLike($user_id, $session_id, $post_id)
    {
        if ($user_id) {
            $column = 'user_id';

            return $this->unLikeByColumn($column, $user_id, $post_id);
        } elseif ($session_id) {
            $column = 'session_id';

            return $this->unLikeByColumn($column, $session_id, $post_id);
        } else {
            throw new \Exception("user_id or session_id must be given");
        }

    }

    /**
     * Get Post Count
     *
     * @param $post_id
     *
     * @return mixed
     */
    public function getPostCount($post_id)
    {
        return $this->query()
            ->where('post_id', $post_id)
            ->count();
    }

    /**
     * Add By Column
     *
     * @param $user_id
     * @param $post_id
     *
     * @return bool
     */
    private function addByColumn($column, $value, $post_id)
    {
        $like = new Like();
        $like->{$column} = $value;
        $like->post_id = $post_id;
        $like->save();

        return true;
    }

    /**
     * Unlike by Column
     *
     * @param $column
     * @param $value
     * @param $post_id
     */
    private function unLikeByColumn($column, $value, $post_id)
    {
        Like::where($column, $value)
            ->where('post_id', $post_id)
            ->delete();
    }

}