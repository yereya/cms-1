<?php namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Page;
use App\Entities\Models\Sites\Post;
use App\Entities\Repositories\Repository;
use App\Entities\Repositories\Sites\Widgets\SiteWidgetTemplateRepo;
use App\Entities\Repositories\SiteSettingRepo;
use App\Http\Traits\TreeToArrayTrait;
use App\Libraries\PageInfo\PagesUrls;
use Illuminate\Support\Collection;

class PageRepo extends Repository
{
    use TreeToArrayTrait;
    const MODEL = Page::class;

    /**
     * Get Homepage
     *
     * @return Page
     */
    public function getHomepage()
    {
        $homepage_post_id = (new SiteSettingRepo())->getByKey('homepage')->value;

        return $this->query()
            ->where('post_id', $homepage_post_id)
            ->first();
    }

    /**
     * Get Page By ID
     *
     * @param int $id
     *
     * @return Page
     */
    public function getByID($id)
    {
        return $this->query()
            ->where('id', $id)
            ->first();
    }

    /**
     * Get Homepage
     *
     * @return Page
     */
    public function get404()
    {
        $homepage_post_id = (new SiteSettingRepo())->getByKey('404_page')->value;

        return $this->query()
            ->where('post_id', $homepage_post_id)
            ->first();
    }

    /**
     * Get All In Select Format
     *
     * @return array
     */
    public function getAllInSelectFormat()
    {
        return (new PostRepo())->getAllInSelectFormat();
    }

    /**
     * Get Url (slug only)
     *
     * @return string
     * @throws \Exception
     */
    public function pageListToSelect()
    {
        return $this->model()
            ->select('id', 'name as text')
            ->orderBy('priority')
            ->get();
    }

    /**
     * Return pages with paths
     * [
     *      title: page name,
     *      value: https://jkjlkjk
     * ]
     *
     * @return array
     */
    public function pageListWithPaths()
    {
        $routes = new PagesUrls();

        $pages = $routes->listRoutes();

        foreach ($pages as $key => $href) {
            $pages_with_paths[] = [
                'title' => $key,
                'value' => $href
            ];
        }

        return $pages_with_paths;
    }

    /**
     * Create href to single page
     *
     * @param $site_id
     * @param $parent_id - page parent id
     *
     * @return string
     */
    public function buildHref($site_id, $parent_id = null)
    {
        $site_domain = new SiteDomainRepo();

        if (env('APP_ENV') == 'local') {
            $domains = $site_domain->model()
                ->where('site_id', $site_id)
                ->where('domain_dev', true)
                ->first();
        } else {
            $domains = $site_domain->model()
                ->where('site_id', $site_id)
                ->where('domain_dev', false)
                ->first();
        }

        $url  = $domains->domain ?? getBaseUrl(); //if domain not found insert base url
        $slug = DIRECTORY_SEPARATOR;

        return $url . $this->getUrl($parent_id) . $slug;
    }

    /**
     * Get Url (slug only)
     *
     * @param $post_id
     *
     * @return string
     * @throws \Exception
     */
    public function getUrl($post_id)
    {
        if (empty($post_id)) {
            return null;
        }

        $post = (new PostRepo())->getByIdWithParentRecursive($post_id);

        if (!count($post)) {
            throw new \Exception("Page could not be found. [post id: $post_id]");
        }

        $url = $this->buildUrl($post);

        return rtrim($url, DIRECTORY_SEPARATOR);
    }

    /**
     * Get Full Url
     *
     * @param $domain
     * @param $post_id
     *
     * @return string
     */
    public function getFullUrl($domain, $post_id)
    {
        return 'http://' . $domain . $this->getUrl($post_id);
    }

    /**
     * Duplicate
     *
     * @param $post
     * @param $duplicated_post
     */
    public function duplicate($post, $duplicated_post)
    {
        foreach ($post->page->pageContentTypes ?? [] as $page_single) {
            $dup_page_single          = $page_single->replicate();
            $dup_page_single->post_id = $duplicated_post->id;
            $dup_page_single->save();
        }

        $post->page->is_homepage = 0;
    }

    /**
     * buildUrl
     *
     * @param Post   $page
     * @param string $slug
     *
     * @return string
     */
    private function buildUrl(Post $page, &$slug = '')
    {
        if (is_null($page->parentRecursive)) {
            $slug = '/' . $page->slug . '/' . $slug;

            return $slug;
        } else {
            $slug = $page->slug . '/' . $slug;

            return $this->buildUrl($page->parentRecursive, $slug);
        }
    }


    /**
     * Get Route List Of All Pages - single and pages from api
     *
     *
     * @return Collection
     */
    public function getRouteListFromApi() : Collection
    {
        $pages = new PagesUrls();

        return $pages->resolvePagesList()
            ->sortBy('text');
    }

    /**
     * Create home page
     *
     * @param $content_type_page
     * @param $model
     *
     * @return mixed
     */
    public function createHomePage($content_type_page, $model)
    {
        /*Create blade*/
        $blades_repo = (new SiteWidgetTemplateRepo());
        $blade = $blades_repo->createHomePage($model);

        /*Create template */
        $template_repo = (new TemplateRepo());
        $template = $template_repo->createHomePage($blade);

        /*Create post and page*/
        $post_repo = (new PostRepo());
        $post_repo->createHomePage($template, $content_type_page);

        /*Update home page and 404_page value to post_id*/
        (new SettingsRepo())->updateByKey($model,'homepage');
        (new SettingsRepo())->updateByKey($model,'404_page');

        /*Flush and publish all templates*/
        $blades_repo->flushBlade($blade,$model);
        $blades_repo->publishAll($model);
    }
}
