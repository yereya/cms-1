<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\ContentAuthor;
use App\Entities\Repositories\Repository;
use DB;

class  ContentAuthorRepo extends Repository
{
    const MODEL = ContentAuthor::class;

    /**
     * Get Id Name List
     *
     * @return mixed
     */
    public function getIdNameList()
    {
        return  ContentAuthor::select('id', DB::raw('CONCAT(first_name, " ", last_name) as text'))
            ->get();
    }
}
