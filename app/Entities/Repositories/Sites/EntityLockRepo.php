<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\EntityLock;
use App\Entities\Models\Users\User;
use App\Entities\Repositories\Repository;
use Carbon\Carbon;

class EntityLockRepo extends Repository
{
    const MODEL = EntityLock::class;

    const LOCK_CREATED = 'LOCK_CREATED';
    const LOCK_ERROR = 'LOCK_ERROR';

    /**
     * Create Lock
     *
     * @param User $user
     * @param string $type
     * @param int $entity_id
     * @param null $site_id
     * @return EntityLock|bool|void
     */
    public function createLock(User $user, string $type, int $entity_id, $site_id = null)
    {
        $lock = $this->getLock($type, $entity_id, $site_id);
        if ($lock) {

            return $this->updateLock($lock, $user);
        } else {

            return $this->createNewLock($user, $type, $entity_id, $site_id);
        }
    }

    /**
     * Create Hard Lock
     *
     * @param User $user
     * @param string $type
     * @param int $entity_id
     * @param null $site_id
     * @return EntityLock|mixed
     */
    public function createHardLock(User $user, string $type, int $entity_id, $site_id = null)
    {
        $lock = $this->getLock($type, $entity_id, $site_id);
        if (!$lock) {
            $lock = $this->createNewLock($user, $type, $entity_id, $site_id);
        } else {
            $lock = $this->updateLockUser($lock, $user);
        }

        return [
            'status' => self::LOCK_CREATED,
            'lock' => $lock
        ];
    }

    /**
     * Is My Lock
     *
     * @param User $user
     * @param string $type
     * @param int $entity_id
     * @param int|null $site_id
     * @return bool
     */
    public function isMyLock(User $user, string $type, int $entity_id, int $site_id = null)
    {
        $lock = $this->getLock($type, $entity_id, $site_id);
        if ($lock && $lock->user_id != $user->id) {

            return false;
        } else {

            return true;
        }
    }

    /**
     * Get Lock
     *
     * @param string $type
     * @param int $entity_id
     * @param int|null $site_id
     * @return mixed
     */
    public function getLock(string $type, int $entity_id, int $site_id = null)
    {
        $query = $this->query()
            ->where('type', $type)
            ->where('entity_id', $entity_id);

        if ($site_id) {
            $query->where('site_id', $site_id);
        }

        return $query->first();
    }

    /**
     * Get All Locks For Type
     *
     * @param $site_id
     * @param $type
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAllLocksForType($site_id, $type)
    {
        $since_when_to_check = Carbon::now()
            ->subSeconds(config('app.lock_timeout'))
            ->format('Y-m-d H:i:s');

        return $this->query()
            ->where('updated_at', '>', $since_when_to_check)
            ->where('site_id', $site_id)
            ->where('type', $type)
            ->with('user')
            ->get();
    }

    /**
     * Create New Lock
     *
     * @param User $user
     * @param string $type
     * @param int $entity_id
     * @param $site_id
     * @return EntityLock
     */
    private function createNewLock(User $user, string $type, int $entity_id, $site_id)
    {
        /**
         * @var  $model EntityLock
         */
        $model = $this->model();
        $model->user_id = $user->id;
        $model->site_id = $site_id ?? null;
        $model->type = $type;
        $model->entity_id = $entity_id;
        $model->save();

        return [
            'status' => self::LOCK_CREATED,
            'lock' => $model
        ];
    }

    /**
     * Update Lock
     *
     * @param $lock
     * @param $user
     * @return EntityLock|bool
     */
    private function updateLock($lock, $user)
    {
        if ($lock->user_id == $user->id) {
            $this->updateLockTime($lock);

            $status = self::LOCK_CREATED;
        } else {
            if ($this->isActiveLock($lock)) {

                $status = self::LOCK_ERROR;
            } else {

                $lock = $this->updateLockUser($lock, $user);
                $status = self::LOCK_CREATED;
            }
        }

        return [
            'status' => $status,
            'lock' => $lock
        ];
    }

    /**
     * Update Lock User
     *
     * @param EntityLock $lock
     * @param User $user
     * @return EntityLock
     */
    private function updateLockUser(EntityLock $lock, User $user)
    {
        $lock->user_id = $user->id;
        $lock->save();

        return $lock;
    }

    /**
     * Update Lock time
     *
     * @param $lock
     * @return mixed
     */
    private function updateLockTime($lock)
    {
        $lock->touch();

        return $lock;
    }

    /**
     * Is Active Lock
     *
     * @param $lock
     * @return bool
     */
    private function isActiveLock($lock)
    {
        $lock_time = Carbon::createFromFormat('Y-m-d H:i:s', $lock->updated_at);
        $timeDiff = $lock_time->diffInSeconds(Carbon::now());

        return $timeDiff < config('app.lock_timeout');
    }
}