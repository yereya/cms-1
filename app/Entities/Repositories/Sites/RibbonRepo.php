<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Ribbon;
use App\Entities\Repositories\Repository;

class RibbonRepo extends Repository
{
    const MODEL = Ribbon::class;

    /**
     * Fill
     *
     * @param $values
     *
     * @return bool
     */
    public function fill(array $values) {
        $ribbon = $this->model()->fill($this->validate($values));

        return $ribbon->save();
    }

    /**
     * Update
     *
     * @param Ribbon $ribbon
     * @param $values
     *
     * @return mixed
     */
    public function updateRibbon(Ribbon $ribbon, $values) {
        return $this->update($ribbon, $this->validate($values));
    }

    /**
     * Delete post ribbons by dynamic list
     *
     * @param $dynamic_list_id
     */
    public function forgetByDynamicList($dynamic_list_id) {
        (new PostRibbonRepo())->model()->where('dynamic_list_id', $dynamic_list_id)->delete();
    }

    /**
     * Save post ribbon
     *
     * @param $ribbons_with_post
     */
    public function saveRibbonsPerPost(array $ribbons_with_post) {
        (new PostRibbonRepo())->model()->insert($ribbons_with_post);
    }

    /**
     * Validate values
     *
     * @param $values
     *
     * @return array
     */
    private function validate(array $values) {
        $valid = [];
        foreach ($values as $key => $value) {
            if (!empty($value)) {
                $valid[$key] = $value;
            }
        }

        return $valid;
    }
}