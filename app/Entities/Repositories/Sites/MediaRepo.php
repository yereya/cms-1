<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Media;
use App\Entities\Repositories\Repository;

class MediaRepo extends Repository
{
    const MODEL = Media::class;

    /**
     * Replace Image Id To Path
     *
     * @param $post
     * @param $post_fields
     *
     * @return mixed
     */
    public function replaceImageIdToPath($post, $post_fields)
    {
        $images_column_name = $post_fields->filter(function ($item) {
            return $item->type == 'image';
        })->pluck('name', 'id');

        $images_id = [];
        foreach ($images_column_name as $name) {
            if ($post->{$name}) {
                $images_id[$name] = $post->{$name};
            }
        }

        $medias = $this->model()
            ->whereIn('id', array_values($images_id))
            ->get()
            ->keyBy('id');

        foreach ($images_id as $key => $value) {
            $post->{$key} = $medias->get($value) ? $medias->get($value)->getFullMediaPath() : null;
            $post->{$key . '_media_id'} = $value;
        }

        return $post;
    }

    /**
     * Find media by name
     *
     * @param $name
     * @param $with
     *
     * @return mixed
     */
    public function findByName ($name, $with) {
        if (empty($name)) {
            return null;
        }

        // elFinder return name with folder name - db save filename without folder name
        $remove_substring = 'images/';
        if (strpos($name, $remove_substring) !== false) {
            $name = substr($name, strpos($name, $remove_substring) + strlen($remove_substring));
        }

        $model = $this->model()
            ->where('path', DIRECTORY_SEPARATOR . $name);
        if ($with) {
            $model->with($with);
        }

        return $model->first();
    }

    /**
     * Find media
     *
     * @param $label_id
     *
     * @return mixed
     */
    public function findByLabel($label_id) {
        return $this->model()
            ->whereHas('labels', function ($label) use ($label_id) {
                $label->where('label_id', $label_id);
            })
            ->get();
    }
}
