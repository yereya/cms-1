<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Label;
use App\Entities\Repositories\Repository;
use App\Http\Traits\TreeToArrayTrait;
use Illuminate\Support\Collection;

class LabelRepo extends Repository
{
    const MODEL = Label::class;

    use TreeToArrayTrait;

    /**
     * Get All In Select Format
     *
     * @return array
     */
    public static function getAllInSelectFormat()
    {
        $label_tree = Label::getAsTree();

        return self::convertTreeToSelectFormat($label_tree);
    }


    /**
     * Store From Request
     *
     * @return $this
     */
    public function storeFromRequest()
    {
        $label      = $this->model();
        $attributes = request()->all();
        $label->fill($attributes);
        $label->parent_id = request()->get('parent_id') > 0 ? request()->get('parent_id') : null;
        $label->save();

        return $label;
    }

    /**
     * Get All Childrens By Id
     *
     * @param array $id
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @internal param array $ids
     */
    public function getChildrenById($id)
    {
        return $this->query()
            ->where('parent_id', $id)
            ->get();
    }


    /**
     * Assign Active Post
     *
     * @param Collection $labels
     * @param array      $active_label_ids
     *
     * @return mixed
     * @internal param $post_id
     */
    public function assignActivePost(Collection $labels, array $active_label_ids)
    {
        $labels->each(function ($value) use ($active_label_ids) {
            $value->active = in_array($value->id, $active_label_ids);
        });
    }
}
