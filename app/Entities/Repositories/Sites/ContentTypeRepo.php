<?php

namespace App\Entities\Repositories\Sites;


use App\Entities\Models\Sites\ContentType;
use App\Entities\Models\Sites\ContentTypes\ContentTypeLink;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\Post;
use App\Entities\Repositories\Repository;
use App\Facades\SiteConnectionLib;
use DB;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Mockery\Exception;


class ContentTypeRepo extends Repository
{
    const MODEL = ContentType::class;

    /**
     * Get Table Query
     *
     * @param $table
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getTableQuery($table)
    {
        $content_type_data = $this->model();
        $content_type_data->setTable($table);

        return $content_type_data->newQuery();
    }

    /**
     * Get Table Model
     *
     * @param $table
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getTableModel($table)
    {
        $content_type_data = $this->model();
        $content_type_data->setTable($table);

        return $content_type_data;
    }

    /**
     * Alter Column
     *
     * @param $content_type
     * @param $field
     */
    public function alterColumn($content_type, $field)
    {
        $connection = SiteConnectionLib::getFacadeRoot();
        $connection->migrate()->setShortCodesHaystack([
            'table'      => $content_type->table,
            'field'      => $field->name,
            'default'    => $field->default ? "DEFAULT '$field->default'" : '',
            'field_type' => (new SiteContentTypeFieldRepo())->getFieldType($field),
            'nullable'   => $field->is_required ? 'NOT NULL' : ''
        ]);
        $connection->migrate()->run('site_content_type_field');
    }

    /**
     * Rename column name and type
     *
     * @param $content_type
     * @param $field
     * @param $old_name
     */
    public function changeColumn($content_type, $field, $old_name)
    {
        $connection = SiteConnectionLib::getFacadeRoot();
        $connection->migrate()->setShortCodesHaystack([
            'table'          => $content_type->table,
            'old_field_name' => $old_name,
            'new_field_name' => $field->name,
            'field_type'     => (new SiteContentTypeFieldRepo())->getFieldType($field),
        ]);
        $connection->migrate()->run('site_content_type_change_field');
    }

    /**
     * Remove column name
     *
     * @param $table_name
     * @param $field_name
     **/
    public function removeField($table_name, $field_name)
    {
        $connection = SiteConnectionLib::getFacadeRoot();
        $connection->migrate()->setShortCodesHaystack([
            'table' => $table_name,
            'field' => $field_name
        ]);
        $connection->migrate()->run('site_content_type_field', 'down');
    }

    /**
     * Get Content Type Data with specific Post id
     *
     * @param $table
     * @param $post_id
     *
     * @return mixed
     */
    public function getContentTypeDataByPostId($table, $post_id)
    {
        $content_type_data = $this->setCurrentTable($table);

        return $content_type_data->newQuery()->where('post_id', $post_id)
            ->first();
    }

    /**
     * Set Current Table
     *
     * @param $table
     *
     * @return $this
     */
    public function setCurrentTable($table)
    {
        return $this->model()->setTable($table);
    }

    /**
     * Save Current Table
     *
     * @param $table
     *
     * @return bool
     */
    public function saveCurrentTable($table)
    {
        $content_type_data = $this->setCurrentTable($table);

        return $content_type_data->save();
    }


    /**
     * Save from Request
     *
     * @param Request $request
     * @param Post $post
     * @param string $table
     */
    public function saveFromRequest(Request $request, Post $post, string $table)
    {
        $content_type_data = $this->getTableModel($table);
        $content_type_data->post_id = $post->id;
        $this->parseDataForm($request, $post->contentType, $content_type_data);
        $content_type_data->save();
    }

    /**
     * Update From Request
     *
     * @param Request $request
     * @param Post $post
     * @param string $table
     *
     * @return void
     */
    public function updateFromRequest(Request $request, Post $post, string $table)
    {

        $content_type_data = $this->getTableQuery($table)
            ->where('post_id', $post->id)
            ->first();
        
        if (!$content_type_data){
            
            return;
        }
        
        $content_type_data->setTable($table);

        $this->parseDataForm($request, $post->contentType, $content_type_data);
        $content_type_data->save();
    }

    /**
     * saveModel
     * Updates or creates the model
     *
     * @param ContentType $content_type
     * @param Post        $post
     */
    public function saveModel(ContentType $content_type, Post $post)
    {
        $table_name = $post->contentType->full_table;

        $content_type->setTable($table_name);
        $content_type->save();
    }

    /**
     * Save fields to the related content type.
     *
     * @param Post  $post
     * @param array $fields
     *
     * @return boolean
     */
    public function saveFieldsByPost(Post $post, array $fields)
    {
        $post_content_type = $post->content;

        if (!$post_content_type){
            return false;
        }

        $post_content_type->setTable($post->contentType->full_table);

        foreach($fields as $key => $value){
            $post_content_type->$key = $value;
        }

        return $post_content_type->save();
    }

    /**
     * Parse data from request
     * Because not used in migrations, this must methods to remove not valid values
     *
     * @param Request $request
     * @param SiteContentType $content_type
     * @param ContentType $model_content_type
     */
    private function parseDataForm(Request $request, SiteContentType $content_type, ContentType $model_content_type)
    {
        if (isset($request->content_author_id)) {
            $model_content_type->content_author_id = $request->content_author_id;
        }

        // parse and insert data to link content type
        $this->insertContentTypeLink($content_type, $request, $model_content_type);
        // parse content type
        $this->parseCustomFields($content_type, $request, $model_content_type);
    }

    /**
     * Insert Content Type Link
     *
     * @param SiteContentType $content_type
     * @param Request         $request
     * @param ContentType     $model_content_type
     */
    private function insertContentTypeLink(SiteContentType $content_type, Request $request,
        ContentType $model_content_type)
    {
        $content_type_links_repo = new LinkContentTypeRepo();
        $content_type_links      = $this->parseContentTypeLink($content_type, $request, $model_content_type);
        $post_id                 = $model_content_type->post_id;

        foreach ($content_type_links as $field_id => $connection) {
            $content_type_links_repo->removeLinksByPostIdAndFieldId($post_id, $field_id);

            $content_type_links_repo->model()->insert($connection);
        }
    }

    /**
     * Return only fields - content_type_link
     * Parse to two sides - linked post and in link post
     *
     * @param SiteContentType $content_type
     * @param Request $request
     * @param ContentType $model_content_type
     *
     * @return array
     */
    private function parseContentTypeLink(SiteContentType $content_type, Request $request, ContentType $model_content_type)
    {
        $fields = $content_type->fields->filter(function ($item) {
            return $item->type == 'content_type_link';
        });

        $content_type_links = [];
        $post_id = $model_content_type->post_id;

        foreach ($fields as $field) {
            $in_request = $request->get($field->name) ?? [];
            $metadata = json_decode($field->metadata);
            $content_type_id_link = $metadata->content_type_id;
            $linked_field = (new SiteContentTypeFieldRepo())
                ->getLinkedFieldByContentTypeIdAndFieldName($content_type_id_link, $field->name);

            if ($in_request) {
                $priority = 10;
                foreach ($in_request as $index => $item) {
                    $content_type_links[$field->id][] = [
                        'connect_to_post_id' => $item,
                        'field_id' => $field->id,
                        'post_id' => $post_id,
                        'priority' => $index * $priority
                    ];

                    $content_type_links[$linked_field->id][] = [
                        'connect_to_post_id' => $post_id,
                        'field_id' => $linked_field->id,
                        'post_id' => $item,
                        'priority' => $index * $priority
                    ];
                }
            } else {
                $content_type_links[$field->id]        = [];
                $content_type_links[$linked_field->id] = [];
            }
        }

        return $content_type_links;
    }

    /**
     * Parse custom fields
     *
     * @param SiteContentType $content_type
     * @param Request $request
     * @param ContentType $content_type_data
     */
    private function parseCustomFields(SiteContentType $content_type, Request $request, ContentType $content_type_data)
    {
        // looping through the content type specific custom fields
        foreach ($content_type->fields as $field) {

            if (is_array($request->get($field->name)) && !in_array($field->type,
                    ['multi_select', 'content_type_link'])
            ) {
                if (count($request->get($field->name)) == 3 && isset($request->get($field->name)['key'])) {
                    //save field selects
                    $content_type_data->{$field->name} = $this->parseSelectFieldType($request->get($field->name));
                } else {
                    if ($field->type == 'currency') {
                        $content_type_data->{$field->name} = $this->parseArrayFieldType($request->get($field->name));
                    } else {
                        $content_type_data->{$field->name} = $this->parseCheckBoxFieldType($request->get($field->name));
                    }
                }
            } elseif ($field->type == 'multi_select' && is_array($request->get($field->name))) {
                $content_type_data->{$field->name} = $this->parseMultiSelect($request->get($field->name));
            } else {
                if ($field->type == 'checkbox') {
                    $content_type_data->{$field->name} = ($request->get($field->name) !== "0");
                } else {
                    if ($field->type != 'content_type_link') {
                        $content_type_data->{$field->name} = $request->get($field->name);
                    }
                }
            }
        }
    }

    /*
     * Parse multi select fields
     */
    private function parseMultiSelect($multi_select)
    {
        return $this->parseArrayFieldType($multi_select);
    }

    /**
     * Parse checkbox fields
     *
     * @param $checkbox_rows
     *
     * @return string
     */
    private function parseCheckBoxFieldType($checkbox_rows)
    {
        $params = [];
        foreach ($checkbox_rows as $key => $value) {
            $params[$key] = ($value === "0") ? false : true;
        }

        return json_encode($params);
    }

    /**
     * Parse array fields
     *
     * @param $checkbox_rows
     *
     * @return string
     */
    private function parseArrayFieldType($checkbox_rows)
    {
        $params = [];
        foreach ($checkbox_rows as $key => $name) {
            $params[$key] = $name;
        }
        return json_encode($params);
    }

    /**
     * Parse select fields
     *
     * @param $select_rows
     *
     * @return string
     */
    private function parseSelectFieldType($select_rows)
    {
        $params = [];
        foreach ($select_rows['key'] as $key => $name) {
            $params[] = [
                'priority' => $key,
                'default' => ($select_rows['default'] == $key) ? true : false,
                'key' => $name,
                'value' => $select_rows['value'][$key] ?? null
            ];
        }
        return json_encode($params);
    }
}