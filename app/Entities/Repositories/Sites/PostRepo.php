<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Repositories\SiteSettingRepo;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Facades\SiteConnectionLib;
use App\Entities\Models\Sites\Page;
use App\Entities\Models\Sites\Post;
use App\Http\Traits\TreeToArrayTrait;
use App\Libraries\PageInfo\PagesUrls;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Repositories\Repository;
use App\Entities\Models\Sites\SiteSetting;
use App\Entities\Models\Sites\PageContentType;
use App\Entities\Models\Sites\SiteSettingDefault;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;

/**
 * Class PostRepo
 *
 * @package App\Entities\Repositories\Sites
 */
class PostRepo extends Repository
{
    const HOMEPAGE = 'homepage';
    use TreeToArrayTrait;

    const MODEL = Post::class;

    private $site;

    public function __construct()
    {
        $this->site = SiteConnectionLib::getSite();
    }

    /**
     * Save the post fields for the native post object
     * or for the related content type object.
     *
     * @param integer $post_id
     * @param array   $fields
     * @param bool    $is_content_type
     *
     * @return bool
     */
    public function savePost(int $post_id, array $fields, bool $is_content_type = false)
    {
        $post = $this->model()->findOrFail($post_id);

        if ($is_content_type) {
            $content_type_repository = new ContentTypeRepo();

            return $content_type_repository->saveFieldsByPost($post, $fields);
        }

        return $post->update($fields);
    }

    /**
     * Get all the revision history related to a given post object.
     *
     * @param PostRepo $post
     * @param Callable $filters
     *
     * @return Collection
     */
    public function revisionHistory($post, callable $filters)
    {
        // Get the revision history query builder object for a given post.
        $revisions = $post->revisionHistory();

        // Apply dynamic filters with the $revisions object.
        $filters($revisions);

        // Get the results into a collection.
        $revisions = $revisions->get();

        if ($post->content) {
            // The current post has a content type, get his revision history query builder.
            $content_type_revisions = $post->content->revisionHistory();

            // Again, we apply dynamic filter to the query builder.
            $filters($content_type_revisions);

            // Merge the results into the `$revisions` collection.
            foreach ($content_type_revisions->get() as $item) {
                $revisions->push($item);
            }
        }

        return $revisions;
    }

    /**
     * Get by Ids
     *
     * @param array $post_ids
     *
     * @param bool $with_draft
     * @return Collection
     */
    public function getByIds(array $post_ids, bool $with_draft = false)
    {
        $query = $this->query()->whereIn('id', $post_ids);

        return $with_draft ? $query->get() : $query->whereNotNull('published_at')->get();
    }

    /**
     * Get Posts By Same Order By IDs And Ordered IDs
     *
     * @param array $post_ids
     *
     * @return Collection
     */
    public function getPublishedByIds(array $post_ids)
    {
        $post_ids_glued = implode(',', $post_ids);
        /**
         * return all post by same order need orderByRaw(DB::raw("FIELD(id, $ids_ordered)"))
         * http://stackoverflow.com/questions/26704575/laravel-order-by-where-in
         */

        return $this->query()
            ->whereIn('id', $post_ids)
            ->whereNotNull('published_at')
            ->whereDate('published_at', '<=', Carbon::now())
            ->orderByRaw("FIELD(id, $post_ids_glued)")
            ->get();
    }

    /**
     * Get Hidden Posts By Content Type ID And Without IDs
     *
     * @param int   $content_type_id
     * @param array $post_ids
     *
     * @return mixed
     */
    public function getHiddenByContentTypeIdAndWithoutIds($content_type_id, array $post_ids)
    {
        return $this->query()
            ->where(compact('content_type_id'))
            ->whereNotIn('id', $post_ids)
            ->whereNotNull('published_at')
            ->whereDate('published_at', '<=', Carbon::now())
            ->get();
    }

    /**
     * Get Labels
     * return connected labels to current post
     *
     * @param $post_id
     *
     * @return mixed
     */
    public function getLabelsByPostId($post_id)
    {
        if ($post_id) {
            $post = $this->find($post_id);

            return $post->labels;
        }
    }

    /**
     * Delete Page
     *
     * @param $page_id
     *
     * @return mixed
     */
    public function deletePage($page_id)
    {
        $pages_tree_to_delete = $this->getPagesAsTree($page_id)->flatten();
        $pages_tree_to_delete->each(function ($page_post) {
            $page_post->delete();
        });
        $page = $this->find($page_id);
        $page->delete();

        return $page;
    }

    /**
     * Get As Tree
     * Return templates as nested tree.
     *
     * @param null $page_id
     *
     * @return Collection
     */
    public function getPagesAsTree($page_id = null)
    {
        $page_content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();

        $pages = $this->query()
            ->where('content_type_id', $page_content_type_id)
            ->where('parent_id', $page_id)
            ->with('childrenRecursive')
            ->orderBy('priority', 'asc')
            ->get();

        return $this->buildHref($pages);
    }

    /**
     * Adds to pages href
     *
     * @param $pages
     *
     * @return mixed
     */
    private function buildHref($pages)
    {
        $site_domain_model = (new SiteDomainRepo())->getBySite($this->site->id);
        $url               = 'http://' . $site_domain_model->domain;

        $pages->each(function ($item) use ($url) {
            $slug      = DIRECTORY_SEPARATOR;
            $parent_id = null;
            $this->appendHref($item, $url, $parent_id, $slug);

            return $item;
        });

        return $pages;
    }

    /**
     * Append Href
     * appends href to an element for preview
     *
     * @param $item
     * @param $url
     * @param $parent_id
     * @param $slug
     */
    private function appendHref(&$item, $url, $parent_id, $slug)
    {
        if (!($item->childrenRecursive->isEmpty())) {
            $item->childrenRecursive->each(function ($elem, $key) use ($url, $slug) {
                $this->appendHref($elem, $url, $elem->parent_id, $slug);

                return $elem;
            });
        }

        $item->href = $url . (new PageRepo())->getUrl($parent_id) . $slug . $item->slug;
    }

    /**
     * Delete Post
     *
     * @param Model $post
     *
     * @return bool|null|void
     */
    public function delete(Model $post)
    {
        if ($this->canDeletePost($post)) {
            parent::delete($post);
        } else {
            throw new Exception('Cannot delete post that still connected to widgets');
        }
    }

    /**
     * Can Delete post
     *
     * @param $post
     *
     * @return mixed
     */
    public function canDeletePost(Post $post)
    {
        $widget_data_repo = new WidgetDataRepo();

        return $widget_data_repo->isPostUsedByWidgets($post);
    }

    /**
     * Get Group Fields
     *
     * @param $post_fields
     *
     * @return array
     */
    public function getGroupedFields($post_fields)
    {
        $grouped_fields = [];

        foreach ($post_fields as $key => $field) {
            if (!empty($field->field_group_id)) {
                $grouped_fields[$field->field_group_id][] = $field;
                unset($post_fields[$key]);
            }
        }

        return $grouped_fields;
    }


    /**
     * Create With Content Data
     *
     * @param Request         $request
     * @param SiteContentType $content_type
     *
     * @return Post
     */
    public function createWithContentData(Request $request, SiteContentType $content_type)
    {
        $values = $this->convertValues($request->all());
        $post   = new Post($values);

        $post->save();
        (new PostRedirectUrlRepo())->saveFromRequest($post->id, $request);

        $labels = $request->get('labels') ?? [];
        $post->labels()->sync($labels);

        $content_type_repo = new ContentTypeRepo();
        $content_type_repo->saveFromRequest($request, $post, $content_type->full_table);

        return $post;
    }

    /**
     * Convert values to valid
     * current : slug and parent_id
     *
     * @param $values
     *
     * @return mixed
     */
    private function convertValues($values)
    {
        if (isset($values['parent_id'])) {
            $values['parent_id'] = empty($values['parent_id']) ? null : $values['parent_id'];
        }

        if ($values['is_publish'] == 'draft') {
            $values['published_at'] = null;
        } else {
            if (empty($values['published_at'])) {
                $values['published_at'] = Carbon::now();
            }
        }

        $values['slug'] = toSlug($values['slug']);
        return $values;
    }

    /**
     * Update With Content Data
     *
     * @param Request $request
     * @param         $post
     */
    public function updateWithContentData(Request $request, Post $post)
    {
        $values = $this->convertValues($request->all());
        $post->fill($values);

        $table             = $post->contentType->full_table;
        $content_type_repo = new ContentTypeRepo();
        $content_type_repo->updateFromRequest($request, $post, $table);

        $post->touch();
        (new PostRedirectUrlRepo())->saveFromRequest($post->id, $request);

        $labels = $request->get('labels') ?? [];
        $post->labels()->sync($labels);
    }

    /**
     * Get All In Select Format
     *
     * @return array
     */
    public function getAllInSelectFormat()
    {
        $pages_url = new PagesUrls();

        return $pages_url->listRoutes('_href');
    }

    /**
     * @return mixed
     */
    public function getAllPages()
    {
        $page_content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();

        $query = $this->query()
            ->where('content_type_id', $page_content_type_id);

        return $query->get();
    }

    /**
     * getHomepagePage
     *
     * @return mixed
     */
    public function getHomepagePage()
    {
        $homepage_post_id = (new SettingsRepo())->getByKey(self::HOMEPAGE)->value;

        return $this->query()->where('id', $homepage_post_id)->with('content')->first();
    }

    /**
     * Get By Content Type Id
     *
     * @param      $content_type_id
     * @param bool $only_published
     *
     * @return Model|null|static
     */
    public function getByContentTypeId($content_type_id, $only_published = false)
    {
        $query = $this->query()->where('content_type_id', $content_type_id);

        if ($only_published) {
            $query->whereNotNull('published_at')
                ->whereDate('published_at', '<=', Carbon::now());
        }

        return $query->get();
    }

    /**
     * Get First Post By Content Type Id
     *
     * @param      $content_type_id
     * @param bool $only_published
     *
     * @return Post|null
     */
    public function getFirstPostByContentTypeId($content_type_id, $only_published = false)
    {
        $query = $this->query()->where('content_type_id', $content_type_id);

        if ($only_published) {
            $query->whereNotNull('published_at')
                ->whereDate('published_at', '<=', Carbon::now());
        }

        return $query->first();
    }

    /**
     * Insert page values
     *
     * @param      $values
     * @param Page $page
     * @param Post $post
     *
     * @return response message
     */
    public function savePage($values, Page $page, Post $post)
    {
        DB::beginTransaction();
        $is_exists = $post->exists;
        $values    = $this->convertValues($values);
        try {
            $post              = $this->insertPost($post, $values);
            $values['post_id'] = $post->id;
            $page              = $this->insertPage($page, $values);
            $this->saveArchiveContent($post, $values);
        } catch (\Exception $e) {
            DB::rollBack();
            return ['error' => json_encode($e->getMessage())];
        }

        if ($this->isPageExist($post)) {
            DB::rollBack();
            return ['error' => 'A page with the same slug and parent_id is already exists'];
        }

        if ($is_exists) {
            $this->insertSiteSettings($post);
        }

        DB::commit();

        return ['success' => "The page {$page->name} added"];
    }

    /**
     * Insert Post
     *
     * @param Post $post
     * @param      $values
     *
     * @return bool
     */
    private function insertPost(Post $post, $values)
    {
        $post->fill($values);
        $post->content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();
        $post->save();

        return $post;
    }

    /**
     * Insert Page
     *
     * @param Page $page
     * @param      $values
     *
     * @return bool
     */
    private function insertPage(Page $page, $values)
    {
        $page->fill($values)->save();

        return $page;
    }

    /**
     * Save Archive Content
     *
     * @param Post  $post
     * @param array $page_values
     */
    private function saveArchiveContent(Post $post, Array &$page_values)
    {
        if ($page_values['type'] == 'archive') {
            $content_types = [];
            foreach ($page_values['archive'] as $value) {
                $content_types[] = [
                    'post_id'            => $post->id,
                    'template_id'        => $value['template_desktop_id'] ?? null,
                    'template_mobile_id' => $value['template_mobile'] ?? null,
                    'content_type_id'    => $value['content_type_id'] ?? null,
                    'label_id'           => $value['label'] ?? null
                ];
            }

            PageContentType::where('post_id', $post->id)->delete();
            $result = PageContentType::insert($content_types);
        } else {
            PageContentType::where('post_id', $post->id)->delete();
        }
    }

    /**
     * Is Page Exist
     *
     * @param Post $page
     *
     * @return bool
     */
    public function isPageExist(Post $page)
    {
        return $this->isPageSlugExists($page->slug, $page->id, $page->parent_id);
    }

    /**
     * @param String $slug
     * @param null   $parent_id
     * @param null   $post_id
     *
     * @return bool
     */
    public function isPageSlugExists($slug, $post_id = null, $parent_id = null)
    {
        $page_content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();

        return $this->isSlugExists($page_content_type_id, $slug, $post_id, $parent_id);
    }

    /**
     * Is Slug Exists
     *
     * @param      $content_type_id
     * @param      $slug
     * @param null $parent_id
     * @param null $post_id
     *
     * @return bool
     */
    public function isSlugExists($content_type_id, $slug, $post_id = null, $parent_id = null)
    {
        $query = $this->query()->where('content_type_id', $content_type_id)
            ->where('slug', toSlug($slug));

        if ($parent_id) {
            $query->where('parent_id', '=', $parent_id);
        } else {
            $query->whereNull('parent_id');
        }

        if ($post_id) {
            $query->where('id', '!=', $post_id);
        }

        return $query->count() > 0;
    }

    private function insertSiteSettings(Post $post)
    {
        $settings_type = SiteSetting::getPageType($post->id);
        SettingsDefaultRepo::setDefaultSettingForType($this->site->id, $settings_type, SiteSettingDefault::TYPE_PAGE);
    }

    /**
     * Get all content types posts
     * used only if type content_type_link
     *
     * @param $fields
     *
     * @return null
     */
    public function getLinkedPosts(Collection $fields)
    {
        $content_type_links = $fields->filter(function ($item) {
            return $item->type == 'content_type_link';
        });

        $content_types = [];

        foreach ($content_type_links as $content_type) {
            if ($content_type && !empty($content_type->metadata)) {
                $parse_metadata = json_decode($content_type->metadata);

                $content_type_id = $parse_metadata->content_type_id ?? null;

                if ($content_type_id) {
                    $content_types[] = $content_type_id;
                }
            }
        }

        return count($content_types)
            ? $this->model()
                ->whereIn('content_type_id', array_values($content_types))
                ->whereNotNull('published_at')
                ->whereDate('published_at', '<=', Carbon::now())
                ->get()
                ->groupBy('content_type_id')
            : new Collection();
    }

    /*
     * Insert site settings in post is new
     *
     * @param Post $post
     */

    /**
     * Post List By Archive Page To Select
     *
     * @param $post_id
     *
     * @return array
     */
    public function postListByArchivePageToSelect($post_id)
    {
        if (empty($post_id)) {
            return [];
        }

        $content_types = PageContentType::where('post_id', $post_id)
            ->get()
            ->pluck('content_type_id')
            ->toArray();

        return $this->model()->select('id', 'name as text')->whereIn('content_type_id', $content_types)->get()
            ->toArray();
    }

    /**
     * Get Posts Order By Slugs And Priority.
     * 1. Get the relevant posts by slug , order by slugs.
     * 2. Order by the page priority.
     *
     * @param array $slugs
     *
     * @return mixed
     */
    public function getOrderedBySlugs(array $slugs)
    {
        if (request()->has('render_draft')) {
            $posts = $this->model()
                ->notDeleted()
                ->whereIn('slug', $slugs)
                ->get();
        } else {
            $posts = $this->model()
                ->published()
                ->notDeleted()
                ->whereIn('slug', $slugs)
                ->get();
        }

        return $this->getOrderedByPageContentTypesPriority($posts);
    }

    /**
     * isPage
     *
     * @param Post $post
     *
     * @return bool
     */
    public function isPage(Post $post): Bool
    {
        if (!empty($post->page->type)) {
            return true;
        }

        return false;
    }

    /**
     * getPostPageSingleContentTypes
     *
     * @param Post $post
     *
     * @return mixed
     */
    public function getPostPageSingleContentTypes(Post $post)
    {
        return $post->archiveContentTypes ?? collect();
    }

    /**
     * Get post from content types.
     *
     * @param array $content_types
     * @param       $slug
     *
     * @return Collection
     */
    public function getPostFromContentTypesBySlug(array $content_types, $slug)
    {
        return $this->query()
            ->whereIn('content_type_id', $content_types)
            ->where('slug', $slug)
            ->first();
    }

    /**
     * Get Redirect Url
     *
     * @param      $content_type_slug
     * @param      $post_slug
     * @param null $redirect_slug
     *
     * @return null
     */
    public function getRedirectUrl($content_type_slug, $post_slug, $redirect_slug = null)
    {
        $post = $this->getBySlugs($content_type_slug, $post_slug);

        if (!$post) {

            return null;
        }

        $post_redirect = null;

        if ($redirect_slug) {
            $post_redirect = $post->redirectUrls()->where('slug', $redirect_slug)->first();
        }

        //return the default if didn't found by slug
        if (!$post_redirect) {
            $post_redirect = $post->redirectUrls()->where('is_default', true)->first();
        }

        if (!$post_redirect) {
            //TODO Log Error and send mail
        }

        return $post_redirect;
    }

    /**
     * Get By Content Type Slug And Post Slug
     *
     * @param $content_type_slug
     * @param $post_slug
     *
     * @return null
     */
    public function getBySlugs($content_type_slug, $post_slug)
    {
        $site_content_type_repo = new SiteContentTypeRepo();
        $content_type           = $site_content_type_repo->getBySlug($content_type_slug);

        if ($content_type) {
            $post = $this->query()
                ->where('content_type_id', $content_type->id)
                ->where('slug', $post_slug)
                ->first();

            return $post;
        }

        return null;
    }

    /**
     * @param $page_content_type_id
     *
     * @return mixed
     */
    public function getPagesByContentTypeIdWithParentRecursive($page_content_type_id)
    {
        return $this->query()
            ->where('content_type_id', $page_content_type_id)
            ->with('parentRecursive')
            ->get();
    }

    /**
     * Get By Page Template Id
     *
     * @param $template_id
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getByPageTemplateId($template_id, $is_mobile = false)
    {
        $template_column = $is_mobile ? 'template_mobile_id' : 'template_id';
        $query           = $this->getQueryByContentTypeColumn($template_column, $template_id);

        $page_content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();

        return $query->where('content_type_id', $page_content_type_id)
            ->get();
    }

    /**
     * Get Query By Content Type Column
     *
     * @param $column_name
     * @param $column_value
     *
     * @return $this
     */
    public function getQueryByContentTypeColumn($column_name, $column_value)
    {
        return $this->query()
            ->whereHas('content', function ($query) use ($column_name, $column_value) {
                $query->where($column_name, $column_value);
            })
            ->with('content');
    }

    /**
     * Get the first Post by id with parent Recursive
     *
     * @param $post_id
     *
     * @return mixed
     */
    public function getByIdWithParentRecursive($post_id)
    {
        return $this->query()
            ->where('id', $post_id)
            ->with('parentRecursive')
            ->first();
    }

    /**
     * Get Dynamic Lists By Post id
     *
     * @param $id
     *
     * @return mixed
     */
    public function getDynamicLists($id)
    {
        $model = $this->find($id);

        return $model->dynamicLists()
            ->pluck('name', 'id');
    }

    /**
     * Get Active Labels From Specific Post
     *
     * @param $id
     *
     * @return mixed
     */
    public function getActiveLabelsById(int $id)
    {
        $model = $this->find($id);

        return $model->labels->pluck('id')->toArray();
    }

    /**
     * Duplicate
     *
     * @param $post
     *
     * @return mixed
     */
    public function duplicate($post)
    {
        $duplicated_post               = $post->replicate();
        $duplicated_post->name         .= ' - Duplicated';
        $duplicated_post->slug         .= '_dup_' . time();
        $duplicated_post->published_at = null;
        $duplicated_post->save();

        $content_type_data = $post->content->replicate();
        $content_type_data->setTable($post->contentType->full_table);
        $content_type_data->post_id = $duplicated_post->id;
        $content_type_data->save();

        $duplicated_post->labels()->attach($post->labels);

        if ($this->isPage($post)) {
            (new PageRepo())->duplicate($post, $duplicated_post);
        } else {
            (new LinkContentTypeRepo())->duplicateForPost($post, $duplicated_post);
        }

        return $duplicated_post;
    }

    /**
     * @param Post $parent
     * @param Post $post
     *
     * @return bool
     */
    Public function isParent(Post $parent, Post $post): bool
    {
        if (isset($post->parent_id)) {

            return $parent->id == $post->parent_id;
        }

        return false;
    }

    /**
     * @param Post $post
     *
     * @return bool
     */
    public function isArchive(Post $post): Bool
    {
        if (!empty($post->page->type) && $post->page->type == 'archive') {

            return true;
        }

        return false;
    }

    /**
     * Get all urls for a given post.
     *
     * @param integer $post_id
     *
     * @return array
     */
    public function getUrls($post_id)
    {
        $urls                   = [];
        $page_content_type_repo = new PageContentTypeRepo();

        $original_post = $this->find($post_id);

        if (!$original_post) {

            return [];
        }

        $pages_urls = new PagesUrls();

        $domain        = $pages_urls->getDomain();
        $related_pages = $page_content_type_repo->getPagesByContentTypeId($original_post->content_type_id);

        foreach ($related_pages as $related_page) {
            $urls[] = $this->resolveUrlItemByRelatedPage($related_page, $original_post->id, $domain, $pages_urls);
        }

        return $urls;
    }

    /**
     * Resolve the url item for a given page.
     *
     * @param Page      $related_page
     * @param integer   $original_post_id
     * @param string    $domain
     * @param PagesUrls $pages_urls
     *
     * @return array
     */
    protected function resolveUrlItemByRelatedPage($related_page, $original_post_id, $domain, PagesUrls $pages_urls)
    {
        $path = $pages_urls->getHref($related_page->post_id, $original_post_id);

        return [
            'path'     => $path,
            'page'     => $related_page,
            'full_url' => $domain . $path
        ];
    }

    /**
     * Sort posts By column priority.
     * 1. Get posts
     *
     * @param $posts
     *
     * @return mixed
     */
    private function getOrderedByPageContentTypesPriority($posts)
    {
        $posts_priority = (new PageContentTypeRepo())->getPostsPriority($posts);

        return $posts->sort(function ($a, $b) use ($posts_priority) {
            /* pages should always be prioritize first*/
            if ($this->isPage($b)) {

                // this is the height priority we can get for pages
                return 99;
            }
            $a_priority = $posts_priority->get($a->id) ? $posts_priority->get($a->id)->priority : $a->id;
            $b_priority = $posts_priority->get($b->id) ? $posts_priority->get($b->id)->priority : $b->id;

            return $a_priority - $b_priority;
        });

    }

    /**
     * Get By Label
     * Optionally supports content type as well
     *
     * Create Homepage layout:
     *  1  create Post.
     *  2  create Page.
     *
     * @param $template
     * @param $page_content_type
     *
     * @return response
     */
    public function createHomePage($template,$page_content_type)
    {
        $params = [
            'slug'            => 'homepage',
            'name'            => 'Home Page',
            'active'          => 1,
            'section_id'      => null,
            'content_type_id' => $page_content_type->id,
            'show_on_sitemap' => 1,
            'seo_post_title'  => 'Home Page',
            'is_publish'      => 'publish',
            'template_id'     => $template->id,
            'is_homepage'     => 1
        ];

        return $this->savePage($params, new Page(), $this->model());
    }


    /**
     * Get By Label
     * Optionally supports content type as well
     *
     * @param int      $label_id
     * @param int|null $content_type_id
     *
     * @return mixed
     */
    public function getByLabel(int $label_id, int $content_type_id = null)
    {
        $model = $this->model()
            ->whereHas('labels', function ($query) use ($label_id) {
                $query->where('label_id', $label_id);
            });

        if ($content_type_id > 0) {
            $model->where('content_type_id', $content_type_id);
        }

        return $model->get();
    }
}
