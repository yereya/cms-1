<?php

namespace App\Entities\Repositories\Sites;


use App\Entities\Models\Sites\PostRedirectUrl;
use App\Entities\Repositories\Repository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class PostRedirectUrlRepo extends Repository
{
    const MODEL = PostRedirectUrl::class;

    /**
     * Save From Request
     *
     * @param $post_id
     * @param $values
     */
    public function saveFromRequest($post_id, Request $request)
    {
        $urls_from_request = $this->buildRedirectUrls($request);
        $redirect_urls = $this->query()->where('post_id', $post_id)->get()->keyBY('slug');

        foreach ($urls_from_request as $input) {
            if (isset($redirect_urls[$input['slug']])) {
                $this->updateRedirectUrl($redirect_urls, $input);
            } else {
                $this->createRedirectUrl($post_id, $input);
            }
        }

        //Delete the rest
        $this->deleteLinks($redirect_urls);
    }


    /**
     * Create Link
     *
     * @param $post_id
     * @param $input
     */
    public function createRedirectUrl(int $post_id, Array $input)
    {
        $input['post_id'] = $post_id;
        $redirect_url = $this->model();
        $redirect_url->fill($input);
        $redirect_url->save();
    }

    /**
     * Update Link
     *
     * @param Collection $redirect_urls
     * @param array $input
     */
    public function updateRedirectUrl(Collection $redirect_urls, Array $input)
    {
        $slug = $input['slug'];
        $redirect_url = $redirect_urls[$slug];
        $redirect_url->fill($input);
        $redirect_url->save();
        $redirect_urls->forget($slug);
    }

    /**
     * Delete Links
     *
     * @param $redirect_urls
     */
    protected function deleteLinks(Collection $redirect_urls)
    {
        $ids = $redirect_urls->pluck('id')->toArray();
        if (!empty($ids)) {
            $this->deleteByIds($ids);
        }
    }

    /**
     * Delete by Ids
     *
     * @param $ids
     */
    public function deleteByIds(Array $ids)
    {
        $this->query()->whereIn('id', $ids)->delete();
    }

    /**
     * Build Redirect Links
     *
     * @param $values
     */
    private function buildRedirectUrls(Request $request)
    {
        $redirect_urls = $request->get('redirect_links');
        $new_redirect_urls = [];
        if (isset($redirect_urls)) {
            $default = $redirect_urls['default'][0] ?? 0;
            foreach ($redirect_urls['slug'] as $index => $slug) {
                if (empty($slug)) continue;
                $new_redirect_urls[] = [
                    'slug' => $slug,
                    'url' => $redirect_urls['url'][$index],
                    'is_default' => $default == $index
                ];
            }
        }

        return $new_redirect_urls;
    }
}