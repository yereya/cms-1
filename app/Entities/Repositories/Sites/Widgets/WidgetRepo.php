<?php

namespace App\Entities\Repositories\Sites\Widgets;

use App\Entities\Models\Sites\Templates\Item;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Entities\Repositories\Repository;
use Illuminate\Support\Collection;

class WidgetRepo extends Repository
{
    const MODEL = SiteWidget::class;

    public static function isDynamicListWidget($widget)
    {
        return $widget->widget_id == 15;
    }

    /*
     * Is widget dynamic list
     */

    /**
     * getAllScss
     *
     * @return mixed|string
     */
    public function getAllSiteScss()
    {
        $used_widgets = Item::where('element_type', 'widget')
                            ->groupBy('element_id')
                            ->get()
                            ->pluck('element_id');

        $widget_templates = SiteWidget::whereIn('id', $used_widgets)->get();

        return $widget_templates->pluck('scss')->implode('');
    }

    /**
     * Get widget Text Editor
     *
     */
    public function getWidgetTextEditor()
    {
        return $this->query()
            ->where('controller','Wysiwyg')
            ->first();
    }
}
