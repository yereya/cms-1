<?php

namespace App\Entities\Repositories\Sites\Widgets;

use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Entities\Models\Sites\Widgets\SiteWidgetTemplate;
use App\Entities\Repositories\Repository;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use App\Libraries\Guzzle;
use App\Libraries\SiteWidgetTemplates\SiteWidgetTemplatesLib;
use Illuminate\Http\Request;

/**
 * @property \Illuminate\Support\Collection getWidgetDataNameAndId
 */
class SiteWidgetTemplateRepo extends Repository
{
    const MODEL = SiteWidgetTemplate::class;

    /**
     * getAllScss
     *
     * @return mixed|string
     */
    public function getGeneralScss()
    {
        $used_widgets_data = WidgetData::with([
            'widgetDataItems' => function ($query) {
                $query->where('element_type', 'widget')
                    ->groupBy('element_id');
            }
        ])->get();

        $used_widget_template = $used_widgets_data->unique('widget_template_id')
            ->pluck('widget_template_id');

        $site_widget_templates = SiteWidgetTemplate::whereIn('id', $used_widget_template)
            ->whereNotNull('scss')
            ->get();

        return $site_widget_templates->pluck('scss')->implode('');
    }


    public function getAllScss(){
        $site_widget_templates = $this->query()
            ->whereNotNull('scss')
            ->get();

       return  $site_widget_templates->pluck('scss')->implode('');
    }
    /**
     * Get Widget Name And Id
     *
     * @return \Illuminate\Support\Collection
     */
    public function getWidgetsNameAndId()
    {
        return SiteWidget::all()->pluck('name', 'id');
    }

    /**
     * @param $widget_data_id
     * @return mixed
     */
    public function getWidgetDataByID($widget_data_id)
    {
        return WidgetData::where('id', $widget_data_id)
            ->groupBy('id')
            ->get();
    }

    /**
     * Get Widget By Id
     *
     * @param $widget_id
     * @return mixed
     */
    public function getWidgetById($widget_id)
    {
        return SiteWidget::where('id', $widget_id)
            ->groupBy('id')
            ->get();

    }


    /**
     * @param Request $request
     * @return SiteWidgetTemplate
     */
    public function prepareSiteWidgetTemplateData(Request $request)
    {
        $widgetTemplate = new SiteWidgetTemplate();
        $filtered_request = $request->except('_token');

        foreach ($filtered_request as $field_name => $field_value) {
            $widgetTemplate->$field_name = $field_value;
        }
        $widgetTemplate->site_id = $request->route()->getParameter('site');

        return $widgetTemplate;
    }

    /**
     * Duplicate
     *
     * @param SiteWidgetTemplate $widget_template
     *
     * @return mixed
     * @internal param $widget_template_id
     */
    public function duplicate(SiteWidgetTemplate $widget_template)
    {
        $duplicate_widget_template = $widget_template->replicate();
        $duplicate_widget_template->file_name .=  '_duplicated_' . time();
        $duplicate_widget_template->name .=  ' - Duplicated';
        $duplicate_widget_template->save();

        return $duplicate_widget_template;
    }


    /**
     *
     *
     * @param $site
     *
     * @return $this
     */
    public function getWithWidgetAndWidgetDataBySite($site)
    {
        return $this->query()
            ->with('widgetData', 'widget')
            ->whereSite($site);
    }

    /**
     * @param $site
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createHomePage($site)
    {
        $widget_repo = (new WidgetRepo());
        $widget_text = $widget_repo->getWidgetTextEditor();

        $params = [
            'site_id'=> $site->id,
            'widget_id' => $widget_text->id,
            'published'=> 1,
            'name'=>'Home Page - hello world',
            'file_name'=>'homepage-hello-world',
            'blade'=>'{!! $widget_data_attributes[\'content\'] !!}',
            'scss'=>'.PLACEHOLDER_CLASS {}'
        ];

        $model = $this->model();
        $model->fill($params);
        $model->save();

        return $model;
    }

    /**
     * @param $site
     *
     * @return mixed
     */
    public function publishAll($site)
    {
        $url = (new SiteDomainRepo())->getLoadAllWidgetsUrl($site->id);
        $guzzle = new Guzzle();
        $guzzle->fetch($url, [], 'GET');

        return json_decode($guzzle->getBody());
    }


    /**
     * Flush All Blades
     *
     * @param                    $site
     *
     * @internal param $site
     */
    public function flushBlades($site)
    {
        $templates_lib = $this->getWidgetTemplatesLib($site);
        $templates_lib->createAllWidgetTemplates();
    }


    /**
     * @param $widget_template
     * @param $site
     */
    public function flushBlade($widget_template,$site)
    {
        $templates_lib = $this->getWidgetTemplatesLib($site);
        $templates_lib->createWidgetTemplateFile($widget_template);
    }

    /**
     * Get Widget Templates Lib
     *
     * @param $site
     *
     * @return SiteWidgetTemplatesLib
     */
    protected function getWidgetTemplatesLib($site)
    {
        $widgets_view_path = config('view.widgets_view_path') . '/' . $site->id;
        $templates_lib     = new SiteWidgetTemplatesLib($site, $widgets_view_path);

        return $templates_lib;
    }
}
