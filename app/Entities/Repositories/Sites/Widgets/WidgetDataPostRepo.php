<?php

namespace App\Entities\Repositories\Sites\Widgets;


use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Repositories\Repository;
use App\Entities\Models\Sites\Widgets\WidgetDataPost;
use App\Entities\Repositories\Sites\PostRepo;

class WidgetDataPostRepo extends Repository
{
    const MODEL = WidgetDataPost::class;

    /**
     * getPostLabels
     * Return connected labels to post in current widget
     *
     * @param $widget_data
     *
     * @return null
     */
    public function getPostLabels(WidgetData $widget_data)
    {
        //used by model because query don't have method first
        $widget_data_post = $this->model()
            ->where('widget_data_id', $widget_data->id)
            ->first();

        if ($widget_data_post && !empty($widget_data_post->post_id)) {
            return (new PostRepo())->getLabelsByPostId($widget_data_post->post_id);
        } else {
            return null;
        }
    }

    /**
     * Get WidgetDataPost By Widget Data Id
     *
     * @param $id
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function getByWidgetDataId($id)
    {
        return $this->query()
            ->where('widget_data_id', $id)
            ->first();
    }
}