<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Repositories\Repository;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use App\Http\Traits\TreeToArrayTrait;
use Illuminate\Support\Collection;

class TemplateRepo extends Repository
{
    const MODEL = Template::class;
    const TEMPLATE_CLASS_PREFIX = 'tpl_';

    use TreeToArrayTrait;

    /**
     * Get All In Select Format
     *
     * @return array
     */
    public static function getAllInSelectFormat()
    {
        $template_tree = Template::getAsTree();

        return self::convertTreeToSelectFormat($template_tree);
    }

    /**
     * getAllScss
     *
     * @param Template $template
     *
     * @return mixed|string
     */
    public function getScss(Template $template)
    {
        $scss = $template->scss;

        if ($parent_template = $template->parent) {
            $scss .= $this->getAllScss($parent_template);
        }

        return $scss;
    }

    /**
     * getParentClasses
     *
     * @param Template $template
     *
     * @return string
     */
    public function getParentClasses(Template $template)
    {
        $template     = $template->with('parentRecursive')->find($template->id);
        $class_string = $this->getClass($template);

        while (!is_null($template->parentRecursive)) {
            $class_string .= ' ' . $this->getClass($template->parentRecursive);
            $template = $template->parentRecursive;
        }

        return $class_string;
    }

    /**
     * getClass
     *
     * @param Template $template
     *
     * @return string
     */
    public function getClass(Template $template)
    {
        return self::TEMPLATE_CLASS_PREFIX . $template->id;
    }

    /**
     * getAllScss
     *
     * @return mixed|string
     */
    public function getAllScss()
    {
        $templates = Template::getAsTree();

        $scss = '';
        do {
            $scss      .= $templates->pluck('scss')->implode('');
            $templates = $this->getAllChildren($templates);
        } while ($templates->count());

        return $scss;
    }

    /**
     * getWidgetScss
     *
     * @param Template $template
     *
     * @return mixed
     */
    public function getWidgetScss(Template $template): String
    {
        return $this->getTemplateWidgets($template)
            ->pluck('scss')
            ->filter(function ($value) {
                return $value ?? false;
            })
            ->implode(' ');
    }

    /**
     * getTemplateWidgets
     *
     * @param Template $template
     *
     * @return mixed
     */
    public function getTemplateWidgets(Template $template): Collection
    {
        return WidgetData::whereNotNull('active')
            ->whereHas('templateRowColumnItem', function ($query) use ($template) {
                $query->where('template_row_column_items.template_id', $template->id);
            })->get();
    }

    /**
     * Get Template Bread Crumbs Route
     *
     * @return array
     */
    public function getTemplateBreadCrumbsRoute($template)
    {
        $template_list    = [];
        $parent_templates = $template->parentRecursive('parentRecursive')->first();

        if ($parent_templates) {
            $template_list = $this->getTemplateParentRecursive($parent_templates, $template_list);
        }

        return $template_list;
    }

    /**
     * Get Template Parent Recursive
     *
     * @param $template
     * @param $templates_list
     *
     * @return array
     */
    private function getTemplateParentRecursive($template, &$templates_list)
    {
        if (isset($template->parent)) {
            $this->getTemplateParentRecursive($template->parent, $templates_list);
        }

        $templates_list[] = $template;

        return $templates_list;
    }

    /**
     * getAllChildren
     *
     * @param Collection $templates
     *
     * @return Collection
     */
    private function getAllChildren(Collection $templates)
    {
        $children = collect();

        foreach ($templates as $key => $template) {
            if ($template->childrenRecursive->count() > 0) {
                foreach ($template->childrenRecursive as $sub_template) {
                    $children->push($sub_template);
                }
            }
        }

        return $children;
    }

    /**
     * Save HomePageTemplate
     *
     * @param $widget_template
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createHomePage($widget_template)
    {
        $template_params = $this->getTemplateParams();
        $row_params      = $this->getRowParams();

        $model = $this->model();
        $model->fill($template_params);
        $model->save();

        $column_params      = $this->getColumnParams($model);
        $row                = $model->rows()->create($row_params);
        $column             = $row->columns()->create($column_params);
        $widget_data_params = $this->getWidgetDataParams($widget_template);
        $widget_data        = (new WidgetDataRepo())->saveFromParams($widget_data_params);
        $item_params       = $this->getItemParams($column, $model, $widget_data);
        $item               = $row->item()->create($item_params);
        $item->element_type = 'widget';
        $item->save();

        return $model;
    }

    /**
     * @return array
     */
    private function getTemplateParams()
    {
        return [
            'name'      => 'Home Page',
            'parent_id' => null,
            'active'    => 1,
            'priority'  => null,
        ];
    }

    /**
     * @return array
     */
    private function getRowParams()
    {
        return [
            'html_class'           => 'row',
            'html_container_class' => 'container',
            'html_wrapper_class'   => '',
            'order'                => 1,
            'total_size'           => 12,
            'locked'               => 0,
            'active'               => 0
        ];
    }

    /**
     * @param $template
     *
     * @return array
     */
    private function getColumnParams($template)
    {
        return [
            'order'       => 0,
            'size'        => 12,
            'locked'      => 0,
            'template_id' => $template->id
        ];
    }

    /**
     * @param $column
     * @param $template
     * @param $widget_data
     *
     * @return array
     */
    private function getItemParams($column, $template, $widget_data)
    {
        return [
            'element_type'    => 'widget',
            'column_id'       => $column->id,
            'template_id'     => $template->id,
            'element_id'      => $widget_data->id,
            'order'           => 1,
            'show_on_desktop' => '1',
            'show_on_tablet'  => '1',
            'show_on_mobile'  => '1',
            'is_live'         => 0
        ];
    }

    /**
     * @param $widget_template
     *
     * @return array
     */
    private function getWidgetDataParams($widget_template)
    {
        return [
            'widget_id'          => 1,
            'name'               => 'homepage-hello-world',
            'widget_template_id' => $widget_template->id,
            'data'               => ['content' => '<h2 class="text-center">Hello World</h2>']
        ];
    }
}