<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Section;
use App\Entities\Repositories\Repository;

class SectionRepo extends Repository
{
    const MODEL = Section::class;

    /**
     * Get It Name List
     * @return mixed
     */
    public function getIdNameList()
    {
        return Section::all()->map(function ($item) {
            return [
                'id'   => $item->id,
                'text' => $item->name
            ];
        });
    }
}
