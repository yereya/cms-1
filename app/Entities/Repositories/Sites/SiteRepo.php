<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Users\AccessControl\SiteUserPermission;
use App\Entities\Models\Users\AccessControl\SiteUserRole;
use App\Entities\Repositories\Repository;
use App\Entities\Repositories\Sites\Widgets\SiteWidgetTemplateRepo;
use App\Entities\Repositories\SiteSettingRepo;
use App\Entities\Repositories\Users\AccessControl\SiteUserRoleRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Requests\Sites\SitesFormRequest;
use DB;
use Exception;
use Illuminate\Support\Collection;


class SiteRepo extends Repository
{

    const MODEL = Site::class;

    /**
     * Get Permitted Sites
     * Get all top permitted sites with top.view permission
     *
     * @param $user
     *
     * @return Collection
     */
    public function getPermittedSites($user)
    {
        $site_user_roles = SiteUserRole::where('user_id', $user->id)
            ->whereHas('roles', function ($query) {
                $query->whereHas('permissions', function ($query) {
                    $query->where('type', 'view')->where('name', 'top');
                });
            })->get();

        $site_user_permissions = SiteUserPermission::where('user_id', $user->id)
            ->whereHas('permissions', function ($query) {
                $query->where('type', 'view')->where('name', 'top');
            })->get();

        $site_ids = array_merge(
            $site_user_roles->pluck('site_id')->toArray(),
            $site_user_permissions->pluck('site_id')->toArray()
        );

        return Site::whereIn('id', $site_ids)->get();
    }

    /**
     * Save New Site From Request
     * 1. Create new site.
     *  - Configure new connection.
     *  - Create new database and publish it.
     * 2. Seed site configuration
     *  - Create domains.
     *  - Set permission For specific user.
     *  - Set default settings
     *  - Set First Page Homepage
     * 3. Insert Site Into Session
     *
     * @param SitesFormRequest $request
     * @param Site|null        $site
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveFromRequest(SitesFormRequest $request, Site $site = null)
    {
        $params = $this->prepareParams($request);
        $model  = $site ? $site : $this->model();

        $this->createNewSite($model, $params);
        $this->seedSiteConfigurations($model, $request);

        insertSitesToSession();

        return $model;
    }

    /**
     * @param $request
     *
     * @return array
     */
    private function prepareParams($request)
    {
        $db_name = $this->resolveDBName($request);
        $model = $this->query()
            ->orderBy('created_at','desc')
            ->limit(1)
            ->get();

        $model->transform(function ($item) use ($request, $db_name) {
            return collect([
                'name'         => $request->input('name'),
                'is_public'    => $request->input('is_public') ?? 1,
                'db_host'      => $item->db_host,
                'db_user'      => $item->db_user,
                'db_pass'      => $item->db_pass,
                'db_name'      => $request->input('db_name') ?? $db_name,
                'publisher_id' => $request->input('publisher_id') ?? '',
            ]);
        });

        return $model->first()->toArray();
    }

    /**
     * Resolve the db_name property.
     *
     * @param $request
     */
    private function resolveDBName($request)
    {
        $db_name = toSlug(!empty($request->input('db_name'))
            ? $request->input('db_name')
            : $request->input('name')
        ,'_');
        $request->request->add(['db_name' => $db_name]);

        return $request->input('db_name');
    }

    /**
     * Configure Site
     *  - Set site Connection
     *  - Create database For the site
     *
     * @param $model
     */
    private function configure($model)
    {
        /*Set database and site configurations*/
        if(!$model->exists){
            SiteConnectionLib::setSite($model);
            SiteConnectionLib::createDatabase();
        }
    }

    /**
     * Create New Site Routine
     *
     * @param $model
     * @param $params
     */
    private function createNewSite($model,$params)
    {
        $model->fill($params);
        $this->configure($model);
        $model->save();
    }

    /**
     *  Create All Site Tasks While Create New Site:
     *
     *  1. domains.
     *  2. user permissions.
     *  3. settings.
     *  4. content type page.
     *  5. create home page.
     *
     * @param $model
     * @param $request
     */
    private function seedSiteConfigurations($model, $request)
    {
        /*Set domain repo*/
        $site_domain_repo = (new SiteDomainRepo());
        $site_domain_repo->saveFromRequest($model, $request);

        /*Create permission for user to view the site*/
        $site_user_role_repo = new SiteUserRoleRepo();
        if(!$site_user_role_repo->hasRole(auth()->user(), $model)){
            $site_user_role_repo->set($model);
        }

        /*Create default values for site*/
        $setting_repo = new SiteSettingRepo();
        $setting_repo->setDefaultSettings($model);

        /*Create Content Type pages to the new site*/
        $content_type_repo = (new SiteContentTypeRepo());
        $content_type_page = $content_type_repo->addContentTypePages($model);

        /*Create home page*/
        $page_repo = new PageRepo();
        $page_repo->createHomePage($content_type_page, $model);
    }
}