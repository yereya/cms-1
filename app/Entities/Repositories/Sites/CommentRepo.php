<?php

namespace App\Entities\Repositories\Sites;

use App\Entities\Models\Sites\Comment;
use App\Entities\Repositories\Repository;
use Illuminate\Http\Request;

class CommentRepo extends Repository
{
    const REVIEW = 'review';
    const REPLY_TYPE = 'reply';
    const MODEL = Comment::class;
    const LIKE_DISLIKE_TYPE = 'like_dislike';

    const TYPES = [
        self::LIKE_DISLIKE_TYPE => 'Like Dislike',
        self::REPLY_TYPE        => 'Reply',
        self::REVIEW            => 'Review'
    ];

    const RULES_BY_TYPE = [
        self::LIKE_DISLIKE_TYPE => [
            'like'    => 'required_without:dislike',
            'dislike' => 'required_without:like',
            'rating'  => 'integer'
        ],
        self::REVIEW            => [
            'author'  => 'required',
            'email'   => 'required',
            'message' => 'required',
        ],
        self::REPLY_TYPE        => [
            'author'  => 'required',
            'email'   => 'required',
            'message' => 'required'
        ],
    ];

    const CONTENT_FIELDS_BY_TYPE = [
        self::LIKE_DISLIKE_TYPE => ['like', 'dislike'],
        self::REPLY_TYPE        => ['message'],
        self::REVIEW            => ['message']
    ];


    /**
     * Get Count
     *
     * @return mixed
     */
    public function getPostCount($post_id)
    {
        return $this->query()
            ->where('post_id', $post_id)
            ->where('status', Comment::STATUS_APPROVED)
            ->where('visible', Comment::VISIBILITY_VISIBLE)
            ->count();
    }

    /**
     * Get Comments Tree
     *
     * @param $post_id
     * @param $type
     *
     * @return mixed
     */
    public function getCommentsTree($post_id, $type)
    {
        $comments = $this->query()
            ->where('post_id', $post_id)
            ->where('parent_id', null)
            ->where('type', $type)
            ->where('visible', Comment::VISIBILITY_VISIBLE)
            ->where('status', Comment::STATUS_APPROVED)
            ->with('childrenRecursive')
            ->orderBy('created_at')
            ->get();

        return $comments;
    }

    /**
     * Save Comment
     *
     * @param Request $request
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function saveComment(Request $request)
    {
        $params  = $this->buildFillParams($request);
        $comment = $this->model();
        $comment->fill($params);
        $comment->save();

        return $comment;
    }

    /**
     * Get Fill Params
     *
     * @param $request
     *
     * @return mixed
     */
    public function buildFillParams($request)
    {
        $whitelist_fields = [
            'type',
            'email',
            'post_id',
            'author',
            'notify_me',
            'parent_id',
            'rating_overall',
            'rating_1',
            'rating_2',
            'rating_3',
            'rating_4',
            'rating_5',
            'rating_6'
        ];

        $params = $request->only($whitelist_fields);

        $params['notify_me'] = ($request->get('notify_me') == "1") ? true : false;

        $type    = $params['type'];

        $content = [];

        foreach (static::CONTENT_FIELDS_BY_TYPE[$type] as $field) {
            $content[$field] = $request->get($field);
        }

        $params['content'] = $content;

        return $params;
    }
}
