<?php


namespace App\Entities\Repositories\Alerts;


use App\Entities\Models\Alerts\Account;
use App\Entities\Repositories\Repository;
use App\Http\Traits\DashboardTrait;

/**
 * Class AccountRepo
 *
 * @package app\Entities\Repositories\Alerts
 */
class AccountRepo extends Repository
{
    use DashboardTrait;

    /**
     * Model for table alert.accounts
     */
    const MODEL = Account::class;
}