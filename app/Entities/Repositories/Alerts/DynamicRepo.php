<?php


namespace App\Entities\Repositories\Alerts;


use App\Entities\Models\Alerts\Dynamic;
use App\Entities\Repositories\AlertStateRepo;
use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Repository;
use App\Entities\Repositories\UserActionFeedbackRepo;
use Illuminate\Support\Facades\DB;

/**
 * Class DynamicRepo
 *
 * @package App\Entities\Repositories\Alerts
 */
class DynamicRepo extends Repository
{

    /**
     * Dynamic Model
     */
    const MODEL = Dynamic::class;


    /**
     * Get alert stats
     *
     * @internal param $string
     */
    public function statsFactory()
    {
        $filtered_state = request()->input('filtered');
        $resolved       = $this->getResolved();

        $records = $filtered_state ? $this->getFiltered() : $this->getStats();

        if(!$records->has('error')){
            $records->put('resolved', count($resolved));
        }

        return $records;
    }

    /**
     * Build dynamic date query
     *
     * @param $query
     */
    private function whereDates($query)
    {
        $date = request()->input('date_range');

        /*IF no date passed then pass 7 days back */
        $from_date = DB::raw('NOW() - INTERVAL 7 DAY');
        $to_date   = DB::raw('NOW()');

        if (!empty($date)) {
            $date = explode(' ', $date['value']);

            if (!in_array('', $date)) {
                $from_date = $date[0];
                $to_date   = $date[1];
            }

        }


        $query->where('creation_date', '>=', $from_date);
        $query->where('creation_date', '<=', $to_date);
    }

    /**
     * Select Specific Fields From Dynamic new
     *
     * @param $query
     *
     * @return
     */
    private function selectFields($query)
    {
        return $query->select(
            'dynamic_new.id',
            'advertisers.name as advertiser_name',
            'advertiser_id',
            'account_name',
            'account_id',
            'campaign_name',
            'campaign_id',
            'ad_gourp_name',
            'ad_group_id',
            'keywords',
            'keyword_id',
            'match_type_four',
            'device',
            'cost',
            'profit',
            'profit_history',
            'cts',
            'cpa',
            'cpa_history',
            'cts_history',
            'cpc',
            'cts_history',
            'cpc',
            'cpc_history',
            'profit',
            'profit_history',
            'alert_type',
            'alert_level',
            'active_alert',
            'creation_date',
            'alert_days',
            'site_ctr',
            'site_ctr_history',
            'redundant',
            'token'
        );
    }

    /**
     *Build scoped query for general usage
     *
     * @param $query
     */
    private function scopedQuery($query)
    {
        $query->active();
        $query->orderBy('advertiser_id');
        $this->whereDates($query);
    }

    /**
     * Get resolved alerts
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getResolved()
    {
        $resolved_alert_ids = (new UserActionFeedbackRepo())->getResolvedAlertIds();

        $query = $this->query();
        $this->selectFields($query);
        $query->join('bo.advertisers as advertisers', 'advertiser_id', '=', 'advertisers.id');
        $this->scopedQuery($query);
        $query->whereIn('dynamic_new.id', $resolved_alert_ids);
        $this->whereAdvertiser($query);
        $this->whereAccount($query);
        $this->whereCampaign($query);
        $result = $query->get();

        return $result ?? collect();
    }

    /**
     * Get Accounts By Advertiser Id
     *
     * @param string $id
     *
     * @return mixed
     */
    public function getAccountsByAdvertiserId(string $id)
    {
        $query = $this->query()
            ->select('account_id', 'account_name')
            ->where('advertiser_id', $id)
            ->active()
            ->distinct('account_id');
        $this->whereDates($query);

        $result = $query->get();

        return $result;
    }

    /**
     * @param string $advertiser_id
     * @param string $account_id
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getCampaignsByAccountId(string $advertiser_id, string $account_id)
    {
        $query = $this->query();
        $query->select(['campaign_name', 'campaign_id'])
            ->where('advertiser_id', $advertiser_id)
            ->where('account_id', $account_id)
            ->active()
            ->distinct('campaign_id');

        $this->whereDates($query);
        $result = $query->get();

        return $result;
    }

    /**
     * @param $query
     */
    private function getBasicStats($query)
    {
        $this->selectFields($query);
        $query->join('bo.advertisers as advertisers', 'advertiser_id', '=', 'advertisers.id');
        $this->scopedQuery($query);
        $query->whereNotResolved();
	    $this->whereAdvertiser( $query );
	    $this->whereNotRedundant( $query );
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getStats()
    {
        $query = $this->query();
        $this->getBasicStats($query);

        $results = $query->get();

        return count($results)
            ? collect(['data' => $results])
            : collect(['error' => 'no data found']);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function getFiltered()
    {
        $query = $this->query();
        $this->getBasicStats($query);
        $this->setFilters($query);

        $results = $query->get();

        return count($results)
            ? collect(['data' => $results])
            : collect(['error' => 'no data found']);
    }


    /**
     * @param $query
     */
    private function whereAdvertiser($query)
    {
        $account_repo   = (new AccountsRepo());
        $advertisers_id = request()->input('advertiser') ?? $account_repo->getAdvertisersId();

        if (is_array($advertisers_id)) {
            $query->whereIn('advertiser_id', $advertisers_id);

            return;
        }

        $query->where('advertiser_id', $advertisers_id);
    }

    /**
     * @param $query
     */
    private function whereAccount($query)
    {
        $account_id = request()->input('account');

        if (!empty($account_id)) {
            $query->where('account_id', $account_id);
        }

    }

    /**
     * @param $query
     */
    private function whereCampaign($query)
    {
        $campaign_id = request()->input('campaign');

        if (!empty($campaign_id)) {
            $query->where('campaign_id', $campaign_id);
        }
    }


    /**
     * @param $query
     */
    private function paramsResolver($query)
    {
        $wheres = request()->input('where');

        array_walk($wheres, function ($where) use ($query) {
            $operator           = $where['operator'];
            $field              = $where['field'];
            $operator_to_amount = [
                '>'  => $where['value'][0],
                '<'  => $where['value'][1],
                '><' => $where['value']
            ];

            switch ($operator) {
                case '>':
                case '<':
                    $query->where($field, $operator, $operator_to_amount[$operator]);
                    break;

                case '><':
                    $query->whereBetween($field, $operator_to_amount[$operator]);
                    break;

                default:
                    break;
            }
        });
    }


    /**
     * get Advertisers
     */
    public function getAdvertisers()
    {
        $query = $this->query();
        $query->select([
            'advertiser_id',
            DB::raw('advertisers.name as advertiser_name'),
            'active_alert',
            'creation_date'
        ]);
        $query->join('bo.advertisers as advertisers', 'advertiser_id', '=', 'advertisers.id');
        $this->scopedQuery($query);
        $this->whereAdvertiser($query);
        $query->whereNotResolved();
        $query->distinct('advertiser_id');

        $result = $query->get();

        if (count($result)) {

            return $result->pluck('advertiser_name', 'advertiser_id');
        }
    }

    /*
     * get only not redundant alerts
     */
    /**
     * @param $query
     */
    private function whereNotRedundant($query)
    {
        $remove_duplicate = request()->input('remove_duplicates');

        if ($remove_duplicate == 1) {
	        $query->where( 'first_occurence', 1 );
        }
    }


    /**
     * @param $query
     */
    private function setFilters($query)
    {
        $this->whereAccount($query);
        $this->whereCampaign($query);
        $this->paramsResolver($query);
    }

}