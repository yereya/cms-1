<?php


namespace App\Entities\Repositories\Alerts;


use App\Entities\Models\Alerts\Budget;
use App\Entities\Repositories\Repository;
use app\Http\Traits\DashboardTrait;
use Illuminate\Support\Facades\DB;

/**
 * Class BudgetRepo
 *
 * @package App\Entities\Repositories\Alerts
 */
class BudgetRepo extends Repository
{
    use DashboardTrait;

    /**
     * Base model is alerts - budget table
     */
    const MODEL = Budget::class;
}