<?php


namespace App\Entities\Repositories\Alerts;


use App\Entities\Models\Alerts\Campaign;
use App\Entities\Repositories\Repository;
use App\Http\Traits\DashboardTrait;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Class CampaignRepo
 *
 * @package App\Entities\Repositories\Alerts
 */
class CampaignRepo extends Repository
{
    use DashboardTrait;

    /**
     * Base model is alerts - campaign table
     */
    const MODEL = Campaign::class;
}