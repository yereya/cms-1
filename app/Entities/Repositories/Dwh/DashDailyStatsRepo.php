<?php

namespace App\Entities\Repositories\Dwh;


use App\Entities\Models\Dwh\DashDailyStats;
use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Repository;
use App\Http\Traits\DashboardTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;


/**
 * Class DashDailyStatsRepo
 *
 * @package App\Entities\Repositories\Dwh
 */
class DashDailyStatsRepo extends Repository
{
    use DashboardTrait;

    const SALES_COUNT = 'sales_count';
    const DAILY = 'daily';
    const WEEKLY = 'weekly';
    const MONTHLY = 'monthly';
    const MONTHBACK = 'monthback';
    const DAYSBACK = 'daysback';
    const CPA = 'cpa';
    const PROFIT = 'profit';
    const SALES = 'sales';
    const YESTERDAY = 'yesterday';
    const TWO_WEEKS_AGO = 'two_weeks_ago';
    const TILL_NOW = 'till-now';
    const COST = 'cost';
    const REVENUE = 'revenue';
    const QUARTERS = [1 => '03', 2 => '06', 3 => '09', 4 => '12'];

    /**
     * DashDailyStats model
     */
    const MODEL = DashDailyStats::class;

    /**
     * @var
     */
    protected $from_date;

    /**
     * @var
     */
    protected $to_date;

    /**
     * Get Dash Yesterday Stats
     *
     * @return mixed
     */
    public function getYesterday()
    {
        $query = $this->query()
            ->select(
                DB::raw('sum(cost) cost'),
                DB::raw('sum(revenue) revenue'),
                DB::raw('sum(profit) profit')
            );
        $this->whereYesterday($query);
//        $this->whereAdvertisers($query); // Filer by Advertiser
        $this->whereSAdvertisers($query); // Filer by Source Advertiser
        $this->whereVerticals($query);
        $this->whereDevices($query);
        $this->whereAccounts($query);
        $this->whereChannel($query);
        if (count($query->get())) {
            return $query->get()
                ->toArray();
        }
    }

    /**
     * Get Daily Stats
     *
     * @return mixed
     */
    public function getDailyStats()
    {
        $query = $this->query();
        $this->selectByType($query);
//        $this->setFilters($query); // Filter by Advertiser
        $this->setSFilters($query); // Filter by Source Advertiser
        $this->whereDates($query);
        $this->groupByDuration($query);

        $result = $query->get();

        return count($result)
            ? $result->toArray()
            : collect();
    }

    /**
     * @return mixed
     */
    public function getLastMonth()
    {
        $query = $this->query()
            ->select(
                DB::raw('sum(cost) cost'),
                DB::raw('sum(revenue) revenue'),
                DB::raw('sum(profit) profit')
            )
            ->where('stats_date_tz', '>=',
                DB::raw("DATE_FORMAT(NOW() ,'%Y-%m-01')")
            )
            ->where(
                'stats_date_tz', '<',
                DB::raw('cast(now() as date)')
            );
//        $this->setFilters($query); // Filter by Advertiser
        $this->setSFilters($query); // Filter by Source Advertiser

        $result = $query->get();

        return count($result) ? $result->toArray() : collect();
    }

    /**
     * @return array
     */
    public function getLastTwoMonthsPercentageRatio()
    {
        $first_query = $this->query()
            ->select(
                DB::raw('sum(profit) profit_1monthback'),
                DB::raw('0 profit_2monthback')
            )
            ->where('stats_date_tz', '>=',
                DB::raw("DATE_FORMAT(NOW(), '%Y-%m-01')")
            )
            ->where('stats_date_tz', '<',
                DB::raw('cast(now() as date)'));

//        $this->whereAdvertisers($first_query);// Filter by Advertiser
        $this->whereSAdvertisers($first_query); // Filter by Source Advertiser
        $this->whereVerticals($first_query);
        $this->whereDevices($first_query);
        $this->whereAccounts($first_query);
        $this->wherechannel($first_query);

        $second_query = $this->query()
            ->select(
                DB::raw('0 profit_1monthback'),
                DB::raw('sum(profit) profit_2monthback')
            )
            ->where('stats_date_tz', '>=',
                DB::raw('DATE_FORMAT(date_sub(now(), interval 1 month) ,\'%Y-%m-01\')')
            )
            ->where('stats_date_tz', '<',
                DB::raw('date_sub(cast(now() as date), interval 1 month)')
            );
//        $this->whereAdvertisers($second_query); // Filter by Advertiser
        $this->whereSAdvertisers($second_query); // Filter by Source Advertiser
        $this->whereVerticals($second_query);
        $this->whereDevices($second_query);
        $this->whereAccounts($second_query);
        $this->wherechannel($second_query);
        $second_query->union($first_query);

        if (count($second_query->get())) {
            $results = $second_query->get()->toArray();

            return $this->getRatio($results, 'month');
        }

    }

    /**
     * Get Last Two Weeks Ratio.
     */
    public function getLastTwoWeeksPercentageRatio()
    {
        $first_query = $this->query()
            ->select(DB::raw('SUM(profit) profit_1daysback, 0 profit_2daysback'))
            ->where('stats_date_tz', DB::raw('DATE_SUB(cast(now() as date), interval 1 day)'));

//        $this->whereAdvertisers($first_query); // Filter by Advertiser
        $this->whereSAdvertisers($first_query); // Filter by Source Advertiser
        $this->whereVerticals($first_query);
        $this->whereAccounts($first_query);
        $this->whereDevices($first_query);
        $this->wherechannel($first_query);

        $second_query = $this->query()
            ->select(DB::raw('0 profit_1daysback, sum(profit)/14 profit_2daysback'))
            ->where('stats_date_tz', '>',
                DB::Raw("DATE_SUB(CAST(NOW() as date), INTERVAL 16 DAY)"))
            ->where('stats_date_tz', '<=',
                DB::raw("DATE_SUB(NOW(), INTERVAL 2 DAY)"));

//        $this->whereAdvertisers($second_query); // Filter by Advertiser
        $this->whereSAdvertisers($second_query); // Filter by Source Advertiser
        $this->whereVerticals($second_query);
        $this->whereAccounts($second_query);
        $this->whereDevices($second_query);
        $this->wherechannel($second_query);
        $second_query->union($first_query);

        $query = $second_query->get();

        if (count($query)) {
            $result = $query->toArray();
            return $this->getRatio($result, 'day');
        }
    }

    /**
     * Get Ratio
     *
     * @param array $results
     * @param       $period
     *
     * @internal  $diff  - difference between the two period.
     * @internal  $ratio -  ratio between the two period.
     * @return array
     */
    private function getRatio(array $results, string $period)
    {
        $period          = $period == 'month' ? self::MONTHBACK : self::DAYSBACK;
        $one_period_back = [];
        $two_period_back = [];

        foreach ($results as $key => $value) {
            array_push($one_period_back, $value['profit_1' . $period]);
            array_push($two_period_back, $value['profit_2' . $period]);
        }

        $sum_one_period_back = array_sum($one_period_back);
        $sum_two_period_back = array_sum($two_period_back);

        if ($sum_one_period_back > 0 && $sum_two_period_back > 0) {
            $initial_ratio = (($sum_one_period_back / $sum_two_period_back) - 1);
            $ratio         = round($initial_ratio * 100, 4);
        } else {
            $ratio = 0;
        }

        $diff = $sum_one_period_back - $sum_two_period_back;

        return [
            'params'  => [
                $period . 'profit_1' => $sum_one_period_back,
                $period . 'profit_2' => $sum_two_period_back
            ],
            'percent' => $ratio,
            'diff'    => $diff,
        ];
    }

    /**
     * Get Table Results by Period
     * PPC - Advertiser-target
     * - create sub query that gets all stats result - (sales/leads = according to advertiser id)
     * - create outer query that sum all variables
     * - return array
     *
     * @param $period
     *
     * @return mixed
     */
    public function getByPeriod(string $period)
    {
        $inner_query = $this->getInnerResults();
        $this->wherePeriod($inner_query, $period);
        $this->addGlobalFilters($inner_query);

        $query = DB::table(DB::raw("({$inner_query->toSql()}) as sub"))
            ->mergeBindings($inner_query->getQuery())
            ->select(
                DB::raw('SUM(sales) as sales'),
                DB::raw('SUM(cost) as cost'),
                DB::raw('SUM(revenue) as revenue')
            );

        $result = $query->first();

        return $result ? (array)$result : [];
    }


    /**
     * Get Table Results by Period
     * PPC - Advertiser-target
     * - create sub query that gets all stats result - (sales/leads = according to advertiser id)
     * - create outer query that sum all variables
     * - return array
     *
     * @param $period
     *
     * @return mixed
     */
    public function getAdvertisersStatsByPeriod(string $period)
    {
        $inner_query = $this->getInnerResults('advertiser_name');
        $this->wherePeriod($inner_query, $period);
        $this->addGlobalFilters($inner_query);

        $query = DB::table(DB::raw("({$inner_query->toSql()}) as sub"))
            ->mergeBindings($inner_query->getQuery())
            ->select(
                DB::raw('SUM(sales) as sales'),
                DB::raw('SUM(cost) as cost'),
                DB::raw('SUM(revenue) as revenue')
            );

        $result = $query->first();

        return $result ? (array)$result : [];
    }


    /**
     * Build query according to the period variable
     * PPC - Advertiser-target - table
     *
     * @param $query
     * @param $period
     *
     * @return mixed
     */
    private function wherePeriod($query, $period)
    {
        switch ($period) {
            case self::YESTERDAY:
                return $query
                    ->whereRaw('stats_date_tz = DATE_SUB(CAST(NOW() AS DATE), INTERVAL 1 DAY)');

            case self::TWO_WEEKS_AGO:
                return $query
                    ->whereRaw('stats_date_tz <= date_sub(cast(NOW() as date), interval 2 day)')
                    ->whereRaw('stats_date_tz > date_sub(cast(NOW() as date), interval 16 day)');

            case self::TILL_NOW:
                return $query
                    ->whereRaw("stats_date_tz between date_format(now() ,'%Y-%m-01') and now()");
        }
    }

    /**
     *Prepare statistics for response
     * PPC - Advertiser-target - table
     *
     * @param array  $stats
     * @param string $period
     *
     * @return array
     * @internal param $statistics
     * @internal param $stats
     */
    public function prepareStats(array $stats, string $period)
    {
        $no_zero_value  = $stats[self::COST] != 0 && $stats[self::SALES] != 0;
        $prepared_stats = [];

        foreach ($stats as $key => $stat) {
            $period_is_two_weeks_ago = $period == self::TWO_WEEKS_AGO;
            $stats[$key]             = $period_is_two_weeks_ago ? $stat / 14 : $stat;
        }

        /*type cast sales values to integer (it pass as string for some reason)*/
        $prepared_stats[self::SALES]  = (int)$stats[self::SALES];
        $prepared_stats[self::CPA]    = $no_zero_value ? round($stats[self::COST] / $stats[self::SALES], 1) : 0;
        $prepared_stats[self::PROFIT] = round($stats[self::REVENUE] - $stats[self::COST]);

        return $prepared_stats;
    }

    /**
     * Resolve Statistics By Period
     * PPC - Advertiser-target - table
     *
     * @param $period
     *
     * @return mixed
     */
    public function resolveStatsByPeriod(string $period)
    {
        $stats = $this->getByPeriod($period);

        return $this->prepareStats($stats, $period);
    }


    public function resolveTargetStats()
    {

        $diff_from_target      = [];
        $days_from_start_month = date('j');
        $stats                 = [self::CPA, self::SALES, self::PROFIT];
        $dash_daily_repo       = (new DashDailyStatsRepo());
        $till_now              = $dash_daily_repo->getStatsTillNow(self::TILL_NOW);
        $monthly_target        = $this->resolveMonthlyTargets();
        $days_of_month         = date('t', strtotime($monthly_target['date']));
    }


    /**
     *  Get Sales Results
     *  1- set sub query (select all inner stats and conditional sales according to advertiser id).
     *  2- add global filter - (advertisers and verticals ).
     *  3- add specific filters.
     *  4- create new query that wraps the query and sum all the results and group them by date.
     *  5- change results to collection.
     */
    public function _getStatsResult()
    {
        $query = $this->resolveQueryByTarget();
        $query->groupBy('date');
        $result = $query->get();
        $result = collect($result);

        return $result ?? collect();
    }

    /**
     *  Get Chart Results - (Results
     *  1- set sub query (select all inner stats and conditional sales according to advertiser id).
     *  2- add global filter - (advertisers and verticals ).
     *  3- add specific filters.
     *  4- create new query that wraps the query and sum all the results and group them by date.
     *  5- change results to collection.
     */
    public function getStatsResult()
    {
        $outer_fields = "date,SUM(profit) profit,SUM(conversion) conversion, SUM(cost) cost";
        $inner_fields = "stats_date_tz AS date, profit,IF(sale_conversion=1, sales_count,leads) conversion,cost, publisher_name";

        /*INNER QUERY LEVEL ONE*/
        $inner_query_level_one = $this->getChartStatsResultLevelOne($inner_fields);

        /*INNER QUERY LEVEL TWO*/
        $inner_query_level_two = $this->buildOuterQuery(null, $inner_query_level_one);
        $this->wherePublisherNameNotSEOORNULL($inner_query_level_two);

        /*OUTER QUERY*/
        $outer_query = $this->buildOuterQuery($outer_fields, $inner_query_level_two);
        $outer_query->where("publisher_name", '<>', "Branding Package");
        $outer_query->groupBy(['date']);

        $results = $outer_query->get();

        $results = collect($results);

        return $results
            ? $results->map(function ($result) {
                return (array)$result;
            })
            : collect();
    }
    /**
     *  Get Chart Results By Source Advertiser- (Results
     *  1- set sub query (select all inner stats and conditional sales according to advertiser id).
     *  2- add global filter - (advertisers and verticals ).
     *  3- add specific filters.
     *  4- create new query that wraps the query and sum all the results and group them by date.
     *  5- change results to collection.
     */
    public function getStatsResultBySAdvertiser()
    {
        $outer_fields = "date,SUM(profit) profit,SUM(conversion) conversion, SUM(cost) cost";
        $inner_fields = "stats_date_tz AS date, profit,IF(sale_conversion=1, sales_count,leads) conversion,cost, publisher_name";

        /*INNER QUERY LEVEL ONE*/
        $inner_query_level_one = $this->getChartStatsResultLevelOneBySAdvertiser($inner_fields);

        /*INNER QUERY LEVEL TWO*/
        $inner_query_level_two = $this->buildOuterQuery(null, $inner_query_level_one);
        $this->wherePublisherNameNotSEOORNULL($inner_query_level_two);
        /*OUTER QUERY*/
        $outer_query = $this->buildOuterQuery($outer_fields, $inner_query_level_two);
        $outer_query->where("publisher_name", '<>', "Branding Package");
        $outer_query->groupBy(['date']);

        $results = $outer_query->get();

        $results = collect($results);

        return $results
            ? $results->map(function ($result) {
                return (array)$result;
            })
            : collect();
    }


    /**
     * Where target
     *
     * @param $query
     *
     * @return mixed
     */
    private
    function selectResults($query)
    {
        $target   = request()->input('target') ?? self::SALES_COUNT;
        $duration = request()->input('duration') ?? self::DAILY;
        $options  = [
            self::DAILY   => [
                self::CPA    => 'stats_date_tz as date, (sum(cost))/(sum(sales)) as cpa',
                self::PROFIT => 'stats_date_tz as date, sum(profit) as profit',
                self::SALES  => 'stats_date_tz as date, sum(sales) as sales'
            ],
            self::WEEKLY  => [
                self::CPA    => 'CONCAT(YEAR(stats_date_tz),"-",WEEK(stats_date_tz)) as date, (sum(cost))/(sum(sales)) as cpa',
                self::PROFIT => 'CONCAT(YEAR(stats_date_tz),"-",WEEK(stats_date_tz)) as date, sum(profit) as profit',
                self::SALES  => 'CONCAT(YEAR(stats_date_tz),"-",WEEK(stats_date_tz)) as date, sum(sales) as sales'
            ],
            self::MONTHLY => [
                self::CPA    => 'DATE_FORMAT(stats_date_tz ,"%Y-%m-01") as date, sum(cost)/sum(sales) as cpa',
                self::PROFIT => 'DATE_FORMAT(stats_date_tz ,"%Y-%m-01") as date, sum(profit) as profit',
                self::SALES  => 'DATE_FORMAT(stats_date_tz ,"%Y-%m-01") as date, sum(sales) as sales'
            ]
        ];

        return $query->selectRaw($options[$duration][$target]);
    }

    /**
     * Get stats till now.
     * PPC - Advertiser-target - table
     *
     * @return mixed
     */
    public
    function getStatsTillNow()
    {
        $till_now           = $this->getByPeriod(self::TILL_NOW);
        $till_now['profit'] = $till_now['revenue'] - $till_now['cost'];

        return $till_now;
    }

    /**
     * Get Inner Query For Getting stats
     * PPC - Advertiser-target - table
     *
     * @param $extra_column
     *
     * @return mixed
     */
    private
    function getInnerResults($extra_column = null)
    {
        $model        = $this->model();
        $table        = $model->getConnectionName() . '.' . $model->getTable();
        $sales_select = 'CASE WHEN advertiser_id IN ("76","82","132","134","0","133") THEN leads ELSE sales_count END AS sales';

        $sub = $this->query()
            ->from($table)
            ->select('cost', 'revenue', DB::raw($sales_select));

        if ($extra_column) {
            $sub->select('cost', 'revenue', DB::raw($sales_select), $extra_column);
        }

        return $sub;

    }


    /**
     * Add global filters :
     * - advertiser
     * - verticals
     * PPC - Advertiser-target - table
     *
     * @param $query
     */
    private function addGlobalFilters($query,$s_advertiser=null)
    {
        if(!isset($s_advertiser)){
            $this->whereAdvertisers($query);
        }else{
            $this->whereSAdvertisers($query);
        }
        $this->whereVerticals($query);
        $this->whereSaleGroup($query);
        $this->whereBusinessGroup($query);
        $this->whereTrafficSource($query);
    }


    /**
     *      * Get Query Results
     *
     * @return Builder
     */
    private
    function prepareBasic()
    {
        $query = $this->query();
        $this->selectResults($query);
        $this->addGlobalFilters($query);
        $this->whereDates($query);

        return $query;
    }

    /**
     * Prepare subquery for advertiser targets
     *
     * @return $this
     */
    private
    function prepareWithSubQuery()
    {
        $inner_query = $this->getInnerResults('stats_date_tz');
        $this->addGlobalFilters($inner_query);
        $this->whereDates($inner_query);

        $query = DB::table(DB::raw("({$inner_query->toSql()}) as sub"))
            ->mergeBindings($inner_query->getQuery());

        $this->selectResults($query);

        return $query;
    }

    /**
     * Create Dynamic Query According to target parameter
     *
     * @return DashDailyStatsRepo|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    private
    function resolveQueryByTarget()
    {
        $target = request()->input('target') ?? SELF::SALES;

        switch ($target) {
            case SELF::SALES:
            case SELF::CPA:
                return $this->prepareWithSubQuery();

            default:
                return $this->prepareBasic();
        }
    }

    /**
     * Get quarterly results
     *
     * @param $quarter
     *
     * @return array|static[]
     */
    public
    function getQuarterlyResultsTillNow($quarter)
    {
        $inner_query = $this->query();
        $this->getInnerQuery($inner_query);
        $this->addGlobalFilters($inner_query);
        $from_date = $this->resolveFirstDayOfQuarter(self::QUARTERS[$quarter]);
        $to_date   = date('Y-m-d');

        $query = DB::table(DB::raw("({$inner_query->toSql()}) as sub"))
            ->mergeBindings($inner_query->getQuery())
            ->selectRaw('sum(sales) sales, sum(cost) cost, sum(profit) profit, date')
            ->where('quarter', $quarter)
            ->where('date', '>=', $from_date)
            ->where('date', '<=', $to_date);


        $results = $query->first();

        return $results ?? [];
    }

    /**
     * Get results per quarter
     *
     * @param $query
     *
     * @return mixed
     */
    private
    function getInnerQuery($query)
    {
        $model = $this->model();
        $table = $model->getConnectionName() . '.' . $model->getTable();

        $query
            ->from($table)
            ->select(
                DB::raw('sales_count as sales'),
                DB::raw('cost'),
                DB::raw('profit'),
                DB::raw('QUARTER(stats_date_tz) as quarter'),
                DB::raw('stats_date_tz as date')
            );
    }

    /**
     * Select query fields according to target
     *
     * @param $query
     */
    private
    function selectByType($query)
    {
        $target = request()->input('target_type') ?? 'cost_profit';
        $date   = $this->resolveDurationSelector();

        switch ($target) {
            case 'profit':
                $query_string = DB::raw("{$date} ,advertiser_name, SUM(profit) profit");
                break;

            case 'clicks':
                $query_string = DB::raw("{$date} ,SUM(clicks) clicks");
                break;

            case 'sales_count':
                $query_string = DB::raw("{$date} ,SUM(sales_count) sales_count");
                break;

            case 'avg_position':
                $query_string = DB::raw("{$date} ,(sum_of_impressions/ SUM(`impressions`)) as avg_position");
                break;

            case 'cts':
                $query_string = DB::raw("{$date} ,ROUND(SUM(`sales_count`) / SUM(`clicks`),3) cts");
                break;

            case 'site_ctr':
                $query_string = DB::raw("{$date} ,ROUND(SUM(`out_clicks`) / SUM(`clicks`), 2) as site_ctr");
                break;

            case 'cpc':
                $query_string = DB::raw("{$date}, ROUND(SUM(`cost`)/SUM(`clicks`),2) as cpc");

                break;

            case 'click_out_unique':
                $query_string = DB::raw("{$date}, ROUND(SUM(`click_out_unique`)/SUM(`clicks`),2) as click_out_unique");

                break;


            default:

                $query_string = DB::raw("{$date} ,SUM(cost) cost, SUM(revenue) revenue, SUM(profit) profit");
                break;


        }

        $query->select($query_string);
    }

    /**
     * Resolve Duration Selector
     *
     * @return string
     */
    private
    function resolveDurationSelector()
    {
        $duration          = request()->input('duration') ?? 'daily';
        $duration_to_query = [
            'daily'   => 'stats_date_tz as date',
            'weekly'  => 'CONCAT(YEAR(stats_date_tz),"-",WEEK(stats_date_tz)) as date',
            'monthly' => 'DATE_FORMAT(stats_date_tz ,\'%Y-%m\') as date'
        ];

        return $duration_to_query[$duration];
    }

    /**
     * Get advertisers
     *  - get active advertisers
     *
     * @return mixed
     */
    public function getAdvertisers()
    {
        $accounts_repo  = (new AccountsRepo());
        $advertisers_id = request()->input('advertisers') ?? $accounts_repo->getAdvertisersId();

        return $this->model()
            ->select(DB::raw('DISTINCT advertiser_name, advertiser_id'))
            ->whereIn('advertiser_id', $advertisers_id);
    }

    /**
     * Get advertisers - New Method
     *  - get active advertisers
     *
     * @return mixed
     */
    public function getAdvertisersNew()
    {
        $accounts_repo  = (new AccountsRepo());
        $advertisers_id = request()->input('advertisers') ?? $accounts_repo->getAdvertisersId();

        $advertisers = DB::connection('bo')->table('advertisers')->whereIn('id',$advertisers_id)->get();

        $advertisers = array_reduce($advertisers->toArray(), function ($result,$advertiser){
            if($advertiser &&  $advertiser->status === 'active')
                $result[$advertiser->id] = $advertiser->name;
            return $result;
        },[]);

        return $advertisers;
    }

    /**
     * Get All Verticals
     *
     * @return \Illuminate\Database\Query\Builder| Builder $this
     */
    public function getVerticals()
    {
        $account_names = (new AccountsRepo())->getUniqueActiveUserAccountsNames();

        return $this->query()
            ->select('product_name as vertical')
            ->whereIn('product_name', $account_names);
    }
    /**
     * Get All Verticals - New Method
     *
     * @return \Illuminate\Database\Query\Builder| Builder $this
     */
    public function getVerticalsNew()
    {
        $account_names = (new AccountsRepo())->getUniqueActiveUserAccountsNames();
        $verticals = array_reduce($account_names->toArray(), function ($result,$account_name){
            if($account_name && $account_name != 'NULL')
                $result[$account_name] = $account_name;
            return $result;
        },[]);

        return $verticals;

    }

    /**
     * Get All Accounts
     *
     * @return \Illuminate\Support\Collection | Builder
     */
    public function getAccounts()
    {
        $user_accounts = (new AccountsRepo())->getActiveUserAccounts();

        return $this->query()
            ->select('account_id', 'account_name as account', 'advertiser_id')
            ->distinct()
            ->whereIn('account_id', $user_accounts->pluck('source_account_id'));
    }
    /**
     * Get All Accounts - New Method
     *
     * @return \Illuminate\Support\Collection | Builder
     */
    public function getAccountsNew()
    {
        $user_accounts = (new AccountsRepo())->getActiveUserAccounts();

        $user_accounts = array_reduce($user_accounts->toArray(), function ($result,$account){
            if($account &&  $account['status'] === 'active')
                $result[$account['source_account_id']] = $account['name'];
            return $result;
        },[]);

        return $user_accounts;
    }
    /**
     * @return array|\Illuminate\Support\Collection
     */
    public
    function getMorningStats()
    {
        $query = $this->query();
        $query->select(DB::raw("stats_date_tz AS `date` ,advertiser_name, SUM(profit) profit"));
        $this->whereAdvertisers($query);
        $this->whereLastMonth($query);
        $query->groupBy('advertiser_name', 'stats_date_tz');

        $result = $query->get();

        return $result ? $result->toArray() : collect();

    }

    /**
     * @param $query
     */
    private
    function whereLastMonth($query)
    {
        $from_date = date('Y-m-d', strtotime('-30 day'));
        $to_date   = date('Y-m-d');
        $query->whereBetween("stats_date_tz", [$from_date, $to_date]);
    }

    /**
     * @return array|\Illuminate\Support\Collection
     */
    public
    function getOverallStats()
    {
        $query = $this->query();
        $query->select(DB::raw('stats_date_tz AS DATE, SUM(profit) profit ,\'Overall\' AS advertiser_name'));
        $this->whereAdvertisers($query);
        $this->whereLastMonth($query);
        $query->groupBy('stats_date_tz');

        $result = $query->get();

        return $result->toArray() ?? collect();
    }

    /**
     * @param $params
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public
    function getLast4WeeksAvg($params)
    {
        /*build inner query*/
        $inner_query = $this->buildInnerQuery($params);

        /*join Tables according to table name*/
        $this->resolveJoinsByTableName($params['table'], $inner_query);

        /*filter date*/
        $this->whereLast4WeekAvg($inner_query);

        /*filter specific advertisers*/
        $this->whereAdvertisers($inner_query, $params); // Filter by Advertiser
//        $this->whereSAdvertisers($inner_query, $params); // Filter by Source Advertiser

        /*build outer query*/
        $outer_query = $this->buildOuterQuery($params['fields']['outer'], $inner_query);
        $outer_query->where("publisher_name", '<>', "Branding Package");
        /*group by dynamic field*/
        $outer_query->groupBy($params['groupBy']);

        /*order by dynamic field*/
        $outer_query->orderBy($params['orderBy']);

        return $outer_query->get();
    }


    /**
     * @param $query
     */
    private
    function whereLast4WeekAvg($query)
    {
        $last4WeekDates = [
            0 => 'CAST(DATE_SUB(NOW(), INTERVAL 8 DAY) AS DATE)',
            1 => 'CAST(DATE_SUB(NOW(), INTERVAL 15 DAY) AS DATE)',
            2 => 'CAST(DATE_SUB(NOW(), INTERVAL 22 DAY) AS DATE)',
            3 => 'CAST(DATE_SUB(NOW(), INTERVAL 29 DAY) AS DATE)'
        ];

        $query->whereRaw("stats_date_tz IN ($last4WeekDates[0], $last4WeekDates[1], $last4WeekDates[2], 
        $last4WeekDates[3])");
    }


    /**
     *  Filter wrapper
     *
     * @param $query
     */
    private function setFilters($query)
    {
        $this->whereAdvertisers($query); // Filter by Advertiser
        $this->whereVerticals($query);
        $this->whereDevices($query);
        $this->whereAccounts($query);
        $this->wherechannel($query);
    }
    /**
     *  Filter wrapper by source advertiser
     *
     * @param $query
     */
    private function setSFilters($query)
    {
        $this->whereSAdvertisers($query); // Filter by Source Advertiser
        $this->whereVerticals($query);
        $this->whereDevices($query);
        $this->whereAccounts($query);
        $this->whereChannel($query);
    }
    /**
     * Group records by view duration
     *
     * @param Builder $query
     */
    private
    function groupByDuration($query)
    {
        $duration          = request()->input('duration');
        $duration_to_query = [
            'daily'   => 'date',
            'weekly'  => 'WEEK(stats_date_tz)',
            'monthly' => 'MONTH(stats_date_tz)'
        ];

        $query->groupBy(DB::raw($duration_to_query[$duration]));
    }


    /**
     * Get first day of quarter according to quarter last month
     *
     * @param $quarter
     *
     * @return false|string
     */
    private
    function resolveFirstDayOfQuarter($quarter)
    {
        switch ($quarter) {
            case '03':
                return date('Y-01-01');
            case '06':
                return date('Y-04-01');
            case '09':
                return date('Y-07-01');
            case '12':
                return date('Y-10-01');
        }
    }


    /**
     * Get Dash Yesterday Stats Grouped By Advertisers
     *
     * @return mixed
     */
    public
    function getYesterdayGroupedByAdvertiser()
    {
        $query = $this->query()
            ->select(
                DB::raw('advertiser_name'),
                DB::raw('advertiser_id'),
                DB::raw('sum(cost) cost'),
                DB::raw('sum(sales_count) sales'),
                DB::raw('sum(revenue) revenue'),
                DB::raw('round(sum(revenue) - sum(cost)) profit')
            );
        $this->whereYesterday($query);
        $this->whereAdvertisers($query);
        $this->whereVerticals($query);
        $this->whereDevices($query);
        $this->whereAccounts($query);
        $this->wherechannel($query);
        $query->groupBy('advertiser_name');
        return $query->get()->toArray();
    }

    /**
     * Get Dash Last Month Stats Grouped By Advertisers
     *
     * @return mixed
     */
    public
    function getLastMonthGroupedByAdvertisers()
    {
        $query = $this->query()
            ->select(
                DB::raw('advertiser_name'),
                DB::raw('advertiser_id'),
                DB::raw('sum(cost) cost'),
                DB::raw('sum(sales_count) sales'),
                DB::raw('sum(revenue) revenue'),
                DB::raw('round(sum(revenue) - sum(cost)) profit')
            )
            ->where('stats_date_tz', '>=',
                DB::raw("DATE_FORMAT(NOW() ,'%Y-%m-01')")
            )
            ->where(
                'stats_date_tz', '<',
                DB::raw('cast(now() as date)')
            );

        $this->whereAdvertisers($query);
        $this->whereVerticals($query);
        $this->whereDevices($query);
        $this->whereAccounts($query);
        $this->wherechannel($query);
        $query->groupBy('advertiser_name');
        $query->orderBy('advertiser_name');

        $result = $query->get();

        return count($result) ? $result->toArray() : collect();
    }

    /**
     * Get stats till now Grouped By Advertisers.
     *
     * @return mixed
     */
    public
    function getAdvertisersStatsTillNow()
    {
        $innerRepo  = new DashDailyStatsRepo();
        $innerQuery = $innerRepo->query();
        $innerQuery->select(
            DB::raw('stats_date_tz'),
            DB::raw('advertiser_name'),
            DB::raw('advertiser_id'),
            DB::raw('SUM(cost) AS cost'),
            DB::raw('SUM(sales_count) AS sales'),
            DB::raw('sum(revenue) revenue'),
            DB::raw('round(SUM(revenue) - sum(cost)) AS profit')
        );
        $model = $this->model();
        $innerQuery->from($model->getConnection()->getDatabaseName() . '.' . $model->getTable());
        $innerRepo->whereAdvertisers($innerQuery);
        $innerRepo->whereVerticals($innerQuery);
        $innerRepo->whereDevices($innerQuery);
        $innerRepo->whereAccounts($innerQuery);
        $innerRepo->wherechannel($innerQuery);
        $innerQuery->whereRaw("stats_date_tz BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01') AND NOW()");

        $innerQuery->groupBy('advertiser_name')
            ->groupBy('stats_date_tz')
            ->orderBy('advertiser_name');


        $outerQuery = DB::table(DB::raw("({$innerQuery->toSql()}) as sub"))
            ->mergeBindings($innerQuery->getQuery());
        $outerQuery->select(
            DB::raw('advertiser_name'),
            DB::raw('advertiser_id'),
            DB::raw('SUM(cost) AS cost'),
            DB::raw('SUM(sales) AS sales'),
            DB::raw('SUM(profit) AS profit')
        )
            ->groupBy('advertiser_name');

        $result = $outerQuery->get()->toArray();
        return $result;
    }

    /**
     * @param $display_value
     *
     * @return \Illuminate\Support\Collection
     */
    public function getDailyAvgActualResults($display_value)
    {
        /*get data from db*/
        $actual_results = $this->getStatsResults();

        /*cook data*/
        if (count($actual_results) > 0) {
            $fields = ['a_cost', 'a_revenue', 'a_conversions', 'a_profit'];

            return $this->getRecordsFromMonthlyAccordingToDisplayValue($actual_results, $display_value, $fields);
        }

        return collect();
    }

    /**
     * @param $display_value
     *
     * @return \Illuminate\Support\Collection
     */
    public function getDailyAvgActualResultsBySAdvertiser($display_value)
    {
        /*get data from db*/
        $actual_results = $this->getStatsResultsBySadvertiser();

        /*cook data*/
        if (count($actual_results) > 0) {
            $fields = ['a_cost', 'a_revenue', 'a_conversions', 'a_profit'];

            $results = $this->getRecordsFromMonthlyAccordingToDisplayValue($actual_results, $display_value, $fields);

            foreach ($results as $actual_result){
                if(!$actual_result['a_revenue'] || !$actual_result['a_conversions']){
                    $actual_result['a_epa'] = 0;
                }else{
                    $actual_result['a_epa'] = $actual_result['a_revenue'] / $actual_result['a_conversions'];
                }
                if(!$actual_result['a_revenue'] || !$actual_result['a_cost']){
                    $actual_result['a_roi'] = 0;
                }else{
                    $actual_result['a_roi'] =($actual_result['a_revenue']/$actual_result['a_cost'] )  ;
                }
            }
            return $results;
        }

        return collect();
    }


    /**
     * Get Stats Result ( For Table target vs Actual At Ppc Targets Dashboards)
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     * @inner_query Builder
     */
    private function getStatsResults()
    {
        $outer_fields = 'advertiser_id,DATE_FORMAT(stats_date_tz,\'%Y-%m\') AS stats_date_tz,days_in_month,advertiser,sum(cost) a_cost,sum(revenue) a_revenue,SUM(profit) a_profit,SUM(conversions) a_conversions';
        /*LEVEL ONE*/
        $inner_query_level_one = $this->getResultStatsLevelOneSubQuery();

        /*LEVEL TWO*/
        $inner_query_level_two = $this->buildOuterQuery(null, $inner_query_level_one);
        $this->wherePublisherNameNotSEOORNULL($inner_query_level_two);

        /*LEVEL TREE*/
        $outer_query = $this->buildOuterQuery($outer_fields, $inner_query_level_two);
        $outer_query->where("publisher_name", '<>', "Branding Package");
        $outer_query->groupBy([
                'advertiser',
                'days_in_month',
                DB::raw('DATE_FORMAT(stats_date_tz,"%Y-%m")', 'advertiser_id'),
                'advertiser_id'
            ]
        );

        $result = $outer_query->get();

        return $result ?? collect();
    }
    /**
     * Get Stats Result based on source advertiser ( For Table target vs Actual At Ppc Targets Dashboards)
     *
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     * @inner_query Builder
     */
    private function getStatsResultsBySAdvertiser()
    {
        $outer_fields = 'advertiser_id,DATE_FORMAT(stats_date_tz,\'%Y-%m\') AS stats_date_tz,days_in_month,advertiser,sum(cost) a_cost,sum(revenue) a_revenue,SUM(profit) a_profit,SUM(conversions) a_conversions';
        /*LEVEL ONE*/
        $inner_query_level_one = $this->getResultStatsLevelOneSubQueryBySAdvertiser();

        /*LEVEL TWO*/
        $inner_query_level_two = $this->buildOuterQuery(null, $inner_query_level_one);
        $this->wherePublisherNameNotSEOORNULL($inner_query_level_two);
        /*LEVEL TREE*/
        $outer_query = $this->buildOuterQuery($outer_fields, $inner_query_level_two);
        $outer_query->where("publisher_name", '<>', "Branding Package");
        $outer_query->groupBy([
                'advertiser',
                'days_in_month',
                DB::raw('DATE_FORMAT(stats_date_tz,"%Y-%m")', 'advertiser_id'),
                'advertiser_id'
            ]
        );

        $result = $outer_query->get();

        return $result ?? collect();
    }

    /**
     * @param $records
     * @param $display_value
     *
     * @return \Illuminate\Support\Collection
     */
    private
    function _getRecordsFromMonthlyAccordingToDisplayValue($records, $display_value): \Illuminate\Support\Collection
    {
        $_records         = collect();
        $advertisers      = $records->keyBy('advertiser');
        $start_n_end_date = $this->resolveStartEndDate();

        $advertisers->each(function ($adv_record, $advertiser)
        use ($_records, $records, $start_n_end_date) {

            $records->each(function ($record) use ($_records, $advertiser, $start_n_end_date) {
                $temp          = collect();
                $days_of_month = $record->days_in_month;

                /*Days in range*/
                $days_in_range = $this->resolveDaysInRange($start_n_end_date, $record->stats_date_tz, $days_of_month);

                /*Create Advertiser*/
                $temp->put('advertiser', $record->advertiser);

                /*Iterate on same advertiser */
                if ($advertiser == $record->advertiser) {
                    $fields = ['a_cost', 'a_revenue', 'a_conversions', 'a_profit'];
                    array_map(function ($field) use (
                        $temp, $record, $advertiser, $_records, $days_of_month, $days_in_range
                    ) {

                        /*Assign record according to display value type (daily avg , till now )*/
                        $record = $this->resolveResultsByDisplayValues($field, $record, $days_of_month, $days_in_range);

                        /*If value is already assign - update the value*/
                        $target_existence = $_records->where('advertiser', $advertiser)->first()[$field];
                        if ($target_existence) {
                            $_record_model  = $_records->where('advertiser', $advertiser)->first();
                            $current_record = $_record_model[$field];
                            $updated_record = $current_record + $record;
                            $_records->put($field, $updated_record);
                        } else {
                            $temp->put($field, $record);
                        }
                        $temp->put($field, $record);
                    }, $fields);
                }

                if (count($temp) > 1) {
                    $temp->put('a_cpa', round($temp['a_cost'] / $temp['a_conversions']));
                    $_records->push($temp);
                }
            });
        });

        if ($display_value == 'daily') {
            $_records = $this->getAvg($_records, 'a_');
        }

        return $_records;
    }

    /**
     * @param $targets
     * @param $results
     *
     * @return \Illuminate\Support\Collection
     */
    public
    function getTargetVsActual($targets, $results): \Illuminate\Support\Collection
    {
        $target_n_result = collect();
        $ratio           = $this->getTargetResultRatio($targets, $results)->keyBy('advertiser_id');

        $targets->each(function ($target, $key) use ($results, $target_n_result, $ratio) {
            $temp = collect();
            $temp->put('advertiser', $target['advertiser']);
            $fields  = [
                't_cost',
                'a_cost',
                't_revenue',
                'a_revenue',
                't_profit',
                'a_profit',
                't_conversions',
                'a_conversions',
                't_cpa',
                'a_cpa',
                't_epa',
                'a_epa',
                't_roi',
                'a_roi'
            ];
            $results = $results->keyBy('advertiser_id');

            array_map(function ($field) use ($target, $temp, $results, $key, $ratio) {
                $advertiser_id_target = $target['advertiser_id'];
                /*add values according to advertiser id*/
                if (\count($results->where('advertiser_id', $advertiser_id_target)->first())) {
                    if ($target->has($field)) {
                        return $temp->put($field, $target[$field]);
                    }

                    $value  = $results[$advertiser_id_target][$field];
                    $_ratio = $ratio[$advertiser_id_target][str_replace('a_', '', $field)];

                    return $temp->put($field, ["ratio" => $_ratio, 'value' => $value]);
                }
            }, $fields);

            $target_n_result->push($temp);
        });

//        /*Sort by actual cost*/
        $target_n_result = $target_n_result->sortByDesc('a_cost.value')->values();

        return $target_n_result;
    }


    public function getTillNowActualResults($display_value)
    {
        $actual_results = $this->getStatsResults();

        if ($actual_results) {
            $fields = ['a_cost', 'a_revenue', 'a_conversions', 'a_profit'];

            return $this->getRecordsFromMonthlyAccordingToDisplayValue($actual_results, $display_value, $fields);
        }
    }

    public function getTillNowActualResultsBySAdvertiser($display_value)
    {
        $actual_results = $this->getStatsResultsBySAdvertiser();

        if ($actual_results) {
            $fields = ['a_cost', 'a_revenue', 'a_conversions', 'a_profit'];

            $results = $this->getRecordsFromMonthlyAccordingToDisplayValue($actual_results, $display_value, $fields);

            foreach ($results as $actual_result){
                if(!$actual_result['a_revenue'] || !$actual_result['a_conversions']){
                    $actual_result['a_epa'] = 0;
                }else{
                    $actual_result['a_epa'] = $actual_result['a_revenue'] / $actual_result['a_conversions'];
                }
                if(!$actual_result['a_revenue'] || !$actual_result['a_cost']){
                    $actual_result['a_roi'] = 0;
                }else{
                    $actual_result['a_roi'] = ($actual_result['a_revenue']/$actual_result['a_cost'] ) ;
                }
            }
            return $results;
        }
    }

    public function whereTargetActualDates($query)
    {
        $from_date  = request()->input('from') ?? date('Y-m-01', time());
        $to_date    = request()->input('to') ?? date('Y-m-d', strtotime('-1 day'));
        $date_range = request()->input('date_range');

        if (!empty($date_range)) {
            $date_range = explode(' ', $date_range['value']);
            $from_date  = date('Y-m-d', strtotime($date_range[0]));
            $to_date    = date('Y-m-d', strtotime($date_range[1]));
        }

        $field_name = in_array('date', $query->getModel()['fillable']) ? 'date' : 'stats_date_tz';

        if ($field_name === 'date') {
            $from_first_day_of_month_date = date('Y-m-01',strtotime($from_date));
            $to_last_day_of_month_date =  date("Y-m-t", strtotime($to_date));

            $date_between = "$field_name BETWEEN '$from_first_day_of_month_date' AND '$to_last_day_of_month_date'";

            return $query->whereRaw($date_between);
        }

            // --- Old Query ---
//        if ($field_name === 'date') {
//
//            $month_range = "MONTH($field_name) BETWEEN MONTH('$from_date') AND MONTH('$to_date')";
//            $year_range = "YEAR($field_name) BETWEEN YEAR('$from_date') AND YEAR('$to_date')";
//            $query->whereRaw($month_range);
//
//            return $query->whereRaw($year_range);
//        }

        return $query->whereBetween($field_name, [$from_date, $to_date]);
    }

    private
    function resolveResultsByDisplayValues($field, $result, $days_passed, $days_of_month)
    {
        $display_is_daily = request()->input('display-values') == 'daily';

        if ($display_is_daily) {
            if ($field === 'advertiser') {
                return $result->$field;
            }

            return round(($result->$field / $days_of_month) * $days_passed);
        }

        return round($result->$field);
    }


    public function getTargetResultRatio($targets, $results)
    {
        $target_result_with_ratio = collect();

        $targets->map(function ($target, $key) use ($results, $target_result_with_ratio) {
            $temp   = collect();
            $fields = ['cost', 'revenue', 'profit', 'conversions', 'cpa','epa', 'roi'];
            array_map(/**
             * @param $field
             */
                function ($field) use ($target, $results, $key, $temp) {
                    $target_field = 't_' . $field;
                    $result_field = 'a_' . $field;

                    $target_advertiser_id = $target['advertiser_id'];
                    $results              = $results->keyBy('advertiser_id');

                    if (\count($results->where('advertiser_id', $target_advertiser_id)->first())) {
                        $ratio         = '';
                        $valid_records = ($results[$target_advertiser_id][$result_field] != 0) && ($target[$target_field] != 0);
                        if (!$valid_records) {
                            $ratio = 'NA';
                        } else {
                            $result                  = ($results[$target_advertiser_id][$result_field] / $target[$target_field]) * 100;
                            $record_smaller_then_one = $result < 1;

                            if ($record_smaller_then_one) {
                                $ratio = round($result, 3);
                            } else {
                                $ratio = round($result);
                            }
                        }
                        $temp->put('advertiser_id', $target['advertiser_id']);
                        if (!empty($ratio)) {
                            $temp->put($field, $ratio);
                        }
                    }
                }, $fields
            );

            if ($temp->count()) {
                $target_result_with_ratio->push($temp);
            }
        });

        return $target_result_with_ratio;
    }

    /**
     *  return query that does not have publisher with the name SEO.
     *
     * @param $query
     *
     * @return mixed
     */
    public function wherePublisherNameNotSEOORNULL($query)
    {
        $query->where('publisher_name', '<>', 'SEO');

        return $query->orWhereNull('publisher_name');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function getResultStatsLevelOneSubQuery()
    {
        /*Inner query*/
        $query = $this->query();
        $query->select(
            'advertiser_id',
            DB::raw('DAY(LAST_DAY(stats_date_tz)) AS days_in_month'),
            DB::raw('DATE_FORMAT(stats_date_tz, "%Y-%m-%d") stats_date_tz'),
            DB::raw('advertiser_name as advertiser'),
            DB::raw('sum(cost) cost'),
            DB::raw('sum(revenue) revenue'),
            DB::raw('SUM(profit) profit'),
            DB::raw('(CASE
             WHEN adv.click_out_conversion=1 THEN SUM(out_clicks)
             WHEN adv.lead_conversion=1 THEN SUM(leads)
             WHEN adv.sale_conversion=1 THEN SUM(sales_count)
             END) AS conversions'),
            DB::raw('publisher_name')
        );
        $query
            ->join('bo.advertisers as adv', 'dash_dailyStats.advertiser_id', 'adv.id');

        $this->whereTargetActualDates($query);
        $this->addGlobalFilters($query);
        $query->groupBy([
            'advertiser',
            'stats_date_tz',
            'adv.sale_conversion',
            'publisher_name',
            'advertiser_id',
            'click_out_conversion'
        ]);
        $query->orderBy('advertiser');

        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function getResultStatsLevelOneSubQueryBySAdvertiser()
    {
        /*Inner query*/
        $query = $this->query();
        $query->select(
            's_advertiser_id AS advertiser_id',
            DB::raw('DAY(LAST_DAY(stats_date_tz)) AS days_in_month'),
            DB::raw('DATE_FORMAT(stats_date_tz, "%Y-%m-%d") stats_date_tz'),
            DB::raw('adv.name AS advertiser'),
            DB::raw('sum(cost) cost'),
            DB::raw('sum(revenue) revenue'),
            DB::raw('SUM(profit) profit'),
            DB::raw('(CASE
             WHEN adv.click_out_conversion=1 THEN SUM(out_clicks)
             WHEN adv.lead_conversion=1 THEN SUM(leads)
             WHEN adv.sale_conversion=1 THEN SUM(sales_count)
             END) AS conversions'),
            DB::raw('publisher_name')
        );
        $query
            ->join('bo.advertisers as adv', 'dash_dailyStats.s_advertiser_id', 'adv.id');

        $this->whereTargetActualDates($query);
        $this->addGlobalFilters($query,1);
        $query->groupBy([
            'advertiser',
            'stats_date_tz',
            'adv.sale_conversion',
            'publisher_name',
            'advertiser_id',
            'click_out_conversion'
        ]);
        $query->orderBy('advertiser');
        return $query;
    }

    private function getChartStatsResultLevelOne($inner_fields)
    {
        /*INNER QUERY*/
        $query = $this->query();
        $query->selectRaw($inner_fields);
        $query->join('bo.advertisers as adv', 'dash_dailyStats.advertiser_id', 'adv.id');
        $query->groupBy(['date', 'sale_conversion', 'profit', 'sales_count', 'leads', 'cost', 'publisher_name']);
        $this->whereDates($query);
        $this->whereAdvertisers($query);

        return $query;
    }
    private function getChartStatsResultLevelOneBySAdvertiser($inner_fields)
    {
        /*INNER QUERY*/
        $query = $this->query();
        $query->selectRaw($inner_fields);
        $query->join('bo.advertisers as adv', 'dash_dailyStats.s_advertiser_id', 'adv.id');
        $query->groupBy(['date', 'sale_conversion', 'profit', 'sales_count', 'leads', 'cost', 'publisher_name']);
        $this->whereDates($query);
        $this->whereSAdvertisers($query);
        $this->whereTrafficSource($query);

        return $query;
    }
}
