<?php


namespace App\Entities\Repositories\Dwh;


use App\Entities\Models\Dwh\DwhFactTracks;
use App\Entities\Repositories\Repository;

class DwhFactTracksRepo extends Repository
{
    const  MODEL = DwhFactTracks::class;

    public function deleteByTokens($tokens){

        return $this->query()
            ->whereIn('id',$tokens)
            ->delete();
    }
}