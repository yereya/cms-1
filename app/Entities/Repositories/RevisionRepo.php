<?php

namespace App\Entities\Repositories;


use Venturecraft\Revisionable\Revision;

/**
 * Class RevisionRepo
 *
 * @package App\Entities\Repositories
 */
class RevisionRepo extends Repository
{
    const MODEL = Revision::class;

    /**
     * @param $model
     * @param $model_id
     * @return mixed
     */
    public function getByModelId($model, $model_id)
    {

        return Revision::where('revisionable_type', $model)
            ->where('revisionable_id', $model_id)
            ->orderBy('updated_at', 'desc')
            ->limit(50)
            ->get();
    }

}