<?php

namespace App\Entities\Repositories;

use App\Console\Commands\CheckSitesPageSpeed;
use App\Entities\Models\Bo\AdvertiserTarget;
use App\Entities\Models\SitesPageSpeedScore;
use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Repository;
use App\Http\Traits\DashboardTrait;
use App\Libraries\Mail;
use DB;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Mockery\Exception;


/**
 * Class TargetsRepo Repository of advertisers targets
 *
 * @property  pageSpeedKey
 * @package App\Entities\Repositories\Bo\Advertisers
 */
class PageSpeedRepo extends Repository
{
    use DashboardTrait;

    /**
     * Model for advertiser target
     */
    const MODEL = SitesPageSpeedScore::class;

    /*
     * Get the scores for today
     */
    /**
     * @return array
     */
    public function getDailyScore()
    {
        $today_scores = $this->getScoresByDay(TRUE);

        return $today_scores;
    }

    /*
     * Get the scores for yesterday
     */
    /**
     * @return array
     */
    public function getYesterdayScore()
    {
        $yesterday_scores = $this->getScoresByDay(FALSE);

        return $yesterday_scores;
    }

    /*
     *  extract the scores from the DB
     */
    /**
     * @param $isToday
     * @return array
     */
    public function getScoresByDay($isToday)
    {
        $where_query = $isToday ? 'DAY(created_at) = DAY(CURRENT_DATE)' : 'DAY(created_at) = DAY(NOW() - INTERVAL 1 DAY)';
        $query = $this->query()
            ->select(
                DB::raw('site_url'),
                DB::raw('desktop_score'),
                DB::raw('mobile_score'),
                DB::raw('created_at')
            )
            ->whereRaw($where_query)
            ->orderBy('created_at');
        $result = $query->get();

        return $result ? $result->toArray() : [];
    }

    /*
     *  combines ala scores in one array
     */

    /**
     * @param $today
     * @param $yesterday
     * @return array
     */
    public function unionScores($today, $yesterday)
    {
        $daysUnion = [];
        foreach ($today as $index => $tod) {
            $url = $tod['site_url'];
            $daysUnion[$url]['desktop_score'] = $tod['desktop_score'];
            $daysUnion[$url]['mobile_score'] = $tod['mobile_score'];
            $daysUnion[$url]['created_at'] = $tod['created_at'];
            $daysUnion[$url]['yesterday_desktop_score'] = "N/A";
            $daysUnion[$url]['yesterday_mobile_score'] = "N/A";
        }
        foreach ($yesterday as $index => $yest) {
            $url = $yest['site_url'];
            if (!isset($daysUnion[$url])) {
                $daysUnion[$url]['desktop_score'] = "N/A";
                $daysUnion[$url]['mobile_score'] = "N/A";
            }
            $daysUnion[$url]['yesterday_desktop_score'] = $yest['desktop_score'];
            $daysUnion[$url]['yesterday_mobile_score'] = $yest['mobile_score'];
        }

        return $daysUnion;
    }

    /*
     *  returns the scores in the rows format for the table
     */
    /**
     * @return array
     */
    public function getRows()
    {
        $today_scores = $this->getDailyScore();
        $yesterday_scores = $this->getYesterdayScore();
        $scores = $this->unionScores($today_scores, $yesterday_scores);
        $rows = [];
        if (empty($scores)) {
            $rows[] = [
                'site_url' => "N/A",
                'desktop_score' => "N/A",
                'mobile_score' => "N/A",
                'yesterday_desktop_score' => "N/A",
                'yesterday_mobile_score' => "N/A",
                'created_at' => "N/A"
            ];
            return $rows;
        }
        $index = 0;
        foreach ($scores as $url => $stats) {

            $rows[$index]['site_url'] = $url;
            $rows[$index]['desktop_score'] = $stats['desktop_score'];
            $rows[$index]['yesterday_desktop_score'] = $stats['yesterday_desktop_score'];
            $rows[$index]['mobile_score'] = $stats['mobile_score'];
            $rows[$index]['yesterday_mobile_score'] = $stats['yesterday_mobile_score'];
            $rows[$index]['created_at'] = $stats['created_at'];
            $index++;
        }
        return $rows;
    }

    /**
     * @param $siteUrl
     * @param bool $is_desktop
     * @param $page_speed_key
     * @return mixed
     *
     * Uses guzzle client to get the score for each site
     * waits 30 second if there is an error
     * trying each link max 3 times before skipping
     */
    public function getScoreForSite($siteUrl, $is_desktop = true, $page_speed_key)
    {
        $page_speed_site_url = $this->getPageSpeedUrl($page_speed_key, $siteUrl, $is_desktop);
        $client = new Client();
        $score = 'N/A';
        for ($i = 0; $i < 3; $i++) {
            try {
                $response = $client->get($page_speed_site_url);
                $page_speed_content_string = $response->getBody()->getContents();
                $page_speed_content = json_decode($page_speed_content_string, true);
                $score = $page_speed_content['lighthouseResult']['categories']['performance']['score'] * 100;
                break 1;
            } catch (Exception $e) {
                sleep(30);
                echo("trying again site " . $siteUrl . PHP_EOL);
            }

        }

        return $score;
    }

    /**
     *  Saves the scores to DB
     *
     * @param $attr Collection
     * @return bool
     */
    public function store($attr): bool
    {
        $model = $this->model();

        return $model->insert($attr->toArray());
    }


    /**
     *  Sets the Url
     *
     * @param $page_speed_key
     * @param $siteUrl
     * @param bool $isDesktop
     * @return string
     */
    private function getPageSpeedUrl($page_speed_key, $siteUrl, $isDesktop = true): string
    {
        $pagespeedUrl = 'https://www.googleapis.com/pagespeedonline/v5/runPagespeed?url=';
        $device = $isDesktop ? 'desktop' : 'mobile';
        $strategy = '&strategy=' . $device;
        $key = '&key=' . $page_speed_key;
        return $pagespeedUrl . $siteUrl . $strategy . $key;
    }

    /**
     * @param $scores
     * @return array
     */
    public function getSitesToReport($scores): array
    {
        $sites_to_report = [];
        $index = 0;
        foreach ($scores as $key => $score) {
            if ($score['desktop_score'] != "N/A" && $score['yesterday_desktop_score'] != "N/A") {
                if ($score['desktop_score'] <= $score['yesterday_desktop_score'] - 10) {
                    $sites_to_report[$index] = $key . " - desktop score dropped today from " .
                        $score['yesterday_desktop_score'] .
                        " to " .
                        $score['desktop_score'] .
                        PHP_EOL;
                }
            }
            if ($score['mobile_score'] != "N/A" && $score['yesterday_mobile_score'] != "N/A") {
                if ($score['mobile_score'] <= $score['yesterday_mobile_score'] - 10) {
                    $sites_to_report[$index] = $key . " - mobile score dropped today from " .
                        $score['yesterday_mobile_score'] .
                        " to " .
                        $score['mobile_score'] .
                        PHP_EOL;
                }
            }
            $index++;
        }

        return $sites_to_report;
    }
}
