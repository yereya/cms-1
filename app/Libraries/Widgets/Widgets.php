<?php
namespace App\Libraries\Widgets;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Entities\Models\Sites\Widgets\SiteWidgetTemplate;
use App\Facades\SiteConnectionLib;
use App\Libraries\Widgets\Contracts\WidgetController;
use Exception;
use Illuminate\Support\Collection;
use View;

abstract class Widgets implements WidgetController
{
    /**
     * @var WidgetData $widget_data
     */
    protected $widget_data;

    /**
     * @var SiteWidget
     */
    protected $widget;

    /**
     * @var SiteContentType $content_type
     */
    protected $content_type;

    /**
     * @var Collection $items
     */
    protected $items;

    /**
     * @var string
     */
    protected $base_view_namespace = 'WidgetsLib';

    /**
     * @var string
     */
    protected $view_base_path;

    /**
     * @var $controller_name
     */
    protected $controller_name;

    /**
     * @var $widget_template
     */
    protected $widget_template;

    /**
     * @var array $widget_data_attributes
     */
    protected $widget_data_attributes;

    /*
     * @var $co
     */
    protected $current_post = null;

    /**
     * @param WidgetData $widget_data
     * @param SiteWidget $widget
     * @param Post       $current_post
     */
    public function __construct(WidgetData $widget_data, SiteWidget $widget, Post $current_post = null)
    {
        $this->current_post           = $current_post;
        $this->widget_data            = $widget_data;
        $this->widget_data_attributes = (array)$widget_data->data;
        $this->widget                 = $widget;

        $site = SiteConnectionLib::getSite();
        $this->view_base_path = config('view.widgets_view_path') . '/' . $site->id;

        $this->initViewNamespace();
        $this->setWidgetTemplate();
    }

    /**
     * Init View Namespace
     *
     * @throws \Exception
     */
    protected function initViewNamespace()
    {
        View::addNamespace($this->widget->view_namespace, $this->view_base_path . '/' . $this->widget->controller);
        View::addNamespace($this->base_view_namespace, __DIR__ . '/Partials');

        $this->widget->templates_namespace = 'Templates';
        View::addNamespace($this->widget->templates_namespace,
            __DIR__ . '/Partials/Templates/' . $this->widget->controller);
    }

    /**
     * setTemplateFilename
     *
     * @throws \Exception
     */
    private function setWidgetTemplate()
    {
        $this->widget_template = SiteWidgetTemplate::find($this->widget_data->widget_template_id);

        if (!$this->widget_template) {
            throw new \Exception('Could not find template id ' . $this->widget_data->widget_template_id);
        }
    }

    /**
     * Get View Path
     *
     * @return string
     */
    protected function getViewPath()
    {
        $this->registerSharedViewParams();
        return $this->widget->view_namespace . '::' . $this->widget_template->file_name;
    }

    /**
     * register Shared View Params
     */
    protected function registerSharedViewParams()
    {
        View::share('widget_data', $this->widget_data);
        View::share('widget_data_attributes', $this->widget_data_attributes);
        View::share('html_attr', $this->getHtmlAttr());
        View::share('widget_template', $this->widget_template);
        View::share('widget', $this->widget);
        View::share('current_post', $this->current_post);
    }

    /**
     * Get Html Attributes
     *
     * @return array
     */
    protected function getHtmlAttr()
    {
        $html_attributes          = [];
        $html_attributes['style'] = $this->buildCssAttributes();

        return $html_attributes;
    }

    /**
     * Build Css Attributes
     *
     * @return string|void
     */
    private function buildCssAttributes()
    {
        if (isset($this->widget_data_attributes['css_attr']) &&
            is_array($this->widget_data_attributes['css_attr']) &&
            count($this->widget_data_attributes['css_attr'])
        ) {
            $css_attr_str = '';
            foreach ($this->widget_data_attributes['css_attr'] as $key => $val) {
                $css_attr_str .= $key . ':' . $val . ';';
            }

            return $css_attr_str;
        }

        return null;
    }

    /**
     * Exception
     *
     * @param      $parameters
     * @param null $message
     *
     * @throws Exception
     */
    protected function controllerException($parameters, $message = null)
    {
        if(env('APP_ENV') == 'local'){
            $message = $message ?? 'Parameter %s not found or null. Controller: %s. WidgetDataId: %s';

            throw new Exception(sprintf($message, $parameters, get_class($this), $this->widget_data->id));
        }else{
            abort('404');
        }
    }
}
