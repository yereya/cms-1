<?php

namespace app\Libraries\Widgets\Helpers;


use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Repositories\Sites\SiteContentTypeFieldRepo;
use Illuminate\Database\Eloquent\Collection;

class DynamicListFilters
{
    protected $widget_data;
    protected $filter_ids;
    protected $order_ids;
    private $filters;
    private $orders;
    private $selectors = ['checkboxes', 'link_content_type'];

    /**
     * DynamicListFilters constructor.
     *
     * @param WidgetData $widget_data
     */
    public function __construct(WidgetData $widget_data)
    {
        $this->widget_data = $widget_data;
        $this->filter_ids  = $this->metadataToArray($widget_data->data, 'fields_filter_ids');
        $this->order_ids   = $this->metadataToArray($widget_data->data, 'fields_order_ids');

        $this->init();
    }

    /**
     * Init main methods
     */
    private function init()
    {
        $this->buildOrders();
        $this->buildFilters();
    }

    /**
     * Per-load a posts collection content type data.
     *
     * @param Collection $posts
     *
     * @return Collection
     */
    protected function preloadPostsContent(Collection $posts): Collection
    {
        if (!$posts->first()){
            return $posts;
        }

        $content_type = $posts->first()->resolveContentType();

        $posts_ids = $posts->pluck('id')->all();

        $posts_content = $content_type->whereIn('post_id', $posts_ids)->get()->keyBy('post_id');

        return $posts->map(function ($post) use ($posts_content) {
            $post_content_type = $posts_content->get($post->id);

            if ($post_content_type){
                $post->preloaded_content = $post_content_type;
            }

            return $post;
        });
    }

    /**
     * Add Filters attributes to posts
     *
     * @param Collection $posts
     *
     * @return void
     */
    public function attachFilterAttributes(Collection $posts)
    {
        $_this   = $this;
        $filters = $this->filters;

        $this->preloadPostsContent($posts)->map(function ($post) use ($filters, $_this) {
            $parameters    = $_this->buildColumnParameters($post->preloaded_content, $filters, $post);
            $post->filters = collect($parameters);
        });
    }

    /**
     * Add Orders attributes to posts
     *
     * @param Collection $posts
     *
     * @return void
     */
    public function attachOrdersAttributes(Collection $posts)
    {
        $orders = $this->orders;
        $_this  = $this;

        $this->preloadPostsContent($posts)->map(function ($post) use ($orders, $_this) {
            $parameters   = $_this->buildColumnParameters($post->preloaded_content, $orders, $post);
            $post->orders = collect($parameters);
        });
    }

    private function buildColumnParameters($content, $groups, $post)
    {
        $parameters = [];
        foreach ($groups as $group) {
            foreach ($group as $item) {
                $parameters[$item['name']] = $this->parseColumnDataToAttribute($content, $item, $post);
            }
        }

        return $parameters;
    }

    /**
     * Parse column with metadata to attributes
     *
     * @param $content
     * @param $search
     * @param $post
     *
     * @return int
     */
    private function parseColumnDataToAttribute($content, $search, $post)
    {
        if (array_key_exists('multi', $search)) {
            return $this->getMulti($content, $search);
        }

        if (array_key_exists('linked', $search)) {
            return $this->getLinked($search, $post);
        }

        return $content->{$search['name']} ?? 0;
    }

    /**
     * Return parsed column linked post
     *
     * @param $search
     * @param $post
     *
     * @return bool
     */
    private function getLinked($search, $post)
    {
        $is_empty = $post->linkedPosts($search['field_id'])->isEmpty();

        return $is_empty ? false : true;
    }

    /**
     * Return parsed column multi select or checkboxes
     *
     * @param $content
     * @param $search
     *
     * @return int
     */
    private function getMulti($content, $search)
    {
        $metadata = json_decode($content->{$search['multi']});

        return $metadata->{$search['original']} ?? 0;
    }

    /**
     * Get Filters
     *
     * @return mixed
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Get Orders
     *
     * @return mixed
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Build new array to orders
     */
    private function buildOrders()
    {
        $fields = $this->getDefaultFields($this->order_ids);

        $this->orders = $this->getConvertedFields($fields);
    }

    /**
     * Build new array to filters
     */
    private function buildFilters()
    {
        $fields = $this->getDefaultFields($this->filter_ids);

        $this->filters = $this->getConvertedFields($fields);
    }

    /**
     * @param array $fields
     *
     * @return array
     */
    private function getConvertedFields(array $fields)
    {
        $new_fields = [];
        foreach ($fields as $group_name => $group) {
            foreach ($group as $field_key => $field) {
                if (in_array($field['type'], $this->selectors)) {
                    $new_fields[$field['display_name']] = $this->buildNewGroup($field, $field['type']);
                } else {
                    $new_fields[$group_name][] = ['name' => $field['name'], 'display_name' => $field['display_name']];
                }
            }
        }

        return $new_fields;
    }

    /**
     * Build params to new group
     *
     * @param array  $field
     * @param string $type
     *
     * @return array
     */
    private function buildNewGroup(array $field, string $type = 'checkboxes')
    {
        $metadata         = json_decode($field['metadata']);
        $new_group_params = [];

        switch ($type) {
            case 'checkboxes':
                return $this->buildGroupCheckboxes($metadata->checkbox, $field);
            case 'content_type_link':
                return $this->buildGroupContentTypeLinked($metadata->content_type_id, $field);
        }

        return $new_group_params;
    }

    /**
     * Build group linked posts
     *
     * @param       $content_type_id
     * @param array $field
     *
     * @return array
     */
    private function buildGroupContentTypeLinked($content_type_id, array $field)
    {
        $new_group_params = [];

        $content_type_content = Post::published()->where('content_type_id', $content_type_id)->get();

        foreach ($content_type_content as $content) {
            $new_group_params[] = [
                'name'         => $content->slug,
                'display_name' => $content->name,
                'linked'       => $content->id,
                'field_id'     => $field['id']
            ];
        }

        return $new_group_params;
    }

    /**
     * Build group checkboxes
     *
     * @param       $checkboxes
     * @param array $field
     *
     * @return array
     */
    private function buildGroupCheckboxes($checkboxes, array $field)
    {
        $new_group_params = [];
        foreach ($checkboxes as $checkbox) {
            $new_group_params[] = [
                'name'         => snake_case($checkbox->key),
                'display_name' => $checkbox->key,
                'multi'        => $field['name'],
                'original'     => $checkbox->key
            ];
        }

        return $new_group_params;
    }

    /**
     * Get default fields
     *
     * @param array $ids
     *
     * @return array
     */
    private function getDefaultFields(array $ids)
    {
        $fields = (new SiteContentTypeFieldRepo())->model()->with('fieldGroup')->whereIn('id', $ids)
            ->get();

        $new_fields = [];

        $fields->map(function ($field) use (&$new_fields) {
            $group_name                = $field->fieldGroup->name ?? 'none_name';
            $new_fields[$group_name][] = [
                'id'           => $field->id,
                'name'         => $field->name,
                'display_name' => $field->display_name,
                'type'         => $field->type,
                'metadata'     => $field->metadata
            ];
        });

        return $new_fields;
    }

    /**
     * Metadata to array
     * in - [{\"id\":273},{\"id\":281},{\"id\":515}]
     * out - [273, 281, 515]
     *
     * @param $metadata
     * @param $attribute
     *
     * @return array
     */
    private function metadataToArray($metadata, $attribute)
    {
        $parsed = [];

        //if attribute exists
        if (!isset($metadata->{$attribute})) {
            return $parsed;
        }

        $to_array = json_decode($metadata->{$attribute});

        // if attribute not null and array
        if (!is_array($to_array) || empty($to_array)) {
            return $parsed;
        }

        foreach ($to_array as $item) {
            array_push($parsed, $item->widget_data_id);
        }

        return $parsed;
    }
}