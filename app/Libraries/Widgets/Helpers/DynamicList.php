<?php

namespace App\Libraries\Widgets\Helpers;


use App\Entities\Models\Sites\ContentType;
use App\Entities\Models\Sites\DynamicLists\DynamicList as ModelDynamicList;
use App\Entities\Models\Sites\Post;
use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\SiteContentTypeFieldRepo;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class DynamicList
{
    /**
     * Default size parameters
     */
    const DEFAULT_OFFSET = 0;
    const DEFAULT_LIMIT = 10;

    /**
     * Global vars
     *
     * @var mixed
     */
    protected $content_type_id_id;
    protected $is_static;
    protected $labels;
    protected $order_fields;
    protected $filter_fields;
    protected $dynamic_list;
    protected $offset;
    protected $limit;
    protected $included_ids;
    protected $not_included_ids;

    /**
     * DynamicList constructor.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->dynamic_list = !empty($parameters['widget_data_id'])
            ? ModelDynamicList::find($parameters['widget_data_id'])
            : null;
        $this->content_type_id = $parameters['content_type_id'];
        $this->is_static = $this->toBoolean($parameters['is_static_list'] ?? false);
        $this->order_fields = $this->orderFields($parameters['orders'] ?? []);
        $this->filter_fields = $this->filterFields($parameters['filters'] ?? []);
        $this->labels = $this->parseLabels($parameters['labels'] ?? []);
        $this->offset = $parameters['query_offset'] ?? self::DEFAULT_OFFSET;
        $this->limit = $parameters['query_limit'] ?? self::DEFAULT_LIMIT;
    }

    /**
     * Get Enabled posts
     *
     * @return mixed
     */
    public function getBasePosts() {
        return $this->getIncludedPosts(true);
    }

    /**
     * Get Disabled posts
     *
     * @return array
     */
    public function getDisabledPosts() {
        if ($this->is_static) {
            return Post::published()
                ->where('content_type_id', $this->content_type_id)
                ->whereNotIn('id', $this->included_ids)
                ->get();
        } else {
            $this->getNotIncludedPosts();
        }
    }

    /**
     * Get included posts
     *
     * @param bool $base
     *
     * @return mixed
     */
    public function getIncludedPosts($base = false) {
        return $this->is_static
            ? $this->getIncludedStaticPosts($base)
            : $this->getIncludedDynamicPosts($base);
    }

    /**
     * Get not included posts
     *
     * @return array
     */
    public function getNotIncludedPosts() {
        return $this->is_static
            ? $this->getNotIncludedStaticPosts()
            : $this->getNotIncludedDynamicPosts();
    }

    /**
     * $is_static_list - true is manually (or static), false is auto (or dynamic)
     *
     * @param $parameter
     *
     * @return bool
     */
    private function toBoolean($parameter) {
        if ($parameter) {
            return $parameter == 'manual';
        }

        return false;
    }

    /**
     * Get not included static posts - order manually
     *
     * @return array
     */
    private function getNotIncludedStaticPosts() {
        /* if dynamic list exists find saved posts */
        if ($this->dynamic_list && $this->is_static) {
            if (!$this->not_included_ids) {
                $this->getPostsByQueryParams();
            }

            return Post::published()
                ->where('content_type_id', $this->content_type_id)
                ->whereIn('id', $this->not_included_ids)
                ->get();
        }

        /* if not exists get posts by query params */
        return $this->getNotIncludedDynamicPosts();
    }

    /**
     * Get included static posts
     *
     * @param bool $base
     *
     * @return mixed
     */
    private function getIncludedStaticPosts($base = false) {
        /* if dynamic list exists find saved posts */
        if ($this->dynamic_list && $this->is_static) {
            $posts = $this->dynamic_list->posts;
            $this->updateNotIncludePosts($posts, $base);

            return $base
                ? $posts
                : $posts->slice($this->offset, $this->limit);
        }

        /* if not exists get posts by query params */
        return $this->getIncludedDynamicPosts($base);
    }

    /**
     * Get not included dynamic posts - order auto
     *
     * @return array
     */
    private function getNotIncludedDynamicPosts() {
        if (!$this->not_included_ids) {
            $this->getPostsByQueryParams();
        }

        return Post::published()
            ->whereIn('id', $this->not_included_ids)
            ->where('content_type_id', $this->content_type_id)
            ->get();
    }

    /**
     * Get included dynamic posts - order auto
     *
     * @param bool $base
     *
     * @return mixed
     */
    private function getIncludedDynamicPosts($base = false) {
        return $this->included_ids = $this->getPostsByQueryParams($base);
    }

    /**
     * Return posts
     *
     * @param bool $base
     *
     * @return mixed
     */
    public function getPostsByQueryParams($base = false) {
        return $this->sortByQueryParams($this->getPosts(), $base);
    }

    /**
     * Get all published post by content type
     */
    private function getPosts() {
        return Post::published()
            ->where('content_type_id', $this->content_type_id)
            ->get();
    }

    /**
     * Sort posts by query params
     *
     * @param Collection $posts
     * @param bool       $base
     *
     * @return mixed
     */
    public function sortByQueryParams(Collection $posts, $base = false) {
        $_this = $this;
        $content = new Collection();

        /* insert only content types to content collection */
        $posts->map(function ($item) use ($content, $_this) {
            if ($_this->inLabels($item->labels)) {
                $content->push($item->content);
            }
        });

        /* filter operations */
        if ($filter_fields = $this->filter_fields) {
            $content = $content->filter(function ($item) use ($filter_fields, $_this) {
                return $_this->filterContent($item, $filter_fields) ? $item : null;
            });
        }

        /* order operations */
        foreach ($this->order_fields as $order) {
            if ($order['value'] == 'ASC') {
                $content = $content->sortBy($order['name']);
            } else {
                $content = $content->sortByDesc($order['name']);
            }
        }

        /* get post ids */
        $post_ids = $content->pluck('post_id')->toArray();

        /* get only needle posts by order */
        /* we need use EloquentCollection and not Collection */
        $results = new \Illuminate\Database\Eloquent\Collection();
        $posts_by_id = $posts->keyBy('id');
        foreach ($post_ids as $id) {
            $results->push($posts_by_id->get($id));
        }

        $this->updateNotIncludePosts($results, $base);

        /* offset and limit */
        return $base
            ? $results
            : $results->slice($this->offset, $this->limit);
    }

    /**
     * Update not included posts
     *
     * @param $posts
     * @param bool $base
     */
    private function updateNotIncludePosts($posts, $base = false) {
        if ($base) {
            $this->included_ids = $posts->pluck('id')->toArray();
        } else {
            $included_ids = $posts->slice($this->offset, $this->limit)->pluck('id');
            $all_ids = $posts->pluck('id');
            $not_included = $all_ids->diff($included_ids);

            $this->not_included_ids = $not_included->all();
            $this->included_ids = $included_ids->toArray();
        }
    }

    /**
     * Method helper
     *
     * @param ContentType $content_type
     * @param array       $filters
     *
     * @return bool
     */
    private function filterContent(ContentType $content_type, array $filters) {
        $count = 0;
        foreach ($filters as $filter) {
            switch ($filter['operator']) {
                case '=':
                    if ($content_type->{$filter['name']} == $filter['value']) {
                        $count++;
                    }
                    break;
                case '<':
                    if ($content_type->{$filter['name']} < $filter['value']) {
                        $count++;
                    }
                    break;
                case '>':
                    if ($content_type->{$filter['name']} > $filter['value']) {
                        $count++;
                    }
                    break;
            }
        }

        return $count == count($filters);
    }

    /**
     * Helper methods - create fields to search (to query)
     *
     * @param array $filter_fields
     *
     * @return array
     */
    private function filterFields(array $filter_fields) {
        $filters = [];
        $is_parsed = false;

        foreach ($filter_fields as $filter) {
            if (isset($filter['name']) &&
                isset($filter['value']) &&
                isset($filter['operator']))
            {
                $is_parsed = true;
            }
        }

        if ($is_parsed) {
            return $filter_fields;
        }

        if (isset($filter_fields['name']) &&
            isset($filter_fields['value']) &&
            isset($filter_fields['operator']))
        {
            return $filter_fields;
        }

        if (!isset($filter_fields['field_id']) ||
            !isset($filter_fields['operator']) ||
            !isset($filter_fields['value']) ||
            count($filter_fields['field_id']) != count($filter_fields['operator']) ||
            count($filter_fields['field_id']) != count($filter_fields['value']))
        {
            return $filters;
        }

        for ($i = 0; $i < count($filter_fields['field_id']); $i++) {
            $filters[] = [
                'name' => (new SiteContentTypeFieldRepo)->find($filter_fields['field_id'][$i])->name,
                'operator' => $filter_fields['operator'][$i],
                'value' => $filter_fields['value'][$i]
            ];
        }

        return $filters;
    }

    /**
     * Helper method - to create order list (to query)
     *
     * @param array $order_fields
     *
     * @return array
     */
    private function orderFields(array $order_fields) {
        $orders = [];
        $is_parsed = false;

        foreach ($order_fields as $order) {
            if (isset($order['name']) && isset($order['value'])) {
                $is_parsed = true;
            }
        }

        if ($is_parsed) {
            return $order_fields;
        }

        if (!isset($order_fields['field_id']) ||
            !isset($order_fields['direction']) ||
            count($order_fields['field_id']) != count($order_fields['direction']))
        {
            return $orders;
        }

        for ($i = 0; $i < count($order_fields['field_id']); $i++) {
            $orders[$i] = [
                'name' => (new SiteContentTypeFieldRepo)->find($order_fields['field_id'][$i])->name,
                'value' => $order_fields['direction'][$i] == 'ascending' ? 'ASC' : 'DESC'
            ];
        }

        return $orders;
    }

    /**
     * Check if post exists in label filters
     * if one label post in labels filter array return true
     *
     * param $this->labels - dynamic list labels
     * @param $labels - current post labels
     *
     * @return bool
     */
    private function inLabels($labels) {
        if (count($this->labels) == 0) {
            return true;
        }

        $labels_id = $labels->pluck('id')->toArray();

        $into = true;

        foreach ($this->labels as $label_id) {
            if (!in_array($label_id, $labels_id)) {
                $into = false;
            }
        }

        return $into;
    }

    /**
     * Parse Labels
     *
     * @param array $labels
     *
     * @return array|mixed
     */
    private function parseLabels(array $labels) {
        if (isset($labels['id'])) {
            return $labels['id'];
        }

        return $labels;
    }
}