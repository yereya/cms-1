<?php
namespace App\Libraries\Widgets;

use App\Entities\Models\Sites\Widgets\SiteWidget;
use Illuminate\Http\Request;

class WidgetForm
{

    /**
     * getFormSavedFields

*
*@param SiteWidget    $widget
     * @param Request $request
     *
     * @return mixed
     */
    public static function getFormSavedFields(SiteWidget $widget, Request $request)
    {
        $form_data_attr = $request->only($widget->fields->pluck('name')->toArray());

        $form_data_attr = array_merge($form_data_attr, [
            'name'               => $request->get('name'),
            'active'             => $request->get('active'),
            'class'              => $request->get('class'),
            'css_attributes'     => $request->get('css_attributes'),
        ]);

        $form_data_attr = self::clearEmptyDataFields($form_data_attr);

        self::validateFormFields($form_data_attr);

        return $form_data_attr;
    }

    /**
     * Clear Empty Data Fields
     *
     * @param array $form_data_attr
     *
     * @return array
     */
    private static function clearEmptyDataFields($form_data_attr)
    {
        // Clears all css attributes
        if (isset($form_data_attr['css_attributes'])) {
            $form_data_attr['css_attributes'] = array_filter($form_data_attr['css_attributes'], function ($item, $key) {
                return $item !== "" && $item !== "_unset";
            }, ARRAY_FILTER_USE_BOTH);
        }

        return $form_data_attr;
    }

    /**
     * validateFormFields
     *
     * @param $form_data_attr
     *
     * @return bool
     */
    private static function validateFormFields($form_data_attr)
    {
        // TODO
        return true;
    }


}