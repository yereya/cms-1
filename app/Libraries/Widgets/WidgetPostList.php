<?php

namespace App\Libraries\Widgets;

use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\WidgetDataPost;
use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\RibbonRepo;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Request;

class WidgetPostList
{
    const DEFAULT_OFFSET = 0;
    const DEFAULT_LIMIT = 10;

    public static $operators = ['=', '<', '>'];
    private $post_ids = null;
    private $content_type_id;
    private $widget_data;
    private $posts_by_order = [];
    private $offset;
    private $limit;
    private $orders = [];
    private $wheres = [];
    private $labels = [];
    private $ribbons = [];

    /**
     * @param WidgetData $widget_data
     * @param Request    $request
     *
     * @return $this
     */
    public function init(Request $request, WidgetData $widget_data = null)
    {
        $this->setPostIds($request);

        $this->widget_data = $widget_data;

        $this->setContentTypeId($request->get('content_type_id'));
        $this->setOffset($request->get('posts_offset'));
        $this->setLimit($request->get('posts_limit'));
        $this->setOrders($request->get('order'));
        $this->setWheres($request->get('where'));
        $this->setLabels($request->get('labels'));

        if ($this->widget_data) {
            $this->setRibbons($request->get('ribbon'));
        }

        return $this;
    }

    /**
     * Set Post Ids
     *
     * @param $request
     */
    public function setPostIds($request)
    {
        $post_ids = null;
        if ($request->has('post_ids_nestable')) {
            $post_ids = is_string($request->get('post_ids_nestable'))
                ? json_decode($request->get('post_ids_nestable'))
                : $request->get('post_ids_nestable');
        } elseif ($request->has('post_ids')) {
            $post_ids = is_string($request->get('post_ids'))
                ? json_decode($request->get('post_ids'))
                : $request->get('post_ids');
        }

        if (is_array($post_ids) && count($post_ids) > 0) {
            $clear = [];
            foreach ($post_ids as $key => $value) {
                $clear[$key] = is_object($value) ? $value->id : $value;
            }
            $post_ids = $clear;
        }

        $this->post_ids = $post_ids;
    }

    /**
     * Save
     *
     * @return $this
     * @throws \Exception
     */
    public function save()
    {
        $this->buildPostOrders();
        $this->savePostOrders();
        $this->saveRibbons();

        return $this;
    }

    /**
     * Build array post orders
     */
    private function buildPostOrders()
    {
        if (is_null($this->post_ids)) {
            return;
        }

        if (!is_array($this->post_ids)) {
            $this->post_ids = [$this->post_ids];
        }

        foreach ($this->post_ids as $key => $id) {
            $this->posts_by_order[] = [
                'widget_data_id'  => $this->widget_data->id,
                'content_type_id' => $this->getContentTypeId(),
                'post_id'         => $id,
                'order'           => ($key + 1) * 10
            ];
        }
    }

    /**
     * Return array ids
     *
     * @param $posts
     *
     * @return mixed
     */
    public function getIdsOnly($posts)
    {
        $ids = [];

        foreach ($posts as $post) {
            $ids[] = $post->id;
        }

        return $ids;
    }

    /**
     * Find Posts to Dynamic list
     *
     * @return Collection
     */
    public function findPosts()
    {
        // if posts saved manually find it
        $widget_data_posts = $this->get();
        if ($widget_data_posts && !$widget_data_posts->isEmpty()) {
            $post_ids = $widget_data_posts->pluck('post_id')->toArray();
            $post_repo = new PostRepo();

            return $post_repo->getPublishedByIds($post_ids);
        }

        $builder = Post::where('content_type_id', $this->getContentTypeId())
            ->whereNotNull('published_at')
            ->whereDate('published_at', '<=', Carbon::now());

        foreach ($this->getOrders() as $order) {
            $builder->orderBy($order);
        }

        foreach ($this->getWheres() as $where) {
            $builder->where($where['name'], $where['operator'], $where['value']);
        }

        if ($this->getLabels()) {
            $labels = $this->getLabels();
            $builder->whereHas('labels', function ($query) use ($labels) {
                $query->whereIn('label_id', array_values($labels));
            });
        }

        $builder->offset($this->getOffset());
        $builder->limit($this->getLimit());

        return $builder->get();
    }

    /**
     * Get orders
     *
     * @return mixed
     */
    public function get()
    {
        return isset($this->widget_data)
            ? WidgetDataPost::where('widget_data_id', $this->widget_data->id)
                ->orderBy('order')
                ->get()
            : null;
    }

    public function getContentTypeId()
    {
        return $this->content_type_id;
    }

    /*
     * Get Content Type Id
     */

    /**
     * Set Content Type Id
     *
     * @param $content_type_id
     */
    public function setContentTypeId($content_type_id)
    {
        $this->content_type_id = $content_type_id;
    }

    /**
     * Get Orders
     *
     * @return array
     */
    public function getOrders()
    {
        return $this->orders;
    }

    /**
     * Set Orders
     *
     * @param $orders
     */
    public function setOrders($orders)
    {
        if ($orders) {
            $this->orders = SiteContentTypeField::whereIn('id', $orders)
                ->get()
                ->pluck('name')
                ->toArray();
        }
    }

    /**
     * Get Wheres
     *
     * @return array
     */
    public function getWheres()
    {
        return $this->wheres;
    }

    /**
     * Set Wheres
     *
     * @param $wheres
     */
    public function setWheres($wheres)
    {
        if ($wheres) {
            foreach ($wheres as $where) {
                if (is_array($where) && count($where) == 3) {
                    $field = SiteContentTypeField::find($where['key']);

                    $this->wheres[] = [
                        'name'     => $field->name,
                        'operator' => $where['operator'],
                        'value'    => $where['value']
                    ];
                }
            }
        }
    }

    /**
     * Get Labels
     *
     * @return array
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * Set Labels
     *
     * @param $labels
     */
    public function setLabels($labels)
    {
        $this->labels = $labels;
    }

    /**
     * Get Offset
     *
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Set Offset
     *
     * @param $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset ?? self::DEFAULT_OFFSET;
    }

    /**
     * Get Limit
     *
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set Limit
     *
     * @param $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit ?? self::DEFAULT_LIMIT;
    }

    /**
     * Save post orders
     */
    private function savePostOrders()
    {
        if (count($this->posts_by_order) > 0) {
            WidgetDataPost::insert($this->posts_by_order);
        }
    }

    /**
     * Save ribbons per post
     */
    private function saveRibbons()
    {
        (new RibbonRepo())->saveRibbonsPerPost($this->getRibbons());
    }

    /**
     * Get Ribbons
     */
    public function getRibbons()
    {
        return $this->ribbons;
    }

    /**
     * Set Ribbons
     *
     * @param $ribbons
     */
    public function setRibbons($ribbons)
    {
        if ($ribbons) {
            foreach ($ribbons as $key => $value) {
                $this->ribbons[] = [
                    'post_id'         => $value,
                    'ribbon_id'       => str_replace('_', '', $key),
                    'dynamic_list_id' => $this->widget_data->id
                ];
            }
        }
    }

    /**
     * Delete
     *
     * @return $this
     */
    public function delete()
    {
        (new RibbonRepo())->forgetByDynamicList($this->widget_data->id);
        WidgetDataPost::where('widget_data_id', $this->widget_data->id)->delete();

        return $this;
    }
}