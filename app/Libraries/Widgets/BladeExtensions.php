<?php

namespace App\Libraries\Widgets;


use Illuminate\Support\Facades\Blade;

class BladeExtensions
{
    /**
     * Add All
     */
    public function register()
    {
        $this->addIncludeDirective('tagsButton', 'WidgetsLib::Assets.tags-button');
        $this->addIncludeDirective('bonusButton', 'WidgetsLib::Assets.bonus-button');
        $this->addIncludeDirective('carouselSlick', 'WidgetsLib::Assets.carousel-slick');
        $this->addIncludeDirective('MobileExpendButton', 'WidgetsLib::Assets.mobile-expend-button');
        $this->addIncludeDirective('picture', 'WidgetsLib::Assets.picture');
        $this->addIncludeDirective('lightBox','WidgetsLib::Assets.light-box');
        $this->addIncludeDirective('captchaButton','WidgetsLib::Assets.captcha-button');
        $this->addIncludeDirective('socialShare','WidgetsLib::Assets.social-share');
    }

    /**
     * Add Include Directive
     *
     * @param $directive_name
     * @param $view
     */
    protected function addIncludeDirective($directive_name, $view)
    {
        Blade::directive($directive_name, function ($expression) use ($directive_name, $view) {
            $expression = $this->stripParentheses($expression);
            $expression = "'$view' , " . $expression;

            return "<?php echo \$__env->make($expression, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>";

        });

    }

    /**
     * Strip Parentheses
     *
     * @param $expression
     * @return string
     */
    protected function stripParentheses($expression)
    {
        if (starts_with($expression, '(')) {
            $expression = substr($expression, 1, -1);
        }

        return $expression;
    }

}