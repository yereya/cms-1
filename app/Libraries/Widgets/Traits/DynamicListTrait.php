<?php

namespace App\Libraries\Widgets\Traits;

use App\Entities\Models\Sites\DynamicLists\DynamicList;
use App\Libraries\Widgets\WidgetPostList;

trait DynamicListTrait
{
    /**
     * @param DynamicList $dynamic_list
     *
     * @return array
     */
    private function buildDynamicListParams(DynamicList $dynamic_list) {
        return [
            'widget_data_id' => $dynamic_list->id,
            'content_type_id' => $dynamic_list->content_type_id,
            'is_static_list' => $dynamic_list->is_static_list ? 'manual' : 'auto',
            'query_offset' => $dynamic_list->query_offset,
            'query_limit' => $dynamic_list->query_limit,
            'labels' => $this->parseLabels($dynamic_list),
            'orders' => $this->orderFields($dynamic_list),
            'filters' => $this->filterFields($dynamic_list),
        ];
    }

    /**
     * Parse Order
     *
     * @param DynamicList $dynamic_list
     *
     * @return array
     */
    private function orderFields(DynamicList $dynamic_list) {
        $orders = $dynamic_list->orders;
        $orders_to_array = [];

        foreach ($orders as $order) {
            $orders_to_array[] = [
                'name' => $order->name,
                'value' => $order->pivot_direction == 'ascending' ? 'ASC' : 'DESC'
            ];
        }

        return $orders_to_array;
    }

    /**
     * Parse Filters
     *
     * @param DynamicList $dynamic_list
     *
     * @return array
     */
    private function filterFields(DynamicList $dynamic_list) {
        $filters = $dynamic_list->filters;
        $filters_to_array = [];

        foreach ($filters as $filter) {
            $filters_to_array[] = [
                'name' => $filter->name,
                'operator' => $this->getOperator($filter->pivot->operator),
                'value' => $filter->pivot->value
            ];
        }

        return $filters_to_array;
    }

    /**
     * Get Operator
     *
     * @param $operator
     *
     * @return
     */
    private function getOperator($operator) {
        return WidgetPostList::$operators[$operator];
    }

    /**
     * Parse Labels
     *
     * @param $dynamic_list
     *
     * @return array
     */
    private function parseLabels($dynamic_list) {
        $labels = $dynamic_list->labels;
        if ($labels) {
            return $labels->pluck('id')->toArray();
        }

        return [];
    }
}