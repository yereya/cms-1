<?php namespace App\Libraries\Widgets;

use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Libraries\Widgets\Contracts\WidgetController as WidgetControllerContract;
use App\Libraries\Widgets\Exceptions\WidgetControllerInvalidDataException;
use App\Libraries\Widgets\Exceptions\WidgetControllerNotExistsException;

/***
 * Class Builder
 *
 * @package App\Libraries\WidgetsBuilder
 */
class WidgetBuilder
{
    /**
     * @var string $namespace
     */
    protected $namespace = '\App\Libraries\Widgets\Controllers\\';

    protected $current_post;

    /**
     * Builder constructor.
     *
     * @param WidgetData $widget_data
     * @param SiteWidget $widget
     * @param Post       $current_post
     */
    public function __construct(WidgetData $widget_data, SiteWidget $widget, Post $current_post = null)
    {
        $this->widget       = $widget;
        $this->widget_data  = $widget_data;
        $this->current_post = $current_post;

        $this->validate();
    }

    /**
     * Validate
     *
     * @throws WidgetControllerNotExistsException
     */
    private function validate()
    {
        if (!class_exists($this->namespace . $this->widget->controller)) {
            throw new WidgetControllerNotExistsException("the controller {$this->widget->controller} for {$this->widget->name} does not exists");
        }
    }

    /**
     * Get
     *
     * @return array
     */
    public function get()
    {
        return [
            'hash' => $this->getHash(),
            'html' => $this->getHtml()
        ];
    }

    /**
     * Get Hash
     *
     * @return string
     */
    private function getHash()
    {
        return md5($this->widget->id . '-' . serialize($this->widget_data));
    }

    /**
     * Get Html
     *
     * @return string
     * @throws WidgetControllerNotExistsException
     */
    public function getHtml()
    {
        return $this->getResultByController($this->getController());
    }

    /**
     * Get Result By Controller
     *
     * @param WidgetControllerContract $controller
     *
     * @return mixed
     * @throws WidgetControllerInvalidDataException
     */
    private function getResultByController(WidgetControllerContract $controller)
    {
        if ($controller->verify()) {
            $controller->build();

            return $controller->get();
        } else {
            throw new WidgetControllerInvalidDataException("The data used for {$this->widget->name} controller {$this->widget->controller} is invalid");
        }
    }

    /**
     * Get Controller
     *
     * @return WidgetControllerContract
     */
    private function getController()
    {
        $controller = $this->namespace . $this->widget->controller;

        return new $controller($this->widget_data, $this->widget, $this->current_post);
    }
}