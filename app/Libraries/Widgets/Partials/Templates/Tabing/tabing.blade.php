@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'open'])

@if(empty($tabs) || count($tabs) == 0)

@else
    <div class="tabbing">
        <div class="row types-row">
            @foreach($tabs as $tab)
                <div class="col-md-4"
                     data-tab="{{ $tab['tab_name'] }}"
                     data-widget_data_id="{{ $tab['widget_data_id'] }}"
                     data-widget_default="{{ $tab['default'] ?? 0 }}">
                    <h2 class="game-type">{{ $tab['tab_name'] }}</h2>
                </div>
            @endforeach
        </div>

        <div class="row types-row">
            @foreach($tabs as $tab)
                <div class="{{ $tab['default'] ? '' : 'hidden' }}"
                     data-widget_data_id="{{ $tab['widget_data_id'] }}"
                     data-tab_content="{{ $tab['tab_name'] }}">
                    {!! $tab['html']['html'] !!}
                </div>
            @endforeach
        </div>
    </div>
@endif

@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'close'])