@if(!$post || !$post_fields)
    <h2>The required posts and fields were not passed for this widget template</h2>
@else
    @foreach($post_fields as $post_field)
        @if(isset($post->content->{$post_field}))
            <div class="row">
                <div class="image-wrapper col-md-3">
                    <img class="post-thumbnail img-responsive" src="{{ $post->content->image_slider_lp }}"
                         alt="featured image">
                </div>
                <div class="mini-content-wrapper col-md-9">
                    <h2 class="title">{{$post->name}}</h2>
                    <span> {{$post->content->published_at}} | <i
                                class="post-icon icon-article-like"></i> {{ $post->likesCount() }} |
                    <i class="post-icon icon-article-comment"></i>
                        {{ $post->commentsCount() }}
                        | by {{ isset($post->content->author->first_name) && isset($post->content->author->last_name)? $post->content->author->first_name . ' ' . $post->content->author->last_name[0] : '' }}
                </span>
                    {!! substr($post->content->{$post_field},0,100) !!}
                    <a class="post-more-info" href="#" role="button" aria-expanded="false">More info</a>
                </div>
            </div>
        @endif
    @endforeach
@endif