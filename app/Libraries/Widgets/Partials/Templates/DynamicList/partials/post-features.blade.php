<div class="row">
    <div class="col-md-12">
        <div class="row logo-wrapper">
            <div class="col-md-12">
                <span class="brand-type pull-left">
                    <p>CASINO</p>
                </span>
                <img class="post-logo" src="{{ fullImagePathById($post->content->image_logo ?? 0) }}"
                     alt="No logo found">
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="col-md-12">
                <div class="features-content">
                    <div>Top 5 Exclusive Bonus</div>
                    <h5>200% UP to 400$</h5>
                    <div>Use Code: TOP5</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 bonus-wrapper">
                @bonusButton([
                    'class' => 'bonus-button green',
                    'content' => 'PLAY GAME'
                ])
            </div>
        </div>
    </div>
</div>