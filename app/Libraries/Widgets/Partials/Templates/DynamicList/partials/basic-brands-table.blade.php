<div class="table-row">
    <div class="cell logo">
        <div class="before iterator"></div>
        <a href="#">
            <img src="{{ isset($posts[$i]->content->image_logo) && $posts[$i]->content->image_logo ? fullImagePathById($posts[$i]->content->image_logo) : ''}}"
                 alt="brand logo">
        </a>
    </div>

    <div class="cell bonus">
        {!! isset($posts[$i]->content->bonus_text) ? $posts[$i]->content->bonus_text : '' !!}
        @include('WidgetsLib::Assets.tooltip', [
            'content' => $posts[$i]->content->tooltip_text,
            'html_id' => 'tooltip-more-info'
        ])
        <a class="tooltipster" data-tooltip-content="#tooltip-more-info"
           data-tooltipster='{"side":"bottom","animation":"slide", "theme": "tooltipster-custom"}'>More
            Info</a>
    </div>

    <div class="cell features">
        <ul class="list">
            @if (isset($posts[$i]->content->features_live_dealer))
                <li data-toggle="Vip program" title="Live Dealer">
                    <i class="icon-live-dealer"></i>
                </li>
            @endif

            @if (isset($posts[$i]->content->features_vip_program))
                <li data-toggle="Vip program" title="Vip program">
                    <i class="icon-vip-program"></i>
                </li>
            @endif

            @if (isset($posts[$i]->content->features_mobile_compatible))
                <li data-toggle="tooltip" title="Mobile Compatible">
                    <i class="icon-mobile-compatible"></i>
                </li>
            @endif

            @if (isset($posts[$i]->content->features_downloadable_software))
                <li data-toggle="tooltip" title="Downloadably Software">
                    <i class="icon-bingo-downloadable"></i>
                </li>
            @endif

            @if (isset($posts[$i]->content->features_instant_play))
                <li data-toggle="tooltip" title="Instant Play">
                    <i class="icon-bingo-instant-play"></i>
                </li>
            @endif

            @if (isset($posts[$i]->content->features_play_with_paypal))
                <li data-toggle="tooltip" title="Paypal">
                    <i class="icon-play-witch-paypal"></i>
                </li>
            @endif
        </ul>
    </div>

    <div class="cell payout">
        <p>{{ isset($posts[$i]->content->ecogra_all_games) ? $posts[$i]->content->ecogra_all_games : ''}}%</p>
    </div>

    <div class="cell rating">
        <div class="score hidden">(929)</div>
        <div class="rate-it">
            <i class="stars hidden-md hidden-sm"
               data-rate_amount="{{ isset($posts[$i]->content->rating_overall_score) ? $posts[$i]->content->rating_overall_score : ''}}"></i>
            <a class="review-link" href="/casino/{{ $posts[$i]->slug }}">Read Review</a>
        </div>
    </div>

    <div class="cell get-bonus">
        <a href="{{ isset($posts[$i]->content->outlink) ? $posts[$i]->content->outlink : '#'}}">
            <span class="get-bonus-top-line">Get</span>
            <span class="get-bonus-bottom-line">Bonus</span>
        </a>
    </div>
</div>