<div class="row top-row">
    <div class="col-md-6">
        <img src="{{ fullImagePathById($post->content->image_logo_review) }}" alt="brand logo">
    </div>

    <div class="col-md-6">
        @include('WidgetsLib::Assets.bonus-button', [
                'class'   => 'bonus-button green pull-right',
                'content' => 'Play',
                'outlink' => $post->content->outlink
            ])
    </div>
</div>

<div class="row bottom-row">
    <span>{!! $post->content->bonus_banner_text ?? '' !!}</span>
</div>