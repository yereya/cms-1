<div class="row top-row">
    <div class="col-md-6">
        <img src="{{ fullImagePathById($post->content->image_logo_review) }}" alt="brand logo">
    </div>

    <div class="col-md-6">
        @include('WidgetsLib::Assets.bonus-button', [
                'class' => 'bonus-button pull-right',
                'content' => 'VISIT SITE',
                'outlink' => $post->content->outlink
            ])
    </div>
</div>

<div class="row bottom-row">
    <div class="col-md-6">
        <div class="rate-it">
            <i class="stars"
               data-rate_amount="{{ $post->content->rating_overall_score ?? '' }}"></i>
        </div>
    </div>

    <div class="col-md-6">
        <a class="pull-right" href="/casino/{{ $post->slug }}">Read Review</a>
    </div>
</div>