@if(!$post || !$post_fields)
    <h2>No post or post fields were found</h2>
@else
    <div class="row archive-wrapper">
        <div class="col-md-12">
            <div class="row top-layer">
                <div class="col-md-2 logo-separator">
                    <img class="img-responsive logo" src="{{ fullImagePathById($post->content->image_logo) }}">
                    <div class="stars star-wrapper">
                        <form action="">
                            <fieldset class="rating-group">
                                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-5" name="rating" value="5"/><label for="star-5"></label>
                                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-4" name="rating" value="4"/><label for="star-4"></label>
                                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-3" name="rating" value="3"/><label for="star-3"></label>
                                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-2" name="rating" value="2"/><label for="star-2"></label>
                                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-1" name="rating" value="1"/><label for="star-1"></label>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <div class="col-md-10">
                    @foreach($post_fields as $post_field)
                        <h2>{{ $post->name }}</h2>
                        <p class="post-paragraph">{!! limitWordsByCharCount($post->content->{$post_field},300) !!}</p>
                        <a class="review" href="#" role="button">Read Review</a>
                    @endforeach
                </div>
            </div>
            <hr>
            <div class="row bottom-layer">
                <span class="exclusive-bonus">{!! $post->content->bonus_banner_text !!}</span>
                <span class="exclusive-bonus">Top 5 Exclusive Bonus   <strong>200% UP to 400$</strong>   Use Code : TOP5</span>
                @bonusButton([
                    'class' => 'bonus-button green pull-right',
                    'content' => 'GET YOUR BONUS NOW'
                ])
            </div>
        </div>
    </div>
@endif