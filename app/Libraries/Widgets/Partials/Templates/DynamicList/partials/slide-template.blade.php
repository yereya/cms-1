<div class="slide-element" role="slider">
    <div class="row" style="width: 950px;">
        <div class="col-md-7 left-slide-part">
            <div class="title-with-logo clearfix">
                <h3 class="slide-title">{{$post->name}}</h3>
                <img class="slide-logo" src="{{ fullImagePathById($post->content->image_slider_logo) }}"
                     alt="brand logo">
            </div>
            {!! $post->content->excerpt ?? '' !!}
            <div class="benefits-wrapper">
                <div class="benefit-card">
                    <p> {!! $post->content->registration_process ?? '' !!} </p>
                </div>
                @if(isset($post->content->platforms_available))
                    @if(isJson($post->content->platforms_available))
                        <div class="benefit-card-list benefit-card">
                            <p>
                                @foreach(json_decode($post->content->platforms_available) as $device => $device_value)
                                    @if($device == true)
                                        <i class="icon-ok green"><span>{!! ucfirst($device) !!}</span></i>
                                    @else
                                        <i class="icon-no red"><span>{!! ucfirst($device) !!}</span></i>
                                    @endif
                                @endforeach
                            </p>
                        </div>
                    @endif
                @endif
                <div class="benefit-card">
                    <p> {!! $post->content->registration_deposit ?? '' !!} </p>
                </div>
            </div>
        </div>

        <div class="col-md-5 slide-featured-image">
            <img class="img-responsive" src="{{ fullImagePathById($post->content->image_lp_dl_screenshot) }}"
                 alt="featured image">
        </div>

        <div class="col-md-6 actions-slider" aria-orientation="horizontal" role="slider">
            @bonusButton([
                'class' => 'bonus-button pull-left',
                'content' => 'Claim your bonus now',
                'outlink' => $post->content->outlink
            ])
        </div>

        <button type="button" data-role="none" class="slick-prev slick-arrow left" aria-label="Previous" role="button" style="display: inline-block;">
            <i class="icon-party-casino-arrow left-nav"></i></button>

        <button type="button" data-role="none" class="slick-next slick-arrow right" aria-label="Next" role="button" style="display: inline-block;">
            <i class="icon-party-casino-arrow right-nav"></i></button>
    </div>
</div>
