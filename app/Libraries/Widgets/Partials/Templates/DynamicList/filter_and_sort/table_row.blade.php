@if ($part == 'open')
    <div class="table-row {{ $class ?? '' }}"
         data-container="{{ $container }}"
         @if(isset($global_key))
           data-order-index="{{ $global_key }}"
         @endif
         @if (isset($orders) && count($orders) > 0)
             @foreach($orders as $order)
                 @foreach($order as $field)
                     @php ($field_value = $post->content->{$field['name']} ?? 0)
                     @php ($value = is_numeric($field_value) ? $field_value : 1)
                     @php ($attr = 'data-' . $field['name'])
                     data-{{ $field['name'] }}="{{ $value }}"
                 @endforeach
             @endforeach
         @endif

         @if (isset($filters) && count($filters) > 0)
             @foreach($filters as $filter)
                 @foreach($filter as $field)
                     @php ($value = isset($post->content->{$field['name']}) ? true : false)
                     @php ($attr = 'data-' . $field['name'])
                     data-{{ $field['name'] }}="{{ $value }}"
                 @endforeach
             @endforeach
         @endif
    >
@endif

@if ($part == 'close')
    </div>
@endif