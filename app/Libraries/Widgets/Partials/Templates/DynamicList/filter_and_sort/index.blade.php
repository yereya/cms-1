@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'open'])

@php
    $content="<p style='max-width: 400px; width: 400px; line-height: 1.4; font-size: 1em;'>The ranking and rating available on this site is a combination of user experience recommendations, views, ratings and comments as well as our own ranking and rating algorithms. In order to provide you with this information, we generate advertising revenues and referral fees from service providers featured on the site. We make best efforts to keep the information up-to-date and accurate. This website and the services accessible on or via this website are provided 'as is', and your use of and reliance on the information on this website and the online services are entirely at your own responsibility and risk. To view the full site terms &amp; conditions, <a href='http://top5casinosites.co.uk/terms-and-conditions/' target=' _blank'>click here</a></p>";
@endphp

@include('WidgetsLib::Assets.tooltip', [
            'content' => $content,
            'html_id' => 'tooltip-advertiser'
         ])

<div class="row dynamic-list-filter">
    <div class="col-lg-5 labels">
        <a href="#" role="button">Download Software</a>
        <a href="#" role="button">Skillonnet</a>
    </div>

    <div class="col-lg-7 sort-filter text-right">
        {{-- START SORT BY --}}
        <div class="text-right disclosure">
            <p class="tooltipster" data-tooltip-content="#tooltip-advertiser" data-tooltipster='{"side":"top",
        "animation":"slide", "theme": "tooltipster-shadow"}'>Advertiser disclosure</p>
        </div>
        @if (isset($orders) && count($orders) > 0)
            <div class="btn-group">
                <button type="button" class="btn btn-default" aria-pressed="false">Sort By</button>
                <button type="button" class="btn btn-default dropdown-toggle" aria-pressed="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    @foreach($orders as $group)
                        <li><a href="#"><b>{{ $group[0]['field_group']['name'] }}</b><i class="icon-filter-arrow pull-right text-warning"></i></a>
                        @foreach($group as $field)
                            <li data-sort="{{ $field['name'] }}"><a href="#" class="option">{{ $field['display_name'] }}<i class="icon-filter-plus pull-right"></i></a></li>
                        @endforeach
                    @endforeach
                </ul>
            </div>
        @endif
        {{-- END SORT BY --}}

        {{-- START FILTER --}}
        @if (isset($filters) && count($filters) > 0)
            <div class="btn-group">
                <button type="button" class="btn btn-default" aria-pressed="false">Filter</button>
                <button type="button" class="btn btn-default dropdown-toggle" aria-pressed="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    @foreach($filters as $group)
                        <li><a href="#"><b>{{ $group[0]['field_group']['name'] }}</b><i class="icon-filter-arrow pull-right text-warning"></i></a>
                        @foreach($group as $field)
                            <li data-filter="{{ $field['name'] }}"><a href="#" class="option">{{ $field['display_name'] }}<i class="icon-filter-plus pull-right"></i></a></li>
                        @endforeach
                    @endforeach
                </ul>
            </div>
        @endif
        {{-- END FILTER --}}

    </div>
</div>

@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'close'])
