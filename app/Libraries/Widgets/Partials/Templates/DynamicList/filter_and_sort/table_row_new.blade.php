@if ($part == 'open')
    <div class="table-row {{ $class ?? '' }}"
         data-container="{{ $container }}"
         @if ($post->orders ?? false)
            @foreach($post->orders as $name => $value)
                data-{{ $name }}="{{ $value }}"
            @endforeach
         @endif

         @if ($post->filters ?? false)
            @foreach($post->filters as $name => $value)
                data-{{ $name }}="{{ $value ? 1 : 0 }}"
            @endforeach
         @endif
    >
        @endif

        @if ($part == 'close')
    </div>
@endif