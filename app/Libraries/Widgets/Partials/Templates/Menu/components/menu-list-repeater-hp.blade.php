<ul role="menu" aria-haspopup="true">
    @foreach($menu_items as $menu_item)
        <li class="nav-item" role="menuitem">
            <div class="{!! strtolower($menu_item->post->slug) ?? 'casino' !!}-background hidden-xs"></div>
            <div class="menu-element-wrapper" data-animate="pulse">
                <a href="{!! $menu_item->href !!}"
                   title="{{ $menu_item->title ?? '' }}"
                   class="{{ $menu_item->class ?? '' }}"
                        {!!  $menu_item->attributes ?? '' !!}
                        {{ isset($menu_item['target']) ? "target=$menu_item->target" : '' }}
                        {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
                >{!! $menu_item->custom_html !!}
                </a>
            </div>

            @if(isset($menu_item->children) && (0!=count($menu_item->children)))
                <ul class="sub-menu"
                    aria-label="submenu" {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}>
                    @foreach($menu_item->children as $child_item)
                        <li role="menuitem">
                            <a href="{!! $child_item->href !!}"
                               title="{{ $child_item->title ?? '' }}"
                               class="{{ $child_item->class ?? '' }}"
                                    {{ isset($child_item->target) ? "target=$child_item->target" : '' }}
                                    {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
                            >{!! $child_item->name !!}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>
