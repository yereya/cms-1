<ul role="menu" aria-haspopup="true">
    @foreach($menu_items as $menu_item)
        <li class="nav-item {{ isCurrentURL($menu_item->href) ? 'active' : '' }}" role="menuitem">
            <a href="{!! $menu_item->href !!}"
               title="{{ $menu_item->title ?? '' }}"
               class="{{ $menu_item->class ?? '' }}"
               {!!  $menu_item->attributes ?? '' !!}
               {{ isset($menu_item['target']) ? "target=$menu_item->target" : '' }}
               {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
            >{!! $menu_item->custom_html !!}
                <span class="menu-item-brand">{!! $menu_item->name !!}</span></a>

            @if(isset($menu_item->children) && count($menu_item->children))
                @include($widget->templates_namespace . '::components.menu-list-repeater', ['menu_items' => $menu_item->children])
            @endif
        </li>
    @endforeach
</ul>