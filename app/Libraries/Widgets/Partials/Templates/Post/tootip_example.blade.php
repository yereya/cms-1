@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'open'])

{{--TODO DELETE ME AFTER YOU HAVE REAL EXAMPLES IN THE SYSTEM--}}

<div style="background:aliceblue" >
    <span class="tooltipster"
          data-tooltip-content="#tooltipster_content">
          This span has a tooltip with HTML when you hover over it!
    </span>
</div>

@include('WidgetsLib::Assets.tooltip', [
    'content' => '<span id="tooltipster_content">
        <strong>This is the content of my tooltip!</strong>
        <img src="https://www.gravatar.com/avatar/ed236e72f44d5c8f29314ed0a4add798?s=328&d=identicon&r=PG" />
    </span>'
])

@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'close'])

