<div class="slot-image img-responsive">
    <img src="{{ topMediaPath() . toLowerCleanSequence($field_item) . '.png' }}">
</div>

<div class="row">
    <div class="col-md-6 slot-review-button">
        @include('WidgetsLib::Assets.bonus-button', [
            'class' => 'bonus-button yellow',
            'content' => 'FULL REVIEW'
        ])
    </div>
    <div class="col-md-6 slot-game-button">
        @include('WidgetsLib::Assets.bonus-button', [
            'class' => 'bonus-button green',
            'content' => 'PLAY GAME'
        ])
    </div>
</div>