@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'open'])

<div class="slide-element row" role="slider">
    <div class="slide-content">
        <div class="title-with-logo clearfix">
            <h3 class="slide-title">{{$post->name}}</h3>
            <img class="slide-logo" src="{{ fullImagePathById($post->content->image_slider_logo) }}" alt="brand logo">
        </div>

        <div class="benefits-wrapper">
            <div class="benefit-card">
                <p> Registration timer 4 Minutes Click to deposit </p>
            </div>
            <div class="benefit-card-list">
                <i class="icon-ok">Desktop</i>
                <i class="icon-no">Tablet</i>
                <i class="icon-ok">Mobile</i>
                <p> test text  </p>
            </div>
            <div class="benefit-card">
                <p> test text  </p>
            </div>
        </div>
    </div>

    <div class="slide-featured-image">
        <img src="{{ fullImagePathById($post->content->og_image) }}" alt="featured image">
    </div>

</div>

@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'close'])