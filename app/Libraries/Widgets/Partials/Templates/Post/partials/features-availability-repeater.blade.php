<li>
    <div class="row">
        <div class="col-md-6">
            DEPOSIT
        </div>

        <div class="col-md-6">
            WITHDRAW
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 deposit">
                                <span class="icon-wrapper">
                                    <i class="icon-ok"></i>
                                </span>
        </div>

        <div class="col-md-6 withdraw">
                                <span class="icon-wrapper">
                                    <i class="icon-ok"></i>
                                </span>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 feature-wrapper">
            <div class="sprite {{snakeCase($field_key)}}"></div>
        </div>
    </div>
</li>