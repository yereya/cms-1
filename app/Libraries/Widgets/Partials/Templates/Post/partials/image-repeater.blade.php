<div class="image-wrapper">
    <img class="img-responsive" src="{{ url('top-assets/play_right/images/' . strtolower(str_replace([' ', "'"], ['_',''],
     $field_item)). '.png') }}" alt="{{ $field_item }} icon">
</div>
<p class="slot-title">{{ $field_item }}</p>