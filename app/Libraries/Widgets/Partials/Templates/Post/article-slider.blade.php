@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'open'])

<div class="widget_article article-slider">

    <div class="slider-wrapper">

        @foreach($slides as $slider)

            @include('WidgetsLib::Templates.Articles.partials.slide-template', ['slide' => $slide])

            <div class="actions-slider" aria-orientation="horizontal" role="slider">
                <button class="btn btn-bonus pull-left" role=link>Claim your bonus now</button>
            </div>

            <span class="slider-navigation-buttons">
                <a href="#" onclick="switchSlides(1)" role="switch" class="left-navigator"><i class="fa fa-arrow-left"></i></a>
                <a href="#" onclick="switchSlides(-1)" role="switch" class="right-navigator"><i class="fa fa-arrow-right"></i></a>
            </span>

        @endforeach
    </div>

</div>

@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'close'])
@php(\Assets::addCss(['libs/slick/slick/slick.css', 'libs/slick/slick/slick-theme.css']))
{!! requireJsAsset('carousel') !!}