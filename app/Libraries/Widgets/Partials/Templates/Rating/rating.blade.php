@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'open'])

<div class="widget_rating rating">

    <span class="num-of-ratings">(929)</span>

    <div class="stars star-wrapper">
        <form action="">
            <fieldset class="rating-group">
                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-5" name="rating" value="5"/><label for="star-5"></label>
                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-4" name="rating" value="4"/><label for="star-4"></label>
                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-3" name="rating" value="3"/><label for="star-3"></label>
                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-2" name="rating" value="2"/><label for="star-2"></label>
                <input type="radio" role="menuitemradio" aria-checked="false" class="star star-1" name="rating" value="1"/><label for="star-1"></label>
            </fieldset>
        </form>
    </div>

    <div class="review-wrapper">
        <a href="#" class="read-review">Read Review</a>
    </div>
</div>

@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'close'])
