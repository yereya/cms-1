@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'open'])
    <ul>
        @foreach($related_articles as $article)
            <li>
                @if(isset($article->title))
                    <h3>{{$article->title}}</h3>
                @endif
                @if(isset($article->image))
                    <img src="{{$article->image}}" alt="featured image">
                @endif

                @if(isset($article->excerpt))
                    <p>{!!  substr($article->excerpt,0,100) !!}</p>
                @endif
            </li>
        @endforeach
    </ul>
@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'close'])