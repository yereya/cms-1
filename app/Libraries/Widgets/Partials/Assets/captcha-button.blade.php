<button type='submit' class="base_submit default-button pull-left">
    {{$content ?? ''}}
</button>
<button class="g-recaptcha hidden"
        role="link"
        data-sitekey="{{$attributes['data']['sitekey'] ?? ''}}"
        data-callback="{{$attributes['data']['callback'] ?? ''}}"
        data-size="{{$attributes['data']['size'] ?? ''}}">
</button>
{!! Recaptcha::render() !!}
