<picture>
    <?php $path = fullImagePathById($image_id) ?>

    <source srcset="{{ $path }} 1x,
                    {{ ($path && fullRetinaImagePath($path, '@2x')) ? fullRetinaImagePath($path, '@2x') : '' }} 2x,
                    {{ ($path && fullRetinaImagePath($path, '@3x')) ? fullRetinaImagePath($path, '@3x') : '' }} 3x ">
    <img src="{{ $path }}" alt="{{ $alt ?? '' }}" class="{{ $class ?? '' }} img-responsive"
         image_id="{{ $image_id ?? '' }}">
</picture>