@if($part == 'open')

    @php(\Assets::addCss(['libs/slick/slick/slick.css']))
    {!! requireJsAsset('carousel') !!}

    <div class="carousel-wrapper {{ $class_wrapper ?? ''}}">

        @if(!empty($custom_arrows))
            {!! $custom_arrows !!}
        @endif

        <div class="slider-wrapper {{ $class ?? ''}}" {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}>
@endif

@if($part == 'close' )
        </div>

        @if((!empty($custom_arrows)))
            {!! $custom_arrows !!}
        @endif
    </div>
@endif