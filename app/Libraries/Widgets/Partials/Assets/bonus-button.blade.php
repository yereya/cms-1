<a class="{{$class ?? ''}}" role="link"
   href="{{$outlink ?? '#'}}" {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}>{!! $content !!}</a>