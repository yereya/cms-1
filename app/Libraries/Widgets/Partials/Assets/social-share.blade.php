{{--FACEBOOK--}}
@if(!empty($types['facebook']))
    <script>
        var app_id = {{getSiteSetting('facebook_app_id') ?? 0}};
        window.fbAsyncInit = function () {
            FB.init({
                appId: app_id,
                xfbml: true,
                version: 'v2.5'
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <span class="facebook fb-button"
          date-href="{{$url}}"
          data-layout="button_count"
          data-size="large"
          data-mobile-iframe="true"
          onclick="FB.ui({method: 'share',href: location.href}, function (response) {});">
        <a class="fb-xfbml-parse-ignore"
           target="_blank"
           href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">
            <i class="icon-facebook"></i>
        </a>
    </span>
@endif

@if(!empty($types['twitter']))

    {{--TWEETER --}}
    <a class="twitter" href="https://twitter.com/share"
       onclick="window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"
       data-url="" data-count="none">
        <i class="icon-twitter"></i>
    </a>
@endif

@if(!empty($types['google-plus']))
    {{--GOOGLE PLUS--}}
    <script src="https://apis.google.com/js/platform.js" async="" defer="" gapi_processed="true"></script>
    <a class="google-plus" href="https://plus.google.com/share?url={{$url}}"
       onclick="window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
        <i class="icon-google-plus"></i>
    </a>
@endif