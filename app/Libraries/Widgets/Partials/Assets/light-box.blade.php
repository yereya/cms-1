<div class="light_box">

{{--TYPE IMAGE --}}
@if(isset($type) && $type == 'image')
     <a href="{{ $attributes['lightbox_link'] ?? $url }}" data-toggle="lightbox" data-title="{{$attributes['name'] ??
     ''}}">
         <img src="{{ $thumbnail ?? '' }}" {!! HTML::attributes($attributes['link_custom_attributes'] ?? $html_attr) !!} />
     </a>

{{--TYPE VIDEO --}}
@else
    @php
        $not_empty_post_n_content = !empty($post) && !empty($post->content);
        $not_empty_post_field =  !empty($post->content->{$post_fields[0]}) && !empty($post->content->{$post_fields[1]});
        $all_fields_valid = $not_empty_post_field && $not_empty_post_field;
    @endphp

      <a href="{!! $all_fields_valid ? $post->content->{$post_fields[1]} : '' !!}" data-toggle="lightbox">
          <img src="{{ $all_fields_valid ? $post->content->{$post_fields[0]} : '' }}" />
      </a>
    @endif
</div>
