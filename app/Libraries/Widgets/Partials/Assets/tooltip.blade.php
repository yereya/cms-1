<div class="tooltip_templates">
    <div id="{!! $html_id !!}">
        {!! $content !!}
        @if(isset($get_bonus_link))
            <a class="get-bonus"
               {!! isset($attributes) ? HTML::attributes($attributes) : '' !!}
               href="{{ $get_bonus_link ?? '' }}">{{ $button_value ?? 'GET BONUS' }}</a>
        @endif
    </div>
</div>