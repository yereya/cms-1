
@if($part == 'open')
    <div class="widget-wrapper {{ $widget->class }} {{ $widget_data_attributes['class'] ?? '' }} {{ $widget_template->class }} {{ $widget_data->class }}"
            {!! isset($html_attr) ? HTML::attributes($html_attr) : '' !!}>
@endif

@if($part == 'close')
    </div>
@endif