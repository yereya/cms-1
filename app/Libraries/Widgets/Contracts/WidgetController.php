<?php namespace App\Libraries\Widgets\Contracts;

/**
 * Interface WidgetController
 *
 * @package App\Libraries\TemplateBuilder\Contracts
 */
interface WidgetController
{
    /**
     * Verify
     *
     * @return bool
     */
    public function verify();

    /**
     * Build
     *
     * @return void
     */
    public function build();

    /**
     * Get
     *
     * @return mixed
     */
    public function get();
}