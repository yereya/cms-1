<?php
namespace App\Libraries\Widgets\Contracts;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Templates\Column;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\SiteWidget;

/**
 * Interface WidgetFormBuilder
 */
interface WidgetFormBuilder
{
    /**
     * WidgetFormBuilder constructor.

*
*@param Site                  $site
     * @param Template        $template
     * @param SiteWidget      $widget
     * @param Column          $column
     * @param WidgetData|null $widget_data
     */
    public function __construct(
        Site $site,
        Template $template,
        SiteWidget $widget,
        Column $column = null,
        WidgetData $widget_data = null
    );
}