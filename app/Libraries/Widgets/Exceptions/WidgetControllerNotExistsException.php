<?php namespace App\Libraries\Widgets\Exceptions;

/**
 * Class WidgetControllerNotExistsException
 *
 * @package App\Libraries\TemplateBuilder\Exceptions
 */
class WidgetControllerNotExistsException extends \Exception
{
    //
}