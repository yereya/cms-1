<?php

namespace App\Libraries\Widgets;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\ABTest;
use App\Entities\Models\Sites\Templates\Item;
use App\Entities\Models\Sites\Templates\Column;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use App\Libraries\Widgets\Contracts\WidgetFormBuilder as WidgetFormBuilderInterface;

class WidgetFormBuilder implements WidgetFormBuilderInterface
{
    /**
     * @var Site $site
     */
    protected $site;

    /**
     * @var Template $template
     */
    protected $template;

    /**
     * @var SiteWidget $widget
     */
    protected $widget;

    /**
     * @var WidgetData|null $widget_data
     */
    protected $widget_data;

    /**
     * @var ABTest|null $ab_tests
     */
    protected $ab_tests;

    /**
     * @var Column $column
     */
    private $column;

    /**
     * @ver Item
     */
    private $column_item;

    /**
     * WidgetFormBuilder constructor.
     *
     * @param Site            $site
     * @param Template        $template
     * @param SiteWidget      $widget
     * @param Column          $column
     * @param WidgetData|null $widget_data
     * @param ABTest|null     $ab_tests
     *
     * @internal param ABTest|null $ab_test
     */
    public function __construct(
        Site $site,
        Template $template,
        SiteWidget $widget,
        Column $column = null,
        WidgetData $widget_data = null,
        ABTest $ab_tests = null
    )
    {
        // TODO Remove the $column = null after column will be passed well on widget creation
        $this->site        = $site;
        $this->template    = $template;
        $this->widget      = $widget;
        $this->column      = $column;
        $this->widget_data = $widget_data;
        $this->ab_tests    = $ab_tests->all();

        if (request()->has('item_id')) {
            $this->column_item = Item::find(request()->input('item_id'));
        }
    }

    /**
     * View
     *
     * @return \Illuminate\View\View
     */
    public function view()
    {
        // if there is no template->id (that means we came from route sites.{site}.widgets} ) we take portlet-form
        // and modal-form if it is a modal ( sites.{site}.templates.{template} )
        $form_type = isset($this->template->id) ? 'widgets-forms.containers.modal-form' : 'widgets-forms.containers.portlet-form';

        return view($form_type)->with([
            'site'          => $this->site,
            'template'      => $this->template,
            'widget'        => $this->widget,
            'source_column' => $this->column,
            'widget_data'   => $this->widget_data,
            'view'          => $this->widget->view,
            'column_item'   => $this->column_item,
            'ab_tests'      => $this->ab_tests
        ]);
    }
}