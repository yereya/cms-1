<?php
namespace App\Libraries\Widgets\Controllers;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Widgets\WidgetDataPost;
use App\Entities\Repositories\Sites\CommentRepo;
use App\Entities\Repositories\Sites\PostRepo;
use App\Libraries\Widgets\Widgets;

/**
 * Class Image
 *
 * @package App\Libraries\TemplateBuilder\Widgets
 */
class Comments extends Widgets
{

    /**
     * @var
     */
    private $commentsTree;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        if (empty($this->widget_data->content_type_id)) {
            $this->controllerException('content_type_id');
        }

        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        $content_type_id = $this->widget_data->content_type_id;
        $this->content_type = SiteContentType::find($content_type_id);

        $post_id = $this->getPostId();
        $comment_type = $this->widget_data->data->comment_type;
        $this->commentsTree = (new CommentRepo())->getCommentsTree($post_id, $comment_type);
    }

    /**
     * Get Post Id
     *
     * @return int
     */
    private function getPostId()
    {
        $selected_post = WidgetDataPost::where('widget_data_id', $this->widget_data->id)->first();

        if ($selected_post) {

            return $selected_post->post_id;
        } else if ($this->current_post) {

            return $this->current_post->id;
        } else {
            $post = (new PostRepo())->getFirstPostByContentTypeId($this->widget_data->content_type_id);

            return $post->id;
        }
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('comments', $this->commentsTree)
            ->render();
    }

}