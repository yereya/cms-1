<?php

namespace App\Libraries\Widgets\Controllers;

use HTML;
use App\Libraries\Widgets\Widgets;
use App\Entities\Models\Sites\Media;
use App\Entities\Repositories\Sites\PageRepo;
use App\Entities\Repositories\Sites\PostRepo;

/**
 * Class Image
 *
 * @package App\Libraries\TemplateBuilder\Widgets
 */
class Image extends Widgets
{
    /**
     * Hold the current image link href value.
     *
     * @ver String
     */
    private $link_to_page;

    /**
     * Hold the current image link attributes.
     *
     * @ver String
     */
    private $link_attributes = '';

    /**
     * Hold the current image src value.
     *
     * @ver String
     */
    private $image_url = null;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        if (empty($this->widget_data_attributes['image_id'])) {
            $this->controllerException('image_id');
        }

        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        $media = Media::find($this->widget_data_attributes['image_id']);

        if ($media) {
            $this->image_url = $media->getFullMediaPath();
        } else {
            $this->image_url = '';
        }

        $this->link_to_page = $this->widget_data_attributes['custom_link'] ?? null;

        $this->buildLinkAttributes(['target','title']);

        if (!empty($this->widget_data_attributes['page_id'])) {
            $page_repo = new PageRepo();
            $page = $page_repo->find($this->widget_data_attributes['page_id']);
            $page_url = $page_repo->getUrl($page->post_id);

            if (!empty($this->widget_data_attributes['post_id'])) {
                $post = (new PostRepo())->find($this->widget_data_attributes['post_id']);
                $page_url .= DIRECTORY_SEPARATOR . $post->slug;
            }

            $this->link_to_page = $page_url;
        }
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('url', $this->image_url)
            ->with('link_to_page', $this->link_to_page)
            ->with('link_attributes', $this->link_attributes)
            ->withShortcodes()
            ->render();
    }

    /**
     * Build link attributes.
     *
     * @param array $attributes
     *
     * @return void
     */
    protected function buildLinkAttributes(array $attributes){
        $link_attributes = [];
        foreach($attributes as $attribute) {
            if (isset($this->widget_data_attributes["link_$attribute"])) {
                $link_attributes[$attribute] = $this->widget_data_attributes["link_$attribute"];
            }
        }
        // Convert attributes array to string
        $this->link_attributes = HTML::attributes($link_attributes);
        // Check if raw attributes exists
        if (isset($this->widget_data_attributes['link_custom_attributes'])){
            // Add raw attributes string
            $this->link_attributes .= ' ' . $this->widget_data_attributes['link_custom_attributes'];
        }
    }
}