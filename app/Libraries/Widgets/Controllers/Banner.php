<?php
namespace App\Libraries\Widgets\Controllers;

use App\Entities\Repositories\Sites\BannerRepo;
use App\Libraries\Widgets\Widgets;
use Faker\Provider\DateTime;

class Banner extends Widgets
{
    /**
     * Build
     *
     * @return void
     */
    public function build()
    {
        // TODO: Implement build() method.
    }

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Get
     *
     * @return mixed
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('now', date('Y/m/d H:i:s'))
            ->render();
    }
}
