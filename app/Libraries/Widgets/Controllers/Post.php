<?php

namespace App\Libraries\Widgets\Controllers;

use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\SiteContentTypeFieldRepo;
use App\Entities\Repositories\Sites\Widgets\WidgetDataPostRepo;
use App\Libraries\Widgets\Widgets;
use Doctrine\Common\Collections\Collection;

/**
 * Class Article
 *
 * @package App\Libraries\WidgetsBuilder\Widgets
 */
class Post extends Widgets
{
    /**
     * @var Collection
     */
    protected $post;

    /**
     * @var
     */
    protected $post_fields;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        $post_repo         = new PostRepo();
        $this->resolvePost($post_repo);
        $this->post_fields = $this->getFields();
    }

    /**
     * Get Post Data
     *
     * @param PostRepo $post_repo
     *
     * @return mixed
     */
    private function resolvePost(PostRepo $post_repo)
    {
        $post_order = (new WidgetDataPostRepo())->getByWidgetDataId($this->widget_data->id);

        if (!empty($post_order->post_id)) {
            $this->post = $post_repo->find($post_order->post_id);
        } else {
            if (!empty($this->current_post) && $this->current_post->isAPage()) {
                $this->post = $this->current_post;
            } else {
                $this->post = $post_repo->getFirstPostByContentTypeId($this->widget_data->content_type_id);
            }
        }

        return $this->post;
    }

    /**
     * Get Fields
     *
     * @return null
     */
    private function getFields()
    {
        $site_content_type_field_repo = new SiteContentTypeFieldRepo();

        if (isset($this->widget_data_attributes['post_field_ids'])) {

            return $site_content_type_field_repo->getFieldsById($this->widget_data_attributes['post_field_ids'])
                ->pluck('name');
        }

        return null;
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('post', $this->post)
            ->with('post_fields', $this->post_fields)
            ->withShortcodes()
            ->render();
    }
}
