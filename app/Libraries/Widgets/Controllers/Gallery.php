<?php

namespace app\Libraries\Widgets\Controllers;

use App\Entities\Repositories\Sites\MediaRepo;
use App\Libraries\Widgets\Widgets;

class Gallery extends Widgets
{
    private $media;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        if (empty($this->widget_data_attributes['label_id'])) {
            $this->controllerException('label_id');
        }

        return true;
    }

    public function build()
    {
        $label_id = $this->widget_data_attributes['label_id'];

        $this->media = (new MediaRepo())->findByLabel($label_id);
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('media', $this->media)
            ->withShortcodes()
            ->render();
    }
}