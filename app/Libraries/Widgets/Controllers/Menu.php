<?php
namespace App\Libraries\Widgets\Controllers;

use App\Entities\Models\Sites\MenuItem;
use App\Libraries\Widgets\Widgets;
use Doctrine\Common\Collections\Collection;

/**
 * Class Image
 *
 * @package App\Libraries\TemplateBuilder\Widgets
 */
class Menu extends Widgets
{
    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        if (empty($this->widget_data->menu_id)) {
            $this->controllerException('menu_id');
        }

        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        $this->widget_data_attributes['view']['menu_items'] = MenuItem::getTree($this->widget_data->menu_id);
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->render();
    }
}