<?php

namespace App\Libraries\Widgets\Controllers;

use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Libraries\Widgets\WidgetBuilder;
use App\Libraries\Widgets\Widgets;

class Group extends Widgets
{
    protected $tabs;

    /**
     * @return bool
     */
    public function verify() {
        if (empty($this->widget_data_attributes['tab'])) {
            $this->controllerException('tab');
        }

        return true;
    }

    /**
     * Build
     */
    public function build() {
        $tabs_attributes = $this->widget_data_attributes['tab'];
        $tabs = [];

        foreach ($tabs_attributes as $id => $options) {
            $tabs[] = [
                'widget_data_id' => str_replace('_', '', $id),
                'tab_name' => $options->tab_name ?? '',
                'default' => $options->default ?? 0,
                'html' => $this->widgetHtml(str_replace('_', '', $id))
            ];
        }

        $this->tabs = $tabs;
    }

    /**
     * Get widget html
     *
     * @param $widget_data_id
     *
     * @return array
     */
    private function widgetHtml($widget_data_id) {
        $widget_data = WidgetData::find($widget_data_id);

        if (!$widget_data){

            return ['html' => ''];
        }

        $widget = SiteWidget::find($widget_data->widget_id);

        $html = new WidgetBuilder($widget_data, $widget, $this->current_post);

        return $html->get();
    }

    /**
     * Get view
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('tabs', $this->tabs)
            ->withShortcodes()
            ->render();
    }
}