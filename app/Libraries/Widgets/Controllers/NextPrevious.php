<?php

namespace App\Libraries\Widgets\Controllers;

use App\Libraries\Widgets\Traits\DynamicListTrait;
use App\Entities\Repositories\Sites\DynamicList\DynamicListRepo;
use App\Libraries\Widgets\Widgets;
use Illuminate\Support\Facades\Request;
use App\Entities\Models\Sites\Post as ModelPost;
use App\Libraries\Widgets\Helpers\DynamicList;

class NextPrevious extends Widgets
{
    use DynamicListTrait;

    protected $next_url;
    protected $previous_url;

    protected $dynamic_list_lib;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        if (empty($this->widget_data_attributes['dynamic_list_id'])) {
            return $this->controllerException('dynamic_list_id');
        }

        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        // Template builder not have post
        if (empty($this->current_post)) {
            $this->previous_url = null;
            $this->next_url = null;

            return;
        }

        $dynamic_list = (new DynamicListRepo())->find($this->widget_data_attributes['dynamic_list_id']);

        $this->dynamic_list_lib = new DynamicList($this->buildDynamicListParams($dynamic_list));
        // saved posts ids
        $posts = $this->dynamic_list_lib->getBasePosts();
        $posts_index = $posts->pluck('id')->toArray();

        $post_included = array_search($this->current_post->id, $posts_index);
        if ($post_included === null || $post_included === false) {
            $this->controllerException(
                $this->current_post->id,
                'This current post %d not contain in current dynamic list. Controller: %s. WidgetDataId: %s');
        }

        $previous_key = $this->findPreviousPosition($this->current_post->id, $posts_index);
        $next_key = $this->findNextPosition($this->current_post->id, $posts_index);

        $this->previous_url = $this->buildUrl($posts->get($previous_key));
        $this->next_url = $this->buildUrl($posts->get($next_key));
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('previous_url', $this->previous_url)
            ->with('next_url', $this->next_url)
            ->render();
    }

    /**
     * Build url
     *
     * @param $post
     * @return string
     */
    private function buildUrl(ModelPost $post) :string
    {
        $current_url = Request::url();
        $start_position = 0;
        $end_position = -1 * strlen($this->current_post->slug);
        $base_url = substr($current_url, $start_position, $end_position);

        return $base_url . $post->slug;
    }

    /**
     * Find next index position
     *
     * @param $current_position
     * @param $posts
     * @return mixed
     */
    private function findNextPosition(int $current_position, array $posts) :int
    {
        $next_position = ++$current_position;

        return array_search($next_position, $posts) ?? $current_position;
    }

    /**
     * Find previous index position
     *
     * @param $current_position
     * @param $posts
     * @return mixed
     */
    private function findPreviousPosition(int $current_position, array $posts) :int
    {
        $previous_position = --$current_position;

        return array_search($previous_position, $posts) ?? $current_position;
    }
}