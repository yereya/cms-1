<?php

namespace App\Libraries\Widgets\Controllers;

use App\Entities\Repositories\Sites\ContentTypeRepo;
use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\SiteContentTypeFieldRepo;
use App\Entities\Repositories\Sites\Widgets\WidgetDataPostRepo;
use App\Libraries\Widgets\Widgets;
use Doctrine\Common\Collections\Collection;

/**
 * Class Article
 *
 * @package App\Libraries\WidgetsBuilder\Widgets
 */
class PostList extends Widgets
{
    /**
     * @var Collection
     */
    protected $posts;

    /**
     * @var array
     */
    protected $post_fields;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        $post_repo = new PostRepo();
        $this->resolvePosts($post_repo);
        $this->post_fields = $this->getFields();
    }

    /**
     * Get Post Data
     *
     * @param PostRepo $post_repo
     *
     * @return mixed
     */
    private function resolvePosts(PostRepo $post_repo)
    {
        $this->posts = $post_repo->getByLabel($this->widget_data->data->label_id, $this->widget_data->content_type_id);
    }

    /**
     * Get Fields
     *
     * @return mixed
     */
    private function getFields()
    {
        $site_content_type_field_repo = new SiteContentTypeFieldRepo();

        if (isset($this->widget_data_attributes['post_field_ids'])) {

            return $site_content_type_field_repo->getFieldsById($this->widget_data_attributes['post_field_ids'])
                ->pluck('name');
        }

        return null;
    }

    /**
     * Render the widget view.
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('posts', $this->posts)
            ->with('post_fields', $this->post_fields)
            ->withShortcodes()
            ->render();
    }
}
