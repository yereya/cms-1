<?php

namespace App\Libraries\Widgets\Controllers;

use App\Entities\Models\Sites\Post as PostModel;
use App\Entities\Models\Sites\Widgets\WidgetDataPost;
use App\Libraries\Widgets\Widgets;

/**
 * Class ComparisonTable
 *
 * @package app\Libraries\Widgets\Controllers
 */
class ComparisonTable extends Widgets
{
    protected $posts;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify() {
        if (empty($this->widget_data->content_type_id)) {
            $this->controllerException('content_type_id');
        }

        return true;
    }

    /**
     * Build
     *
     * @return void
     */
    public function build()
    {
        $post_orders = WidgetDataPost::where('widget_data_id', $this->widget_data->id)->orderBy('order')->limit(10)->get()->pluck('post_id')->toArray();

        $this->posts = PostModel::whereIn('id', $post_orders)->get();
    }

    /**
     * Get
     *
     * @return mixed
     */
    public function get() {
        return view($this->getViewPath())
            ->with('posts', $this->posts)
            ->withShortcodes()
            ->render();
    }
}