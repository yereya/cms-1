<?php
namespace App\Libraries\Widgets\Controllers;

use App\Libraries\Widgets\Widgets;

/**
 * Class Wysiwyg
 *
 * @package App\Libraries\TemplateBuilder\Widgets
 */
class Wysiwyg extends Widgets
{
    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        if (!isset($this->widget_data_attributes['content'])) {
            $this->controllerException('content');
        }

        return true;
    }

    /**
     * Build
     */
    public function build()
    {}

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->withShortcodes()
            ->render();
    }
}