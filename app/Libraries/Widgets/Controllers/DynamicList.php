<?php

namespace app\Libraries\Widgets\Controllers;

use App\Entities\Repositories\Sites\PostRibbonRepo;
use App\Libraries\Widgets\Helpers\DynamicList as DynamicListLib;
use App\Libraries\Widgets\Helpers\DynamicListFilters;
use App\Libraries\Widgets\Traits\DynamicListTrait;
use App\Libraries\Widgets\Widgets;

class DynamicList extends Widgets
{

    use DynamicListTrait;

    /**
     * @var DynamicList $dynamic_list
     */
    private $dynamic_list;

    /**
     * @var DynamicListFilters $dynamic_list_filters
     */
    private $dynamic_list_filters;

    /**
     * @var array $posts
     */
    private $posts;

    private $hidden_posts;

    private $filters;

    private $orders;

    private $load_count;

    private $ribbons;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     *
     * @return void
     */
    public function build()
    {
        $this->dynamic_list = $this->widget_data->dynamicList;

        $widget_dynamic_list = new DynamicListLib($this->buildDynamicListParams($this->dynamic_list));

        $this->posts = $widget_dynamic_list->getIncludedPosts();

        $this->hidden_posts = $widget_dynamic_list->getNotIncludedPosts();

        $this->load_count = $this->dynamic_list->query_limit;

        $this->ribbons = $this->getRibbons();

        $this->initDynamicListHelper();
    }

    /**
     * Init Filters and Orders
     *
     * Before attached filters or orders
     * "id" => 112
        "slug" => "32red"
        "name" => "32 Red Casino Review"
        "active" => 1
        "section_id" => 9
        "content_type_id" => 21
        "parent_id" => null
        "priority" => null
        "show_on_sitemap" => 1
        "seo_post_title" => "32Red Casino Review - Great rating for a great product"
        "description" => "See Playright's review on the long-standing UK favorite 32Red. Our review looks at its bonuses, games, payment details, mobile, track record. & more."
        "keywords" => null
        "og_image" => null
        "og_title" => null
        "og_type" => null
        "og_url" => null
        "og_description" => null
        "robots_index" => 1
        "robots_follow" => 1
        "robots_custom" => null
        "permalink" => "https://www.playright.com/casino/32red"
        "canonical_url" => "https://www.playright.com/casino/32red"
        "published_at" => "2017-01-29 13:47:00"
        "created_at" => "2017-01-29 13:49:22"
        "updated_at" => "2017-04-23 09:15:10"
        "deleted_at" => null
     *
     * After attach
     *
     * "id" => 112
        "slug" => "32red"
        "name" => "32 Red Casino Review"
        "active" => 1
        "section_id" => 9
        "content_type_id" => 21
        "parent_id" => null
        "priority" => null
        "show_on_sitemap" => 1
        "seo_post_title" => "32Red Casino Review - Great rating for a great product"
        "description" => "See Playright's review on the long-standing UK favorite 32Red. Our review looks at its bonuses, games, payment details, mobile, track record. & more."
        "keywords" => null
        "og_image" => null
        "og_title" => null
        "og_type" => null
        "og_url" => null
        "og_description" => null
        "robots_index" => 1
        "robots_follow" => 1
        "robots_custom" => null
        "permalink" => "https://www.playright.com/casino/32red"
        "canonical_url" => "https://www.playright.com/casino/32red"
        "published_at" => "2017-01-29 13:47:00"
        "created_at" => "2017-01-29 13:49:22"
        "updated_at" => "2017-04-23 09:15:10"
        "deleted_at" => null
        "filters" => Collection {#5052
            #items: array:9 [
                "livedealer_total_games" => 18
                "e_cogra" => true
                "gamblingcomission" => true
                "malta_gaming_authority" => false
                "playtech" => false
                "play-n-go" => false
                "netent" => false
                "microgaming" => false
                "igt" => false
            ]
        }
        "orders" => Collection {#5106
            #items: array:8 [
                "baccarat_total_games" => 50
                "american" => true
                "european" => true
                "french" => true
                "high_stakes_roulette" => true
                "3_d_roulette" => false
                "multi-_wheel_roulette" => true
                "live_roulette" => true
            ]
        }
        ]
     *
     *
     * Filters or Orders
     *
     * "Baccarat_games_available" => array:1 [ <--- group name
            0 => array:2 [
                "name" => "baccarat_total_games"
                "display_name" => "Baccarat_total offer"
            ]
        ]
        "Roulette Types" => array:7 [ <--- group name
            0 => array:4 [
                "name" => "american"
                "display_name" => "American"
                "multi" => "roulette_types"
                "original" => "American"
            ]
     *  ]
     */
    private function initDynamicListHelper () {
        /* init DynamicListFilters */
        $this->dynamic_list_filters = new DynamicListFilters($this->widget_data);

        /* get filters - return collection {key = value} */
        $this->filters = $this->dynamic_list_filters->getFilters();

        /* get orders - return collection {key = value} */
        $this->orders = $this->dynamic_list_filters->getOrders();

        /* attach filters to posts */
        $this->dynamic_list_filters->attachFilterAttributes($this->posts);
        $this->dynamic_list_filters->attachFilterAttributes($this->hidden_posts);

        /* attach orders to posts */
        $this->dynamic_list_filters->attachOrdersAttributes($this->posts);
        $this->dynamic_list_filters->attachOrdersAttributes($this->hidden_posts);
    }

    /**
     * Get Ribbons
     *
     * @return array
     */
    private function getRibbons()
    {
        return (new PostRibbonRepo())->ribbonPostByDynamicList($this->widget_data->id);
    }

    /**
     * Get
     *
     * @return \Illuminate\View\View
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('posts', $this->posts)
            ->with('hidden_posts', $this->hidden_posts)
            ->with('filters', $this->filters)
            ->with('orders', $this->orders)
            ->with('load_count', $this->load_count)
            ->with('ribbons', $this->ribbons)
            ->with('dynamic_list', $this->dynamic_list)
            ->withShortcodes()
            ->render();
    }
}