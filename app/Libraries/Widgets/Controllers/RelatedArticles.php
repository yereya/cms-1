<?php
namespace App\Libraries\Widgets\Controllers;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Libraries\Widgets\Widgets;

/**
 * Class Image
 *
 * @package App\Libraries\WidgetsBuilder\Widgets
 */
class RelatedArticles extends Widgets
{
    /**
     * @var array
     */
    protected $related_articles = [];

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        $this->setDefaultStyles();

        $related_list_ids = $this->getRelatedArticlesIdsList();

        if (!empty($related_list_ids)) {
            $image_field_name    = $this->getFieldNameByType('image_field_id');
            $excerpt_field_name  = $this->getFieldNameByType('excerpt_field_id');
            $relatedArticlesData = $this->getRelatedArticlesData($related_list_ids);

            foreach ($relatedArticlesData as $row) {
                $arr          = [];
                $arr['title'] = $row->title;
                if ($image_field_name) {
                    $arr['image'] = $row->{$image_field_name};
                }
                if ($excerpt_field_name) {
                    $arr['excerpt'] = $row->{$excerpt_field_name};
                }
                $this->related_articles[] = $arr;
            }
        }
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('related_articles', $this->related_articles)
            ->withShortcodes()
            ->render();
    }

    /**
     * getRelatedArticlesIdsList
     *
     * @return array
     */
    private function getRelatedArticlesIdsList(): array
    {
        $related_metadata = SiteContentTypeField::where('id', $this->widget_data_attributes['related_field_id'])->first()->metadata;
        if (isset($related_metadata->select)) {
            $related_list_ids = Collect($related_metadata->select)->sortBy('priority')->pluck('key')->toArray();
        }

        return $related_list_ids ?? [];
    }

    /**
     * getRelatedData
     * get the related data from the specific table of the content type
     *
     * @return array
     */
    private function getRelatedArticlesData(Array $related_list_ids): array
    {
        $site = \SiteConnectionLib::getSite();
        $content_type_id = $this->widget_data->content_type_id;
        $content_type = SiteContentType::find($content_type_id);
        //TODO FIX ME ContentTypeDataRepo has been deleted
        //TODO Also change the global related $this->related_articles to $this->posts

        $contentTypeDataRepo = new ContentTypeDataRepo($site, $content_type, null);
        $relatedArticlesData = $contentTypeDataRepo->getQuery()
            ->whereIn('id', $related_list_ids)
            ->limit($this->widget_data_attributes['limit'])
            ->get();

        return $relatedArticlesData;
    }

    /**
     * @param String $field_type
     *
     * @return String
     */
    private function getFieldNameByType(String $field_type)
    {
        if (empty($this->widget_data_attributes[$field_type])) {

            return null;
        }

        return SiteContentTypeField::where('id', $this->widget_data_attributes[$field_type])
            ->first()
            ->name;
    }
}