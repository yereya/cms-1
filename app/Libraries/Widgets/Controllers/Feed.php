<?php
namespace App\Libraries\Widgets\Controllers;

use App\Libraries\Widgets\Widgets;

class Feed extends Widgets
{
    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     *
     * @return void
     */
    public function build()
    {
        //...
    }

    /**
     * Get
     *
     * @return mixed
     */
    public function get()
    {
        return view($this->getViewPath())->render();
    }
}
