<?php
namespace App\Libraries\Widgets\Controllers;

use App\Libraries\Widgets\Widgets;

/**
 * Class Image
 *
 * @package App\Libraries\TemplateBuilder\Widgets
 */
class Title extends Widgets
{
    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     */
    public function build()
    {
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->render();
    }
}