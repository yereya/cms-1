<?php

namespace App\Libraries\Widgets\Controllers;

use App\Libraries\Widgets\Widgets;
use App\Entities\Models\Sites\ContentAuthor;

/**
 * Class ContentAuthors
 *
 * @package App\Libraries\WidgetsBuilder\Widgets
 */
class ContentAuthors extends Widgets
{
    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        $this->content_author = ContentAuthor::take(1)->with('image')->first();
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('content_authors', $this->content_author)
            ->withShortcodes()
            ->render();
    }

}