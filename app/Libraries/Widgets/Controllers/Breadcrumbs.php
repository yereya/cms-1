<?php
namespace App\Libraries\Widgets\Controllers;

use App\Libraries\Widgets\Widgets;

class Breadcrumbs extends Widgets
{
    protected $breadcrumbs;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     *
     * @return void
     */
    public function build()
    {
        $url_segments = request()->segments();
        $href_helper = '';

        foreach ($url_segments as $index => $segment) {
            $href_helper                        .= "/" . $segment;
            $this->breadcrumbs[$index]['title'] = $segment;
            $this->breadcrumbs[$index]['href']  = $href_helper;
        }
    }

    /**
     * Get
     *
     * @return mixed
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('breadcrumbs', $this->breadcrumbs)
            ->render();
    }
}
