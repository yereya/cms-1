<?php

namespace App\Libraries\Widgets\Controllers;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Repositories\Sites\LikesRepo;
use App\Libraries\Widgets\Widgets;

/**
 * Class Article
 *
 * @package App\Libraries\WidgetsBuilder\Widgets
 */
class Likes extends Widgets
{

    /**
     * @var int
     */
    protected $likes = 0;

    /**
     * @var
     */
    protected $content_type;

    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        if(empty($this->widget_data->content_type_id)) {

            $this->controllerException('content_type_id');
        }

        return true;
    }

    /**
     * Build
     */
    public function build()
    {
        $content_type_id    = $this->widget_data->content_type_id;
        $this->content_type = SiteContentType::find($content_type_id);
        if (!$this->content_type->has_likes) {
            return;
        }

        $post_id = $this->widget_data->post_id;

        $this->likes = (new LikesRepo())->count($post_id);
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->with('likes', $this->likes)
            ->render();
    }
}
