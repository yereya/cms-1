<?php

namespace App\Libraries\Widgets\Controllers;

use App\Libraries\Widgets\Widgets;

/**
 * Class ContactUs
 */
class Form extends Widgets
{
    /**
     * Verify
     *
     * @return bool
     */
    public function verify()
    {
        return true;
    }

    /**
     * Build
     */
    public function build()
    {
    }

    /**
     * Get
     *
     * @return string
     */
    public function get()
    {
        return view($this->getViewPath())
            ->withShortcodes()
            ->render();
    }
}
