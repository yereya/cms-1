<?php namespace App\Libraries\SitePageSpeed;

use App\Entities\Models\SitesPageSpeedScore;
use GuzzleHttp\Client;

class SitePageSpeed
{
    protected $pageSpeedKey = 'AIzaSyDpTt42PFQlryWk8qKE3Nu7Bdmi2jika8o';

    protected $sites = [];


    /**
     * LogMigrations constructor.
     */
    public function __construct()
    {
        $this->sites = config('mail.pagespeed_mail_alerts.sites');
    }

    /**
     * Run partners alerts script
     */
    public function run()
    {
        $output = [];
        foreach ($this->sites as $site) {
            $desktopScore = $this->getScoreForSite($site, true);
            $mobileScore = $this->getScoreForSite($site, false);
            $this->saveScoresToDb($site, $desktopScore, $mobileScore);
            $output[] = "score for site $site desktop: $desktopScore and mobile: $mobileScore" . PHP_EOL;
        }

        return $output;
    }


    /*
     *  Saves the scores to DB
     */
    private function saveScoresToDb($siteUrl, $desktopScore, $mobileScore)
    {
        $spss = new SitesPageSpeedScore();
        $spss->site_url = $siteUrl;
        $spss->desktop_score = $desktopScore;
        $spss->mobile_score = $mobileScore;
        $spss->save();
    }

    /*
     *  Sets the Url
     */
    private function getPageSpeedUrl($siteUrl, $isDesktop = true)
    {
        $pagespeedUrl = 'https://www.googleapis.com/pagespeedonline/v4/runPagespeed?url=';
        $device = $isDesktop ? 'desktop' : 'mobile';
        $strategy = '&strategy=' . $device;
        $key = '&key=' . $this->pageSpeedKey;
        $pageSpeedSiteUrl = $pagespeedUrl . $siteUrl . $strategy . $key;

        return $pageSpeedSiteUrl;
    }


    public function fetch($content, $atts = null)
    {

        $emails = $this->option('emails') ? $this->option('emails') : false;
        if ($emails) {
            self::email($content, $emails);
        } else {
            self::email($content, config('mail.pagespeed_mail_alerts.username'));
        }
    }


    public static function email($content, $emails, $another_param = NULL)
    {
        Mail::tplBlank($emails, [
            'content' => $content,
            'title' => 'PageSpeed Scores Alert'
        ]);
    }


    /**
     * @param $siteUrl
     * @param bool $isDesktop
     * @return mixed
     *
     * Uses guzzle client to get the score for each site
     * waits 30 second if there is an error
     * trying each link max 3 times before skipping
     *
     */
    private function getScoreForSite($siteUrl, $isDesktop = true)
    {
        $pageSpeedSiteUrl = $this->getPageSpeedUrl($siteUrl, $isDesktop);
        $client = new Client();
        $score = "N/A";
        for ($i = 0; $i < 3; $i++) {
            try {
                $response = $client->get($pageSpeedSiteUrl);
                $pagespeedContentString = $response->getBody()->getContents();
                $pagespeedContent = json_decode($pagespeedContentString, true);
                $score = $pagespeedContent['ruleGroups']['SPEED']['score'];
                break 1;
            } catch (Exception $e) {
                sleep(30);
                echo("trying again site " . $siteUrl . PHP_EOL);
            }

        }
        return $score;
    }

    /**
     * Close connection
     */
    public function __destruct()
    {
    }
}