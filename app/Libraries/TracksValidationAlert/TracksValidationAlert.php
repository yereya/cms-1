<?php namespace App\Libraries\TracksValidationAlert;

use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Users\UserRepo;
use App\Libraries\TpLogger as tLog;
use DB;
use App\Libraries\Mail;

/**
 * Class TracksValidationAlert
 * @package App\Libraries\TracksValidationAlert
 */
class TracksValidationAlert
{

    private static $system_id = 108;

    /**
     * @var TpLogger
     */
    private static $logger;

    /**
     * @var \Illuminate\Database\Connection
     */
    private static $dwh_connection;

    /**
     * @var \Illuminate\Database\Connection
     */
    private static $out_connection;

    /**
     * Fetch
     *
     * @param array $atts
     *
     * @return mixed
     * @internal param $email
     */

    public static function fetch($atts = [])
    {
        $atts = filterAndSetParams([
            'email'               => false,
            'email_by_permission' => false
        ], $atts);

        static::$logger = tLog::getInstance();
        static::$logger->setSystem(static::$system_id);

        try {
            // General script settings
            static::$out_connection = DB::connection('putin');
            static::$dwh_connection = DB::connection('dwh');
            $ids_difference = self::retrieveTrackDifference();

            if (!$ids_difference) {
                static::$logger->info(['All ids have been fetched from Out to DWH! Yay!']);
            } else {
                static::$logger->info(['Ids difference result: ' => $ids_difference]);
            }

            // Close active connections
            self::closeConnections();
            $date_format = 'Y-m-d 00:00:00';
            $body_str = '[' . date($date_format, strtotime('last day')) . ' TO ' . date($date_format) . ']<br/>';
            $body_str .= implode('<br/>', array_flip($ids_difference));

            // Begin mailing process
            self::alertMailailToRecipients($body_str, $atts);
        } catch(\Exception $e) {
            static::$logger->error(['Could not complete the validation. Reason: ', $e->getMessage()]);
            self::closeConnections();
        }
    }

    /**
     * Returns the difference between out and dwh track ids
     *
     * @param string $date_format
     * @return array
     */
    private static function retrieveTrackDifference($date_format = 'Y-m-d 00:00:00')
    {
        // Perform the query to retrieve DWH fact tracks from the last 24 hours
        $dwh_track_ids_list = static::$dwh_connection->table('dwh_fact_tracks')->select('track_id')
            ->where('src_updated', '>', date($date_format, strtotime('last day')))
            ->where('src_updated', '<=', date($date_format))->get()->pluck('track_id')->toArray();

        // Perform the query to retrieve DWH fact deleted tracks from the last 24 hours
        $dwh_deleted_track_ids_list = static::$dwh_connection->table('dwh_deleted_tracks_rows')->select('track_id')
            ->where('tracks_updated', '>', date($date_format, strtotime('last day')))
            ->where('tracks_updated', '<=', date($date_format))->get()->pluck('track_id')->toArray();

        // Union deleted_tracks with tracks from dwh
        $dwh_merged_tracks_and_deleted = array_merge($dwh_track_ids_list, $dwh_deleted_track_ids_list);

        // Perform the query to retrieve dwh_trackId_underHighWaterMark
        $dwh_trackId_underHighWaterMark = static::$dwh_connection->table('dwh_trackId_underHighWaterMark')->select('track_id')
            ->get()->pluck('track_id')->toArray();

        // Union dwh_trackId_underHighWaterMark with tracks and deleted tracks from dwh
        $dwh_merged_tracks_and_deleted = array_merge($dwh_merged_tracks_and_deleted, $dwh_trackId_underHighWaterMark);

        // Perform the query to retrieve OUT(new) tracks from the last 24 hours
        $out_track_ids_list = static::$out_connection->table('tracks')->select('id')
            ->where('updatedAt', '>', date($date_format, strtotime('last day')))
            ->where('updatedAt', '<=', date($date_format))->get()->pluck('id')->toArray();
        // Find the difference between the two ids list
        return array_except(array_flip($out_track_ids_list), $dwh_merged_tracks_and_deleted);
    }

    /**
     * Sends the ids difference results to the mail recipients
     *
     * @param $result
     * @param $atts
     */
    private static function alertMailailToRecipients($result, $atts)
    {
        // Check if one of the email options is set
        if ($result && ($atts['email_by_permission'] || $atts['email'])) {

            // Check if option email_by_permission is set and there's records
            if ($atts['email_by_permission']) {
                // build collection from the results
                $collection_result = collect($result);
                $users_repo        = (new UserRepo());
                $account_repo      = (new AccountsRepo());
                $variable_to_field = [
                    'account_names'    => 'account_name',
                    'advertiser_names' => 'advertiser_name'
                ];

                /*Build variables dynamic */
                foreach ($variable_to_field as $variable => $field) {
                    ${$variable} = $collection_result->filter(function ($record) use ($field) {
                        if (array_key_exists($field, $record)) {
                            return $record[$field];
                        }

                        return false;
                    });
                }

                /*If account names exist in the results*/
                if (count($account_names)) {
                    $accounts_id = $account_repo->getByName($account_names)->pluck('id');
                }

                /*find users that have these accounts and get there mails*/
                $emails = $users_repo->getEmailsByAccountsId($accounts_id);

                /*Transform email collection to string*/
                $string_emails = $emails->implode(',');

                /*if --email passed concat it to the permitted emails*/
                if ($atts['email']) {
                    $string_emails .= ',' . $atts['email'];
                }

                self::email($result, $string_emails);

            } else {

                // Email the results when an email is passed
                self::email($result, $atts['email']);
            }
        }
    }

    /**
     * Closes all active connections
     */
    public static function closeConnections()
    {
        self::$dwh_connection->disconnect();
        self::$out_connection->disconnect();
    }

    /**
     * email
     *
     * @param $result
     * @param $email
     */
    private static function email($result, $email)
    {
        $title = 'Report: Tracks Ids in db_out.tracks that are missing from dwh.dwh_fact_tracks';

        Mail::tplBlank($email, [
            'content' => $result,
            'title'   => $title
        ]);
    }

    /**
     * Validate Query Result
     *
     * @param $query_id
     * @param $query
     *
     * @return bool
     */
    private static function validateQueryResult($query_id, $query)
    {


        /** @var array $countable_query */
        if (!static::resultsExist($query)) {
            static::$logger->error('Query id ' . $query_id . ' could not be found');
            return false;
        }

        if (isset($query['query']) && !$query['query']) {
            static::$logger->error('Query id ' . $query_id . ' does not have valid query');
            return false;
        }

        return true;
    }

    /**
     * @param $query
     *
     * @return int
     */
    private static function resultsExist($query)
    {
        $countable_query = [];

        if (is_object($query)) {
            $countable_query = $query->toArray();
        }

        if (is_array($query)) {
            $countable_query = $query;
        }

        return count($countable_query);
    }
}
