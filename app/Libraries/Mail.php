<?php namespace App\Libraries;

use Mail as BaseMail;

//use PhpImap\Mailbox as ImapMailbox;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\MailExtended as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;


/**
 * Class EmailLib
 *
 * @package App\Console\Commands\ReportsScraper\Providers
*/
class Mail extends BaseMail
{

    /**
     * @var int System ID
     */
    private static $system_id = 37;

    /**
     * @var \Logger logger
     */
    private static $logger;


    /**
     * Template Blank
     *
     * @param $email
     * @param array $view_params
     * @param string $subject
     * @param null $name
     * @param string $view_name
     *
     *
     * @return bool
     */
    public static function tplBlank($email, $view_params, $subject = null, $name = null, $view_name = 'emails.tp-blank')
    {

        $name = (isset($name)) ? $name : $email;

        $description = isset($view_params['description']) ? $view_params['description'] : '';

        // Set default values
        $view_params = filterAndSetParams([
            'content' => '',
            'title' => $subject
        ], $view_params);

        $subject = isset($subject) ? $subject : $view_params['title'];
        $email = self::validateEmail($email);

        // Build the content into tables when it is an array
        if (is_array($view_params['content'])) {
            $view_params['content'] = $description . array2table($view_params['content'], true, true, true);
        } elseif (is_object($view_params['content'])) {
            $view_params['content'] = $description . array2table(object2Array($view_params['content'], true, true));
        }else{
            $view_params['content'] = $description . $view_params['content'];
        }

        self::send($view_name, $view_params, function($message) use ($email, $name, $subject) {
            $message->to($email, $name)->subject($subject);
        });

        return (!self::failures()) ? true: false;
    }
    /**
     * Template With attachment
     *
     * @param $email
     * @param array $view_params
     * @param string $pathToFile
     * @param string $subject
     * @param null $name
     * @param string $view_name

     *
     * @return bool
     */
    public static function tplAttachment($email, $view_params, $pathToFile ='' , $subject = null, $name = null, $view_name = 'emails.tp-blank')
    {

        $name = (isset($name)) ? $name : $email;

        $description = isset($view_params['description']) ? $view_params['description'] : '';

        // Set default values
        $view_params = filterAndSetParams([
            'content' => '',
            'title' => $subject,
        ], $view_params);

        $subject = isset($subject) ? $subject : $view_params['title'];
        $email = self::validateEmail($email);
        $view_params['content'] = $description;

        self::send($view_name, $view_params, function($message) use ($email, $name, $subject,$pathToFile) {
            $message->to($email, $name)->subject($subject);
            $message->attach($pathToFile,['as' => 'Report.csv']);
        });

        \File::delete($pathToFile);

        return (!self::failures()) ? true: false;
    }

    /**
     * Template Reset Password
     *
     * @param $email
     * @param null $name
     *
     * @return bool
     */
    public static function tplResetPassword($email, $name = null)
    {
        $name = (isset($name)) ? $name : $email;
        $email = self::validateEmail($email);

        self::send('emails.password-reset', [], function($message) use ($email, $name) {
            $message->to($email, $name)->subject('Traffic Point - Password reset');
        });

        return (!self::failures()) ? true: false;
    }


    /**
     * Validate Email
     * Breaks the email string into array when several emails provided, comma delimited
     *
     * @param $email
     *
     * @return array
     */
    private static function validateEmail($email)
    {
        // Turn to array of emails when comma separated emails provided
        return strpos($email, ',') ? explode(',', $email) : $email;
    }


    /**
     * Fetch
     *
     * @param $params
     *
     * @return array|null
     * @throws \PhpImap\Exception
     */
    public static function fetch($params)
    {
        $params = filterAndSetParams([
            'label'   => 'INBOX',
            'criteria' => 'UNSEEN',
            'limit'    => 3,
        ], $params);

        // Connect to the mailbox
        $imap_path = '{' . env('MAIL_HOST_IMAP') . '}' . $params['label'];
        $mailbox   = new ImapMailbox($imap_path, env('MAIL_USERNAME'), env('MAIL_PASSWORD'), self::getStoragePath());

        // Read messages according to the passed criteria
        $email_ids = $mailbox->searchMailbox($params['criteria']);

        if (!$email_ids) {
            return null;
        }

        $emails = [];
        foreach ($email_ids as $email_id) {
            if ($params['limit']-- <= 0) {
                break;
            }

            // Get the first message and save its attachment(s) to disk
            $emails[$email_id] = $mailbox->getMail($email_id);
        }

        return $emails;
    }


    /**
     * Init
     */
    public static function init()
    {
//        self::$logger = new TpLogger();
//        self::$logger->setSystem(self::$system_id);
    }


    /**
     * Get Storage Path
     *
     * @return string
     */
    private static function getStoragePath()
    {
        $storage_path = storage_path('app/email-attachments');
        if (!file_exists($storage_path)) {
            mkdir($storage_path, 0777, true);
        }

        return $storage_path;
    }
}
Mail::init();
