<?php namespace App\Libraries\TemplateBuilder;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Templates\Column;
use App\Entities\Models\Sites\Templates\Item;
use App\Entities\Models\Sites\Templates\Row;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Sites\Templates\WidgetData;
use Illuminate\Support\Collection;

class BuilderLib
{
    /**
     * @var Template $template
     */
    protected $template;

    /**
     * @var Collection $column_items
     */
    protected $rows;

    /**
     * @var Collection $column_items
     */
    protected $columns;

    /**
     * @var Collection $column_items
     */
    protected $column_items;

    /**
     * @var Collection $widgets_data
     */
    protected $widgets_data;

    /**
     * @var Collection $grid
     */
    protected $grid;

    /**
     * Builder constructor.
     *
     * @param Template $template
     */
    public function __construct(Template $template)
    {
        $this->template = $template;
    }

    /**
     * Save
     *
     * @param array $grid
     */
    public function save(array $grid)
    {
        $this->storeRows($grid);

        $this->template->touch();
    }

    /**
     * Store Rows
     *
     * @param array $rows
     * @param bool  $return_single
     *
     * @return Row|null
     */
    private function storeRows(array $rows, $return_single = false)
    {
        foreach ($rows as $row) {
            $row_model = $this->getRowModel($row);

            if (!$this->isInherited($row)) {
                if ($this->isDeleted($row)) {
                    $row_model->delete();

                    if ($row_model->item) {
                        $row_model->item->delete();
                    }
                    continue;
                }

                $row_model->save();
                $row_model->abTests()
                    ->sync($row['ab_tests_ids'] ?? []);
            }

            if (!empty($row['columns'])) {
                $this->storeColumns($row['columns'], $row_model);
            }

            if ($return_single) {
                return $row_model;
            }
        }
    }

    /**
     * Get Row Model
     * if row not exists - create new row (do not save, only create model)
     *
     * @param array $row
     *
     * @return Row
     */
    private function getRowModel(array $row)
    {
        if ($this->isCreated($row)) {
            $row_model              = new Row($row);
            $row_model->template_id = $this->template->id;
        } else {
            $row_model = Row::findOrFail($row['id']);
            $row_model->fill($row);
        }

        return $row_model;
    }

    /**
     * Is Created
     *
     * @param $item
     *
     * @return bool
     */
    private function isCreated($item)
    {
        return isset($item['created']) && $item['created'] == 'true';
    }

    /**
     * Is Inherited
     *
     * @param $item
     *
     * @return bool
     */
    private function isInherited($item)
    {
        return isset($item['inherited']) && $item['inherited'] == 'true';
    }

    /**
     * Is Deleted
     *
     * @param $item
     *
     * @return bool
     */
    private function isDeleted($item)
    {
        return isset($item['deleted']) && $item['deleted'] == 'true';
    }

    /**
     * Store Data Columns
     *
     * @param array $columns
     * @param Row   $row
     */
    private function storeColumns(array $columns, Row $row)
    {
        foreach ($columns as $column) {
            $column_model = $this->getColumnModel($column, $row);

            if (!$this->isInherited($column)) {
                if ($this->isDeleted($column)) {
                    $column_model->delete();
                    continue;
                }

                $column_model->save();
            }

            if (!empty($column['items'])) {
                $this->storeItems($column['items'], $column_model);
            }
        }
    }

    /**
     * Get Column Model
     * if column not exists - create new column (do not save, only create model)
     *
     * @param array $column
     * @param Row   $row
     *
     * @return Column
     */
    private function getColumnModel(array $column, Row $row)
    {
        if ($this->isCreated($column)) {
            $column_model              = new Column($column);
            $column_model->row_id      = $row->id;
            $column_model->template_id = $this->template->id;
        } else {
            $column_model = Column::findOrFail($column['id']);
            $column_model->fill($column);
        }

        return $column_model;
    }

    /**
     * Store Data Columns
     *
     * @param array  $items
     * @param Column $column
     */
    private function storeItems(array $items, Column $column)
    {
        foreach ($items as $item) {
            $item_model = $this->getItemModel($item, $column);

            if ($item_model->element_type == 'row' && isset($item['instance'])) {
                if ($row = $this->storeRows([$item['instance']], true)) {
                    $item_model->element_id = $row->id;
                }
            }

            if ($this->isInherited($item)) {
                continue;
            }

            if ($this->isDeleted($item)) {
                $item_model->delete();
                continue;
            }

            $item_model->save();
        }
    }

    /**
     * Get Item Model
     * if item not exists - create new item (do not save, only create model)
     *
     * @param array  $item
     * @param Column $column
     *
     * @return Item
     */
    private function getItemModel(array $item, Column $column)
    {
        if ($this->isCreated($item)) {
            $item_model              = new Item($item);
            $item_model->column_id   = $column->id;
            $item_model->template_id = $this->template->id;
        } else {
            $item_model = Item::findOrFail($item['id']);
            $item_model->fill($item);
        }

        return $item_model;
    }

    /**
     * Get Grid
     *
     * @return Collection
     */
    public function getGrid()
    {
        $this->collectGrid();

        return $this->grid;
    }

    /**
     * Collect Grid
     */
    private function collectGrid()
    {
        $this->grid = collect();

        $this->collectElements();
        $this->collectRootRows();

        $this->matchElementsCollector($this->grid);
    }

    /**
     * Collect Elements
     */
    private function collectElements()
    {
        $parents_id = $this->getAllParentsId($this->template);

        $this->rows         = $this->getAllRows($parents_id);
        $this->columns      = $this->getAllColumns($parents_id);
        $this->column_items = $this->getAllColumnItems($parents_id);
        $this->widgets_data = $this->getAllWidgetsData($this->column_items);
    }

    /**
     * Get All Parents ID
     *
     * @param Template $template
     * @param array    $ids
     *
     * @return array
     */
    public function getAllParentsId(Template $template, &$ids = [])
    {
        if ($parent = $template->parent) {
            $this->getAllParentsId($parent, $ids);
        }

        $ids[] = $template->id;

        return $ids;
    }

    /**
     * Get All Rows
     *
     * @param array $templates_id
     *
     * @return Collection
     */
    private function getAllRows($templates_id)
    {
        return Row::whereIn('template_id', $templates_id)
            ->orderBy('order')
            ->get()
            ->keyBy('id');
    }

    /**
     * Get All Columns
     *
     * @param array $templates_id
     *
     * @return Collection
     */
    private function getAllColumns($templates_id)
    {
        return Column::whereIn('template_id', $templates_id)
            ->orderBy('order')
            ->get()
            ->keyBy('id');
    }

    /**
     * Get All Column Items
     *
     * @param array $templates_id
     *
     * @return Collection
     */
    private function getAllColumnItems($templates_id)
    {
        return Item::whereIn('template_id', $templates_id)
            ->orderBy('order')
            ->get()
            ->keyBy('id');
    }

    /**
     * Get All Widgets Data
     *
     * @param Collection $column_items
     *
     * @return Collection
     */
    private function getAllWidgetsData(Collection $column_items)
    {
        $ids = $column_items->where('element_type', 'widget')
            ->pluck('element_id');

        return WidgetData::whereIn('id', $ids)
            ->get()
            ->keyBy('id');
    }

    /**
     * Collect Root Rows
     */
    private function collectRootRows()
    {
        /** @var Row $row */
        foreach ($this->rows as $row) {
            // Find the row in column items
            $row_in_pivot = $this->column_items->filter(function ($item) use ($row) {
                return $item->element_type == 'row' && $item->element_id == $row->id;
            });

            // if it the row does not exists in the column items this would mean that this is a base row
            if ($row_in_pivot->count()) {
                continue;
            }

            $row->inherited = $this->template->id != $row->template_id;

            $row->load('abTests');
            $row->ab_tests_ids = $row->abTests->pluck('id');

            $this->grid->put($row->id, $row);
            $this->rows->forget($row->id);
        }
    }

    /**
     * Match Elements Collector
     * Walks over the grid and tries to match the type of the element. Executes the proper collector depending on it.
     *
     * @param Collection $grid
     *
     * @return Collection
     */
    private function matchElementsCollector(Collection $grid)
    {
        foreach ($grid as $element_key => $element) {
            if (is_a($element, Row::class)) {
                $this->collectColumns($element);
            } else {
                if (is_a($element, Column::class)) {
                    $this->collectColumnItems($element);
                } else {
                    if (is_a($element, Item::class) && $element->element_type == 'row' && $element->instance) {
                        $this->collectColumns($element->instance);
                    }
                }
            }
        }

        return $grid;
    }

    /**
     * Collect Columns
     *
     * @param Row $row
     */
    private function collectColumns(Row $row)
    {
        $columns = collect();
        foreach ($this->columns as $column) {
            if ($column->row_id == $row->id) {
                $column->inherited = $this->template->id != $column->template_id;

                $columns->put($column->id, $column);
                $this->columns->forget($column->id);
            }
        }

        $row->columns = $this->matchElementsCollector($columns);
    }

    /**
     * Collect Column Items
     *
     * @param Column $column
     */
    private function collectColumnItems(Column $column)
    {
        $items = collect();
        foreach ($this->column_items as $item) {
            if ($item->column_id == $column->id) {
                $item->inherited = $this->template->id != $item->template_id;

                if ($item->element_type == 'row') {
                    $item->instance = $this->rows->where('id', $item->element_id)
                        ->first();
                } else {
                    if ($item->element_type == 'widget') {
                        $item->instance = $this->widgets_data->where('id', $item->element_id)
                            ->first();
                        $item->instance->append('html');
                    }
                }

                $items->put($item->id, $item);
                $this->column_items->forget($item->id);
            }
        }

        $column->items = $this->matchElementsCollector($items);
    }

    /**
     * Get Assets
     *
     * @param Site $site
     *
     * @return array
     */
    public function getAssets(Site $site)
    {
        $DS = DIRECTORY_SEPARATOR;

        return array_merge(
            $this->findAssetsByExtension('top-assets' . $DS . 'sites' . $DS . $site->id . $DS . 'js', 'js'),
            $this->findAssetsByExtension('top-assets' . $DS . 'sites' . $DS . $site->id . $DS . 'css/', 'css')
        );
    }

    /**
     * Find Assets By Extension
     *
     * @param $folder
     * @param $extension
     *
     * @return array
     */
    private function findAssetsByExtension($folder, $extension)
    {
        $path = public_path($folder);

        $resources = [];
        if (\File::exists($path)) {
            /** @var \SplFileInfo $file */
            foreach (\File::allFiles($path) as $file) {
                if ($file->getExtension() == $extension) {
                    $resources[] = asset($folder . DIRECTORY_SEPARATOR . $file->getRelativePathname());
                }
            }
        }
        return $resources;
    }
}