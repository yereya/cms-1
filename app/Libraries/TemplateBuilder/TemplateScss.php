<?php namespace App\Libraries\TemplateBuilder;

use File;
use Illuminate\Support\Collection;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Templates\Row;
use App\Entities\Models\Sites\Templates\Item;
use App\Entities\Models\Sites\Templates\Column;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Repositories\Sites\TemplateRepo;
use App\Libraries\TemplateBuilder\BuilderLib as TemplateBuilder;
use App\Entities\Repositories\Sites\Widgets\SiteWidgetTemplateRepo;

class TemplateScss
{

    /**
     * Hold the base template and all his parents.
     *
     * @ver Collection
     */
    protected $templates;

    /**
     * Holds the collective style content.
     *
     * @ver array
     */
    protected $content;

    /**
     * Holds the current site object.
     *
     * @ver Site
     */
    protected $site;

    /**
     * @param integer $template_id
     * @param Site    $site
     */
    public function __construct(int $template_id, Site $site)
    {
        $base_template = (new TemplateRepo)->model()->findOrFail($template_id);

        $this->site      = $site;
        $this->content   = [];
        $this->sassLib   = new ScssLib($site);
        $this->templates = new Collection();

        $this->templates->push($base_template);

        $this->getParentsOf($this->templates->first());
    }

    /**
     * Get the parents templates recursively.
     *
     * @param Template $template
     *
     * @return Collection
     */
    protected function getParentsOf($template)
    {
        if ($parent = $template->parent) {
            $this->templates[] = $parent;

            return $this->getParentsOf($parent);
        }

        return $this->templates;
    }

    /**
     * Parse items of a given column.
     *
     * @param Column $column
     *
     * @return mixed
     */
    public function handleColumn(Column $column)
    {
        return $column->items->each([$this, 'handleItem']);
    }

    /**
     * Parse columns of a given row.
     *
     * @param Row $row
     *
     * @return mixed
     */
    public function handleRow(Row $row)
    {
        return $row->columns->each([$this, 'handleColumn']);
    }

    /**
     * Parse instance of a given item.
     *
     * @param mixed $item
     *
     * @return mixed
     */
    public function handleItem(Item $item)
    {
        if ($item->instance->columns) {
            $item->instance->columns->each([$this, 'handleColumn']);
        }
        if ($item->instance->widget_template_id) {
            $blade_template = (new SiteWidgetTemplateRepo)->find($item->instance->widget_template_id);
            if ($blade_template) {
                $this->content[] = $blade_template->scss;
            }
        }
        $this->content[] = $item->instance->scss;
    }

    /**
     * Generate a css file for the selected base template.
     *
     * @param boolean $raw_output
     *
     * @return mixed
     */
    public function generate($raw_output = false)
    {
        $input_file  = $this->getFilename('scss');
        $output_file = $this->getFilename('css');

        // Store the resolved content for node.
        File::put($input_file, $this->resolveContent());

        // Compile scss.
        $output = $this->sassLib->executeScssCompile($input_file, $output_file);

        // Delete the scss input file.
        File::delete($input_file);

        if ($raw_output) {

            return $this->getRawOutput($output_file);
        }

        return $output;
    }

    /**
     * Get the raw css output of a given file path.
     *
     * @param string $file
     *
     * @return string
     */
    protected function getRawOutput($file){
        if (!File::exists($file)) {

            return '/** output file not found. **/';
        }

        // Read the output file content.
        $css_content = File::get($file);

        // Delete the output file.
        File::delete($file);

        return $css_content;
    }

    /**
     * Resolve the style file content.
     *
     * @return string
     */
    protected function resolveContent()
    {
        $this->templates->each(function ($template) {
            $builder         = new TemplateBuilder($template);
            $grid            = $builder->getGrid();
            $this->content[] = $template->scss;
            $grid->each([$this, 'handleRow']);
        });

        $placeholder = "%s \n@import \"main.scss\";\n@import \"compass\"; \n\n %s \n\n %s";

        $custom_file_content = '';
        $custom_style_file   = resource_path('assets/sass/top/sites/' . $this->site->id . '/style.scss');

        if (File::exists($custom_style_file)) {
            $custom_file_content = File::get($custom_style_file);
        }

        return sprintf($placeholder, $this->sassLib->getDynamicVars(), $custom_file_content, join($this->content));
    }

    /**
     * Get the full path for a style file.
     *
     * @param string $suffix
     *
     * @return string
     */
    protected function getFilename($suffix = 'css')
    {
        $placeholder = sprintf('template_%s.%s', $this->templates->first()->id, $suffix);

        return $this->sassLib->getCssSaveSitePath($placeholder);
    }

}