<?php

namespace App\Libraries\TemplateBuilder;


use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\TemplateRepo;
use App\Entities\Repositories\Sites\Widgets\SiteWidgetTemplateRepo;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use Storage;

class ScssLib
{
    protected $site;

    protected $temp_scss_filename;
    protected $temp_scss_full_path;


    /**
     * ScssLib constructor.
     *
     * @param Site $site
     */
    public function __construct(Site $site)
    {
        $this->site                = $site;
        $this->temp_scss_filename  = 'temp_scss/' . $site->id . ".scss";
        $this->temp_scss_full_path = storage_path('app/' . $this->temp_scss_filename);
        $this->setCssSaveSitePath();
    }

    /**
     * @return string
     */
    public function getTempScssContent()
    {
        return Storage::get($this->temp_scss_filename);
    }

    /**
     * compile
     *
     * @param string $formatting
     * @param null   $temp_output
     *
     * @return $this
     */
    public function compile($formatting = 'dev', $temp_output = null)
    {
        $scss_str = "\n/*@@@ Site SASS FILES */\n";
        $scss_str .= $this->site->getAllScss();
        $scss_str .= "\n/* @@@ Start of templates Scss */\n";
        $scss_str .= (new TemplateRepo())->getAllScss();
        $scss_str .= "\n/* @@@ Start of cms.site_widget_templates Scss */\n";
        $scss_str .= $temp_output
            ? (new SiteWidgetTemplateRepo())->getAllScss()
            : (new SiteWidgetTemplateRepo())->getGeneralScss();
        $scss_str .= "\n/* @@@ Start of widget_data Scss */\n";
        $scss_str .= (new WidgetDataRepo())->getAllSiteScss();

        return $this->compileScss($scss_str, $formatting, $temp_output);
    }

    /**
     * Compile Template Scss To Css
     *
     * @param      $scss_str
     * @param      $formatting
     * @param null $temp_output
     *
     * @return string - error if there is any
     */
    private function compileScss($scss_str, $formatting, $temp_output = null)
    {
        $dynamic_vars = $this->getDynamicVars();
        $scss_str     = $dynamic_vars . "@import \"main.scss\";\n@import \"compass\"\n;" . $scss_str;

        Storage::put($this->temp_scss_filename, $scss_str);

        return $this->runCompileCommand($this->temp_scss_full_path, $temp_output);
    }

    /**
     * Get Dynamic Vars
     */
    public function getDynamicVars()
    {
        $paths        = config('scss-dynamic-vars.paths');
        $dynamic_vars = '';
        foreach ($paths as $key => $path) {
            $path         = str_replace('{site_id}', $this->site->id, $path);
            $dynamic_vars .= "$key: '$path'; \n";
        }

        return $dynamic_vars;
    }

    /**
     * Run Compile Command
     *
     * @param string $input_file
     * @param null   $temp_output
     *
     * @return string
     * @internal param null $temp_scss
     */
    private function runCompileCommand($input_file,$temp_output = null)
    {
        $file_name = $temp_output ?? 'main.css';
        return $this->executeScssCompile($input_file, $this->getCssSaveSitePath($file_name));
    }

    /**
     * Run a scss compiling script.
     *
     * @param string $input_file
     * @param string $output_file
     *
     * @return mixed
     */
    public function executeScssCompile(string $input_file, string $output_file)
    {
        $output_style = $this->resolveOutputStyle();

        $node_path = env('NODE_PATH');
        $cmd       = $node_path . ' ' . base_path('scss-compile.js');
        $cmd       .= " --outputStyle $output_style";
        $cmd       .= ' --inputFile ' . $input_file;
        $cmd       .= ' --outputFile ' . $output_file;
        $cmd       .= ' --includePath ' . resource_path('/assets/sass/top/');

        return shell_exec($cmd);
    }

    /**
     * getSaveSiteFilePath
     *
     * @param string $filename
     *
     * @return string
     */
    public function getCssSaveSitePath(string $filename = '')
    {
        return public_path('top-assets' . DIRECTORY_SEPARATOR . 'sites' . DIRECTORY_SEPARATOR . $this->site->id . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . $filename);
    }

    /**
     * Resolve the css output format.
     *
     * @return string
     */
    private function resolveOutputStyle()
    {
        $application_environment = env('APP_ENV');
        $production_level_names  = ['staging', 'production'];

        $output_style = in_array($application_environment, $production_level_names) ? 'compressed' : 'nested';

        if (request()->has('production')) {
            $output_style = 'compressed';
        }

        return $output_style;
    }

    /**
     * Create Css Save File
     * - if there's no source file.
     * - create one.
     */
    private function setCssSaveSitePath()
    {
        $dir = public_path('top-assets' . DIRECTORY_SEPARATOR . 'sites' . DIRECTORY_SEPARATOR . $this->site->id .
            DIRECTORY_SEPARATOR . 'css');

        if (!file_exists($dir)) {
            mkdir(dirname($dir), 0777, true);
        }
    }
}