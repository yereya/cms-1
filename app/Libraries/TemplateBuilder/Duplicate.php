<?php

namespace App\Libraries\TemplateBuilder;

use App\Entities\Models\Sites\Templates\Column;
use App\Entities\Models\Sites\Templates\Item;
use App\Entities\Models\Sites\Templates\Row;
use App\Entities\Models\Sites\Templates\Template;

/**
 * Class Duplicate
 *
 * @package App\Libraries\TemplateBuilder
 */
class Duplicate
{
    /**
     * @var
     */
    private $original_template;

    /**
     * @var
     */
    private $new_template;

    /**
     * @var array
     */
    private $rows_map = [];

    /**
     * @var array
     */
    private $columns_map = [];

    /**
     * Duplicate Tree
     *
     * @param Template $template
     *
     * @return Template
     */
    public function duplicateTree(Template $template)
    {
        $new_template = $this->duplicateOne($template);

        if ($template->children) {
            foreach ($template->children as $child_template) {
                $duplicated_child_template            = $this->duplicateTree($child_template);
                $duplicated_child_template->parent_id = $new_template->id;
                $duplicated_child_template->save();
            }
        }

        return $new_template;
    }

    /**
     * Duplicate One
     *
     * @param Template $template
     *
     * @return Template
     */
    public function duplicateOne(Template $template)
    {
        $this->original_template = $template;

        $this->duplicateTemplate();
        $this->duplicateRows();
        $this->duplicateColumns();
        $this->duplicateItems();

        return $this->new_template;
    }

    /**
     * Duplicate Template
     */
    private function duplicateTemplate()
    {
        $this->new_template = $this->original_template->replicate();
        $this->new_template->name .= '_duplicate';
        $this->new_template->save();
    }

    /**
     * Duplicate Rows
     */
    private function duplicateRows()
    {
        $rows = Row::where('template_id', $this->original_template->id)
            ->get();

        foreach ($rows as $row) {
            $new_row              = $row->replicate();
            $new_row->template_id = $this->new_template->id;
            $new_row->save();
            $this->rows_map[$row->id] = $new_row->id;
        }
    }

    /**
     * Duplicate Columns
     */
    private function duplicateColumns()
    {
        $columns = Column::where('template_id', $this->original_template->id)
            ->get();

        foreach ($columns as $column) {
            $new_column              = $column->replicate();
            $new_column->template_id = $this->new_template->id;

            if (isset($this->rows_map[$column->row_id])) {
                $new_column->row_id = $this->rows_map[$column->row_id];
            }

            $new_column->save();
            $this->columns_map[$column->id] = $new_column->id;
        }
    }

    /**
     * Duplicate Items
     */
    private function duplicateItems()
    {
        $items = Item::where('template_id', $this->original_template->id)
            ->get();

        foreach ($items as $item) {
            $new_item = $item->replicate();
            $new_item->template_id = $this->new_template->id;

            if (isset($this->columns_map[$item->column_id])) {
                $new_item->column_id = $this->columns_map[$item->column_id];
            }

            if ($new_item->element_type == 'row') {
                if (isset($this->rows_map[$new_item->element_id])) {
                    $new_item->element_id = $this->rows_map[$new_item->element_id];
                }
            }

            $new_item->save();
        }
    }
}
