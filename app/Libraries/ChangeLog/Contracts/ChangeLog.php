<?php namespace App\Libraries\ChangeLog\Contracts;

interface ChangeLog
{
    /**
     * Returns the Change subject level
     * Helps with figuring out at which level and which actions we can do at that level
     *
     * @param $filters
     *
     * @return mixed
     */
    public static function getChangeLevel(array $filters);
}