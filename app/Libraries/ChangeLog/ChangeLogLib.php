<?php namespace App\Libraries\ChangeLog;

use App\Entities\Models\Mrr\MrrChangeLogHistory;
use App\Entities\Models\Mrr\MrrChangeLogState;
use App\Libraries\ChangeLog\Contracts\ChangeLog;
use App\Libraries\PublisherEditor\PublisherEditorLib;
use App\Libraries\TpLogger;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Session;
use Validator;

class ChangeLogLib implements ChangeLog
{

    protected static $change_levels_actions = [
        'keyword'  => [
            'bid',
            'status',
            'comment'
        ],
        'ad_group' => [
            'status',
            'bid_adjustment',
            'comment'
        ],
        'campaign' => [
            'status',
            'budget',
            'scheduling',
            'location',
            'campaign_settings',
            'site_link',
            'bid_adjustment',
            'comment'
        ],
        'account'  => [
            'site_link',
            'comment'
        ],
    ];

    /**
     * @var TpLogger
     */
    protected static $logger;

    /**
     * @var array
     */
    protected static $change_attributes = [];

    /**
     * @var Collection
     */
    protected static $filtered_records;

    /**
     * Holds the collection of all collection
     * which are returned from the user filtered results
     * @var Collection
     */
    protected static $history_candidate_records;

    /**
     * Holds the save subjects with the save result
     * @var Array
     */
    protected static $history_subjects_records;

    /**
     * @var bool
     */
    protected static $failed = false;

    protected static $session_name = 'change_log_lib';


    /**
     * Handle Action
     *
     * @param array $request
     */
    public static function handleAction(array $request)
    {
        self::$logger = TpLogger::getInstance();
        self::cleanSession();

        // Build change attributes
        self::$change_attributes['type']            = $request['change_type'];
        self::$change_attributes['level']           = self::getChangeLevel($request['filters']);
        self::$change_attributes['new_value']       = self::getNewStatusByChangeLevel($request);
        self::$change_attributes['selections']      = self::getFilteredSelectionOptions($request['filters']['checkbox'] ?? null);
        self::$change_attributes['value_format']    = self::getActionValueFormat($request);
        self::$change_attributes['value_operation'] = self::getActionValueOperation($request);
        self::$history_candidate_records            = self::getHistoryCollection($request); //grouped filtered results

        self::sessionPutConcat('attributes', self::$change_attributes);

        // TODO temporary - disabled due to request
        /* if (!self::handlePublishersApi($request)) {
            // TODO handle the failed results

            // 1. Save the the new values
            // 2. Find them inside the $history_candidate_records
            //    and turn error flag to TRUE

            self::$failed = true;

            return false;
        } */

        // Handles the history change registration
        self::handleHistoryRegistration($request);
    }


    /**
     * @return Array
     */
    public static function getHistory()
    {
        return self::$history_subjects_records;
    }


    public static function getDetailsById($id)
    {
        return MrrChangeLogHistory::where('id', $id)->get();
    }


    /**
     * Returns the change level at which you are working
     * This later on will allow you to decide the actions you are able to do.
     *
     * @param array $filters
     *
     * @return string
     * @throws \Exception
     */
    public static function getChangeLevel(array $filters)
    {
        if (isset($filters['keywords']) && !empty($filters['keywords'])) {
            return 'keyword';
        } elseif (isset($filters['ad_group']) && !empty($filters['ad_group'])) {
            return 'keyword';
        } elseif (isset($filters['campaign']) && !empty($filters['campaign'])) {
            return 'ad_group';
        } elseif (isset($filters['account']) && !empty($filters['account'])) {
            return 'campaign';
        } else {
            throw new \Exception('Wrong filters passed.');
        }
    }


    /**
     * @param $level
     *
     * @return mixed
     */
    public static function getAvailableActions($level)
    {
        return self::$change_levels_actions[$level];
    }


    /**
     * Set Filters
     *
     * @param array $filters
     */
    public static function setFilters(array $filters)
    {
        self::cleanFilters($filters);
        self::$change_attributes['level'] = self::getChangeLevel($filters);
    }

    /**
     * Get Recent Changes
     * get $limit recent changes for given $user_id user
     *
     * @param $user_id
     * @param $limit
     *
     * @return mixed
     */
    public static function getRecentChanges($user_id, $limit = 1)
    {
        return MrrChangeLogHistory::where('user_id', $user_id)
            ->orderby('created_at', 'DESC')
            ->limit($limit)
            ->get();
    }


    /**
     * @param $request
     *
     * @return bool
     */
    private static function handlePublishersApi($request)
    {
        self::$filtered_records = self::getAllFilteredRecords($request);

        PublisherEditorLib::operation(self::$filtered_records, self::$change_attributes);
        if (PublisherEditorLib::getFails()) {
            // TODO handle the fails
            PublisherEditorLib::getCollection();
            return false;
        }

        return true;
    }


    /**
     * Clears the filters extra fields
     *
     * @param $filters
     */
    private static function cleanFilters(&$filters)
    {
        array_pull($filters, '_token');
        array_pull($filters, 'method');
    }


    public static function renderModalActionView(Array $request)
    {
        // TODO Validation

        $change_type          = $request['action'];
        $change_level         = self::getChangeLevel($request);
        $operations_list      = self::getOperationsList($change_type);
        $value_operation_list = $operations_list['operation'];
        $value_type_list      = $operations_list['type'];

        return view('pages.changes-log.modals.new-change')
            ->with('change_type', $change_type)
            ->with('change_level', $change_level)
            ->with('value_operation_list', $value_operation_list)
            ->with('value_type_list', $value_type_list)
            ->with('filters', $request)
            ->render();
    }


    public static function validateStoreRequestArguments($request_args)
    {
        $rules = [
            'follow_up_date' => "date",
        ];

        // Validate the passed params
        return Validator::make($request_args, $rules);
    }


    /**
     * Returns the change summary
     * Includes the successful and failed values
     *
     * @param bool $user_friendly
     *
     * @return array|null
     */
    public static function getChangeSummary($user_friendly = true)
    {
        $session = Session::get(self::$session_name);

        if ($user_friendly && !empty($session['history_subjects_records'])) {
            $show_fields = [
                'advertiser_name',
                'publisher_name',
                'account_name',
                'campaign_name',
                'campaign_status',
                'campaign_budget',
                'campaign_device_bid_adjustment',
                'device',
                'ad_group_name',
                'ad_group_status',
                'ad_group_bid',
                'ad_group_device_bid_adjustment',
                'match_type',
                'keyword_name',
                'keyword_status',
                'keyword_bid',
                'ad',
                'change_type',
                'change_level',
                'old_value',
                'new_value',
                'failure_reason'
            ];

            foreach($session['history_subjects_records'] as &$item) {
                $item = arrayIntersectingFields($item, $show_fields);
            }
        }

        return $session['history_subjects_records'] ?? null;
    }


    /**
     * Query for the Change Log to get the candidate records
     * which fit the current change level
     *
     * @param $request
     *
     * @return bool|Collection
     */
    private static function handleHistoryRegistration($request)
    {
        //TODO this is a temporary fix, we need to find a way to do this with collections
        $change_candidates = self::$history_candidate_records->toArray();
        self::$logger->debug(['Change Candidates', $change_candidates]);

        $user_id = auth()->user()->id;
        foreach ($change_candidates as $change) {

            // The comments
            $change['change_type']     = $request['change_type'];
            $change['change_level']    = self::$change_attributes['level'];
            $change['user_id']         = $user_id;
            $change['comment']         = $request['comment'];
            $change['reason']          = $request['reason'];
            $change['expected_result'] = $request['expected_result'];
            $change['follow_up_date']  = $request['follow_up_date'];
            $change['old_value']       = self::getOldStatusByChangeLevel($change);
            $change['new_value']       = null;
            $change['failure_reason']  = '';

            // Calculate the new value for this record
            if (in_array(self::$change_attributes['type'], ['bid', 'budget', 'bid_adjustment'])) {
                $change['new_value'] = self::getCalculatedNewValue($change['old_value']);
            } else {
                $change['new_value'] = self::$change_attributes['new_value'];
            }

            if (self::isHistoryRecordRedundant($change)) {
                self::sessionPutConcat('history_subjects_records', $change);
                continue;
            }
            self::sessionPutConcat('history_subjects_records', $change);

            $change_log_history = new MrrChangeLogHistory($change);
            $change['db_saved'] = $change_log_history->save() ? 'Saved' : 'Failed';

            self::$history_subjects_records[] = $change;
        }

        self::$logger->info(['New created changes', self::$history_subjects_records]);
    }


    /**
     * @param $record
     *
     * @return bool
     */
    private static function isHistoryRecordRedundant(&$record)
    {
        // Ignore redundant records
        // Values are the same
        if (!is_null($record['old_value']) && !is_null($record['new_value'])
            && $record['old_value'] === $record['new_value']
        ) {
            // Used to display the records changed after page refresh
            $record['failure_reason'] = 'The old and new values are equal';
            self::$logger->notice(['Record skipped, no value change', $record]);

            return true;
        }

        return false;
    }


    private static function getHistoryCollection($request)
    {
        $query = self::buildQueryByFilters($request, true);

        // Build the selects for the query
        // We will limit to this columns because we want to avoid
        // receiving unneeded columns and recording them
        call_user_func_array([$query, 'select'], self::getHistorySelectsByFilter($request['filters']));

        //TODO this is a temporary fix, we need to find a way to do this with collections
        return $query->get();
    }


    /**
     * Get Old Status By Change Level
     *
     * @param $record
     *
     * @return mixed
     * @throws Exception
     */
    private static function getOldStatusByChangeLevel($record)
    {
        switch (self::$change_attributes['type']) {
            case 'status' :
                return $record[self::$change_attributes['level'] . '_status'];

            case 'bid' :
                return $record[self::$change_attributes['level'] . '_bid'];

            case 'budget' :
                return $record[self::$change_attributes['level'] . '_budget'];

            case 'bid_adjustment' :
                return $record[self::$change_attributes['level'] . '_device_bid_adjustment'];

            case 'scheduling' :
            case 'location' :
            case 'campaign_settings' :
            case 'site_links' :
            case 'comment' :
                return null;
        }

        throw new Exception('Unknown change Type');
    }


    /**
     * @param $request
     *
     * @return mixed
     * @throws Exception
     */
    private static function getNewStatusByChangeLevel($request)
    {
        switch ($request['change_type']) {
            case 'status' :
                return ($request['status'] === 'active') ? 1 : 0;

            case 'bid' :
                return $request['bid'];

            case 'budget' :
                return $request['budget'];

            case 'bid_adjustment' :
                return $request['bid_adjustment'];

            case 'scheduling' :
            case 'location' :
            case 'campaign_settings' :
            case 'site_links' :
            case 'comment' :
                return null;
        }

        throw new Exception('Change type value was not passed.');
    }


    /**
     * @param $request
     *
     * @return array|static[]
     */
    private static function getAllFilteredRecords($request)
    {
        return self::buildQueryByFilters($request)->get();
    }


    /**
     * @param $checkbox
     *
     * @return bool
     */
    private static function getFilteredSelectionOptions($checkbox)
    {
        // The ids of the selected subjects
        // they are selected in the datatable, if the use selected all of them using the table header checkbox
        // the instead of an array the string value "ALL" will be passed

        if (is_array($checkbox) && !empty($checkbox)) {
            return $checkbox;
        } elseif ($checkbox == 'ALL') {
            return false;
        }
    }


    private static function getHistorySelectsByFilter($filters)
    {
        $selects = [
            'report_id',
            'report_name',
            'advertiser_id',
            'advertiser_name',
            'publisher_id',
            'publisher_name',
            'account_id',
            'account_name',
        ];

        // Device
        if (!empty($filters['device'] ?? null)) {
            $selects[] = 'device';
        }

        // Campaign
        if (!empty($filters['account'] ?? null)) {
            $selects[] = 'campaign_id';
            $selects[] = 'campaign_name';

            if (self::$change_attributes['type'] == 'status') {
                $selects[] = 'campaign_status';
            } elseif (self::$change_attributes['type'] == 'bid_adjustment') {
                $selects[] = 'campaign_device_bid_adjustment';
            } elseif (self::$change_attributes['type'] == 'budget') {
                $selects[] = 'campaign_budget';
            }
        }

        // Ad group
        if (!empty($filters['campaign'] ?? null)) {
            $selects[] = 'ad_group_id';
            $selects[] = 'ad_group_name';

            if (self::$change_attributes['type'] == 'status') {
                $selects[] = 'ad_group_status';
            } elseif (self::$change_attributes['type'] == 'bid') {
                $selects[] = 'ad_group_bid';
            } elseif (self::$change_attributes['type'] == 'bid_adjustment') {
                $selects[] = 'ad_group_device_bid_adjustment';
            }
        }

        // Match type
        if (!empty($filters['match_type'] ?? null)) {
            $selects[] = 'match_type';
        }

        // Keyword
        if (!empty($filters['ad_group'] ?? null)) {
            $selects[] = 'keyword_id';
            $selects[] = 'keyword_name';

            if (self::$change_attributes['type'] == 'status') {
                $selects[] = 'keyword_status';
            } elseif (self::$change_attributes['type'] == 'bid') {
                $selects[] = 'keyword_bid';
            }
        }

        return $selects;
    }


    /**
     * @param $request
     * @param bool $grouped_by
     *
     * @return Builder
     * @throws Exception
     */
    private static function buildQueryByFilters($request, $grouped_by = false) : Builder
    {
        // Builds the eloquent builder filters according to the the change level
        switch (self::$change_attributes['level']) {

            case 'campaign' :
                $query = MrrChangeLogState::where('account_id', $request['filters']['account']);

                if (is_array(self::$change_attributes['selections'])) {
                    $query->whereIn('campaign_id', self::$change_attributes['selections']);
                }
                break;

            case 'ad_group' :
                $query = MrrChangeLogState::whereIn('campaign_id', $request['filters']['campaign']);

                if (is_array(self::$change_attributes['selections'])) {
                    $query->whereIn('ad_group_id', self::$change_attributes['selections']);
                }
                break;

            case 'keyword' :
                $query = MrrChangeLogState::whereIn('ad_group_id', $request['filters']['ad_group'])
                    ->groupBy('keyword_id');

                if (is_array(self::$change_attributes['selections'])) {
                    $query->whereIn('keyword_id', self::$change_attributes['selections']);
                }
                break;

            default :
                throw new \Exception('Wrong change level');
        }

        // Add device filter
        if (!empty($request['device'] ?? null)) {
            $query->whereIn($request['device']);
        }

        // Add match_type filter
        if (!empty($request['match_type'] ?? null)) {
            $query->whereIn($request['match_type']);
        }

        // Group By the filtered results
        if ($grouped_by) {
            if (in_array(self::$change_attributes['level'], ['campaign', 'ad_group', 'keyword'])) {
                $query->groupBy(self::$change_attributes['level'] . '_id');
            }

            // Add device filter and group by
            if (!empty($request['device'] ?? null)) {
                $query->groupBy('device');
            }

            // Add match_type filter and group by
            if (!empty($request['match_type'] ?? null)) {
                $query->groupBy('match_type');
            }
        }

        return $query;
    }


    /**
     * @param $request
     *
     * @return null|string
     */
    private static function getActionValueFormat($request)
    {
        if (in_array(self::$change_attributes['type'], ['bid', 'budget', 'bid_adjustment'])) {
            if (($request['value_format'] ?? null) == 'percent') {
                return $request['value_format'];
            } else {
                return 'usd';
            }
        }

        return null;
    }


    /**
     * @param $request
     *
     * @return null
     */
    private static function getActionValueOperation($request)
    {
        if (in_array(self::$change_attributes['type'], ['bid', 'budget', 'bid_adjustment'])) {
            if (in_array($request['value_operation'], ['increase_by', 'decrease_by', 'set_to'])) {
                return $request['value_operation'];
            }
        }

        return null;
    }


    /**
     * Calculates the new value according to the current passed value
     *
     * @param $current_value
     *
     * @return mixed|number
     * @throws Exception
     */
    private static function getCalculatedNewValue($current_value)
    {
        switch (self::$change_attributes['value_operation']) {

            case 'increase_by' :
                if (self::$change_attributes['value_format'] == 'percent') {
                    return $current_value * (1 + self::$change_attributes['new_value'] / 100);
                } elseif (self::$change_attributes['value_format'] == 'usd') {
                    return $current_value + self::$change_attributes['new_value'];
                }
                break;

            case 'decrease_by' :
                if (self::$change_attributes['value_format'] == 'percent') {
                    return abs($current_value * (1 - self::$change_attributes['new_value'] / 100));
                } elseif (self::$change_attributes['value_format'] == 'usd') {
                    return abs($current_value - self::$change_attributes['new_value']);
                }
                break;

            case 'set_to' :
                return self::$change_attributes['new_value'];
        }

        throw new \Exception('Could not calculate new value');
    }


    /**
     * Clears the data stored in the session
     */
    protected static function cleanSession()
    {
        Session::remove(self::$session_name);
    }


    /**
     * Add to session
     * Will save what is currently in the session instead of replacing what ever it is in there
     *
     * @param $key
     * @param $value
     * @param bool $push_to_array
     */
    protected static function sessionPutConcat($key, $value, $push_to_array = true)
    {
        $current_session = Session::get(self::$session_name);

        if ($push_to_array) {
            if (!isset($current_session[$key])) {
                $current_session[$key] = [];
            }
            array_push($current_session[$key], $value);
        }

        Session::put(self::$session_name, $current_session);
    }

    /**
     * Get Operations List
     * get operations list according to change_type (bid, bid_adjustment, budget)
     *
     * @param $change_type
     *
     * @return array
     */
    private static function getOperationsList($change_type)
    {
        $operations = [
            'increase_by' => ['id' => 'increase_by', 'text' => 'Increase By'],
            'decrease_by' => ['id' => 'decrease_by', 'text' => 'Decrease By'],
            'set_to'      => ['id' => 'set_to', 'text' => 'Set To'],
        ];
        $type = [
            'percent' => ['id' => 'percent', 'text' => 'Percent'],
            'usd'     => ['id' => 'usd', 'text' => 'Usd'],
        ];

        if ($change_type == 'bid_adjustment') {
            return [
                'operation' => [
                    $operations['set_to']
                ],
                'type' => [
                    $type['percent']
                ]
            ];
        } elseif ($change_type == 'budget') {
            return [
                'operation' => [
                    $operations['set_to']
                ],
                'type' => [
                    $type['usd']
                ]
            ];
        } elseif ($change_type == 'bid') {
            return [
                'operation' => [
                    $operations['increase_by'],
                    $operations['decrease_by'],
                    $operations['set_to']
                ],
                'type' => [
                    $type['percent'],
                    $type['usd']
                ]
            ];
        }

    }
}