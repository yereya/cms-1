<?php namespace App\Libraries\Alerts;


/**
 * Class AlertTableCampaignTransformer
 * @package App\Libraries\Alerts
 */
class AlertTableCampaignTransformer implements AlertTableTransformerInterface
{
    /**
     * Transform campaigns alerts array to a predictable structure
     *
     * @param $data
     * @param $date
     *
     * @return array
     */
    public static function transform($data, $date)
    {
        //format each item so it contains predictable table columns
        return array_map(function ($item) use ($date) {
            return [
                'stats_date_tz'  => $item['stats_date_tz'],
                'publisher_id'   => $item['publisher_id'],
                'publisher_name' => $item['publisher'],
                'account_id'     => $item['account_id'],
                'account_name'   => $item['account_name'],
                'campaign_id'    => $item['campaign_id'],
                'campaign_name'  => $item['campaign_name'],
                'clicks'         => $item['clicks'],
                'avg_clicks'     => $item['avg_clicks'],
                'yesterday_cpc'  => $item['yesterday_cpc'],
                'cpc'            => $item['cpc'],
                'exception'      => $item['exception'],
                'created_at'     => $date,
                'updated_at'     => $date,
            ];
        }, $data);
    }

}