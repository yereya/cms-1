<?php namespace App\Libraries\Alerts;


/**
 * Class AlertTableSystemErrorTransformer
 * @package App\Libraries\Alerts
 */
class AlertTableSystemErrorTransformer implements AlertTableTransformerInterface
{
    /**
     * Transform system errors array to a predictable structure
     *
     * @param $data
     * @param $date
     * @return array
     */
    public static function transform($data, $date)
    {
        //format each item so it contains predictable table columns
        return array_map(function ($item) use ($date) {
            $item['system_name'] = $item['system'];
            unset($item['system']);
            $item['created_at'] = $date;
            $item['updated_at'] = $date;
            return $item;
        }, $data);
    }

}