<?php
/**
 * Created by PhpStorm.
 * User: dvirm
 * Date: 26/05/2019
 * Time: 11:06
 */

namespace App\Libraries\Alerts;
use App\Entities\Models\HolsteredQuery;
use App\Entities\Models\Scheduler\Task;
use App\Libraries\Mail;
use DB;
use PDO;
use Mockery\Exception;


class AlertData
{
    protected $id;
    protected $advertiser_id;

    public function __construct($params)
    {
        foreach ( $params as $colName=>$colValue) {
            $this->$colName = $colValue;
        }
    }
}

class EmailAlertsByRoleID
{
    protected $roleID;
    protected $dbRes;
    protected $taskName;
    protected $asCsv;
    protected $query_id;


    public function __construct($taskName, $roleID, $dbRes,$query_id, $asCsv = false)
    {
        $this->roleID   = $roleID;
        $this->dbRes    = $dbRes;
        $this->taskName = $taskName;
        $this->asCsv    = $asCsv;
        $this->query_id = $query_id;
    }

    public function start()
    {
        $advertisersIds           = $this->getAdvertisersIdsFromDbRes();
        $advertisersIdsAndEmails  = $this->getUsersEmailByAdvertisersAndRoleIds($advertisersIds);
        $alertsPerEmails          = $this->getAlertsPerEmails($advertisersIdsAndEmails);
        $this->sendEmails($alertsPerEmails);
    }

    private function getAdvertisersIdsFromDbRes()
    {
        $advertisersIds = array($this->dbRes[0]['advertiser_id']);
        foreach($this->dbRes as $dbResItem)
        {
            if (!in_array($dbResItem['advertiser_id'], $advertisersIds))
            {
                array_push($advertisersIds, $dbResItem['advertiser_id']);
            }
        }
        return $advertisersIds;
    }

    private function sendEmails($alertsPerEmails)
    {
        $query = HolsteredQuery::find($this->query_id);
        # If description for the query exists in cms.holstered_queries under describe column, add it to body and parse later on
        try{
            # If description for the query exists in cms.scheduler_tasks under description column, add it to body and parse later on
            $body['description'] = Task::select('description')
                ->where('field_query_params_id','=','9')
                ->where('task_id','=',$query->getAttributes()['id'])
                ->first()['description'];
            $description = $body['description'] ? $body['description'] : '';
        }catch (\Exception $e){
            static::$logger->error(['Exception' => 'Error in extracting description in EmailAlertsByRoleID.php 82 '.' '.$e->getMessage()]);
        }

        foreach( $alertsPerEmails as $email=>$alerts )
        {
            try
            {
                if($this->asCsv){
                    Mail::tplAttachment($email, [
                        'content' => $alerts,
                        'title' => $this->taskName,
                        'description' => $description
                    ],self::create_csv_string($alerts,$this->query_id));

                }else{
                    Mail::tplBlank($email, [
                        'content' => $this->createHtmlTableFromDbResults($alerts),
                        'title'   => $this->taskName,
                        'description' => $description
                    ]);
                }

            } catch (Exception $e) {
                static::$logger->error(['Exception' => 'Failed to send email to '.$email.'. Error message - '.$e->getMessage()]);
            }
        }
    }

    /**
     * Create csv
     *
     * @param $data
     */
    private static function create_csv_string($data, $query_id) {

    try {
        # --- Create storage folder and path ---
        $report_folder = 'reports' . DIRECTORY_SEPARATOR . 'MailAlertCsv';
        \Storage::makeDirectory($report_folder);
        $report_folder_path = storage_path('app') . DIRECTORY_SEPARATOR . $report_folder;
        $file_name = $report_folder_path . DIRECTORY_SEPARATOR . $query_id . '.csv';

        # --- Create CSV file ---
        // Set headers
        $headers = array_keys((array)array_pop($data));

        // Open temp file pointer
        $fp = fopen($file_name, 'w');

        // Loop data and write to file pointer
        fputcsv($fp, $headers); // add Headers
        foreach ($data as $line) fputcsv($fp, (array)$line);

        // Place stream pointer at beginning
        rewind($fp);
    }catch (\Exception $e){
        throw new \Exception('Raised exception in creating csv from data, EmailAlertsByRoleID.php -> create_csv_string. Exception: ' . $e);
    }
        // Return the data
        return $file_name;
    }

    /* Grouping all alerts by email address */
    private function getAlertsPerEmails($advertisersIdsAndEmails)
    {
        $alertsPerEmails = array();
        $dbResCount      = count($this->dbRes);

        for ($i = 0; $i < $dbResCount; $i++)
        {
            $emailsForSpecificAdvertiserId = $this->getEmailsBySpecificAdvertiserId($advertisersIdsAndEmails, $this->dbRes[$i]['advertiser_id']);
            foreach ($emailsForSpecificAdvertiserId as $email)
            {
                if (!isset($alertsPerEmails[$email])) {
                    $alertsPerEmails[$email] = array();
                }
                array_push($alertsPerEmails[$email], new AlertData($this->dbRes[$i]));
            }
        }
        return $alertsPerEmails;
    }

    private function getEmailsBySpecificAdvertiserId($advertisersIdsAndEmails, $advertiser_id)
    {
        $advertisersIdsAndEmailsCount  = count($advertisersIdsAndEmails);
        $emailsForSpecificAdvertiserId = array();
        for($j=0; $j<$advertisersIdsAndEmailsCount; $j++)
        {
            if($advertisersIdsAndEmails[$j]['advertiser_id']== $advertiser_id)
            {
                array_push($emailsForSpecificAdvertiserId, $advertisersIdsAndEmails[$j]['email'] );
            }else if(count($emailsForSpecificAdvertiserId)>0)
            {
                return $emailsForSpecificAdvertiserId;
            }
        }
        return $emailsForSpecificAdvertiserId;
    }

    private function getUsersEmailByAdvertisersAndRoleIds($advertisersIds)
    {
        $binding = array();
        $bindString = bindParamArray("id", $advertisersIds, $binding );
        $SQL = "SELECT
                           DISTINCT(email),
                           advertiser_id
                    FROM   bo.accounts AS a
    
                    JOIN   bo.user_assigned_accounts AS uaa
                    ON     uaa.account_id = a.id
    
                    JOIN (
                                SELECT
                                        email,
                                        username,
                                        u.id AS user_id
                                FROM    bo.users AS u
                                WHERE   u.id IN (
                                                    SELECT
                                                            DISTINCT(user_id)
                                                    FROM    bo.site_user_role
                                                    WHERE   role_id =:roleID
                                                 )
                          ) AS Y
                    ON uaa.user_id= Y.user_id
    
                    WHERE  advertiser_id IN ($bindString)
                    ORDER BY advertiser_id";
        $db_connection='';
        try{
            $db_connection = DB::connection('bo');  // note - 'bo' contains some duplicates tables from 'cms'.
        }catch (Exception $err){
            static::$logger->error(['Exception' => 'Failed to connect to DB - bo.'.' Error message -'.$err->getMessage()]);
        }
        $db_connection->setFetchMode(PDO::FETCH_ASSOC);
        $binding["roleID"] = $this->roleID;
        try {
            $res = $db_connection->select($SQL, $binding);
        } catch (Exception $e) {
            static::$logger->error(['Exception' => 'Failed to execute sql query '.$SQL.' Error message -'.$err->getMessage()]);
        }
        try {
            $db_connection->disconnect();
        } catch (Exception $e) {
            static::$logger->error(['Exception' => 'Failed to disconnect from DB - bo.'.' Error message -'.$err->getMessage()]);
        }

        return $res;
    }

    private function createHtmlTableFromDbResults($alerts)
    {
        $htmlString = '<!DOCTYPE html>
                    <html>
                    <head>
                    <style>
                        table {
                          font-family: arial, sans-serif;
                          border-collapse: collapse;
                          width: 100%;
                          color: black;
                        }
                        
                        td, th {
                          border: 1px solid #dddddd;
                          text-align: left;
                          padding: 8px;
                        }
                        
                    </style>
                    </head>
                    <body>
                    
                    <h2>Alerts</h2>
                    
                    <table>';

        $htmlString = $htmlString .'<tr>';
        $evenIndex = 1;
        foreach (get_object_vars($alerts[0]) as $colName=>$colValue)
        {
            if($colName!='id' && $colName!='advertiser_id' && $colName!='email')
            {
                if( $evenIndex%2 == 0){
                    $htmlString = $htmlString .'<th style="background-color: #CBC6C6">'.$colName.'</th>';
                }else{
                    $htmlString = $htmlString .'<th>'.$colName.'</th>';
                }
            }
            $evenIndex++;
        }
        $htmlString = $htmlString .'</tr>';
        $evenIndex = 1;
        foreach ($alerts as $alert)
        {
            $htmlString = $htmlString .'<tr>';
            foreach (get_object_vars($alert) as $colName=>$colValue)
            {
                if($colName!='id' && $colName!='advertiser_id' && $colName!='email')
                {
                    if( $evenIndex%2 == 0){
                        $htmlString = $htmlString .'<td style="background-color: #CBC6C6">'. $this->getFormalTd($colName,$colValue) .'</td>';
                    }else{
                        $htmlString = $htmlString .'<td>'. $this->getFormalTd($colName,$colValue) .'</td>';
                    }
                }
                $evenIndex++;
            }
            $htmlString = $htmlString .'</tr>';
        }
        return $htmlString .'</table>
                                
                                </body>
                                </html>';

    }

    private function getFormalTd($colName, $colValue)
    {
        $colNamesForPercentages = array('share', 'ratio', 'cts', 'ctr');
        $colNamesForPrices      = array('epc', 'profit', 'cost');
        switch(true)
        {
            case $this->isStringContainsOneOfTheSubstrings($colNamesForPercentages, $colName):
                return (number_format((float)$colValue, 3, '.', ',')*100).'%';
            case $this->isStringContainsOneOfTheSubstrings($colNamesForPrices, $colName):
                return (number_format((float)$colValue, 2, '.', ',')).'$';
            default:
                return $colValue;
        }
    }

    private function isStringContainsOneOfTheSubstrings($substrings, $string){
        foreach ( $substrings as $substring )
        {
            $pos = stripos($string, $substring);
            // pos can be 0
            if( $pos !== false )
            {
                return true;
            }
        }
        return false;
    }




}
