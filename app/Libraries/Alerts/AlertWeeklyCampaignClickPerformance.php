<?php namespace App\Libraries\Alerts;

class AlertWeeklyCampaignClickPerformance implements AlertTableTransformerInterface
{
    /**
     * Transform accounts alerts array to a predictable structure
     *
     * @param $data
     * @param $date
     *
     * @return array
     */
    public static function transform($data, $date)
    {
        //format each item so it contains predictable table columns
        return array_map(function ($item) use ($date) {
            return [
                'yrs' => $item['yrs'],
                'week_of_year' => $item['week_of_year'],
                'publisher_id'   => $item['publisher_id'],
                'publisher_name' => $item['publisher'],
                'campaign_id'    => $item['campaign_id'],
                'campaign_name'  => $item['campaign_name'],
                'clicks'         => $item['clicks'],
                'avg_clicks'     => $item['avg_clicks'],
                'exception'      => $item['exception'],
                'created_at'     => $date,
                'updated_at'     => $date,
            ];
        }, $data);
    }
}