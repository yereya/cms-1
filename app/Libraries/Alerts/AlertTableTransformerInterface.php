<?php namespace App\Libraries\Alerts;

/**
 * Interface AlertTableTransformerInterface
 * @package App\Libraries\Alerts
 */
interface AlertTableTransformerInterface
{
    /**
     * Transform array to a predictable structure
     *
     * @param $data
     * @param $date
     * @return mixed
     */
    public static function transform($data, $date);
}
