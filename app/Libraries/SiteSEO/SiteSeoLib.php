<?php namespace App\Libraries\SiteSEO;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Page;
use App\Entities\Models\Sites\SiteSeoSettings;
use App\Entities\Models\Sites\SiteSetting;
use App\Facades\SiteConnectionLib;

class SiteSeoLib
{
    protected static $global_seo_settings = [
        'title_size' => [
            'symbol_length' => '',
            'width_pixels'  => ''
        ],
        'desc_size'  => [
            'symbol_length' => '',
            'width_pixels'  => ''
        ],
    ];

    protected static $seo_default_settings = [];
    protected static $seo_model;

    /**
     * Save Content Type
     *
     * @param SiteContentType $model
     * @param                 $request
     * @param                 $data_id
     */
    public static function saveContentType(SiteContentType $model, $request, $data_id)
    {
        $seo_record = SiteSeoLib::getSeoSettings($data_id, $model->fullTable);
        self::saveItem($seo_record, $request);
    }

    /**
     * Get Seo Settings
     * get SEO Settings of post by unique combination of post_id and post_table_name
     *
     * @param $post_id
     * @param $table
     *
     * @return SiteSeoSettings
     */
    public static function getSeoSettings($post_id, $table)
    {
        self::$seo_model = SiteSeoSettings::where('post_id', $post_id)->where('post_table_name', $table)->first();

        if (!isset(self::$seo_model)) {
            self::$seo_model                  = new SiteSeoSettings();
            self::$seo_model->post_id         = $post_id;
            self::$seo_model->post_table_name = $table;
        }

        return self::$seo_model;
    }

    /**
     * Save Item
     *
     * @param $seo_record
     * @param $request
     */
    private static function saveItem($seo_record, $request)
    {
        $seo_values = $request->only($seo_record->getFillable());
        self::getSeoDefaults();
        self::fillRequiredFields($seo_values, $request->title);
        $seo_record->fill($seo_values);
        $seo_record->save();
    }

    /**
     * Get Seo Defaults
     * gets the site's default settings for SEO
     */
    private static function getSeoDefaults()
    {
        $default_settings = SiteSetting::where('site_id', (SiteConnectionLib::getSite())->id)
            ->where('type', 'site')->get()->keyBy('key')->toArray();

        self::$seo_default_settings['site_name']        = $default_settings['site_name']['value'] ?? '';
        self::$seo_default_settings['og_image_default'] = $default_settings['og_image']['value'] ?? '';
        self::$seo_default_settings['type']             = $default_settings['type']['value'] ?? '';
    }

    /**
     * Fill Required Fields
     * fills the fields that are empty and should be filled with default values
     *
     * @param $seo_values
     * @param $title
     */
    private static function fillRequiredFields(&$seo_values, $title)
    {
        $seo_values['seo_post_title'] = $seo_values['seo_post_title'] ?: $title . ' ' . self::$seo_default_settings['site_name'];
        $seo_values['og_title']       = $seo_values['og_title'] ?: $title . ' ' . self::$seo_default_settings['site_name'];
        $seo_values['og_description'] = $seo_values['og_description'] ?: $seo_values['description'];
        $seo_values['og_url']         = $seo_values['canonical_url'] ?: '';
        $seo_values['og_type']        = $seo_values['type'] ?: self::$seo_default_settings['type'];
        $seo_values['og_image']       = $seo_values['og_image'] ?: self::$seo_default_settings['og_image_default'];

    }

    /**
     * Save Page
     *
     * @param Page $page
     * @param      $request
     */
    public static function savePage(Page $page, $request)
    {
        $seo_record = SiteSeoLib::getSeoSettings($page->id, $page->getTable());
        self::saveItem($seo_record, $request);
    }

    /**
     * Delete Content Type
     *
     * @param SiteContentType $model
     * @param                 $data_id
     */
    public static function deleteContentType(SiteContentType $model, $data_id)
    {
        $seo_record = SiteSeoLib::getSeoSettings($data_id, $model->fullTable);
        $seo_record->delete();
    }

    /**
     * Delete Page
     *
     * @param Page $page
     */
    public static function deletePage(Page $page)
    {
        $seo_record = SiteSeoLib::getSeoSettings($page->id, $page->getTable());
        $seo_record->delete();
    }

}