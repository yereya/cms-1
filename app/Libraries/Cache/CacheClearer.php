<?php

namespace App\Libraries\Cache;

use App\Libraries\Guzzle;
use App\Facades\SiteConnectionLib;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use Mockery\Exception;

class CacheClearer
{
    const BASE_CLEAR_URL = '/api/cache/clear';

    /**
     * Clear From Event
     *
     * @param string  $type
     * @param integer $entity_id
     * @param integer $site
     *
     * @return string
     */
    public function clearFromEvent($type, $entity_id, $site = null)
    {
        $site = $site ? $site : SiteConnectionLib::getSite();
        $data = [
            'type'      => $type,
            'entity_id' => $entity_id
        ];

        return $this->sendRequest($site, $data);
    }

    /**
     * Purge all cache for a site.
     *
     * @param Site $site
     *
     * @return string
     */
    public function clearAll(Site $site)
    {
        return $this->sendRequest($site, [], '/all');
    }

    /**
     * Send Request
     *
     * @param Site   $site
     * @param array  $data
     * @param string $uri
     *
     * @return string
     */
    private function sendRequest($site, $data, $uri = '')
    {
        $base_url = (new SiteDomainRepo())->getBaseUrl($site->id);
        $url      = $base_url . static::BASE_CLEAR_URL . $uri;

        $guzzle = new Guzzle();
        $params = [
            'json'            => $data,
            'connect_timeout' => 0, // Don't Wait for the result
        ];

        $guzzle->fetch($url, $params, 'POST');

        try {
            $body = $guzzle->getBody();

            if (!$body) {

                return false;
            }

            return json_decode($body, true);
        } catch (Exception $ex) {
            // Invalid JSON string.
            return false;
        }
    }
}