<?php

namespace App\Libraries;


/**
 * Class CommandQueueLib
 * @package App\Libraries
 */
use Illuminate\Support\Facades\Storage;

/**
 * Class CommandQueueLib
 * @package App\Libraries
 */
class CommandQueueLib
{

    /**
     * Directory where all the queue log files will be saved,
     * relative from storage/app directory
     *
     * @var string
     */
    public static $storage_directory = 'queue';

    /**
     * Execution time limit before log file deletion
     *
     * @var int
     */
    private static $deletion_timeout = 600;

    /**
     * If log file doesn't exist, Create a log file with start epoch time.
     * If it does exist, throw an exception
     *
     * @param $command
     * @param $params
     * @return string
     * @throws \Exception
     */
    public static function start($command, array $params)
    {
        // Resolve the log file path to look for, according to a naming convention,
        // if log file returns false it means that the specific command
        // should not be queued and is free to keep running
        if (!$log_file_path = self::resolveLogFilePathFromCommand($command, $params)) {
            return false;
        }

        // If file already exists, the same instance is already running
        if (Storage::disk('local')->exists($log_file_path)) {

            // if execution time limit has passed, delete the log file
            if (self::isTimedOut($log_file_path)) {
                self::end($log_file_path);

                // Start the same command again, this time the file won't exists
                return self::start($command, $params);
            }

            // else, the script is still running, write a warning with the TpLogger
            $message = self::logAlreadyRunning($command, $params, $log_file_path);

            // Terminate command run with the warning message
            die($message);

        } else { // File doesn't exist, instance is free to run

            $time = time();

            // Store the epoch timestamp in the new file
            Storage::disk('local')->put($log_file_path, $time);

            return $log_file_path;
        }
    }


    /**
     * Resolve the log file name to look for according to
     * a naming convention which considers only the
     * mandatory options of each command
     *
     * @param $command
     * @param $params
     * @return bool
     */
    private static function resolveLogFilePathFromCommand($command, $params)
    {

        // Each of these commands has its own logic which is encapsulated
        // in its own method. the method is determined by a naming convention
        if (in_array($command, ['scraper', 'query'])) {

            // build the method name according to the naming convention
            $methodName = 'resolve' . ucfirst($command) . 'FileName';

            // call the relevant method
            $filename = self::$methodName($params);

        } else {

            // The rest of commands don't have any special logic and will be
            // handled by the default logic
            $filename = self::resolveDefaultFileName($command, $params, ['id']);
        }

        // If filename is false, it means the the process should continue run freely
        if (!$filename) {
            return false;
        }

        // Escape any non valid filename chars
        $clean_filename = self::escapeFileName($filename);

        // Build the filename path
        return self::$storage_directory . '/' . $clean_filename;
    }

    /**
     * Concatenate a file name according to the naming convention logic
     *
     * @param $params
     * @return string
     */
    private static function resolveDefaultFileName($command, $params, array $keys = [])
    {
        $filename = $command.'_';

        foreach ($keys as $key) {
            //if option or value has value then concatenate the option key and value
            if (isset($params[$key]) && $params[$key]) {

                $filename .= '_'. $key .'-'. $params[$key];
            }
        }

        return $filename;

    }

    /**
     * @param $params
     * @return string
     */
    private static function resolveScraperFileName($params)
    {
        return self::resolveDefaultFileName('scraper', $params,
            ['request', 'id', 'provider', 'date-range-end', 'date-range-start']);
    }

    /**
     * @param $params
     * @return string
     */
    private static function resolveQueryFileName($params)
    {
        // If it doesn't have any of these flags, let it run freely
        if (!$params['id']) {
            return false;
        }

        return self::resolveDefaultFileName('query', $params, ['id', 'sc']);
    }

    /**
     * Replace any non supported characters with
     * underscore to make a valid file name
     *
     * @param $log_file_name
     * @return mixed
     */
    private static function escapeFileName($log_file_name)
    {
        return preg_replace('/[^A-Za-z0-9_,-]/', '_', $log_file_name);
    }

    /**
     * Delete a log file of a command which finished running
     *
     * @param $log_file_path
     */
    public static function end($log_file_path)
    {
        // if $log_file_path is false then there is nothing you should do
            return Storage::disk('local')->delete($log_file_path);
    }

    /**
     * Check if execution time limit has passed
     *
     * @param $log_file_path
     * @return bool
     */
    private static function isTimedOut($log_file_path)
    {
        $epoch = Storage::disk('local')->get($log_file_path);

        return $epoch + self::$deletion_timeout < time();
    }

    /**
     * Log a warning when same instance is already running
     *
     * @param $path
     * @return mixed
     */
    public static function logAlreadyRunning($command, $params, $path = '')
    {
        $tpLogger = TpLogger::getInstance();

        if (!$path) {
            // Resolve the log file name to look for according to a naming convention
            $path = self::resolveLogFilePathFromCommand($command, $params);
        }

        // Filter params to contain only the relevant params that should be included in the error message
        $params = array_intersect_key($params, array_flip(['request', 'id', 'date-range-end', 'date-range-start',
            'date-range-type']));

        $message = 'Same Command Instance Is Already Running. Log file Path: ' . $path;

        $tpLogger->warning([
            $message,
            $command,
            $params
        ]);

        return $message;
    }

}