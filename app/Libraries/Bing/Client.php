<?php

namespace App\Libraries\Bing;

use File;
use Exception;
use Microsoft\BingAds\Auth\ServiceClient;
use Microsoft\BingAds\Auth\ApiEnvironment;
use Microsoft\BingAds\Auth\ServiceClientType;
use Microsoft\BingAds\Auth\AuthorizationData;
use Microsoft\BingAds\Auth\OAuthWebAuthCodeGrant;

use Microsoft\BingAds\V13\CustomerManagement\Paging;
use Microsoft\BingAds\V13\CustomerManagement\Predicate;
use Microsoft\BingAds\V13\CustomerManagement\GetUserRequest;
use Microsoft\BingAds\V13\Reporting\PollGenerateReportRequest;
use Microsoft\BingAds\V13\CustomerManagement\PredicateOperator;
use Microsoft\BingAds\V13\CustomerManagement\SearchAccountsRequest;
use Microsoft\BingAds\V13\CampaignManagement\GetCampaignsByAccountIdRequest;

class Client
{
    /**
     * @ver string
     */
    protected $client_id;

    /**
     * @ver AuthorizationData
     */
    protected $authorization;

    /**
     * @ver string
     */
    protected $client_secret;

    /**
     * @ver string
     */
    protected $developer_token;

    /**
     * @ver string
     */
    protected $refresh_token_path;


    /** @var string */
    protected $namespace;

    /**
     * Init our Bing api client.
     *
     * @param string $client_id
     * @param string $client_secret
     * @param string $developer_token
     *
     * @throws Exception
     */
    public function __construct($client_id, $client_secret, $developer_token, $refresh_token_path='app/bing-token.txt')
    {
        $this->client_id          = $client_id;
        $this->client_secret      = $client_secret;
        $this->developer_token    = $developer_token;
        $this->refresh_token_path = storage_path($refresh_token_path);

        $this->authorization = $this->buildAuthorization();
    }

    /**
     * Get a list of accounts by a given user id.
     *
     * @param integer $user_id
     *
     * @return mixed
     * @throws Exception
     */
    public function getAccountsByUserId($user_id)
    {
        $pageInfo            = new Paging();
        $pageInfo->Index     = 0;
        $pageInfo->Size      = 100;
        $predicate           = new Predicate();
        $predicate->Field    = "UserId";
        $predicate->Operator = PredicateOperator::Equals;
        $predicate->Value    = $user_id;
        $request             = new SearchAccountsRequest();
        $request->Ordering   = null;
        $request->PageInfo   = $pageInfo;
        $request->Predicates = [$predicate];

        return $this->customerService()->GetService()->SearchAccounts($request);
    }

    /**
     * Get user object for a given id.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws Exception
     */
    public function getUser($id = null)
    {
        $request         = new GetUserRequest();
        $request->UserId = $id;
        return $this->customerService()->GetService()->GetUser($request);
    }


    /**
     * Pull a report status by for a given report id.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function pullGeneratedReport($id)
    {
        $request                  = new PollGenerateReportRequest();
        $request->ReportRequestId = $id;
        return $this->reportingService()->GetService()->PollGenerateReport($request);
    }

    /**
     * Make a customer service client object.
     *
     * @return ServiceClient
     * @throws Exception
     */
    public function customerService()
    {
        return new ServiceClient(ServiceClientType::CustomerManagementVersion13, $this->authorization,
            ApiEnvironment::Production);
    }

    /**
     * Make a reporting service client object.
     *
     * @return ServiceClient
     * @throws Exception
     */
    public function reportingService()
    {
        return new ServiceClient(ServiceClientType::ReportingVersion13, $this->authorization,
            ApiEnvironment::Production);
    }

    /**
     * Make a campaigns service client object.
     *
     * @return ServiceClient
     * @throws Exception
     */
    public function campaignsService()
    {
        return new ServiceClient(ServiceClientType::CampaignManagementVersion13, $this->authorization,
            ApiEnvironment::Production);
    }

    /**
     * Get campaigns by account id
     *
     * @param integer $account_id
     *
     * @return mixed
     * @throws Exception
     */
    public function getCampaigns($account_id)
    {
        $request            = new GetCampaignsByAccountIdRequest();
        $request->AccountId = $account_id;

        return $this->campaignsService()->GetService()->GetCampaignsByAccountId($request);
    }

    /**
     * Generate an access token.
     * This process will also update our refresh token file.
     *
     * @throws Exception
     * @return AuthorizationData
     */
    protected function buildAuthorization(): AuthorizationData
    {
        # -------- Old Authenticaion -------

//        $authentication = (new OAuthWebAuthCodeGrant())
//            ->withClientId($this->client_id)
//            ->withClientSecret($this->client_secret)
//            ->withRedirectUri('https://login.live.com/oauth20_desktop.srf')
//            ->withState(rand(0, 999999999))
//            ->withRequireLiveConnect(true);

        # -------- New Authenticaion -------

        $authentication = (new OAuthWebAuthCodeGrant())
            ->withClientId($this->client_id)
            ->withClientSecret($this->client_secret)
            ->withRedirectUri('https://login.microsoftonline.com/common/oauth2/nativeclient')
            ->withState(rand(0, 999999999))
            ->withRequireLiveConnect(false);


        $authorization_data = (new AuthorizationData())
            ->withAuthentication($authentication)
            ->withDeveloperToken($this->developer_token);


        $refresh_token = $this->readRefreshToken();

        if (!$refresh_token) {
            throw new Exception('Invalid refresh token.');
        }

        $auth_tokens = $authorization_data->Authentication->RequestOAuthTokensByRefreshToken($refresh_token);

        File::put($this->refresh_token_path, $auth_tokens->RefreshToken);

        return $authorization_data;
    }

    /**
     * Set an account id for the current authorization object.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function withAccountId($id)
    {
        return $this->authorization->withAccountId($id);
    }

    /**
     * Read the refresh token from the config file.
     *
     * @return mixed
     */
    protected function readRefreshToken()
    {
        try {

            return File::get($this->refresh_token_path);
        } catch (Exception $ex) {
            $token = config('bing-ads.refresh_token');
            File::put($this->refresh_token_path, $token);

            return $token;
        }
    }

    /**
     * @param $id
     *
     * @return AuthorizationData
     */
    public function withCustomerId($id)
    {
        return $this->authorization->withCustomerId($id);
    }
}
