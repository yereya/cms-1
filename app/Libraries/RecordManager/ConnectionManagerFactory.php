<?php namespace App\Libraries\RecordManager;


use App\Libraries\RecordManager\Records\AdvertiserTracksConnection;
use App\Libraries\RecordManager\Records\BaseConnection;

class ConnectionManagerFactory
{

    /**
     * Instantiates the relevant connection manager
     *
     * @param $connection_name
     * @return mixed
     * @throws \Exception
     */
    public static function create($connection_name) : BaseConnection
    {
        switch ($connection_name) {
            case "AdvertiserTracks" :
                return new AdvertiserTracksConnection();

            // TODO - make connection managers for publishers, accountCampaigns and advertisersTargets

            default :
                throw new \Exception("No connection handler by the name [$connection_name] was found.");
        }
    }
}