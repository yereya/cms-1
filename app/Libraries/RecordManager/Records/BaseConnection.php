<?php namespace App\Libraries\RecordManager\Records;

use App\Libraries\TpLogger;
use Illuminate\Database\Connection;
use Illuminate\Support\Facades\DB;

abstract class BaseConnection
{
    /**
     * @var Connection $to_connection - connection to insert data to
     */
    protected $to_connection;

    /**
     * @var TpLogger $logger
     */
    protected $logger;

    /**
     * BaseConnection constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        // Generate a connection
        $this->to_connection = DB::connection();
        $this->logger = TpLogger::getInstance();
    }

    /**
     * Returns the active connection
     *
     * @return Connection
     */
    public function getConnection() : Connection
    {
        return $this->to_connection;
    }

    /**
     * Resets the current connection to a new one
     *
     * @param $connection_name
     */
    protected function resetConnectionTo($connection_name)
    {
        $this->to_connection->disconnect();
        $this->to_connection = \DB::connection($connection_name);
    }

    /**
     * Closes all connection sessions
     */
    protected function closeConnections()
    {
        $this->to_connection->disconnect();
    }

    /**
     * Check if a certain connection name exists in our DB config
     *
     * @param $connection_name
     * @return bool
     */
    protected function connectionExists($connection_name)
    {

        return in_array($connection_name, array_keys(config('database.connections')));
    }

    public function __destruct()
    {
        $this->closeConnections();
    }
}