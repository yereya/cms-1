<?php namespace App\Libraries\RecordManager\Records;


use App\Entities\Repositories\Bo\Publishers\PublishersRepo;

class PublishersRecordsHandler extends BaseHandler
{
    protected $manipulation_name = 'PublishersRecordsManipulation';

    /**
     * System id
     *
     * @var int
     */
    protected $system_id = 56;


    /**
     * @param $manipulation_action
     *
     * @return array
     */
    public function getManipulationRules($manipulation_action)
    {
        $rules = [
            'add' => [
                'source_report_uid'   => "required",
                'account_id'          => "required",
                'network_net_revenue' => "required",
                'stats_date_tz'       => "required|date",
                'device'              => "required",
                'cost'                => "required",
                'sid'                 => "required",
            ],
        ];

        return $rules[$manipulation_action];
    }


    /**
     * Returns the right managing repo
     *
     * @return string
     */
    protected function getRepo()
    {
        return PublishersRepo::class;
    }


    /**
     * @param $items
     *
     * @return mixed
     */
    protected function buildAddCandidates($items)
    {
        return $items;
    }


    /**
     * @param $items
     *
     * @return mixed
     */
    protected function buildUpdateCandidates($items)
    {
        return $items;
    }


    /**
     * @param $items
     *
     * @return mixed
     */
    protected function buildRemoveCandidates($items)
    {
        return $items;
    }
}