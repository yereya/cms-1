<?php namespace App\Libraries\RecordManager\Records;

use App\Libraries\RecordManager\Contracts\Handler;
use App\Libraries\RecordManager\Exceptions\InvalidRecordActionException;
use App\Libraries\TpLogger;
use League\Csv\Reader;
use Session;

/**
 * Class Handler
 * @package App\Libraries\RecordManager
 */
abstract class BaseHandler implements Handler
{

    protected $folder_name_prefix = 'manipulator_';

    /**
     * Global variable, used as a failure flag
     *
     * @var bool
     */
    protected $validation_failed_tracks_exist = false;

    protected $manipulation_name = null;

    protected $file_name;

    protected $logger;

    /**
     * System id
     *
     * @var int
     */
    protected $system_id;


    public function __construct($handler_name)
    {
        if (is_null($this->manipulation_name)) {
            throw new \Exception('Unset manipulator name in child handler.');
        }

        // Generate manipulation name
        $this->manipulation_name = $this->folder_name_prefix .'_' . $handler_name;
    }


    /**
     * Get Storage Folder Path
     *
     * @param bool $absolute_path -- will include the path to the storage folder or not
     *
     * @return string
     * @throws \Exception
     */
    public function getFolderPath($absolute_path = true)
    {
        $record_folder = 'record-manipulations';

        // Make sure the directory exists
        \Storage::makeDirectory($record_folder);

        if ($absolute_path) {
            return storage_path('app') . DIRECTORY_SEPARATOR . $record_folder;
        } else {
            return $record_folder;
        }
    }


    /**
     * Set the action for this manipulation procedure
     *
     * @param $action
     *
     * @throws InvalidRecordActionException
     */
    public function setAction($action)
    {
        if (!in_array(strtolower($action), ['add', 'update', 'remove'])) {
            throw new InvalidRecordActionException();
        }

        $this->cleanSession();
        $this->sessionPutConcat('action', $action);
    }


    /**
     * @return integer
     */
    public function getLoggerParentRecordId()
    {
        return $this->logger->getParentRecordId();
    }

    /**
     * Validates the auction insights file passed by a POST request
     * and saves
     *
     * @param $file_attribute_name
     *
     * @return bool|\Illuminate\Validation\Validator
     */
    public function saveAuctionFile($file_attribute_name)
    {
        try {
            $validation_verdict = $this->validateFile(\Input::file($file_attribute_name));

            if ($validation_verdict->fails()) { // Pass the validation results if the result is unsuccessful
                return $validation_verdict;
            }

            $file = \Input::file($file_attribute_name)->openFile();
            $csv_string = $file->fread($file->getSize());

            $this->createTpLoggerInstance();
            $mrr_connection = \DB::connection('mrr');
            $this->logger->info('connection to mrr success' ); //for check

            if (str_contains($csv_string,"\n")){ // If it's the right CSV pattern
                $csv_string = preg_replace('/[\0]/', '', $csv_string);// Delete unnecessary hidden signs
                $translated_csv = explode("\n",$csv_string); // Split the string to array
                //$headers = explode(",",preg_replace('/[.]/', '',$translated_csv[2])); // Takes the headers by split by ',' and delete '.' from the headers

                $headers = explode(",","account_name,account_id,display_url_domain,stats_date_tz,campaign_name,campaign_id,device,search_impr_share_auction_insights,search_outranking_share,search_overlap_rate,position_above_rate,top_of_page_rate,abs_top_of_page_rate");

                $data = array_slice($translated_csv,3); // Take only the data, cut unnecessary rows
                $chunks = array_chunk($data,1000); // Split to chunks because the file is too large and make memory low throw
                foreach ($chunks as &$data) {
                    if($data[0] == ""){
                        return false;
                    }
                    $final_result = [];
                    foreach ($data as &$item) {
                        if(str_contains($item,",")){ // If invalid row
                            $item = explode(",",$item);
                            $item = array_map(function($value) { if(str_contains($value,'<10%')) { $value = 0.05; } if(str_contains($value,'%')) { $value =  preg_replace('/[%]/','',$value); $value = floatval($value)/100; } return $value; },$item);
                            $item = preg_replace('/ --/',NULL,$item);//  Delete unnecessary signs --
                            $row = array_combine($headers,$item);
                            $final_result[] = $row;
                        }
                    }
                    $mrr_connection->table('mrr_auction_insight')->insert($final_result);
                }
            }
        } catch (\Exception $e) {
            $this->logger->error(['Exception' => 'Error message -'.$e->getMessage()]);
            return false;
        }
        return true;
    }

    /**
     * Validates the file passed by a POST request
     * and saves
     *
     * @param $file_attribute_name
     *
     * @return bool|\Illuminate\Validation\Validator
     */
    public function saveFile($file_attribute_name)
    {
        $validation_verdict = $this->validateFile(\Input::file($file_attribute_name));

        // Pass the validation results if the result is unsuccessful
        if ($validation_verdict->fails()) {
            return $validation_verdict;
        }

        $file = \Input::file($file_attribute_name)->openFile();
        $csv_string = $file->fread($file->getSize());

        return $this->save($csv_string);
    }


    /**
     * Saves a csv string to tracks array
     * saves it to the session
     *
     * @param $csv_string
     *
     * @return bool
     * @throws \Exception
     */
    public function save($csv_string)
    {
        $this->createTpLoggerInstance();

        $csv = Reader::createFromString($csv_string);
        $fields     = $csv->fetchOne();

        // validate fields headers
        if (!$this->validateCsvHeaders($fields)) {
            return false;
        }

        $csv_tracks = [];
        foreach ($csv->setOffset(1)->fetchAssoc($fields) as $item) {
            $csv_tracks[] = $item;
        }

        $this->validation_failed_tracks_exist = !$this->clearIllegalTracks($csv_tracks);

        $this->sessionPutConcat('records', $csv_tracks);

        return !$this->validation_failed_tracks_exist;
    }


    /**
     * Will build a candidate array of both the uploaded tracks and their copies
     * inside the database
     *
     * @return mixed
     * @throws InvalidRecordActionException
     */
    public function fetchCandidates()
    {
        $this->createTpLoggerInstance(true);

        $uploaded_tracks = Session::get($this->manipulation_name);

        switch ($uploaded_tracks['action']) {
            case 'add':
                $candidates = $this->buildAddCandidates($uploaded_tracks['records']);
                break;
            case 'update':
                $candidates = $this->buildUpdateCandidates($uploaded_tracks['records']);
                break;
            case 'remove':
                $candidates = $this->buildRemoveCandidates($uploaded_tracks['records']);
                break;
            default :
                throw new InvalidRecordActionException();
        }

        $this->sessionPutConcat('candidateRecords', $candidates);

        return $candidates;
    }


    /**
     * Stores the candidates to the DB
     *
     * @return int|null
     * @throws InvalidRecordActionException
     */
    public function store()
    {
        $uploaded_tracks = Session::get($this->manipulation_name);

        $this->createTpLoggerInstance(true);
        $this->logger->info(['The tracks requested to '. $uploaded_tracks['action'] .' are ', $uploaded_tracks['records']]);

        // Makes sure we wind up with valid tracks
        // Otherwise logs it and returns nothing
        if (!isset($uploaded_tracks['candidateRecords'])) {
            $this->logger->warning(['No tracks were saved', $uploaded_tracks]);
            return $this->logger->getParentRecordId();
        }

        switch ($uploaded_tracks['action']) {
            case 'add':
                call_user_func_array([$this->getRepo(), 'insert'], [$uploaded_tracks['candidateRecords']]);
                break;
            case 'update':
                call_user_func_array([$this->getRepo(), 'update'], [$uploaded_tracks['candidateRecords']]);
                break;
            case 'remove':
                call_user_func_array([$this->getRepo(), 'delete'], [array_pluck($uploaded_tracks['candidateRecords'], 'token_old')]);
                break;
            default :
                throw new InvalidRecordActionException();
        }

        return $this->logger->getParentRecordId();
    }


    /**
     * @param $file
     *
     * @return \Illuminate\Validation\Validator
     */
    protected function validateFile($file)
    {
        $file_type = ['text/csv' => $file];
        $rules     = ['mime' => 'csv',]; //mimes:jpeg,bmp,png and for max size max:10000

        // doing the validation, passing post data, rules and the messages
        return \Validator::make($file_type, $rules);
    }

    /**
     * @return boolean
     */
    public function isValidationFailedTracksExist()
    {
        return $this->validation_failed_tracks_exist;
    }


    /**
     * Clears the manipulation data stored in the session
     */
    protected function cleanSession()
    {
        Session::remove($this->manipulation_name);
    }


    /**
     * Add to session
     * Will save what is currently in the session instead of replacing what ever it is in there
     *
     * @param $key
     * @param $value
     */
    protected function sessionPutConcat($key, $value)
    {
        $current_session = Session::get($this->manipulation_name);
        $current_session[$key] = $value;

        Session::put($this->manipulation_name, $current_session);
    }


    /**
     * @param $csv_tracks
     *
     * @return bool
     */
    protected function clearIllegalTracks(&$csv_tracks)
    {
        $uploaded_tracks = Session::get($this->manipulation_name);
        $rules = $this->getManipulationRules($uploaded_tracks['action']);

        $valid_tracks_flag = true;

        foreach ($csv_tracks as $key => $track) {
            $verdict = \Validator::make($track, $rules);

            // If the validation failed remove the track from the approval list
            // and log the error
            if ($verdict->fails()) {
                $this->createTpLoggerInstance(true);
                $this->logger->warning([$verdict->errors(), $track]);
                unset($csv_tracks[$key]);
                $valid_tracks_flag = false;
            }
        }

        return $valid_tracks_flag;
    }


    /**
     * Creates an instance of the logger and saves it to the tp_logger variable
     *
     * @return mixed
     */
    protected function createTpLoggerInstance($concatenate_log = false)
    {
        if (isset($this->logger)) {
            return $this->logger;
        }
        elseif (!TpLogger::isActive()) {
            $this->logger = new TpLogger($this->system_id, null, $concatenate_log);
        } else {
            $this->logger = TpLogger::getInstance();
        }
        $this->logger->info($this->manipulation_name);
    }

    /**
     * Returns the right managing repo
     *
     * @return Repo
     */
    protected abstract function getRepo();


    /**
     * Validate Csv Headers
     * Makes sure no empty columns were passed
     * * This happened when extra commas are passed in the first line
     *
     * @param $fields
     *
     * @return bool
     */
    private function validateCsvHeaders($fields)
    {
        foreach ($fields as $column) {
            if (empty($column)) {
                $this->logger->error(["Columns should not be empty", $fields]);
                return false;
            }
        }

        return true;
    }
}