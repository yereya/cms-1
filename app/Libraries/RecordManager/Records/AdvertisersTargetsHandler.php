<?php namespace App\Libraries\RecordManager\Records;


use App\Entities\Repositories\Bo\Advertisers\TargetsRepo;
use App\Entities\Repositories\Bo\Advertisers\TracksRepo;

class AdvertisersTargetsHandler extends BaseHandler
{
    protected $manipulation_name = 'AdvertisersTargetsManipulation';

    /**
     * System id
     *
     * @var int
     */
    protected $system_id = 82;

    /**
     * @param $manipulation_action
     *
     * @return array
     */
    public function getManipulationRules($manipulation_action)
    {
        // TODO right here, update the manipulation rules
        $rules = [
            'add'    => [
                'event'                  => "required",
                'token'                  => "required",
                'currency'               => "required|alpha",
                'date'                   => "required|date",
                'commission_amount'      => "required",
                'amount'                 => "numeric",
                'trx_id'                 => "required",
                'base_commission_amount' => "numeric",
            ],
            'update' => [
                'token'                  => "required",
                'currency'               => "alpha|required_without_all:amount,commission_amount,date,event",
                'amount'                 => "numeric|required_without_all:currency,commission_amount,date,event",
                'commission_amount'      => "numeric|required_without_all:amount,currency,date,event",
                'date'                   => "date|required_without_all:amount,currency,commission_amount,event",
                'event'                  => "alpha|required_without_all:amount,commission_amount,date,currency",
                'base_commission_amount' => "numeric",
            ],
            'remove' => [
                'token' => "required",
            ],
        ];

        return $rules[$manipulation_action];
    }


    /**
     * Returns the right managing repo
     *
     * @return string
     */
    protected function getRepo()
    {
        return TargetsRepo::class;
    }


    protected function buildAddCandidates($items)
    {
        $tracks = call_user_func_array([$this->getRepo(), 'getTracksByToken'], [array_pluck($items, 'token')]);

        foreach ($items as $item_idx => $item) {
            $db_track_item_found = false;
            foreach (object2Array($tracks) as $db_track) {
                if ($item['token'] == $db_track['token_old']) {
                    $db_track_item_found = true;
                    unset($db_track['token_old']);
                    continue;
                }
            }

            // If we could not find the token in the db
            if (!$db_track_item_found) {
                $this->logger->warning(['Token not found', $items[$item_idx]]);
                unset($items[$item_idx]);
            } else {
                ksort($items[$item_idx], SORT_STRING);
            }
        }

        return $items;
    }


    /**
     * @param $uploaded_tracks
     *
     * @return mixed
     */
    protected function buildUpdateCandidates($uploaded_tracks)
    {
        $uploaded_tokens = array_pluck($uploaded_tracks, 'token');
        $db_tracks       = call_user_func_array([$this->getRepo(), 'getTracksByToken'], [$uploaded_tokens]);

        foreach ($uploaded_tracks as $upload_idx => $uploaded) {
            $db_track_item_found = false;
            foreach (object2Array($db_tracks) as $db_track) {
                if ($uploaded['token'] == $db_track['token_old']) {
                    $db_track_item_found = true;
                    unset($db_track['token_old']);
                    $uploaded_tracks[$upload_idx] = array_merge($uploaded_tracks[$upload_idx], $db_track);
                }
            }

            // If we could not find the token in the db
            if (!$db_track_item_found) {
                $this->logger->warning(['Token not found', $uploaded_tracks[$upload_idx]]);
                unset($uploaded_tracks[$upload_idx]);
            } else {
                ksort($uploaded_tracks[$upload_idx], SORT_STRING);
            }
        }

        return $uploaded_tracks;
    }


    /**
     * @param $uploaded_tracks
     *
     * @return mixed
     */
    protected function buildRemoveCandidates($uploaded_tracks)
    {
        $db_tracks = call_user_func_array([$this->getRepo(), 'getTracksByToken'],
            [array_pluck($uploaded_tracks, 'token')]);
        $db_tracks = object2Array($db_tracks);

        if (count($db_tracks)) {
            $this->logger->info(["Found " . count($db_tracks) . " tracks out of the tokens you passed", $db_tracks]);
        } else {
            $this->logger->notice(['No tracks were found in the database to match your tokens', $db_tracks]);
        }

        return $db_tracks;
    }
}