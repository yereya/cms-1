<?php namespace App\Libraries\RecordManager\Records;


use App\Entities\Repositories\Bo\Advertisers\CampaignsRepo;

class AccountCampaignsHandler extends BaseHandler
{
    protected $manipulation_name = 'CampaignsRecordsHandler';

    /**
     * System id
     *
     * @var int
     */
    protected $system_id = 65;


    /**
     * @param $manipulation_action
     *
     * @return array
     */
    public function getManipulationRules($manipulation_action)
    {
        $rules = [
            'update' => [
                'campaign_id' => "required",
                'segment'     => "string|required_without_all:country,target_roi,campaign_quality",
                'country'     => "string|required_without_all:segment,target_roi,campaign_quality",
                'target_roi'  => "numeric|required_without_all:country,segment,campaign_quality",
                'campaign_quality' => "string|required_without_all:segment,country,target_roi"
            ],
        ];

        return $rules[$manipulation_action];
    }

    /**
     * @param $uploaded_tracks
     *
     * @return mixed
     */
    protected function buildUpdateCandidates($uploaded_tracks)
    {
        $campaign_ids = array_pluck($uploaded_tracks, 'campaign_id');
        $db_campaigns = call_user_func([$this->getRepo(), 'getByAccountIds'], $campaign_ids);
        if (!count($db_campaigns)) {
            return [];
        }
        $db_campaigns = object2Array($db_campaigns);

        foreach ($uploaded_tracks as $upload_idx => &$uploaded) {
            $db_item_found = false;

            foreach ($db_campaigns as $db_campaign_key => $db_campaign) {
                if ($uploaded['campaign_id'] == $db_campaign['campaign_id_old']) {
                    $db_item_found = true;
                    unset($db_campaign['campaign_id_old']);
                    $uploaded = array_merge($uploaded, $db_campaign);
                    unset($db_campaigns[$db_campaign_key]);
                }
            }

            // If we could not find the token in the db
            if (!$db_item_found) {
                $this->logger->warning(['Token not found', $uploaded_tracks[$upload_idx]]);
                unset($uploaded_tracks[$upload_idx]);
            } else {
                ksort($uploaded_tracks[$upload_idx], SORT_STRING);
            }
        }

        return $uploaded_tracks;
    }

    /**
     * Returns the right managing repo
     *
     * @return string
     */
    protected function getRepo()
    {
        return CampaignsRepo::class;
    }
}