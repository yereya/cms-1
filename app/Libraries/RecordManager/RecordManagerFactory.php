<?php namespace App\Libraries\RecordManager;


use App\Libraries\RecordManager\Exceptions\InvalidHandlerRequestedException;
use App\Libraries\RecordManager\Records\AdvertisersTargetsHandler;
use App\Libraries\RecordManager\Records\AdvertiserTracksHandler;
use App\Libraries\RecordManager\Records\AuctionInsightsHandler;
use App\Libraries\RecordManager\Records\BaseHandler;
use App\Libraries\RecordManager\Records\AccountCampaignsHandler;
use App\Libraries\RecordManager\Records\PublishersRecordsHandler;

class RecordManagerFactory
{

    /**
     * Instantiates the handler requested
     * 
     * @param $handler_name
     *
     * @return mixed
     * @throws InvalidHandlerRequestedException
     */
    public static function create($handler_name) : BaseHandler
    {
        switch ($handler_name) {
            case "AuctionInsights":
                return new AuctionInsightsHandler($handler_name);

            case "AdvertiserTracks" :
                return new AdvertiserTracksHandler($handler_name);

            case "PublishersRecords" :
                return new PublishersRecordsHandler($handler_name);

            case "AccountCampaigns" :
                return new AccountCampaignsHandler($handler_name);

            case "AdvertisersTargets" :
                return new AdvertisersTargetsHandler($handler_name);

            default :
                throw new InvalidHandlerRequestedException("No handler by the name [$handler_name] was found.");
        }
    }
}