<?php namespace App\Libraries\RecordManager\Contracts;

/**
 * Class Handler
 * @package App\Libraries\RecordManager\Contracts
 */
interface Handler
{

    public function save($csv_string);

    
    public function getManipulationRules($manipulation_action);
    
    
    public function fetchCandidates();
}