<?php namespace App\Libraries\RecordManager\Exceptions;

/**
 * Class InvalidDateRangeException
 *
 * @package App\Libraries\RecordManager\Exceptions
 */
class InvalidRecordActionException extends \Exception
{
    //
}