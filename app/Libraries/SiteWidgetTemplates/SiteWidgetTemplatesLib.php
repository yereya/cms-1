<?php

namespace App\Libraries\SiteWidgetTemplates;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Widgets\SiteWidgetTemplate;

class SiteWidgetTemplatesLib
{
    /**
     * @var Site
     */
    private $site;

    /**
     * @var
     */
    private $widgets_view_path;

    /**
     * SiteWidgetTemplatesLib constructor.
     * @param Site $site
     * @param $widgets_view_path
     */
    public function __construct(Site $site, $widgets_view_path)
    {
        $this->site = $site;
        $this->widgets_view_path = $widgets_view_path;
    }

    /**
     * Create All Widget Templates
     *
     * @param $site
     */
    public function createAllWidgetTemplates()
    {
        $widget_templates = SiteWidgetTemplate::with('widget')
            ->where('site_id', $this->site->id)
            ->get();

        foreach ($widget_templates as $widget_template) {
            $this->createWidgetTemplateFile($widget_template);
        }
    }

    /**
     * Create Widget Template File
     *
     * @param SiteWidgetTemplate $widget_template
     */
    public function createWidgetTemplateFile(SiteWidgetTemplate $widget_template)
    {
        $path = $this->widgets_view_path . '/' . $widget_template->widget->controller;
        $this->createDirIfNotExists($path);
        $filename = $path . "/{$widget_template->file_name}.blade.php";

        $template_content = $this->getTemplateContent($widget_template);
        file_put_contents($filename, $template_content);
    }

    /**
     * Get Template Content
     *
     * @param $site_widget_template
     * @return string
     */
    private function getTemplateContent($site_widget_template)
    {
        $content = $this->getTemplateHeader();
        $content .= $site_widget_template->blade;
        $content .= $this->getTemplateFooter();

        return $content;
    }

    /**
     * Get Template Header
     *
     * @return string
     */
    private function getTemplateHeader()
    {
        return "@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'open'])" . PHP_EOL;
    }

    /**
     * Get Template Footer
     *
     * @return string
     */
    private function getTemplateFooter()
    {
        return PHP_EOL . "@include('WidgetsLib::Assets.widget-wrapper', ['part' => 'close'])";
    }


    /**
     * Create Dir If Not Exists
     *
     * @param $path
     */
    private function createDirIfNotExists($path)
    {
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
    }
}
