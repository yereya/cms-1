<?php namespace App\Libraries\LPValidator;

use App\Entities\Models\HolsteredQuery;
use App\Libraries\Mail;
use Closure;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;

class LPValidatorLib
{

    /**
     * @var array
     */
    protected static $responses = [];
    protected static $failures  = [];

    public static function validate(array $lp_ids = [], $query_id = 51, array $atts = [])
    {
        $atts = filterAndSetParams([
            'email' => false
        ], $atts);

        $urls   = self::loadURLs($lp_ids, $query_id);
        $client = new Client();

        $pool = self::generateIteratorPool($client, $urls);

        // Initiate the transfers and create a promise
        $promise = $pool->promise();

        // Force the pool of requests to complete.
        $promise->wait();

        self::saveResponsesToDb();

        // Email the results when an email is passed
        self::sendFailuresEmails($atts);

        return self::$responses;
    }


    /**
     * Has Failures
     *
     * @return bool
     */
    private static function hasFailures()
    {
        if (empty(self::$failures)) {
            return false;
        } else {
            return true;
        }
    }


    /**
     * Load URLs
     * loads landing page's URL, name, campaign_name, brand_name, advertiser_name
     *
     * @param $lp_ids
     * @param $query_id
     * @return mixed
     */
    private static function loadURLs($lp_ids, $query_id)
    {
        $query = HolsteredQuery::find($query_id);

        if (!empty($lp_ids)) {
            $lp_ids = implode(', ', $lp_ids);
            $query->query .= " AND l.id IN ($lp_ids)";
        }

        return DB::connection($query->db)->select($query->query);
    }

    /**
     * Generate Requests
     * generates requests
     *
     * @param $urls
     *
     * @return Closure
     */
    private static function generateRequests($urls)
    {
        return function () use ($urls) {
            foreach ($urls as $url) {
                yield new Request('GET', $url->lp_url);
            }
        };
    }

    /**
     * @param $client
     * @param $urls
     *
     * @return Pool
     */
    private static function generateIteratorPool($client, $urls)
    {
        $requests = self::generateRequests($urls);

        return new Pool($client, $requests(), [
            'concurrency' => 24,
            'options'     => [
                'timeout' => 20,
                'allow_redirects' => [
                    'max' => 10,
                ],
            ],

            'fulfilled' => function ($response, $index) use ($urls) {
                // this is delivered each successful response

                $row = object2Array($urls[$index]);

                if (self::checkFor404OrError((string) $response->getBody())) {
                    $row['status']    = 404;
                    $row['reason']    = $response->getReasonPhrase();
                    self::$failures[] = $row;
                } else {
                    $row['status'] = $response->getStatusCode();
                    $row['reason'] = $response->getReasonPhrase();
                }

                self::$responses[] = $row;
            },

            'rejected' => function ($reason, $index) use ($urls) {
                // this is delivered each failed request

                $row = object2Array($urls[$index]);

                $response = $reason->getResponse();
                if ($response) {
                    $row['reason'] = $response->getReasonPhrase();
                    $row['status'] = $response->getStatusCode();
                } else {
                    $row['reason'] = $reason->getMessage();
                    $row['status'] = 'no status returned';
                }

                self::$failures[]  = $row;
                self::$responses[] = $row;
            },
        ]);
    }

    /**
     * @param array $atts
     */
    private static function sendFailuresEmails(array $atts)
    {
        if (!empty($atts['email']) && self::hasFailures()) {
            Mail::tplBlank($atts['email'], [
                'title'   => 'Landing Page',
                'content' => self::$failures,
            ]);
        }
    }

    /**
     * Save Responses To Db
     */
    private static function saveResponsesToDb()
    {
        DB::connection('alert' )->table('landing_pages')->insert(self::$responses);
    }


    /**
     * @param string $html
     * @return bool
     */
    private static function checkFor404OrError($html)
    {
        $error_msgs = [
            'page not found',
            'error 404',
            'oops!'
        ];

        foreach ($error_msgs as $error_msg) {
            if (stripos($html, $error_msg) !== false) {
                return true;
            }
        }

        return false;
    }

}