<?php

namespace App\Libraries;

use App\Entities\Models\Reports\Queries\ReportsQueryField;
use Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class ReportQueryBuilder
 * @package App\Libraries
 */
class ReportQueryBuilder
{

    /**
     * @var null|string
     */
    protected $tables = null;

    /**
     * @var int
     */
    protected $query_id = 1;

    /**
     * ReportsFilter constructor.
     * @param null $tables
     */
    public function __construct($tables = null)
    {
        $this->tables = $tables;
    }

    /**
     * @return array
     */
    public static function operations()
    {
        return ['1' => '=', '2' => '<', '3' => '>', '4' => '<>', '5' => 'LIKE', '6' => 'NOT LIKE'];
    }


    /**
     * @param Builder $query_fields
     * @param null $filters
     * @return mixed
     */
    public static function buildPortletNavParams(Builder $query_fields, $filters = null)
    {
        $fields = $query_fields->select('display_name as text', 'id', 'order', 'clause_type', 'input_type as type', 'permission')
            ->orderBy('clause_type')->orderBy('order', 'asc')->orderBy('display_name', 'asc')->get();

        $field_params['where'] = $fields->filter(function ($item) {
            return $item->clause_type == 'where';
        });
        $field_params['having'] = $fields->filter(function ($item) {
            return $item->clause_type == 'having';
        });
        $field_params['select'] = $fields->filter(function ($item) {
            return $item->clause_type == 'select' && (auth()->user()->can($item->permission) || is_null($item->permission));
        });
        $field_params['order_by'] = $fields->filter(function ($item) {
            return $item->clause_type == 'order_by';
        });
        $field_params['group_by'] = $fields->filter(function ($item) {
            return $item->clause_type == 'group_by';
        });
        $field_params['variable'] = $fields->filter(function ($item) {
            return $item->clause_type == 'variable';
        });

        if (!is_null($filters)) {
            $field_params['select'] = self::resortSelectColumns($field_params['select'], $filters['select']);
        }

        return $field_params;
    }

    /**
     * Sort select collection by saved filters columns
     *
     * @param $selects - original select columns
     * @param $filters - user saved select columns
     * @return Collection - new sortable collection
     */
    private static function resortSelectColumns($selects, $filters) {
        $filter_orders = $filters->keyBy(['query_field_id']);
        $current_orders = $selects->keyBy(['id']);

        $sorted = new Collection();
        foreach ($filter_orders as $key => $collection) {
            $sorted->push($current_orders->get($key));
            $current_orders->forget($key);
        }

        if (!empty($current_orders)) {
            foreach ($current_orders as $key => $collection) {
                $sorted->push($collection);
            }
        }

        return $sorted;
    }

    /**
     * Return columns name from current table
     *
     * @return array
     */
    public function getColumns()
    {
        $cols = DB::table($this->tables)->select('field_name')->where('query_id', $this->query_id)->get();

        return $cols->each(function ($item) {
            return $item->field_name;
        });
    }

    /**
     * Get values
     *
     * @param $selector
     * @param $search
     * @param $multi_select
     *
     * @return array
     */
    public function getValues($selector, $search, $multi_select)
    {
        if ($search == -1) {
            return null;
        }

        $row = ReportsQueryField::find($selector);
        $query = $this->buildQuery($row->getAttribute('query'), '%' . $search . '%');
        $display_name = $row->getAttribute('display_name');

        //TODO - see todo isNumeric method
        // Only if the search field is numeric and
        // also we need to find primary key id then query field should be id in in the specific model
        if(is_numeric($search) && !empty($display_name) && strpos(strtolower($display_name), 'id') != false ){
            $query = $this->buildQuery($row->getAttribute('model_query'), $search);
        }

        return $this->connectToDb('bo', $query);
    }

    /**
     * TODO - need create flag in where blade if this name or ids
     * after this remove this method
     *
     * @param $search
     *
     * @return bool
     */
    private function isNumeric($search) {
        $arr = explode(',', $search);

        $is_numeric = true;
        foreach ($arr as $value) {
            if (!is_numeric($value)) {
                $is_numeric = false;
            }
        }

        return $is_numeric;
    }

    /**
     * Connect to db and return query result
     *
     * @param $connection
     * @param $query
     * @return array
     */
    private function connectToDb($connection, $query)
    {
        $connection = DB::connection($connection);
        try {
            $result = $connection->select($query);

            $connection->disconnect($connection);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return [];
        }

        return $result;
    }

    /**
     * Change short code in query
     *
     * @param $query
     * @param $search
     * @return mixed
     */
    private function buildQuery($query, $search)
    {
        $model = str_replace('/', '\\', $query);
        $model = str_replace('[like]', $search, $model);
        $model = str_replace('[user_id]', Auth::user()->id, $model);
        if (!is_null(session('user_advertiser_access'))) {
            $model = str_replace('[user_advertiser_access]', implode(',', session('user_advertiser_access')), $model);
        }

        if (!is_null(session('user_publisher_access'))) {
            $model = str_replace('[user_publisher_access]', implode(',', session('user_publisher_access')), $model);
        }

        if (!is_null(session('user_account_access'))) {
            $model = str_replace('[user_account_access]', implode(',', session('user_account_access')), $model);
        }

        if (!is_null(session('user_source_account_access'))) {
            $model = str_replace('[user_source_account_access]', implode(',', session('user_source_account_access')), $model);
        }

        return $model;
    }

    /**
     * Return only field name by ID
     *
     * @param $id
     * @param string $field
     *
     * @return mixed
     */
    public static function getFieldName($id, $field = 'field_name')
    {
        $row = ReportsQueryField::find($id);

        return $row->{$field};
    }

    /**
     * Return ReportsQueryField obj by field name
     *
     * @param $field_name
     * @param $query_id
     * @return mixed
     */
    public static function getByFieldName($field_name, $query_id) {
        return ReportsQueryField::where('field_name', $field_name)->where('query_id', $query_id)->first();
    }

    /**
     * Return value
     *
     * @param int         $query_id
     *
     * @return null
     */
    public static function getDefaultWhereValue(int $query_id)
    {
        $field_name = self::getDefaultValueByType($query_id, 'where');
        if (!empty($field_name)) {
            $row = ReportsQueryField::where('field_name', $field_name['where'])
                ->where('query_id', $query_id)
                ->where('clause_type','where')
                ->first();

            if ($row) {
                return ['where' => $row->id, 'value' => $field_name['value'] ?? null];
            }
        } else {
            return null;
        }
    }

    /**
     * Return default group by
     *
     * @param int $query_id
     *
     * @return int
     */
    public static function getDefaultGroupValue(int $query_id) {
        $field_name = self::getDefaultValueByType($query_id, 'group_by');

        $row = ReportsQueryField::where('field_name', $field_name)
            ->where('query_id', $query_id)
            ->where('clause_type','group_by')->first();

        return $row->id ?? 0;
    }

    private static function getDefaultValueByType(int $query_id, string $type) {
        switch ($type) {
            case 'group_by':
                return self::groupByDefaultName($query_id);
            case 'where':
                return self::whereByDefaultName($query_id);
            default:
                return self::groupByDefaultName($query_id);
        }
    }

    /**
     * sort array
     *
     * @param $rows
     * @return array|null
     */
    private function sortToArray($rows)
    {
        $sort = null;
        foreach ($rows as $row) {
            $sort[] = [
                'id' => $row->id,
                'text' => $row->display_name,
                'type' => $row->input_type
            ];
        }

        return $sort;
    }

    /**
     * Default Fields to query, to example:
     * All fields by default added to query select
     *
     * @param $query_id
     * @param string $clause - by default select
     * @param string $delimiter - by default ','
     * @return string - 'apple,lemon,blueberry'
     */
    final public static function defaultFields($query_id, $clause = 'select', $delimiter = ',')
    {
        $fields = ReportsQueryField::where('query_id', $query_id)
            ->where('clause_type', $clause)->orderBy('order', 'asc')->get()
            ->pluck('field_name')->filter()->toArray();

        return implode($delimiter, $fields);
    }

    /**
     * Return tooltips to headers in datatable
     *
     * @param $query_id
     * @return array
     */
    final public static function getTooltipLabels($query_id)
    {
        $results = ReportsQueryField::select('display_name', 'label_description')
            ->where('query_id', $query_id)
            ->whereNotNull('label_description')->get();

        $tooltips = [];
        foreach ($results as $result) {
            $display_name = str_replace(' ', '_', $result->display_name);
            $tooltips[strtolower($display_name)] = $result->label_description;
        }

        return $tooltips;
    }

    /**
     * build the query string for total summary row
     *
     * @param $query
     * @param $summary_fields
     * @return string
     */
    public static function buildSummaryQuery($query, $summary_fields)
    {
        $summary_query = 'SELECT '; // chain the select summary fields

        foreach ($summary_fields as $key => $select) {
            if ($key > 0) {
                $summary_query .= ',';
            }
            $summary_query .= $select['select'] . ' `' . $select['display'] . '`';

        }

        $summary_query .= ' FROM ('; // open the wrapping FROM
        $summary_query .= $query;
        $summary_query .= ') as t1';//must give a name for the inner FROM to differentiate the inner
        // table from the upper one

        return $summary_query;
    }

    /**
     * return summary fields and display names
     *
     * @param $id
     * @return array
     */
    public static function getSummaryFields($id)
    {
        $row = ReportsQueryField::find($id);
        return [
            'select' => $row->summary_field,
            'display' => $row->display_name
        ];
    }

    /**
     * Get Default Group By value by report
     *
     * @param $report_id
     *
     * @return array
     */
    private static function groupByDefaultName($report_id) {
        $defaults = [
            1 => 'advertiser_name',
            2 => 'account_name',
            3 => 's_advertiser_name',
            4 => 'advertiser_name',
            5 => 'advertiser_name',
            6 => 'advertiser_name',
            7 => 'advertiser_name',
            18 => 's_advertiser_name',
            26 => 'advertiser_name',
            34 => 's_advertiser_name'

        ];

        return $defaults[$report_id] ?? '';
    }

    /**
     * Get Defaults Where
     *
     * @param $report_id
     *
     * @return mixed
     */
    private static function whereByDefaultName($report_id) {
        $defaults = [
            1 => [
                'where' => 'advertiser_id'
            ],
            16 => [
                'where' => 'first_occurence', 'value' => 1
            ],
            17 => [
                'where' => 'advertiser_name'
            ],
            18 => [
                'where' => 'advertiser_name'
            ],
            26 => [
                'where' => 'advertiser_id'
            ]
        ];

        return $defaults[$report_id] ?? '';
    }
}
