<?php namespace App\Libraries\ShortCodes\Contracts;

interface ShortCodes
{
    /**
     * Set Haystack
     *
     * @param array|string $haystack
     *
     * @return void
     */
    public function setHaystack($haystack);

}