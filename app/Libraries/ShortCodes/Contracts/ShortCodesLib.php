<?php namespace App\Libraries\ShortCodes\Contracts;

/**
 * Interface Manager
 *
 * @package App\Libraries\ShortCodes\Contracts
 */
interface ShortCodesLib
{
    /**
     * Set Short Codes
     * Replaces the short codes inside the subject string|array using the available data in the haystack
     *
     * @param $subject
     * @param $haystack
     *
     * @return array|mixed
     */
    public function setShortCodes($subject, $haystack = null);


    /**
     * Sets the haystack
     *
     * @param $haystack
     */
    public function setHaystack($haystack);

    /**
     * Get Short Codes Atts
     *
     * @param $string
     *
     * @return array|void
     */
    public function getShortCodesAtts($string);

    /**
     * Do Short Code
     *
     * @param $short_code
     *
     * @return bool|string
     */
    public function doShortCode($short_code);
}