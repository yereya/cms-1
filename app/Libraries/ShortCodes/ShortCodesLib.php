<?php namespace App\Libraries\ShortCodes;

use App\Libraries\ShortCodes\Contracts\ShortCodes as ShortCodesContract;
use App\Libraries\ShortCodes\Contracts\ShortCodesLib as ShortCodesLibContract;

/**
 * Class ShortCodesLib
 *
 * @package App\Libraries
 */
class ShortCodesLib implements ShortCodesLibContract
{
    /**
     * @var ShortCodesContract $short_codes
     */
    private $short_codes;

    /**
     * @var TpLogger $logger
     */
    private $logger;

    /**
     * Manager constructor.
     *
     * @param ShortCodesContract $short_codes
     * @param Boolean $useLogger
     */
    public function __construct(ShortCodesContract $short_codes = null,$useLogger = false)
    {
        if (!is_null($short_codes)) {
            $this->short_codes = $short_codes;
        } else {
            $this->short_codes = new ShortCodes();
        }

        if ($useLogger && TpLogger::isActive()) {
            $this->logger = TpLogger::getInstance();
            if (method_exists($this->short_codes,'setLogger')) {
                $this->short_codes->setLogger($this->logger);
            }
        }
    }

    /**
     * Set Short Codes
     * Replaces the short codes inside the subject string|array using the available data in the haystack
     *
     * @param $subject
     * @param $haystack
     *
     * @return array|mixed
     */
    public function setShortCodes($subject, $haystack = null)
    {
        if ($haystack) {
            $this->short_codes->setHaystack($haystack);
        }

        // Run through the subject and search for short codes to "Set"
        if (is_array($subject)) {
            array_walk_recursive($subject, function (&$item) {
                $item = $this->replaceShortCodes($item);
            });
        } else {
            $subject = $this->replaceShortCodes($subject);
        }

        return $subject;
    }

    /**
     * Set Haystack
     *
     * @param $haystack
     */
    public function setHaystack($haystack)
    {
        $this->short_codes->setHaystack($haystack);
    }


    /**
     * Will replace the shortcode inside the string with it's parsed value
     *
     * @param $string
     *
     * @return mixed
     */
    private function replaceShortCodes($string)
    {
        $short_codes = $this->getShortCodesAtts($string);

        if (!$short_codes) {
            return $string;
        }

        foreach ($short_codes as $param) {
            $result = $this->doShortCode($param);

            if ($result !== false) {
                $string = str_replace($param['raw'], $result, $string);
            }
        }

        // used for special short codes like [or]
        $string = $this->resolveShortCodesConditions($string);

        return $string;
    }

    /**
     * Resolve Processed Value Condition
     * Will search for the [or] short code
     * and return the first segment that has value
     *
     * @param $string
     *
     * @return mixed
     */
    private function resolveShortCodesConditions($string)
    {
        if (!method_exists($this->short_codes, 'sc_or')) {
            return $string;
        }

        $or_sign = $this->short_codes->sc_or();
        if (strpos($string, $or_sign) !== false) {
            $segments = explode($or_sign, $string);

            // If we have any segments lets return the first valid one
            foreach ((array) $segments as $segment) {
                if (!empty($segment)) {
                    return $segment;
                }
            }
        } else {
            return $string;
        }

        return '';
    }

    /**
     * Get Short Codes Atts
     *
     * @param $string
     *
     * @return array|void
     */
    public function getShortCodesAtts($string)
    {
        if (false === strpos($string, '[')) {
            return;
        }

        $pattern       = $this->getShortCodeRegex();
        $match_results = preg_match_all("/$pattern/s", $string, $matches, PREG_SET_ORDER);

        if (!$match_results) {
            return;
        }

        return $this->cleanMatches($matches);
    }

    /**
     * Do Short Code
     * Will activate the shortcode method
     *
     * @param      $short_code
     * @param null $atts
     *
     * @return bool|string
     * @throws \Exception
     *
     */
    public function doShortCode($short_code, $atts = null)
    {
        if (!isset($short_code['name'])) {
            return false;
        }

        $short_code['atts'] = (isset($short_code['atts'])) ? $short_code['atts'] : [];

        $short_code_method = 'sc_' . $short_code['name'];

        if (method_exists($this->short_codes, $short_code_method)) {
            try {
                $attributes = [$short_code['atts']];
                if ($atts) {
                    $attributes[] = $atts;
                }

                return call_user_func_array([$this->short_codes, $short_code_method], $attributes);
            } catch (\Exception $e) {
                if ($this->logger) {
                    $this->logger->warning(['Failed to set short code', $e]);
                }
                throw $e;
            }
        }

        return false;
    }

    /**
     * Uses the regex results to put out
     *
     * @param $matches
     *
     * @return array
     */
    private function cleanMatches($matches)
    {
        $short_codes = [];
        foreach ((array) $matches as $match) {
            $_short_codes['raw']  = $match[0];
            $_short_codes['name'] = $match[2];
            $_short_codes['atts'] = htmlAttsToArray($match[3]); // Build attributes
            $_short_codes['val']  = htmlAttsToArray($match[5]); // Inner shortcode string (might also be another shortcode

            // Decide the type of the short code
            // variables are used to find
            if ($match[2][0] == '$') {
                $_short_codes['type'] = 'variable';
            } else {
                $_short_codes['type'] = 'function';
            }

            $short_codes[] = $_short_codes;
        }

        return $short_codes;
    }

    /**
     * Retrieve the shortcode regular expression for searching.
     * The regular expression combines the shortcode tags in the regular expression
     * in a regex class.
     * The regular expression contains 6 different sub matches to help with parsing.
     * 1 - An extra [ to allow for escaping shortcodes with double [[]]
     * 2 - The shortcode name
     * 3 - The shortcode argument list
     * 4 - The self closing /
     * 5 - The content of a shortcode when it wraps some content.
     * 6 - An extra ] to allow for escaping shortcodes with double [[]]
     *
     * @return string The shortcode search regular expression
     */
    private function getShortCodeRegex()
    {
        return '\\['                                // Opening bracket
               . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
               . "([^\W].+?)"                       // 2: Shortcode name (Edited: [^\W] exclude certain cases where shortcode name doesn't start with alphabetical char
               . '(?![\\w-])'                       // Not followed by word character or hyphen
               . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
               . '[^\\]\\/]*'                       // Not a closing bracket or forward slash
               . '(?:' . '\\/(?!\\])'               // A forward slash not followed by a closing bracket
               . '[^\\]\\/]*'                       // Not a closing bracket or forward slash
               . ')*?' . ')' . '(?:' . '(\\/)'      // 4: Self closing tag ...
               . '\\]'                              // ... and closing bracket
               . '|' . '\\]'                        // Closing bracket
               . '(?:' . '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
               . '[^\\[]*+'                         // Not an opening bracket
               . '(?:' . '\\[(?!\\/\\2\\])'         // An opening bracket not followed by the closing shortcode tag
               . '[^\\[]*+'                         // Not an opening bracket
               . ')*+' . ')' . '\\[\\/\\2\\]'       // Closing shortcode tag
               . ')?' . ')' . '(\\]?)';             // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    }
}