<?php

namespace App\Libraries\ShortCodes\Compilers;

class ShortcodeCompiler
{
    /**
     * Enabled state
     *
     * @var boolean
     */
    protected $enabled;

    /**
     * Enable strip state
     *
     * @var boolean
     */
    protected $strip = false;

    /**
     * @ver mixed
     */
    protected $driver;

    /**
     * @param $driver
     */
    public function __construct($driver)
    {
        $this->enabled = false;
        $this->driver = $driver;
    }

    /**
     * Enable
     *
     * @return void
     */
    public function enable()
    {
        $this->enabled = true;
    }

    /**
     * Disable
     *
     * @return void
     */
    public function disable()
    {
        $this->enabled = false;
    }

    /**
     * Compile the contents
     *
     * @param  string $value
     *
     * @return string
     */
    public function compile($value)
    {
        if (!$this->enabled){
            return $value;
        }
        return $this->driver->setShortCodes($value);
    }

}
