<?php namespace App\Libraries\ShortCodes;

use App\Libraries\PageInfo\PagesUrls;
use App\Libraries\ShortCodes\Contracts\ShortCodes as ShortCodesContract;
use App\Libraries\ShortCodes\Exceptions\InvalidShortCodeAttributesException;
use App\Libraries\TpLogger;
use Mockery\Exception;

class ShortCodes implements ShortCodesContract
{

    /**
     * @var array|string $haystack
     */
    protected $haystack;

    /**
     * @var string $separator_sign
     */
    protected $separator_sign = '¬';

    /**
     * @var TpLogger $tp_logger
     */
    protected $tp_logger;

    public function __construct()
    {
        $this->tp_logger = TpLogger::getInstance();
    }

    /**
     * Set Haystack
     *
     * @param array|string $haystack
     */
    public function setHaystack($haystack)
    {
        $this->haystack = $haystack;
    }

    /**
     * Search for a variable/property
     * Also allows you to pass type which will allow you search more specifically
     *
     * @param $atts
     *
     * @return bool|string
     * @throws \Exception
     */
    public function sc_var($atts)
    {
        $atts = filterAndSetParams([
            'col_name'   => '', // the name of the returned variable
            'col_number' => '', // the name of the returned variable
        ], $atts);

        return arraySearchKey($atts['col_name'], $this->haystack);
    }

    /**
     * Runs php date function
     *
     * @param array $atts short code standard attributes
     *
     * @return string
     * @throws \Exception
     */
    public function sc_date($atts)
    {
        $atts = filterAndSetParams([
            'format'          => 'Y-m-d',
            // the php format for the date. http://php.net/manual/en/function.date.php
            'span'            => null,
            // Same as the params accepted in strtotime(). http://php.net/manual/en/function.strtotime.php. Ex: "+1 week", "last Monday"
            'col_name'        => null,
            'col_number'      => null,
            //Remove characters with the matched regex
            'remove_by_regex' => null
        ], $atts);

        $now = time();

        // Try parsing the date
        if ($atts['col_name'] OR $atts['col_number']) {
            $str = $this->sc_var($atts);
            if (!is_string($str)) {
                return date($atts['format'], $now);
            }

            if (!empty($atts['remove_by_regex'])) {
                $str = preg_replace($atts['remove_by_regex'], '', $str);
            }

            if (($timestamp = strtotime($str)) !== false) {
                $now = $timestamp;
            } else {
                throw new \Exception("** Fatal error: could not translate column value (" . $this->sc_var($atts) . ") using strtotime(). " . __CLASS__ . '@' . __FUNCTION__ . '():' . __LINE__ . " " . json_encode($atts));
            }
        }

        if (!is_string($atts['span'])) {
            return date($atts['format'], $now);
        }

        if (isset($atts['span']) && ($timestamp = strtotime($atts['span'], $now)) !== false) {
            $now = $timestamp;
        }

        return date($atts['format'], $now);
    }

    /**
     * Short Code Or
     * used to separate two different string
     * Note: Later on this is used to decide on which string will be returned by exploding on the $separator_sign,
     * and deciding which section should stay by order from top to bottom
     *
     * @return mixed
     */
    public function sc_or()
    {
        return $this->separator_sign . 'or' . $this->separator_sign;
    }

    /**
     * Short Code String Replace
     * Which uses the standard str_replace params
     * http://php.net/manual/en/function.str-replace.php
     *
     * @param $atts
     *
     * @return string
     * @throws \Exception
     */
    public function sc_strreplace($atts)
    {
        $atts = filterAndSetParams([
            'search' => null,   // The value being searched for, otherwise known as the needle.
            // An array may be used to designate multiple needles.

            'replace' => '',    // The replacement value that replaces found search values.
            // An array may be used to designate multiple replacements.

            'col_name'   => null,
            'col_number' => null
        ], $atts);

        if (!is_null($atts['search'])) {
            $subject = $this->sc_var($atts);
            $result  = str_replace($atts['search'], $atts['replace'], $subject);
        } else {
            throw new \Exception("Short code " . $atts['raw'] . " incorrect search param");
        }

        return $result;
    }

    /**
     * Short Code String Position
     * Which uses the standard strpos to return the value of the column
     * if it exists of empty string if not
     * http://php.net/manual/en/function.strpos.php
     *
     * @param $atts
     *
     * @return string
     * @throws \Exception
     */
    public function sc_strpos($atts)
    {
        $atts = filterAndSetParams([
            'needle'          => null,
            // If needle is not a string, it is converted to an integer and applied as the ordinal value of a character.
            'strict'          => 'true',
            // Strict mode would search for exact needle
            'return_val'      => null,
            // Will return the value passed if it is found
            'return_col_name' => null,
            // Returns the value of the column if needle exists in the col_name
            'col_name'        => null,
            'col_number'      => null
        ], $atts);

        if (!is_null($atts['needle'])) {
            $subject = $this->sc_var($atts);

            // resolve string mod
            if ($atts['strict'] === 'false') {
                $subject = strtolower($subject);
            }

            if (strpos($subject, $atts['needle']) !== false) {

                // If needle was found
                // need to decide what to return
                if (!is_null($atts['return_val'])) {
                    return $atts['return_val'];
                } elseif (!is_null($atts['return_col_name'])) {
                    return $this->sc_var(['col_name' => $atts['return_col_name']]);
                } else {
                    return $subject;
                }
            } else {
                return '';
            }
        } else {
            throw new \Exception("Short code " . $atts['raw'] . " incorrect needle param");
        }
    }

    /**
     * Short Code Sub String
     * Which uses the standard substr params
     * http://php.net/manual/en/function.substr.php
     *
     * @param $atts
     *
     * @return string
     * @throws \Exception
     */
    public function sc_substr($atts)
    {
        $atts = filterAndSetParams([
            'start' => null,
            // The input string. Must be one character or longer.

            'length' => null,
            // If start is non-negative, the returned string will start at the start'th position in string,
            // counting from zero. For instance, in the string 'abcdef',
            // the character at position 0 is 'a', the character at position 2 is 'c', and so forth.

            'col_name'   => null,
            'col_number' => null
        ], $atts);

        if (!is_null($atts['start'])) {
            $string = $this->sc_var($atts);
            if (!is_null($atts['length'])) {
                $result = substr($string, $atts['start'], $atts['length']);
            } else {
                $result = substr($string, $atts['start']);
            }
        } else {
            throw new \Exception("Short code " . $atts['raw'] . " incorrect start param");
        }

        return $result;
    }

    /**
     * Short Code basic math equation
     * example - [math col_name="ColName" operator="+" parameter="100" abs="true"]
     * operator - required
     * parameter - required
     * abs - optional
     *
     * @param $atts
     *
     * @return float|int
     */
    public function sc_math($atts)
    {
        $atts   = filterAndSetParams([
            'col_name'  => null, //first parameter
            'operator'  => null, //mathematical operator symbol or name
            'parameter' => null, //second parameter
            'abs'       => false //absolute or not
        ], $atts);
        $result = 0;
        if (!is_null($atts['operator']) && !is_null($atts['parameter'])) {
            $value = $this->sc_var($atts);
            // cast the value according to its type - int or float
            $value = ctype_digit((string)$value) ? (int)$value : (float)$value;
            switch ($atts['operator']) {
                case 'division':
                case '/':
                    $result = $value / ((float)$atts['parameter']);
                    break;
                case 'sum':
                case '+':
                    $result = $value + ((float)$atts['parameter']);
                    break;
                case 'minus':
                case '-':
                    $result = $value - ((float)$atts['parameter']);
                    break;
                case 'multiplication':
                case '*':
                    $result = $value * ((float)$atts['parameter']);
                    break;
            }
        }

        if ($atts['abs']) {
            $result = abs($result);
        }

        return $result;
    }

    /**
     * Url Query Value
     * Will find a query in the haystack and extract the value
     *
     * @param $atts
     *
     * @return string
     * @throws InvalidShortCodeAttributesException
     */
    public function sc_urlQueryValue($atts)
    {
        $atts = filterAndSetParams([
            'key'      => null,   // The key of the query value you are looking for
            // key=val&key2=val the key would be the key of the value we are interested in getting
            'col_name' => null, // the name of key inside the haystack to search for
        ], $atts);

        $this->validateRequiredAttributes($atts, ['key', 'col_name']);

        // Extract the query
        $query_str = $this->sc_var($atts);

        if ($query_str === false && strpos($query_str, $atts['key']) === false) {
            return '';
        }

        $str = str_replace([']', '[', '"'], '', $query_str);
        // Parse the query to array
        parse_str(parse_url($str, PHP_URL_QUERY), $sid);

        return (isset($sid[$atts['key']])) ? $sid[$atts['key']] : '';
    }

    /**
     * Validate Attributes for a short code
     *
     * @param array $atts
     * @param array $keys
     *
     * @throws InvalidShortCodeAttributesException
     */
    protected function validateRequiredAttributes($atts, array $keys)
    {
        foreach ($keys as $key) {
            if (is_null($atts[$key])) {
                throw new InvalidShortCodeAttributesException("Short code {$atts['raw']} incorrect attribute");
            }
        }
    }

    /**
     * Url Query Value
     * Will find a query in the haystack and extract the value
     * TODO return to custom short code
     *
     * @param $atts
     *
     * @return string
     * @throws InvalidShortCodeAttributesException
     */
    public function sc_destinationUrl($atts)
    {
        $atts = filterAndSetParams([
            'key'      => null,   // The key of the query value you are looking for
            // key=val&key2=val the key would be the key of the value we are interested in getting
            'col_name' => null, // the name of key inside the haystack to search for
        ], $atts);

        $this->validateRequiredAttributes($atts, ['key', 'col_name']);

        // Extract the query
        $query_str = $this->sc_var($atts);

        if ($query_str === false && strpos($query_str, $atts['key']) === false) {
            return '';
        }

        $str = str_replace([']', '[', '"'], '', $query_str);

        return isset($str) && strlen($str) > 5 ? $str : '';
    }


    /**
     *  Give the option to remove part of string:
     *  - pass column name
     * - pass character to trigger
     * - pass before needle in case you want to save string before the target needle else you will get only the string
     * after the needle (like in method strstr
     *
     *
     * @param $atts
     * @param $array
     * @return string
     */
    public function sc_strstr($atts, $array)
    {

        $atts = filterAndSetParams([
            'col_name'      => '', // the name of the returned variable
            'col_number'    => '', // the name of the returned variable
            'needle'        => 'T',
            'before_needle' => false
        ], $atts);

        $field = $array[$atts['col_name']];

        return strstr($field, $atts['needle'], $atts['before_needle']);
    }

    /**
     * pregReplace shortcode
     * Uses the preg_replace native function for regex.
     * Documentation: http://php.net/manual/en/function.preg-replace.php
     *
     * @param      $atts
     * @param null $array
     *
     * @return null|string|string[]
     * @throws \Exception
     */
    public function sc_pregReplace($atts, $array = null)
    {
        $atts = filterAndSetParams([
            'pattern'     => null,   // The searched pattern inside the current subject
            'col_name'    => null,
            'col_number'  => null,
            'replacement' => null // The value you wish to replace the pattern matches with
        ], $atts);

        $result = '';

        // Validates the pattern exists
        if (is_null($pattern = $atts['pattern'])) {
            return $result;
        }

        $string = $this->sc_var($atts);


        // Uses the preg_replace function according to the $pattern variable
        $result = preg_replace('/' . $pattern . '/', $atts['replacement'] ?? '', $string);

        return $result ?? '';
    }


    /**
     * Get last index from array
     *  - if string is supplied - explode it by delimiter and then return last key
     *
     * @param $atts
     *
     * @return mixed
     * @throws \Exception
     */
    public function sc_arrayEnd($atts)
    {
        $atts = filterAndSetParams(
            [
                'col_name'   => null,
                'delimiter'  => null,
                'col_number' => null,
                'is_string'  => false
            ]
            , $atts);

        $array = [];

        if (!empty($atts['is_string']) && $atts['delimiter']) {
            $string = $this->sc_var($atts);
            $array  = explode($atts['delimiter'], $string);
        }

        return end($array);
    }
}