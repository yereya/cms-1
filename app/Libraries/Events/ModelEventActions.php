<?php

namespace App\Libraries\Events;

class ModelEventActions
{
    const CREATED = 'created';
    const UPDATED = 'updated';
    const DELETED = 'deleted';
}