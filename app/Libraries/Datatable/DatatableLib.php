<?php

namespace App\Libraries\Datatable;

use App\Libraries\ActionButton\ActionButtonLib;
use App\Libraries\Export\ToCsv;
use Illuminate\Database\Eloquent\Collection;
use Mockery\CountValidator\Exception;

class DatatableLib
{
    /**
     * Default parameters
     *
     * @var array
     */
    private $columns = []; //columns to search in model query
    private $addition_params_columns = []; //if result field not only 
    private $draw; //datatable draw column
    private $recordsTotal; //count results
    private $recordsFiltered; //count results after filter
    private $filters; // all filters array
    private $data = []; // results 
    private $permission_name; // need to build addition fields with html tags
    private $actions = []; // build column actions
    private $offset = 0; // start
    private $limit = 0; // limit
    protected $model = null; // model query
    protected $showStr = 'show'; // parameter to show entities
    protected $orders = []; // sort by column
    private $search = null; // search
    private $searchColumns = []; // search in columns

    private $notFilters = ['_token', 'undefined', 'actions']; // columns not to search

    private $not_url_filters = ['draw', 'columns', 'order', 'start', 'length', 'search', '_token', 'method', 'cols', 'filters', 'permission_name', 'csv'];

    private $download_csv = false;

    /**
     * DatatableLib constructor.
     *
     * insert filters and columns
     *
     * @param $params
     */
    public function __construct($params)
    {
        $this->columns = $this->setColumns($params);
        $this->filters = $this->setFilters($params);
        $this->search = $this->buildSearch($params);
        $this->download = $this->setDownloadCsv($params);

        if (count($this->orders) == 0) {
            $this->orders = $this->setDefaultOrders($params); // fill orders
        }
    }

    /**
     * main function fill and run
     *
     * @param $params
     * @param $transformFunctions !Optional! - array of functions:
     *      transformData - if you want to change the data before saving the file
     *      transformHeader - if you want to change the header before saving the file
     * @return array
     */
    public function fill($params, $transformFunctions=null)
    {
        $this->validate($params); // validate parameters
        $this->buildFilteredQuery(); // add filters to query
        $this->buildSearchQuery(); // add search if exists

        if (!isset($this->recordsFiltered)) {
            $this->recordsFiltered = $this->getRecordsFiltered(); // get counts results
        }

        if ($this->toCsv()) {
            return $this->buildCsv($transformFunctions);
        }
        $this->runAndParse(); // run and return results

        return $this->buildDatatableJson(); // return json in datatable format
    }

    /**
     * Set Records Filtered
     *
     * @param $records
     *
     * @throws \Exception
     */
    public function setRecordsFiltered($records)
    {
        if (!is_numeric($records)) {
            throw new \Exception('Invalid records filtered was passed. ' . __CLASS__ . '@' . __FUNCTION__ . '():' . __LINE__);
        }

        $this->recordsFiltered = (int)$records;
    }

    /**
     * Changed columns name from client datatable to model column name
     *
     * if in needle name is false - removed
     *
     * @param $changes
     */
    public function renderColumns($changes)
    {
        foreach ($changes as $key => $value) {
            $pos = array_search($key, $this->columns);
            $old_name = $key;
            $new_name = null;
            unset($this->columns[$pos]);

            if ($value && $key != 'actions') {
                if (is_array($value)) {
                    $this->columns[$pos] = $value['db_name'];
                    $this->addition_params_columns[$key][$value['db_name']] = $value;
                } else {
                    $this->columns[$pos] = $value;
                }

                $new_name = $this->columns[$pos];
            } else if ($key == 'actions') {
                $this->actions = $value;
            }

            $this->fixOrderAfterSettings($old_name, $new_name);
        }

        ksort($this->columns);
    }

    /**
     * @param $params - download csv setter
     *                bool
     */
    public function setDownloadCsv($params)
    {
        $this->download_csv = isset($params['csv']) && $params['csv'] ? true : false;
    }

    /**
     * @return bool - download csv getter
     * bool
     */
    public function toCsv()
    {
        return $this->download_csv;
    }

    /**
     * changes names from filtres
     * need get array(@param $changes )
     * [current_name => to_change_name]
     * if to_change_name = false - deleted from array
     *
     * @param $changes
     */
    public function changeFilters($changes)
    {
        foreach ($changes as $key => $value) {
            $keys = array_keys($this->filters);

            if (in_array($key, $keys)) {
                if (!is_null($value) || $value) {
                    $this->filters[$value] = $this->filters[$key];
                }
                unset($this->filters[$key]);
            }
        }
    }

    /**
     * ignoreFilters
     *
     * @param array $filters
     */
    public function ignoreFilters(array $filters)
    {
        foreach ($filters as $key => $filter) {
            if (isset($this->filters[$filter])) {
                unset($this->filters[$filter]);
            }
        }
    }

    /**
     * Set orders -- $datatable->setOrder(['name' => 'desc'], true);
     *
     * @param $orders array - ['name' => 'desc', 'id' => 'asc']|['name', 'id'] - default asc order
     * @param bool $override - remove datatable orders if exists|merge with datatable order
     */
    public function setOrder($orders, $override = false) {
        if ($override) {
            $this->orders = [];
        }

        foreach ($orders as $column => $order) {
            if (is_numeric($column)) {
                if (array_search($order, array_column($this->orders, 'column')) === false) {
                    $this->orders[] = ['column' => $order, 'dir' => 'asc'];
                }
            } else {
                if (array_search($column, array_column($this->orders, 'column')) === false) {
                    $this->orders[] = ['column' => $column, 'dir' => $order];
                }
            }
        }
    }

    /**
     * Get orders
     *
     * @return array
     */
    public function getOrder() {
        return $this->orders;
    }

    /**
     * Added column name if not exists in array columns
     * added with character underscore
     *
     * if exists do nothing
     *
     * @param $name
     */
    public function addColumn($name)
    {
        if (!in_array($name, $this->columns)) {
            $this->columns[] = $name . ' as _' . $name;
        }
    }

    /**
     * @return array|void
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @return null|string
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * Build Search String - setter
     *
     * @param null $search
     */
    public function setSearch($search = null)
    {
        if ($search) {
            $this->search = '%' . $search . '%';
        } else {
            $search = $this->filters['search'];
            unset($this->filters['search']);
            if (!empty($search)) {
                $this->search = '%' . $search . '%';
            }
        }
    }

    /**
     * Fix orders after change column name
     * example in logs: default sort (in datatable) by column 0 - this is recorded_at,
     *                  but this column not exists in table. In table this created_at.
     *
     * @param $old
     * @param $new
     */
    private function fixOrderAfterSettings($old, $new) {
        foreach ($this->orders as $key => $order) {
            if ($order['column'] == $old && is_string($new)) {
                $this->orders[$key]['column'] = $new;
            }
        }
    }

    /**
     * Build Search String - Build search string if this one exists
     * checked from filter search
     * and checked from datatable search box
     * override this filter search to search box if two exists
     *
     * @param $params
     *
     * @return null|string
     */
    private function buildSearch($params)
    {
        $search = null;
        if (isset($this->filters['search'])) {
            $filter_search = $this->filters['search'];
            unset($this->filters['search']);

            if (!empty($filter_search)) {
                $search = '%' . $filter_search . '%';
            }
        }

        if (isset($params['search']['value']) && !empty($params['search']['value'])) {
            $search = '%' . $params['search']['value'] . '%';
        }

        return $search;
    }

    /**
     * build  datatable json
     *
     * @return array
     */
    private function buildDatatableJson()
    {
        return [
            'draw' => $this->draw,
            'data' => $this->data,
            'info' => true,
            'pageLength' => $this->limit,
            'iTotalRecords' => $this->recordsTotal,
            'iTotalDisplayRecords' => $this->recordsFiltered,
            'pagination' => true
        ];
    }

    /**
     * add filters to query
     */
    private function buildFilteredQuery()
    {
        foreach ($this->filters as $where => $value) {
            if (is_array($value)) {
                $this->model->whereIn($where, array_values($value));
            } else {
                $this->model->where($where, $value);
            }
        }
    }

    /**
     * add to query search(like) params
     */
    private function buildSearchQuery()
    {
        if ($this->search) {
            $search = $this->search;
            $wheries = $this->searchColumns;

            $this->model->where(
                function ($query) use ($search, $wheries) {
                    foreach ($wheries as $where) {
                        $query->orWhere($where, 'like', $search);
                    }
                }
            );
        }
    }

    /**
     * @param $filters
     *
     * @return array
     */
    private function removeNotFilters($filters)
    {
        $results = [];
        foreach ($filters as $key => $value) {
            if (!in_array($key, $this->notFilters) && !empty($value)) {
                $results[$key] = $value;
            }
        }

        return $results;
    }

    /**
     * run query and parse results to array not keys
     */
    private function runAndParse()
    {
        $query = $this->model->offset($this->offset)->limit($this->limit);
        if (count($this->orders)) {
            foreach ($this->orders as $order) {
                $query->orderBy($order['column'], $order['dir']);
            }
        }

        $results = $query->get();

        if( is_a($results, Collection::class)) {
            $results = $results->toArray();
        }

        $this->data = $this->parseModel($results, $this->permission_name);
    }

    /**
     * return count from filtered search
     */
    private function getRecordsFiltered()
    {
        return $this->model->count();
    }

    private function setColumns($params)
    {
        if (isset($params['cols'])) {
            return $params['cols'];
        } else {
            throw new Exception('Not found columns');
        }
    }

    /**
     * @param $params
     *
     * @return array
     */
    private function setFilters($params)
    {
        $filters = [];
        if (isset($params['filters'])) {
            $filters = array_merge($filters, $this->removeNotFilters($params['filters']));
        }

        foreach ($params as $name => $param) {
            if (!in_array($name, $this->not_url_filters)) {
                if (is_array($param)) {
                    $filters = array_merge($filters, $this->removeNotFilters($param));
                } else {
                    $filters = array_merge($filters, $this->removeNotFilters(json_encode([$name => $param])));
                }
            }
        }
        return $filters;
    }

    /**
     * @param $params
     *
     * @return array
     * @throws \Exception
     */
    private function setDefaultOrders($params)
    {
        $orders = [];
        if (isset($params['order'])) {
            $order = $params['order'];
            foreach ($order as $or) {
                if (isset($this->columns[$or['column']])) {
                    $orders[] = [
                        'column' => $this->columns[$or['column']],
                        'dir' => $or['dir']
                    ];
                } else {
                    throw new \Exception('Check method renderColumns in your Controller, one or more columns nulls.');
                }
            }
        }

        return $orders;
    }

    /**
     * Validate if all parameters exists
     *
     * @param $params
     */
    private function validate($params)
    {
        if (isset($params['query'])) {
            $this->model = $params['query'];
        } else {
            throw new Exception('Model not found');
        }

        $this->draw = isset($params['draw']) ? $params['draw'] : 1;

        $this->offset = isset($params['start']) ? $params['start'] : 0;
        $length = isset($params['length']) ? $params['length'] : 10;
        $this->limit = $this->limit > 0 ? $this->limit : $length;

        $this->recordsTotal     = $params['recordsTotal']    ?? 0;
        $this->permission_name  = $params['permission_name'] ?? 0;
        $this->searchColumns    = $params['searchColumns']   ?? [];
    }

    /**
     * parse model results to array not keys
     * add column action
     *
     * @param $model
     * @param $permission_name
     *
     * @return array
     */
    private function parseModel($model, $permission_name)
    {
        $results = [];
        foreach ($model as $key1 => $m) {
            $curr_columns = $this->columns;
            $result = [];
            foreach ($m as $key2 => $value) {
                $key_found = false;

                foreach ($this->addition_params_columns as $index => $column) {
                    if ($key2 == array_keys($column)[0]) {
                        $save_index = array_search($key2, $curr_columns);
                        unset($curr_columns[$save_index]);
                        $result[$save_index] =
                            $this->buildCombineField($this->addition_params_columns[$index][array_keys($column)[0]], $m);

                        $key_found = true;
                    }
                }

                if (!$key_found && (substr($key2, 0, 1) !== '_' && in_array($key2, $this->columns))) {
                    $save_index = array_search($key2, $curr_columns);
                    unset($curr_columns[$save_index]);
                    $result[$save_index] = $value;
                }
            }

            ksort($result);

            // resolve the row id and set it inside params
            $params = [
                "id" => $this->initId($m)
            ];

            // if actions is a closure, resolve the closure and assign it to actions
            if (isClosure($this->actions)) {
                $result[count($result)] = $this->actions->__invoke($m, $params['id']);
            } elseif (!empty($this->actions) && is_array($this->actions)) {
                $result[count($result)] = ActionButtonLib::render($this->actions, $permission_name, $params);
            } elseif (!empty($this->actions)) {
                throw new Exception('Invalid data type of: ' . typeOf($this->actions) . ' for $this->actions');
            }

            $results[] = $result;
        }

        return $results;
    }

    /**
     * @param $params
     * @param $object
     *
     * @return string
     */
    private function buildCombineField($params, $object)
    {
        $id = $this->initId($object);
        $res = '';

        if (isset($params['type']) && $params['type'] == 'link') {
            $href = route($params['route'], $id);
            $title = $object[$params['relation']][$params['show_name']];
            $res .= "<a href='$href'>$title</a>";
        } else if (isset($params['callback'])) {
            $function = $params['callback'];

            return $function->__invoke($object, $id);
        }

        return $res;
    }

    /**
     * Init id
     *
     * @param $object
     *
     * @return null
     */
    private function initId($object)
    {
        if (isset($object['id'])) {
            return $object['id'];
        } else if (isset($object['_id'])) {
            return $object['_id'];
        }

        // if there is no id or _id check for keys that ends with .id
        $suffix = '.id';
        foreach ($object as $key => $val) {
            // check if $key ends with .id
            if (substr($key, -strlen($suffix)) === $suffix) {
                return $object[$key];
            }
        }

        return null;
    }

    /**
     * create csv
     * @param $transformFunctions !Optional! - array of functions:
     *      transformData - if you want to change the data before saving the file
     *      transformHeader - if you want to change the header before saving the file
     * @return array
     */
    private function buildCsv($transformFunctions=null)
    {
        $writer = new ToCsv($this->model);
        return ['file' => 1, 'path' => $writer->create($transformFunctions)];
    }
}