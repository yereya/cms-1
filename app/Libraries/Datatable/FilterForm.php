<?php
/**
 * Created by PhpStorm.
 * User: azusdex
 * Date: 13/07/2016
 * Time: 5:12 PM
 */

namespace App\Libraries\Datatable;


class FilterForm
{
    /**
     * @var null - form data not parsed
     */
    protected $destination_data = null;

    /**
     * @var array - key need to check, value - correct size
     */
    protected $valid_fields = ['where' => 3, 'having' => 3];

    /**
     * Parse form to array
     *
     * current parse only format
     * [
     *      name: kkkkk,
     *      value: jjjjj
     * ],
     * [
     *      name: eeeee,
     *      value: rrrrr
     * ]
     *
     * @param $form
     * @return array
     * @throws \Exception
     */
    public function filter($form) {
        $this->validFormFormat($form);
        $this->destination_data = $form;
        $results = $this->convertToArray($form);

        $this->validFormParams($results, 'where');
        $this->validFormParams($results, 'group_by', []);

        return $results;
    }

    /**
     * Valid Form Params
     *
     * parse form parameters to filter form class
     *
     * @param       $results
     * @param       $param_name
     * @param array $needle_params
     */
    private function validFormParams(&$results, $param_name, $needle_params = ['column', 'value', 'operator']) {
        if (!empty($results[$param_name])) {
            if (!is_array($results[$param_name])) {
                $results[$param_name] = [[$results[$param_name]]];
            }

            foreach ($results[$param_name] as $key => $where) {
                $count = 0;

                foreach ($needle_params as $needle) {
                    if (empty($where[$needle])) {
                        $count++;
                    }
                }

                if ($count > 0) {
                    unset($results[$param_name][$key]);
                }
            }
        }
    }

    /**
     * Valid From Format
     * Checked if form in format array if not converting to form
     *
     * @param $form
     * @return mixed
     * @throws \Exception
     */
    private function validFormFormat(&$form) {
        if (isJson($form)) {
            $form = json_decode($form, true);
        } elseif (is_object($form)) {
            $form = object2Array($form);
        } else {
            throw new \Exception('Unknown input format.');
        }
    }

    /**
     * Convert form to array
     *
     * @param $form
     * @return array
     */
    private function convertToArray($form) {
        /**
         * remove all arrays where one or more params is null
         * [
         *   key => key,
         *   value => null
         * ]
         */
        $this->removeJsonNull($form);

        /**
         * get array keys, example:
         * [
         *      where(name key) => 6(count reiteration this key in form)
         * ]
         */
        $keys = $this->extractKeys($form);

        /**
         * create array associative key->sub key, example:
         * [
         *     where(name key) => [
         *          id(name sub key) => 3
         *          operand(name sub key) => 3
         *          value => 3
         *      ]
         * ]
         */
        $struct = [];
        foreach ($keys as $key => $count) {
            if ($count > 1) {
                $struct[$key] = $this->extractSubKeys($form, $key);
            } else {
                $struct[$key] = $count;
            }
        }

        /**
         * Fill associative array to data
         */
        $results = [];
        foreach ($struct as $key => $value) {
            if (is_array($value) && $key != 'where' && $key != 'having') {
                $results[$key] = $this->extractMultiValues($form, $key, $value);
            } elseif ($key == 'where') {
                //add because multi select this field
                $results[$key] = $this->extractMultiSelectField($form, 'where');
            } elseif ($key == 'having') {
                $results[$key] = $this->extractMultiSelectField($form, 'having');
            } else {
                $results[$key] = $this->extractValues($form, $key);
            }
        }

        $this->removeNotValidArrays($results);
        return $results;
    }

    /**
     * Extract only main Keys
     *
     * @param $form
     * @return array
     */
    private function extractKeys(&$form) {
        $keys = [];

        foreach ($form as $index => $value) {
            $key = strpos($value['name'], '[') ?
                substr($value['name'], 0, strpos($value['name'], '[')) :
                $value['name'];
            if (array_key_exists($key, $keys)) {
                $keys[$key] += 1;
            } else {
                $keys[$key] = 1;
            }
        }

        return $keys;
    }

    /**
     * Extract only sub keys
     *
     * @param $form
     * @param $key
     * @return array
     */
    private function extractSubKeys(&$form, $key) {
        $sub = [];

        foreach ($form as $f) {
            $first = array_shift($f);
            if (strpos($first, $key) !== false) {
                $start = strpos($first, '[') + 1;
                $end = strpos($first, ']');
                $size = $end - $start;
                $param = substr($first, $start, $size);
                if (array_key_exists($param, $sub)) {
                    $sub[$param] += 1;
                } else {
                    $sub[$param] = 1;
                }
            }
        }

        return $sub;
    }

    /**
     * Extract multi values
     * Example to multi values
     * Set:
     * [
     *      +name: where[key][]
     *      +value: 10
     * ],
     * [
     *      +name: where[operand][]
     *      +value: <>
     * ],
     * [
     *      +name: where[value][]
     *      +value: 100
     * ]
     *
     * @param $form
     * @param $key
     * @param $params
     * @return array
     */
    private function extractMultiValues(&$form, $key, $params) {
        $subs = [];
        foreach ($params as $index => $param) {
            for ($i = 0; $i < $param; $i++) {
                $subs[$i][] = $this->extractValues($form, $key . '[' . $index . ']', true);
            }
        }

        return $subs;
    }

    /*
     *
     */
    private function extractMultiSelectField(&$form, $cause) {
        $wheres = [];
        foreach ($form as $item) {
            if (strpos($item['name'], $cause) !== false) {
                $indexStart = strpos($item['name'], '[') + 1;
                $indexEnd = strpos($item['name'], ']');
                $index = substr($item['name'], $indexStart, $indexEnd - $indexStart);

                if (strpos($item['name'], 'column') !== false) {
                    $wheres[$index]['column'] = $item['value'];
                } else if (strpos($item['name'], 'operator') !== false) {
                    $wheres[$index]['operator'] = $item['value'];
                } else if (strpos($item['name'], 'value') !== false) {
                    if (isset($wheres[$index]['value'])) {
                        $old = $wheres[$index]['value'] . ',';
                    } else {
                        $old = '';
                    }

                    $wheres[$index]['value'] = $old . $item['value'];
                }
            }
        }

        return $wheres;
    }

    /**
     * Helper to extract multi values
     *
     * @param $form
     * @param $key
     * @param bool $remove - if true remove last record from form data
     * @return array|null
     */
    private function extractValues(&$form, $key, $remove = false) {
        $results = [];
        $last = 0;
        foreach ($form as $index => $value) {
            $first = array_shift($value);
            if (strpos($first, $key) !== false) {
                $param = array_values($value);
                $results = $param[0];
                $last = $index;
            }
        }

        if ($remove) {
            unset($form[$last]);
        }

        return $results;
    }

    /**
     * Remove fields where one or more values is empty
     *
     * @param $form
     */
    private function removeJsonNull(&$form) {
        foreach ($form as $key => $fields) {
            if (is_array($fields)) {
                $remove = false;
                foreach ($fields as $field) {
                    if (empty($field)) {
                        $remove = true;
                    }
                }

                if ($remove) {
                    unset($form[$key]);
                }
            }
        }
    }

    /**
     * After parse, remove not valid obj
     * Get all checked names from global value valid_fields
     * Currently - all array with size 3 is valid
     *
     * @param $result
     */
    private function removeNotValidArrays(&$result) {
        foreach ($result as $key => $params) {
            if (in_array($key, array_keys($this->valid_fields))) {
                if (!is_array($params)) {
                    unset($result[$key]);
                } else {
                    foreach ($params as $key_in => $param) {
                        if (count($param) != $this->valid_fields[$key]) {
                            unset($result[$key_in]);
                        }
                    }
                }
            }
        }
    }
}