<?php

namespace App\Libraries\ImageOptimizer;


use ImageOptim\API;

/**
 * Class Optimizer
 * https://imageoptim.com/api/post
 * https://github.com/ImageOptim/php-imageoptim-api
 *
 * @package App\Libraries\ImageOptimazer
 */
class Optimizer
{
    /**
     * @var API
     */
    protected $api;

    /**
     * @var array $options
     */
    protected $options;

    /**
     * @var self $optimized
     */
    protected $optimized;

    /**
     * Optimazer constructor.
     *
     * @param array $options
     */
    public function __construct($options = [])
    {
        $this->api = new API(config('media.image_optim_username'));
        $this->setOptions($options);
    }

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Optimize by Url
     *
     * @param string $image_url
     *
     * @return mixed
     */
    public function optimizeByUrl(string $image_url)
    {
        $this->optimized = $this->api->imageFromURL($image_url);

        $this->insertOptions();

        return $this->optimized->getBytes();
    }

    /**
     * Optimize by Local path
     *
     * @param string $image_path
     *
     * @return bool|string
     */
    public function optimizeByPath(string $image_path)
    {
        $this->optimized = $this->api->imageFromPath($image_path);

        $this->options['multi_dimensions'] = 1;
        $this->insertOptions();

        return $this->optimized->getBytes();
    }

    /**
     * Resize and optimize image
     *
     * @param string $image_path
     * @param int    $percent_resize
     *
     * @return bool|string
     */
    public function resizeAndOptimize(string $image_path, $percent_resize = 100)
    {
        $dimensions  = getimagesize($image_path);
        if ($percent_resize == 0) {
            return null;
        }
        $pre_counter = $percent_resize / 100;

        $width = round($pre_counter * $dimensions[0]);
        $height = round($pre_counter * $dimensions[1]);

        $this->optimized = $this->api->imageFromPath($image_path);

        $this->options['multi_dimensions'] = 1;
        if ($width && $height) {
            $this->options['resize']['width']  = $width;
            $this->options['resize']['height'] = $height;
            $this->options['resize']['fit']    = 'fit';
        }

        $this->insertOptions();

        return $this->optimized->getBytes();
    }

    /**
     * Insert options
     */
    private function insertOptions()
    {
        if ($this->options['resize']) {
            $this->optimized->resize(
                $this->options['resize']['width'],
                $this->options['resize']['height'],
                $this->options['resize']['fit']
            );
        }

        if ($this->options['crop']) {
            $this->optimized->resize($this->options['crop']);
        }

        if ($this->options['multi_dimensions']) {
            $this->optimized->dpr($this->options['multi_dimensions']);
        }

        if ($this->options['quality']) {
            $this->optimized->quality($this->options['quality']);
        }
    }

    /**
     * Set Options
     *
     * @param $options
     */
    private function setOptions($options)
    {
        $default_options             = $this->defaultOptions();
        $options['multi_dimensions'] = $options['multi_dimensions'] ?? $default_options['multi_dimensions'];
        $options['crop']             = $options['crop'] ?? $default_options['crop'];
        $options['quality']          = $options['quality'] ?? $default_options['quality'];
        $options['resize']           = $options['resize'] ?? $default_options['resize'];

        $this->options = $options;
    }

    /**
     * Default Options
     *
     * @return array
     */
    private function defaultOptions()
    {
        return [
            'multi_dimensions' => 1,
            //Multiply image dimensions by 2 or 3 for High-DPI ("Retina") displays. This is for use with HTML5's srcset attribute that defines higher-resolution image alternatives.
            'crop'             => null,
            //Image is scaled and cropped to completely fill the given width and height 80x80,crop
            'quality'          => 'medium',
            //Specifies desired image quality when saving images in a lossy format - low|medium|high|lossless
            'resize'           => null
        ];
    }
}