<?php

namespace App\Libraries\Bo\OldOutInterfaces;

use App\Libraries\Guzzle;

class OutConnection
{
    /**
     * @var Guzzle - client curl
     */
    protected $client;

    /**
     * @var created token from out server
     */
    protected $token;

    /**
     * @var string - base url
     */
    protected $base_url = 'https://out.trafficpointltd.com';

    /**
     * @var string - interface url
     */
    protected $local_url;

    /**
     * @var string - url to authentication
     */
    protected $url_auth = '/auth/local';

    /**
     * @var string - request methods
     */
    protected $method_post = 'POST';
    protected $method_get = 'GET';
    protected $method_put = 'PUT';

    /**
     * @var string - params to auth
     */
    protected $user_email = 'kostya@trafficpoint.io';
    protected $user_password = '123456';

    /**
     * OutConnection constructor.
     */
    public function __construct() {
        if (!$this->client) {
            $this->client = new Guzzle();
        }

        $this->client->fetch(
            $this->base_url . $this->url_auth,
            [
                'form_params' => [
                    'email' => $this->user_email,
                    'password' => $this->user_password
                ]
            ],
            $this->method_post
        );

        $this->token = $this->decodeBody($this->client->getBody())->token;
        if (!$this->token) {
            throw new \Exception('This adwords token, not exists');
        }
    }

    /**
     * @return Guzzle - get client
     */
    public function getClient() {
        return $this->client;
    }

    /**
     * @return created - created token
     */
    public function getToken() {
        return $this->token;
    }

    /**
     * @return array - create request header
     */
    protected function fillHeaders() {
        return [
            'Authorization' => 'Bearer ' . $this->token,
            'Accept' => 'application/json'
        ];
    }

    /**
     * get data from mongo db out
     *
     * @param $url
     * @return mixed|null
     */
    protected function get($url) {
        $advertiser = $this->client->fetch($url, ['headers' => $this->fillHeaders()], $this->method_get);

        if ($advertiser->getErrorMsgs()) {
            return ['status' => false, 'message' => $advertiser->getErrorMsgs()];
        } else {
            return ['status' => true, 'message' => $this->decodeBody($advertiser->getBody())];
        }
    }

    /**
     * create new data to mongo db out
     *
     * @param $url
     * @param $params
     * @return $this|mixed
     */
    protected function store($url, $params) {
        $advertiser = $this->client->fetch($url,
            [
                'headers' => $this->fillHeaders(),
                'form_params' => $params
            ], $this->method_post);

        if ($advertiser->getErrorMsgs()) {
            return ['status' => false, 'message' => $advertiser->getErrorMsgs()];
        } else {
            return ['status' => true, 'message' => $this->decodeBody($advertiser->getBody())->_id];
        }
    }

    /**
     * update data to mongo db out
     *
     * @param $url
     * @param $params
     * @return $this|mixed
     */
    protected function set($url, $params) {
        $advertiser = $this->client->fetch($url,
            [
                'headers' => $this->fillHeaders(),
                'form_params' => $params
            ], $this->method_put);

        if ($advertiser->getErrorMsgs()) {
            return ['status' => false, 'message' => $advertiser->getErrorMsgs()];;
        } else {
            return ['status' => true, 'message' => $this->decodeBody($advertiser->getBody())->_id];
        }
    }

    /**
     * parse response body
     *
     * @param $body
     * @return mixed
     */
    private function decodeBody($body) {
        return json_decode($body);
    }
}