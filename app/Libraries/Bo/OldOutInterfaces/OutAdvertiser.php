<?php
/**
 * Created by PhpStorm.
 * User: azusdex
 * Date: 11/04/2016
 * Time: 2:02 PM
 */

namespace App\Libraries\Bo\OldOutInterfaces;

class OutAdvertiser extends OutConnection
{
    /**
     * @var string - url to get or update
     * GET - method GET
     * base_url/api/advertisers/{mongoId}
     *
     * UPDATE - method PUT
     * base_url/api/advertisers/{mongoId}
     * params [
     *    _id(mongo id), adserver(domain), category, name, status, timezone, type
     * ]
     *
     * CREATE - method POST
     * base_url/api/advertisers
     * params [
     *      adserver(domain), category, name, status, timezone, type
     * ]
     */
    protected $local_url = '/api/advertisers';

    /**
     * get advertiser data from mongo
     *
     * return
     * {
     *      "__v":0,
     *      "adserver":"http://fishki.net",
     *      "name":"JoraTest",
     *      "category":"Domain",
     *      "status":"Inactive",
     *      "_id":"570b94b39f97a4e93f79bbfd",
     *      "timezone":"+02:00"
     * }
     * or error
     *
     * @param $advertiser_mongo_id
     * @return mixed|null
     */
    public function getAdvertiser($advertiser_mongo_id) {
        $url = $this->base_url . $this->local_url . '/' . $advertiser_mongo_id;

        return $this->get($url);
    }

    /**
     * create advertiser in mongo db
     *
     * return
     * {
     *      "__v":0,
     *      "adserver":"http://fishki.net",
     *      "name":"JoraTest",
     *      "category":"Domain",
     *      "status":"Inactive",
     *      "_id":"570b94b39f97a4e93f79bbfd", -> new mongo id
     *      "timezone":"+02:00"
     * }
     * or error
     *
     * @param array $values
     * @return $this|mixed
     */
    public function storeAdvertiser($values = []) {
        $url = $this->base_url . $this->local_url;

        return $this->store($url, $this->fillParams(null, $values));
    }

    /**
     * update advertiser in mongo db
     *
     * return
     * {
     *      "__v":0,
     *      "adserver":"http://fishki.net",
     *      "name":"JoraTest",
     *      "category":"Domain",
     *      "status":"Inactive",
     *      "_id":"570b94b39f97a4e93f79bbfd", -> new mongo id
     *      "timezone":"+02:00"
     * }
     * or error
     *
     * @param null $advertiser_mongo_id
     * @param array $values
     * @return $this|mixed
     */
    public function setAdvertiser($advertiser_mongo_id, $values = []) {
        $url = $this->base_url . $this->local_url . '/' . $advertiser_mongo_id;

        return $this->set($url, $this->fillParams($advertiser_mongo_id, $values));
    }

    /**
     * Helper to fill params
     *
     * @param null $mongo_id
     * @param $values
     * @return
     */
    private function fillParams($mongo_id = null, $values) {
        $result = filterAndSetParams([
            'adserver' => null,
            'name' => null,
            'category' => null,
            'status' => null,
            'timezone' => null,
            'type' => null
        ], $values);

        if ($mongo_id) {
            $result['_id'] = $mongo_id;
        }

        return $result;
    }
}