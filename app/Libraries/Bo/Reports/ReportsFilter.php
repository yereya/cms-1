<?php

namespace App\Libraries\Bo\Reports;

use Illuminate\Support\Facades\DB;

class ReportsFilter {

    /**
     * @var null|string
     */
    protected $tables = 'reports_brand_scrapers';

    /**
     * ReportsFilter constructor.
     * @param null $tables
     */
    public function __construct($tables = null) {
        if ($tables) {
            $this->tables = $tables;
        }
    }

    /**
     * Return columns name from current table
     *
     * @return array
     */
    public function getColumns() {
        return DB::getSchemaBuilder()->getColumnListing($this->tables);
    }

}