<?php
/**
 * Created by PhpStorm.
 * User: azusdex
 * Date: 12/04/2016
 * Time: 4:10 PM
 */

namespace App\Libraries\Bo\OutInterfaces;


class OutMedia extends OutConnection
{
    protected $local_url = '/api/publisher/media';

    /**
     * get media from mongo db
     * 
     * return 
     * {
     *      "accounts":[],
     *      "conversions":[],
     *      "__v":0,
     *      "status":"Active",
     *      "type":"Web",
     *      "name":"SupersonicDE",
     *      "pubId":"5459e8ec792763b665849b93",
     *      "_id":"5459e927792763b665849b9d"
     * }
     * or error
     * 
     * @param $mongo_id
     * @return mixed|null
     */
    public function getMedia($mongo_id) {
        $url = $this->base_url . $this->local_url . '/' . $mongo_id;

        return $this->find($url);
    }

    /**
     * create media data from mongo
     *
     * return
     * {
     *      "accounts":[],
     *      "conversions":[],
     *      "__v":0,
     *      "status":"Active",
     *      "type":"Web",
     *      "name":"SupersonicDE",
     *      "pubId":"5459e8ec792763b665849b93",
     *      "_id":"5459e927792763b665849b9d"
     * }
     * or error
     *
     * @param $values
     * @return mixed|null
     */
    public function storeMedia($values = []) {
        $url = $this->base_url . $this->local_url;

        return $this->create($url, $this->fillParams(null, $values));
    }

    /**
     * update media data from mongo
     *
     * return
     * {
     *      "accounts":[],
     *      "conversions":[],
     *      "__v":0,
     *      "status":"Active",
     *      "type":"Web",
     *      "name":"SupersonicDE",
     *      "pubId":"5459e8ec792763b665849b93",
     *      "_id":"5459e927792763b665849b9d"
     * }
     * or error
     *
     * @param $mongo_id
     * @param array $values
     * @return mixed|null
     */
    public function setMedia($mongo_id, $values = []) {
        $url = $this->base_url . $this->local_url . '/' . $mongo_id;

        return $this->set($url, $this->fillParams($mongo_id, $values));
    }

    private function fillParams($mongo_id = null, $values)
    {
        $result = filterAndSetParams([
            'pubId' => null,
            'name' => null,
            'type' => null,
            'status' => 'Inactive',
            'conversions' => [],
            'accounts' => []
        ], $values);

        if ($mongo_id) {
            $result['_id'] = $mongo_id;
        }

        return $result;
    }
}