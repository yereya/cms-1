<?php

namespace App\Libraries\Bo\OutInterfaces;

class CampaignsGroupsLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/advertiser/{advertiser_id}/brands/{brand_id}/campaigns/{campaign_id}/groups';

    /**
     * stores and associates a group with a campaign
     *
     * @param array $values
     * @return $this|mixed
     */
    public function create($values = [])
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$values['advertiser_id'], $values['brand_id'], $values['campaign_id'], $values['group_id']],
            [static::ADVERTISER, static::BRAND, static::CAMPAIGN, static::GROUP], $url);

        return $this->fetch($this->method_post, $url, $this->fillParams($values));
    }

    /**
     * helper method to fill required parameters
     *
     * @param $values
     * @return array
     */
    private function fillParams($values = [])
    {
        $result = filterAndSetParams([
            'campaign_id' => null,
            'group_id' => null
        ], $values);

        return $result;
    }
}