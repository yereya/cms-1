<?php
/**
 * Created by PhpStorm.
 * User: azusdex
 * Date: 12/04/2016
 * Time: 4:10 PM
 */

namespace App\Libraries\Bo\OutInterfaces;


class OutPublisher extends OutConnection
{
    protected $local_url = '/api/publishers';

    /**
     * get publisher data from mongo
     *
     * return
     * {
     *      "_id":"53fc5330de77c40965df75d8",
     *      "name":"Google Adwords",
     *      "status":"Active",
     *      "__v":0,
     *      "type":"AdWords",
     *      "pixels":[]
     * }
     * or error
     *
     * @param $mongo_id
     * @return mixed|null
     */
    public function getPublisher($mongo_id) {
        $url = $this->base_url . $this->local_url . '/' . $mongo_id;

        return $this->find($url);
    }

    /**
     * create publisher in mongo db
     *
     * return
     * {
     *      "_id":"53fc5330de77c40965df75d8",
     *      "name":"Google Adwords",
     *      "status":"Active",
     *      "__v":0,
     *      "type":"AdWords",
     *      "pixels":[]
     * }
     * or error
     *
     * @param array $values
     * @return $this|mixed
     */
    public function storePublisher($values) {
        $url = $this->base_url . $this->local_url;

        return $this->create($url, $this->fillParams(null, $values));
    }

    /**
     * update advertiser in mongo db
     * else update param advertiser_mongo_id = mongo_id
     *
     * return
     * {
     *      "_id":"53fc5330de77c40965df75d8",
     *      "name":"Google Adwords",
     *      "status":"Active",
     *      "__v":0,
     *      "type":"AdWords",
     *      "pixels":[]
     * }
     * or error
     *
     * @param null $mongo_id
     * @param array $values
     * @return $this|mixed
     */
    public function setPublisher($mongo_id, $values) {
        $url = $this->base_url . $this->local_url . '/' . $mongo_id;

        return $this->set($url, $this->fillParams($mongo_id, $values));
    }

    /**
     * Helper to fill params
     *
     * @param null $mongo_id
     * @param $values
     * @return array
     */
    private function fillParams($mongo_id = null, $values)
    {
        $result = filterAndSetParams([
            'name' => null,
            'status' => 'Inactive',
            'type' => false,
            'pixels' => isset($values['pixels']) ? $this->fillPixelParams($values['pixels']) : []
        ], $values);

        if ($mongo_id) {
            $result['_id'] = $mongo_id;
        }

        return $result;
    }

    /**
     * @param $values
     * @return array
     */
    private function fillPixelParams($values) {
        $result = [];
        
        foreach ($values as $value) {
            $temp = filterAndSetParams([
                'name' => $value['name'],
                'url' => $value['url']
            ], $value);
            
            if (isset($value['mongo_id'])) {
                $temp['_id'] = $value['mongo_id'];
            }
            
            $result[] = $temp;
            unset($temp);
        }
        
        return $result;
    }
}