<?php

namespace App\Libraries\Bo\OutInterfaces;


class BrandsLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/advertiser/{advertiser_id}/brands';

    const MONGO_ID_KEY = 'id';

    /**
     * get brand(io) data from mongo
     * or error
     *
     * @param $advertiser_id
     * @param $brand_mongo_id
     *
     * @return mixed|null
     */
    public function find($advertiser_id, $brand_mongo_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $brand_mongo_id;
        $url = $this->injectUrlParams([$advertiser_id], [static::ADVERTISER], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * store method
     *
     * @param array $values
     *
     * @return $this|mixed
     */
    public function create($values = [])
    {
        parent::create($values);
        $this->createMongoIdCol(static::MONGO_ID_KEY);
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$values['advertiser_id']], [static::ADVERTISER], $url);

        return $this->fetch($this->method_post, $url, $this->fillParams(null, $this->mongo_id_col + $values));
    }

    /**
     * update method
     *
     * @param null  $brand_id
     * @param array $values
     *
     * @return $this|mixed
     */
    public function update($brand_id, $values = [])
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$values['advertiser_id']], [static::ADVERTISER], $url);

        return $this->fetch($this->method_put, $url, $this->fillParams($brand_id, $values));
    }

    /**
     * gets all of the brand's campaigns
     *
     * @param $advertiser_id
     * @param $brand_id
     *
     * @return mixed|null
     */
    public function campaigns($advertiser_id, $brand_id)
    {
        $url = $this->base_url . $this->local_url . "/{$brand_id}/campaigns";
        $url = $this->injectUrlParams([$advertiser_id], [static::ADVERTISER], $url);

        return $this->find($this->method_get, $url);
    }


    /**
     * Retrieves an array of action parameter ids along with their corresponding url query
     *
     * @param $advertiser_id
     * @param $brand_id
     *
     * @return array
     */
    public function getActionsUrlQueries($advertiser_id, $brand_id)
    {
        $actions         = self::find($advertiser_id, $brand_id)['data']->actions;
        $actions_queries = [];

        foreach ($actions as $action) {
            $url                          = $this->buildValidUrlParamsQuery($action->parameters ?? []);
            $actions_queries[$action->id] = $action->id . '/?ioid=' . $brand_id . (!empty($url) ? '&' . $url : '');
        }

        return $actions_queries;
    }

    /**
     * Helper method to fill params
     *
     * @param null $mongo_id
     * @param      $values
     *
     * @return array
     */
    private function fillParams($mongo_id = null, $values)
    {
        $result = filterAndSetParams([
            'id'              => null,
            'advertiser_id'   => null,
            'name'            => null,
            'status'          => null,
            'client'          => null,
            'brand_type'      => null,
            'company_id'      => null,
            'brands_group_id' => null,
            'cpa'             => null,

        ], $values);

        if ($mongo_id) {
            $result['id'] = $mongo_id;
        }
        if(isset($result['cpa']) && $result['cpa'] === ''){
            $result['cpa'] = NULL;
        }

        return $result;
    }
}
