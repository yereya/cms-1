<?php

namespace App\Libraries\Bo\OutInterfaces;

class CampaignGroupsLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/advertiser/{advertiser_id}/brands/{brand_id}/campaign-groups';

    /**
     * Retrieve all group models
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return mixed|null
     */
    public function all($advertiser_id, $brand_id)
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id], [static::ADVERTISER, static::BRAND], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * Gets the parameter data
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $group_id
     * @return mixed|null
     */
    public function find($advertiser_id, $brand_id, $group_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $group_id;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $group_id],
            [static::ADVERTISER, static::BRAND, static::GROUP], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * @param array $values
     * @return $this|mixed
     */
    public function create($values = [])
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$values['advertiser_id'],
            $values['brand_id']], [static::ADVERTISER, static::BRAND], $url);
        $campaign = $this->fetch($this->method_post, $url, $this->fillParams(null, $values));
        $values['id'] = $campaign['data']->id;
        $this->associateParams($values);

        return $campaign;
    }

    /**
     * update method
     *
     * @param $group_id
     * @param array $values
     * @return $this|mixed
     */
    public function update($group_id, $values = [])
    {
        $url = $this->base_url . $this->local_url;
        $this->empty($values['advertiser_id'], $values['brand_id'], $values['id']);
        $this->associateParams($values);
        $url = $this->injectUrlParams([$values['advertiser_id'], $values['brand_id'], $values['id']],
            [static::ADVERTISER, static::BRAND, static::GROUP], $url);

        return $this->fetch($this->method_put, $url, $this->fillParams($group_id, $values));
    }

    /**
     * delete a group along with its parameters
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $group_id
     */
    public function delete($advertiser_id, $brand_id, $group_id)
    {
        $url = $this->base_url . $this->local_url;
        $this->empty($advertiser_id, $brand_id, $group_id);
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $group_id],
            [static::ADVERTISER, static::BRAND, static::GROUP], $url);

        $this->fetch($this->method_delete, $url, $this->fillParams($group_id, ['id' => $group_id]));
    }

    /**
     * delete all of the current landing page's parameters
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $group_id
     */
    public function empty($advertiser_id, $brand_id, $group_id)
    {
        $params_lib = new GroupParametersLib();
        $params_lib->empty($advertiser_id, $brand_id, $group_id);
    }

    /**
     * Retrieves an array of group ids along with their corresponding url query
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return array
     */
    public function getGroupsUrlQueries($advertiser_id, $brand_id)
    {
        $groups = $this->all($advertiser_id, $brand_id)['data'];
        $groups_queries = [];

        foreach ($groups as $group) {
            $url = $this->buildValidUrlParamsQuery($group->group_parameters ?? []);
            $groups_queries[$group->id] = $url;
        }

        return $groups_queries;
    }

    /**
     * Associates new parameters with the current group
     *
     * @param $values
     */
    private function associateParams($values)
    {
        $group_params_lib = new GroupParametersLib();
        foreach ($values['group_params']['key'] ?? [] as $index => $param) {
            $param_values = [
                'key' => $param,
                'value' => $values['group_params']['value'][$index],
                'status' => $values['status'],
                'group_id' => $values['id'],
                'advertiser_id' => $values['advertiser_id'],
                'brand_id' => $values['brand_id']
            ];

            $group_params_lib->create($param_values);
        }
    }

    /**
     * helper method to fill required parameters
     *
     * @param null $mongo_id
     * @param $values
     * @return array
     */
    private function fillParams($mongo_id = null, $values = [])
    {
        $result = filterAndSetParams([
            'id' => null,
            'name' => null,
            'group_id' => null,
            'status' => 'Active'
        ], $values);

        if ($mongo_id) {
            $result['id'] = $mongo_id;
            $result['group_id'] = $mongo_id;
        }

        return $result;
    }
}