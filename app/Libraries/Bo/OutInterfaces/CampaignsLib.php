<?php

namespace App\Libraries\Bo\OutInterfaces;

class CampaignsLib extends NewOutConnection
{
    /**
     * @var string
     * local url pattern to be met
     */
    protected $local_url = '/advertiser/{advertiser_id}/brands/{brand_id}/campaigns';

    const MONGO_ID_KEY = 'mongodb_id';

    /**
     * get campaign data from mongo
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_mongodb_id
     * @return mixed|null
     */
    public function find($advertiser_id, $brand_id, $campaign_mongodb_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $campaign_mongodb_id;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id], [static::ADVERTISER, static::BRAND], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * create campaign in mongo db
     *
     * @param array $values
     * @return $this|mixed
     */
    public function create($values = [])
    {
        parent::create($values);
        $this->createMongoIdCol(static::MONGO_ID_KEY);
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams(
            [$values['advertiser_id'], $values['brand_id']],
            [static::ADVERTISER, static::BRAND], $url
        );

        /* Separate values into arrays of campaign and landing parameters */
        $values = $this->segregateParams($values);
        $new_campaign = $this->fetch($this->method_post, $url, $this->fillParams(null, $this->mongo_id_col + $values['campaign']));

        if (!$new_campaign['status']) {
            return $new_campaign;
        }

        // TODO find a way to inject these parameters in a cleaner way
        $values['landing']['advertiser_id'] = $values['campaign']['advertiser_id'];
        $values['landing']['brand_id']      = $values['campaign']['brand_id'];
        $values['landing']['campaign_id']   = $new_campaign['data']->id;
        $values['landing']['campaign_mongodb_id']   = $new_campaign['data']->mongodb_id;


        $landing = (new LandingPagesLib())->create($values['landing']);

        if (!$landing['status']) {
            return $landing;
        }

        return $new_campaign;
    }

    /**
     * create or update campaign in mongo db
     * if create new param campaign = null
     * else update param campaign_mongo_id = mongo_id
     *
     * @param null $campaign_mongodb_id
     * @param array $values
     * @return $this|mixed
     */
    public function update($campaign_mongodb_id, $values = [])
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$values['advertiser_id'], $values['brand_id']], [static::ADVERTISER, static::BRAND], $url);

        return $this->fetch($this->method_put, $url, $this->fillParams($campaign_mongodb_id, $values));
    }

    /**
     * Helper to fill params
     *
     * @param null $mongo_id
     * @param $values
     * @return array
     * @internal param array $lp_values
     */
    private function fillParams($mongo_id = null, $values)
    {
        $result = filterAndSetParams([
            self::MONGO_ID_KEY => $mongo_id,
            'brand_id' => null,
            'name' => null,
            'status' => "Inactive",
            'timezone' => '+00:00'
        ], $values);

        if ($mongo_id) {
            $result['id'] = $mongo_id;
        }

        return $result;
    }

    /**
     * recreates the array as two sub-arrays
     * the 'campaign' sub-array will contain the campaign parameters
     * the 'landing' sub-array will contain the landing page parameters
     *
     * @param array $values
     * @return array
     */
    private function segregateParams($values = [])
    {
        $campaign_params = [];
        $lp_params = [];

        foreach ($values as $key => $value) {
            if (preg_match('/^(lp_)+/', $key)) {
                $proper_key = str_replace('lp_', '', $key);
                $lp_params[$proper_key] = $value;
            } else {
                $campaign_params[$key] = $value;
            }
        }

        return [
            'campaign' => $campaign_params,
            'landing' => $lp_params
        ];
    }
}