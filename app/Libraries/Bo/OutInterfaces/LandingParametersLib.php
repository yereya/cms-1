<?php

namespace App\Libraries\Bo\OutInterfaces;

class LandingParametersLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/advertiser/{advertiser_id}/brands/{brand_id}/campaigns/{campaign_id}/lp/{landing_id}/params';

    /**
     * Gets the parameter data
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     * @param $parameter_id
     * @return mixed|null
     */
    public function find($advertiser_id, $brand_id, $campaign_id, $landing_id, $parameter_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $parameter_id;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $campaign_id, $landing_id],
            [static::ADVERTISER, static::BRAND, static::CAMPAIGN, static::LANDING], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * store method
     *
     * @param array $values
     * @return $this|mixed
     */
    public function create($values = [])
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams(
            [$values['advertiser_id'], $values['brand_id'], $values['campaign_id'], $values['landing_page_id']],
            [static::ADVERTISER, static::BRAND, static::CAMPAIGN, static::LANDING], $url
        );

        return $this->fetch($this->method_post, $url, $this->fillParams(null, $values));
    }

    /**
     * update method
     *
     * @param $mongo_id
     * @param array $values
     * @return $this|mixed
     */
    public function update($mongo_id, $values = [])
    {
        $url = $this->base_url . $this->local_url . '/' . $mongo_id;
        $url = $this->injectUrlParams(
            [$values['advertiser_id'], $values['brand_id'], $values['campaign_id'], $values['landing_page_id']],
            [static::ADVERTISER, static::BRAND, static::CAMPAIGN, static::LANDING], $url
        );

        return $this->fetch($this->method_put, $url, $this->fillParams($mongo_id, $values));
    }

    /**
     * delete a parameter resource
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     * @param $param_id
     */
    public function delete($advertiser_id, $brand_id, $campaign_id, $landing_id, $param_id)
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $campaign_id, $landing_id],
            [static::ADVERTISER, static::BRAND, static::CAMPAIGN, static::LANDING], $url);

        $this->fetch($this->method_delete, $url, $this->fillParams($param_id, ['id' => $param_id , 'landing_page_id' => $landing_id]));
    }

    /**
     * empty all of the associated params
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     */
    public function empty($advertiser_id, $brand_id, $campaign_id, $landing_id)
    {
        $url = $this->base_url . $this->local_url . '/empty';
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $campaign_id, $landing_id],
            [static::ADVERTISER, static::BRAND, static::CAMPAIGN, static::LANDING], $url);

        $this->fetch($this->method_delete, $url, $this->fillParams(null, ['landingId' => $landing_id]));
    }

    /**
     * helper method to fill required parameters
     *
     * @param null $mongo_id
     * @param $values
     * @return array
     */
    private function fillParams($mongo_id = null, $values = [])
    {
        $result = filterAndSetParams([
            'landing_page_id' => null,
            'key' => null,
            'value' => null,
            'status' => 'Active'
        ], $values);

        if ($mongo_id) {
            $result['id'] = $mongo_id;
        }

        return $result;
    }
}