<?php

namespace App\Libraries\Bo\OutInterfaces;


class ActionsLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/advertiser/{advertiser_id}/brands/{brand_id}/actions';

    /**
     * Retrieve all group models
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return mixed|null
     */
    public function all($advertiser_id, $brand_id)
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id], [static::ADVERTISER, static::BRAND], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * Gets the parameter data
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $action_id
     * @return mixed|null
     */
    public function find($advertiser_id, $brand_id, $action_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $action_id;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $action_id],
            [static::ADVERTISER, static::BRAND, static::ACTION], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * @param array $values
     * @return $this|mixed
     */
    public function create($values = [])
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$values['advertiser_id'], $values['brand_id']], [static::ADVERTISER, static::BRAND], $url);
        $action = $this->fetch($this->method_post, $url, $this->fillParams(null, $values));
        $values['id'] = $action['data']->id ?? '';
        if (!empty($values['id'])) {
            $this->associateParams($values);
        }

        return $action;
    }

    /**
     * update method
     *
     * @param $action_id
     * @param array $values
     * @return $this|mixed
     */
    public function update($action_id, $values = [])
    {
        $url = $this->base_url . $this->local_url;
        $this->empty($values['advertiser_id'], $values['brand_id'], $values['id'] = $action_id);
        $this->associateParams($values);
        $url = $this->injectUrlParams([$values['advertiser_id'], $values['brand_id'], $action_id],
            [static::ADVERTISER, static::BRAND, static::ACTION], $url);

        return $this->fetch($this->method_put, $url, $this->fillParams($action_id, $values));
    }

    /**
     * delete an action along with its parameters
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $action_id
     */
    public function delete($advertiser_id, $brand_id, $action_id)
    {
        $url = $this->base_url . $this->local_url;
        $this->empty($advertiser_id, $brand_id, $action_id);
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $action_id],
            [static::ADVERTISER, static::BRAND, static::ACTION], $url);

        $this->fetch($this->method_delete, $url, $this->fillParams($action_id, ['id' => $action_id]));
    }

    /**
     * delete all of the current action's parameters
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $action_id
     */
    public function empty($advertiser_id, $brand_id, $action_id)
    {
        $params_lib = new ActionParamsLib();
        $params_lib->empty($advertiser_id, $brand_id, $action_id);
    }

    /**
     * Associates new parameters with the current action
     *
     * @param $values
     */
    private function associateParams($values)
    {
        $action_params_lib = new ActionParamsLib();
        foreach ($values['action_params']['key'] ?? [] as $index => $param) {
            $param_values = [
                'key' => $param,
                'value' => $values['action_params']['value'][$index],
                'sum' => $values['sum'] ?? '',
                'action_id' => $values['id'],
                'advertiser_id' => $values['advertiser_id'],
                'brand_id' => $values['brand_id'],
                'group_by' => $values['group_by'] ?? '',
                'count' => $values['count'] ?? ''
            ];

            $action_params_lib->create($param_values);
        }
    }

    /**
     * helper method to fill required parameters
     *
     * @param null $id
     * @param $values
     * @return array
     */
    private function fillParams($id = null, $values = [])
    {
        $result = filterAndSetParams([
            'id' => null,
            'name' => null,
            'advertiser_id' => null,
            'brand_id' => null,
            'status' => 'active',
            'type' => 'lead',
            'is_unique' => false
        ], $values);

        if ($id) {
            $result['id'] = $id;
            $result['action_id'] = $id;
        }

        return $result;
    }
}