<?php

namespace App\Libraries\Bo\OutInterfaces;

class DomainsLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/domain';

    /**
     * get domain model
     *
     * @param $domain_id
     * @return mixed|null
     */
    public function find($domain_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $domain_id;

        return $this->fetch('GET', $url);
    }

    /**
     * store method
     *
     * @param $values
     * @return mixed|null
     */
    public function create($values)
    {
        $url = $this->base_url . $this->local_url;

        return $this->fetch($this->method_post, $url, $this->fillParams(null, $values));
    }

    /**
     * update method
     *
     * @param $domain_id
     * @param array $values
     * @return mixed|null
     */
    public function update($domain_id, $values = [])
    {
        $url = $this->base_url . $this->local_url . "/{$domain_id}";

        return $this->fetch($this->method_put, $url, $this->fillParams($domain_id, $values));
    }

    /**
     * Gets all domains data
     */
    public function all()
    {
        $url = $this->base_url . $this->local_url;

        return $this->fetch($this->method_get, $url);
    }


    /**
     * delete a domain
     *
     * @param $domain_id
     */
    public function delete($domain_id)
    {
        $url = $this->base_url . $this->local_url;

        $this->fetch($this->method_delete, $url, $this->fillParams($domain_id, []));
    }


    /**
     * Helper to fill params
     *
     * @param null $domain_id
     * @param $values
     * @return array
     */
    private function fillParams($domain_id = null, $values)
    {
        $result = filterAndSetParams([
            'status'    => null,
            'domain'    => null
        ], $values);

        if ($domain_id) {
            $result['id'] = $domain_id;
        }

        return $result;
    }
}