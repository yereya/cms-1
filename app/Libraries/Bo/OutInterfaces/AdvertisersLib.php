<?php

namespace App\Libraries\Bo\OutInterfaces;

use App\Entities\Models\Bo\Account;

class AdvertisersLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/advertiser';
    const MONGO_ID_KEY = 'advertiser_mongo_id';

    /**
     * get advertiser model
     *
     * @param $advertiser_id
     * @return mixed|null
     */
    public function find($advertiser_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $advertiser_id;

        return $this->fetch('GET', $url);
    }

    /**
     * store method
     *
     * @param $values
     * @return mixed|null
     */
    public function create($values)
    {
        parent::create($values);
        $this->createMongoIdCol(static::MONGO_ID_KEY);
        $url = $this->base_url . $this->local_url;

        return $this->fetch($this->method_post, $url, $this->fillParams(null, $this->mongo_id_col + $values));
    }

    /**
     * update method
     *
     * @param $advertiser_id
     * @param array $values
     * @return mixed|null
     */
    public function update($advertiser_id, $values = [])
    {
        $url = $this->base_url . $this->local_url;

        return $this->fetch($this->method_put, $url, $this->fillParams($advertiser_id, $values));
    }

    /**
     * Gets all advertisers data
     */
    public function all()
    {
        # Contruct URL
        $url = $this->base_url . $this->local_url;

        # Get advertisers from out
        $data = $this->fetch($this->method_get, $url);
        $data_array = json_decode(json_encode($data['data']), true);

        # Get accounts related to the user
        $account_access = session('user_account_access');

        # Get advertisers related to the accounts
        $advertisers = Account::select('advertiser_id')->whereIn('id',$account_access )
            ->get()
            ->pluck('advertiser_id')
            ->toArray();

        # Leave only the relavent advertisers
        $output = array_filter($data_array,function($object) use($advertisers){
            return in_array($object['id'],$advertisers);
        });

        # Transfer data back to array{stdObject,stdObject..}
        $data['data'] = [];
        foreach ($output as $row){
            $data['data'][] = (object)$row;
        }

        return $data;
    }

    /**
     * @param $id
     * Get a list of the current advertiser's brands
     *
     * @return mixed|null
     */
    public function brands($id)
    {
        $url = $this->base_url . $this->local_url . "/{$id}/brands";

        return $this->fetch($this->method_get, $url);
    }


    /**
     * Retrieves an array of action parameter ids along with their corresponding url query
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return array
     */
    public function getActionsUrlQueries($advertiser_id, $brand_id)
    {
        $actions = self::find($advertiser_id)['data']->actions;
        $actions_queries = [];

        foreach ($actions as $action) {
            $url = $this->buildValidUrlParamsQuery($action->parameters ?? []);
            $actions_queries[$action->id] = $action->id . '/?ioid=' . $brand_id . (!empty($url) ? '&' . $url : '');
        }

        return $actions_queries;
    }

    /**
     * Helper to fill params
     *
     * @param null $mongo_id
     * @param $values
     * @return array
     */
    private function fillParams($mongo_id = null, $values)
    {
        $result = filterAndSetParams([
            self::MONGO_ID_KEY => $mongo_id,
            'name'      => null,
            'status'    => null,
            'url'       => null,
            'timezone'  => '+00:00'
        ], $values);

        if ($mongo_id) {
            $result['id'] = $mongo_id;
        }

        return $result;
    }
}