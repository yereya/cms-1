<?php

namespace App\Libraries\Bo\OutInterfaces;

use App\Libraries\Guzzle;
use App\Libraries\JWT\TrackingSystemToken;
use Webpatser\Uuid\Uuid;

abstract class NewOutConnection
{
    /**
     * @var static parameter indicator
     */
    const ADVERTISER    = '{advertiser_id}';
    const BRAND         = '{brand_id}';
    const CAMPAIGN      = '{campaign_id}';
    const LANDING       = '{landing_id}';
    const GROUP         = '{group_id}';
    const ACTION        = '{action_id}';
    const DOMAIN        = '{domain_id}';
    const COMPANY       = '{company_id}';
    const BRAND_GROUP    = '{brand_group_id}';

    /**
     * @var $uuid - will be created for specific API models
     */
    protected $uuid = null;

    /**
     * @var array $mongo_id_col - indicates the new out created mongo_id
     */
    protected $mongo_id_col = [];

    /**
     * @var Guzzle - client curl
     */
    protected $client;

    /**
     * @var string - base url
     *
     */
    protected $base_url;

    /**
     * @var string - request methods
     */
    protected $method_post      = 'POST';
    protected $method_get       = 'GET';
    protected $method_put       = 'PUT';
    protected $method_delete    = 'DELETE';

    public function __construct()
    {
        $this->base_url     = config('services.tracking_system.tracking_url');
        $this->client       = new Guzzle();
    }

    /**
     * @return Guzzle
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return array - create request header
     */
    protected function fillHeaders()
    {
        return [
            'Accept' => 'application/json',
            'Token' => (new TrackingSystemToken())->get()
        ];
    }

    /**
     * Generic fetch method to make a custom RESTful http request
     *
     * @param string $method
     * @param string $relative_url
     * @param array $params
     * @return array|null
     */
    protected function fetch($method = 'GET', $relative_url = '', $params = [])
    {
        $out_model = $this->client->fetch(
            $relative_url,
            [
                'headers' => $this->fillHeaders(),
                'form_params' => $params ?? []
            ],
            $method);

        return $this->getFormattedResponse($out_model) ?? null;
    }

    /**
     * Provides a simple interface for creating a Uuid for new out database models
     *
     * @param array $args
     */
    protected function create($args)
    {
        $this->setUuid($this->generateUuid());
    }

    /**
     * replaces passed params with the corresponding url values
     *
     * @param array $params
     * @param array $url_values
     * @param $url
     * @return mixed
     */
    protected function injectUrlParams(array $params, array $url_values, $url)
    {
        $injected_url = $url;
        $url_params = array_combine($url_values, $params);

        foreach ($url_params as $key => $param) {
            $injected_url = str_replace($key, $param, $injected_url);
        }

        return $injected_url;
    }

    /**
     * builds a valid url query from the given parameters
     *
     * @param array $parameters
     * @return string
     */
    protected function buildValidUrlParamsQuery(array $parameters)
    {
        $url_query = '';
        $parameters_arr = collect($parameters)->pluck('value', 'key')->toArray();

        if (!empty($parameters_arr))
            $url_query = http_build_query($parameters_arr);

        return $url_query;
    }

    /**
     * parse response body
     *
     * @param $body
     * @return mixed
     */
    private function decodeBody($body)
    {
        return json_decode($body);
    }

    /**
     * Process and return the normalized format
     *
     * @param $out_model
     * @return array
     */
    private function getFormattedResponse($out_model)
    {
        $status = 0;
        $message = '';
        $data = null;

        if (isJson($_model_body = $out_model->getBody())) {
            $model = $this->decodeBody($_model_body);
            if ($model->status != 'error') {
                $status = 1;
                $message = $model->desc;
                $data = $model->data;
            } else {
                $errors = $model->description ?? [];
                foreach ($errors as $error) {
                    $message .= $error->error . '.';
                }
            }
        } else {
            if ($this->client->getStatusCode() == 404) {
                \Redirect::route('login')->send();
            }

            $message = 'could not complete the request';
            $data = $out_model->getErrorMsgs();
        }

        return ['status' => $status, 'message' => $message, 'data' => $data];
    }

    /**
     * Generates a Uuid according to timestamp
     * documentation: https://github.com/webpatser/laravel-uuid/tree/2.1.1
     */
    private function generateUuid()
    {
        return Uuid::generate(1);
    }

    /**
     * Sets the $uuid variable string
     *
     * @param Uuid $uuid_obj
     */
    private function setUuid(Uuid $uuid_obj)
    {
        $this->uuid = $uuid_obj->string ?? null;
    }

    /**
     * Creates the mongo id column proportional to the current creation
     *
     * @param $mongo_key
     */
    protected function createMongoIdCol($mongo_key)
    {
        if (!$mongo_key) {return;}
        $this->mongo_id_col = [$mongo_key => $this->uuid];
    }
}