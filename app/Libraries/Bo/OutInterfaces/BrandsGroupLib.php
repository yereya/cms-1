<?php


namespace App\Libraries\Bo\OutInterfaces;


class BrandsGroupLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/company/{company_id}/brandsGroups';

    /**
     * get brand group model
     *
     * @param $company_id
     * @param $brand_group_id
     *
     * @return mixed|null
     */
    public function find($company_id, $brand_group_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $brand_group_id;
        $url = $this->injectUrlParams([$company_id, $brand_group_id], [static::COMPANY, static::BRAND_GROUP], $url);

        return $this->fetch('GET', $url);
    }

    /**
     * store method
     *
     * @param $values
     *
     * @return mixed|null
     */
    public function create($values)
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$values['company_id']], [static::COMPANY], $url);

        return $this->fetch($this->method_post, $url, $this->fillParams(null, $values));
    }

    /**
     * update method
     *
     * @param       $brand_group_id
     * @param array $values
     *
     * @return mixed|null
     */
    public function update($brand_group_id, $values = [])
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$values['company_id']], [static::COMPANY], $url);

        return $this->fetch($this->method_put, $url, $this->fillParams($brand_group_id, $values));
    }

    /**
     * Gets all companies data
     *
     * @param $company_id
     *
     * @return array|null
     */
    public function all($company_id)
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$company_id], [static::COMPANY], $url);


        return $this->fetch($this->method_get, $url);
    }


    /**
     * delete a company
     *
     * @param $brand_group_id
     */
    public function delete($brand_group_id)
    {
        $url = $this->base_url . $this->local_url;

        $this->fetch($this->method_delete, $url, $this->fillParams($brand_group_id, []));
    }


    /**
     * Helper to fill params
     *
     * @param null $brand_group_id
     * @param      $values
     *
     * @return array
     */
    private function fillParams($brand_group_id = null, $values)
    {
        $result = filterAndSetParams([
            'name'        => null,
            'description' => null,
            'company_id'  => null
        ], $values);

        if ($brand_group_id) {
            $result['id'] = $brand_group_id;
        }

        return $result;
    }


    /**
     * Get the list of companies (for select box field)
     *
     * @return array
     */
    public function getList()
    {
        $results = $this->all();

        return collect($results['data'])
            ->pluck('name', 'id')
            ->toArray();
    }

    /**
     * @param $company_id
     *
     * @return array
     */
    public function getByCompanyId($company_id)
    {
        $results = $this->all($company_id);

        return collect($results['data'])->map(function ($record) {
            return [
                'id'   => $record->id,
                'text' => $record->name
            ];
        })->toArray();
    }
}