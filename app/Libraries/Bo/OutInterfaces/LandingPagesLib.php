<?php

namespace App\Libraries\Bo\OutInterfaces;


class LandingPagesLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/advertiser/{advertiser_id}/brands/{brand_id}/campaigns/{campaign_id}/lp';

    const MONGO_ID_KEY = 'mongodb_id';

    /**
     * Retrieve a landing page model
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $mongo_id
     * @return mixed|null
     */
    public function find($advertiser_id, $brand_id, $campaign_id, $mongo_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $mongo_id;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $campaign_id], [static::ADVERTISER, static::BRAND, static::CAMPAIGN], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * store method
     *
     * @param array $values
     * @return $this|mixed
     */
    public function create($values = [])
    {
        parent::create($values);
        $this->createMongoIdCol(static::MONGO_ID_KEY);
        $url = $this->base_url . $this->local_url;

        /* Store the landing page before storing its parameters */
        $url = $this->injectUrlParams(
            [$values['advertiser_id'], $values['brand_id'], $values['campaign_id']],
            [static::ADVERTISER, static::BRAND, static::CAMPAIGN], $url
        );

        $landing = $this->fetch($this->method_post, $url, $this->fillParams($this->uuid ?? null, $values));
        if (!$landing['status']) {
            return $landing;
        }

        $values['landing_id'] = $landing['data']->id;

        if (!empty($values['landing_id'])) {
            /* Associate newly allocated parameters */
            $this->associateParams($values);
        }

        return $landing;
    }

    /**
     * update method
     *
     * @param $mongo_id
     * @param array $values
     * @return $this|mixed
     */
    public function update($mongo_id, $values = [])
    {
        $values['landing_id'] = $mongo_id;
        $url = $this->base_url . $this->local_url;

        /* Deletes associated parameters */
        $this->empty($values['advertiser_id'], $values['brand_id'], $values['campaign_id'], $mongo_id);

        /* Associate newly allocated parameters */
        $this->associateParams($values);

        $url = $this->injectUrlParams(
            [$values['advertiser_id'], $values['brand_id'], $values['campaign_id']], [static::ADVERTISER, static::BRAND, static::CAMPAIGN], $url
        );

        return $this->fetch($this->method_put, $url, $this->fillParams($mongo_id, $values));
    }

    /**
     * Get the current landing page's parameters
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     * @return mixed
     */
    public function parameters($advertiser_id, $brand_id, $campaign_id, $landing_id)
    {
        $landing = $this->getLandingPage($advertiser_id, $brand_id, $campaign_id, $landing_id)['data'];

        return $landing->parameters ?? [];
    }

    /**
     * Deletes a specified landing page
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     * @return array
     */
    public function delete($advertiser_id, $brand_id, $campaign_id, $landing_id)
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$advertiser_id, $brand_id, $campaign_id], [static::ADVERTISER, static::BRAND, static::CAMPAIGN], $url);

        /* Deletes the landing page's parameters */
        $this->empty($advertiser_id, $brand_id, $campaign_id, $landing_id);

        return $this->fetch($this->method_delete, $url, ['campaign_id' => $campaign_id, 'id' => $landing_id]);
    }

    /**
     * delete all of the current landing page's parameters
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     */
    public function empty($advertiser_id, $brand_id, $campaign_id, $landing_id)
    {
        $params_lib = new LandingParametersLib();
        $params_lib->empty($advertiser_id, $brand_id, $campaign_id, $landing_id);
    }

    /**
     * Associates new parameters with the current landing page
     *
     * @param $values
     * @internal param $advertiser_id
     * @internal param $brand_id
     * @internal param $campaign_id
     * @internal param $landing_id
     */
    private function associateParams($values)
    {
        $params_lib = new LandingParametersLib();
        foreach ($values['dynamic']['key'] ?? [] as $index => $dynamic) {
            $param_values = [
                'key'               => $dynamic,
                'value'             => $values['dynamic']['value'][$index],
                'status'            => $values['status'],
                'landing_page_id'   => $values['landing_id'],
                'advertiser_id'     => $values['advertiser_id'],
                'brand_id'          => $values['brand_id'],
                'campaign_id'       => $values['campaign_id'],
                'campaign_mongodb_id' => $values['campaign_mongodb_id']
            ];

            $params_lib->create($param_values);
        }
    }

    /**
     * helper method to fill required parameters
     *
     * @param null $mongo_id
     * @param $values
     * @return array
     */
    private function fillParams($mongo_id = null, $values)
    {
        $result = filterAndSetParams([
            self::MONGO_ID_KEY => $mongo_id,
            'is_default' => false,
            'name' => null,
            'page' => null,
            'status' => 'Active',
            'token' => null,
            'campaign_id' => null,
            'campaign_mongodb_id' => $values['campaign_mongodb_id'] ?? null,
            'url' => null,
            'use_token' => false,
            'weight' => 100,
            'dynamic' => []
        ], $values);

        if ($mongo_id) {
            $result['id'] = $mongo_id;
        }

        return $result;
    }
}