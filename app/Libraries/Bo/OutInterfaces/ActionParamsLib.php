<?php

namespace App\Libraries\Bo\OutInterfaces;

class ActionParamsLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/advertiser/{advertiser_id}/actions/{action_id}/params';

    /**
     * Gets the parameter data
     *
     * @param $advertiser_id
     * @param $action_id
     * @param $parameter_id
     * @return mixed|null
     */
    public function find($advertiser_id, $action_id, $parameter_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $parameter_id;
        $url = $this->injectUrlParams([$advertiser_id, $action_id],
            [static::ADVERTISER, static::ACTION], $url);

        return $this->fetch($this->method_get, $url);
    }

    /**
     * store action parameters
     *
     * @param array $values
     * @return $this|mixed
     */
    public function create($values = [])
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams(
            [$values['advertiser_id'], $values['action_id']],
            [static::ADVERTISER, static::ACTION], $url
        );

        return $this->fetch($this->method_post, $url, $this->fillParams(null, $values));
    }

    /**
     * update method
     *
     * @param $param_id
     * @param array $values
     * @return $this|mixed
     */
    public function update($param_id, $values = [])
    {
        $url = $this->base_url . $this->local_url . '/' . $param_id;
        $url = $this->injectUrlParams(
            [$values['advertiser_id'], $values['action_id']],
            [static::ADVERTISER, static::ACTION], $url
        );

        return $this->fetch($this->method_put, $url, $this->fillParams($param_id, $values));
    }

    /**
     * delete an action parameter
     *
     * @param $advertiser_id
     * @param $action_id
     * @param $param_id
     */
    public function delete($advertiser_id, $action_id, $param_id)
    {
        $url = $this->base_url . $this->local_url;
        $url = $this->injectUrlParams([$advertiser_id, $action_id],
            [static::ADVERTISER, static::ACTION], $url);

        $this->fetch($this->method_delete, $url, $this->fillParams($param_id, ['id' => $param_id , 'action_id' => $action_id]));
    }

    /**
     * empty all of the current action's parameters
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $action_id
     */
    public function empty($advertiser_id, $brand_id, $action_id)
    {
        $url = $this->base_url . $this->local_url . '/empty';
        $url = $this->injectUrlParams([$advertiser_id, $action_id],
            [static::ADVERTISER, static::ACTION], $url);

        $this->fetch($this->method_delete, $url, $this->fillParams(null, ['action_id' => $action_id]));
    }

    /**
     * helper method to fill required parameters
     *
     * @param null $param_id
     * @param $values
     * @return array
     */
    private function fillParams($param_id = null, $values = [])
    {
        $result = filterAndSetParams([
            'id'            => null,
            'action_id'     => null,
            'key'           => null,
            'value'         => null,
            'sum'           => '',
            'count'         => '',
            'is_unique'     => false,
            'group_by'      => ''
        ], $values);

        if ($param_id) {
            $result['id'] = $param_id;
        }

        return $result;
    }
}