<?php namespace App\Libraries\Bo\OutInterfaces;


class ScraperLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/scraper';

    /**
     * store method
     *
     * @param $values
     * @return mixed|null
     */
    public function create($values)
    {
        $url = $this->base_url . $this->local_url;

        return $this->fetch($this->method_post, $url, $this->fillParams($values));
    }

    public function retrieveFilteredTokens($tokens_arr, $last_run_time)
    {
        $url = $this->base_url . $this->local_url . '/filterExistingTokens';

        $json_tokens_object = [
            'lastRunTime' => $last_run_time,
            'tokens'      => json_encode($tokens_arr)
        ];

        return $this->fetch($this->method_post, $url, $json_tokens_object);
    }

    /**
     * Helper to fill params
     *
     * @param $values
     * @return array
     */
    private function fillParams(array $values)
    {
        $result = filterAndSetParams([
            'data' => null
        ], $values);

        return $result;
    }
}