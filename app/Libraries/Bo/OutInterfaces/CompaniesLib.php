<?php


namespace App\Libraries\Bo\OutInterfaces;


/**
 * Class CompaniesLib
 *
 * @package app\Libraries\Bo\OutInterfaces
 */
class CompaniesLib extends NewOutConnection
{
    /**
     * @var string - url to get or update
     * local url pattern to be met
     */
    protected $local_url = '/company';

    /**
     * get company model
     *
     * @param $company_id
     * @return mixed|null
     */
    public function find($company_id)
    {
        $url = $this->base_url . $this->local_url . '/' . $company_id;

        return $this->fetch('GET', $url);
    }

    /**
     * store method
     *
     * @param $values
     * @return mixed|null
     */
    public function create($values)
    {
        $url = $this->base_url . $this->local_url;

        return $this->fetch($this->method_post, $url, $this->fillParams(null, $values));
    }

    /**
     * update method
     *
     * @param $company_id
     * @param array $values
     * @return mixed|null
     */
    public function update($company_id, $values = [])
    {
        $url = $this->base_url . $this->local_url;

        return $this->fetch($this->method_put, $url, $this->fillParams($company_id, $values));
    }

    /**
     * Gets all companies data
     */
    public function all()
    {
        $url = $this->base_url . $this->local_url;

        return $this->fetch($this->method_get, $url);
    }


    /**
     * delete a company
     *
     * @param $company_id
     */
    public function delete($company_id)
    {
        $url = $this->base_url . $this->local_url;

        $this->fetch($this->method_delete, $url, $this->fillParams($company_id, []));
    }


    /**
     * Helper to fill params
     *
     * @param null $company_id
     * @param $values
     * @return array
     */
    private function fillParams($company_id = null, $values)
    {
        $result = filterAndSetParams([
            'name'    => null,
            'product_description' => null,
            'SAP_id'  => null
        ], $values);

        if ($company_id) {
            $result['id'] = $company_id;
        }

        return $result;
    }


    /**
     * Get the list of companies (for select box field)
     *
     * @return array
     */
    public function getList(){
        $results = $this->all();

        return collect($results['data'])
            ->pluck('name','id')
            ->toArray();
    }
}