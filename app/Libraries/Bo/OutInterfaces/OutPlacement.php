<?php
/**
 * Created by PhpStorm.
 * User: azusdex
 * Date: 12/04/2016
 * Time: 4:10 PM
 */

namespace App\Libraries\Bo\OutInterfaces;


class OutPlacement extends OutConnection
{
    protected $local_url = '/api/publisher/placements';

    /**
     * get placement from mongo db
     * 
     * return 
     * {
     *      "_id":"540c5479be49c15c211f2a61",
     *      "pubId":"53fc5330de77c40965df75d8",
     *      "mediaId":"53fca55d97ebe932681c980e",
     *      "name":"DE_Goolge_Display_SLM",
     *      "type":"DirectLink",
     *      "__v":0,
     *      "data":
     *          {
     *              "lpId":"54088818d7b3875c03501d59"
     *          },
     *      "offering":[]
     * }
     * or error
     * 
     * @param $mongo_id
     * @return mixed|null
     */
    public function getPlacement($mongo_id) {
        $url = $this->base_url . $this->local_url . '/' . $mongo_id;

        return $this->find($url);
    }

    /**
     * create placement in mongo db
     *
     * return
     * {
     *      "_id":"540c5479be49c15c211f2a61",
     *      "pubId":"53fc5330de77c40965df75d8",
     *      "mediaId":"53fca55d97ebe932681c980e",
     *      "name":"DE_Goolge_Display_SLM",
     *      "type":"DirectLink",
     *      "__v":0,
     *      "data":
     *          {
     *              "lpId":"54088818d7b3875c03501d59"
     *          },
     *      "offering":[]
     * }
     * or error
     *
     * @param array $values
     * @return $this|mixed
     */
    public function storePlacement($values) {
        $url = $this->base_url . $this->local_url;

        return $this->create($url, $this->fillParams(null, $values));
    }

    /**
     * update advertiser in mongo db
     * else update param advertiser_mongo_id = mongo_id
     *
     * return
     * {
     *      "_id":"540c5479be49c15c211f2a61",
     *      "pubId":"53fc5330de77c40965df75d8",
     *      "mediaId":"53fca55d97ebe932681c980e",
     *      "name":"DE_Goolge_Display_SLM",
     *      "type":"DirectLink",
     *      "__v":0
     * }
     * or error
     *
     * @param null $mongo_id
     * @param array $values
     * @return $this|mixed
     */
    public function setPlacement($mongo_id, $values) {
        $url = $this->base_url . $this->local_url . '/' . $mongo_id;

        return $this->set($url, $this->fillParams($mongo_id, $values));
    }

    private function fillParams($mongo_id = null, $values)
    {
        $result = filterAndSetParams([
            'pubId' => null,
            'mediaId' => null,
            'name' => null,
            'type' => null,
            'data' => null,
            'offering' => [],
        ], $values);

        if ($mongo_id) {
            $result['_id'] = $mongo_id;
        }

        return $result;
    }
}