<?php namespace App\Libraries\Mail;

use Mail as BaseMail;
use PhpImap\Mailbox as ImapMailbox;
use PhpImap\IncomingMail;
use PhpImap\IncomingMailAttachment;


/**
 * Class EmailLib
 *
 * @package App\Console\Commands\ReportsScraper\Providers
 */
class MailLib extends BaseMail
{

    /**
     * @var int System ID
     */
    private static $system_id = 37;

    /**
     * @var \Logger logger
     */
    private static $logger;


    /**
     * Template Blank
     * 
     * @param $email
     * @param string $subject
     * @param null $name
     * @param string $view_name
     * @param array $view_params
     * 
     * @return bool
     */
    public static function tplBlank($email, $view_params, $subject = null, $name = null, $view_name = 'emails.tp-blank')
    {
        $name = (isset($name)) ? $name : $email;

        // Set default values
        $view_params = filterAndSetParams([
            'content' => '',
            'title' => $subject,
        ], $view_params);
        $subject = isset($subject) ? $subject : $view_params['title'];
        $email = self::validateEmail($email);
        
        // Build the content into tables when it is an array
        if (is_array($view_params['content'])) {
            $view_params['content'] = array2table($view_params['content'], true, true, true);
        } elseif (is_object($view_params['content'])) {
            $view_params['content'] = array2table(object2Array($view_params['content'], true, true));
        }

        self::send($view_name, $view_params, function($message) use ($email, $name, $subject) {
            $message->to($email, $name)->subject($subject);
        });
        
        return (!self::failures()) ? true: false;
    }


    /**
     * Template Reset Password
     * 
     * @param $email
     * @param null $name
     *
     * @return bool
     */
    public static function tplResetPassword($email, $name = null)
    {
        $name = (isset($name)) ? $name : $email;
        $email = self::validateEmail($email);

        self::send('emails.password-reset', [], function($message) use ($email, $name) {
            $message->to($email, $name)->subject('Traffic Point - Password reset');
        });

        return (!self::failures()) ? true: false;
    }


    /**
     * Fetch
     *
     * @param $params
     *
     * @return array|null
     */
    public static function fetch($params)
    {
        $params = filterAndSetParams([
            'label'   => 'INBOX',
            'criteria' => 'UNSEEN',
            'limit'    => 3,
        ], $params);

        $mail_fetcher = new MailBox();
        $mail_fetcher->connect($params['label'])
            ->search($params['criteria']);

        return $mail_fetcher->getMail();
    }


    /**
     * Init
     */
    public static function init()
    {
//        self::$logger = new TpLogger();
//        self::$logger->setSystem(self::$system_id);
    }


    /**
     * Validate Email
     * Breaks the email string into array when several emails provided, comma delimited
     *
     * @param $email
     *
     * @return array
     */
    private static function validateEmail($email)
    {
        // Turn to array of emails when comma separated emails provided
        return strpos($email, ',') ? explode(',', $email) : $email;
    }
}
Mail::init();