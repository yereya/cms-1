<?php namespace App\Libraries\Mail;

use ArrayIterator;
use IteratorAggregate;
use PhpImap\Mailbox as ImapMailbox;
use Traversable;

class MailBox implements IteratorAggregate
{
    /**
     * @var ImapMailBox
     */
    protected $mailbox;

    protected $found_email_ids = null;

    protected $emails = [];

    public function connect($label)
    {
        // Connect to the mailbox
        $imap_path = '{' . env('MAIL_HOST_IMAP') . '}' . $label;
        $this->mailbox = new ImapMailbox($imap_path, env('MAIL_USERNAME'), env('MAIL_PASSWORD'), $this->getStoragePath());

        return $this;
    }

    public function search($criteria)
    {
        $this->found_email_ids = $this->mailbox->searchMailbox($criteria);

        if ($this->found_email_ids) {
            $this->emails = [];
            foreach ($this->found_email_ids as $email_id) {

                // Get the first message and save its attachment(s) to disk
                $this->emails[$email_id] = $this->mailbox->getMail($email_id, false);
            }

        }
    }

    /**
     * Retrieve an external iterator
     * @link  http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {
        return new ArrayIterator($this->emails);
    }


    /**
     * @return array
     */
    public function getMail()
    {
        return $this->emails;
    }


    /**
     * Get Storage Path
     *
     * @return string
     */
    private function getStoragePath()
    {
        $storage_path = storage_path('app/email-attachments');
        if (!file_exists($storage_path)) {
            mkdir($storage_path, 0777, true);
        }

        return $storage_path;
    }

}