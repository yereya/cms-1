This application uploads all the events from table bo.analitycs_conversions to google analytics.
We had to add new field to bo.advertisers to know the google analytics id for every advertiser. 
When posting event failed, the high water mark won't update and the status(status=is uploaded to GA) would update only for the succeeded posts.
We also created an index 'updated_at'

CREATE INDEX updated_at
ON bo.analitycs_conversions (updated_at)

ALTER TABLE advertisers
ADD ga_id VARCHAR(50);

UPDATE bo.advertisers SET ga_id = "UA-60524656-39" WHERE id = 69
UPDATE bo.advertisers SET ga_id = "UA-60524656-22" WHERE id = 140
UPDATE bo.advertisers SET ga_id = "UA-60524656-33" WHERE id = 100033
UPDATE bo.advertisers SET ga_id = "UA-60524656-31" WHERE id = 171
UPDATE bo.advertisers SET ga_id = "UA-60524656-28" WHERE id = 167

UPDATE bo.advertisers SET ga_id = "UA-60524656-16" WHERE id = 131
UPDATE bo.advertisers SET ga_id = "UA-60524656-34" WHERE id = 100042
UPDATE bo.advertisers SET ga_id = "UA-60524656-32" WHERE id = 100027
UPDATE bo.advertisers SET ga_id = "UA-60524656-38" WHERE id = 100051
UPDATE bo.advertisers SET ga_id = "UA-60524656-10" WHERE id = 122

UPDATE bo.advertisers SET ga_id = "UA-60524656-25" WHERE id = 146
UPDATE bo.advertisers SET ga_id = "UA-60524656-6" WHERE id = 82
UPDATE bo.advertisers SET ga_id = "UA-60524656-11" WHERE id = 76
UPDATE bo.advertisers SET ga_id = "UA-60524656-7" WHERE id = 119
UPDATE bo.advertisers SET ga_id = "UA-60524656-12" WHERE id = 125

UPDATE bo.advertisers SET ga_id = "UA-60524656-3" WHERE id = 113
UPDATE bo.advertisers SET ga_id = "UA-60524656-9" WHERE id = 124
UPDATE bo.advertisers SET ga_id = "UA-60524656-26" WHERE id = 152
UPDATE bo.advertisers SET ga_id = "UA-60524656-13" WHERE id = 130
UPDATE bo.advertisers SET ga_id = "UA-60524656-2" WHERE id = 101

UPDATE bo.advertisers SET ga_id = "UA-60524656-24" WHERE id = 143
UPDATE bo.advertisers SET ga_id = "UA-60524656-14" WHERE id = 132
UPDATE bo.advertisers SET ga_id = "UA-60524656-27" WHERE id = 155
UPDATE bo.advertisers SET ga_id = "UA-60524656-23" WHERE id = 142
UPDATE bo.advertisers SET ga_id = "UA-60524656-15" WHERE id = 135

UPDATE bo.advertisers SET ga_id = "UA-60524656-35" WHERE id = 100048
UPDATE bo.advertisers SET ga_id = "UA-60524656-21" WHERE id = 137
UPDATE bo.advertisers SET ga_id = "UA-60524656-21" WHERE id = 138
UPDATE bo.advertisers SET ga_id = "UA-60524656-21" WHERE id = 139
UPDATE bo.advertisers SET ga_id = "UA-60524656-21" WHERE id = 149

UPDATE bo.advertisers SET ga_id = "UA-60524656-8"  WHERE id = 121
UPDATE bo.advertisers SET ga_id = "UA-60524656-29" WHERE id = 161
UPDATE bo.advertisers SET ga_id = "UA-60524656-30" WHERE id = 168
UPDATE bo.advertisers SET ga_id = "UA-60524656-30" WHERE id = 100054
UPDATE bo.advertisers SET ga_id = "UA-60524656-40" WHERE id = 100057

UPDATE bo.advertisers SET ga_id = "UA-60524656-19"  WHERE id = 125
UPDATE bo.advertisers SET ga_id = "UA-60524656-21" WHERE id = 137
UPDATE bo.advertisers SET ga_id = "UA-60524656-21" WHERE id = 138
UPDATE bo.advertisers SET ga_id = "UA-60524656-21" WHERE id = 139
UPDATE bo.advertisers SET ga_id = "UA-60524656-21" WHERE id = 149




