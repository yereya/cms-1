require('dotenv').config();
const axios = require('axios');
const dateFormat = require('dateformat');
const mysql = require("mysql");

const requestUrl = 'http://www.google-analytics.com/collect?v=1&t=event&ni=1';
const requestUrlOnlySales = 'http://www.google-analytics.com/collect?v=1&t=event&ni=1&ec=TrackID';

const dbOptions = {
    host: process.env.DWH_HOST, // || 'localhost',
    user: process.env.DWH_USERNAME, // || 'root' ,
    password: process.env.DWH_PASSWORD, //   || '',
    database: process.env.DWH_DATABASE, //   || 'bo',
    port: process.env.DWH_PORT //  || '3306'
};

let connection;

async function getEventsAndNewHighWaterMarkFromDB(highWaterMark){
    try {
        return new Promise((resolve, reject) => {
            connection.beginTransaction((err) => {
                if (err) {
                    reject(err);
                    return console.log("Error", err);
                }
                connection.query(
                    `SELECT   id,
                              ga_id,
                              gid_var,
                              event,
                              position,
                              brand_name,
                              track_id,
                              page,
                              commission_amount,
                              vwr_country
                     FROM     bo.analitycs_conversions
                     WHERE    STATUS = FALSE
                     AND      event IN ('lead','sale','call', 'canceled-sale','canceled-lead',  'click', 'adjustment')
                     AND      updated_at > ?
                     ORDER BY updated_at DESC`,
                    [highWaterMark,highWaterMark],
                    (err, events) => {
                        if (err) {
                            connection.rollback(() => {
                                reject(err);
                                return console.log("Error", err);
                            });
                        }
                        connection.query(
                            ` SELECT  MAX(updated_at) as newHighWaterMark
                              FROM    bo.analitycs_conversions`,
                            (err, maxDate) => {
                                if (err) {
                                    connection.rollback(() => {
                                        reject(err);
                                        return console.log("Error", err);
                                    });
                                }
                                connection.commit((err) => {
                                    if (err) {
                                        connection.rollback(() => {
                                            reject(err);
                                            return console.log("Error", err);
                                        });
                                    }
                                    resolve({newHighWaterMark:maxDate[0].newHighWaterMark, events: events});
                                });
                            });
                    });
            });
        });
    } catch (e) {
        connection.end();
        throw e;
    }
}

function getRequestUrl(tid, cid, event,el,geoid,revenue, page){
    return `${requestUrl}&ec=conversion-${event}&tid=${tid}&cid=${cid}&ea=${page}&el=${el}&geoid=${geoid}&ev=${revenue}`;
}

function getAdjustmentRequestUrl(tid, cid, track_id){

  return `${requestUrl}&tid=${tid}&cid=${cid}&ec=Adjustments&ea=${track_id}`;
}

function getOnlySalesRequestUrl(tid, cid, event,el,geoid,track_id,revenue){
    return `${requestUrlOnlySales}&tid=${tid}&cid=${cid}&ea=${track_id}&el=${el}&geoid=${geoid}&ev=${revenue}`;
}

async function updateConversionsPushedToGA(succeededIds){
    try {
        return new Promise((resolve, reject) => {
            connection.query(
                `UPDATE  bo.analitycs_conversions
                 SET     STATUS = TRUE
                 WHERE   id IN (?)`,
                [succeededIds],
                (error, response, fields) => {
                    if (error) {
                        reject(error);
                        return console.log("Error", error);
                    }
                    resolve(response);
                }
            );
        });
    } catch (e) {
        throw e;
    }
}

async function getHighWaterMark(){
    try {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT   high_water_mark
                 FROM     dwh.high_water_mark_tbl
                 WHERE    table_name = 'upload_conversion_dwh_fact_tracks'
                 AND      field_name = 'updated_at'`,
                (error, waterMark) => {
                    if (error) {
                        reject(error);
                        return console.log("Error", error);
                    }
                    resolve(waterMark);
                }
            );
        });
    } catch (e) {
        throw e;
    }
}

async function updateHighWaterMark(newHighWaterMark){
    try {
        return new Promise((resolve, reject) => {
            connection.query(
                `UPDATE     dwh.high_water_mark_tbl 
                 SET        high_water_mark = ? 
                 WHERE      table_name = 'upload_conversion_dwh_fact_tracks' 
                 AND        field_name = 'updated_at'`,
                [newHighWaterMark],
                (error, waterMark) => {
                    if (error) {
                        reject(error);
                        return console.log("Error", error);
                    }
                    resolve(waterMark);
                }
            );
        });
    } catch (e) {
        throw e;
    }
}

async function start(){
    try {
        connection = mysql.createConnection(dbOptions);
        connection.connect();

        const response          = await getHighWaterMark();
        let highWaterMark     = response[0].high_water_mark;
        highWaterMark.setDate(highWaterMark.getDate() - 1);
        let formattedDate = dateFormat(highWaterMark, 'yyyy-mm-dd HH:MM:ss');
        console.log('formatted high water mark date:  ', formattedDate);

        const result            = await getEventsAndNewHighWaterMarkFromDB(formattedDate);
        console.log('results from db:  ', result);

        const events            = result.events;
        const newHighWaterMark  = result.newHighWaterMark;

        if(!events || events.length === 0){
            console.log("No events to push");
            connection.end();
            return;
        }

        const promisesResult = await Promise.all(
            events.map( async event => {
                let cid = event.gid_var.substring(6, event.gid_var.length);
                let el = event.position + '\\' + encodeURIComponent(event.brand_name);
                let geoid = event.vwr_country;
                let revenue = Math.floor(event.commission_amount);
                let page = event.page;
                let requestUrl = '';
                try {
                    let res = '';
                    if(event.event === 'sale' ){ // There is additional API call for sale event
                        requestUrl = getOnlySalesRequestUrl(event.ga_id, cid, event.event, el, geoid, event.track_id, revenue);
                        res = await axios.post(requestUrl);
                        if ( !res.status || res.status !== 200 ) {
                            return {succeeded: false, eventID: event.id};
                        }
                    }
                    if(event.event === 'adjustment' ){
                      requestUrl = getAdjustmentRequestUrl(event.ga_id, cid, event.track_id);
                    }else{
                      requestUrl = getRequestUrl(event.ga_id, cid, event.event, el, geoid, revenue, page);
                    }
                    res = await axios.post(requestUrl);

                    if ( !res.status || res.status !== 200 ) {
                        return {succeeded: false, eventID: event.id};
                    }
                    return {succeeded: true, eventID: event.id};
                } catch (e) {
                    return {succeeded: false, eventID: event.id};
                }
            })
        );

        let succeededEventsIds  = [];
        let failedEventsIds     = [];

        promisesResult.forEach( res => {
            if(res.succeeded){
                succeededEventsIds.push(res.eventID);
            }else{
                failedEventsIds.push(res.eventID);
            }
        });

        if(succeededEventsIds.length > 0){
            if( failedEventsIds.length === 0 ){
                await Promise.all([updateConversionsPushedToGA(succeededEventsIds), updateHighWaterMark(newHighWaterMark)]);
                console.log('Successfully uploaded event ids:  ', succeededEventsIds.join());
            }else{
                await updateConversionsPushedToGA(succeededEventsIds);
                throw Error(`Failed to push events ids - ${failedEventsIds.join()}`);
            }
        }else{
            throw Error(`Failed to push all events ids - ${failedEventsIds.join()}`);
        }
        connection.end();
    } catch (e) {
        connection.end();
        throw e;
    }
}

start().then(() => {
        console.log("Task succeeded! ");
    },
    err => {
       console.log("Task failed! ", err);
    });

