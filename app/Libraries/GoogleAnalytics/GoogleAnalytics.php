<?php namespace App\Libraries\GoogleAnalytics;

use App\Libraries\TpLogger;
use Illuminate\Support\Facades\DB;

class GoogleAnalytics
{
    private $bo_connection;

    /**
     * LogMigrations constructor.
     */
    public function __construct()
    {
        $this->bo_connection = DB::connection('bo');
    }

    /**
     * Run partners alerts script
     */
    public function run()
    {
        $output = $this->executeUploadConversion();
        return $output;
    }

    private function executeUploadConversion()
    {
        $logger = TpLogger::getInstance();
        $logger->info('Upload Analytics Conversions - Google Analytics');
        exec('cd ' . __DIR__ . "/Conversions" . " && node app.js", $out);
        $logger->info($out);

        return $out;
    }

    /**
     * Close connection
     */
    public function __destruct()
    {
        $this->bo_connection->disconnect();
    }
}