<?php


namespace App\Libraries\PageSpeed;


use App\Entities\Repositories\PageSpeedRepo;
use App\Libraries\Mail;
use Carbon\Carbon;

/**
 * Class PageSpeed
 * @package App\Libraries\PageSpeed
 */
class PageSpeed
{
    protected const ERROR_MESSAGE = 'Some error occurs ,please contact with your admin.';
    protected const SUCCESS_MESSAGE = 'Congratulation you been succeeded.';
    protected const EMAIL_DID_NOT_SENT = "email didn't send, please check with your admin.";
    protected const EMAIL_SENT = 'Email sent successfully.';
    protected const ALL_SCORES_ARE_OK = 'all sites scores are OK';

    /**
     * PageSpeed constructor.
     * @param $options
     */
    public function __construct($options)
    {
        $this->options = $options;
        $this->repo = new PageSpeedRepo();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function get(): \Illuminate\Support\Collection
    {
        $scores = collect();
        $sites = collect($this->getSites());
        $page_speed_key = $this->getPageSpeedKey();

        $sites->each(function ($site) use ($scores, $page_speed_key) {
            $desktop_score = $this->repo->getScoreForSite($site, true, $page_speed_key);
            $mobile_score = $this->repo->getScoreForSite($site, false, $page_speed_key);
            $attr = [
                'site_url' => $site,
                'desktop_score' => $desktop_score,
                'mobile_score' => $mobile_score,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ];

            $scores->push($attr);
        });

        return $scores;
    }

    /**
     * @return bool
     */
    public function saveFromRequest(): bool
    {
        $scores = $this->get();

        return $this->repo->store($scores);
    }

    /**
     *
     *  Checks if there is a score difference of over 10 points between yesterday and today
     *  Sends alert mail with the relevant sites
     *
     * @return bool
     */
    public function notifyUserByMailAccordingToScoreDifference(): bool
    {
        $repo = new PageSpeedRepo();
        $scores = $repo->unionScores($repo->getDailyScore(), $repo->getYesterdayScore());
        $sites_to_report = $repo->getSitesToReport($scores);

        if (\count($sites_to_report) > 0) {
            $sites_to_report = implode('<br/>', $sites_to_report);
            $response = $this->fetch($sites_to_report);
            if (!$response) {
                echo self::EMAIL_DID_NOT_SENT;

                return false;
            }
            echo self::EMAIL_SENT;

            return true;
        }
        echo self::ALL_SCORES_ARE_OK;

        return true;
    }

    /**
     * @param $content
     * @param null $atts
     * @return bool
     */
    public function fetch($content, $atts = null): bool
    {
        $emails = !empty($this->options['emails']) ? $this->options['emails'] : false;
        if ($emails) {

            return self::email($content, $emails);
        }

        return self::email($content, config('mail.page_speed_mail_alerts.username'));
    }


    /**
     * Send email thought mail-box
     *
     * @param $content
     * @param $emails
     * @param null $another_param
     * @return bool
     */
    public static function email($content, $emails, $another_param = NULL): bool
    {
        return Mail::tplBlank($emails, [
            'content' => $content,
            'title' => 'PageSpeed Scores Alert'
        ]);
    }

    /**
     * @return mixed
     */
    public function getPageSpeedKey()
    {
        return config('services.page_speed.key');
    }

    /**
     * @return mixed
     */
    public function getSites()
    {
        return config('mail.page_speed_mail_alerts.sites');
    }
}