<?php namespace App\Libraries\FacebookConversions;

use App\Libraries\Scraper\Providers\FacebookReport;
use App\Libraries\TpLogger;
use FacebookAds\Api as Api;
use Illuminate\Support\Facades\DB;

class FacebookConversions extends FacebookReport
{
    private $bo_connection;
    /**
     * @var string $connection
     */
    protected $connection = 'bo';
    protected $conversions = [];
//    protected $pixel_endpoint = '/261571405490896/events'; // ConversionAPI
    protected $pixel_endpoint = '/2242943625725287/events'; // Top 5 Mattresses
    protected $offline_conversions_endpoint = '/4033315853347643/events';

    /**
     * LogMigrations constructor.
     */
    public function __construct()
    {
        $this->logger = new TpLogger('112');
        parent::__construct($this->logger,[],[],[]);
        $this->bo_connection = DB::connection($this->connection);


        # --- Set Credentials ---
        $this->app_id               = '425659281950369';
        $this->app_secret           = '788dbedd5724e9a68e0736b1e918fe0f';

    }

    /**
     * Run partners alerts script
     */
    public function run()
    {
        $this->executeUploadConversion();
    }

    private function executeUploadConversion()
    {
        $logger = TpLogger::getInstance();

        try{
            $logger->info('--- Before getting conversions --- ');
            $this->conversions = $this->getConversions();
            if(!$this->conversions){
                $logger->info('--- Empty --- ');
                return;
            }
            $logger->info('--- After getting conversions and before refreshing Access Token --- ');
            # --- Update Access Token ---
            $this->getAndUpdateAccessToken();
            $logger->info('--- After getting and Updating Access Token --- ');

            $this->appendPbpByTrackIdToConversions();
            $logger->info('--- After appendPbpByTrackIdToConversions --- ');
            foreach ($this->conversions as $conversion) {
                // Send Data using Conversion API
                $conversion = $this->improveConversion($conversion);
                $result = $this->sendPixel($conversion);
                if(!$result){
                    $logger->info('--- Pixel Error for : ' . $conversion['id'] . ' id, Skipping.');
                    continue;
                }
                $logger->info('--- Sent Pixel Successfully for : ' . $conversion['id']);

                // Send Data using Offline Conversions API
//                $result = $this->uploaldOfflineConversions($conversion);
//                if(!$result){
//                    $logger->info('--- Offline Conversion Error for : ' . $conversion['id'] . ' id, Skipping.');
//                    continue;
//                }
//                $logger->info('--- Sent Offline Conversion Successfully for : ' . $conversion['id']);
                $this->updateSuccessulUpload($conversion);
                $logger->info('--- Updated Successfully status in DB for : ' . $conversion['id']);
            }
        }catch (\Exception $e){

            $logger->error(['Uncaught Exception -> ' . $e->getMessage()]);
            throw new \Exception($e);
        }

    }

    /**
     * Close connection
     */
    public function __destruct()
    {
        $this->bo_connection->disconnect();
    }
}
