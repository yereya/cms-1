<?php namespace App\Libraries;

use Exception;
use Form;
use Orchestra\Html\Form\Fieldset;
use Orchestra\Html\Form\Grid as FormGrid;
use Orchestra\Html\Table\Grid as TableGrid;
use Route;
use Table;

/**
 * Class Layout
 *
 * @package App\Libraries
 */
class Layout
{

    /**
     * @var string $page_title
     */
    protected $page_title = null;

    /**
     * @var string $page_title_prefix
     */
    protected $page_title_prefix = 'Traffic Point | ';

    /**
     * @var array $body_classes
     */
    protected $body_classes = [
        'page-header-fixed',
        'page-sidebar-closed-hide-logo',
        'page-content-white'
    ];

    /**
     * Set Page Title
     *
     * @param $title
     */
    public function setPageTitle($title)
    {
        $this->page_title = $title;
    }

    /**
     * Get Page Title
     *
     * @param boolean $with_prefix
     *
     * @throws Exception
     * @return string
     */
    public function getPageTitle($with_prefix = true)
    {
        // We cannot return a title without one
        if (!$this->page_title) {
            throw new Exception('Missing page title value');
        }

        // We may want to have a prefix
        if ($with_prefix) {
            return $this->page_title_prefix . $this->page_title;
        }

        return $this->page_title;
    }

    /**
     * Set Body Class
     *
     * @param string $class
     */
    public function setBodyClass($class)
    {
        // All the classes assigned should be sanitized and merged
        $this->body_classes = array_merge($this->sanitizeClass((array) $class), $this->body_classes);
    }

    /**
     * Get Body Classes
     *
     * @param boolean $with_attribute
     *
     * @return string
     */
    public function getBodyClasses($with_attribute = true)
    {
        $route_name = $route_action = null;

        // grab the route name if exists, if not, use the route action
        if (Route::currentRouteName()) {
            $route_name = str_replace('.', '-', Route::currentRouteName());
        } elseif (Route::currentRouteAction()) {
            $route_action = explode("@", Route::currentRouteAction())[1];
        }

        // make it look nice
        $this->body_classes[] = snake_case(($route_name ?: $route_action), '-');

        $classes = implode(" ", $this->body_classes);

        // The user may choose to not use a class attribute
        if ($with_attribute) {
            return 'class="' . $classes . '"';
        }

        return $classes;
    }

    /**
     * Sanitize Class
     *
     * @param array $classes
     *
     * @return array
     */
    public function sanitizeClass(array $classes)
    {
        foreach ($classes as $i => $class) {
            //Strip out any % encoded octets
            $sanitized = preg_replace('|%[a-fA-F0-9][a-fA-F0-9]|', '', $class);

            //Limit to A-Z,a-z,0-9,_,-
            $sanitized = preg_replace('/[^A-Za-z0-9_-]/', '', $sanitized);

            $classes[$i] = $sanitized;
        }

        return $classes;
    }

}