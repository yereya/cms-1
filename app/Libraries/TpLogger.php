<?php namespace App\Libraries;

use App;
use Auth;
use App\Entities\Models\Log;
use Session;
use voku\helper\AntiXSS;

/**
 * Class TpLogger
 * @package App\Console\Commands\ReportsScraper\Providers
 */
class TpLogger
{

    private $initiator_user_id = 0;
    private $parent_record_id = null;
    private $system_id = null;
    private $task_id = null;
    private $xss = null;
    private $group_record = [];

    /**
     * Error levels
     * @var array
     */
    private $errors = [
        0 => 'info',
        1 => 'notice',
        2 => 'warning',
        3 => 'error',
        4 => 'debug',
    ];

    /**
     * @var int
     */
    private $base_memory = 0;

    /**
     * @var string
     */
    private $session_name = 'tp_logger';

    /**
     * @var $instance - The reference to *Singleton* instance of this class
     */
    private static $instance;


    /**
     * Get Instance
     * Returns the *Singleton* instance of this class.
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new self();
        }

        return static::$instance;
    }


    /**
     * Is Active
     * Are we logging this instance
     * @return bool
     */
    public static function isActive()
    {
        if (null === static::$instance) {
            return false;
        }

        return true;
    }


    public function __construct($system_id = null, $task_id = null, $concatenate_log = false)
    {
        if ($concatenate_log) {
            $this->handleLoggerConcatenation($system_id, $task_id);
        }

        $this->system_id         = $system_id;
        $this->task_id           = $task_id;
        $this->initiator_user_id = App::runningInConsole() || !Auth::user() ? 0 : Auth::user()->id;
        $this->xss               = new AntiXSS();

        $this->base_memory = memory_get_usage(true);

        static::$instance = $this;
    }


    /**
     * Record
     *
     * @param $messages
     * @param string $error
     * @param array $extra_params
     *
     * @return $this
     */
    public function record($messages, $error = 'info', array $extra_params = [])
    {
        $this->cleanMessages($messages);
        foreach ($messages as $message) {
            if (!$message) {
                continue;
            }

            // Convert exceptions
            if (is_a($message, \Exception::class)) {
                $message = nl2br((string)$message);
            } // Convert to array when an object is passed
            elseif (is_object($message)) {
                $message = object2Array($message);
            }

            $this->save($message, $error, $extra_params);
        }

        return $this;
    }

    /**
     * Error record
     */
    public function error()
    {
        $args = func_get_args();
        $this->record($args, 'error');

        return $this;
    }


    /**
     * Info record
     */
    public function info()
    {
        $args = func_get_args();
        $this->record($args, 'info');

        return $this;
    }


    /**
     * Warning record
     */
    public function warning()
    {
        $args = func_get_args();
        $this->record($args, 'warning');

        return $this;
    }


    /**
     * Notice record
     */
    public function notice()
    {
        $args = func_get_args();
        $this->record($args, 'notice');

        return $this;
    }


    /**
     * Debug record
     */
    public function debug()
    {
        $args = func_get_args();
        $this->record($args, 'debug');

        return $this;
    }


    /**
     * Reset Recording
     */
    public function reset()
    {
        $this->setParentRecordId(null);

        return $this;
    }


    public function setSystem($system_id)
    {
        if ($this->parent_record_id) {
            Log::where('id', $this->parent_record_id)->update(['system_id' => $system_id]);
        }
        $this->system_id = $system_id;

        return $this;
    }


    public function setTask($task_id)
    {
        if ($this->parent_record_id) {
            Log::where('id', $this->parent_record_id)->update(['task_id' => $task_id]);
        }
        $this->task_id = $task_id;

        return $this;
    }


    /**
     * Get System Id
     * @return null
     */
    public function getSystemId()
    {
        return $this->system_id;
    }


    public function memoryDifference()
    {
        return memory_get_usage(true) - $this->base_memory;
    }


    /**
     * Get Parent Record ID
     * @return null|Integer
     */
    public function getParentRecordId()
    {
        return $this->parent_record_id;
    }


    /**
     * @param $parent_record_id
     */
    private function setParentRecordId($parent_record_id)
    {
        $this->parent_record_id = $parent_record_id;
    }


    /**
     * @param $message
     * @param $error_type
     * @param $extra_params
     */
    private function save($message, $error_type, $extra_params)
    {
        $params = $this->buildRecordParams($message, $error_type, $extra_params);

        // Escalating the error type for better visibility
        if ($this->parent_record_id) {

            // Compares the error the error index, which is used to as the importance level of the error
            // if the error index is larger the the current in the error group record lets escalate
            $_errors = array_flip($this->errors);
            if ($error_type != 'debug' && @$_errors[$error_type] > @$_errors[$this->group_record['error_type']]) {
                Log::where('id', $this->parent_record_id)->update(['error_type' => $error_type]);
            }
        }

        // Set parent record id
        if (is_null($this->parent_record_id)) {
            $this->setParentRecordId(\DB::table('logs')->insertGetId($params));
            Session::put($this->session_name, [
                'system_id' => $this->system_id,
                'task_id' => $this->task_id,
                'parent_id' => $this->parent_record_id,
            ]);
            $this->group_record = $params;
        } else {
            Log::insert($params);
        }
    }


    /**
     * @param $message
     * @param string $error
     * @param array $extra_params
     *
     * @return mixed
     */
    private function buildRecordParams($message, $error = 'info', $extra_params = [])
    {
        $extra_params = filterAndSetParams([
            'class_name'    => null, // the name of the returned variable
            'function_name' => null  // scraper/fetcher/translated
        ], $extra_params);

        // Validate and set default value to error type
        if (!in_array($error, ['info', 'warning', 'error', 'notice', 'debug'])) {
            $error = 'info';
        }
        $message = json_encode($message);

        $result = [
            'message'           => $message,
            'error_type'        => $error,
            'initiator_user_id' => $this->initiator_user_id,
            'system_id'         => $this->system_id,
            'task_id'           => $this->task_id,
            'parent_id'         => $this->parent_record_id,
            'created_at'        => date('Y-m-d H:i:s'),
            'updated_at'        => date('Y-m-d H:i:s')
        ];

        return array_merge($result, $extra_params);
    }


    /**
     * Clean Messages - recursively searcher for xss and trims extra spaces
     *
     * @param $messages
     */
    private function cleanMessages(&$messages)
    {
        array_walk_recursive($messages, function (&$item) {
            if (is_string($item)) {
                $item = strip_tags(trim($this->xss->xss_clean($item)));
            }
        });
    }


    /**
     * Handles logging concatenation
     *
     * @param $system_id
     * @param $task_id
     */
    private function handleLoggerConcatenation($system_id, $task_id)
    {
        $session = Session::get($this->session_name);

        if (isset($session['parent_id']) && $session['parent_id']
            && isset($session['system_id']) && $session['system_id']
            && $system_id == $session['system_id']
            && (is_null($task_id) || $task_id == $session['task_id'])) {

            $this->setParentRecordId($session['parent_id']);
            $this->setSystem($system_id);

            if (is_null($task_id)) {
                $this->setTask($task_id);
            }
        }
    }
}