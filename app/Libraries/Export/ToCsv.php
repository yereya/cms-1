<?php namespace App\Libraries\Export;
use App\Entities\Models\Log;
use DB;
use SplFileObject;
use Storage;

class ToCsv
{
    private $model = null; //model query builder
    private $count = 0; //count rows
    private $limit = 0; //limit to each query
    private $offset = 0; //offset to each query
    private $full_path = null;
    private $storage_path = null;

    /**
     * ToCsv constructor.
     *
     * @param $model
     * @param int $limit
     */
    public function __construct($model = null, $limit = 500) {
        if (is_object($model)) {
            $this->model = $model;
            $this->count = $this->model->count();
            $this->limit = $this->setBranches($this->count, $limit);
            $this->offset = 0;
        }

        $this->storage_path = public_path() . DIRECTORY_SEPARATOR;
        $this->full_path = $this->getDownloadPath();
    }

    /**
     * create file
     * @param $transformFunctions !Optional! - array of functions:
     *          transformData - if you want to change the data before saving the file
     *          transformHeader - if you want to change the header before saving the file
     * @return array
     */
    public function create($transformFunctions=null) {
        $fp = new SplFileObject($this->storage_path . $this->getFullPath(), 'w');

        $fcount = 0;
        if ($this->limit != $this->count) {
            $fcount = ceil($this->count / $this->limit);
        }
        if(!is_callable($transformFunctions["SQL_Query"])){
            for ($i = 0; $i <= $fcount; $i++) {
                $data = $this->getData($i * $this->limit, $this->limit);
                if ($i == 0) {
                    $this->headersToFile($data, $fp);
                }
                $this->writeToFile($data, $fp);
            }
        }
        else{
            $data = $this->getDataBySQLQuery($transformFunctions["SQL_Query"]);
            $this->headersToFile($data, $fp, $transformFunctions['transformHeader']);
            $this->writeToFile($data, $fp, $transformFunctions['transformData']);
        }

        return $this->full_path;
    }



    /**
     * setter name to file
     *
     * @param $name
     */
    public function setFileName($name) {
        $this->full_path = $this->getDownloadPath($name);
    }

    /**
     * getter full file path
     *
     * @return null|string
     */
    public function getFullPath() {
        return $this->full_path;
    }

    /**
     * create limit
     *
     * @param $count
     * @param $limit
     * @return mixed
     */
    private function setBranches($count, $limit) {
        if ($limit > $count) {
            return $count;
        }

        return $limit;
    }

    /**
     * get download full path
     *
     * @param null $name
     * @return string
     */
    public function getDownloadPath($name = null)
    {
        $directory = 'export' . DIRECTORY_SEPARATOR . 'ReportCsv';
        if (!file_exists($this->storage_path . $directory)) {
            if(!mkdir($this->storage_path . $directory, 0777, true)) {
                return ['file' => 0, 'desc' => 'Cannot create folder'];
            }
        }

        if (is_null($name)) {
            $name = time();
        }

        if (file_put_contents($this->storage_path . $directory . DIRECTORY_SEPARATOR . $name . '.csv', '')) {
            return ['file' => 0, 'desc' => 'Cannot create file'];
        }
        return $directory . DIRECTORY_SEPARATOR . $name . '.csv';
    }

    /**
     * run query
     *
     * @param $offset
     * @param $limit
     * @return mixed
     */
    private function getData($offset, $limit) {
        try {
            return $this->model->offset($offset)->limit($limit)->get()->toArray();
        } catch (\Exception $e) {
            dd($e->getMessage()); //TODO remove
        } catch (\PDOException $e) {
            dd($e->getMessage()); //TODO remove
        }
    }

    /**
     * run query
     *
     * @param $offset
     * @param $limit
     * @return mixed
     */
    private function getDataBySQLQuery($sql) {
        try {
            return DB::select($sql());
        } catch (\Exception $e) {
            dd($e->getMessage()); //TODO remove
        } catch (\PDOException $e) {
            dd($e->getMessage()); //TODO remove
        }
    }

    /**
     * insert data to file
     * @param $data
     * @param $file
     * @param $transformHeader !Optional!  -  if you want to change the header before saving the file
     */
    private function writeToFile($data, &$file, $transformData=null) {

        foreach ($data as $d) {
            $row = [];
            if (is_callable($transformData))
                $row = $transformData($d);
            else
                $this->convertToArray($d, $row);
            $file->fputcsv($row);
        }
    }
    /**
     * insert header to file
     *
     * @param $data
     * @param $file
     * @param $transformHeader - if you want to change the header before saving the file
     */

    private function headersToFile($data, &$file, $transformHeader=null) {
        if (is_array($data)) {
            $row = [];
            if(is_callable($transformHeader)){
                $keys = $transformHeader();
            }
            else{
                $this->convertToArray($data[0], $row);
                $keys = array_keys($row);
            }
            $file->fputcsv($keys);
        }
    }

    /**
     * reclusive convert associative array to simple array key -> value
     *
     * @param $data
     * @param $row
     * @param null $key
     */
    private function convertToArray($data, &$row, $key = null) {
        foreach ($data as $index => $value) {
            if (is_array($value)) {
                $this->convertToArray($value, $row, $index);
            } else {
                $current = is_null($key) ? $index : $key . '.' . $index;
                $row[$current] = $value;
                unset($current);
            }
        }
    }
}