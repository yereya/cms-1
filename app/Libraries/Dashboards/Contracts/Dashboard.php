<?php


namespace App\Libraries\Dashboards\Contracts;


interface Dashboard
{
    /**
     * Build Data
     *
     * @return mixed
     */
    public function build();

    /**
     * Pass Data To View
     *
     * @return mixed
     */
    public function get();
}