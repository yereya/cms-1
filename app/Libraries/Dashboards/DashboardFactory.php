<?php namespace App\Libraries\Dashboards;

use App\Libraries\Dashboards\Controllers\ChartStats;
use App\Libraries\Dashboards\Controllers\FlagStats;
use App\Libraries\Dashboards\Controllers\SummaryStats;
use App\Libraries\Dashboards\Controllers\TableStats;

class DashboardFactory
{
    public function make($widget, $request)
    {
        switch ($widget) {
            case 'summary':
                return (new SummaryStats($request));
            case 'chart':
                return (new ChartStats($request));
            case 'table':
                return (new TableStats($request));
            case 'flag':
                return (new flagStats($request));

        }
    }
}