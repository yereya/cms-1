<?php namespace App\Libraries\Dashboards\Controllers;

use App\Entities\Repositories\Bo\Advertisers\TargetsRepo;
use App\Entities\Repositories\Dwh\DashDailyStatsRepo;
use Illuminate\Http\Request;

/**
 * Class ChartStats
 *
 * @package App\Libraries\Dashboards\Controllers
 */
class ChartStats extends Dashboards
{
    /**
     * @var
     */
    protected $stats = [];

    /**
     * @var DashDailyStatsRepo
     */
    protected $dash_daily_stats_repo;

    /**
     * Dashboards constructor.
     *
     * @param  $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->dash_daily_stats_repo = (new DashDailyStatsRepo());
    }

    /**
     * Build Data
     *
     * @return mixed
     */
    public function build()
    {
        $this->chartFactory();
    }

    /**
     * Pass Data To View
     *
     * @return mixed
     */
    public function get()
    {
        return $this->stats;
    }

    /**
     * Filter requests by type
     */
    private function chartFactory()
    {
        $type = request()->input('type');
        switch ($type) {
            case 'advertiser_target':
                return $this->buildPpcTargets();

            case 'ppc_morning_routine':
                return $this->buildMorningRoutine();

            default:
                return $this->buildDailyStats();
        }
    }

    /**
     * Build Ppc Chart
     * 1. Get advertisers targets from (dash-daily stats).
     * 2. Get results from (advertiser targets).
     * 3 .Union them together and assign to $stats.
     */
    private function buildPpcTargets()
    {
        $advertiser_targets_repo = new TargetsRepo();
        $daily_stats_repo        = new DashDailyStatsRepo();
        $targets                 = $advertiser_targets_repo->getStatsTarget();
        $results                 = $daily_stats_repo->getStatsResultBySAdvertiser();

        if ($targets && $results) {
            $this->stats = $this->unionStats($targets, $results);
        }
    }

    /**
     * Build Daily Stats Chart
     */
    private function buildDailyStats()
    {
        $this->stats = $this->dash_daily_stats_repo->getDailyStats();

        if (!empty($this->stats)) {
            $extremal = $this->getExtremalNumbers();

            if (!empty($extremal) && !empty($this->stats)) {
                $this->stats = array_merge($this->stats, $extremal);
            }
        }
    }

    /**
     * Get Extremal Numbers
     *
     * @return array|bool
     */
    private function getExtremalNumbers()
    {
        $all_numbers = [];
        $target      = request()->input('target_type') ?? 'cost_profit';

        $paramsPerTarget = [
            'cost_profit'      => ['cost', 'revenue', 'profit'],
            'clicks'           => ['clicks'],
            'sales_count'      => ['sales_count'],
            'avg_position'     => ['avg_position'],
            'cts'              => ['cts'],
            'site_ctr'         => ['site_ctr'],
            'cpc'              => ['cpc'],
            'click_out_unique' => ['click_out_unique']
        ];

        foreach ($this->stats as $stat) {
            foreach ($paramsPerTarget[$target] as $key => $val) {
                $all_numbers[] = (float)$stat[$val];
            }
        }

        if (!$all_numbers) {
            return false;
        }

        $max = max($all_numbers);
        $min = min($all_numbers);

        $max = round($max * 1.06, -1);
        $min = $min < 0 ? floor($min * 1.4) : round($min * 0.4);

        return [
            'max' => $max,
            'min' => $min
        ];
    }


    /**
     * @param $targets
     * @param $results
     *
     * @return mixed
     */
    private function _unionStats($targets, $results)
    {
        $targets = $targets->keyBy('date');
        $type    = request()->input('target') ?? 'sales';

        $targets_n_results = $results->map(function ($result, $index) use ($targets, $type, $results) {
            $target_column = (new TargetsRepo())->resolveTargetByDuration($result, $targets);

            if (!$target_column) {

                return 'remove';
            }

            $date = $this->resolveDate($index, $results, $result);

            return [
                'target' => $target_column ?? 0,
                'result' => $result->{$type} ?? 0,
                'date'   => $date,
            ];
        });

        $targets_n_results = $targets_n_results->filter(function ($record) {
            return $record !== 'remove';
        });


        return $targets_n_results
            ->values()
            ->sortBy('date')
            ->all();
    }

    /**
     * @param $index
     * @param $results
     * @param $result
     *
     * @return mixed
     */
    private function resolveDate($index, $results, $result)
    {
        $date_range      = request()->input('date_range');
        $date_range      = explode(' ', $date_range['value']);
        $date_range      = [
            'from' => $date_range[0],
            'to'   => $date_range[1]
        ];
        $first_iteration = $index == 0;
        $last_iteration  = $index + 1 == count($results);
        $date            = $result->date;

        if ($first_iteration) {
            $date = $date_range['from'];
        }
        if ($last_iteration) {
            $date = $date_range['to'];
        }

        return $date;
    }

    /**
     *
     */
    private function buildMorningRoutine()
    {
        //get all stats per advertiser
        $advertisers_stats = $this->dash_daily_stats_repo->getMorningStats();

        /*get over-all for all advertisers */
        $overall_stats = $this->dash_daily_stats_repo->getOverallStats();

        /*merge data */
        $this->stats = array_merge_recursive($advertisers_stats, $overall_stats);

        /*add labels*/
        $this->stats['labels']   = $this->dash_daily_stats_repo->getAdvertisers()->pluck('advertiser_name');
        $this->stats['labels'][] = 'Overall';
    }

    /**
     * @param $targets
     * @param $results
     *
     * @return \Illuminate\Support\Collection
     */
    private function unionStats($targets, $results)
    {
        $graphs = $results->map(function ($result, $idx) use ($targets) {
            if (!$targets->has($idx)) {
                return false;
            }

            $zero_in_targets = $targets[$idx]['cost'] == 0 || ((int)$targets[$idx]['conversion']) == 0;
            $zero_in_results = $result['cost'] == 0 || ((int)$result['conversion']) == 0;

            $target_cpa = $zero_in_targets ? 0 : $targets[$idx]['cost'] / (int)$targets[$idx]['conversion'];
            $result_cpa = $zero_in_results ? 0 : $result['cost'] / (int)$result['conversion'];


            $valid_cpa         = $target_cpa !== 0 && $result_cpa !== 0;
            $valid_profits     = $result['profit'] != 0 && $targets[$idx]['profit'] != 0;
            $valid_conversions = $result['conversion'] != 0 && $targets[$idx]['conversion'] != 0;

            $cpa        = $valid_cpa
                ? round(($result_cpa / $target_cpa) * 100)
                : 0;
            $profit     = $valid_profits
                ? round(($result['profit'] / $targets[$idx]['profit']) * 100)
                : 0;
            $conversion = $valid_conversions
                ? round(((int)$result['conversion'] / (int)$targets[$idx]['conversion']) * 100)
                : 0;

            return [
                'cpa'        => $cpa,
                'profit'     => $profit,
                'conversion' => $conversion,
                'date'       => date('m-d', strtotime($result['date']))
            ];
        });

        return $graphs;
    }
}