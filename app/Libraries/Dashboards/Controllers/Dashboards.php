<?php


namespace App\Libraries\Dashboards\Controllers;


use App\Libraries\Dashboards\Contracts\Dashboard;
use Illuminate\Http\Request;

abstract class Dashboards implements Dashboard
{

    /**
     * @var mixed|null
     */
    protected $from_date;

    /**
     * @var mixed|null
     */
    protected $to_date;

    /**
     * @var
     */
    protected $period;

    /**
     * Dashboards constructor.
     *
     * @param  $request
     */
    public function __construct(Request $request)
    {
        $this->period    = $request->input('period') ?? null;
    }
}