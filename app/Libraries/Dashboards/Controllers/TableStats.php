<?php


namespace App\Libraries\Dashboards\Controllers;


use App\Entities\Repositories\Alerts\BudgetRepo;
use App\Entities\Repositories\Alerts\CampaignRepo;
use App\Entities\Repositories\Alerts\AccountRepo;
use App\Entities\Repositories\Bo\Advertisers\TargetsRepo;
use App\Entities\Repositories\Dwh\DashDailyStatsRepo;
use App\Entities\Repositories\PageSpeedRepo;
use Illuminate\Http\Request;

/**
 * Class TableStats
 *
 * @package App\Libraries\Dashboards\Controllers
 */
class TableStats extends Dashboards
{

    const YESTERDAY = 'yesterday';
    const TWO_WEEKS_AGO = 'two_weeks_ago';
    const DAILY_TARGETS = 'daily_targets';
    const DISTANCE_FROM_TARGET = 'distance_from_target';

    /**
     * @var DashDailyStatsRepo
     */
    protected $daily_stats_repo;
    /**
     * @var TargetsRepo
     */
    protected $advertisers_targets_repo;
    /**
     * @var PageSpeedRepo
     */
    protected $pagespeed_repo;
    /**
     * @var
     */
    protected $table;

    /**
     * TableStats constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->daily_stats_repo = (new DashDailyStatsRepo());
        $this->advertisers_targets_repo = (new TargetsRepo());
        $this->pagespeed_repo = (new PageSpeedRepo());
    }

    /**
     * Build Data
     */
    public function build()
    {
        $this->tableResolver();
    }

    /**
     * Pass Data To View
     *
     * @return mixed
     */
    public function get()
    {
        return $this->table;
    }

    /**
     * Resolve table by type
     */
    private function tableResolver()
    {
        $type = request()->input('type');
        switch ($type) {
            case 'ppc-morning-routine':
                return $this->createPpcMorningRoutine();
                break;

            case 'manager-target':
                return $this->createManagerTarget();
                break;

            case 'page-speed':
                return $this->createPageSpeed();
                break;

            default:
                return $this->createPpc();
        }
    }

    /**
     * Set table info
     */
    private function createManagerTarget()
    {
        $headers = [
            ['label' => 'Advertiser', 'key' => 'advertiser_name'],
            ['label' => 'CPA Actual', 'key' => 'cpa'],
            ['label' => 'CPA Target', 'key' => 'cpa_target'],
            ['label' => 'CPA DistanceFrom Target', 'key' => 'cpa_distance'],
            ['label' => 'Leads/Sales Actual', 'key' => 'sales'],
            ['label' => 'Leads/Sales Target', 'key' => 'sales_target'],
            ['label' => 'Leads/Sales Distance From Target', 'key' => 'sales_distance'],
            ['label' => 'Profit Actual', 'key' => 'profit'],
            ['label' => 'Profit Target', 'key' => 'profit_target'],
            ['label' => 'Profit Distance From Target', 'key' => 'profit_distance'],
            ['label' => 'ROI', 'key' => 'roi']
        ];

        $targets = $this->advertisers_targets_repo->getAdvertiserStatsTarget();
        $stats = $this->daily_stats_repo->getYesterdayGroupedByAdvertiser();
        $month = $this->daily_stats_repo->getLastMonthGroupedByAdvertisers();
        $dist_from_target = $this->advertisers_targets_repo->getAdvertisersDifferenceFromTarget($targets);
        $rows = $this->advertisers_targets_repo->unifyAdvertisersStats($targets, $stats, $dist_from_target);
        $table = $this->prepareMangerTargetTable($headers, $rows);
        $this->table = $table;
    }

    /**
     * Set table info
     */

    private function createPageSpeed()
    {
        $headers = [
            ['label' => 'Site   ||  Scores', 'key' => 'site_url'],
            ['label' => "Today's Desktop", 'key' => 'desktop_score'],
            ['label' => "Yesterday's Desktop", 'key' => 'yesterday_desktop_score'],
            ['label' => "Today's Mobile", 'key' => 'mobile_score'],
            ['label' => "Yesterday's Mobile", 'key' => 'yesterday_mobile_score'],
            ['label' => 'Date', 'key' => 'created_at']
        ];

        $rows = $this->pagespeed_repo->getRows();
        $rows = $this->setFontColorAttributes($rows);
        $table = $this->preparePageSpeedTable($headers, $rows);
        $this->table = $table;
    }

    /*
     *  Sets color attributes according to scores increase/decrease
     *  0 = same value, no color
     *  1 = lower value than yesterday, red color
     *  2 = higher value than yesterday, blue color
     */

    private function setFontColorAttributes($rows)
    {
        foreach ($rows as $index => $row) {
            $row['desktop_attributes'] = [];
            $row['mobile_attributes'] = [];
            if ($row['desktop_score'] > $row['yesterday_desktop_score']) {
                $row['desktop_attributes']['desktop'] = "2";
            } elseif ($row['desktop_score'] < $row['yesterday_desktop_score']) {
                $row['desktop_attributes']['desktop'] = "1";
            } else {
                $row['desktop_attributes']['desktop'] = "0";
            }

            if ($row['mobile_score'] > $row['yesterday_mobile_score']) {
                $row['mobile_attributes']['mobile'] = "2";
            } elseif ($row['mobile_score'] < $row['yesterday_mobile_score']) {
                $row['mobile_attributes']['mobile'] = "1";
            } else {
                $row['mobile_attributes']['mobile'] = "0";
            }
            $rows[$index] = $row;
        }
        return $rows;
    }

    private function createPpc()
    {
        $params = $this->getTableParams('advertiser-targets');
        $table = $this->prepare($params['headers'], $params['rows']);
        $this->table = $table;
    }

    /**
     * Prepare table response.
     *
     * @param $headers
     * @param $rows
     *
     * @return array|bool
     * @internal param $columns
     */
    private function prepare($headers, $rows)
    {
        $variables = [
            'table' => [
                'headers' => $headers,
                'rows' => $rows,
                'data' => []
            ],
            'counter' => 0,
        ];

        foreach ($rows as $subject => $row) {

            $variables['table']['subject'] = $subject;

            if (array_key_exists('callback', $row)
                && \is_callable($row['callback'])) {
                $variables['table']['params'] = $row['callback']();
            }

            if (!empty($variables['table']['params']['ratio'])) {
                $variables['table']['ratio'] = $variables['table']['params']['ratio'];
                unset($variables['table']['params']['ratio']);
            }

            $raw_data = $variables['table']['params'];

            if (!$raw_data) {
                return [];
            }

            $variables['table'] = $this->resolveTableStructureByRow($variables);
            $variables['counter']++;
        }

        return $variables['table'];
    }

    /**
     * @param $headers
     * @param $rows
     * @return array
     */
    private function preparePageSpeedTable($headers, $rows)
    {
        $table = [];
        $table['headers'] = $headers;
        foreach ($rows as $row) {
            $table['data'][] = [
                'site_url' => ['value' => $row['site_url'], 'attributes' => []],
                'desktop_score' => ['value' => $row['desktop_score'], 'attributes' => $row['desktop_attributes']],
                'yesterday_desktop_score' => ['value' => $row['yesterday_desktop_score'], 'attributes' => []],
                'mobile_score' => ['value' => $row['mobile_score'], 'attributes' => $row['mobile_attributes']],
                'yesterday_mobile_score' => ['value' => $row['yesterday_mobile_score'], 'attributes' => []],
                'created_at' => ['value' => $row['created_at'], 'attributes' => []]
            ];
        }

        return $table;
    }


    private function prepareMangerTargetTable($headers, $rows)
    {
        $table = [];
        $table['headers'] = $headers;
        foreach ($rows as $row) {
            $table['data'][] = [
                'advertiser_name' => ['value' => $row['advertiser_name'], 'attributes' => []],
                'cpa' => ['value' => $row['cpa'], 'attributes' => $this->resolveAttributesByKey('cpa')],
                'cpa_target' => ['value' => $row['cpa_target'], 'attributes' => $this->resolveAttributesByKey('cpa_target')],
                'cpa_distance' => ['value' => $row['cpa_distance'], 'attributes' => $this->resolveAttributesByKey('cpa_distance')],
                'sales' => ['value' => $row['sales'], 'attributes' => $this->resolveAttributesByKey('sales')],
                'sales_target' => ['value' => $row['sales_target'], 'attributes' => $this->resolveAttributesByKey('sales_target')],
                'sales_distance' => ['value' => $row['sales_distance'], 'attributes' => $this->resolveAttributesByKey('sales_distance')],
                'profit' => ['value' => $row['profit'], 'attributes' => $this->resolveAttributesByKey('profit')],
                'profit_target' => ['value' => $row['profit_target'], 'attributes' => $this->resolveAttributesByKey('profit_target')],
                'profit_distance' => ['value' => $row['profit_distance'], 'attributes' => $this->resolveAttributesByKey('profit_distance')],
                'roi' => ['value' => (float)$row['roi'], 'attributes' => $this->resolveAttributesByKey('roi')]
            ];
        }
        return $table;
    }


    /**
     * Resolve columns attributes for given key.
     *
     * @param string $key
     *
     * @return array|mixed
     */
    public function resolveAttributesByKey(string $key)
    {
        $attributes = [
            'subject' => [
                'type' => 'subject',
            ],
            'site_ctr' => [
                'suffix' => '%'
            ],
            'sales' => [
                'class' => 'sales'
            ],
            'cpa' => [
                'suffix' => '$',
                'class' => 'cpa'
            ],
            'profit' => [
                'suffix' => '$',
                'class' => 'profit'
            ],
            'clicks' => [
                'class' => 'clicks'
            ],
            'cost' => [
                'suffix' => '$',
                'class' => 'cost'
            ],
            'budget' => [
                'suffix' => '$',
                'class' => 'budget',
                'currency' => 2
            ],
            'revenue' => [
                'suffix' => '$',
                'class' => 'revenue'
            ],
            'action' => [
                'class' => 'action',
                'empty' => 1
            ],
            'id' => [
                'class' => 'record-id',
                'data' => [
                    'key' => 'record_id'
                ]
            ],
            'advertiser_name' => [
                'class' => 'advertiser'
            ],
            'ctl' => [
                'suffix' => '%',
                'decimal' => 2
            ],
            'lts' => [
                'suffix' => '%',
                'decimal' => 2
            ],
            'cts' => [
                'suffix' => '%',
                'decimal' => 2
            ],
            'roi' => [
                'suffix' => '%'
            ],
            'cpc' => [
                'suffix' => '$'
            ],
            'usage' => [
                'class' => 'usage',
                'decimal' => 2
            ],
            'account' => [
                'class' => 'account'
            ],
            'profit_target' => [
                'suffix' => '$'
            ],
            'profit_distance' => [
                'suffix' => '$'
            ],
            'income' => [
                'suffix' => '$'
            ],
            'cpa_target' => [
                'suffix' => '$'
            ],
            'cpa_distance' => [
                'suffix' => '$'
            ],
            'leads_target' => [
            ],
            'a_cost' => [
                'suffix' => '$',
                'class' => 'actual-field bg-strip text-center',
                'decimal' => 2,
                'description' => 'actual-record',
                'dashboard' => 'ppc-target',
            ],
            'a_revenue' => [
                'suffix' => '$',
                'class' => 'actual-field  text-center',
                'decimal' => 2,
                'description' => 'actual-record',
                'dashboard' => 'ppc-target',
            ],
            'a_epa' => [
                'suffix' => '$',
                'class' => 'actual-field  text-center',
                'decimal' => 2,
                'description' => 'actual-record',
                'dashboard' => 'ppc-target',
            ],
            'a_profit' => [
                'suffix' => '$',
                'class' => 'actual-field bg-strip text-center',
                'decimal' => 2,
                'description' => 'actual-record',
                'dashboard' => 'ppc-target',
            ],
            'a_conversions' => [
                'class' => 'actual-field  text-center',
                'decimal' => 2,
                'description' => 'actual-record',
                'dashboard' => 'ppc-target',
            ],
            'a_cpa' => [
                'class' => 'actual-field bg-strip text-center',
                'suffix' => '$',
                'decimal' => 2,
                'description' => 'actual-record',
                'dashboard' => 'ppc-target',
            ],
            't_cost' => [
                'suffix' => '$',
                'class' => 'target-field bg-strip text-center ',
                'decimal' => 2,
                'dashboard' => 'ppc-target',
            ],
            't_revenue' => [
                'suffix' => '$',
                'class' => 'target-field text-center',
                'decimal' => 2,
                'dashboard' => 'ppc-target',
            ],
            't_epa' => [
                'suffix' => '$',
                'class' => 'target-field text-center',
                'decimal' => 2,
                'dashboard' => 'ppc-target',

            ],
            't_profit' => [
                'suffix' => '$',
                'class' => 'target-field bg-strip text-center',
                'decimal' => 2,
                'dashboard' => 'ppc-target',
            ],
            't_conversions' => [
                'class' => 'target-field  text-center',
                'decimal' => 2,
                'format' => true,
                'dashboard' => 'ppc-target',
            ],
            't_cpa' => [
                'class' => 'target-field bg-strip text-center',
                'decimal' => 2,
                'suffix' => '$',
                'dashboard' => 'ppc-target',
            ],
            't_roi' =>[
                'class' => 'target-field bg-strip text-center',
                'decimal' => 1,
                'suffix' => '%',
                'dashboard' => 'ppc-target',
            ],
            'a_roi' =>[
                'class' => 'target-field bg-strip text-center',
                'suffix' => '%',
                'decimal' => 1,
                'dashboard' => 'ppc-target',

            ]
        ];

        return $attributes[$key] ?? [];
    }

    /**
     * Create morning routine
     */
    private function createPpcMorningRoutine()
    {
        $name = request()->input('section') ?? 'summary';

        if ($name === 'summary') {

            return $this->createSummary();
        }

        return $this->createAlerts();
    }

    /**
     * Build multi rows to each category
     *
     * @param $variables
     *
     * @return bool
     */
    private function buildMultiRows($variables)
    {
        $subject = $variables['table']['subject'];
        $params = $variables['table']['params'];

        foreach ($variables['table']['headers'] as $index => $header) {

            foreach ($params as $key => $item) {

                /*build subject key-value*/
                foreach ($item as $name => $val) {
                    $variables['table']['data'][$subject][$key][$name] = [
                        'value' => false !== strpos($name, 'a_') ? $val['value'] : $val,
                        'attributes' => $this->resolveAttributesByKey(strtolower($name))
                    ];

                    if (false !== strpos($name, 'a_')) {
                        $variables['table']['data'][$subject][$key][$name]['ratio'] = $val['ratio'];
                    }
                }
            }
        }
        $variables['table']['data'] = array_values($variables['table']['data']);

        return $variables['table'];
    }

    /**
     * @param $variables
     *
     * @return mixed
     */
    private function buildRow($variables)
    {
        $counter = $variables['counter'];
        $params = $variables['table']['params'];

        foreach ($variables['table']['headers'] as $index => $header) {

            /*other then */
            $variables['table']['data'][$counter][$header['key']] = [

                /*if index is the subject of table = set the name of the table*/
                'value' => $header['key'] == 'subject'
                    ? $variables['table']['subject']
                    : (isset($params[$header['key']]) ? $params[$header['key']] : 0),

                'attributes' => $this->resolveAttributesByKey($header['key'])
            ];
        }

        return $variables['table'];
    }

    /**
     * @param $variables
     *
     * @return mixed
     */
    private function resolveTableStructureByRow($variables)
    {
        $multi_param_state = isset($variables['table']['params'][0]);

        if ($multi_param_state) {
            return $this->buildMultiRows($variables);
        }

        return $this->buildRow($variables);
    }

    /**
     * @param $data
     * @param $table_name
     *
     * @return mixed
     */
    private function mergeBeforeSend($data, $table_name)
    {
        if (!$data) {
            return false;
        }

        $temp_table = array_values($data);

        if (!empty($temp_table[1])) {
            $temp_table = array_merge($temp_table[0], $temp_table[1]);
            $temp_table = collect($temp_table);
            $sorted = collect();
            $advertisers = [];


            $temp_table->each(function ($item) use ($temp_table, &$advertisers, $sorted, $table_name) {
                $advertiser = isset($item['advertiser_name']) ? $item['advertiser_name']['value'] : 'no-advertiser';

                if (in_array($advertiser, $advertisers)) {
                    return;
                }

                $advertiser_records = $temp_table->filter(function ($record) use ($advertiser) {
                    return $record['advertiser_name']['value'] == $advertiser;
                })->values();

                if ($table_name == 'daily_stats') {

                    $advertiser_records = $this->resolvePercentageChange($advertiser_records);
                }

                $sorted_records = $advertiser_records->sortByDesc('subject')->values()->toArray();
                $sorted->push($sorted_records);

                array_push($advertisers, $advertiser);
            });

            $sorted = $sorted->collapse();

        } else {
            $sorted = $temp_table[0];
        }


        return $sorted;
    }

    /**
     * Create summary table
     */
    private function createSummary()
    {
        $table = $this->getTableParams('daily_stats');
        $table = $this->prepare($table['headers'], $table['rows']);
        if (!$table) {
            return [];
        }

        $table['data'] = $this->mergeBeforeSend($table['data'], 'daily_stats');
        $this->table = $table;
    }

    /**
     * Create Alerts tables
     */
    private function createAlerts()
    {
        $table_name = request()->input('table_name') ?? 'budgets';

        $table = $this->getTableParams($table_name);
        $table = $this->prepare($table['headers'], $table['rows']);

        if (!$table) {
            return $this->table = [];
        }

        $table['data'] = $this->mergeBeforeSend($table['data'], $table_name);
        $this->table = $table;
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    private function getTableParams($name)
    {
        $table = [
            'target' => [
                'section' => $name,
                'headers' => [
                    ['label' => 'Subject', 'key' => 'subject'],
                    ['label' => 'Sales/Leads', 'key' => 'sales'],
                    ['label' => 'CPA', 'key' => 'cpa'],
                    ['label' => 'Profit', 'key' => 'profit']
                ],
                'rows' => [
                    'Last 14 Days Avg' => [
                        'callback' => function () {

                            return $this->daily_stats_repo->resolveStatsByPeriod(self::TWO_WEEKS_AGO);
                        }
                    ],
                    'Daily Target' => [
                        'callback' => function () {
                            $daily_targets = $this->advertisers_targets_repo->getDailyTargets();

                            return $daily_targets;
                        }
                    ],
                    'To Reach Monthly Target' => [
                        'callback' => function () {

                            return $this->advertisers_targets_repo->getDifferenceFromTarget();
                        }
                    ],
                    'To Reach Quarterly Target' => [
                        'callback' => function () {

                            return $this->advertisers_targets_repo->getQuarterDifferenceByTargets();
                        }
                    ]
                ]
            ],
            'daily_stats' => [
                'section' => $name,
                'headers' => [
                    ['label' => 'Advertiser', 'key' => 'advertiser_name'],
                    ['label' => 'Subject', 'key' => 'subject'],
                    ['label' => 'Clicks', 'key' => 'clicks'],
                    ['label' => 'Cost', 'key' => 'cost'],
                    ['label' => 'Revenue', 'key' => 'revenue'],
                    ['label' => 'CPC', 'key' => 'cpc'],
                    ['label' => 'CPA', 'key' => 'cpa'],
                    ['label' => 'Leads', 'key' => 'leads'],
                    ['label' => 'Sales', 'key' => 'sales'],
                    ['label' => 'CTL', 'key' => 'ctl'],
                    ['label' => 'LTS', 'key' => 'lts'],
                    ['label' => 'CTS', 'key' => 'cts'],
                    ['label' => 'Profit', 'key' => 'profit'],
                    ['label' => 'Site CTR', 'key' => 'site_ctr'],
                    ['label' => 'ROI', 'key' => 'roi']
                ],
                'rows' => [
                    'Yesterday' => [
                        'callback' => function () {
                            $params = [
                                'table' => 'daily_stats',
                                'fields' => [
                                    'outer' => 'advertiser_name,"Yesterday" AS subject ,ROUND(SUM(`clicks`)) clicks, ROUND(SUM(`cost`)) cost,
                                          ROUND(SUM(`revenue`)) AS revenue,ROUND(SUM(`cost`) / SUM(`clicks`), 2) cpc, ROUND(SUM(`cost`) / SUM(`sales`)) cpa,
                                          ROUND(SUM(`leads`)) AS leads,ROUND(SUM(`sales`)) AS sales, (SUM(`leads`) / SUM(`clicks`)) ctl,
                                          (SUM(`sales`) / SUM(`leads`)) lts, (SUM(`sales`) / SUM(`clicks`)) cts, ROUND(SUM(`profit`)) AS profit,
                                          ROUND(SUM(`out_clicks`) / SUM(`clicks`), 2) site_ctr, SUM(`revenue`) / SUM(`cost`) roi',

                                    'inner' => 'advertiser_name, sales_count as sales, clicks, cost ,revenue ,leads,
                                               profit,stats_date_tz, out_clicks,accounts.advertiser_id as advertiser_id',
                                ],
                                'groupBy' => 'advertiser_name',
                                'orderBy' => 'advertiser_name',
                                'inner_table_alias' => 'b'
                            ];

                            $yesterday = $this->daily_stats_repo->getYesterdayStats($params)->toArray();

                            return $yesterday;
                        }
                    ],
                    'Last 4 Weeks Avg' => [
                        'callback' => function () {
                            $params = [
                                'table' => 'daily_stats',
                                'fields' => [
                                    'outer' => '  advertiser_name ,
                                                  "Last 4 Week Avg" AS Subject,
                                                  ROUND((SUM(`clicks`) / 4)) clicks,
                                                  ROUND((SUM(`cost`) / 4)) cost,
                                                  ROUND((SUM(`revenue`) / 4)) revenue,
                                                  ROUND((SUM(`cost`) / SUM(`clicks`)), 2) cpc,
                                                  ROUND((SUM(`cost`) / SUM(`sales_count`))) cpa,
                                                  ROUND((SUM(`leads`) / 4)) leads,
                                                  ROUND((SUM(`sales_count`) / 4)) sales,
                                                  (SUM(`leads`) / SUM(`clicks`)) ctl,
                                                  (SUM(`sales_count`) / SUM(`leads`)) lts,
                                                  (SUM(`sales_count`) / SUM(`clicks`)) cts,
                                                  ROUND((SUM(`profit`) / 4)) profit,
                                                  ROUND((SUM(`out_clicks`) / SUM(`clicks`)), 2) AS site_ctr,
                                                  (SUM(`revenue`) / SUM(`cost`)) roi ',

                                    'inner' => 'b.advertiser_name, b.sales_count, b.clicks, b.cost ,b.revenue ,b.leads, b.profit, b.stats_date_tz, b.out_clicks,
                                                accounts.advertiser_id as advertiser_id',
                                ],
                                'groupBy' => 'advertiser_name',
                                'orderBy' => 'advertiser_name',
                                'inner_table_alias' => 'b'
                            ];

                            $last_4_weeks_avg = $this->daily_stats_repo->getLast4WeeksAvg($params)->toArray();

                            return $last_4_weeks_avg;
                        }
                    ]
                ]
            ],
            'budgets' => [
                'section' => $name,
                'headers' => [
                    ['label' => 'Id', 'key' => 'id'],
                    ['label' => 'Advertiser', 'key' => 'advertiser_name'],
                    ['label' => 'Date', 'key' => 'stats_date_tz'],
                    ['label' => 'Budget Name', 'key' => 'budget_name'],
                    ['label' => 'Budget', 'key' => 'budget'],
                    ['label' => 'Cost', 'key' => 'cost'],
                    ['label' => 'Usage', 'key' => 'usage'],
                    ['label' => 'Account', 'key' => 'account_name'],
                    ['label' => 'Exceed', 'key' => 'exceed'],
                    ['label' => 'Update hour', 'key' => 'update_hour'],
                    ['label' => 'Action', 'key' => 'action']
                ],
                'rows' => [
                    'Yesterday' => [
                        'callback' => function () {
                            $budget_repo = new BudgetRepo();
                            $params = [
                                'table' => 'budgets',
                                'fields' => [
                                    'outer' => 'id, advertiser_name,  stats_date_tz, budget_name,  
                                              budget, cost, `usage`, account_name account ,exceed, update_hour, ""  action ',
                                    'inner' => 'b.id, adv.name as advertiser_name,  b.stats_date_tz, b.budget_name,  
                                              b.budget, b.cost, b.`usage`, b.account_name ,b.exceed, b.update_hour',
                                ],
                                'groupBy' => 'budget_name',
                                'orderBy' => 'advertiser_name'
                            ];
                            $yesterday = $budget_repo->getYesterdayStats($params)->toArray();

                            return $yesterday;
                        }
                    ]
                ]
            ],
            'campaigns' => [
                'section' => $name,
                'headers' => [
                    ['label' => 'Id', 'key' => 'id'],
                    ['label' => 'Advertiser', 'key' => 'advertiser_name'],
                    ['label' => 'Date', 'key' => 'stats_date_tz'],
                    ['label' => 'Publisher', 'key' => 'publisher_name'],
                    ['label' => 'Account', 'key' => 'account_name'],
                    ['label' => 'Campaign', 'key' => 'campaign_name'],
                    ['label' => 'Clicks', 'key' => 'clicks'],
                    ['label' => 'Avg Clicks', 'key' => 'avg_clicks'],
                    ['label' => 'Yesterday CPC', 'key' => 'yesterday_cpc'],
                    ['label' => 'Cpc', 'key' => 'cpc'],
                    ['label' => 'Exception', 'key' => 'exception'],
                    ['label' => 'Action', 'key' => 'action']
                ],
                'rows' => [
                    'Yesterday' => [
                        'callback' => function () {
                            $campaign_repo = new CampaignRepo();
                            $params = [
                                'table' => 'campaigns',
                                'fields' => [
                                    'outer' => 'id,advertiser_name, date, publisher_name as publisher,
                                         account_name as account, campaign_name as campaign, clicks, avg_clicks,
                                         yesterday_cpc, cpc, exception, ""  action',
                                    'inner' => 'adv.id advertiser_id, adv.`name` AS advertiser_name 
                                    ,b.id, stats_date_tz as date, publisher_name, account_name, campaign_name, clicks, avg_clicks,
                                     yesterday_cpc, cpc, exception',
                                ],
                                'groupBy' => 'campaign_name',
                                'orderBy' => 'advertiser_name'
                            ];

                            $yesterday = $campaign_repo->getYesterdayStats($params)->toArray();

                            return $yesterday;
                        }
                    ]
                ],

            ],
            'accounts' => [
                'section' => $name,
                'headers' => [
                    ['label' => 'Id', 'key' => 'id'],
                    ['label' => 'Advertiser', 'key' => 'advertiser_name'],
                    ['label' => 'Date', 'key' => 'stats_date_tz'],
                    ['label' => 'Publisher', 'key' => 'publisher_name'],
                    ['label' => 'Account', 'key' => 'account_name'],
                    ['label' => 'Clicks', 'key' => 'clicks'],
                    ['label' => 'Avg Clicks', 'key' => 'avg_clicks'],
                    ['label' => 'Exception', 'key' => 'exception'],
                    ['label' => 'Action', 'key' => 'action']
                ],
                'rows' => [
                    'Yesterday' => [
                        'callback' => function () {
                            $account_repo = new AccountRepo();
                            $params = [
                                'table' => 'accounts',
                                'fields' => [
                                    'outer' => 'id, advertiser_name, date, publisher_name as publisher,
                                            account_name as account, clicks, avg_clicks, exception, ""  action',
                                    'inner' => 'adv.id advertiser_id, adv.`name` AS advertiser_name,b.id,
                                     publisher_name, account_name, clicks, avg_clicks, exception, b.stats_date_tz as date',
                                ],
                                'groupBy' => 'account_name',
                                'orderBy' => 'advertiser_name'
                            ];

                            $yesterday = $account_repo->getYesterdayStats($params)->toArray();

                            return $yesterday;
                        }
                    ]
                ]
            ],
            'advertiser-targets' => [
                'section' => $name,
                'headers' => [
                    ['label' => 'Advertiser', 'key' => 'advertiser'],
                    ['label' => 'Target Cost', 'key' => 't_cost'],
                    ['label' => 'Actual Cost', 'key' => 'a_cost'],
                    ['label' => 'Target Revenue', 'key' => 't_revenue'],
                    ['label' => 'Actual Revenue', 'key' => 'a_revenue'],
                    ['label' => 'Target Profit', 'key' => 't_profit'],
                    ['label' => 'Actual Profit', 'key' => 'a_profit'],
                    ['label' => 'Target Conversion', 'key' => 't_conversions'],
                    ['label' => 'Actual Conversion', 'key' => 'a_conversions'],
                    ['label' => 'Target CPA', 'key' => 't_cpa'],
                    ['label' => 'Actual CPA', 'key' => 'a_cpa'],
                    ['label' => 'Target EPA', 'key' => 't_epa'],
                    ['label' => 'Actual EPA', 'key' => 'a_epa'],
                    ['label' => 'Target ROI', 'key' => 't_roi'],
                    ['label' => 'Actual ROI', 'key' => 'a_roi'],
                ],
                'rows' => [
                    'Target Vs Actual' => [
                        'callback' => function () {
                            $display_values = request()->input('display-values') ?? 'daily';

                            /*run according to display values*/
                            $values_to_method = [
                                'daily' => function () use ($display_values) {
                                    $targets = $this->advertisers_targets_repo->getDailyAvgTargets($display_values);
                                    $actual_results = $this->daily_stats_repo->getDailyAvgActualResultsBySAdvertiser($display_values);
                                    # --- If we would like to go back to use advertise instead of s_advertiser
                                    //$actual_results = $this->daily_stats_repo->getDailyAvgActualResults($display_values);
                                    return [
                                        'target' => count($targets) ? $targets : false,
                                        'actual' => count($actual_results) ? $actual_results : false
                                    ];
                                },
                                'till-now' => function () use ($display_values) {
                                    $targets = $this->advertisers_targets_repo->getTillNowTargets($display_values);
                                    $actual_results = $this->daily_stats_repo->getTillNowActualResultsBySAdvertiser($display_values);
                                    # --- If we would like to go back to use advertise instead of s_advertiser
//                                    $actual_results = $this->daily_stats_repo->getTillNowActualResults($display_values);


                                    return [
                                        'target' => count($targets) ? $targets : false,
                                        'actual' => count($actual_results) ? $actual_results : false
                                    ];
                                }
                            ];

                            /*load results into new variable*/
                            $results = $values_to_method[$display_values]();

                            /*return empty array if no results */
                            if (!$results['actual'] || !$results['target']) {
                                return [];
                            }

                            /*build results according to target and actual  */
                            $target_vs_actual = $this->daily_stats_repo->getTargetVsActual($results['target'], $results['actual']);

                            # Create 'Total' Row
                            $advertisers = $target_vs_actual->toArray();
                            $total_row = $this->calculateTotal($advertisers); // Returns the sum for each parameter for all the advertisers
                            if(!empty($total_row)){
                                $advertisers[] = $total_row;
                            }

                            return $advertisers;
                        }
                    ],
                ]
            ]
        ];

        return $table[$name];
    }
    /**
     * @param $advertisers
     *
     * @return $new_row
     */
    private function calculateTotal($advertisers)
    {
        // * The order of the keys in the $new_row is important for presenting the data
        $new_row['advertiser'] = 'Total';
        $temp_row = [];
        $keys = ['cost', 'revenue', 'profit', 'conversions', 'epa', 'roi'];
        try {
            # Sum the KPIs for all the advertisers
            foreach ($keys as $key) {  // For each key loop over all the
                foreach ($advertisers as $advertiser) {
                    if($key == 'epa' || $key == 'roi'){
                        if (!isset($temp_row['t_' . $key])) { // If the key haven't been set yet
                            $temp_row['t_' . $key] = $advertiser['t_' . $key];
                            $temp_row['a_' . $key]['value'] = $advertiser['a_' . $key]['value'];
                        } else { // Add to the existing value
                            $temp_row['t_' . $key] += $advertiser['t_' . $key];
                            $temp_row['a_' . $key]['value'] += $advertiser['a_' . $key]['value'];
                        }
                    }else {
                        if (!isset($new_row['t_' . $key])) { // If the key haven't been set yet
                            $new_row['t_' . $key] = $advertiser['t_' . $key];
                            $new_row['a_' . $key]['value'] = $advertiser['a_' . $key]['value'];
                        } else { // Add to the existing value
                            $new_row['t_' . $key] += $advertiser['t_' . $key];
                            $new_row['a_' . $key]['value'] += $advertiser['a_' . $key]['value'];
                        }
                    }
                }
                # Calculate the ratio
                if($key != 'epa' && $key!='roi') {
                    if(isset($new_row['a_' . $key]['value']) && isset($new_row['t_' . $key]) && $new_row['t_' . $key])
                        $new_row['a_' . $key]['ratio'] = intval(sprintf('%0.2f', $new_row['a_' . $key]['value'] / $new_row['t_' . $key] * 100));
                    else
                        $new_row['a_' . $key]['ratio'] = 'NA';
                }else{
                    if(isset($temp_row['a_' . $key]['value']) && isset($temp_row['t_' . $key]) && $temp_row['t_' . $key]) {
                        if($key == 'roi')
                            $temp_row['a_' . $key]['ratio'] = ($temp_row['a_' . $key]['value'] / $temp_row['t_' . $key]);
                        else
                            $temp_row['a_' . $key]['ratio'] = intval(sprintf('%0.2f', $temp_row['a_' . $key]['value'] / $temp_row['t_' . $key] * 100));
                    }
                    else {
                        $temp_row['a_' . $key]['ratio'] = 'NA';
                    }
                }
            }
            # Calculate CPA
            if(isset($new_row['t_cost']) && isset($new_row['t_conversions']) && $new_row['t_conversions'])
                $new_row['t_cpa'] = sprintf('%0.0f', $new_row['t_cost'] / $new_row['t_conversions']);
            else
                $new_row['t_cpa'] = 'NA';
            if(isset($new_row['a_cost']['value']) && isset($new_row['a_conversions']['value']) && $new_row['a_conversions']['value'])
                $new_row['a_cpa']['value'] = sprintf('%0.0f', $new_row['a_cost']['value'] / $new_row['a_conversions']['value']);
            else
                $new_row['a_cpa']['value'] = 'NA';
            if(isset($new_row['a_cpa']['value']) && isset($new_row['t_cpa']) && $new_row['t_cpa'])
                $new_row['a_cpa']['ratio'] = intval(sprintf('%0.2f', $new_row['a_cpa']['value'] / $new_row['t_cpa'] * 100));
            else
                $new_row['a_cpa']['ratio'] = 'NA';
            $new_row['t_epa'] = $temp_row['t_epa'];
            $new_row['a_epa'] = $temp_row['a_epa'];
            $new_row['t_roi'] = $temp_row['t_roi'];
            $new_row['a_roi'] = $temp_row['a_roi'];
            return $new_row;


        } catch (\Exception $e) {
            return [];
        }
    }
    /**
     * @param $advertiser_records
     *
     * @return mixed
     */
    public function resolvePercentageChange($advertiser_records)
    {
        $advertisers = [];
        $first_record = [];

        $advertiser_records->each(function ($record, $key) use ($advertisers, &$first_record) {
            $advertiser_name = $record['advertiser_name']['value'];

            if ($key == 0) {
                $first_record = $record;

                return;
            }

            if (!in_array($advertiser_name, $advertisers)) {
                foreach ($record as $name => $item) {
                    if (strtolower($name) != 'advertiser_name' && strtolower($name) != 'subject') {
                        $current_record = $first_record[$name]['value'];
                        $avg_record = $record[$name]['value'];
                        $no_zero = $avg_record != 0;
                        $entirely_negative = $avg_record < 0 && $current_record < 0;
                        $exclusive_negative = $avg_record * $current_record < 0;

                        /*check if result not contain zero value */
                        if ($no_zero) {
                            $first_record[$name]['attributes']['ratio'] = ($current_record / $avg_record) * 100;

                            /*check if one of the values  negative*/
                            if ($exclusive_negative) {
                                $first_record[$name]['attributes']['ratio'] = (1 - $current_record / $avg_record) * 100;
                                continue;

                            } elseif ($entirely_negative) {
                                $first_record[$name]['attributes']['ratio'] *= -1;
                            }
                        } else {
                            $first_record[$name]['attributes']['ratio'] = 0;
                        }
                    }
                }
            }
        });

        $advertiser_records[0] = $first_record;

        return $advertiser_records;
    }

    /**
     * @param $targets
     * @param $results
     * @return \Illuminate\Support\Collection
     */
    private function getRatio($targets, $results): \Illuminate\Support\Collection
    {
        $ratio = collect();
        $targets->map(function ($target, $key) use ($results, $ratio) {
            $temp = collect();
            $fields = ['cost', 'revenue', 'profit', 'conversions', 'cpa'];
            array_map(/**
             * @param $field
             */
                function ($field) use ($target, $results, $key, $temp) {
                    $ratio = 'NA';
                    $target_field = 't_' . $field;
                    $result_field = 'a_' . $field;

                    $target_advertiser_id = $target['advertiser_id'];
                    $results = $results->keyBy('advertiser_id');

                    if (\count($results->where('advertiser_id', $target_advertiser_id)->first())) {
                        $valid_values = ($results[$target_advertiser_id][$result_field] != 0) && ($target[$target_field] != 0);

                        if ($valid_values) {
                            $ratio = round(($results[$target_advertiser_id][$result_field] / $target[$target_field]) * 100);
                        }
                    }

                    $temp->put('advertiser_id', $target['advertiser_id']);
                    $temp->put($field, $ratio);
                }, $fields
            );

            $ratio->push($temp);
        });

        return $ratio;
    }

    /**
     * @param $name
     * @param $key
     * @param $records
     * @return bool|number
     */
    private function resolveRatio($name, $key, $records)
    {
        $ratio = '';
        if (false !== strpos($name, 'a_')) {
            $ratio = $records[$key][str_replace('a_', '', $name)];
        }

        return $ratio;
    }
}
