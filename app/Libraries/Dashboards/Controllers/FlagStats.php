<?php


namespace App\Libraries\Dashboards\Controllers;


use App\Entities\Repositories\Alerts\DynamicRepo;

class FlagStats extends Dashboards
{
    protected $stats;

    /**
     * Build Data
     *
     * @return mixed
     */
    public function build()
    {
        $dynamic_repo = (new DynamicRepo());
        $this->stats = $dynamic_repo->statsFactory();
    }

    /**
     * Pass Data To View
     *
     * @return mixed
     */
    public function get()
    {
       return $this->stats;
    }
}