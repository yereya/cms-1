<?php namespace App\Libraries\Scheduler;

use App\Entities\Models\FieldsQueryParam;
use App\Entities\Models\Scheduler\Task;
use App\Libraries\TpLogger;

/**
 * Class TasksLib
 *
 * @package App\Libraries
 */
class SchedulerTasksLib
{

    /**
     * @var array $field_query_params
     */
    private $field_query_params = [];

    /**
     * @var TpLogger
     */
    private $tp_logger;

    /**
     * @var int
     */
    private $system_id = 3;


    public function __construct()
    {
        $this->tp_logger = TpLogger::getInstance();
        $this->tp_logger->setSystem($this->system_id);
    }

    
    public function fireCron()
    {
        $tasks = $this->getDueTasks();
        if (!count($tasks)) {
            return false;
        }

        $this->tp_logger->info('Scheduled tasks: '. count($tasks) . ' - '. date('Y-m-d H:i:s'));
        $this->HandleTasks($tasks);
    }
    
    
    /**
     * Create or update a related record matching the attributes, and fill it with params.
     *
     * @param  integer  $field_query_param_id
     * @param  integer  $task_id
     * @param  array    $params
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function updateOrCreate($field_query_param_id, $task_id, array $params)
    {
        $task = Task::where('task_id', $task_id)->where('field_query_params_id', $field_query_param_id)->first();

        $params['field_query_params_id'] = $field_query_param_id;
        $params['task_id'] = $task_id;
        $params['cli_custom_command'] = with(new self)->generateNewCliCommand($field_query_param_id, $params);

        if (is_null($task)) {
            $result = Task::create($params);
        } else {
            $result = $task->fill($params)->save();
        }

        return (bool) $result;
    }


    /**
     * Finds all the due to run tasks
     * 
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    private function getDueTasks()
    {
        $query = "  active = 1
          AND ((date_format(now(), '%w') = day_of_week) OR (day_of_week IS NULL))
          AND ((date_format(now(), '%e') >= days_from AND date_format(now(), '%e') <= days_until AND days_from IS NOT NULL AND
                days_until IS NOT NULL)
               OR (days_from IS NULL AND days_until IS NULL))
          AND (
            (date_format(now(), '%H:%i') >= date_format(run_time_start, '%H:%i') AND
             date_format(now(), '%H:%i') <= date_format(run_time_stop, '%H:%i')
             AND run_time_start IS NOT NULL AND run_time_stop IS NOT NULL AND
             mod(floor(time_to_sec(now()) / 60) - (floor(time_to_sec(run_time_start) / 60)), repeat_delay) = 0
            ) OR (run_time_start IS NULL AND run_time_stop IS NULL)
          )
          AND (
            (date_format(now(), '%H') >= hours_from AND date_format(now(), '%H') <= hours_until
             AND hours_from IS NOT NULL AND hours_until IS NOT NULL AND
             mod(floor(time_to_sec(now()) / 60) - (hours_from * 60), repeat_delay) = 0
            ) OR (hours_from IS NULL AND hours_until IS NULL)
          )
          AND (
            (date_format(now(), '%i') >= minutes_from AND date_format(now(), '%i') <= minutes_until
             AND minutes_from IS NOT NULL AND minutes_until IS NOT NULL AND
             mod(floor(time_to_sec(now()) / 60) - minutes_from, repeat_delay) = 0
            ) OR (minutes_from IS NULL AND minutes_until IS NULL)
          )";

        return Task::whereRaw($query)->get();
    }


    /**
     * Call The Tasks - will activate all the timed tasks
     *
     * @param $tasks
     * @throws \Exepetion
     */
    private function HandleTasks($tasks)
    {
        foreach ($tasks as $task) {
            $command = $this->buildCliCommand($task);
            if (!$command) {
                continue;
            }

            try {
                $this->tp_logger->info(['Cli Command: ', $command]);

                // Build exec command string
                // it will redirect the output stream to a different file
                // which will allow us to create multiple calls (multi threading)
                $command_string = '/usr/bin/php ' . base_path() . '/artisan ' . $command . ' > /dev/null &';
                exec($command_string);
            } catch (\Exception $e) {
                $this->tp_logger->error(['Exception:', $e]);
            }
        }
    }


    /**
     * Build Cli Command - finds the command used to run the specific task
     * 
     * @param $task
     * @return null|string
     * @throws \Exception
     */
    private function buildCliCommand($task)
    {
        if ($task->cli_custom_command) {
            $command = $task->cli_custom_command .' '. $task->cli_command_params;
        } else {
            $command = $this->generateNewCliCommand($task->field_query_params_id);
        }

        return $command;
    }


    /**
     * Generate New Cli Command - Will create the command and replace all the place
     * holders with it's correct arguments
     *
     * @param $field_query_params_id
     * @param array $args
     * @return null
     * @throws \Exception
     */
    private function generateNewCliCommand($field_query_params_id, $args = [])
    {
        $this->getQueryParamRecord($field_query_params_id);

        // Build the Cli command in case we managed to find the record
        $command = FieldsQueryParam::buildCliCommand($this->field_query_params[$field_query_params_id]);

        if (isset($command)) {
            // Replace the place holders with it's $args value
            foreach ((array) $args as $key => $val) {
                if (strpos($command, '['. $key .']')) {
                    $command = str_replace('['. $key .']', $val, $command);
                }
            }
        } else {
            $command = '';
        }

        return $command;
    }


    /**
     * Get Query Params Record - find the correct field query param
     *
     * @param $field_query_params_id
     * @throws \Exception
     */
    private function getQueryParamRecord($field_query_params_id)
    {
        // Retrieve the field query params record
        // Singleton pattern
        if (!isset($this->field_query_params[$field_query_params_id])) {
            $this->field_query_params[$field_query_params_id] = FieldsQueryParam::where('id', $field_query_params_id)->first();

            if (!$this->field_query_params[$field_query_params_id]) {
                throw new \Exception('Could not find the cli command for this task');
            }
        }
    }
}