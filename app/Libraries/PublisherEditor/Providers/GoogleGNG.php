<?php namespace app\Libraries\PublisherEditor\Providers;

use AdWordsUser;
use Cache;

abstract class GoogleGNG
{
    /**
     * @var AdWordsUser
     */
    protected $user;

    /**
     * @var - current account - source account id
     */
    protected $account_id;

    /**
     * Google constructor.
     *
     * @param $source_account_id
     */
    public function __construct($source_account_id)
    {
        $this->user = new AdWordsUser(config_path() . '/adwords-auth-gng.ini');
        $this->refreshTokenCache();
        $this->account_id = $source_account_id;
    }

    /**
     * @return $this - this object
     */
    public function get() {
        return $this;
    }

    /**
     * @param $customer_id or account_id - source account id
     */
    public function setCustomerId($customer_id) {
        $this->user->SetClientCustomerId($customer_id);
    }

    /**
     * Getter AccountId
     *
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->account_id;
    }

    /**
     * Setter AccountId
     *
     * @param $account_id - source account id
     */
    public function setAccountId($account_id)
    {
        $this->account_id = $account_id;
    }

    /**
     * Execute service
     *
     * @param $selector
     *
     * @param $service
     *
     * @return mixed
     * @throws \Exception
     */
    protected function execute($selector, $service) {
        try {
            return $service->get($selector);
        } catch (\SoapFault $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Preload Service
     *
     * @return mixed
     */
    abstract protected function preloadService();

    /**
     * Update service
     *
     * @param $service - campaign service|budget service
     * @param $operator
     *
     * @return mixed
     * @throws \Exception
     */
    protected function mutate($service, $operator)
    {
        try {
            $result = $service->mutate($operator);
            return $result;
        } catch (Exception $e) {
            throw $e;
        } catch (\SoapFault $e) {
            throw $e;
        }
    }

    /**
     * Refresh Token Cache
     */
    private function refreshTokenCache()
    {
        if (!Cache::has('google_oauth2')) {
            $this->requestAuthorization();

            Cache::put('google_oauth2', $this->user->GetOAuth2Info(), 60 * 24 * 30 * 3);
        } else {
            $this->user->SetOAuth2Info(Cache::get('google_oauth2'));
        }
    }

    /**
     * Request Authorization
     */
    private function requestAuthorization()
    {
        $OAuth2Handler      = $this->user->GetOAuth2Handler();
        $authorization_url  = $OAuth2Handler->GetAuthorizationUrl($this->user->GetOAuth2Info(), null, true);
        $authorization_code = $this->requestAuthorizationCode($authorization_url);
        $access_token       = $OAuth2Handler->GetAccessToken($this->user->GetOAuth2Info(), $authorization_code);

        $this->user->SetOAuth2Info($access_token);
    }

    /**
     * Request Authorization Code
     *
     * @param $authorizationUrl
     *
     * @return mixed
     */
    private function requestAuthorizationCode($authorizationUrl)
    {
        print "Log in to your AdWords account and open the following URL:\n{$authorizationUrl}\n\n";
        print "After approving the token enter the authorization code here: ";

        $stdin = fopen('php://stdin', 'r');
        $code  = trim(fgets($stdin));
        fclose($stdin);

        return $code;
    }
}
