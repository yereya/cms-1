<?php namespace App\Libraries\PublisherEditor\Operations\GoogleGNG;

use App\Libraries\PublisherEditor\Operations\Constants;
use App\Libraries\PublisherEditor\Providers\GoogleGNG;
use App\Libraries\PublisherEditor\Operations\GoogleGNG\ParseGoogleResponse;
use \Exception;
use Money;
use Predicate;
use Selector;

/**
 * Class Campaign
 *
 * Examples:
 *
 * @_showCampaignStatus
 *          EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setCampaignId('283224624')
 *          ->getStatus();
 *          return ENABLED|PAUSED
 * @_storeCampaignStatus
 *          EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setCampaignId('283224624')
 *          ->setStatus(Campaign::CAMPAIGN_STATUS_ENABLED);
 *          return type Campaign (v201607)
 *          https://developers.google.com/adwords/api/docs/reference/v201607/CampaignService.Campaign
 *
 * @_getCampaignBudget
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setCampaignId('283224624')
 *          ->getBudget();
 * return amount (Amount in micros. One million is equivalent to one unit.)
 *        this campaign budget and shared budget - equivalents
 * @_storeCampaignBudget
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setCampaignId('283224624')
 *          ->storeBudget($amount Amount in micros. One million is equivalent to one unit.);
 * return type Campaign (v201607)
 * https://developers.google.com/adwords/api/docs/reference/v201607/CampaignService.Campaign
 *
 * @package App\Libraries\PublisherEditor\Operations\Google
 */
class Campaign extends GoogleGNG
{
    use ParseGoogleResponse;

    /**
     * @var campaign source id
     */
    private $campaign_id;

    /**
     * @var budget source id
     */
    private $budget_id;

    /**
     * @var adwords user service
     */
    private $service;

    /**
     * @var adwords user service budget
     */
    private $service_budget;

    /**
     * Getter CampaignId
     *
     * @return mixed
     */
    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    /**
     * Setter CampaignId
     *
     * @param $campaign_id - source campaign id
     *
     * @return $this
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = is_array($campaign_id) ? $campaign_id : [$campaign_id];

        return $this;
    }

    /**
     * Getter Budget Id
     *
     * @return budget id
     */
    public function getBudgetId()
    {
        return $this->budget_id;
    }

    /**
     * Setter Budget Id
     *
     * @param $budget_id - budget source id
     *
     * @return $this
     */
    public function setBudgetId($budget_id)
    {
        $this->budget_id = $budget_id;

        return $this;
    }

    /**
     * Return status
     *
     * @return mixed|UNKNOWN|ENABLED|PAUSED|REMOVED
     */
    public function getStatus()
    {
        $this->preloadService();
        $response = $this->execute($this->buildRequest(), $this->service);
        return $this->parseResponse(Constants::RESPONSE_TYPE_STATUS, $response);
    }

    /**
     * Get Budget amount
     *
     * @param string $response_type
     *
     * @return mixed
     */
    public function getBudget($response_type = Constants::RESPONSE_BUDGET_AMOUNT)
    {
        $this->preloadService();
        $response = $this->execute($this->buildRequest(), $this->service);
        return $this->parseResponse($response_type, $response);
    }

    /**
     * Get Budget amount
     *
     * @param string $response_type
     *
     * @return mixed
     */
    public function getTarget($response_type = Constants::RESPONSE_TYPE_TARGET)
    {
        $this->preloadService(Constants::ADWORDS_CAMPAIGN_CRITERION_SERVICE);
        $response = $this->execute($this->buildTargetRequest(), $this->service);
        return $this->parseResponse($response_type, $response);
    }

    /**
     * Store Status
     *
     * @param $status - ENABLED|PAUSED
     *
     * @return mixed - campaign object
     * @throws Exception
     */
    public function storeStatus($status)
    {
        if (empty($status)) {
            throw new Exception('This parameters status is required, please give this parameter in $status');
        }

        $this->preloadService();
        $campaigns = $this->createCampaign($status);
        $operation = $this->createOperator($campaigns);
        $records = $this->mutate($this->service, $operation);

        return $this->parseStoreResponse(Constants::RESPONSE_STORE, $records);
    }

    /**
     * Set Budget amount
     *
     * @param $amount - sum in *1000000
     *
     * @return mixed -
     * @throws Exception
     */
    public function storeBudget($amount)
    {
        if (empty($amount)) {
            throw new Exception('This parameters amount is required, please give this parameter in $amount');
        }
        $budgets = $this->getBudget(Constants::RESPONSE_BUDGET_ID);
        $this->preloadBudgetService();
        $money = $this->createMoney($budgets, $amount);
        $operations = $this->createBudgetOperation($money);
        $records = $this->mutate($this->service_budget, $operations);

        return $this->parseStoreResponse(Constants::RESPONSE_BUDGET, $records);
    }

    public function storeTarget($bid_adjustment) {
        if (empty($bid_adjustment) && $bid_adjustment != 0) {
            throw new Exception('This parameters bid adjustment is required, please give this parameter in $bid_adjustment');
        }

        $bids = $this->getTarget(Constants::RESPONSE_BID_ADJUSTMENT_ID);
        $this->preloadService(Constants::ADWORDS_CAMPAIGN_CRITERION_SERVICE);
        $platform = $this->createPlatform($bids, $bid_adjustment);
        $operations = $this->createCampaignOperator($platform);
        $records = $this->mutate($this->service, $operations);

        return $this->parseStoreResponse(Constants::RESPONSE_BID_ADJUSTMENT, $records);
    }

    /**
     * Preload service
     * Load customer id (google name, account id in local name)
     * Load CampaignService
     *
     * @param string $service
     *
     * @return mixed|void - account id null
     * @throws Exception - campaign id null
     */
    protected function preloadService($service = Constants::ADWORDS_CAMPAIGN_SERVICE)
    {
        if (!$this->campaign_id) {
            throw new Exception('Parameter Campaign Id not maybe empty, use method setCampaignId to set this parameter.');
        }

        $this->setCustomerId($this->account_id);
        $this->service = $this->user->GetService($service, Constants::ADWORDS_VERSION);
    }

    /**
     * PreLoad Budget Service
     * Load customer id (google name, account id in local name)
     *
     * @throws Exception - account id null
     */
    private function preloadBudgetService()
    {
        if (!$this->service_budget) {
            $this->service_budget = $this->user->GetService(Constants::ADWORDS_CAMPAIGN_BUDGET_SERVICE, Constants::ADWORDS_VERSION);
        }
    }

    /**
     * Build query
     *
     * @param $campaign_id - source campaign id
     *
     * @return string
     */
    private function buildQuery($campaign_id)
    {
        return sprintf('SELECT Id, Name, Status, BudgetId, Amount WHERE Id=%s', $campaign_id);
    }

    /**
     * @return Selector
     */
    private function buildRequest()
    {
        $selector = new Selector();
        $selector->fields = ["Id", 'Name', 'Status', 'BudgetId', 'Amount'];
        $selector->predicates[] = new Predicate('Id', 'IN', $this->getCampaignId());

        return $selector;
    }

    private function buildTargetRequest()
    {
        $selector = new Selector();
        $selector->fields = ['Id', 'CampaignId', 'BidModifier', 'CriteriaType', 'PlatformName'];
        $selector->predicates[] = new Predicate('CampaignId', 'IN', $this->getCampaignId());

        return $selector;
    }

    /**
     * Create campaign object
     *
     * @param $status
     *
     * @return \Campaign
     */
    private function createCampaign($status)
    {
        $campaigns = [];
        foreach ($this->getCampaignId() as $campaign_id) {
            $campaign = new \Campaign();
            $campaign->id = $campaign_id;
            $campaign->status = $status;

            $campaigns[] = $campaign;
        }

        return $campaigns;
    }

    /**
     * Create campaign operator
     *
     * @param $campaigns
     *
     * @return array
     */
    private function createOperator($campaigns)
    {
        $operations = [];
        foreach ($campaigns as $campaign) {
            $operator = new \CampaignOperation();
            $operator->operand = $campaign;
            $operator->operator = 'SET';

            $operations[] = $operator;
        }

        return $operations;
    }

    /**
     * Create Campaign Criterion
     *
     * @param $campaigns
     *
     * @return array
     */
    private function createCampaignOperator($campaigns) {
        $operations = [];
        foreach ($campaigns as $campaign) {
            $operator = new \CampaignCriterionOperation();
            $operator->operand = $campaign;
            $operator->operator = 'SET';

            $operations[] = $operator;
        }

        return $operations;
    }

    /**
     * Create Money obj and insert amount
     * Used to set campaign budget amount
     *
     * @param $budgets
     * @param $amount
     *
     * @return mixed
     */
    private function createMoney($budgets, $amount)
    {
        foreach ($budgets as $budget) {
            $budget->amount = new Money($amount);
            $budget->deliveryMethod = 'STANDARD';
        }

        return $budgets;
    }

    /**
     * @param $bids
     * @param $bid_adjustment
     *
     * @return mixed
     */
    private function createPlatform($bids, $bid_adjustment) {
        foreach ($bids as $bid) {
            $bid->bidModifier = $bid_adjustment;
        }

        return $bids;
    }

    /**
     * Create Budget operation
     *
     * @param $budgets
     *
     * @return array
     */
    private function createBudgetOperation($budgets)
    {
        $operations = [];

        foreach ($budgets as $budget) {
            $operation = new \BudgetOperation();
            $operation->operand = $budget;
            $operation->operator = 'SET';

            $operations[] = $operation;
        }

        return $operations;
    }

    /**
     * Execute query
     *
     * @param $type - ALL(return all object)|STATUS(return only status)
     * @param $records
     *
     * @return mixed campaign OBJECT|STATUS
     * @throws Exception - not found campaign
     */
    private function parseResponse($type, $records)
    {
        if (!isset($records->entries) || empty($records->entries)) {
            return null;
        }

        switch ($type) {
            case Constants::RESPONSE_TYPE_STATUS:
                return $this->parseStatus($records->entries);
            case Constants::RESPONSE_BUDGET_AMOUNT:
                return $this->parseBudget($records->entries);
            case Constants::RESPONSE_TYPE_TARGET:
                return $this->parseCampaignBidModifier($records->entries);
            case Constants::RESPONSE_BID_ADJUSTMENT_ID:
                return $this->parseCampaignBidModifierId($records->entries);
            case Constants::RESPONSE_BUDGET_ID:
                return $this->parseBudgetId($records->entries);
            default:
                return $records;
        }
    }

    private function parseStoreResponse($type, $records) {
        if (!is_null($records->partialFailureErrors)) {
            return false;
        }

        switch ($type) {
            case Constants::RESPONSE_STORE:
                return $this->parseStatus($records->value);
            case Constants::RESPONSE_BUDGET:
                return $this->parseStoreBudget($records->value);
            case Constants::RESPONSE_BID_ADJUSTMENT:
                return $this->parseStoreAdjustment($records->value);
            default:
                return $records;
        }
    }
}
