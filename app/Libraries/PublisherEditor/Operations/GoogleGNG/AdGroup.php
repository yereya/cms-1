<?php namespace App\Libraries\PublisherEditor\Operations\GoogleGNG;

use App\Libraries\PublisherEditor\Operations\Constants;
use App\Libraries\PublisherEditor\Providers\GoogleGNG;
use Exception;
use Predicate;
use Selector;

/**
 * Class AdGroup
 *
 * Examples:
 * @_getAdGroupStatus
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setAdGroupId('20280117144')
 *          ->getStatus();
 * return ENABLED|PAUSED
 * @_storeAdGroupStatus
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setAdGroupId('20280117144')
 *          ->setStatus(AdGroup::ADGROUP_STATUS_ENABLED);
 * return type AdGroup (v201607)
 * https://developers.google.com/adwords/api/docs/reference/v201607/AdGroupService.AdGroup
 *
 * @_getCampaignBudget
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setCampaignId('283224624')
 *          ->getBudget();
 * return amount (Amount in micros. One million is equivalent to one unit.)
 *        this campaign budget and shared budget - equivalents
 * @_storeCampaignBudget
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setCampaignId('283224624')
 *          ->storeBudget($amount Amount in micros. One million is equivalent to one unit.);
 * return type Campaign (v201607)
 * https://developers.google.com/adwords/api/docs/reference/v201607/AdGroupService.AdGroup
 *
 * @package App\Libraries\PublisherEditor\Operations\Google
 */
class AdGroup extends GoogleGNG
{
    use ParseGoogleResponse;
    /**
     * @var - ad group source id
     */
    private $ad_group_id;

    /**
     * @var - campaign id
     */
    private $campaign_id;

    /**
     * @var - ad group service
     */
    private $service;

    /**
     * Getter AdGroupId
     *
     * @return mixed
     */
    public function getAdGroupId()
    {
        return $this->ad_group_id;
    }

    /**
     * Setter AdGroupId
     *
     * @param $ad_group_id - source ad_group id
     * @return $this
     */
    public function setAdGroupId($ad_group_id)
    {
        $this->ad_group_id = is_array($ad_group_id) ? $ad_group_id : [$ad_group_id];

        return $this;
    }

    /**
     * Getter campaign id
     *
     * @return mixed
     */
    public function getCampaignId() {
        return $this->campaign_id;
    }

    public function setCampaignId($campaign_id) {
        $this->campaign_id = $campaign_id;

        return $this;
    }

    /**
     * Get all ad groups by campaign id
     *
     * @return array
     */
    public function getAdGroups() {
        $this->preloadAdGroupService();
        $response = $this->execute($this->buildAdGroupsRequest(), $this->service);
        return $this->parseResponse(Constants::RESPONSE_TYPE_ALL, $response);
    }

    /**
     * Get AdGroup status
     *
     * @return mixed ENABLED|PAUSED
     */
    public function getStatus()
    {
        $this->preloadService();
        $response = $this->execute($this->buildRequest(), $this->service);
        return $this->parseResponse(Constants::RESPONSE_TYPE_STATUS, $response);
    }

    /**
     * Get Bid
     *
     * @return array
     */
    public function getBid() {
        $this->preloadService();
        $response = $this->execute($this->buildRequest(), $this->service);
        return $this->parseResponse(Constants::RESPONSE_TYPE_BID, $response);
    }

    /**
     * Get device bid adjustment
     *
     * @param string $response_type
     *
     * @return array
     */
    public function getTarget($response_type = Constants::RESPONSE_BID_ADJUSTMENT) {
        $this->preloadService(Constants::ADWORDS_ADGROUP_BIDMODIFIER);
        $response = $this->execute($this->buildTargetRequest(), $this->service);
        return $this->parseResponse($response_type, $response);
    }

    /**
     * @param $adjustment
     *
     * @return array
     */
    public function storeTarget($adjustment) {
        $current_targets = $this->getTarget(Constants::RESPONSE_BID_ADJUSTMENT_ID);

        $this->preloadService(Constants::ADWORDS_ADGROUP_BIDMODIFIER);
        $platform = $this->createPlatform($current_targets, $adjustment);
        $operation = $this->createAdGroupCriterionOperator($platform);
        $records = $this->mutate($this->service, $operation);

        return $this->parseStoreResponse(Constants::RESPONSE_STORE, $records);
    }

    /**
     * Store new status to AdGroup
     *
     * @param $status
     *
     * @return mixed
     * @throws Exception
     */
    public function storeStatus($status)
    {
        if (empty($status)) {
            throw new Exception('This parameters status is required, please give this parameter in $status');
        }
        $this->preloadService();
        $ad_groups = $this->createAdGroup($this->getAdGroupId(), $status);
        $operation = $this->createOperator($ad_groups);
        $records = $this->mutate($this->service, $operation);
        return $this->parseStoreResponse(Constants::RESPONSE_STORE, $records);
    }

    /**
     * Store bid
     *
     * @param $bid
     *
     * @return array
     */
    public function storeBid($bid) {
        $this->preloadService();
        $bids = $this->createBids($this->getAdGroupId(), $bid);
        $operation = $this->createOperator($bids);
        $records = $this->mutate($this->service, $operation);
        return $this->parseStoreResponse(Constants::RESPONSE_TYPE_BID, $records);
    }

    /**
     * Preload service
     * Load customer id (google name, account id in local name)
     * Load AdGroupService
     *
     * @param string $service
     *
     * @return mixed|void - account id null
     * @throws Exception - campaign id null
     */
    protected function preloadService($service = Constants::ADWORDS_ADGROUP_SERVICE)
    {
        if (!$this->ad_group_id) {
            throw new Exception('Parameter AdGroup Id not maybe empty, use method setAdGroupId to set this parameter.');
        }

        if (!$this->service) {
            $this->setCustomerId($this->account_id);
            $this->service = $this->user->GetService($service, Constants::ADWORDS_VERSION);
        }
    }

    /**
     * Preload service
     * Load customer id (google name, account id in local name)
     * Load AdGroupService
     *
     * @throws Exception - account id null
     * @throws Exception - campaign id null
     */
    protected function preloadAdGroupService()
    {
        if (!$this->campaign_id) {
            throw new Exception('Parameter Campaign Id not maybe empty, use method setCampaignId to set this parameter.');
        }

        if (!$this->service) {
            $this->setCustomerId($this->account_id);
            $this->service = $this->user->GetService(Constants::ADWORDS_ADGROUP_SERVICE, Constants::ADWORDS_VERSION);
        }
    }

    /**
     * Create AdGroup
     *
     * @param $ad_group_id - source ad group id
     * @param $status - ENABLED|PAUSED
     * @return \AdGroup object
     */
    private function createAdGroup($ad_group_id, $status) {
        $ads = [];
        foreach ($ad_group_id as $ad_group) {
            $ad = new \AdGroup($ad_group);
            $ad->status = $status;

            $ads[] = $ad;
        }

        return $ads;
    }

    /**
     * Create bid cpcBid
     *
     * @param $ad_groups
     * @param $bid
     *
     * @return array
     */
    private function createBids($ad_groups, $bid) {
        $ads = [];

        foreach ($ad_groups as $ad_group) {
            $ad = new \AdGroup($ad_group);
            $bidding = new \BiddingStrategyConfiguration();
            $nbid = new \CpcBid();
            $money = new \Money();
            $money->microAmount = $bid;
            $nbid->bid = $money;
            $bidding->bids = [$nbid];
            $ad->biddingStrategyConfiguration = $bidding;
            $ads[] = $ad;
        }

        return $ads;
    }

    /**
     * @param $bid_adjustment
     *
     * @return mixed
     */
    private function createPlatform($bids, $bid_adjustment) {
        foreach ($bids as $bid) {
            $bid->bidModifier = $bid_adjustment;
        }

        return $bids;
    }

    /**
     * Create AdGroup operator
     *
     * @param $ad_groups - AdGroup object
     * @return array - operations array
     */
    private function createOperator($ad_groups) {
        $operations = [];
        foreach ($ad_groups as $ad_group) {
            $operation = new \AdGroupOperation();
            $operation->operand = $ad_group;
            $operation->operator = 'SET';

            $operations[] = $operation;
        }

        return $operations;
    }

    /**
     * Create AdGroup Criterion
     *
     * @param $ad_groups
     *
     * @return array
     */
    private function createAdGroupCriterionOperator($ad_groups) {
        $operations = [];
        foreach ($ad_groups as $ad_group) {
            $operator = new \AdGroupBidModifierOperation();
            $operator->operand = $ad_group;
            $operator->operator = 'SET';

            $operations[] = $operator;
        }

        return $operations;
    }

    /**
     * Build query
     *
     * @param $ad_group_id - source ad_group id
     * @return string
     */
    private function buildQuery($ad_group_id)
    {
        return sprintf('SELECT Id, CampaignId, Name, Status WHERE Id=%s', $ad_group_id);
    }

    private function buildRequest() {
        $selector = new Selector();
        $selector->fields = ['Id', 'CampaignId', 'Name', 'Status', 'BidType', 'CpcBid', 'TargetCpa', 'TargetCpaBid', 'TargetCpaBidSource'];
        $selector->predicates[] = new Predicate('Id', 'IN', array_values($this->getAdGroupId()));

        return $selector;
    }

    /**
     * @return Selector
     */
    private function buildAdGroupsRequest()
    {
        $selector = new Selector();
        $selector->fields = ['Id', 'CampaignId', 'Name', 'Status'];
        $selector->predicates[] = new Predicate('CampaignId', 'EQUALS', $this->getCampaignId());

        return $selector;
    }

    private function buildTargetRequest() {
        $selector = new Selector();
        $selector->fields = ['Id', 'AdGroupId', 'BidModifier', 'PlatformName'];
        $selector->predicates[] = new Predicate('AdGroupId', 'IN', array_values($this->getAdGroupId()));

        return $selector;
    }

    /**
     * @param $query
     * @param $type
     * @return mixed
     * @throws \Exception
     */
    private function executeQuery($query, $type) {
        $page = $this->service->query($query);

        if (isset($page->entries)) {
            foreach ($page->entries as $adgroup) {
                switch ($type) {
                    case Constants::RESPONSE_TYPE_STATUS:
                        return $adgroup->status;
                    default:
                        return $adgroup;
                }
            }
        } else {
            throw new \Exception('This AdGroup not found, check parameters in request.');
        }
    }

    private function parseResponse($type, $records) {
        if ($records->totalNumEntries == 0) {
            return null;
        }

        switch ($type) {
            case Constants::RESPONSE_TYPE_STATUS:
            case Constants::RESPONSE_TYPE_ADGROUP:
                return $this->parseStatus($records->entries);
            case Constants::RESPONSE_TYPE_BID:
                return $this->parseBid($records->entries);
            case Constants::RESPONSE_BID_ADJUSTMENT:
                return $this->parseAdGroupBidModifier($records->entries);
            case Constants::RESPONSE_BID_ADJUSTMENT_ID:
                return $this->parseCampaignBidModifierId($records->entries);
            default:
                return $records;
        }
    }

    private function parseStoreResponse($type, $records) {
        if (!is_null($records->partialFailureErrors)) {
            return false;
        }

        switch ($type) {
            case Constants::RESPONSE_STORE:
                return $this->parseStatus($records->value);
            case Constants::RESPONSE_TYPE_BID:
                return $this->parseBid($records->value);
            case Constants::RESPONSE_BID_ADJUSTMENT:
                return $this->parseStoreAdjustment($records->value);
            default:
                return $records;
        }
    }
}
