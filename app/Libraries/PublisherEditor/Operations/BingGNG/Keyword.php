<?php namespace App\Libraries\PublisherEditor\Operations\BingGNG;

use App\Libraries\PublisherEditor\Operations\Constants;
use App\Libraries\PublisherEditor\Providers\BingGNG;
use BingAds\CampaignManagement\Bid;
use BingAds\CampaignManagement\GetKeywordsByAdGroupIdRequest;
use BingAds\CampaignManagement\GetKeywordsByIdsRequest;
use BingAds\CampaignManagement\UpdateKeywordsRequest;

class Keyword extends BingGNG
{
    use ParseBingResponse;
    /**
     * @var - Ad Group Id - source ad group id
     */
    private $ad_group_id;

    /**
     * @var - Keyword Id
     */
    private $keyword_id;

    /**
     * @var - bing request
     */
    private $request;

    /**
     * Getter AdGroup Id
     *
     * @return mixed
     */
    public function getAdGroupId()
    {
        return $this->ad_group_id;
    }

    /**
     * Setter AdGroup Id
     *
     * @param $ad_group_id
     * @return $this
     */
    public function setAdGroupId($ad_group_id) {
        $this->ad_group_id = $ad_group_id;

        return $this;
    }

    /**
     * Getter Keyword Id
     *
     * @return mixed
     */
    public function getKeywordId() {
        return $this->keyword_id;
    }

    /**
     * Setter Keyword Id
     *
     * @param $keyword_id
     * @return $this
     */
    public function setKeywordId($keyword_id) {
        $this->keyword_id = is_array($keyword_id) ? $keyword_id : [$keyword_id];

        return $this;
    }

    /**
     * Get Keyword status
     *
     * @return mixed
     */
    public function getStatus()
    {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_KEYWORDS_BY_IDS, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_TYPE_STATUS);
    }

    /**
     * Get Keywords by AdGroup Id
     *
     * @return mixed
     */
    public function getKeywords()
    {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_KEYWORDS_BY_ADGROUP_ID, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_TYPE_ALL);
    }

    /**
     * Get Keyword
     *
     * @return mixed
     */
    public function getKeyword() {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_KEYWORDS_BY_IDS, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_TYPE_ALL);
    }

    /**
     * Get Keyword Bid Amount
     *
     * @return mixed
     */
    public function getBid() {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_KEYWORDS_BY_IDS, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_TYPE_BID);
    }

    /**
     * Store new status
     *
     * @param $status
     * @return mixed
     */
    public function storeStatus($status) {
        $this->preloadRequest('STORE_STATUS', $status);
        $records = $this->getRecords(Constants::UPDATE_KEYWORD, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_STORE);
    }

    /**
     * Store Keyword Bid Amount
     *
     * @param $amount
     * @return mixed
     */
    public function storeBid($amount) {
        $this->preloadRequest('STORE_BID', $amount);
        $records = $this->getRecords(Constants::UPDATE_KEYWORD, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_STORE);
    }

    /**
     * Preload request
     *
     * Check if campaign id given
     * Build Request object
     * @param $request_type
     * @param null $params
     */
    protected function preloadRequest($request_type, $params = null)
    {
        if (!$this->ad_group_id) {
            throw new Exception('Parameter AdGroup Id not maybe empty, use method setAdGroupId to set this parameter.');
        }

        if (!$this->keyword_id) {
            throw new Exception('Parameter Keyword Id not maybe empty, use method setKeywordId to set this parameter.');
        }

        $this->request = $this->buildRequest($request_type, $params);
    }

    /**
     * Build request
     *
     * @param $request_type
     * 1) Get - ad group status|ad group
     * 2) STORE_STATUS - store status to ad group
     * 3) BY_ADGROUP
     *
     * @param null $params
     * 1) GET - null
     * 2) STORE_STATUS - status
     * 3) BY_ADGROUP - Object type Keywords
     *
     * @return GetAdGroupsByIds
     */
    private function buildRequest($request_type, $params = null)
    {
        switch ($request_type) {
            case 'GET':
                return $this->buildGetRequest();
            case 'STORE_STATUS':
                return $this->buildStoreStatusRequest($params);
            case 'BY_ADGROUP':
                return $this->buildKeywordsRequest();
            case 'STORE_BID':
                return $this->buildStoreBidRequest($params);
        }
    }

    /**
     * Build Keywords request
     *
     * @return GetKeywordsByAdGroupIdRequest
     */
    private function buildKeywordsRequest() {
        $request = new GetKeywordsByAdGroupIdRequest();
        $request->AdGroupId = $this->getAdGroupId();

        return $request;
    }

    /**
     * Build Keyword request
     *
     * @return GetKeywordsByIdsRequest
     */
    private function buildGetRequest() {
        $request = new GetKeywordsByIdsRequest();
        $request->AdGroupId = $this->getAdGroupId();
        $request->KeywordIds = $this->getKeywordId();

        return $request;
    }

    /**
     * Build Status Request
     *
     * @param $status
     * @return UpdateKeywordsRequest
     */
    private function buildStoreStatusRequest($status)
    {
        $request = new UpdateKeywordsRequest();
        $request->AdGroupId = $this->getAdGroupId();

        $keywords = [];
        foreach ($this->getKeywordId() as $keyword_id) {
            $keyword = new \BingAds\CampaignManagement\Keyword();
            $keyword->Id = $keyword_id;
            $keyword->Status = $status;

            $keywords[] = $keyword;
        }

        $request->Keywords = $keywords;

        return $request;
    }

    /**
     * Build Bid Request
     *
     * @param $amount
     * @return UpdateKeywordsRequest
     */
    private function buildStoreBidRequest($amount) {
        $request = new UpdateKeywordsRequest();
        $request->AdGroupId = $this->getAdGroupId();

        $keywords = [];

        foreach ($this->getKeywordId() as $keyword_id) {
            $keyword = new \BingAds\CampaignManagement\Keyword();
            $bid = new Bid();
            $bid->Amount = $amount;
            $keyword->Bid = $bid;
            $keyword->Id = $keyword_id;

            $keywords[] = $keyword;
        }

        $request->Keywords = $keywords;

        return $request;
    }

    /**
     * Parse records from Bing
     *
     * @param $records - records
     * @param $type_result - STATUS|
     *
     * @return mixed - parsed result
     * @throws \Exception
     */
    private function parseRecord($records, $type_result)
    {
        $this->handlerException($records, Constants::ERROR_AD_GROUP_RESPONSE);

        switch ($type_result) {
            case Constants::RESPONSE_TYPE_STATUS:
                return $this->parseStatus($records->Keywords->Keyword);
            case Constants::RESPONSE_STORE:
                return empty($records->PartialErrors) ? true : false;
            default:
                return $records;
        }
    }
}
