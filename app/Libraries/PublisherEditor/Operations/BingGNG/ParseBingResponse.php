<?php namespace App\Libraries\PublisherEditor\Operations\BingGNG;

use App\Libraries\PublisherEditor\Operations\Constants;

trait ParseBingResponse
{
    /**
     * Parse status
     *
     * @param $records
     *
     * @return array
     */

    protected function parseStatus($records)
    {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                $results[$record->Id] = $record->Status == Constants::BING_STATUS_ENABLED ? 1 : 0;
            }
        }

        return $results;
    }

    /**
     * Parse budget
     *
     * @param $records
     *
     * @return array
     */
    protected function parseBudgetAmount($records)
    {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                $results[$record->Id] = $record->DailyBudget;
            }
        }

        return $results;
    }

    /**
     * @param $records
     *
     * @return array
     */
    protected function parseBidAmount($records)
    {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                $results[$record->Id] = $record->SearchBid->Amount;
            }
        }

        return $results;
    }

    /**
     * Parse campaign device bid adjustment
     *
     * @param        $campaigns
     * @param        $records
     * @param string $device
     *
     * @return array
     */
    protected function parseBidAdjustment($campaigns, $records, $device = Constants::DEVICE_SMARTPHONES)
    {
        $results = [];
        foreach ($records as $key => $record) {
            if (!is_null($record) && isset($record->DeviceOS)) {
                foreach ($record->DeviceOS->Bids->DeviceOSTargetBid as $bid) {
                    if ($bid->DeviceName == $device) {
                        $results[$campaigns[$key]] = $bid->BidAdjustment;
                    }
                }
            }
        }

        return $results;
    }

    /**
     * Parse errors
     *
     * @param $records
     * @param $error
     *
     * @throws \Exception
     */
    protected function handlerException($records, $error)
    {
        if (isset($records->PartialErrors->BatchError)) {
            throw new \Exception($error . json_encode($records->PartialErrors->BatchError));
        }
    }


    /**
     * @param $response
     *
     * @throws \Exception
     */
    protected function parseOfflineResults($response)
    {
        $this->handlerException($response, 'Error has occurred:');

        collect($response->ConversionGoalIds->long)->each(function ($id) {
            $this->parseString("Goal with the id -> {$id} added successfully\n\n");
        });

        $this->parseString("Conversions goals added successfully to Account ->", $this->account_id);
    }

    /**
     * @param null $errors
     *
     * @return bool
     */
    public function parseOfflineConversions($errors = null)
    {

        if (!$errors) {
            $this->parseString(self::FINISH_UPLOADING_OFFLINE_CONVERSION);
            return true;
        }

        $this->parseString(self::UPLOADING_WAS_FAILED_BECAUSE_OF_THE_FOLLOWING_ERROR, $errors[0]->Message);
    }


    /**
     * @param $string
     * @param $id
     */
    public function parseString($string, $id = null)
    {
        print("\n\n $string " . ($id ?? ' ') . "\n\n");
        $this->logger->info(["$string ", $id ?? $id]);
    }
}
