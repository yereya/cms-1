<?php namespace App\Libraries\PublisherEditor\Operations\BingGNG;

use App\Libraries\PublisherEditor\Operations\Constants;
use App\Libraries\PublisherEditor\Operations\BingGNG\ParseBingResponse;
use App\Libraries\PublisherEditor\Providers\BingGNG;

use BingAds\CampaignManagement\DeviceOSTarget;
use BingAds\CampaignManagement\DeviceOSTargetBid;
use BingAds\CampaignManagement\GetCampaignsByAccountIdRequest;
use BingAds\CampaignManagement\GetCampaignsByIdsRequest;
use BingAds\CampaignManagement\GetTargetsByCampaignIdsRequest;
use BingAds\CampaignManagement\Target;
use BingAds\CampaignManagement\UpdateCampaignsRequest;
use BingAds\CampaignManagement\UpdateTargetsInLibraryRequest;
use Exception;

/**
 * Class Campaign
 *
 * @package app\Libraries\PublisherEditor\Operations\Bing
 *
 * @Examples to use:
 * EditorLib::campaign(Account::where('source_account_id', '38000895')->first())
 *          ->setCampaignId('91267359')
 *          ->storeBudget(0.45);
 *
 * var
 * @var_$campaign_id
 * @var_$request
 *
 * public
 * @method_getCampaignId - getter campaign id
 * @method_setCampaignId - setter campaign id
 * @method_getStatus - get campaign status, vars: account, source_campaign_id
 * @method_getBudget - get budget amount
 * @method_getBudgetId - get budget id
 * @method_getCampaign - get campaign
 * @method_getTarget - get target (device bid adjustment)
 * @method_storeBudget - save budget amount (example: 12.00)
 * @method_storeStatus - save new campaign status, vars account, source_campaign_id, status(Active, Paused)
 * @method_storeTarget - save target (device bid adjustment) percent only
 *
 * protected
 * @method_preloadRequest - preload request object
 *
 * private
 * @method_buildRequest - build request by current type
 * @method_buildGetRequest - build all request with get
 * @method_buildStoreStatusRequest - build store status request
 * @method_buildStoreBudgetRequest - build store budget request
 * @method_buildGetTargetCampaignRequest - build get target campaign
 * @method_buildStoreTargetCampaignRequest - build store target campaign
 * @method_buildTarget - build helper target to store request
 * @method_buildDevice - build helper device to store request
 * @method_buildBidDevice - build helper bid to store request
 * @method_parseRecord - parse response
 */
class Campaign extends BingGNG
{
    use ParseBingResponse;

    /**
     * @var campaign source id
     */
    private $campaign_id;

    /**
     * @var bing request
     */
    private $request;

    /**
     * Getter Campaign Id
     *
     * @return Campaign
     */
    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    /**
     * Setter Campaign Id
     *
     * @param $campaign_id
     * @return $this
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = is_array($campaign_id) ? $campaign_id : [$campaign_id];

        return $this;
    }

    /**
     * Getter all campaigns by account id
     *
     * @return mixed - show object from campaigns
     */
    public function getCampaigns() {
        $this->request = $this->buildRequest('GET_CAMPAIGNS');
        $records = $this->getRecords(Constants::GET_CAMPAIGN_BY_ACCOUNT_ID, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE);
    }

    /**
     * Get Campaign Status
     *
     * @return mixed
     */
    public function getStatus()
    {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_CAMPAIGN_BY_IDS, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_TYPE_STATUS);
    }

    /**
     * Get Budget amount
     *
     * @return mixed
     */
    public function getBudget()
    {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_CAMPAIGN_BY_IDS, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_BUDGET_AMOUNT);
    }

    /**
     * Get Budget Id - don't work - don't have budget id
     *
     * @return mixed
     */
    public function getBudgetId()
    {
    }

    /**
     * Get Campaign Object
     *
     * @return mixed
     */
    public function getCampaignData()
    {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_CAMPAIGN_BY_IDS, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_TYPE_ALL);
    }

    /**
     * Get Target (device bid adjustment)
     *
     * @param string $response_type
     *
     * @return mixed
     */
    public function getTarget($response_type = Constants::RESPONSE_TYPE_TARGET) {
        $this->preloadRequest('GET_TARGET');
        $records = $this->getRecords(Constants::GET_CAMPAIGN_TARGET, $this->request);
        return $this->parseRecord($records, $response_type);
    }

    /**
     * Store status to Campaign
     *
     * @param $status - Active|Paused
     * @return mixed
     */
    public function storeStatus($status)
    {
        $this->preloadRequest('STORE_STATUS', $status);
        $records = $this->getRecords(Constants::UPDATE_CAMPAIGN, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_STORE);
    }

    /**
     * Store Budget amount
     *
     * @param $amount
     * @return mixed
     */
    public function storeBudget($amount)
    {
        $this->preloadRequest('STORE_BUDGET', $amount);
        $records = $this->getRecords(Constants::UPDATE_CAMPAIGN, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_STORE);
    }

    /**
     * Store target (device bid adjustment)
     *
     * @param $bid_adjustment
     *
     * @return mixed
     */
    public function storeTarget($bid_adjustment) {
        $this->preloadRequest('STORE_TARGET', $bid_adjustment);
        $records = $this->getRecords(Constants::UPDATE_TARGET_TO_LIBRARY, $this->request);
        return $this->parseRecord($records);
    }

    /**
     * Preload request
     *
     * Check if campaign id given
     * Build Request object
     *
     * @param $request_type
     * @param null $params
     *
     * @throws Exception
     */
    protected function preloadRequest($request_type, $params = null)
    {
        if (!$this->campaign_id || empty($this->campaign_id)) {
            throw new Exception('Parameter Campaign Id not maybe empty, use method setCampaignId to set this parameter.');
        }

        $this->request = $this->buildRequest($request_type, $params);
    }

    /**
     * Build request
     *
     * @param $request_type
     * 1) Get - campaign status|campaign
     * 2) STORE_STATUS - store status to campaign
     * 3) STORE_BUDGET - store daily budget to campaign
     *
     * @param null $params
     * 1) GET - null
     * 2) STORE_STATUS - status
     * 3) STORE_BUDGET - budget amount
     *
     * @return GetCampaignsByIdsRequest
     */
    private function buildRequest($request_type, $params = null)
    {
        switch ($request_type) {
            case 'GET':
                return $this->buildGetRequest();
            case 'STORE_STATUS':
                return $this->buildStoreStatusRequest($params);
            case 'STORE_BUDGET':
                return $this->buildStoreBudgetRequest($params);
            case 'GET_TARGET':
                return $this->buildGetTargetCampaignRequest();
            case 'STORE_TARGET':
                return $this->buildStoreTargetCampaignRequest($params);
            case 'GET_CAMPAIGNS':
                return $this->buildGetCampaignByAccount();
        }
    }

    /**
     * Build Request to Bing
     *
     * @return GetCampaignsByIdsRequest
     */
    private function buildGetRequest()
    {
        $request = new GetCampaignsByIdsRequest();
        $request->AccountId = $this->getAccountId();
        $request->CampaignIds = $this->getCampaignId();

        return $request;
    }

    /**
     * Build Request to Bing
     *
     * @param $status
     * @return GetCampaignsByIdsRequest
     */
    private function buildStoreStatusRequest($status)
    {
        $request = new UpdateCampaignsRequest();
        $request->AccountId = $this->getAccountId();

        $campaigns = [];
        foreach ($this->getCampaignId() as $campaign_id) {
            $campaign = new \BingAds\CampaignManagement\Campaign();
            $campaign->Id = $campaign_id;
            $campaign->Status = $status;

            $campaigns[] = $campaign;
        }

        $request->Campaigns = $campaigns;

        return $request;
    }

    /**
     * Build Request to Bing
     *
     * @param $amount
     * @return GetCampaignsByIdsRequest
     */
    private function buildStoreBudgetRequest($amount)
    {
        $request = new UpdateCampaignsRequest();
        $request->AccountId = $this->getAccountId();

        $campaigns = [];
        foreach ($this->getCampaignId() as $campaign_id) {
            $campaign = new \BingAds\CampaignManagement\Campaign();
            $campaign->Id = $campaign_id;
            $campaign->DailyBudget = $amount;

            $campaigns[] = $campaign;
        }

        $request->Campaigns = $campaigns;

        return $request;
    }

    /**
     * Build Get Target Campaign
     *
     * @return GetTargetsByCampaignIdsRequest
     */
    private function buildGetTargetCampaignRequest() {
        $request = new GetTargetsByCampaignIdsRequest();
        $request->CampaignIds = $this->getCampaignId();

        return $request;
    }

    /**
     * Build Store target campaign request
     *
     * @param $bid_adjustment
     *
     * @return UpdateTargetsInLibraryRequest
     */
    private function buildStoreTargetCampaignRequest($bid_adjustment) {
        $request = new UpdateTargetsInLibraryRequest();
        $current_targets = $this->getTarget(Constants::RESPONSE_BID_ADJUSTMENT_ID);

        $targets = [];
        foreach ($current_targets as $current_target) {
            $targets[] = $this->buildTarget($current_target->Id, $bid_adjustment);
        }
        $request->Targets = $targets;

        return $request;
    }

    /**
     * Build All campaign request
     *
     * @return GetCampaignsByAccountIdRequest
     */
    private function buildGetCampaignByAccount() {
        $request = new GetCampaignsByAccountIdRequest();

        $request->AccountId = $this->getAccountId();

        return $request;
    }

    /**
     * Build Helper Target to store
     *
     * @param $id
     * @param $bid_adjustment
     *
     * @return Target
     */
    private function buildTarget($id, $bid_adjustment) {
        $target = new Target();
        $target->Id = $id;
        $target->DeviceOS = $this->buildDevice($bid_adjustment);

        return $target;
    }

    /**
     * Build Helper Device to store
     *
     * @param $bid_adjustment
     *
     * @return DeviceOSTarget
     */
    private function buildDevice($bid_adjustment) {
        $device = new DeviceOSTarget();
        $device->Bids = [$this->buildBidDevice($bid_adjustment)];

        return $device;
    }

    /**
     * Build Helper Bid to store
     *
     * @param $bid_adjustment
     *
     * @return DeviceOSTargetBid
     */
    private function buildBidDevice($bid_adjustment) {
        $bid = new DeviceOSTargetBid();
        $bid->BidAdjustment = $bid_adjustment;
        $bid->DeviceName = 'Smartphones';

        return $bid;
    }

    /**
     * Parse records from Bing
     *
     * @param $records - records
     * @param $type_result - STATUS|
     *
     * @return mixed - parsed result
     * @throws \Exception
     */
    private function parseRecord($records, $type_result = null)
    {
        $this->handlerException($records, Constants::ERROR_CAMPAIGN_RESPONSE);

        switch ($type_result) {
            case Constants::RESPONSE_TYPE_STATUS:
                return $this->parseStatus($records->Campaigns->Campaign);
            case Constants::RESPONSE_TYPE_TARGET:
                return $this->parseBidAdjustment($this->getCampaignId(), $records->Targets->Target);
            case Constants::RESPONSE_BID_ADJUSTMENT_ID:
                return $records->Targets->Target;
            case Constants::RESPONSE_BUDGET_AMOUNT:
                return $this->parseBudgetAmount($records->Campaigns->Campaign);

            case Constants::RESPONSE_STORE:
                return count($records->PartialErrors) == 1 ? true : false;
            default:
                return $records;
        }
    }
}
