<?php namespace App\Libraries\PublisherEditor\Operations\Bing;

use App\Libraries\PublisherEditor\Operations\Constants;
use App\Libraries\PublisherEditor\Operations\Bing\ParseBingResponse;
use App\Libraries\PublisherEditor\Providers\Bing;
use BingAds\CampaignManagement\Bid;
use BingAds\CampaignManagement\GetAdGroupsByCampaignIdRequest;
use BingAds\CampaignManagement\GetAdGroupsByIdsRequest;
use BingAds\CampaignManagement\GetTargetsByAdGroupIdsRequest;
use BingAds\CampaignManagement\UpdateAdGroupsRequest;
use Exception;

/**
 * Class AdGroup
 *
 * @package              app\Libraries\PublisherEditor\Operations\Bing
 *
 * Example:
 * @_getAdGroupStatus
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setAdGroupId('20280117144')
 *          ->getStatus();
 * return ENABLED|PAUSED
 * @_storeAdGroupStatus
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setAdGroupId('20280117144')
 *          ->setStatus(Constants::BING_STATUS_ENABLED);
 *
 * @_getCampaignBudget
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setCampaignId('283224624')
 *          ->getBudget();
 *        this campaign budget and shared budget - equivalents
 * @_storeCampaignBudget
 * EditorLib::campaign(Account::where('source_account_id', '7940838989')->first())
 *          ->setCampaignId('283224624')
 *          ->storeBudget($amount Amount);
 *
 * vars
 * @var_$campaign_id - source campaign id
 * @var_$ad_group_id - source ad group id
 * @var_$request - bing request
 *
 * public
 * @method_getCampaignId - Getter Campaign Id
 * @method_getAdGroupId  - Getter AdGroup Id
 * @method_setCampaignId - Setter Campaign Id
 * @method_setAdGroupId  - Setter AdGroup Id
 * @method_getStatus
 * @method_getBid
 * @method_getTarget
 * @method_storeStatus
 * @method_storeBid
 *
 * protected
 * @method_preloadRequest
 *
 * private
 * @method_buildRequest
 * @method_buildGetRequest
 * @method_buildTargetRequest
 * @method_buildStoreStatusRequest
 * @method_buildStoreBidRequest
 * @method_parseRecord
 *
 */
class AdGroup extends Bing
{
    use ParseBingResponse;

    /**
     * @var - Campaign id - source campaign id
     */
    private $campaign_id;

    /**
     * @var - Ad Group Id - source ad group id
     */
    private $ad_group_id;

    /**
     * @var - bing request
     */
    private $request;

    /**
     * Getter Campaign Id
     *
     * @return mixed
     */
    public function getCampaignId()
    {
        return $this->campaign_id;
    }

    /**
     * Getter AdGroup Id
     *
     * @return mixed
     */
    public function getAdGroupId()
    {
        return $this->ad_group_id;
    }

    /**
     * Setter Campaign Id
     *
     * @param $campaign_id
     *
     * @return $this
     */
    public function setCampaignId($campaign_id)
    {
        $this->campaign_id = $campaign_id;

        return $this;
    }

    /**
     * Setter AdGroup Id
     *
     * @param $ad_group_id
     *
     * @return $this
     */
    public function setAdGroupId($ad_group_id)
    {
        $this->ad_group_id = is_array($ad_group_id) ? $ad_group_id : [$ad_group_id];

        return $this;
    }

    /**
     * Get AdGroups
     *
     * @return mixed
     */
    public function getAdGroups() {
        $this->preloadRequest('AD_GROUPS');
        $records = $this->getRecords(Constants::GET_ADGROUP_BY_CAMPAIGN_ID, $this->request);
        return $this->parseRecord($records);
    }

    /**
     * Get AdGroup Status
     *
     * @return mixed
     */
    public function getStatus()
    {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_ADGROUP_BY_IDS, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_TYPE_STATUS);
    }

    /**
     * Get Bid amount
     *
     * @return mixed
     */
    public function getBid()
    {
        $this->preloadRequest('GET');
        $records = $this->getRecords(Constants::GET_ADGROUP_BY_IDS, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_TYPE_BID);
    }

    /**
     * Get AdGroup Target
     *
     * @return mixed
     */
    public function getTarget()
    {
        $this->preloadRequest('GET_TARGET');
        $records = $this->getRecords(Constants::GET_ADGROUP_ADJUSTMENT, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_BID_ADJUSTMENT);
    }

    /**
     * Store status to AdGroup
     *
     * @param $status - Active|Paused
     *
     * @return mixed
     */
    public function storeStatus($status)
    {
        $this->preloadRequest('STORE_STATUS', $status);
        $records = $this->getRecords(Constants::UPDATE_ADGROUP, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_STORE);
    }

    /**
     * Store bid to AdGroup
     *
     * @param $amount
     *
     * @return mixed
     */
    public function storeBid($amount)
    {
        $this->preloadRequest('STORE_BID', $amount);
        $records = $this->getRecords(Constants::UPDATE_ADGROUP, $this->request);
        return $this->parseRecord($records, Constants::RESPONSE_STORE);
    }

    /**
     * Preload request
     *
     * Check if campaign id given
     * Build Request object
     *
     * @param $request_type
     * @param null $params
     *
     * @throws Exception
     */
    protected function preloadRequest($request_type, $params = null)
    {
        if (!$this->campaign_id && $request_type != 'GET_TARGET') {
            throw new Exception('Parameter Campaign Id not empty, use method setCampaignId to set this parameter.');
        }

        $this->request = $this->buildRequest($request_type, $params);
    }

    /**
     * Build request
     *
     * @param $request_type
     * 1) Get - ad group status|ad group
     * 2) STORE_STATUS - store status to ad group
     *
     * @param null $params
     * 1) GET - null
     * 2) STORE_STATUS - status
     *
     * @return request
     */
    private function buildRequest($request_type, $params = null)
    {
        switch ($request_type) {
            case 'AD_GROUPS':
                return $this->buildAdGroupsRequest();
            case 'GET':
                return $this->buildGetRequest();
            case 'STORE_STATUS':
                return $this->buildStoreStatusRequest($params);
            case 'STORE_BID':
                return $this->buildStoreBidRequest($params);
            case 'GET_TARGET':
                return $this->buildTargetRequest();
        }
    }

    /**
     * Build Get Request
     *
     * @return GetAdGroupsByIdsRequest
     */
    private function buildGetRequest()
    {
        $request = new GetAdGroupsByIdsRequest();
        $request->CampaignId = $this->getCampaignId();
        $request->AdGroupIds = $this->getAdGroupId();

        return $request;
    }

    /**
     * Build AdGroups By Campaign request
     *
     * @return GetAdGroupsByCampaignIdRequest
     */
    private function buildAdGroupsRequest()
    {
        $request = new GetAdGroupsByCampaignIdRequest();
        $request->CampaignId = $this->getCampaignId();

        return $request;
    }

    /**
     * Build Target Request (device bid adjustment)
     *
     * @return GetTargetsByAdGroupIdsRequest
     */
    private function buildTargetRequest()
    {
        $request = new GetTargetsByAdGroupIdsRequest();
        $request->AdGroupIds = $this->getAdGroupId();

        return $request;
    }

    /**
     * Build Request to Bing
     *
     * @param $status
     *
     * @return UpdateAdGroupsRequest
     */
    private function buildStoreStatusRequest($status)
    {
        $request = new UpdateAdGroupsRequest();
        $request->CampaignId = $this->getCampaignId();

        $ad_groups = [];
        foreach ($this->getAdGroupId() as $ad_group) {
            $ad = new \BingAds\CampaignManagement\AdGroup();
            $ad->Id = $ad_group;
            $ad->Status = $status;

            $ad_groups[] = $ad;
        }

        $request->AdGroups = $ad_groups;

        return $request;
    }

    /**
     * Build Bid Request to Bing
     *
     * @param $amount
     *
     * @return UpdateAdGroupsRequest
     */
    private function buildStoreBidRequest($amount)
    {
        $request = new UpdateAdGroupsRequest();
        $request->CampaignId = $this->getCampaignId();
        $request->AdGroups = $this->buildAdGroup($amount);

        return $request;
    }

    /**
     * Build Bid - Request Helper
     *
     * @param $amount
     *
     * @return Bid
     */
    private function buildBid($amount)
    {
        $bid = new Bid();
        $bid->Amount = $amount;

        return $bid;
    }

    /**
     * Build AdGroup - Request Helper
     *
     * @param $amount
     *
     * @return \BingAds\CampaignManagement\AdGroup
     */
    private function buildAdGroup($amount)
    {
        $ads = [];
        foreach ($this->getAdGroupId() as $ad_group) {
            $ad = new \BingAds\CampaignManagement\AdGroup();
            $ad->Id = $ad_group;
            $ad->BroadMatchBid = $this->buildBid($amount);

            $ads[] = $ad;
        }

        return $ads;
    }

    /**
     * Parse records from Bing
     *
     * @param $records     - records
     * @param $type_result - STATUS
     *
     * @return mixed - parsed result
     * @throws \Exception
     */
    private function parseRecord($records, $type_result = null)
    {
        $this->handlerException($records, Constants::ERROR_AD_GROUP_RESPONSE);

        switch ($type_result) {
            case Constants::RESPONSE_TYPE_STATUS:
                return $this->parseStatus($records->AdGroups->AdGroup);
            case Constants::RESPONSE_BID_ADJUSTMENT:
                return $this->parseBidAdjustment($this->getCampaignId(), $records->Targets->Target);
            case Constants::RESPONSE_STORE:
                return count($records->PartialErrors) == 1 ? true : false;
            case Constants::RESPONSE_TYPE_BID:
                return $this->parseBidAmount($records->AdGroups->AdGroup);
            default:
                return $records;
        }
    }
}