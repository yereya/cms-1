<?php namespace App\Libraries\PublisherEditor\Operations\Google;

use App\Libraries\PublisherEditor\Operations\Constants;

trait ParseGoogleResponse
{
    /**
     * Parse status
     *
     * @param $records
     *
     * @return array
     */
    protected function parseStatus($records) {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                $results[$record->id] = $record->status == Constants::STATUS_ENABLED ? 1 : 0;
            }
        }

        return $results;
    }

    /**
     * Parse budget
     *
     * @param $records
     *
     * @return array
     */
    protected function parseBudget($records) {
        $results = [];

        foreach ($records as $record) {
            if (!is_null($record)) {
                $results[$record->id] = $record->budget->amount->microAmount / 1000000;
            }
        }

        return $results;
    }

    protected function parseStoreBudget($records) {
        $results = [];

        foreach ($records as $record) {
            if (!is_null($record)) {
                $results[$record->budgetId] = $record->amount->microAmount / 1000000;
            }
        }

        return $results;
    }

    protected function parseStoreAdjustment($records) {
        $results = [];

        foreach ($records as $record) {
            if (!is_null($record)) {
                $results[$record->campaignId] = (1 - round($record->bidModifier, 1)) * 100;
            }
        }

        return $results;
    }

    protected function parseBudgetId($records) {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                $results[] = $record->budget;
            }
        }

        return $results;
    }

    /**
     * Parse Bid
     *
     * @param $records
     *
     * @return array
     */
    protected function parseBid($records) {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                $bids = $record->biddingStrategyConfiguration->bids;
                foreach ($bids as $bid) {
                    $results[$record->id] = $bid->bid->microAmount / 1000000;
                }
            }
        }

        return $results;
    }

    /**
     * Parse device bid adjustment ad group
     *
     * @param $records
     *
     * @return array
     */
    protected function parseAdGroupBidModifier($records) {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                if ($record->criterion->platformName == Constants::DEVICE_HIGH_END_MOBILE && !is_null($record->bidModifier)) {
                    $percent = (1 - round($record->bidModifier, 1)) * 100;
                    $results[$record->adGroupId] = (int)$percent;
                }
            }
        }

        return $results;
    }

    /**
     * Parse device bid adjustment campaign
     *
     * @param $records
     *
     * @return array
     */
    protected function parseCampaignBidModifier($records) {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                if ($record->criterion->platformName == Constants::DEVICE_HIGH_END_MOBILE && !is_null($record->bidModifier)) {
                    $percent = (1 - round($record->bidModifier, 1)) * 100;
                    $results[$record->campaignId] = (int)$percent;
                }
            }
        }

        return $results;
    }

    protected function parseCampaignBidModifierId($records) {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record)) {
                if ($record->criterion->platformName == Constants::DEVICE_HIGH_END_MOBILE && !is_null($record->bidModifier)) {
                    $results[] = $record;
                }
            }
        }

        return $results;
    }

    protected function parseKeywordStatus($records) {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record) && isset($record->criterion->id)) {
                $results[$record->criterion->id] = ($record->userStatus == Constants::STATUS_ENABLED) ? 1 : 0;
            }
        }

        return $results;
    }

    protected function parseKeywordBid($records) {
        $results = [];
        foreach ($records as $record) {
            if (!is_null($record) && isset($record->biddingStrategyConfiguration->bids->bid)) {
                    $results[$record->criterion->id] = $record->biddingStrategyConfiguration->bids->bid->microAmount / 1000000;
            }
        }

        return $results;
    }
}