<?php namespace App\Libraries\PublisherEditor\Operations\Google;

use AdGroupCriterionOperation;
use App\Libraries\PublisherEditor\Operations\Constants;
use App\Libraries\PublisherEditor\Providers\Google;
use BiddableAdGroup;
use BiddableAdGroupCriterion;
use Predicate;
use Selector;

class Keyword extends Google
{
    use ParseGoogleResponse;
    /**
     * @var - ad group source id
     */
    private $ad_group_id;

    /**
     * @var - keyword id
     */
    private $keyword_id;

    /**
     * @var - ad group service
     */
    private $service;

    /**
     * Getter AdGroupId
     *
     * @return mixed
     */
    public function getAdGroupId()
    {
        return $this->ad_group_id;
    }

    /**
     * Setter AdGroupId
     *
     * @param $ad_group_id - source ad_group id
     *
     * @return $this
     */
    public function setAdGroupId($ad_group_id)
    {
        $this->ad_group_id = $ad_group_id;

        return $this;
    }

    /**
     * Getter keyword id
     *
     * @return mixed
     */
    public function getKeywordId()
    {
        return $this->keyword_id;
    }

    /**
     * Setter keyword id
     *
     * @param $keyword_id
     *
     * @return $this
     */
    public function setKeywordId($keyword_id)
    {
        $this->keyword_id = is_array($keyword_id) ? $keyword_id : [$keyword_id];

        return $this;
    }

    /**
     * Get all Keywords
     */
    public function getKeywordsByAdGroup()
    {
        $this->preloadShowService();
        $response = $this->executeQuery($this->buildShowSelector());
        return $this->parseResponse($response);
    }

    /**
     * Return status
     *
     * @return mixed|UNKNOWN|ENABLED|PAUSED|REMOVED
     */
    public function getStatus()
    {
        $this->preloadService();
        $response = $this->executeQuery($this->buildSelector());
        return $this->parseResponse($response, Constants::RESPONSE_TYPE_STATUS);
    }

    /**
     * Store status
     *
     * @param $status
     *
     * @return mixed
     */
    public function storeStatus($status)
    {
        if (empty($status)) {
            throw new Exception('This parameters status is required, please give this parameter in $status');
        }

        $this->preloadService();
        $operation = $this->createStatusOperation($this->createBiddable($status));
        $response = $this->mutate($this->service, $operation);
        return $this->parseStoreResponse($response, Constants::RESPONSE_TYPE_STATUS);
    }

    /**
     * @param string $response_type
     *
     * @return mixed
     */
    public function getBid($response_type = Constants::RESPONSE_TYPE_BID) {
        $this->preloadService();
        $response = $this->executeQuery($this->buildSelector());
        return $this->parseResponse($response, $response_type);
    }

    public function storeBid($bid) {
        $this->preloadService();
        $bids = $this->getBid(Constants::RESPONSE_TYPE_TARGET);
    }

    /**
     * Build Selector
     *
     * @return Selector
     */
    private function buildSelector()
    {
        $selector = new Selector();
        $selector->fields = array('Id', 'CriteriaType', 'KeywordMatchType', 'KeywordText', 'Status', 'BiddingStrategyId', 'CpcBid');
        $selector->predicates[] = new Predicate('AdGroupId', 'EQUALS', $this->ad_group_id);
        $selector->predicates[] = new Predicate('CriteriaType', 'EQUALS', 'KEYWORD');
        $selector->predicates[] = new Predicate('Id', 'EQUALS', $this->keyword_id);

        return $selector;
    }

    /**
     * Build Selector
     *
     * @return Selector
     */
    private function buildShowSelector()
    {
        $selector = new Selector();
        $selector->fields = array('Id', 'CriteriaType', 'KeywordMatchType', 'KeywordText', 'Status');
        $selector->predicates[] = new Predicate('AdGroupId', 'EQUALS', $this->ad_group_id);
        $selector->predicates[] = new Predicate('CriteriaType', 'EQUALS', 'KEYWORD');

        return $selector;
    }

    /**
     * Create BiddableAdGroupCriterion
     *
     * @param $status
     *
     * @return BiddableAdGroupCriterion
     */
    private function createBiddable($status)
    {
        $biddable = new BiddableAdGroupCriterion();
        $biddable->adGroupId = $this->ad_group_id;
        $biddable->userStatus = $status;
        $biddable->criterion = $this->buildCriterion();

        return $biddable;
    }

    /**
     * Build Helper Criterion to BiddableAdGroupCriterion
     *
     * @return \Criterion
     */
    private function buildCriterion()
    {
        $criterion = new \Criterion();
        $criterion->CriterionType = 'KEYWORD';
        $criterion->id = $this->keyword_id;

        return $criterion;
    }

    /**
     * Create Status Operations
     *
     * @param $biddable
     *
     * @return AdGroupCriterionOperation
     */
    private function createStatusOperation($biddable)
    {
        $operator = new AdGroupCriterionOperation();
        $operator->operand = $biddable;
        $operator->operator = 'SET';

        return $operator;
    }

    /**
     * Preload service
     * Load customer id (google name, account id in local name)
     *
     * @throws Exception - account id null
     * @throws Exception - campaign id null
     */
    protected function preloadService()
    {
        if (!$this->ad_group_id) {
            throw new Exception('Parameter AdGroup Id not maybe empty, use method setAdGroupId to set this parameter.');
        }

        if (!$this->keyword_id) {
            throw new \Exception('Parameter Keyword Id not maybe empty, use method setKeywordId to set this parameter.');
        }

        if (!$this->service) {
            $this->setCustomerId($this->account_id);
            $this->service = $this->user->GetService(Constants::ADWORDS_ADGROUP_CRITERION, Constants::ADWORDS_VERSION);
        }
    }

    /**
     * Preload service
     * Load customer id (google name, account id in local name)
     *
     * @throws Exception - account id null
     * @throws Exception - campaign id null
     */
    protected function preloadShowService()
    {
        if (!$this->ad_group_id) {
            throw new Exception('Parameter AdGroup Id not maybe empty, use method setAdGroupId to set this parameter.');
        }

        if (!$this->service) {
            $this->setCustomerId($this->account_id);
            $this->service = $this->user->GetService(Constants::ADWORDS_ADGROUP_CRITERION, Constants::ADWORDS_VERSION);
        }
    }

    /**
     * Execute query
     *
     * @param $selector
     *
     * @throws \Exception - not found keyword
     */
    private function executeQuery($selector)
    {
        return $this->service->get($selector);
    }

    /**
     * Parse response
     *
     * @param $records
     * @param $type
     *
     * @return mixed
     * @throws \Exception
     */
    private function parseResponse($records, $type)
    {
        if ($records->totalNumEntries == 0) {
            return null;
        }

        switch ($type) {
            case Constants::RESPONSE_TYPE_STATUS:
                return $this->parseKeywordStatus($records->entries);
            case Constants::RESPONSE_TYPE_BID:
                return $this->parseKeywordBid($records->entries);
            case Constants::RESPONSE_TYPE_TARGET:
                return $records;
            default:
                return $records;
        }
    }

    private function parseStoreResponse($records, $type)
    {
        if (!is_null($records->partialFailureErrors)) {
            return false;
        }

        switch ($type) {
            case Constants::RESPONSE_TYPE_STATUS:
                return $this->parseKeywordStatus($records->value);
            case Constants::RESPONSE_TYPE_BID:
                dd($records);
            default:
                return $records;
        }
    }
}