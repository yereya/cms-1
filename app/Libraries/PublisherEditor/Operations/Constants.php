<?php namespace App\Libraries\PublisherEditor\Operations;

class Constants
{
    /**
     * Google Constants
     */
    const ADWORDS_VERSION = 'v201609';
    const ADWORDS_CAMPAIGN_SERVICE = 'CampaignService';
    const ADWORDS_CAMPAIGN_CRITERION_SERVICE = 'CampaignCriterionService';
    const ADWORDS_CAMPAIGN_BUDGET_SERVICE = 'BudgetService';
    const ADWORDS_ADGROUP_SERVICE = 'AdGroupService';
    const ADWORDS_ADGROUP_BID = 'BiddableAdGroupCriterion';
    const ADWORDS_ADGROUP_EXTENSION = 'AdGroupExtensionSettingService';
    const ADWORDS_ADGROUP_CRITERION = 'AdGroupCriterionService';
    const ADWORDS_ADGROUP_BIDMODIFIER = 'AdGroupBidModifierService';

    /**
     * Status google constants
     */
    const STATUS_ENABLED = 'ENABLED';
    const STATUS_DISABLED = 'PAUSED';

    /**
     * Bing Constants
     * V9
     * const BING_URL_CAMPAIGN_MANAGER = 'https://campaign.api.bingads.microsoft.com/Api/Advertiser/CampaignManagement/V10/CampaignManagementService.svc?singleWsdl';
     */

     /** V10 */
     const BING_URL_CAMPAIGN_MANAGER = 'https://campaign.api.bingads.microsoft.com/Api/Advertiser/CampaignManagement/V10/CampaignManagementService.svc?wsdl';

    /**
     * Bing Requests
     */
    const GET_CAMPAIGN_BY_ACCOUNT_ID = 'GetCampaignsByAccountId';
    const GET_CAMPAIGN_BY_IDS = 'GetCampaignsByIds';
    const UPDATE_CAMPAIGN = 'UpdateCampaigns';
    const GET_CAMPAIGN_TARGET = 'GetTargetsByCampaignIds';
    const UPDATE_TARGET_TO_LIBRARY = 'UpdateTargetsInLibrary';
    const GET_ADGROUP_BY_IDS = 'GetAdGroupsByIds';
    const GET_ADGROUP_BY_CAMPAIGN_ID = 'GetAdGroupsByCampaignId';
    const UPDATE_ADGROUP = 'UpdateAdGroups';
    const GET_ADGROUP_ADJUSTMENT = 'GetTargetsByAdGroupIds';
    const GET_KEYWORDS_BY_ADGROUP_ID = 'GetKeywordsByAdGroupId';
    const GET_KEYWORDS_BY_IDS = 'GetKeywordsByIds';
    const UPDATE_KEYWORD = 'UpdateKeywords';

    /**
     * Status Bing constants
     */
    const BING_STATUS_ENABLED = 'Active';
    const BING_STATUS_DISABLED = 'Paused';

    /**
     * Common const
     */
    const RESPONSE_TYPE_STATUS = 'status';
    const RESPONSE_TYPE_ALL = 'all';
    const RESPONSE_BUDGET = 'amount';
    const RESPONSE_BUDGET_AMOUNT = 'budget';
    const RESPONSE_BUDGET_ID = 'budget_id';
    const RESPONSE_STORE = 'store';
    const RESPONSE_TYPE_BID = 'bid';
    const RESPONSE_BID_ADJUSTMENT = 'bid_adjustment';
    const RESPONSE_BID_ADJUSTMENT_ID = 'bid_adjustment_id';
    const RESPONSE_TYPE_TARGET = 'target';
    const RESPONSE = 'response';
    const RESPONSE_TYPE_ADGROUP = 'adgroup';

    const DEVICE_COMPUTERS = 'Computers';
    const DEVICE_SMARTPHONES = 'Smartphones';
    const DEVICE_TABLETS = 'Tablets';
    const DEVICE_HIGH_END_MOBILE = 'HighEndMobile';

    /**
     * Bing errors
     */
    const ERROR_CAMPAIGN_RESPONSE = 'Bing return error Campaigns, check parameters. Error : ';
    const ERROR_AD_GROUP_RESPONSE = 'Bing return error AdGroup object, check parameters. Error : ';

    /**
     * Actions
     */
    const CHANGE_TYPE_STATUS = 'status';
    const CHANGE_TYPE_BID = 'bid';
    const CHANGE_TYPE_BUDGET = 'budget';
    const CHANGE_TYPE_ADJUSTMENT = 'bid_adjustment';
    const CHANGE_LEVEL_CAMPAIGN = 'campaign';
    const CHANGE_LEVEL_AD_GROUP = 'ad_group';
    const CHANGE_LEVEL_KEYWORD = 'keyword';

    /**
     * Value types and value operations
     */
    const VALUE_OPERATOR_INCREASE = 'increase';
    const VALUE_OPERATOR_DECREASE = 'decrease';
    const VALUE_OPERATOR_SET_TO = 'set_to';
    const VALUE_TYPE_USD = '$';
    const VALUE_TYPE_PERCENT = '%';
}