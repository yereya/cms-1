<?php namespace App\Libraries\PublisherEditor;

use App\Entities\Models\Bo\Account;
use App\Libraries\PublisherEditor\Operations\Constants;
use Exception;
use Illuminate\Support\Collection;
use Validator;

class PublisherEditorLib
{
    /**
     * @var
     */
    private static $fails;

    /**
     * @var
     */
    private static $collection;

    /** GLOBAL SCOPE */

    /**
     * Start Operation
     *
     * @param Collection $collection
     * @param $change_attributes
     *
     * @return array|void
     */
    public static function operation(Collection $collection, $change_attributes)
    {
        if (!self::validateChangeAttributes($change_attributes)) {
            return;
        }

        if ($collection->count() == 0) {
            self::setFails(['error' => 'This collection is null']);
            return;
        }

        self::setCollection($collection);
        $collection = self::addToCollectionNewValue(
            $collection,
            $change_attributes
        );

        self::fetch($collection, $change_attributes);
    }

    /**
     * Getter collections
     *
     * @return mixed
     */
    public static function getCollection()
    {
        return self::$collection;
    }

    /**
     * Getter fails
     *
     * @return mixed
     */
    public static function getFails()
    {
        return self::$fails;
    }

    /**
     * Fetch all methods
     *
     * @param Collection $collection
     * @param $change_attributes
     */
    private static function fetch(Collection $collection, $change_attributes) {
        $results = new Collection();
        switch ($change_attributes['level']) {
            case Constants::CHANGE_LEVEL_CAMPAIGN:
                $results->push(self::operationCampaign($collection, $change_attributes['type']));
                break;
            case Constants::CHANGE_LEVEL_AD_GROUP:
                $results->push(self::operationAdGroup($collection, $change_attributes['type']));
                break;
            case Constants::CHANGE_LEVEL_KEYWORD:
                $results->push(self::operationKeyword($collection, $change_attributes['type']));
                break;
            default:
                self::setFails(['error' => 'Unknown value CHANGE LEVEL: ' . $change_attributes['level']]);
                break;
        }

        self::setCollection($results);
    }

    /**
     * Set column name and run init
     *
     * @param $collection
     * @param $params
     *
     * @return mixed
     */
    private static function addToCollectionNewValue($collection, $params)
    {
        $column_name = null;

        switch ($params['level']) {
            case Constants::CHANGE_LEVEL_CAMPAIGN:
                if ($params['type'] == Constants::CHANGE_TYPE_BUDGET) {
                    $column_name = 'campaign_budget';
                } else if ($params['type'] == Constants::CHANGE_TYPE_ADJUSTMENT) {
                    $column_name = 'campaign_device_bid_adjustment';
                }
                break;
            case Constants::CHANGE_LEVEL_AD_GROUP:
                if ($params['type'] == Constants::CHANGE_TYPE_BID) {
                    $column_name = 'ad_group_bid';
                } else if ($params['type'] == Constants::CHANGE_TYPE_ADJUSTMENT) {
                    $column_name = 'ad_group_device_bid_adjustment';
                }
                break;
            case Constants::CHANGE_LEVEL_KEYWORD:
                if ($params['type'] == Constants::CHANGE_TYPE_BID) {
                    $column_name = 'keyword_bid';
                }
                break;
        }

        return self::initNewValue(
            $collection,
            $column_name,
            $params['new_value'],
            isset($params['value_format']) ? $params['value_format'] : null,
            isset($params['value_operator']) ? $params['value_operator'] : null
        );
    }

    /**
     * Init add new value to collection
     *
     * @param $collection
     * @param $column_name
     * @param $value
     * @param $format
     * @param $operator
     *
     * @return mixed
     * @throws Exception
     */
    private static function initNewValue($collection, $column_name, $value, $format, $operator)
    {
        if (is_null($column_name) || is_null($format) || ($format == Constants::VALUE_TYPE_USD && is_null($operator))
            || ($format == Constants::VALUE_TYPE_USD && $operator == Constants::VALUE_OPERATOR_SET_TO)
        ) {
            return self::newValueCollection($collection, $value);
        } else if (!is_null($format) && $operator == Constants::VALUE_OPERATOR_DECREASE) {
            return self::newValueDecreaseCollection($collection, $column_name, $value, $format);
        } else if (!is_null($format) && $operator == Constants::VALUE_OPERATOR_INCREASE) {
            return self::newValueIncreaseCollection($collection, $column_name, $value, $format);
        } else {
            throw new Exception(printf('Error one or more parameters is incorrectly - value type %s or value operator %s', [$format, $operator]));
        }
    }

    /**
     * Add new attribute to collection with name new_value
     *
     * @param $collection
     * @param $value
     *
     * @return mixed
     */
    private static function newValueCollection($collection, $value)
    {
        return $collection->each(function ($item) use ($value) {
            $item->new_value = $value;
        });
    }

    /**
     * Decrease value in collections
     *
     * @param $collection
     * @param $column_name
     * @param $value
     * @param $format
     *
     * @return mixed
     */
    private static function newValueDecreaseCollection($collection, $column_name, $value, $format)
    {
        if ($format == Constants::VALUE_TYPE_USD) {
            return $collection->each(function ($item) use ($column_name, $value) {
                $item->new_value = $item->{$column_name} - $value;
            });
        } else if ($format == Constants::VALUE_TYPE_PERCENT) {
            return $collection->each(function ($item) use ($column_name, $value) {
                $item->new_value = $item->{$column_name} - ($item->{$column_name} / 100) * $value;
            });
        }
    }

    /**
     * Increase value in collections
     *
     * @param $collection
     * @param $column_name
     * @param $value
     * @param $format
     *
     * @return mixed
     */
    private static function newValueIncreaseCollection($collection, $column_name, $value, $format)
    {
        if ($format == Constants::VALUE_TYPE_USD) {
            return $collection->each(function ($item) use ($column_name, $value) {
                $item->new_value = $item->{$column_name} + $value;
            });
        } else if ($format == Constants::VALUE_TYPE_PERCENT) {
            return $collection->each(function ($item) use ($column_name, $value) {
                $item->new_value = $item->{$column_name} + ($item->{$column_name} / 100) * $value;
            });
        }
    }

    /**
     * Setter collection
     *
     * @param $collection
     */
    private static function setCollection($collection)
    {
        self::$collection = $collection;
    }

    /**
     * Setter fails
     *
     * @param $fails
     */
    private static function setFails($fails)
    {
        self::$fails = $fails;
    }

    /**
     * Operations Campaign
     *
     * @param Collection $collection
     * @param $change_type
     *
     * @return array
     * @throws Exception
     */
    private static function operationCampaign(Collection $collection, $change_type)
    {
        self::setFails(null);
        switch ($change_type) {
            case Constants::CHANGE_TYPE_STATUS:
                return self::campaignStatus($collection);
            case Constants::CHANGE_TYPE_BUDGET:
                return self::campaignBudget($collection);
            case Constants::CHANGE_TYPE_ADJUSTMENT:
                return self::campaignBidAdjustment($collection);
            default:
                self::setFails(['error' => 'Unknown value CHANGE TYPE: ' . $change_type]);
                break;
        }
    }

    /**
     * Operation Ad Group
     *
     * @param Collection $collection
     * @param $change_type
     *
     * @return array|void
     * @throws Exception
     */
    private static function operationAdGroup(Collection $collection, $change_type)
    {
        self::setFails(null);
        switch ($change_type) {
            case Constants::CHANGE_TYPE_STATUS:
                return self::adGroupStatus($collection);
            case Constants::CHANGE_TYPE_BID:
                return self::adGroupBid($collection);
            case Constants::CHANGE_TYPE_ADJUSTMENT:
                return self::adGroupAdjustment($collection);
            default:
                self::setFails(['error' => 'Unknown value CHANGE TYPE: ' . $change_type]);
                break;
        }
    }

    /**
     * Operation Keyword
     *
     * @param Collection $collection
     * @param $change_type
     *
     * @return array|void
     * @throws Exception
     */
    private static function operationKeyword(Collection $collection, $change_type)
    {
        self::setFails(null);
        switch ($change_type) {
            case Constants::CHANGE_TYPE_STATUS:
                return self::keywordStatus($collection);
            case Constants::CHANGE_TYPE_BID:
                return self::keywordBid($collection);
            default:
                self::setFails(['error' => 'Unknown value CHANGE TYPE: ' . $change_type]);
                break;
        }
    }

    /** CAMPAIGN OPERATIONS BLOCK */

    /**
     * Campaign status
     *
     * @param Collection $collection
     *
     * @return array
     */
    private static function campaignStatus(Collection $collection)
    {
        $group_by_account = $collection->groupBy('account_id');

        $store = [];
        foreach ($group_by_account as $account_id => $items) {
            $account_model = Account::where('source_account_id', $account_id)->first();
            $campaigns = $items->groupBy('campaign_id');

            $store[$account_id] = self::updateCampaignStatus($account_model, $campaigns);
        }

        return $store;
    }

    /**
     * Campaign Budget
     *
     * @param Collection $collection
     * @param $new_value
     *
     * @return array
     */
    private static function campaignBudget(Collection $collection)
    {
        $group_by_account = $collection->groupBy('account_id');

        $store = [];
        foreach ($group_by_account as $account_id => $items) {
            $account_model = Account::where('source_account_id', $account_id)->first();
            $campaigns = $items->groupBy('campaign_id');
            /** get current budget */
            $current = PublisherEditorFactory::campaign($account_model)->setCampaignId($campaigns->keys()->toArray())->getBudget();

            /** find different */
            $fail = self::diffBetweenValues($current, $campaigns, 'campaign_budget');

            if (!($fail instanceof Collection) || $fail->count() > 0) {
                self::setFails(['error' => 'budget local not match to provider budget', 'fail' => $fail]);
                return;
            }
            /** if find different null store */
            $store[$account_id] = self::updateCampaignBudget($account_model, $campaigns->keys()->toArray(), $campaigns);
        }

        return $store;
    }

    /**
     * Campaign Bid Adjustment
     *
     * @param Collection $collection
     *
     * @return array
     */
    private static function campaignBidAdjustment(Collection $collection)
    {
        $group_by_account = $collection->groupBy('account_id');

        $store = [];
        foreach ($group_by_account as $account_id => $items) {
            $account_model = Account::where('source_account_id', $account_id)->first();
            $campaigns = $items->groupBy('campaign_id');
            /** get current budget */
            $current = PublisherEditorFactory::campaign($account_model)->setCampaignId($campaigns->keys()->toArray())->getTarget();

            /** find different */
            $fail = self::diffBetweenValues($current, $campaigns, 'campaign_device_bid_adjustment');

            if (!($fail instanceof Collection) || $fail->count() > 0) {
                self::setFails(['error' => 'budget local not match to provider budget', 'fail' => $fail]);
                return;
            }
            /** if find different null store */
            $store[$account_id] = self::updateCampaignBidAdjustment($account_model, $campaigns);
        }

        return $store;
    }

    /**
     * Update campaign device bid adjustment
     *
     * @param $account_model
     * @param $campaigns
     *
     * @return array
     */
    private static function updateCampaignBidAdjustment($account_model, $campaigns)
    {
        $store = [];
        foreach ($campaigns as $campaign_id => $models) {
            foreach ($models as $model) {
                $bid_adjustment = $model->new_value;
                if ($account_model->publisher_id == 95) {
                    $bid_adjustment = 1 - $model->new_value / 100;
                }

                $store[$campaign_id] = PublisherEditorFactory::campaign($account_model)->setCampaignId($campaign_id)->storeTarget($bid_adjustment);
            }
        }

        return $store;
    }

    /**
     * Update campaign budget
     *
     * @param $account_model
     * @param $campaigns
     *
     * @return array
     */
    private static function updateCampaignBudget($account_model, $campaigns)
    {
        $store = [];
        foreach ($campaigns as $campaign_id => $models) {
            foreach ($models as $model) {
                $budget = $model->new_value;

                if ($account_model->publisher_id == 95) {
                    $budget = $model->new_value * 1000000;
                }

                $store[$campaign_id] = PublisherEditorFactory::campaign($account_model)->setCampaignId($campaign_id)->storeBudget($budget);
            }
        }

        return $store;
    }

    /**
     * Update campaign status
     *
     * @param $account_model
     * @param $campaigns
     *
     * @return array
     */
    private static function updateCampaignStatus($account_model, $campaigns)
    {
        $store = [];
        foreach ($campaigns as $campaign_id => $models) {
            foreach ($models as $model) {
                if ($account_model->publisher_id == 95) {
                    $status = $model->new_value ? Constants::STATUS_ENABLED : Constants::STATUS_DISABLED;
                }

                if ($account_model->publisher_id == 97) {
                    $status = $model->new_value ? Constants::BING_STATUS_ENABLED : Constants::BING_STATUS_DISABLED;
                }

                $store[$campaign_id] = PublisherEditorFactory::campaign($account_model)->setCampaignId($campaign_id)->storeStatus($status);
            }
        }

        return $store;
    }

    /** AD GROUP OPERATIONS SCOPE */

    /**
     * Init ad group status
     *
     * @param $collection
     *
     * @return array
     */
    private static function adGroupStatus($collection)
    {
        $group_by_account = $collection->groupBy('account_id');

        $store = [];
        foreach ($group_by_account as $account_id => $items) {
            $account_model = Account::where('source_account_id', $account_id)->first();
            $campaigns = $items->groupBy('campaign_id');
            foreach ($campaigns as $campaign_id => $campaign) {
                $ad_groups = $campaign->groupBy('ad_group_id');

                $store[$account_id][$campaign_id] = self::updateAdGroupStatus($account_model, $campaign_id, $ad_groups);
            }
        }

        return $store;
    }

    /**
     * Init Ad Group Bid
     *
     * @param $collection
     *
     * @return array
     */
    private static function adGroupBid($collection)
    {
        $group_by_account = $collection->groupBy('account_id');

        $store = [];
        foreach ($group_by_account as $account_id => $items) {
            $account_model = Account::where('source_account_id', $account_id)->first();
            $campaigns = $items->groupBy('campaign_id');
            foreach ($campaigns as $campaign_id => $campaign) {
                $ad_groups = $campaign->groupBy('ad_group_id');

                $current = PublisherEditorFactory::adGroup($account_model)->setCampaignId($campaign_id)->setAdGroupId($ad_groups->keys()->toArray())->getBid();
                /** find different */
                $fail = self::diffBetweenValues($current, $ad_groups, 'ad_group_bid');

                if (!($fail instanceof Collection) || $fail->count() > 0) {
                    self::setFails(['error' => 'bid local not match to provider bid', 'fail' => $fail]);
                    return;
                }

                $store[$account_id][$campaign_id] = self::updateAdGroupBid($account_model, $campaign_id, $ad_groups);
            }
        }

        return $store;
    }

    /**
     * Init ad group bid device adjustment
     *
     * @param $collection
     *
     * @return array
     */
    private static function adGroupAdjustment($collection)
    {
        $group_by_account = $collection->groupBy('account_id');

        $store = [];
        foreach ($group_by_account as $account_id => $items) {
            $account_model = Account::where('source_account_id', $account_id)->first();
            $campaigns = $items->groupBy('campaign_id');
            foreach ($campaigns as $campaign_id => $campaign) {
                $ad_groups = $campaign->groupBy('ad_group_id');

                $current = PublisherEditorFactory::adGroup($account_model)->setCampaignId($campaign_id)->setAdGroupId($ad_groups->keys()->toArray())->getTarget();
                /** find different */
                $fail = self::diffBetweenValues($current, $ad_groups, 'ad_group_device_bid_adjustment');
                if (!($fail instanceof Collection) || $fail->count() > 0) {
                    self::setFails(['error' => 'bid adjustment local not match to provider bid adjustment', 'fail' => $fail]);
                    return;
                }

                $store[$account_id][$campaign_id] = self::updateAdGroupAdjustment($account_model, $campaign_id, $ad_groups);
            }
        }

        return $store;
    }

    /**
     * @param $account_model
     * @param $campaign
     * @param $ad_groups
     *
     * @return array
     */
    private static function updateAdGroupStatus($account_model, $campaign, $ad_groups)
    {
        $store = [];
        foreach ($ad_groups as $ad_group_id => $models) {
            foreach ($models as $model) {
                if ($account_model->publisher_id == 95) {
                    $status = $model->new_value ? Constants::STATUS_ENABLED : Constants::STATUS_DISABLED;
                }

                if ($account_model->publisher_id == 97) {
                    $status = $model->new_value ? Constants::BING_STATUS_ENABLED : Constants::BING_STATUS_DISABLED;
                }

                $store[$ad_group_id] = PublisherEditorFactory::adGroup($account_model)->setCampaignId($campaign)->setAdGroupId($ad_group_id)->storeStatus($status);
            }
        }

        return $store;
    }

    /**
     * Update ad group bid
     *
     * @param $account_model
     * @param $campaign_id
     * @param $ad_groups
     *
     * @return array
     */
    private static function updateAdGroupBid($account_model, $campaign_id, $ad_groups)
    {
        $store = [];
        foreach ($ad_groups as $ad_group_id => $models) {
            foreach ($models as $model) {
                $bid = $model->new_value;

                if ($account_model->publisher_id == 95) {
                    $bid = $model->new_value * 1000000;
                }

                $store[$ad_group_id] = PublisherEditorFactory::adGroup($account_model)->setCampaignId($campaign_id)->setAdGroupId($ad_group_id)->storeBid($bid);
            }
        }

        return $store;
    }

    /**
     * Update ad group device bid adjustment
     *
     * @param $account_model
     * @param $campaign_id
     * @param $ad_groups
     *
     * @return array
     */
    private static function updateAdGroupAdjustment($account_model, $campaign_id, $ad_groups)
    {
        $store = [];
        foreach ($ad_groups as $ad_group_id => $models) {
            foreach ($models as $model) {
                $adjustment = $model->new_value;

                if ($account_model->publisher_id == 95) {
                    $adjustment = 1 - $model->new_value / 100;
                }

                $store[$ad_group_id] = PublisherEditorFactory::adGroup($account_model)->setCampaignId($campaign_id)->setAdGroupId($ad_group_id)->storeTarget($adjustment);
            }
        }

        return $store;
    }

    /**** KEYWORD BLOCK **/

    /**
     * Init all keyword status actions
     *
     * @param $collection
     *
     * @return array
     */
    private static function keywordStatus($collection)
    {
        $group_by_account = $collection->groupBy('account_id');

        $store = [];
        foreach ($group_by_account as $account_id => $items) {
            $account_model = Account::where('source_account_id', $account_id)->first();
            $ad_groups = $items->groupBy('ad_group_id');
            foreach ($ad_groups as $ad_group_id => $ad_group) {
                $keywords = $ad_group->groupBy('keyword_id');

                $store[$account_id][$ad_group_id] = self::updateKeywordStatus($account_model, $ad_group_id, $keywords);
            }
        }

        return $store;
    }

    /**
     * Init all keyword bid actions
     *
     * @param $collection
     *
     * @return array|void
     */
    private static function keywordBid($collection)
    {
        $group_by_account = $collection->groupBy('account_id');

        $store = [];
        foreach ($group_by_account as $account_id => $items) {
            $account_model = Account::where('source_account_id', $account_id)->first();
            $ad_groups = $items->groupBy('ad_group_id');
            foreach ($ad_groups as $ad_group_id => $ad_group) {
                $keywords = $ad_group->groupBy('keyword_id');

                $current = PublisherEditorFactory::keyword($account_model)->setAdGroupId($ad_group_id)->setKeywordId($keywords->keys()->toArray())->getStatus();
                /** find different */
                $fail = self::diffBetweenValues($current, $ad_groups, 'keyword_bid');

                if (!($fail instanceof Collection) || $fail->count() > 0) {
                    self::setFails(['error' => 'keyword bid local not match to provider bid', 'fail' => $fail]);
                    return;
                }

                $store[$account_id][$ad_group_id] = self::updateKeywordBid($account_model, $ad_group_id, $keywords);
            }
        }

        return $store;
    }

    /**
     * Update keyword status
     *
     * @param $account_model
     * @param $ad_group_id
     * @param $keywords
     *
     * @return array
     */
    private static function updateKeywordStatus($account_model, $ad_group_id, $keywords)
    {
        $store = [];
        foreach ($keywords as $keyword_id => $models) {
            foreach ($models as $model) {
                if ($account_model->publisher_id == 95) {
                    $status = $model->new_value ? Constants::STATUS_ENABLED : Constants::STATUS_DISABLED;
                }

                if ($account_model->publisher_id == 97) {
                    $status = $model->new_value ? Constants::BING_STATUS_ENABLED : Constants::BING_STATUS_DISABLED;
                }

                $store[$keyword_id] = PublisherEditorFactory::keyword($account_model)->setAdGroupId($ad_group_id)->setKeywordId($keyword_id)->storeStatus($status);
            }
        }

        return $store;
    }

    /**
     * Update keyword bid
     *
     * @param $account_model
     * @param $ad_group_id
     * @param $keywords
     *
     * @return array
     */
    private static function updateKeywordBid($account_model, $ad_group_id, $keywords)
    {
        $store = [];
        foreach ($keywords as $keyword_id => $models) {
            foreach ($models as $model) {
                $bid = $model->new_value;
                if ($account_model->publisher_id == 95) {
                    $bid = $model->new_value * 1000000;
                }

                $store[$keyword_id] = PublisherEditorFactory::adGroup($account_model)->setAdGroupId($ad_group_id)->setKeywordId($keyword_id)->storeBid($bid);
            }
        }

        return $store;
    }

    /**
     * Found difference between provider value and local value
     *
     * @param $current_provider
     * @param $current_local
     * @param $value_name
     *
     * @return mixed
     */
    private static function diffBetweenValues($current_provider, $current_local, $value_name)
    {
        $results = new Collection();

        if (empty($current_provider)) {
            return 'Not found this values in provider.';
        }

        foreach ($current_local as $record_id => $items) {
            $budget = $current_provider[$record_id];
            $fail = $items->filter(function ($item) use ($budget, $value_name) {
                return $item->$value_name != $budget;
            });

            if (count($fail) > 0) {
                $results->push($fail);
            }
        }

        return $fail;
    }


    /**
     * Validates the change attributes
     *
     * @param $change_attributes
     *
     * @return bool
     */
    private static function validateChangeAttributes($change_attributes)
    {
        $rules = [
            'level' => "string",
            'type' => "required",
            'new_value' => "required",
            'value_format' => "required_if:action,bid",
            'value_operator' => "required_if:action,bid,budget,adjustment"
        ];

        // Validate the passed params
        $validation = Validator::make($change_attributes, $rules);
        if ($validation->fails()) {
            self::setFails(['error' => $validation->messages()->all()]);
            return false;
        }

        return true;
    }
}