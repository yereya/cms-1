<?php namespace App\Libraries\PublisherEditor;

use App\Entities\Models\Bo\Account;
use App\Libraries\PublisherEditor\Operations\Google\AdGroup as GoogleAdGroup;
use App\Libraries\PublisherEditor\Operations\Google\Campaign as GoogleCampaign;
use App\Libraries\PublisherEditor\Operations\Google\Keyword as GoogleKeyword;
use App\Libraries\PublisherEditor\Operations\Bing\Campaign as BingCampaign;
use App\Libraries\PublisherEditor\Operations\Bing\AdGroup as BingAdGroup;
use App\Libraries\PublisherEditor\Operations\Bing\Keyword as BingKeyword;

// GNG
use App\Libraries\PublisherEditor\Operations\GoogleGNG\AdGroup as GoogleGNGAdGroup;
use App\Libraries\PublisherEditor\Operations\GoogleGNG\Campaign as GoogleGNGCampaign;
use App\Libraries\PublisherEditor\Operations\GoogleGNG\Keyword as GoogleGNGKeyword;
use App\Libraries\PublisherEditor\Operations\BingGNG\Campaign as BingGNGCampaign;
use App\Libraries\PublisherEditor\Operations\BingGNG\AdGroup as BingGNGAdGroup;
use App\Libraries\PublisherEditor\Operations\BingGNG\Keyword as BingGNGKeyword;

class PublisherEditorFactory
{
    /**
     * @var - ad group object
     */
    protected static $ad_group;

    /**
     * @var - campaign object
     */
    protected static $campaign;

    /**
     * @var - keyword object
     */
    protected static $keyword;

    /**
     * Get AdGroup Object
     *
     * @param Account $account
     *
     * @return mixed
     */
    public static function adGroup(Account $account)
    {
        self::initAdGroup($account);
        return self::$ad_group;
    }

    /**
     * Get Campaign Object
     *
     * @param Account $account
     *
     * @return mixed
     */
    public static function campaign(Account $account)
    {
        self::initCampaign($account);
        return self::$campaign;
    }

    /**
     * Get Keyword Object
     *
     * @param Account $account
     *
     * @return mixed
     */
    public static function keyword(Account $account)
    {
        self::initKeyword($account);
        return self::$keyword;
    }

    /**
     * Init Keyword Class
     *
     * @param Account $account
     */
    private static function initKeyword(Account $account)
    {
        if (self::$keyword && self::$keyword->getAccountId() == $account->source_account_id) {
            return;
        }

//        self::$keyword = $account->publisher_id == 95 ? self::getObject(GoogleKeyword::class, $account->source_account_id) : self::getObject(BingKeyword::class, $account->source_account_id);
        switch ( $account->publisher_id){
            case 95:
                self::$keyword = self::getObject(GoogleKeyword::class, $account->source_account_id);
                break;
            case 97:
                self::$keyword  = self::getObject(BingKeyword::class, $account->source_account_id);
                break;
            case 163:
                self::$keyword = self::getObject(GoogleGNGKeyword::class, $account->source_account_id);
                break;
            case 164:
                self::$keyword = self::getObject(BingGNGKeyword::class, $account->source_account_id);
                break;
        }
    }

    /**
     * Init Campaign Class
     *
     * @param Account $account
     */
    private static function initCampaign(Account $account)
    {
        if (self::$campaign && self::$campaign->getAccountId() == $account->source_account_id) {
            return;
        }

        //self::$campaign = $account->publisher_id == 95 ? self::getObject(GoogleCampaign::class, $account->source_account_id) : self::getObject(BingCampaign::class, $account->source_account_id);

        switch ( $account->publisher_id){
            case 95:
                self::$campaign = self::getObject(GoogleCampaign::class, $account->source_account_id);
                break;
            case 97:
                self::$campaign = self::getObject(BingCampaign::class, $account->source_account_id);
                break;
            case 163:
                self::$campaign = self::getObject(GoogleGNGCampaign::class, $account->source_account_id);
                break;
            case 164:
                self::$campaign = self::getObject(BingGNGCampaign::class, $account->source_account_id);
                break;
        }
    }

    /**
     * Init AdGroup Class
     *
     * @param Account $account
     */
    private static function initAdGroup(Account $account)
    {
        if (self::$ad_group && self::$ad_group->getAccountId() == $account->source_account_id) {
            return;
        }

        //self::$ad_group = $account->publisher_id == 95 ? self::getObject(GoogleAdGroup::class, $account->source_account_id) : self::getObject(BingAdGroup::class, $account->source_account_id);

        switch ( $account->publisher_id){
            case 95:
                self::$ad_group = self::getObject(GoogleAdGroup::class, $account->source_account_id);
                break;
            case 97:
                self::$ad_group  = self::getObject(BingAdGroup::class, $account->source_account_id);
                break;
            case 163:
                self::$ad_group = self::getObject(GoogleGNGAdGroup::class, $account->source_account_id);
                break;
            case 164:
                self::$ad_group = self::getObject(BingGNGAdGroup::class, $account->source_account_id);
                break;
        }

    }

    /**
     * Get Class Object
     *
     * @param $class
     * @param $source_account_id
     *
     * @return mixed
     */
    private static function getObject($class, $source_account_id)
    {
        $obj = new $class($source_account_id);
        return $obj->get();
    }
}
