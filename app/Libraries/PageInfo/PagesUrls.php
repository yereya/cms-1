<?php

namespace App\Libraries\PageInfo;

use App\Entities\Models\Sites\Page;
use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\SiteContentTypeRepo;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use App\Facades\SiteConnectionLib;
use Illuminate\Support\Collection;

class PagesUrls extends PagesRouteLibs
{
    private $single;
    private $archive;
    private $domain;
    private $site;
    private $routes;

    /**
     * Build options
     */
    public function __construct()
    {
        parent::__construct();

        $this->single  = new Collection();
        $this->archive = new Collection();

        $this->init();
    }

    /**
     * Init
     */
    private function init()
    {
        $this->setDomain();
        $this->createArchiveHref();
        $this->createStaticHref();

        $this->setRoutes();
    }

    /**
     * Get Href by page id and post id
     *
     * @param int $page_post_id
     * @param int $post_id
     *
     * @return string
     */
    public function getHref(int $page_post_id, int $post_id): string
    {
        $routes = $this->listRoutes('_href');

        $key = "[href page_post_id=$page_post_id post_id=$post_id]";

        return $routes[$key] ?? '';
    }

    /**
     * Get href by short code key
     *
     * @param string $key
     *
     * @return string
     */
    public function getHrefStr(string $key): string
    {
        $routes = $this->listRoutes('_href');

        return $routes[$key] ?? '';
    }

    /**
     * Get collection with page and post object
     *
     * @param int $page_post_id
     * @param int $post_id
     *
     * @return Collection
     */
    public function getPageWithPost(int $page_post_id, int $post_id): Collection
    {
        $result = new Collection();

        $this->routes->each(function ($item) use ($page_post_id, $post_id, $result) {
            if ($item->post_id == $page_post_id && $item->post_id == $post_id) {
                $result->push($item);
            } else {
                if ($item->post_id == $page_post_id && $item->post_id != $post_id && $item->type == self::PAGE_TYPE_ARCHIVE) {
                    $result->push($item);
                    $post = $item->_posts->filter(function ($post) use ($post_id) {
                        return $post->id = $post_id;
                    })->first();
                    $result->push($post);
                }
            }
        });

        return $result;
    }

    /**
     * Get Page with all connected posts and hrefs by page_id
     *
     * @param int $page_post_id
     *
     * @return Page
     */
    public function getPage(int $page_post_id): Page
    {
        $page = $this->routes->filter(function ($item) use ($page_post_id) {
            return $item->post_id == $page_post_id;
        })->first();

        return $page ?? null;
    }

    /**
     * Get Routes
     *
     * @return Collection
     */
    public function routes(): Collection
    {
        return $this->routes;
    }

    /**
     * Return list routes
     *
     * @param string $href
     *
     * @return array
     */
    public function listRoutes(string $href = '_href_full'): array
    {
        return $this->pagesToArray($href);
    }

    /**
     * Create hrefs by type
     *
     * @return string|static
     */
    private function setRoutes()
    {
        $this->routes = $this->archive->merge($this->single);
    }

    /**
     * Set Site
     */
    private function setSite()
    {
        $this->site = SiteConnectionLib::getSite();
    }

    /**
     * Set Domain
     */
    private function setDomain()
    {
        $this->setSite();

        $this->domain = (new SiteDomainRepo())->getBySite($this->site->id)->domain;
    }

    /**
     * Get Domain
     *
     * @return mixed
     */
    public function getDomain(): string
    {
        return $this->domain;
    }

    /**
     * Create hrefs to static pages
     */
    private function createStaticHref()
    {
        foreach ($this->getByType() as $post_id => $page) {
            $href             = $this->buildUrl($page);
            $page->_href      = $href;
            $page->_href_full = $this->getDomain() . $href;
            $this->single->push($page);
        }
    }

    /**
     * Create hrefs to archive pages
     */
    private function createArchiveHref()
    {
        foreach ($this->getByType(self::PAGE_TYPE_ARCHIVE) as $post_id => $page) {
            $href             = $this->buildUrl($page);
            $page->_href      = $href;
            $page->_href_full = $this->getDomain() . $href;
            $page->_posts     = $this->archiveBrandsUrl($page, $href);
            $this->archive->push($page);
        }
    }

    /**
     * Create hrefs to brands in archive page
     *
     * @param $page
     * @param $slug
     *
     * @return Collection
     */
    private function archiveBrandsUrl(Page $page, string $slug): Collection
    {
        $content_type = $page->pageContentTypes->pluck('content_type_id') ?? [];

        $posts = new Collection();
        foreach ($content_type as $type) {
            foreach ($this->getPosts($type) as $post) {
                $post_with_href             = $post->replicate();
                $post_with_href->id         = $post->id;
                $post_with_href->_href      = $slug . DIRECTORY_SEPARATOR . $post->slug;
                $post_with_href->_href_full = $this->getDomain() . $slug . DIRECTORY_SEPARATOR . $post->slug;
                $posts->push($post_with_href);
            }
        }

        return $posts;
    }

    /**
     * Convert hierarchy structure to array list
     *
     * @param string $href
     *
     * @return array
     */
    private function pagesToArray(string $href): array
    {
        $converted = [];

        foreach ($this->routes as $item) {
            $key             = '[href page_post_id=' . $item->post_id . ' post_id=' . $item->post_id . ']';
            $converted[$key] = $item->{$href};

            if ($item->type == self::PAGE_TYPE_ARCHIVE) {
                foreach ($item->_posts as $post) {
                    $key             = '[href page_post_id=' . $item->post_id . ' post_id=' . $post->id . ']';
                    $converted[$key] = $post->{$href};
                }
            }
        }

        return $converted;
    }

    /**
     * Recursive build href
     *
     * @param        $page
     * @param string $directory
     *
     * @return string
     */
    private function buildUrl(Page $page, string $directory = ''): string
    {
        if ($page->post->parent_id) {
            $directory = DIRECTORY_SEPARATOR . $page->post->slug . $directory;
            $parent    = $this->pages->get($page->post->parent_id);

            if ($parent) {

                return $this->buildUrl($parent, $directory);
            }
        }

        return DIRECTORY_SEPARATOR . $page->post->slug . $directory;
    }


    /**
     * Convert hierarchy structure to flatten collection
     * 1. return collection with the following keys:
     *   - name -> name of the page.
     *   - id -> post id.
     *  - text ->  the href of the page.
     *
     *  @return Collection
     */
    public function resolvePagesList(): Collection
    {
        $converted = collect();

        foreach ($this->routes as $key=> $item) {
            $converted->push([
                'id'        => $item->id,
                'text'      => $item->_href,
                'slug'      => $item->_href,
                'name'      => $item->name
            ]);
        }

        return $converted;
    }


    /**
     *  Get Only Pages Hierarchy
     */
    public function resolveOnlyContentTypePages()
    {
        $pages                = collect();
        $page_content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();

        foreach ($this->routes as $index => $page) {
            $pages->push([
                'id'   => $page->post_id,
                'text' => $page->_href . ' ( ID : ' . $page->post_id . ")",
                'slug' => $page->_href,
                'name' => $page->name,
            ]);

            if ($page->type == self::PAGE_TYPE_ARCHIVE && $page->_post) {
                foreach ($page->_posts as $post) {
                    $content_type = (new SiteContentTypeRepo())->find($post->content_type_id);
                    if ($content_type->id == $page_content_type_id) {
                        $pages->push([
                            'id'   => $post->id,
                            'text' => $post->_href . ' ( ID : ' . $post->id . ")",
                            'slug' => $post->_href,
                            'name' => $post->name,
                        ]);
                    }
                }
            }
        }

        return $pages;
    }
}