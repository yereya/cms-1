<?php

namespace App\Libraries\PageInfo;

use App\Entities\Models\Sites\PageContentType;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Repositories\Sites\PostRepo;
use App\Http\Controllers\Sites\SitePostsController;
use Exception;
use View;

class TemplatePagesPostsInfoLib extends PageInfoAbstract
{
    const MOBILE = 'Mobile';
    const DESKTOP = 'Desktop';

    private $template;
    private $pages_info = [];
    private $posts_info = [];

    /**
     * Build
     *
     * @param Template $template
     */
    public function build(Template $template)
    {
        if (!$template) {

            throw new Exception('Template must be supplied');
        }

        $this->template = $template;
        $this->post_repo = new PostRepo();
        $this->page_posts = $this->post_repo->getByPageTemplateId($this->template->id);
        $this->page_mobile_posts = $this->post_repo->getByPageTemplateId($this->template->id, true);
        $this->buildPagesInfo();
        $this->buildFromSingleTemplates();
    }

    /**
     * Get Pages Info
     *
     * @return array
     */
    public function getPagesInfo()
    {
        return $this->pages_info;
    }

    /**
     * Get Posts Info
     *
     * @return array
     */
    public function getPostsInfo()
    {
        return $this->posts_info;
    }

    private function buildPagesInfo()
    {
        $this->page_posts->each(function ($post) {
            $this->addPageInfo($post, self::DESKTOP);
        });

        $this->page_mobile_posts->each(function ($post) {
            $this->addPageInfo($post, self::MOBILE);
        });

        return $this->pages_info;
    }

    /**
     * Build From Single Templates
     *
     * @return array
     */
    private function buildFromSingleTemplates()
    {
        $desktop_single_templates = PageContentType::where('template_id', $this->template->id)->get();
        if ($desktop_single_templates) {
            $desktop_single_templates->each(function ($single) {
                $this->addPagesFromSingle($single, self::DESKTOP);
                $this->addSinglePostsInfo($single, 'template_id', "Desktop");
            });
        }

        $mobile_single_templates = PageContentType::where('template_mobile_id', $this->template->id)->get();
        if ($mobile_single_templates) {
            $mobile_single_templates->each(function ($single) {
                $this->addPagesFromSingle($single, self::MOBILE);
                $this->addSinglePostsInfo($single, 'template_id', "Desktop");
            });
        }

        return $this->posts_info;
    }

    /**
     * Add Pages From Single
     *
     * @param $single
     * @param $device
     */
    private function addPagesFromSingle($single, $device)
    {
        $post = Post::find($single->post_id);
        $this->addPageInfo($post, $device);
    }

    /**
     * Add Page Info
     *
     * @param $post
     * @param $device
     */
    private function addPageInfo($post, $device)
    {
        $page = $post->content;
        if ($device == self::DESKTOP) {
            $template_url = route('sites.{site}.templates.show', [$this->site->id, $page->template_id]);
        } else {
            $template_url = route('sites.{site}.templates.show', [$this->site->id, $page->template_mobile_id]);
        }
        $labels = $this->getLabelsHtml($post);
        $this->pages_info[] = [
            'name' => $post->name,
            'slug' => $post->slug,
            'type' => $page->type,
            'labels' => $labels,
            'device' => $device,
            'page_url' => $this->getPageUrl($post->id),
            'published_status' => View::make('partials.containers.buttons.tag', array(
                'value' => $post->published_display_status,
                'class' => SitePostsController::PUBLISHED_STATUS_CLASS[$post->published_status]
            ))->render(),
            'page_edit_url' => route('sites.{site}.pages.edit', [$this->site->id, $post->id]),
            'template_url' => $template_url
        ];
    }

    /**
     * Add Single Posts Info
     *
     * @param $single
     * @param $template_field_name
     * @param $device
     */
    private function addSinglePostsInfo($single, $template_field_name, $device)
    {
        $template_id = $single->{$template_field_name};
        $posts = $this->post_repo->getByContentTypeId($single->content_type_id);
        $base_page_url = $this->getPageUrl($single->post_id);

        $template_url = route('sites.{site}.templates.show', [$this->site->id, $template_id]);
        $content_type_name = $single->contentType->name;
        foreach ($posts as $post) {
            $page_url = $base_page_url . '/' . $post->slug;
            $this->addPostInfo($content_type_name, $post, 'Single', $device, $template_url, $page_url);
        }
    }

    /**
     * Add Post Info
     *
     * @param $content_type_name
     * @param $post
     * @param $type
     * @param $device
     * @param $template_url
     * @param $page_url
     */
    private function addPostInfo($content_type_name, $post, $type, $device, $template_url, $page_url)
    {
        $labels = $this->getLabelsHtml($post);
        $this->posts_info[] = [
            'name' => $post->name,
            'slug' => $post->slug,
            'content_type' => $content_type_name,
            'device' => $device,
            'labels' => $labels,
            'type' => $type,
            'published_status' => View::make('partials.containers.buttons.tag', array(
                'value' => $post->published_display_status,
                'class' => SitePostsController::PUBLISHED_STATUS_CLASS[$post->published_status]
            ))->render(),
            'post_url' => route('sites.{site}.posts.edit', [$this->site->id, $post->id]),
            'template_url' => $template_url,
            'page_url' => $page_url
        ];
    }
}