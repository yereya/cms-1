<?php

namespace App\Libraries\PageInfo;

use App\Entities\Models\Sites\Page;
use Exception;

/**
 * Class PageTemplatesPostsInfoLib
 * @package App\Libraries\PageInfo
 */
class PageTemplatesPostsInfoLib extends PageInfoAbstract
{
    private $page;
    /**
     * @var array of templates information for data table
     */
    private $templates_info = [];

    /**
     * @var array of posts information for data table
     */
    private $posts_info = [];
    private $page_url;

    /**
     * build
     *
     * @param Page $page
     *
     * @throws Exception
     */
    public function build(Page $page)
    {
        if (!$page) {
            throw new Exception('Page must be supplied');
        }

        $this->page = $page;
        $this->page_url = $this->getPageUrl($this->page->post_id);
        $this->buildPostsInfo();
        $this->buildTemplatesInfo();
    }

    /**
     * Get Posts Info
     *
     * @return array
     */
    public function getPostsInfo()
    {
        return $this->posts_info;
    }

    /**
     * Get Templates Info
     *
     * @return array
     */
    public function getTemplatesInfo()
    {
        return $this->templates_info;
    }

    /**
     * Get Posts Info
     *
     * @return array for data table with all the posts related to the page
     */
    private function buildPostsInfo()
    {
        foreach ($this->page->pageContentTypes as $single_template) {
            $this->addPostInfo($single_template);
        }
    }

    /**
     * Get Templates Info
     *
     * @return array for data table with all the templates related to the page
     */
    private function buildTemplatesInfo()
    {
        $this->addTemplateInfo($this->page->template, $this->page->type, $this->page_url, 'Desktop');
        $this->addTemplateInfo($this->page->mobileTemplate, $this->page->type, $this->page_url, 'Mobile');


        foreach ($this->page->pageContentTypes as $single_template) {
            $post = $this->post_repo->getFirstPostByContentTypeId($single_template->content_type_id, true);

            if ($single_template->label_id && !$post->labels()->find($single_template->label_id)){
                continue;
            }

            $single_page_url = $this->page_url . '/' . $post->slug;
            $this->addTemplateInfo($single_template->mobileTemplate, 'Single', $single_page_url, 'Mobile');
            $this->addTemplateInfo($single_template->desktopTemplate, 'Single', $single_page_url, 'Desktop');
        }
    }

    /**
     * Add Post Info
     *
     * @param $single_template
     */
    private function addPostInfo($single_template)
    {
        $desktop_template_url = $single_template->template_id ?
            route('sites.{site}.templates.show', [$this->site->id, $single_template->template_id])
            : null;
        $mobile_template_url = $single_template->mobile_template_id ?
            route('sites.{site}.templates.show', [$this->site->id, $single_template->mobile_template_id])
            : null;

        $content_type = $single_template->contentType;
        $posts = $content_type->posts;

        foreach ($posts as $post) {
            if ($single_template->label_id && !$post->labels()->find($single_template->label_id)){
                continue;
            }
            $labels = $this->getLabelsHtml($post);
            $this->posts_info[] = [
                'name' => $post->name,
                'slug' => $post->slug,
                'content_type' => $content_type->name,
                'labels' => $labels,
                'post_url' => route('sites.{site}.posts.edit', [$this->site->id, $post->id]),
                'desktop_template_url' => $desktop_template_url,
                'mobile_template_url' => $mobile_template_url,
                'page_url' => $this->page_url . '/' . $post->slug
            ];
        }
    }

    /**
     * Add Template Info
     *
     * @param $template
     * @param $type
     * @param $page_url
     * @param $device
     */
    private function addTemplateInfo($template, $type, $page_url, $device)
    {
        if (!$template) {

            return;
        }

        $this->templates_info[] = [
            'name' => $template->name,
            'device' => $device,
            'type' => $type,
            'builder_url' => route('sites.{site}.templates.show', [$this->site->id, $template->id]),
            'page_url' => $page_url,
        ];
    }
}
