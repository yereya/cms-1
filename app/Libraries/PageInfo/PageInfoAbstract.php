<?php


namespace App\Libraries\PageInfo;


use App\Entities\Repositories\Sites\PageRepo;
use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use View;

/**
 * Class PageInfoAbstract
 * @package App\Libraries\PageInfo
 */
abstract class PageInfoAbstract
{
    protected $page_repo;
    protected $post_repo;
    protected $site;
    protected $domain;

    public function __construct($site)
    {
        $this->site = $site;
        $this->page_repo = new PageRepo();
        $this->post_repo = new PostRepo();
        $this->domain = (new SiteDomainRepo())->getBySite($site->id)->domain;
    }

    /**
     * Get Page Url
     *
     * @param $post_id
     * @return string
     */
    protected function getPageUrl($post_id)
    {
        return $this->page_repo->getFullUrl($this->domain, $post_id);
    }

    /**
     * Get Label Html
     *
     * @param $post
     * @return string
     */
    protected function getLabelsHtml($post)
    {
        $labels_html = '';
        $labels = $post->labels->pluck('name')->toArray();

        foreach ($labels as $label) {
            $labels_html .= View::make('partials.containers.buttons.tag', [
                'value' => $label,
                'class' => 'bold'
            ]);
        }

        return $labels_html;
    }
}