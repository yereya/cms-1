<?php namespace App\Libraries;

use App\Entities\Models\Scheduler\Task;
use App\Entities\Repositories\Bo\AdvertiserRepo;
use App\Entities\Repositories\Users\UserRepo;
use App\Libraries\Export\ToCsv;
use DB;
use PDO;
use Twilio\Rest\Client;
use App\Events\AlertEvent;
use App\Facades\ShortCode;
use App\Entities\Models\HolsteredQuery;
use App\Libraries\Alerts\EmailAlertsByRoleID;


/**
 * Class TpLogger
 *
 * @package App\Console\Commands\ReportsScraper\Providers
 */
class HolsteredQueriesLib
{

    private static $system_id = 31;

    private static $logger;

    /**
     * Fetch
     *
     * @param       $query_id
     * @param array $atts
     *
     * @return mixed
     * @internal param $email
     */

    public static function fetch($query_id, $atts = [])
    {
        $atts = filterAndSetParams([
            'email'               => false,
            'sms'                 => false,
            'enable_shortcodes'   => false,
            'alert'               => false,
            'email_by_permission' => false,
            'email_by_role_id'    => false,
            'parameters'          => false,
            'asCsv'               => false,

        ], $atts);

        static::$logger = TpLogger::getInstance();
        static::$logger->setTask($query_id)->setSystem(static::$system_id);

        try {
            $query = HolsteredQuery::find($query_id);
        } catch (\Exception $e) {
            static::$logger->error(['Exception' => 'Failed to find holstered query id -'.$query_id.'. Error message - '.$e->getMessage()]);
        }

        static::$logger->info('Query: ' . $query_id . ' Name: ' . @$query->name);
        static::$logger->info(['Query' => $query]);

        // TODO - consider adding the entire shortCodes mechanism. for now we just implemented a parameter replacement function.
        if ($atts['parameters']) {
            try {
                $query['query'] = self::replacePlaceholdersWithValues($query['query'], $atts['parameters']);
            } catch (\Exception $e) {
                static::$logger->error(['Exception' => 'Failed to replace parameters in query id -'.$query_id.'. Error message - '.$e->getMessage()]);
            }
        }

        if (!self::validateQueryResult($query_id, $query)) {
            return false;
        }

        self::updateQueryLastRunStamp($query_id);

        // parse all short codes if enabled
        if ($atts['enable_shortcodes']) {
            $query['query'] = self::setShortCodes($query['query']);
        }

        try {
            $db_connection = DB::connection($query->db);
        } catch (\Exception $e) {
            static::$logger->error(['Exception' => 'Failed to connect to db -'.$query->db.' '.$e->getMessage()]);
        }

        $result       = '';
        $query_result = false; // Set default to the result being false
        $statement_name = $query['statement'];
        try {
            switch ($statement_name) {
                case "select":
                    $db_connection->setFetchMode(PDO::FETCH_ASSOC);
                    $query_result = $db_connection->select(DB::raw($query['query']));
                    $result = $query_result;
                    break;
                case "insert":
                case "delete":
                case "update":
                    $query_result = $db_connection->$statement_name(DB::raw($query['query']));
                    $result = ['Effected rows:', $query_result];
                    break;

                default:
                    $query_result = $db_connection->statement(DB::raw($query['query']));
                    $result = ['Run result:', $query_result];
            }
        } catch (\Exception $e) {
            static::$logger->error(['Exception' => 'Failed to execute sql query -'.$query['query'].'. Error message -'.$e->getMessage()]);
        }

        try {
            $db_connection->disconnect();
        } catch (\Exception $e) {
            static::$logger->error(['Exception' => 'Failed to disconnect from db -'.$query->db.'. Error message -'.$e->getMessage()]);
        }

        // If the run was successful lets update the the corresponding field
        if (($query['statement'] == 'select' && static::resultsExist($query_result)) || $query_result) {
            self::updateQueryLastEffectiveRunStamp($query_id);
        }

        static::$logger->info(['Result' => $result]);

        try {/*Check if one of the email options is set*/
            if ($atts['email'] && $result) {
                if ($atts['email_by_permission']) {
                    static::sendByPermission($query_id, $query, $atts, $result);
                }
                else{
                    self::email($result, $query, $query_id, $atts['email'],$atts['asCsv'] );
                }
            }
        } catch (\Exception $e) {
            static::$logger->error(['Exception' => 'Failed to send email. Error message -'.$e->getMessage()]);
        }
        if ($atts['email_by_role_id']  && $result )
        {
            $emailAlertsByRoleID = new EmailAlertsByRoleID(@$query->name, $atts['email_by_role_id'],$result, $query_id, $atts['asCsv']);
            $emailAlertsByRoleID->start();
        }

        // Email the results when an email is passed
        if ($atts['sms'] && $result)
        {
            $sms_message = arrayToString($result, true);
            $atts['sms'] = explode(',', $atts['sms']);
            self::sms($sms_message, $atts['sms']);
        }

        // TODO: this is all a temporary code utill we figure out the big picture
        // meanwhile, only holstered query 27 - About to exceed or exceeded budget, will be sent with the alert option
        if ($atts['alert'] && !empty($result)) {
            event(new AlertEvent($query_id, $result));
        }


        return $result;
    }


    /**
     * Get Query
     *
     * @param $query_id
     *
     * @return mixed
     */
    public static function getQuery($query_id)
    {
        return HolsteredQuery::where('id', $query_id)->get();
    }

    /**
     * email
     *
     * @param $result
     * @param $query
     * @param $query_id
     * @param $email
     */
    private static function email($result, $query, $query_id, $email,$asCsv = false)
    {
        $title = 'Query: [' . $query_id . '] ' . $query['name'];
        $body = ['content' => $result, 'title' => $title];
        try{
            # If description for the query exists in cms.scheduler_tasks under description column, add it to body and parse later on
            $body['description'] = Task::select('description')
                ->where('field_query_params_id','=','9')
                ->where('task_id','=',$query->getAttributes()['id'])
                ->first()['description'];
        }catch (\Exception $e){
            static::$logger->error(['Exception' => 'Error in extracting description in HolsteredQueriesLib.php 195 '.' '.$e->getMessage()]);
        }

        try {
            if($asCsv){
                Mail::tplAttachment(
                    $email,
                    $body ,
                    self::create_csv_string($result, $query_id)
                );

            }else {
                Mail::tplBlank(
                    $email,
                    $body
                );
            }
        } catch (Exception $e) {
            static::$logger->error(['Exception' => 'Failed to send email to '.$email.' '.$e->getMessage()]);
        }

    }


    /**
     * Create csv
     *
     * @param $data
     */
    private static function create_csv_string($data, $query_id) {
        try {

            # --- Create storage folder and path ---
            $report_folder = 'reports' . DIRECTORY_SEPARATOR . 'MailAlertCsv';
            \Storage::makeDirectory($report_folder);
            $report_folder_path = storage_path('app') . DIRECTORY_SEPARATOR . $report_folder;
            $file_name = $report_folder_path . DIRECTORY_SEPARATOR . $query_id . '.csv';

            # --- Create CSV file ---
            // Set headers
            $headers = array_keys(array_pop($data));
            // Open temp file pointer
            $fp = fopen($file_name, 'w');

            // Loop data and write to file pointer
            fputcsv($fp, $headers); // add Headers
            foreach ($data as $line) fputcsv($fp, $line);

            // Place stream pointer at beginning
            rewind($fp);
        }catch (\Exception $e){
            throw new \Exception('Raised exception in creating csv from data, HolsteredQueriesLib.php -> create_csv_string. Exception: ' . $e);
        }

        // Return the data
        return $file_name;
    }

    /**
     * sms
     *
     * @param $message
     * @param $numbers
     *
     * @return bool
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    private static function sms($message, $numbers)
    {
        if (empty(env('TWILIO_SID')) || empty(env('TWILIO_AUTH'))) {
            return false;
        }

        $client = new Client(env('TWILIO_SID'), env('TWILIO_AUTH'));
        foreach ((array)$numbers as $number) {
            try {
                $message_response = $client->messages->create(
                    $number,
                    [
                        'from' => env('TWILIO_NUMBER'),
                        'body' => $message
                    ]
                );

            } catch (\Exception $e) {
                // Silence is key
                static::$logger->error(['Could not send SMS to' => $number, 'Exception' => $e->getMessage()]);
            }
        }
    }

    /**
     * Validate Query Result
     *
     * @param $query_id
     * @param $query
     *
     * @return bool
     */
    private static function validateQueryResult($query_id, $query)
    {


        /** @var array $countable_query */
        if (!static::resultsExist($query)) {
            static::$logger->error('Query id ' . $query_id . ' could not be found');
            return false;
        }

        if (isset($query['query']) && !$query['query']) {
            static::$logger->error('Query id ' . $query_id . ' does not have valid query');
            return false;
        }

        return true;
    }

    /**
     * Replaces placeholders with the given parameters values
     *
     * @param $query - query placeholders should appear as '[parameter name]'
     * @param $arrValues
     *
     * @return bool
     */
    private static function replacePlaceholdersWithValues($query, $arrValues)
    {
        if (is_array($arrValues)) {
            foreach ($arrValues as $key => $value) {
                $query = preg_replace("/\[{$key}\]/", $value, $query);
            }
        }
        return $query;
    }


    /**
     * Update Query Last Run Stamp
     *
     * @param $query_id
     */
    private static function updateQueryLastRunStamp($query_id)
    {
        $update_params = ['last_run' => \DB::raw('NOW()')];
        HolsteredQuery::where('id', $query_id)->update($update_params);
    }


    /**
     * Update Query Last Effective Run Stamp
     *
     * @param $query_id
     */
    private static function updateQueryLastEffectiveRunStamp($query_id)
    {
        $update_params = ['last_effective_run' => \DB::raw('NOW()')];
        HolsteredQuery::where('id', $query_id)->update($update_params);
    }


    /**
     * Set Short Codes
     *
     * @param $query
     *
     * @return mixed
     */
    private static function setShortCodes($query)
    {
        return ShortCode::setShortCodes($query);
    }

    /**
     * @param $query
     *
     * @return int
     */
    private static function resultsExist($query)
    {
        $countable_query = [];

        if (is_object($query)) {
            $countable_query = $query->toArray();
        }

        if (is_array($query)) {
            $countable_query = $query;
        }

        return count($countable_query);
    }


    /**
     * Iterate over emails and send Results according to user permissions
     * @param $query_id
     * @param $query
     * @param $atts
     * @param $result
     */
    public static function sendByPermission($query_id, $query, $atts, $result)
    {
        // build collection from the results
        $collection_result = collect($result);
        $users_repo        = (new UserRepo());
        $emails            = $atts['email'];

        /*Get users by emails*/
        $users = $users_repo->model()
            ->whereIn('email', explode(',', $emails))
            ->where('status', '1');

        $users->each(function ($user) use ($collection_result, $query, $query_id, $users_repo, $atts) {
            $user_accounts    = $users_repo->getActiveAccounts($user->id);
            $advertisers_id   = ($users_repo->getAdvertiserIds($user->id))->toArray();
            $user_advertisers = (new AdvertiserRepo())->getUserAdvertisersByIds($advertisers_id);

            /*Filter results by permissions*/
            $filtered_results = $collection_result->filter(function ($result) use ($user_advertisers, $user_accounts
            ) {
                $has_key         = false;
                $field_to_method = collect([
                    'advertiser_name' => 'user_advertisers',
                    'account_name'    => 'user_accounts'
                ]);

                $field_to_method->each(function ($method, $field) use (
                    $result, &$has_key, $user_accounts, $user_advertisers
                ) {
                    if (array_key_exists($field, $result)) {
                        $field   = $result[$field];
                        $has_key = $$method->search($field);

                        return false;
                    }
                });

                return $has_key;
            });

            if ($filtered_results->count()) {
                // Email the results when an email is passed
                self::email($filtered_results->toArray(), $query, $query_id, $user->email,$atts['asCsv']);

                return true;
            }

            static::$logger->info("no permissions associated to user {$user->name} so there's no need to send email");
        });
    }
}
