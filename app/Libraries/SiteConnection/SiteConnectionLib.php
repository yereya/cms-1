<?php namespace App\Libraries\SiteConnection;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteDomain;
use App\Entities\Repositories\Sites\SiteRepo;
use App\Libraries\SiteConnection\Exceptions\ConnectionNotDefinedException;
use App\Libraries\SiteConnection\Exceptions\DatabaseExistsException;
use Artisan;
use Config;
use DB;
use Illuminate\Database\Connection;
use Illuminate\Database\ConnectionResolver;

/**
 * Class SiteConnectionLib
 *
 * @package App\Libraries\SiteConnection
 */
class SiteConnectionLib
{
    CONST DB_PREFIX = 'tpsite';

    /**
     * @var Site $site
     */
    private $site;

    /**
     * @var Domain $domain
     */
    private $domain;

    /**
     * @var Connection $connection
     */
    private $connection;

    /**
     * @var Migrator $migrator
     */
    private $migrator;

    /**
     * @var ConnectionResolver $resolver
     */
    private $resolver;

    /**
     * Create a new on the fly database connection.
     *
     * @param Site $site
     * @param null $domain
     */
    public function __construct(Site $site = null, $domain = null)
    {
        $this->resolver = new ConnectionResolver();

        if ($site) {
            $this->setSite($site);
            $this->addConnection();
        } else {
            $this->site = new Site();
        }
    }

    /**
     * Add Connection
     */
    public function addConnection()
    {
        $connection_name = $this->storeConnection($this->site);
        $this->resolver->addConnection($connection_name, DB::connection($connection_name));

        if (!$this->connection) {
            $this->setConnection($connection_name);
        }
    }

    /**
     * Set Options With Defaults
     *
     * @param Site $site
     *
     * @return string
     */
    private function storeConnection(Site $site)
    {
        $connection_name = $this->getQualifiedName($site, 'name');
        $site->db_name   = $this->getQualifiedName($site, 'db_name');

        $options = $this->setConnectionOptionsWithDefaults([
            'host'     => env('SITE_DB_HOST', $site->db_host),
            'database' => env('SITE_DB_PREFIX') . $site->db_name,
            'username' => env('SITE_DB_USERNAME', $site->db_user),
            'password' => env('SITE_DB_PASSWORD', $site->db_pass)
        ]);

        Config::set("database.connections.{$connection_name}", $options);

        return $connection_name;
    }

    /**
     * Get Qualified Name
     *
     * @param Site   $site
     * @param string $property
     *
     * @return string
     */
    private function getQualifiedName(Site $site, $property)
    {
        if (starts_with($site->$property, self::DB_PREFIX)) {
            return $site->$property;
        }

        return self::DB_PREFIX . '_' . snake_case($site->$property);
    }

    /**
     * Set Connection Options With Defaults
     *
     * @param array $options
     *
     * @return array
     */
    private function setConnectionOptionsWithDefaults(array $options)
    {
        $default = config('database.connections.' . config("database.default"));

        foreach ($default as $key => $value) {
            if (!isset($options[$key])) {
                $options[$key] = $value;
            }
        }

        return $options;
    }

    /**
     * Set Site By Domain
     *
     * @param null $domain_name
     *
     * @throws \Exception
     */
    public function addConnectionByDomain($domain_name = null)
    {
        if (app()->runningInConsole()) {
            return;
        }

        $this->resolveDomain($domain_name);
        $this->setSite($this->domain->site);

        $this->addConnection();
    }

    /**
     * resolveDomain
     *
     * @param $domain_name
     *
     * @return mixed
     * @throws \Exception
     */
    private function resolveDomain($domain_name)
    {
        if (is_null($domain_name)) {
            $domain_name = request()->server('HTTP_HOST');
        }

        $domain = SiteDomain::where('domain', $domain_name)->first();
        if (!$domain) {
            throw new \Exception('Could not serve site from this domain.');
        }

        $this->setDomain($domain);
    }

    /**
     * setDomain
     *
     * @param SiteDomain $domain
     */
    private function setDomain(SiteDomain $domain)
    {
        $this->domain = $domain;
    }

    /**
     * setEnv
     *
     * @param string $environment
     *
     * @return $this
     */
    public function setEnv($environment = 'CMS')
    {
        if ($environment == 'TpSite') {
            Config::set("database.default", $this->getConnectionName());
        }

        return $this;
    }

    /**
     * Get Connection Name
     *
     * @return string
     */
    public function getConnectionName()
    {
        return $this->connection;
    }

    /**
     * Create Connection
     **
     *
     * @throws DatabaseExistsException
     */
    public function createDatabase()
    {
        $db_name = $this->site->db_name;
        if (!$this->schemeExists($db_name)) {
            $this->createScheme($db_name);
            $this->populateScheme();
        } else {
            throw new DatabaseExistsException("The database '{$db_name}' already exists.");
        }
    }

    /**
     * Scheme Exists
     *
     * @param string $db_name
     *
     * @return bool
     */
    private function schemeExists($db_name)
    {
        return (bool)DB::select("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{$db_name}'");
    }

    /**
     * Create Scheme
     *
     * @param string $db_name
     */
    private function createScheme($db_name)
    {
        DB::statement("CREATE SCHEMA IF NOT EXISTS {$db_name} DEFAULT CHARACTER SET utf8");
    }

    /**
     * Populate Scheme
     */
    private function populateScheme()
    {
        $default_database = config("database.default");
        Artisan::call('migrate', [
            '--database' => $this->getConnection()->getName(),
            '--force'    => true,
            '--path'     => 'app/Libraries/SiteConnection/migrations'
        ]);

        config(["database.default" => $default_database]);
        DB::setDefaultConnection(config("database.default"));
    }

    /**
     * Get Connection
     *
     * @param string|null $name
     *
     * @return \Illuminate\Database\ConnectionInterface
     * @throws \Exception
     */
    public function getConnection($name = null)
    {
        $connection_name = $name ?: $this->connection;
        if (!$connection_name) {
            throw new \Exception('Connection name was not set.');
        }

        return $this->resolver->connection($connection_name);
    }

    /**
     * Set Connection
     *
     * @param string $name
     */
    public function setConnection($name)
    {
        $this->connection = $name;
    }

    /**
     * getSite
     *
     * @return Site
     */
    public function getSite(): Site
    {
        return $this->site;
    }

    /**
     * Set Site
     *
     * @param Site $site
     *
     * @return $this
     */
    public function setSite(Site $site)
    {
        $this->site = $site;

        $this->addConnection();

        return $this;
    }

    /**
     * Test Connection
     *
     * @throws InvalidConnectionCredentialsException
     */
    public function testConnection()
    {
        try {
            $this->getConnection()->getPdo();
        } catch (\Exception $m) {
            throw new InvalidConnectionCredentialsException($m);
        }
    }

    /**
     * Migrate
     *
     * @return Migrator
     */
    public function migrate()
    {
        if (!$this->migrator) {
            $this->migrator = new Migrator($this->site, $this->getConnection());
        }

        return $this->migrator;
    }

}