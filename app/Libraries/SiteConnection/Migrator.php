<?php namespace App\Libraries\SiteConnection;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteMigration;
use App\Libraries\ShortCodes\Contracts\ShortCodesLib as ShortCodesContract;
use App\Libraries\ShortCodes\ShortCodesLib;
use Illuminate\Database\Connection;
use Illuminate\Database\QueryException;

/**
 * Class Migrator
 *
 * @package App\Libraries\SiteConnection
 */
class Migrator
{
    /**
     * @var Site $site
     */
    protected $site;

    /**
     * @var ShortCodesLib $short_codes
     */
    protected $short_codes;

    /**
     * @var array|string $short_codes_haystack
     */
    protected $short_codes_haystack;

    /**
     * @var Connection $connection
     */
    protected $connection;

    /**
     * Migrator constructor.
     *
     * @param Site            $site
     * @param Connection|null $connection
     */
    public function __construct(Site $site, Connection $connection = null)
    {
        $this->site        = $site;
        $this->short_codes = app(ShortCodesContract::class);

        if ($connection) {
            $this->setConnection($connection);
        }
    }

    /**
     * Set Connection
     *
     * @param Connection $connection
     */
    public function setConnection(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * Run All
     *
     * @param string $method
     * @param bool   $pretend
     */
    public function runAll($method = 'up', $pretend = false)
    {
        $batch = $this->getBatch();

        foreach (SiteMigration::active()->orderBy('order')->get() as $migration) {
            $this->run($migration, $method, $pretend);
            $this->log($migration, $method, $batch);

            $batch++;
        }
    }

    /**
     * Get Batch
     *
     * @return int
     */
    private function getBatch()
    {
        return $this->site->migrations()->max('batch') + 1;
    }

    /**
     * Run Up
     *
     * @param string|SiteMigration $migration
     * @param string               $method
     * @param bool                 $pretend
     *
     * @return bool
     */
    public function run($migration, $method = 'up', $pretend = false)
    {
        if (is_string($migration)) {
            /** @var SiteMigration $migration */
            $migration = SiteMigration::whereName($migration)->firstOrFail();
        }

        if ($pretend) {
            return $this->pretend($migration, $method);
        }

        $sql = $this->getSql($migration, $method);

        try {
            $sql = $this->short_codes->setShortCodes($sql, [$this->short_codes_haystack]);

            return $this->connection->statement($sql);
        } catch (\Exception $m) {
            throw new QueryException($sql, [], $m);
        }
    }

    /**
     * Pretend
     *
     * @param SiteMigration $migration
     * @param string        $method
     *
     * @return array
     */
    public function pretend(SiteMigration $migration, $method = 'up')
    {
        return $this->connection->pretend(function () use ($migration, $method) {
            $this->connection->statement($this->getSql($migration, $method));
        });
    }

    /**
     * Get Sql
     *
     * @param SiteMigration $migration
     * @param string        $method
     *
     * @return mixed
     */
    private function getSql(SiteMigration $migration, $method)
    {
        return $migration->{'sql_' . $method};
    }

    /**
     * Log
     *
     * @param SiteMigration $migration
     * @param string        $method
     * @param int           $batch
     */
    public function log(SiteMigration $migration, $method = 'up', $batch = 0)
    {
        if ($method == 'up') {
            $this->site->migrations()->attach($migration, compact('batch'), false);
        } else {
            $this->site->migrations()->detach($migration, false);
        }
    }

    /**
     * Set Short Codes Haystack
     *
     * @param array|string $data
     */
    public function setShortCodesHaystack($data)
    {
        $this->short_codes_haystack = $data;
    }
}