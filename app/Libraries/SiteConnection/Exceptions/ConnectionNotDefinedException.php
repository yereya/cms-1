<?php namespace App\Libraries\SiteConnection\Exceptions;

/**
 * Class ConnectionNotDefinedException
 *
 * @package App\Libraries\SiteConnection\Exceptions
 */
class ConnectionNotDefinedException extends \Exception
{
    //
}