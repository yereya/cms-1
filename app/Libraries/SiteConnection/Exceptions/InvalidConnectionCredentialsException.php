<?php namespace App\Libraries\SiteConnection;

/**
 * Class InvalidConnectionCredentialsException
 *
 * @package App\Libraries\SiteConnection
 */
class InvalidConnectionCredentialsException extends \Exception
{
    //
}