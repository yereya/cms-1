<?php namespace App\Libraries\SiteConnection\Exceptions;

/**
 * Class DatabaseExistsException
 *
 * @package App\Libraries\SiteConnection\Exceptions
 */
class DatabaseExistsException extends \Exception
{
    //
}