<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddTemplateMobileToPageContentType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_content_type', function (Blueprint $table) {
            $table->integer('template_mobile_id')->nullable()->unsigned();
            $table->foreign('template_mobile_id')->references('id')
                ->on('templates')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_content_type', function (Blueprint $table) {
            $table->removeColumn('template_mobile_id');
        });
    }
}
