<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('site_id')->nullable()->unsigned();
            $table->string('site_name', 255)->nullable();
            $table->string('folder')->nullable();
            $table->string('path', 500);
            $table->string('thumb', 500)->nullable();
            $table->string('filename');
            $table->string('title')->nullable();
            $table->string('alt')->nullable();
            $table->string('metadata', 8192)->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->double('size')->nullable();
            $table->string('mime', 32)->index();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('media');
    }
}