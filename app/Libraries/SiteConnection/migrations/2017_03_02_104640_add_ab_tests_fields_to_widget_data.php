<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAbTestsFieldsToWidgetData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('widget_data', function (Blueprint $table) {
            $table->text('ab_tests_action')->nullable()->after('data');
        });

        Schema::create('ab_test_widget_data', function (Blueprint $table) {
            $table->integer('test_id')->unsigned();
            $table->integer('data_id')->unsigned();
        });

        Schema::table('ab_test_widget_data', function (Blueprint $table) {
            $table->foreign('test_id')->references('id')->on('ab_tests')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('data_id')->references('id')->on('widget_data')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('widget_data', function (Blueprint $table) {
            $table->dropColumn('ab_tests_action');
        });

        Schema::dropIfExists('ab_test_widget_data');
    }
}
