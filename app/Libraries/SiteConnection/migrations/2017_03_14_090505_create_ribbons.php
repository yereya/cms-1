<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRibbons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ribbons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->unique();
            $table->integer('image_id')->nullable()->unsigned();
            $table->string('html_class', 1000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('ribbons', function (Blueprint $table) {
            $table->foreign('image_id')->references('id')->on('media')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ribbons');
    }
}
