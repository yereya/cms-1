<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWhereOperatorAndWhereValueToDynamicListFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dynamic_list_fields', function (Blueprint $table) {
            $table->string('operator');
            $table->string('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dynamic_list_fields', function (Blueprint $table) {
            $table->dropColumn('operator');
            $table->dropColumn('value');
        });
    }
}
