<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePopupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popups', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name', 100);
            $table->boolean('active');
            $table->integer('template_id');
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->enum('appear_event', ['on_page_load', 'x_seconds_of_inactivity', 'scroll_page','on_mouse_exit'])
                ->default('on_page_load');
            $table->integer('param')->nullable();
            $table->string('cookie_name', 64)->nullable();
            $table->integer('cookie_time_on_conversion')->nullable();
            $table->integer('cookie_time_on_close')->nullable();
            $table->boolean('pivot_exclude')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->index('start_date');
            $table->index('end_date');
            $table->index('active');
        });

        Schema::create('popup_post', function (Blueprint $table) {
            $table->integer('popup_id');
            $table->integer('post_id');
            $table->unique(['popup_id', 'post_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popups');
        Schema::dropIfExists('popup_post');

    }
}
