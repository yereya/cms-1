<?php

use Illuminate\Database\Migrations\Migration;

class ChangeTableNameWidgetPostOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('widget_post_order', 'widget_data_post');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('widget_data_post');
    }
}
