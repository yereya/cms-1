<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediaLabelPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('label_media', function (Blueprint $table) {
            $table->integer('media_id')->unsigned();
            $table->integer('label_id')->unsigned();
        });

        Schema::table('label_media', function (Blueprint $table) {
            $table->foreign('media_id')->references('id')->on('media')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('label_id')->references('id')->on('labels')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('label_media', function (Blueprint $table) {
            $table->dropForeign('label_media_media_id_foreign');
            $table->dropForeign('label_media_label_id_foreign');
        });

        Schema::dropIfExists('media_label');
    }
}
