<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMembersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('email', 100);
            $table->string('name', 255)->nullable();
            $table->string('password', 100);
            $table->string('password_salt', 30);
            $table->enum('status', ['unverified', 'verified', 'canceled'])->default('unverified');
            $table->rememberToken();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });

        Schema::create('member_groups', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name', 100);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });

        Schema::create('member_group_members', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('member_group_id')->unsigned();
            $table->integer('member_id')->unsigned();

            $table->primary(['member_group_id', 'member_id']);
        });

        Schema::table('member_group_members', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->foreign('member_group_id')->references('id')->on('member_groups')->onDelete('cascade');
            $table->foreign('member_id')->references('id')->on('members')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('members');
        Schema::dropIfExists('member_groups');
        Schema::dropIfExists('member_group_members');

        Schema::enableForeignKeyConstraints();
    }
}
