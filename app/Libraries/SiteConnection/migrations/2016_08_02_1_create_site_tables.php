<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSiteTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name')->index();
        });

        Schema::create('templates', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('parent_id')->nullable()->unsigned();
            $table->foreign('parent_id')->references('id')->on('templates')->onDelete('set null');
            $table->string('name');
            $table->boolean('active')->index();
            $table->text('custom_html_head')->nullable();
            $table->text('custom_html_body')->nullable();
            $table->unsignedInteger('priority')->nullable();
            $table->string('scss', 10000)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        Schema::create('pages', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->string('name')->index();
            $table->string('slug')->nullable();
            $table->string('type', 50)->default('static');
            $table->integer('template_id')->unsigned()->nullable();
            $table->foreign('template_id')->references('id')->on('templates')->onDelete('set null');
            $table->integer('content_type_id')->unsigned()->nullable();
            $table->string('url_type')->nullable();
            $table->string('url_redirect', 1000)->nullable();
            $table->boolean('url_query')->nullable()->default(0);
            $table->integer('page_redirect_id')->unsigned()->nullable();
            $table->unsignedInteger('priority')->nullable();
            $table->boolean('published')->nullable()->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes();
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->unique(['slug']);
        });

        Schema::create('template_rows', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('template_id')->unsigned();
            $table->string('html_element_id')->nullable();
            $table->string('html_class')->nullable();
            $table->string('html_container_class')->nullable();
            $table->string('html_wrapper_class')->nullable();
            $table->integer('order')->index();
            $table->integer('total_size');
            $table->boolean('locked');
            $table->boolean('active')->index();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });


        Schema::create('template_row_columns', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('row_id')->unsigned();
            $table->foreign('row_id')->references('id')->on('template_rows')->onDelete('cascade');
            $table->integer('template_id')->unsigned();
            $table->foreign('template_id')->references('id')->on('templates');
            $table->string('html_element_id')->nullable();
            $table->string('html_class')->nullable();
            $table->integer('order')->index();
            $table->integer('size');
            $table->boolean('locked');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        Schema::table('template_rows', function (Blueprint $table) {
            $table->foreign('template_id')->references('id')->on('templates')->onDelete('cascade');
        });

        Schema::create('widget_templates', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('site_widget_template_id')->unsigned();
            $table->string('scss', 10000)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });


        Schema::create('widget_data', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('widget_id')->index()->unsigned();
            $table->string('name');
            $table->integer('widget_template_id')->index()->nullable()->unsigned();
            $table->integer('content_type_id')->index()->nullable()->unsigned();
            $table->integer('post_id')->index()->nullable()->unsigned();
            $table->integer('menu_id')->index()->nullable()->unsigned();
            $table->text('data')->nullable();
            $table->string('scss', 10000)->nullable();
            $table->boolean('active')->index();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        Schema::create('template_row_column_items', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('column_id')->index()->unsigned();
            $table->integer('item_id')->nullable()->unsigned();
            $table->string('item_type')->index();
            $table->integer('template_id')->unsigned();
            $table->foreign('template_id')->references('id')->on('templates');
            $table->integer('order')->index();

            $table->boolean('show_on_desktop')->default(1);
            $table->boolean('show_on_tablet')->default(1);
            $table->boolean('show_on_mobile')->default(1);
            $table->boolean('is_live')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

        Schema::table('template_row_column_items', function (Blueprint $table) {
            $table->foreign('column_id')->references('id')->on('template_row_columns')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('widget_data_template_row_column');
        Schema::dropIfExists('widget_data');
        Schema::dropIfExists('template_row_columns');
        Schema::dropIfExists('template_rows');
        Schema::dropIfExists('templates');

        Schema::dropIfExists('sections');
        Schema::dropIfExists('pages');

        Schema::enableForeignKeyConstraints();
    }
}
