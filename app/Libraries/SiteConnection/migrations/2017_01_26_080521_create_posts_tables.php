<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('slug', 100);
            $table->string('name', 100);
            $table->boolean('active')->default(false);
            $table->integer('section_id')->nullable();
            $table->integer('content_type_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('priority')->nullable();

            $table->boolean('show_on_sitemap');
            $table->string('seo_post_title')->nullable();
            $table->string('description')->nullable();
            $table->string('keywords')->nullable();
            $table->string('og_image')->nullable();
            $table->string('og_title')->nullable();
            $table->string('og_type')->nullable();
            $table->string('og_url')->nullable();
            $table->string('og_description')->nullable();
            $table->boolean('robots_index');
            $table->boolean('robots_follow');
            $table->string('robots_custom')->nullable();
            $table->string('permalink')->nullable();
            $table->string('canonical_url')->nullable();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('published_at')->nullable();
            $table->softDeletes();

            $table->unique(['slug', 'content_type_id', 'parent_id', 'deleted_at']);
        });


        Schema::create('labels', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->nullable()->usigned();
            $table->integer('priority')->unsigned()->default(0);
            $table->string('name', 150);
            $table->string('icon', 50);
        });


        Schema::create('post_label', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts');
            $table->integer('label_id')->unsigned();
            $table->foreign('label_id')->references('id')->on('labels');
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('posts');
        Schema::dropIfExists('labels');
        Schema::dropIfExists('post_label');

        Schema::enableForeignKeyConstraints();
    }
}
