<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConnectionContentTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_content_type', function (Blueprint $table) {
            $table->integer('post_id')->unsigned();
            $table->integer('field_id');
            $table->integer('connect_to_post_id')->unsigned();
        });

        Schema::table('link_content_type', function (Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('connect_to_post_id')->references('id')->on('posts')->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_content_type');
    }
}
