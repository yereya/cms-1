<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicListTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('content_type_id')->index();
            $table->string('name');
            $table->boolean('is_static_list')->index();
            $table->integer('query_offset')->nullable();
            $table->integer('query_limit')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('dynamic_list_label', function (Blueprint $table) {
            $table->unsignedInteger('dynamic_list_id');
            $table->unsignedInteger('label_id');
        });

        Schema::table('dynamic_list_label', function (Blueprint $table) {
            $table->foreign('dynamic_list_id')->references('id')->on('dynamic_lists')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('label_id')->references('id')->on('labels')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('dynamic_list_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dynamic_list_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->string('type')->index();

            $table->timestamps();
        });

        Schema::table('dynamic_list_fields', function (Blueprint $table) {
            $table->foreign('dynamic_list_id')->references('id')->on('dynamic_lists')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('dynamic_list_post', function (Blueprint $table) {
            $table->unsignedInteger('dynamic_list_id')->index();
            $table->unsignedInteger('post_id')->index();
            $table->integer('order')->index();
        });

        Schema::table('dynamic_list_post', function (Blueprint $table) {
            $table->foreign('dynamic_list_id')->references('id')->on('dynamic_lists')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('widget_data', function (Blueprint $table) {
            $table->unsignedInteger('dynamic_list_id')->nullable()->index()->after('content_type_id');
        });

        Schema::table('widget_data', function (Blueprint $table) {
            $table->foreign('dynamic_list_id')->references('id')->on('dynamic_lists')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('widget_data', function (Blueprint $table) {
            $table->dropColumn('dynamic_list_id');
        });
        Schema::dropIfExists('dynamic_list_post');
        Schema::dropIfExists('dynamic_list_fields');
        Schema::dropIfExists('dynamic_list_label');
        Schema::dropIfExists('dynamic_lists');
    }
}
