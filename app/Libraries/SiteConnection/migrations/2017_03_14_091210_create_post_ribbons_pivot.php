<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostRibbonsPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_ribbon', function (Blueprint $table) {
            $table->integer('post_id')->unsigned();
            $table->integer('ribbon_id')->unsigned();
            $table->integer('dynamic_list_id')->unsigned();
        });

        Schema::table('post_ribbon', function (Blueprint $table) {
            $table->foreign('post_id')->references('id')->on('posts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('ribbon_id')->references('id')->on('ribbons')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('dynamic_list_id')->references('id')->on('widget_data')->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_ribbon_pivot');
    }
}
