<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddAbTestsFieldsToRow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_rows', function (Blueprint $table) {
            $table->text('ab_tests_action')->nullable()->after('active');
        });

        Schema::create('ab_test_row', function (Blueprint $table) {
            $table->integer('test_id')->unsigned();
            $table->integer('row_id')->unsigned();
        });

        Schema::table('ab_test_row', function (Blueprint $table) {
            $table->foreign('test_id')->references('id')->on('ab_tests')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('row_id')->references('id')->on('template_rows')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_rows', function (Blueprint $table) {
            $table->dropColumn('ab_tests_action');
        });

        Schema::dropIfExists('ab_test_row');
    }
}
