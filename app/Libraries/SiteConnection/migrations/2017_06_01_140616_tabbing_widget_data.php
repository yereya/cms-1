<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TabbingWidgetData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_widget_data', function (Blueprint $table) {
            $table->integer('group_widget_id')->unsigned();
            $table->integer('widget_data_id')->unsigned();
            $table->string('tab_name')->nullable();
            $table->integer('priority')->default(10);
            $table->boolean('by_default')->default(0);
        });

        Schema::table('group_widget_data', function (Blueprint $table) {
            $table->foreign('group_widget_id')->references('id')->on('widget_data')->onDelete('cascade');
            $table->foreign('widget_data_id')->references('id')->on('widget_data')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_widget_data', function (Blueprint $table) {
            $table->dropForeign('group_widget_data_group_widget_id_foreign');
            $table->dropForeign('group_widget_data_widget_data_id_foreign');
        });

        Schema::dropIfExists('group_widget_data');
    }
}
