<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('parent_id')->nullable()->unsigned();
            $table->integer('menu_id')->unsigned();
            $table->integer('post_id')->unsigned()->nullable();
            $table->string('title', 100)->nullable();
            $table->string('name', 100)->nullable();
            $table->string('class', 100)->nullable();
            $table->string('url', 255)->nullable();
            $table->enum('target', ['_self', '_blank', '_parent', '_top'])->default('_self');
            $table->string('custom_html', 512)->nullable();
            $table->string('attributes', 255)->nullable();
            $table->integer('priority')->nullable()->unsigned();
            $table->softDeletes();
        });

        Schema::table('menu_items', function (Blueprint $table) {
            $table->foreign('menu_id')->references('id')->on('menus');

            $table->foreign('post_id')->references('id')->on('pages');

            $table->foreign('parent_id')->references('id')
                ->on('menu_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menu_items');
    }
}
