<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ChangeTemplateBuilderItemsColumnsToElement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('template_row_column_items', function (Blueprint $table) {
            $table->renameColumn('item_id', 'element_id');
            $table->renameColumn('item_type', 'element_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('template_row_column_items', function (Blueprint $table) {
            $table->renameColumn('element_id', 'item_id');
            $table->renameColumn('element_type', 'item_type');
        });
    }
}
