<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRatingsAndSubjectToCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments',function (Blueprint $table){
            $table->dropColumn('rating');

            $table->string('subject');
            $table->integer('rating_overall')->unsigned()->nullable();
            $table->integer('rating_1')->unsigned()->nullable();
            $table->integer('rating_2')->unsigned()->nullable();
            $table->integer('rating_3')->unsigned()->nullable();
            $table->integer('rating_4')->unsigned()->nullable();
            $table->integer('rating_5')->unsigned()->nullable();
            $table->integer('rating_6')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments',function (Blueprint $table){
            $table->integer('rating')->unsigned()->nullable();

            $table->dropColumn('subject');
            $table->dropColumn('rating_overall');
            $table->dropColumn('rating_1');
            $table->dropColumn('rating_2');
            $table->dropColumn('rating_3');
            $table->dropColumn('rating_4');
            $table->dropColumn('rating_5');
            $table->dropColumn('rating_6');
        });
    }
}
