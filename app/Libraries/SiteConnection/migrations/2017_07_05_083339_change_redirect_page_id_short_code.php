<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeRedirectPageIdShortCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pages', function (Blueprint $table) {
           $table->dropColumn('page_redirect_id');
           $table->string('redirect_page_short_code')->after('url_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('redirect_page_short_code');
            $table->string('page_redirect_id')->after('url_type')->nullable();
        });
    }
}
