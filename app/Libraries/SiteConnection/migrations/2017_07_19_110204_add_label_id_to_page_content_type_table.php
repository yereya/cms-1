<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLabelIdToPageContentTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('page_content_type',function (Blueprint $table){
            $table->integer('label_id')->nullable()->unsigned();
            $table->foreign('label_id')->references('id')
                ->on('labels')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('page_content_type', function (Blueprint $table) {
            $table->dropColumn('label_id');
        });
    }
}
