<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTablePostRating extends Migration
{
    /**
     * Create Post Rating
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Create Post Rating Class
         */
        Schema::create('post_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->integer('rate_1')->default(0);
            $table->integer('rate_2')->default(0);
            $table->integer('rate_3')->default(0);
            $table->integer('rate_4')->default(0);
            $table->integer('rate_5')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });

        /**
         * Create foreign key To post_id
         */
        Schema::table('post_ratings', function (Blueprint $table) {
            $table->foreign('post_id')
                ->references('id')
                ->on('posts')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_ratings');
    }
}
