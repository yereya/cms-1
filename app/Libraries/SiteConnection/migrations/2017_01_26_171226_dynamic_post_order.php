<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class DynamicPostOrder extends Migration
{
    /**
     * Up table
     */
    public function up()
    {
        Schema::create('widget_post_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('widget_data_id')->unsigned();
            $table->integer('content_type_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->integer('order');
        });
    }

    /**
     * Rollback table
     */
    public function down()
    {
        Schema::drop('widget_post_order');
    }
}
