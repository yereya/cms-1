<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUniqunessToContentTypeLinkFields extends Migration
{
    /**
     * Run the migrations.
     * Add new
     *
     * @return void
     */
    public function up()
    {
        Schema::table('link_content_type', function (Blueprint $table) {
            $table->unique(['post_id', 'field_id', 'connect_to_post_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('link_content_type', function (Blueprint $table) {
            $table->dropUnique(['post_id', 'field_id', 'connect_to_post_id']);
        });
    }
}
