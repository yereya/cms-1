<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class RemoveTableDynamicListFieldsAndAddDynamicListOrdersFieldAndDynamicListFilterFields
 */
class RemoveTableDynamicListFieldsAndAddDynamicListOrdersFieldAndDynamicListFilterFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('dynamic_list_fields');

        Schema::create('dynamic_list_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dynamic_list_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->string('type')->index();
            $table->string('direction')->index();

            $table->timestamps();
        });

        Schema::table('dynamic_list_orders', function (Blueprint $table) {
            $table->foreign('dynamic_list_id')->references('id')->on('dynamic_lists')->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::create('dynamic_list_filters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dynamic_list_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->string('type')->index();
            $table->string('operator')->index();
            $table->string('value')->index();

            $table->timestamps();
        });

        Schema::table('dynamic_list_filters', function (Blueprint $table) {
            $table->foreign('dynamic_list_id')->references('id')->on('dynamic_lists')->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_list_filters');
        Schema::dropIfExists('dynamic_list_orders');

        Schema::create('dynamic_list_fields', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('dynamic_list_id')->index();
            $table->unsignedInteger('field_id')->index();
            $table->string('type')->index();

            $table->timestamps();
        });

        Schema::table('dynamic_list_fields', function (Blueprint $table) {
            $table->foreign('dynamic_list_id')->references('id')->on('dynamic_lists')->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }
}
