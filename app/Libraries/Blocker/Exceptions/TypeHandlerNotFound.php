<?php namespace App\Libraries\Blocker\Exceptions;

/**
 * Class TypeHandlerNotFound
 *
 * @package App\Libraries\Blocker\Exceptions
 */
class TypeHandlerNotFound extends \Exception
{

}