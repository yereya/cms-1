<?php namespace App\Libraries\Blocker;

use App\Entities\Models\Bo\BlockedIp;
use App\Entities\Models\Bo\BlockQuery;
use App\Libraries\Blocker\Contracts\TypeHandler;
use App\Libraries\Blocker\Exceptions\TypeHandlerNotFound;
use App\Libraries\Blocker\TypeHandlers\Ip\Extract\BlockedIps;
use App\Libraries\Blocker\TypeHandlers\Ip\Block\Handler;
use App\Libraries\TpLogger;
use DB;
use Illuminate\Support\Collection;

/**
 * Class Blocker
 *
 * @package App\Libraries\Blocker
 */
class Blocker
{
    /**
     * @var string $connection
     */
    protected $connection = 'bo';
    
    protected $break_blocker = false;
    
    /**
     * Blocker constructor.
     * @param null $logger
     */
    public function __construct($logger = null)
    {
        if (!TpLogger::isActive()) {
            $this->logger = new TpLogger(21);
            $this->logger->info('Started running the blocker');
        } else {
            $this->logger = TpLogger::getInstance();
        }
    }

    /**
     * Refresh
     *
     * @param string|null $account_id
     * @param string|null $ip
     */
    public function refresh($account_id = null, $ip = null)
    {
        $this->refreshIpBlock($this->getRefreshIpBlockIps($account_id, $ip));
    }

    /**
     * ReleaseIps
     *
     * Release all ips with status blocked from google and update status in database
     *
     * @param null $id
     * @param null $account_id
     * @param null $remove_ips
     */
    public function releaseIps($id = null, $account_id = null, $remove_ips = null)
    {
        $blockQueries = $this->getBlockQueries($id);
        $blockedIps = new BlockedIps($blockQueries, $account_id);
        $blockedIps->run($remove_ips);
    }

    /**
     * Refresh IP Block
     *
     * @param Collection $ips
     */
    private function refreshIpBlock(Collection $ips)
    {
        foreach ($ips as $account => $results) {
            $handler = new Handler($results, $account, 0, $this->break_blocker);
            $handler->refresh();
        }
    }

    /**
     * Get Refresh Ip Block Ips
     *
     * @param string|null $account_id
     * @param string|null $ip
     *
     * @return Collection
     */
    private function getRefreshIpBlockIps($account_id, $ip)
    {
        if ($account_id && $ip) {
            return new Collection(BlockedIp::where(['ip' => $ip, 'account_id' => $account_id])->get(['ip', 'account_id'])->groupBy('account_id'));
        }

        return $this->getRefreshIpBlockAccountIps();
    }

    /**
     * Get Refresh IP Block Account Ips
     *
     * @return Collection
     */
    private function getRefreshIpBlockAccountIps()
    {
        return BlockedIp::whereStatus('blocked')->get(['ip', 'account_id'])->groupBy('account_id');
    }

    /**
     * Run
     *
     * @param int|null $id
     */
    public function run($id = null)
    {
        $blockQueries = $this->getBlockQueries($id);
        foreach ($blockQueries as $blockQuery) {
            try {
                $this->process($blockQuery);

                $this->logger->debug(['Finished update ips to account: ' . $blockQuery->account->source_account_id . ' successful.']);
            } catch (\Exception $exception) {
                $this->logger->error($exception);
            }

            if ($this->break_blocker) {
                break;
            }
        }
    }

    /**
     * Get Block Queries
     *
     * @param int|null $id
     *
     * @return Collection
     */
    private function getBlockQueries($id = null)
    {
        $queries = $id ? BlockQuery::whereId($id)->get() : BlockQuery::whereStatus(1)->get();
        $this->logger->info('Found ' . $queries->count() . ' Queries');

        return $queries;
    }

    /**
     * Process
     *
     * @param BlockQuery $blockQuery
     *
     * @throws TypeHandlerNotFound
     */
    public function process(BlockQuery $blockQuery)
    {
        $this->logger->info('Starting working on query: ' . $blockQuery->name . ' [' . $blockQuery->id . ']' . ' type: ' . $blockQuery->type);

        $typeHandler = $this->getTypeHandler($blockQuery);
        /*
         * if found new ips
         */
        if ($typeHandler) {
            $typeHandler->verify();
            $typeHandler->execute();
            if ($typeHandler->getErrors()) {
                $this->logger->warning($typeHandler->getErrors());
            }

            $typeHandler->store();
            $this->break_blocker = $typeHandler->getBreakBlocker();
        }

        $blockQuery->touchLastRun();
    }

    /**
     * Get Type Handler
     *
     * @param BlockQuery $blockQuery
     *
     * @return TypeHandler
     * @throws TypeHandlerNotFound
     */
    private function getTypeHandler(BlockQuery $blockQuery)
    {
        $handler = $this->getTypeHandlerName($blockQuery);

        if (class_exists($handler)) {
            $queryResults = $this->getQueryResults($blockQuery->query);

            if (!is_null($queryResults) && $queryResults->count() > 0) {
                $this->logger->info(['Found ' . $queryResults->count(), 'handler: ' . $handler, 'account_id: ' . $blockQuery->account->source_account_id]);
                return new $handler($queryResults, $blockQuery->account->source_account_id, $blockQuery->id, $this->break_blocker);
            } else {
                $this->logger->info('Not found ips to updated.');
            }

            return null;
        } else {
            $this->logger->error(['couldn\'t find ' . $handler . ' type handler', 'block query' => json_encode($blockQuery)]);
        }
    }

    /**
     * Get Type Handler Name
     *
     * @param BlockQuery $blockQuery
     *
     * @return string
     */
    private function getTypeHandlerName(BlockQuery $blockQuery)
    {
        $path = '';
        foreach (explode(' ', $blockQuery->type) as $item) {
            $path .= ucfirst(camel_case($item)) . '\\';
        }

        return __NAMESPACE__ . '\\TypeHandlers\\' . $path . 'Handler';
    }

    /**
     * Get Query Results
     *
     * @param string
     *
     * @return Collection
     */
    private function getQueryResults($query)
    {
        try {
            $results = new Collection(DB::connection($this->connection)->select(DB::raw($query)));

            return $results;
        } catch (\Exception $exception) {
            $this->logger->error($exception);
        }
    }
}