<?php namespace App\Libraries\Blocker\Contracts;

/**
 * Interface TypeHandler
 *
 * @package App\Libraries\Blocker\Contracts
 */
interface TypeHandler
{

    /**
     * Refresh
     * 
     * @return void
     */
    public function refresh();

    /**
     * Verify
     *
     * @return void
     */
    public function verify();

    /**
     * Store
     *
     * @return void
     */
    public function store();

    /**
     * Execute
     *
     * @return void
     */
    public function execute();
    
}