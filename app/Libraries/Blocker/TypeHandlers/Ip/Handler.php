<?php namespace App\Libraries\Blocker\TypeHandlers\Ip;

use AdWordsUser;
use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\BlockedIp;
use App\Entities\Models\Bo\AccountCampaign;
use App\Libraries\Blocker\TypeHandlers\CampaignsBlockerTrait;
use App\Libraries\Blocker\TypeHandlers\Handler as BaseHandler;
use Illuminate\Support\Collection;
use League\Flysystem\Exception;
use Paging;
use Predicate;
use Selector;
use SoapFault;

/**
 * Class Handler
 *
 * @package App\Libraries\Blocker\TypeHandlers\IP
 */
abstract class Handler extends BaseHandler
{
    private $service = null;

    /**
     * Build Result Item
     *
     * @param Collection $results
     * @param string $ip
     * @return
     */
    abstract protected function buildResultItem(&$results, $ip);

    /**
     * Store
     *
     * @return void
     */
    abstract function store();

    /**
     * Build Operation
     *
     * @param           $campaign
     * @param BlockedIp $blockedIp
     *
     * @return \CampaignCriterionOperation
     */
    abstract protected function buildOperation($campaign, BlockedIp $blockedIp);

    /**
     * Refresh all account campaign
     */
    public function refresh()
    {
        $this->verify();
        $this->execute();
    }

    /**
     * Verify
     */
    public function verify()
    {
        foreach ($this->results->pluck('ip')->filter()->unique() as $row) {
            $this->buildResultItem($this->results, $row);
        }
        $ips = $this->results->pluck('ip')->filter()->unique()->toArray();
        $results = BlockedIp::whereIn('ip', $ips)->where('account_id', $this->account_id)->get();
        $this->results = $results->keyBy('id');
        $this->logger->info(['Verified results with unique IP', $this->results]);
    }

    /**
     * Execute
     */
    public function execute()
    {
        $user = new AdWordsUser(config_path() . '/adwords-auth.ini', null, null, $this->account_id);
        $this->service = $user->GetService('CampaignCriterionService');

        $operations = $this->getOperations();
        if ($operations) {
            $this->logger->info('built ' . count($operations) . ' operations to all campaigns');
            $this->executeServiceWithOperations($operations, $this->service);
        }
    }

    /**
     * Set errors
     *
     * @param $error
     */
    public function setErrors($error) {
        if (is_null($this->errors)) {
            $this->errors = new Collection();
        }

        $this->errors->put($this->errors->count(), $error);
    }

    /**
     * Get Operations
     *
     * @param null $results
     * @return array
     */
    private function getOperations($results = null)
    {
        $operations = [];
        $campaigns = $this->getAccountCampaigns();
        foreach ($campaigns as $campaign) {
            $operations[$campaign->id] = $this->getOperationsForCampaign($campaign, $results);
        }
        return $operations;
    }

    /**
     * Get Campaigns from db
     */
    private function getAccountCampaigns()
    {
        $account = Account::where('source_account_id', $this->account_id)->first();

        return AccountCampaign::select('campaign_id as id', 'campaign_name as name')
            ->where('account_id', $account->id)
            ->where('status', 'ENABLED')
            ->get();
    }

    /**
     * Get Operations For Campaign
     *
     * @param $campaign
     *
     * @param null $results
     * @return array
     */
    private function getOperationsForCampaign($campaign, $results = null)
    {
        $operations = [];

        $ips = is_null($results) ? $this->results : $results;

        foreach ($ips as $blockedIp) {
            $operations[] = $this->buildOperation($campaign, $blockedIp);
        }

        return $operations;
    }

    /**
     * Execute Service
     *
     * @param array $operations
     * @param       $service
     */
    private function executeServiceWithOperations($operations, $service)
    {
        $response = new Collection();
        foreach ($operations as $campaign_id => $chunk) {
            if (!$this->break_blocker) {
                $result = $this->executeService($service, $chunk);
                if ($result) {
                    $response = $response->merge($result);
                }
            } else {
                break;
            }
        }

        $this->response = $response;
    }

    /**
     * Execute Service
     *
     * @param $service
     * @param $chunk
     *
     * @return null|object
     */
    private function executeService($service, $chunk)
    {
        try {
            $response = $service->mutate($chunk)->value;
            return $response;
        } catch (SoapFault $exception) {
            $this->handleExecuteServiceSoapFaultException($service, $chunk, $exception);
            return null;
        }
    }

    /**
     * Handle Execute Service Soap Fault Exception
     *
     * @param $service
     * @param $chunk
     * @param $exception
     *
     * @return null|object
     */
    private function handleExecuteServiceSoapFaultException($service, $chunk, $exception)
    {
        $errors = $exception->detail->ApiExceptionFault->errors;
        $errors = is_array($errors) ? $errors : [$errors];

        foreach ($errors as $error) {
            $reason = $error->enc_value->reason;
            switch ($reason) {
                case 'EMPTY_LIST': //sending empty operations
                case 'REQUIRED': //operations without ip
                    break;
                case 'CAMPAIGN_LIMIT':
                    //campaign have more over 500 ips in status blocked
                    //remove this ip from operations collection
                    return $this->errorCampaignLimit($errors, $error, $chunk, $service);
                case 'INVALID_ID':
                    //send old ip with old ip id
                    //get id from error
                    $this->errorInvalidId($error, $chunk);
                    break;
                case 'UNEXPECTED_INTERNAL_API_ERROR':
                case 'RATE_EXCEEDED':
                    //more over operations in minutes - max 5000 operations in minute
                    //end all operations - google limit of operations
                    $this->errorRateExceeded($error);
                    break;
                default:
                    //remove this ip from operations collection
                    $this->logger->debug(['exception - Other: ', $error]);
                    $this->saveAndRefreshError($chunk, $error);
                    break;
            }

            if ($this->break_blocker) {
                break;
            }
        }
    }

    /**
     * Stop this request
     *
     * @param $error
     */
    private function errorRateExceeded($error)
    {
        $this->setErrors($error);
        $this->break_blocker = true;
        $this->logger->error(['stop scraper by reason', $error->enc_value->reason]);
    }

    /**
     * Find ip id from error and refresh it
     *
     * @param $error
     * @param $chunk
     */
    private function errorInvalidId($error, $chunk)
    {
        $this->setErrors(['error INVALID_ID', $error->enc_value->trigger]);
    }

    /**
     * Remove this ip from operations collection
     *
     * @param $errors
     * @param $error
     * @param $chunk
     * @param $service
     * @return null|object
     */
    private function errorCampaignLimit($errors, $error, $chunk, $service)
    {
        $this->setErrors(['exception CAMPAIGN_LIMIT', $error->enc_value->trigger]);
        $this->saveAndRefreshError($chunk, $error);
        return $this->handleSoapFaultExceptionCampaignLimitError($service, $chunk, $errors);
    }

    /**
     * Remove operation from list
     *
     * @param $chunk
     * @param $error
     * @param bool $log
     */
    private function saveAndRefreshError($chunk, $error, $log = true)
    {
        if ($log) {
            $this->setErrors($error);
        }
        $this->storeError($chunk, $error);
    }

    /**
     * Handle Soap Fault Exception Campaign Limit Error
     *
     * @param $service
     * @param $chunk
     * @param $errors
     *
     * @return null|object
     */
    private function handleSoapFaultExceptionCampaignLimitError($service, $chunk, $errors)
    {
        return $this->executeService($service, array_slice($chunk, 0, count($chunk) - count($errors)));
    }

    /**
     * Store Error
     *
     * @param $chunk
     * @param $error
     */
    private function storeError($chunk, $error)
    {
        $item = $this->getBlockedIpByError($chunk, $error);
        if (is_object($item)) {
            $item->operation = json_encode($chunk[$this->extractOperandPosition($error)]);
            $item->error = json_encode($error);
            $item->save();
            $this->results->forget($item->id);

            $this->setErrors(['error stored for item', $item->id]);
        }
    }

    /**
     * Get Blocked Ip By Error
     *
     * @param $chunk
     * @param $error
     *
     * @return BlockedIp
     */
    private function getBlockedIpByError($chunk, $error)
    {
        try {
            $operation = $chunk[$this->extractOperandPosition($error)];

            return $this->results->filter(function ($item) use ($operation) {
                return $operation->operand->criterion->ipAddress == $item->ip;
            })->first();
        } catch (Exception $e) {
            $this->logger->warning(['error position not found', json_encode($error)]);
            return null;
        }
    }

    /**
     * Get Operation By Error
     *
     * @param $error
     *
     * @return position
     */
    private function extractOperandPosition($error)
    {
        return preg_replace('/[^0-9]/', '', $error->enc_value->fieldPath);
    }
}