<?php namespace App\Libraries\Blocker\TypeHandlers\Ip\Release;

use App\Entities\Models\Bo\BlockedIp;
use App\Libraries\Blocker\TypeHandlers\Ip\Handler as BaseHandler;
use Illuminate\Support\Collection;

/**
 * Class IpBlock
 *
 * @package App\Libraries\Blocker\TypeHandlers
 */
class Handler extends BaseHandler
{

    /**
     * Build Result Item
     *
     * @param Collection $results
     * @param string     $ip
     */
    protected function buildResultItem(&$results, $ip)
    {
        $results->push(BlockedIp::where(['ip' => $ip, 'account_id' => $this->account_id])->firstOrFail());
    }

    /**
     * Store
     */
    public function store()
    {
        foreach ($this->results as $blockedIp) {
            $blockedIp->status           = 'released';
            $blockedIp->source_id        = null;
            $blockedIp->release_query_id = $this->query_id;
            $blockedIp->save();
        }
    }

    /**
     * Build Operation
     *
     * @param           $campaign
     * @param BlockedIp $blockedIp
     *
     * @return \CampaignCriterionOperation
     */
    protected function buildOperation($campaign, BlockedIp $blockedIp)
    {
        $criterion             = new \NegativeCampaignCriterion();
        $criterion->campaignId = $campaign->id;
        $criterion->criterion  = new \IpBlock($blockedIp->ip, $blockedIp->source_id);

        return new \CampaignCriterionOperation($criterion, 'REMOVE');
    }
}