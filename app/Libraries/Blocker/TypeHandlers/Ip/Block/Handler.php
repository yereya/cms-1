<?php namespace App\Libraries\Blocker\TypeHandlers\Ip\Block;

use App\Entities\Models\Bo\BlockedIp;
use App\Libraries\Blocker\TypeHandlers\Ip\Handler as BaseHandler;
use Illuminate\Support\Collection;

/**
 * Class IpBlock
 *
 * @package App\Libraries\Blocker\TypeHandlers
 */
class Handler extends BaseHandler
{

    /**
     * Build Result Item
     *
     * @param Collection $results
     * @param string $ip
     */
    protected function buildResultItem(&$results, $ip)
    {
        $where = [
            'ip'         => $ip,
            'account_id' => $this->account_id,
        ];

        if (!$blockedIp = BlockedIp::where($where)->first()) {
            $blockedIp = BlockedIp::create(['block_query_id' => $this->query_id] + $where);
        }

        if ($blockedIp->error) {
            $blockedIp->error     = null;
            $blockedIp->operation = null;
            $blockedIp->save();
        }

        $results->push($blockedIp);
    }

    /**
     * Store
     */
    public function store()
    {
        foreach ($this->results as $blockedIp) {
            if (!$this->response->where('criterion.ipAddress', $blockedIp->ip . '/32')->first()) {
                continue;
            }
            $blockedIp->count++;
            $blockedIp->status    = 'blocked';
            $blockedIp->source_id = $this->getSourceId($blockedIp);
            $blockedIp->save();
        }
    }

    /**
     * Get Source Id
     *
     * @param BlockedIp $blockedIp
     *
     * @return string
     */
    private function getSourceId(BlockedIp $blockedIp)
    {
        return $this->response->where('criterion.ipAddress', $blockedIp->ip . '/32')->first()->criterion->id;
    }

    /**
     * Build Operation
     *
     * @param           $campaign
     * @param BlockedIp $blockedIp
     *
     * @return \CampaignCriterionOperation
     */
    protected function buildOperation($campaign, BlockedIp $blockedIp)
    {
        $criterion             = new \NegativeCampaignCriterion();
        $criterion->campaignId = $campaign->id;
        $criterion->criterion  = new \IpBlock($blockedIp->ip);

        return new \CampaignCriterionOperation($criterion, 'ADD');
    }
}