<?php namespace App\Libraries\Blocker\TypeHandlers\Ip\Extract;

use AdWordsConstants;
use AdWordsUser;
use App\Entities\Models\Bo\AccountCampaign;
use App\Entities\Models\Bo\BlockedIp;
use App\Entities\Models\Bo\BlockQuery;
use App\Libraries\TpLogger;
use CampaignCriterionOperation;
use DB;
use Illuminate\Database\Eloquent\Collection;
use IpBlock;
use NegativeCampaignCriterion;
use Paging;
use Predicate;
use Selector;

class BlockedIps
{
    //current query (array)
    protected $current_query = null;
    //all queries from blocker (collections)
    protected $blockQueries = null;
    //all queries (array)
    protected $queries = null;
    //AdWords service
    protected $service = null;
    //current logger
    protected $logger = null;
    //chunk size to insert
    protected $chunk_size = 2000;

    private $local_blocked_ips = [];

    private $account_id;

    /**
     * BlockedIps constructor.
     * @param Collection $blockQueries
     * @param null $account_id - account id in local db
     */
    public function __construct(Collection $blockQueries, $account_id = null)
    {
        $this->logger = TpLogger::getInstance();
        $this->blockQueries = is_null($account_id) ? $blockQueries : $this->getBlockQueriesByAccount($account_id);
        $this->queries = $this->getQueries();
    }

    /**
     * Run extract ips
     * @param $remove - release all
     */
    public function run($remove = false)
    {
        foreach ($this->queries as $query) {
            $this->current_query = $query;
            $this->service = $this->createService($query['source_id']);
            $all_ips = [];

            if (empty($this->account_id) || $this->account_id != $query['source_id']) {
                $this->insertLocalBlockedIps($query);
            }

            foreach ($query['campaigns'] as $campaign) {
                $selectors = $this->createSelectors($campaign);
                $current_campaign_ids = $this->fetchIps($selectors);

                if ($remove) {
                    $all_ips = array_merge($all_ips, $current_campaign_ids);
                    $this->removeAll($current_campaign_ids, $query, $campaign);
                } else {
                    $released_ips = $this->getReleased($current_campaign_ids);
                    $all_ips = array_merge($all_ips, $released_ips);
                    $this->removeAll($released_ips, $query, $campaign);

                    $remove_ips = $this->getNotLocal($current_campaign_ids, $released_ips);

                    $this->removeIps($remove_ips);
                }
            }

            //change status in local db
            $this->changeStatus($all_ips);
        }
    }

    /**
     * Init local ips with status blocked, for this account
     *
     * @param $query
     */
    private function insertLocalBlockedIps($query) {
        $this->account_id = $query['source_id'];
        $this->local_blocked_ips = BlockedIp::where('status', 'blocked')
            ->where('block_query_id', $query['query_id'])
            ->get()
            ->pluck('ip')
            ->toArray();
    }

    /**
     * Remove only released ips
     *
     * @param $current_campaign_ids
     * @param $query
     * @param $campaign
     */
    private function removeAll($current_campaign_ids, $query, $campaign)
    {
        $this->removeIps($current_campaign_ids);

        $this->logger->info(['account: ' . $query['source_id'] . ' campaign: ' . $campaign .
            ' found ' . count($current_campaign_ids) . ' and need remove ' . count($current_campaign_ids)]);
    }

    /**
     * Remove ip from adwords
     * remove by campaign
     *
     * @param $fetched
     */
    private function removeIps($fetched)
    {
        if (empty($fetched)) {
            return;
        }

        foreach (array_chunk($this->buildOperation($fetched), $this->chunk_size) as $operations) {
            try {
                $this->service->mutate($operations);
            } catch (SoapFault $exception) {
                $this->handleExecuteServiceSoapFaultException($exception);
            } catch (\Exception $exception) {
                $this->handleExecuteServiceSoapFaultException($exception);
            }
        }
    }

    /**
     * Store exception
     *
     * @param $exception
     */
    private function handleExecuteServiceSoapFaultException($exception)
    {
        $errors = $exception->detail->ApiExceptionFault->errors;
        $errors = is_array($errors) ? $errors : [$errors];

        foreach ($errors as $error) {
            $reason = $error->enc_value->reason;
            if ($reason != 'INVALID_ID' && $reason != 'CONCURRENT_MODIFICATION') {
                $this->logger->error([$error->enc_value->reason]);
            }
        }
    }

    /**
     * Build Operation
     *
     * @param $fetched
     * @return CampaignCriterionOperation
     */
    private function buildOperation($fetched)
    {
        $operations = [];
        foreach ($fetched as $item) {
            $criterion = new NegativeCampaignCriterion();
            $criterion->campaignId = $item['campaign'];
            $criterion->criterion = new IpBlock($item['ip'], $item['source_id']);

            $operations[] = new CampaignCriterionOperation($criterion, 'REMOVE');
        }

        return $operations;
    }

    /**
     * Found ips not included in local table
     *
     * @param $campaign_ips - ips from google
     * @param $released_ips - released ips
     *
     * @return mixed
     */
    private function getNotLocal($campaign_ips, $released_ips) {
        foreach ($campaign_ips as $key => $ip) {
            if (array_search($ip['ip'], $this->local_blocked_ips) !== false) {
                unset($campaign_ips[$key]);
            }
        }

        $ips = [];
        foreach ($released_ips as $result) {
            $ips[] = $result['ip'];
        }

        foreach ($campaign_ips as $key => $ip) {
            if (array_search($ip['ip'], $ips)) {
                unset($campaign_ips[$key]);
            }
        }

        return $campaign_ips;
    }

    /**
     * Get released ip from db
     *
     * @param $results
     * @return array
     */
    private function getReleased($results)
    {
        $released = [];
        $account_id = null;
        $ips = [];
        foreach ($results as $result) {
            $ips[] = $result['ip'];
            $account_id = $result['account_id'];
        }

        $locals = BlockedIp::whereIn('ip', array_values($ips))
            ->where('account_id', $account_id)
            ->where(function($query) {
                $query->where('status', 'released')
                    ->orWhere('status', 'pending');
            })
            ->get();

        foreach ($locals as $local) {
            $released[] = $results[array_search($local->ip, array_column($results, 'ip'))];
        }

        return $released;
    }

    /**
     * After remove form adwords change status to release in local db
     *
     * @param $ips
     */
    private function changeStatus($ips)
    {
        $released_ips = [];
        foreach ($ips as $ip) {
            if (!array_search($ip['ip'], $released_ips)) {
                $released_ips[] = $ip['ip'];
            }
        }

        if (!empty($released_ips)) {
            $models = BlockedIp::whereIn('ip', array_values($released_ips))
                ->where('account_id', $this->current_query['source_id'])
                ->where(function($query) {
                    $query->where('status', 'blocked')
                        ->orWhere('status', 'pending');
                })->get();

            BlockedIp::whereIn('id', $models->pluck('id'))->update(['status' => 'released']);
        }

        $this->logger->info('Status changed successful');
    }

    /**
     * Fetch all blocked ip
     *
     * @param $selector
     * @return array
     * [
     *  "account_id" => 'account source id'
     *  "ip" => "91.232.36.10"
     *  "source_id" => "35625424969", -> ip id
     *  "campaign" => 'campaign source id'
     * ],
     * [
     *  .....
     * ]
     */
    private function fetchIps($selector)
    {
        $data = [];
        do {
            $results = $this->service->get($selector);// Make the get request.
            if (isset($results->entries)) {
                foreach ($results->entries as $campaignCriterion) {
                    if ($campaignCriterion->criterion->type == 'IP_BLOCK') {
                        $ip = substr($campaignCriterion->criterion->ipAddress, 0, strpos($campaignCriterion->criterion->ipAddress, '/'));
                        $data[] = [
                            'ip' => $ip,
                            'source_id' => $campaignCriterion->criterion->id,
                            'account_id' => $this->current_query['source_id'],
                            'campaign' => $campaignCriterion->campaignId
                        ];
                    }
                }
            }
            $selector->paging->startIndex += AdWordsConstants::RECOMMENDED_PAGE_SIZE;
        } while ($results->totalNumEntries > $selector->paging->startIndex);

        return $data;
    }

    /**
     * Create new selector
     * to all campaign need create new selector
     *
     * @param $campaign
     * @return Selector
     */
    private function createSelectors($campaign)
    {
        $selector = new Selector(); //init selector
        $selector->fields = array('Id', 'CriteriaType', 'IpAddress'); //fields to get
        $selector->predicates[] = new Predicate('CampaignId', 'IN', array($campaign)); //where search
        $selector->predicates[] = new Predicate('CriteriaType', 'IN', array('IP_BLOCK')); //what return
        $selector->paging = new Paging(0, AdWordsConstants::RECOMMENDED_PAGE_SIZE); //max size to return

        return $selector;
    }

    /**
     * Create service
     *
     * @return \SoapClient
     */
    private function createService()
    {
        try {
            $user = new AdWordsUser(config_path() . '/adwords-auth.ini', null, null, $this->current_query['source_id']);
            return $user->GetService('CampaignCriterionService');
        } catch (\Exception $e) {
            $this->logger->error(['Not connection to adwords service, ' . $e->getMessage()]);
        }
    }

    /**
     * Set array with data to extract ips
     * @return array [
     *           "id" => 9
     *           "query_id" => 6
     *           "source_id" => "6641538240"
     *           "campaigns" => [
     *               254937171,
     *               254937171,
     *               ......
     *           ]
     *   ]
     */
    private function getQueries()
    {
        $queries = [];
        foreach ($this->blockQueries as $blockQuery) {
            if ($blockQuery->type == 'ip block') {
                $queries[] = [
                    'id' => $blockQuery->account->id,
                    'query_id' => $blockQuery->id,
                    'source_id' => $blockQuery->account->source_account_id,
                    'campaigns' => $this->getCampaign($blockQuery->account_id)
                ];
            }
        }

        return $queries;
    }

    /**
     * Get Queries by account id
     *
     * @param $account_id
     */
    private function getBlockQueriesByAccount($account_id)
    {
        return BlockQuery::where('type', 'ip block')->where('account_id', $account_id)
            ->where('status', true)->get();
    }

    /**
     * Method helper get all campaigns source id
     *
     * @param $account_id
     * @return mixed
     */
    private function getCampaign($account_id)
    {
        return AccountCampaign::select('campaign_id')
            ->where('account_id', $account_id)
            ->where('status', 'ENABLED')
            ->get()->pluck('campaign_id')->filter()->toArray();
    }
}