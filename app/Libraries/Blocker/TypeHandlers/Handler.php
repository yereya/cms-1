<?php namespace App\Libraries\Blocker\TypeHandlers;

use App\Libraries\Blocker\Contracts\TypeHandler;
use App\Libraries\TpLogger;
use Illuminate\Support\Collection;

/**
 * Class Handler
 *
 * @package App\Libraries\Blocker\TypeHandlers
 */
abstract class Handler implements TypeHandler
{

    /**
     * @var TpLogger $logger
     */
    protected $logger;

    /**
     * @var Collection $results
     */
    protected $results;

    /**
     * @var Collection $response
     */
    protected $response;

    /**
     * @var int $account_id
     */
    protected $account_id;

    /**
     * @var int $query_id
     */
    protected $query_id;

    /**
     * @var bool - value to stop run blocker 
     */
    protected $break_blocker = false;

    /**
     * @var null - error collections
     */
    protected $errors = null;

    /**
     * Handler constructor.
     *
     * @param Collection $results
     * @param $account_id
     * @param $query_id
     * @param $break_blocker
     */
    public function __construct(Collection $results, $account_id, $query_id, $break_blocker)
    {
        $this->logger     = TpLogger::getInstance();
        $this->results    = $results;
        $this->account_id = $account_id;
        $this->query_id   = $query_id;
        $this->break_blocker = $break_blocker;
        $this->errors = null;
    }

    /**
     * Return this break if true then stop run
     *
     * @return bool
     */
    public function getBreakBlocker()
    {
        return $this->break_blocker;
    }

    /**
     * @return Collection
     */
    public function getErrors()
    {
        return $this->errors;
    }
}