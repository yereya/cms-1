<?php namespace App\Libraries\TracksDataFlowMonitor;
use App\Entities\Models\Scheduler\Task;
use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Users\UserRepo;
use App\Libraries\TpLogger as tLog;
use DB;
use App\Libraries\Mail;

/**
 * Class TracksValidationAlert
 * @package App\Libraries\TracksValidationAlert
 */
class TracksDataFlowMonitor
{

    private static $system_id = 101;

    private static $outMigrationScheduledTaskId = 142;

    /**
     * @var TpLogger
     */
    private static $logger;

    /**
     * @var \Illuminate\Database\Connection
     */
    private static $dwh_connection;

    /**
     * @var int the past time to check for data insertion in dwh (in minutes)
     */
    private static $defaultPastTimeIntervalToCheck = 10;

    /**
     * Fetch
     *
     * @param array $atts
     *
     * @return mixed
     * @internal param $email
     */

    public static function fetch($atts = [])
    {
        $atts = filterAndSetParams([
            'email'               => false,
            'email_by_permission' => false,
            'time_interval' => false
        ], $atts);
        
        static::$logger = tLog::getInstance();
        static::$logger->setSystem(static::$system_id);

        $pastTimeIntervalToCheck = static::$defaultPastTimeIntervalToCheck;
        if ($atts['time_interval']) {
            $pastTimeIntervalToCheck = $atts['time_interval'];
        }

        try {

            $OutToTracksTask = Task::find(static::$outMigrationScheduledTaskId );
            $isTracksMigrationActive = $OutToTracksTask->active;;
            $dateTimeToCheck = date('Y-m-d H:i:s', strtotime("-" . $pastTimeIntervalToCheck . " minutes"));


            $newlyInsertedTracks = null;
            if ($isTracksMigrationActive) {
                static::$dwh_connection = DB::connection('dwh');
                $date_format = 'Y-m-d H:i:s';
                // Perform the query to retrieve DWH fact tracks from the last 24 hours
                $newlyInsertedTracks = static::$dwh_connection->table('dwh_fact_tracks')->select('track_id')
                    ->where('updated_at', '>', $dateTimeToCheck)->get()->pluck('track_id')->toArray();
            }
            else {
                static::$logger->info('Out Migration is Inactive. no data flow alert will be sent.');
                return;
            }

            if (!empty($newlyInsertedTracks)) {
                static::$logger->info('Found newly inserted tracks to dwh. no data flow alert will be sent.');
                return;
            }

            $body_str = "No tracks where inserted to DWH.dwh_fact_tracks in the past " . $pastTimeIntervalToCheck . " minutes.";
            $subject = "Tracks Dataflow is Down";
            // Begin mailing process
            self::alertMailToRecipients($subject, $body_str, $atts);

        } catch(\Exception $e) {
            static::$logger->error(['Could not check if Tracks migration is working. Reason: ', $e->getMessage()]);
            self::$dwh_connection->disconnect();
        }
    }


    /**
     * Sends the ids difference results to the mail recipients
     *
     * @param $message
     * @param $atts
     */
    private static function alertMailToRecipients($subject, $message, $atts)
    {
        // Check if one of the email options is set
        if ($message && ($atts['email_by_permission'] || $atts['email'])) {

            // Check if option email_by_permission is set and there's records
            if ($atts['email_by_permission']) {
                // build collection from the results
                $collection_result = collect($message);
                $users_repo        = (new UserRepo());
                $account_repo      = (new AccountsRepo());
                $variable_to_field = [
                    'account_names'    => 'account_name',
                    'advertiser_names' => 'advertiser_name'
                ];

                /*Build variables dynamic */
                foreach ($variable_to_field as $variable => $field) {
                    ${$variable} = $collection_result->filter(function ($record) use ($field) {
                        if (array_key_exists($field, $record)) {
                            return $record[$field];
                        }

                        return false;
                    });
                }

                $account_names = null;
                /*If account names exist in the results*/
                if (count($account_names)) {
                    $accounts_id = $account_repo->getByName($account_names)->pluck('id');
                }

                /*find users that have these accounts and get there mails*/
                $emails = $users_repo->getEmailsByAccountsId($accounts_id);

                /*Transform email collection to string*/
                $string_emails = $emails->implode(',');

                /*if --email passed concat it to the permitted emails*/
                if ($atts['email']) {
                    $string_emails .= ',' . $atts['email'];
                }

                self::email($subject, $message, $string_emails);

            } else {

                // Email the results when an email is passed
                self::email($subject, $message, $atts['email']);
            }
        }
    }

    /**
     * email
     *
     * @param $message
     * @param $email
     */
    private static function email($subject, $message, $email)
    {
        Mail::tplBlank($email, [
            'content' => $message,
            'title'   => $subject
        ]);
    }

    /**
     * Validate Query Result
     *
     * @param $query_id
     * @param $query
     *
     * @return bool
     */
    private static function validateQueryResult($query_id, $query)
    {


        /** @var array $countable_query */
        if (!static::resultsExist($query)) {
            static::$logger->error('Query id ' . $query_id . ' could not be found');
            return false;
        }

        if (isset($query['query']) && !$query['query']) {
            static::$logger->error('Query id ' . $query_id . ' does not have valid query');
            return false;
        }

        return true;
    }

    /**
     * @param $query
     *
     * @return int
     */
    private static function resultsExist($query)
    {
        $countable_query = [];

        if (is_object($query)) {
            $countable_query = $query->toArray();
        }

        if (is_array($query)) {
            $countable_query = $query;
        }

        return count($countable_query);
    }
}