<?php namespace App\Libraries\PriceComparison;

use App\Libraries\TpLogger;

class PriceComparison
{
    private $emails;
    private $sheet_name;
    /**
     * LogMigrations constructor.
     */
    public function __construct()
    {

    }

    /**
     * Run partners alerts script
     */
    public function run($emails, $sheet_name)
    {
        $this->emails = $emails;
        $this->sheet_name = $sheet_name;
        $output = $this->startProcess();
        return $output;
    }

    private function startProcess()
    {
        $logger = TpLogger::getInstance();
        $logger->info('Starting Price Comparison Process');
        exec('cd ' . __DIR__ . "/Client" . " && node index.js " . $this->emails . ' ' . $this->sheet_name, $out);
        $logger->info($out);

        return $out;
    }

}

