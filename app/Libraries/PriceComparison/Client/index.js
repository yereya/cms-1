const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

const nodemailer = require('nodemailer');

const request = require('request-promise');
const cheerio = require('cheerio');
// npm install request --save
// npm install request-promise --save

// If modifying these scopes, delete token.json.
const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
// The file token.json stores the user's access and refresh tokens, and is
// created automatically when the authorization flow completes for the first
// time.
const TOKEN_PATH = 'token.json';

//Read Argumens for email
var emails = process.argv[2] || 'antonr@trafficpoint.io';
var sheet_name = process.argv[3] || 'Test';
// var sheet_name = process.argv[3] || 'Mat';

// Set email client
var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'affiliates@trafficpoint.io',
    pass: 'cHwHHS8n1'
  }
});
var mailOptions = {
  from: 'affiliates@trafficpoint.io',
  to: emails,
  subject: 'Mattresses Disparity',
  text: ''
};
const spreadsheet_id = '1hKOc-2HHWOLSmdeZ5m2sHDSYAVhZlXyPlGOZTgTtnLI';

// --- Logic, listMajor is the main function ---

// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then call the Google Sheets API.
  authorize(JSON.parse(content), listMajors);   // listMajors - Main function
});


/**
 * Create an OAuth2 client with the given credentials, and then execute the
 * given callback function.
 * @param {Object} credentials The authorization client credentials.
 * @param {function} callback The callback to call with the authorized client.
 */
function authorize(credentials, callback) {
  const {client_secret, client_id, redirect_uris} = credentials.installed;
  const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

  // Check if we have previously stored a token.
  fs.readFile(TOKEN_PATH, (err, token) => {
    if (err) return getNewToken(oAuth2Client, callback);
    oAuth2Client.setCredentials(JSON.parse(token));
    callback(oAuth2Client);
  });
}

/**
 * Get and store new token after prompting for user authorization, and then
 * execute the given callback with the authorized OAuth2 client.
 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
 * @param {getEventsCallback} callback The callback for the authorized client.
 */
function getNewToken(oAuth2Client, callback) {
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
  });
  console.log('Authorize this app by visiting this url:', authUrl);
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
    code = '4/zQEk3eKjh-SAuJEJPT0KoHIohap8YZsADeoTMHunhDSV-yODYux2Ei8';
    oAuth2Client.getToken(code, (err, token) => {
      if (err) return console.error('Error while trying to retrieve access token', err);
      oAuth2Client.setCredentials(token);
      // Store the token to disk for later program executions
      fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
        if (err) return console.error(err);
        console.log('Token stored to', TOKEN_PATH);
      });
      callback(oAuth2Client);
    });
}
function sendEmail(){
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
          console.log(error);
        } else {
          console.log('Email sent: ' + info.response);
        }
      });
}
function GenerateEmail(prices){
  var body = '';
  var errorMsg = '';
  prices.forEach(function (brand, index) {
    if(!brand['price']){ // undefined price
      errorMsg = errorMsg.concat(brand['brand'],' , price undefined -  ',brand['url'] ,'\n') ;
    }else if(brand['price'] != brand['wpPrice']){
      body = body.concat('For brand : ', brand['brand'], ' , wp_price : ', brand['wpPrice'], " , site's price : ",brand['price'], '\n' );
    }
  });
  console.log(body);
  console.log(errorMsg);
  if(body || errorMsg){
    mailOptions['text'] = "Disparity : \n\n".concat(body,'\n\n','Errors : \n\n',errorMsg);
    sendEmail();
  }
}
/**
 * Prints the names and majors of students in a sample spreadsheet:
 * @see https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
 * @param {google.auth.OAuth2} auth The authenticated Google OAuth client.
 */
function listMajors(auth) {
  var prices_list = [];
  const sheets = google.sheets({version: 'v4', auth});
  sheets.spreadsheets.values.get({
    // spreadsheetId: '1AJjwJRqqsF7PKjR3fpkvfqK760OpCkQXREiBdwNf8ow',
    spreadsheetId: spreadsheet_id,

    // range: 'Mat',
    range: sheet_name,
  }, async(err, res) => {
    if (err) return console.log('The API returned an error: ' + err);
    var rows = res.data.values;
    if (rows.length) {
      const headers = rows[0];
      rows = rows.slice(1, rows.length);
      console.log('Name, Major:');

      // (async () => {
        try{
          // const browser = await puppeteer.launch({args: ['--no-sandbox']});
          // const page = await browser.newPage();
          for (let index = 0; index < rows.length; index++) {
            try{
              var merged_row = headers.reduce((obj, key, i) => ({ ...obj, [key]: rows[index][i] }), {});
              var url = merged_row['Url'];
              var selector = merged_row['Selector'];
              var input_headers = merged_row['Headers'];
              var json_col = merged_row['Json'];
              var wp_price = merged_row['WP Price'];
              var brand =  merged_row['Brand'];
              var regex =  merged_row['Regex'];
              var special =  merged_row['Special'];
              
              if(!brand){
                continue;
              }
            var final_h = {
              'Accept': '*/*',
              'Accept-Language': 'en-US,en;q=0.9',
              'Cache-Control': 'max-age=0',
              'Connection': 'keep-alive',
              'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'
            }
            if(input_headers){
              var input_headers = input_headers.split(",");
              input_headers.forEach(function(el){
                arr = el.split(':');
                final_h[arr[0].replace("\"","")] = arr[1].replace("\"","");
              })
            }

            const response = await request({
              uri: url,
              headers: final_h,
              gzip: true
          }
          );
          var price;
            if(selector){
              let $ = cheerio.load(response);
              price =  Math.floor($(selector).text().trim().replace(/£|\$|,/g,''));
            }else if(json_col){
              price = eval("JSON.parse(response)." + json_col)
            }else if(special){
              let $ = cheerio.load(response);
              switch(brand){                
                case 'Puffy':
                  price =  Math.floor($(special).data().text.trim().replace(/£|\$|,/g,''));
                  break;
                case 'Brooklyn bedding':
                  price =  Math.floor($(special).last().text().trim().replace(/£|\$|,/g,''));
                  break;
              }
            }else{
              throw "Price not found";
            }

            if(regex){
              regex = new RegExp(regex);
              price = String(price).replace(regex,'');
            }        
            console.log(brand + ' : ' + price);
              if(price)
                price_obj = {'index' : index,'url': url,'price' : price, 'wpPrice':wp_price, 'brand':brand};
              else
                price_obj = {'index' : index,'url': url,'price' : 0, 'wpPrice':wp_price, 'brand':brand};

              prices_list.push(price_obj);

              updateVal(price_obj);

            }catch(err){
              price_obj = {'index' : index,'url': url,'price' : null, 'wpPrice':wp_price, 'brand':brand};
              prices_list.push(price_obj);
              console.log('\n Error inside go-to loop, brand name : ' + brand +' , err : \n' + err)
            }
          }
          // await browser.close();
          GenerateEmail(prices_list);
        }catch(err){
          console.log('\n Inside async function for puppeteer, err : \n' + err)
          process.exit()
        }
      // })();
    } else {
      console.log('No data found.');
    }
  });

  function updateVal(price_obj){
    try{
        if (price_obj['price'] || price_obj['price'] === 0){
          var cell_idx = price_obj['index'] + 2;
          var range = sheet_name + '!G' + cell_idx;
          const body = {
              "range": range ,
              "majorDimension": 'ROWS',
              "values": [
                [price_obj['price']]
              ]
            };

          sheets.spreadsheets.values.update({
                  spreadsheetId: spreadsheet_id,
                  range: range,
                  valueInputOption: 'RAW',
                  resource: body
               }).then((response) => {
                 var result = response.result;
               });
        }
     }catch(err){
         console.log('Error in updateVal function \n' + err);
     }
  }
}
