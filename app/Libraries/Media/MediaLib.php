<?php namespace App\Libraries\Media;

use App\Entities\Models\Sites\Media;
use App\Entities\Models\Sites\Site;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;

/**
 * Class MediaLib
 * @package App\Libraries\Media
 */
class MediaLib
{
    /**
     * @var array
     */
    private $upload = [];

    /**
     * handle uploaded media file
     *
     * @param UploadedFile $file
     * @param Site $site
     * @return array
     */
    public function handle(UploadedFile $file, Site $site)
    {
        // validate the file has been uploaded successfully
        if (!$file->isValid()) {
            return false;
        }

        // build the media upload array with all its relevant data
        $this->upload = [
            'file' => $file,
            'site' => $site,
            'mime' => $file->getMimeType(),
            'size' => round($file->getSize() / 1024, 2),
            'directory' => '/' . config('media.directory') . '/' . $site->id . '/',
        ];

        $this->upload['filename'] = $this->resolveFileName();
        $this->upload['url'] = asset($this->upload['directory'] . $this->upload['filename']);

        // move file into directory
        $file->move(public_path() . $this->upload['directory'], $this->upload['filename']);


        // if image, get dimensions
        if (strpos($this->upload['mime'], 'image') == 0) {
            $this->upload = array_merge($this->upload, $this->getDimensions());
        }

        return $this->upload;
    }

    /**
     * Generate media file name
     *
     * Create an acceptable filename out of the uploaded file
     * if the same file name was already uploaded in
     * the past then add an epoch suffix
     *
     * @return mixed|string
     */
    private function resolveFileName()
    {
        // sanitize the name
        $filename = sanitizeFileName($this->upload['file']->getClientOriginalName());

        // if filename already exists, add timestamp to its name
        if (file_exists(public_path() . $this->upload['directory'] . $filename)) {
            // add the epoch time just between the filename and the .extension
            return substr_replace($filename, '_' . time(), strrpos($filename, '.'), 0);
        }

        return $filename;

    }

    /**
     * Get media file dimensions
     *
     * @return array
     */
    private function getDimensions()
    {
        list($width, $height) = getimagesize(public_path() . $this->upload['directory'] . $this->upload['filename']);
        return [
            'width' => $width,
            'height' => $height
        ];
    }

    /**
     * Delete media file
     *
     * @param Media $media
     * @param $site_id
     *
     * @return mixed
     */
    public function destroy(Media $media, $site_id)
    {
        $path = public_path() . '/' . config('media.directory') . '/' . $site_id . '/' . $media->filename;
        if (File::exists($path) && File::delete($path)) {
            return $media->delete();
        }
    }

    /**
     * Delete file only from local storage
     *
     * @param Media $media
     * @param $site_id
     */
    public function removeFile(Media $media, $site_id)
    {
        $path = public_path() . '/' . config('media.directory') . '/' . $site_id . '/' . $media->filename;
        if (File::exists($path)) {
            File::delete($path);
        }
    }

}