<?php
/**
 * Created by PhpStorm.
 * User: azusdex
 * Date: 16/01/2017
 * Time: 15:07
 */

namespace App\Libraries\Media;


use App\Libraries\Guzzle;

class MediaUploader
{
    protected $client;
    protected $action;

    const MEDIA_TYPE_FILE = 'file';
    const MEDIA_TYPE_CSS = 'css';

    /**
     * MediaUploader constructor.
     *
     * @param        $domain
     * @param string $action
     */
    public function __construct($domain, $action = 'upload')
    {
        $this->client = new Guzzle();
        $this->action = $domain . DIRECTORY_SEPARATOR . $action;

        /**
         * The following check was added, in order to keep support for the http protocol
         * Mainly used for development.
         */
        if (env('SSL',false)){
            $this->action = 'https://'.$this->action;
        }
    }

    /**
     * Upload file
     *
     * @param      $paths - array full file path
     * @param      $type  - media or css
     * @param      $site_id
     * @param null $options
     *
     * @return $this
     * @throws \Exception
     */
    public function uploadFile($paths, $type, $site_id, $options = null) {
        $url = $this->action . DIRECTORY_SEPARATOR . $type;

        /** if this os WIN convert in all paths separators to Bill Gates */
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $url = str_replace('\\', '/', $this->action . DIRECTORY_SEPARATOR . $type);
            $temp_paths = [];
            foreach ($paths as $path) {
                $temp_paths[] = str_replace('\\', '/', $path);
            }

            $paths = $temp_paths;
        }

        $this->client->fetch($url, [
            'form_params' => [
                'site' => $site_id,
                'paths' => $paths,
                'options' => $options
            ]
        ], 'POST');

        if ($this->client->getErrorMsgs()) {
            return json_decode($this->client->getErrorMsgs());
        } else {
            return json_decode($this->client->getBody());
        }
    }

    /**
     * @param $media_id
     * @param $action
     * @param $cms_url
     *
     * @return mixed|string
     */
    public function upload($media_id, $action, $cms_url) {
        $this->client->fetch($this->action, [
            'form_params' => [
                'media_id' => $media_id,
                'action' => $action,
                'cms_url' => $cms_url
            ]
        ], 'POST');

        if ($this->client->getErrorMsgs()) {
            return json_encode($this->client->getErrorMsgs());
        } else {
            return json_decode($this->client->getBody());
        }
    }
}