<?php

namespace App\Libraries\Media;


use App\Entities\Models\Sites\Site;

class FileInfo
{
    private $id;
    private $folder;
    private $path;
    private $thumb;
    private $filename;
    private $title;
    private $alt;
    private $metadata;
    private $width;
    private $height;
    private $size;
    private $mime;
    private $media_folder;
    private $site_id;
    private $site_name;

    protected $default_path;

    public function __construct(Site $site, $media_folder = null)
    {
        $this->default_path = public_path('top-assets' . DIRECTORY_SEPARATOR . snake_case($site->name));
        $this->media_folder = $media_folder ?? $this->default_path;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSiteId()
    {
        return $this->site_id;
    }

    /**
     * @param $site_id
     */
    public function setSiteId($site_id)
    {
        $this->site_id = $site_id;
    }

    /**
     * @return mixed
     */
    public function getSiteName()
    {
        return $this->site_name;
    }

    /**
     * @param $site_name
     */
    public function setSiteName($site_name) {
        $this->site_name = snake_case($site_name);
    }

    /**
     * @return mixed
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param mixed $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        if (!$this->path) {
            $this->setPath($this->getFolder() . DIRECTORY_SEPARATOR . $this->filename);
        }
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getThumb()
    {
        return $this->thumb;
    }

    /**
     * @param mixed $thumb
     */
    public function setThumb($thumb)
    {
        $this->thumb = $thumb;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getAlt()
    {
        if (is_null($this->alt) && $this->filename) {
            $this->setAlt(substr($this->filename, 0, strpos($this->getMime(), 'image')));
        }
        return $this->alt;
    }

    /**
     * @param mixed $alt
     */
    public function setAlt($alt)
    {
        $this->alt = $alt;
    }

    /**
     * @return mixed
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * @param mixed $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        if ($this->width) {
            return $this->width;
        } else if (strpos($this->getMime(), 'image') !== false && $this->getPath()) {
            $dim = getimagesize($this->media_folder . DIRECTORY_SEPARATOR . $this->getPath());
            $this->setWidth($dim[0]);

            return $this->width;
        } else {
            return 0;
        }
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        if ($this->height) {
            return $this->height;
        } else if (strpos($this->getMime(), 'image') !== false && $this->getPath()) {
            $dim = getimagesize($this->media_folder . DIRECTORY_SEPARATOR . $this->getPath());
            $this->setHeight($dim[1]);

            return $this->height;
        } else {
            return 0;
        }
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return ceil(filesize($this->media_folder . DIRECTORY_SEPARATOR . $this->path) / 1024);
    }

    /**
     * @return mixed
     */
    public function getMime()
    {
        return $this->mime;
    }

    /**
     * @param mixed $mime
     */
    public function setMime($mime)
    {
        $this->mime = $mime;
    }

    /**
     * @return array
     */
    public function out() :array {
        $time = sqlDateFormat();

        return [
            'folder' => $this->getFolder(),
            'site_id' => $this->getSiteId(),
            'site_name' => $this->getSiteName(),
            'path' => $this->getPath(),
            'thumb' => $this->getThumb(),
            'filename' => $this->getFilename(),
            'title' => $this->getTitle(),
            'alt' => $this->getAlt(),
            'metadata' => $this->getMetadata(),
            'size' => $this->getSize(),
            'mime' => $this->getMime(),
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
            'created_at' => $time,
            'updated_at' => $time
        ];
    }
}