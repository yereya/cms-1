<?php
namespace App\Libraries;


use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteNavigation;
use App\Entities\Models\Sites\SitesModel;
use App\Entities\Models\System;
use App\Facades\ShortCode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Mockery\Exception;

class NavigationLib
{
    /**
     * @var array
     */
    private static $navigation = [];


    public static function get()
    {
        // if in site context, show site navigation menu, else show cms
        $site = Route::getCurrentRoute()->getParameter('site');
        if (!is_object($site)) {
            $site = Site::find($site);
        }

        if (isset($site)) {
            ShortCode::setHaystack([
                'site_id'       => $site->id,
                'database_name' => $site->db_name
            ]);

            return self::buildSiteNavigation();
        }

        return self::buildCmsNavigation();
    }


    /**
     * Build CMS Navigation
     * build the cms navigation menu and all its nested children
     * @return mixed
     */
    private static function buildCmsNavigation()
    {
        self::$navigation = System::navigationTree()->toArray();

        // build the nested navigation tree
        foreach (self::$navigation as $key => &$nav) {

            if (!self::isNavItemPermitted($nav['permission_name'])) {
                unset(self::$navigation[$key]);
                continue;
            }

            self::decodeRouteParams($nav);
            if (count($nav['children'])) {
                foreach ($nav['children'] as $sub_key => &$sub_nav) {

                    if (!self::isNavItemPermitted($sub_nav['permission_name'])) {
                        unset($nav['children'][$sub_key]);
                        continue;
                    }

                    self::decodeRouteParams($sub_nav);
                }
            }
        }

        return self::$navigation;
    }

    /**
     * Build Site Navigation
     * build site navigation menu and all its nested children
     * @return array
     */
    private static function buildSiteNavigation()
    {
        self::$navigation = SiteNavigation::navigationTree()->toArray();

        // build the nested navigation tree
        foreach (self::$navigation as $key => &$nav) {

            if (!self::isNavItemPermitted($nav['permission_name'])) {
                unset(self::$navigation[$key]);
                continue;
            }

            self::decodeRouteParams($nav);
            self::addChildrenFromQuery($nav);

            if (count($nav['children'])) {
                foreach ($nav['children'] as $sub_key => &$sub_nav) {
                    if (!self::isNavItemPermitted($sub_nav['permission_name'])) {
                        unset($nav['children'][$sub_key]);
                        continue;
                    }

                    self::decodeRouteParams($sub_nav);
                }
            }
        }

        return self::$navigation;
    }

    /**
     * Add Children From Query
     *
     * @param $nav
     */
    private static function addChildrenFromQuery(&$nav)
    {
        if (!empty($nav['children_query'])) {
            $children_query = ShortCode::setShortCodes($nav['children_query']);
            $query_results = object2Array(DB::connection(SitesModel::DB_SITES_CONNECTION)->select($children_query));
            $query_results = self::capitalizeQueryResultsNames($query_results);
            self::mergeQueryResults($nav, $query_results);
        }
    }

    /**
     * Capitalize Query Result
     *
     * @param $query_results
     *
     * @return mixed
     */
    private static function capitalizeQueryResultsNames($query_results)
    {
        array_walk($query_results, function (&$item) {
            $item['name'] = ucfirst($item['name']);
        });

        return $query_results;
    }

    /**
     * Get From Children Query
     *
     * @param $nav
     * @param $query_results
     * @return mixed
     */
    private static function mergeQueryResults(&$nav, $query_results)
    {
        if (empty($nav['children'])) {
            $nav['children'] = $query_results;
        } else {
            $nav['children'] = array_merge($nav['children'], $query_results);
        }

        return $nav;
    }

    /**
     * Decode Route Params
     * converts route_params field from json to array
     *
     * @param $nav
     *
     * @return mixed
     */
    private static function decodeRouteParams(&$nav)
    {
        if ($nav['route_params'] && isJson($nav['route_params'])) {
            $route_params        = ShortCode::setShortCodes($nav['route_params']);
            $nav['route_params'] = json_decode($route_params, true);
        }

        return $nav;
    }

    /**
     * Is Navigation Item permitted To View
     *
     * @param String $permission
     *
     * @return bool
     */
    private static function isNavItemPermitted($permission):bool
    {
        if (isset($permission) && !auth()->user()->can($permission)) {
            return false;
        }

        return true;
    }
}