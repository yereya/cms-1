
"use strict";

const pingdomApi = require('pingdom-api')({
    user: process.env.PINGDOM_USER,
    pass: process.env.PINGDOM_PASS,
    appkey:  process.env.PINGDOM_APP_KEY
});
module.exports = class Pingdom {


    async getUsers(){
        try {
            return pingdomApi.getUsers();
        } catch (e) {
            console.log('Error occured during getting Users from pingdom API ', e);
        }
    }


    async getChecks(){
        try {
            return pingdomApi.getChecks();
        } catch (e) {
            console.log('Error occured during getting Checks from pingdom API ', e);
        }
    }


    async deleteChecks( ids ){
        try {
            return pingdomApi.removeChecks({form: {delcheckids: ids}});
        } catch (e) {
            console.log('Error occured during removing Checks from pingdom API ', e);
        }
    }


    /*
     * @param {string}  name                        Give a name to the monitoring instance
     * @param {string}  host                        host name to monitor
     * @param {string}  url                         Full url to monitor
     * @param {string}  type                        Choose a monitoring type http,tcp...
     * @param {string}  contactids                  Comma separated Integers of contact ids
     * @param {integer} sendnotificationwhendown    Notification when the site is down for more then x
     * @param {integer} resolution                  Check site every x min
     * @param {string}  region                      Choose one on the following EU/NA/APAC
     */
    async addCheck({
                       name,
                       host,
                       url,
                       type = 'http',
                       userids = "",
                       sendnotificationwhendown = 2,
                       resolution = 15,
                       region = null,
                       severity_level = 'low'
                   }){

        try {
            const probe_filters = region ? `region:${region}` : "";
            return pingdomApi.setChecks({
                form: {
                    name,
                    host,
                    type,
                    url,
                    userids,
                    resolution,
                    probe_filters,
                    sendnotificationwhendown,
                    severity_level
                }
            });
        } catch (e) {
            console.log('Error occured during adding Checks from pingdom API ', e);
        }
    }


}

