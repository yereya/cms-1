"use strict";

const url = require("url");
const mysql = require("mysql");

const dbOptions = {
    host: process.env.DB_BO_HOST,
    user: process.env.DB_BO_USER,
    password: process.env.DB_BO_PASS,
    database: process.env.DB_BO_NAME,
    port: process.env.DB_BO_PORT
};

console.log({dbOptions: dbOptions});

const connection = mysql.createConnection(dbOptions);

try {
    connection.connect();
} catch (e) {
    console.log('Error occured during connection to DB ', e);
}

module.exports = async function getPartners(){
    try {
        return new Promise((resolve, reject) => {
            connection.query(
                `SELECT *,
                            (epc*click_out_sum) AS landing_page_value
                     FROM (
                             SELECT bo_lps.name AS lp_name,
                                 bo.advertisers.name AS advertiser_name,
                                 advertiser_id,
                                 bo_lps.url,
                                 landingpage_id,
                                 SUM(click_in) AS click_in_sum,
                                 SUM(click_out) AS click_out_sum
                             FROM dwh.dwh_fact_tracks
                             INNER JOIN bo.out_landingpages AS bo_lps ON bo_lps.id = landingpage_id
                             INNER JOIN bo.advertisers ON bo.advertisers.id = dwh.dwh_fact_tracks.advertiser_id
                             WHERE stats_date_tz > DATE_SUB(NOW(),INTERVAL 2 DAY)
                             GROUP BY landingpage_id,
                                      bo.advertisers.name,
                                      advertiser_id,
                                      dwh.dwh_fact_tracks.landingpage_id,
                                      bo_lps.name
                           ) t
                     INNER JOIN (
                                    SELECT advertiser_id,
                                            ROUND(SUM(commission_amount)/SUM(click_out),1) AS epc
                                    FROM dwh.dash_tracks
                                    WHERE stats_date_tz>DATE_SUB(NOW(),INTERVAL 7 DAY)
                                    GROUP BY advertiser_id
                                 ) a ON t.advertiser_id=a.advertiser_id
                     WHERE t.click_out_sum >= 5 AND (epc*click_out_sum)>200`,

                (error, landing_pages, fields) => {
                    connection.end();

                    if (error) {
                        reject(error);
                        return console.log("Error", error);
                    }

                    landing_pages = landing_pages.map(({url, lp_name}) => {
                        return {
                            url: url.replace("?gclid={gclid}", "").replace("?utm_source=B", ""),
                            lp_name
                        };
                    });

                    resolve(Array.from(new Set(landing_pages)));
                }
            );
        });
    } catch (e) {
        console.log('Error occured during getting partners url from DB ', e);
    }
};


