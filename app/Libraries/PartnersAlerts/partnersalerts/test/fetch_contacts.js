'use strict'

require('dotenv').config({path: '../.env'})

const assert = require('assert')
const Pingdom = require('../Pingdom')

const pingdom = new Pingdom({
    user: process.env.PINGDOM_USER,
    pass: process.env.PINGDOM_PASS,
    app_key: process.env.PINGDOM_APP_KEY
})

describe('Pingdom', () => {
    describe('getContacts', () => {
        it('Should return a list of contact ids', done => {
            pingdom.getContacts().then( response => {
                let ids_str = response.contacts.map( c => c.id).join(',')
                if (typeof ids_str !== 'string'){
                    return done(new Error(`Invalid response: ${ids_str}`))
                }
                done()
            }).catch( error => done(error))
        })
    })
})

