
"use strict";

require("dotenv").config();
const url = require("url");
const Pingdom = require("./src/Pingdom");
const getPartners = require("./src/Partners");   // connecting to DB
const pingdom = new Pingdom();


async function start() {
    try {
        // const users = await pingdom.getUsers();
        // const userids = users.users.map(user => user.id).join(",");
        const userids = `${process.env.PINGDOM_NITZAN_ID}`;
        const partners = await getPartners();
        const partnersUrls = partners.map(data => data.url);
        const partnersDataByUrl = partners.reduce((obj, data) => {
            obj[data.url] = data.lp_name;
            return obj;
        }, {});

        const partnersHosts = partnersUrls.map(pUrl => url.parse(pUrl).hostname).filter(host => typeof host == "string");

        let response = await pingdom.getChecks();
        let checks = response[0];

        if (!Array.isArray(checks)) {
            checks = [];
        }

        // Remove hosts from Pingdom( except top5 | top- | playright | zantracker | lendstart | top) that are not in the partners hosts ( from DB)
        let checksToDelete = checks.filter(
            check =>
                !check.hostname.match("(top5)|(top-)|playright|zantracker|lendstart|topsitesderencontre|sonary") && partnersHosts.indexOf(check.hostname) === -1
        );

        let duplicates = [];
        let cache = {};

        for (let i = 0, len = checks.length ; i < len; i++) {
            if(cache[checks[i].hostname] === true){
                duplicates.push(checks[i]);
            }else{
                cache[checks[i].hostname] = true;
            }
        }
        // push duplicates into checksToDelete
        Array.prototype.push.apply(checksToDelete,duplicates);

        const hostsToAdd = partnersUrls.filter(
            partnerUrl => {
                return checks.filter( check => partnerUrl.includes(check.hostname) ).length == 0
            }
        );

        if (checksToDelete.length > 0) {
            console.log(`Deleting ${checksToDelete.length} checks...`);
            console.log(checksToDelete.map(check => check.id).join(","));
            await pingdom.deleteChecks(checksToDelete.map(check => check.id).join(","));
        }else{
            console.log(`No checks to delete.`);
        }

        const promises = [];

        hostsToAdd.forEach(partnerUrl => {
            const parsedUrl = url.parse(partnerUrl);

            if (!parsedUrl.hostname) {
                return;
            }

            promises.push( pingdom.addCheck({
                name: partnersDataByUrl[partnerUrl],
                url: parsedUrl.path,
                userids: userids,
                host: parsedUrl.hostname
            }));
        });


        if (promises.length === 0) {
            console.log("No checks to add.");
            process.exit();
        }

        console.log(`Adding ${promises.length} checks...`);

        await Promise.all(promises);

        console.log("All checks were added!");

    } catch (e) {
        throw e;
    }
}

start().then(() => {
        console.log("Task succeeded! ");
    },
    err => {
        console.log("Task failed! ", err);
    });

