<?php namespace App\Libraries\PartnersAlerts;

use App\Libraries\TpLogger;
use Illuminate\Support\Facades\DB;

class PartnersAlerts
{
    private $bo_connection;

    private $partners_script_path = '';

    /**
     * LogMigrations constructor.
     */
    public function __construct()
    {
        $this->bo_connection = DB::connection('bo');
        $this->partners_script_path = env('PARTNERS_ALERTS_PATH');
    }

    /**
     * Run partners alerts script
     */
    public function run()
    {
        $output = $this->executeAlertsCheck();

        return $output;
    }

    private function executeAlertsCheck()
    {
        exec('cd ' . app_path() . $this->partners_script_path . " && node app.js", $out);
        $logger = TpLogger::getInstance();
        $logger->info($out);

        return $out;
    }

    /**
     * Close connection
     */
    public function __destruct()
    {
        $this->bo_connection->disconnect();
    }
}