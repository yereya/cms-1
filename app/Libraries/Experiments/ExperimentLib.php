<?php namespace App\Libraries\Experiments;

use App\Entities\Models\Experiments\Experiment;
use App\Entities\Models\Experiments\Scheduled;
use App\Entities\Models\Experiments\Tasks;
use App\Libraries\Experiments\FetchHandlers\HolsteredQuery;
use App\Libraries\TpLogger;


/**
 * Class ExperimentLib
 *
 * @package App\Console\Commands\ReportsScraper\Providers
 */
class ExperimentLib
{
    /**
     * @var int
     */
    private static $system_id = 32;

    /**
     * @var TpLogger
     */
    private static $logger;


    /**
     * Trigger
     * Will schedule the experiment
     *
     * @param $experiment_id
     *
     * @return string
     */
    public static function trigger($experiment_id)
    {
        self::$logger->info('Experiment triggered id: ' . $experiment_id);
        $experiment = Experiment::whereId($experiment_id)->with('tasks')->first();

        if (!count($experiment)) {
            self::$logger->error('Experiment id='. $experiment_id .' could not be found.');
            return 'Experiment id='. $experiment_id .' could not be found.';
        }
        
        self::scheduleExperiment($experiment);
    }
    
    
    public static function doScheduledTasks()
    {
        $tasks = Scheduled::whereRaw("date_format(now(), '%H:%i') >= date_format(run_time, '%H:%i')")->get();

        if (!count($tasks)) {
            return;
        }
        
        self::$logger->info('Experiment scheduled tasks: count: ' . count($tasks));

        $tasks_to_do = self::processScheduledTasks($tasks->toArray());
        

        foreach ($tasks_to_do as $task) {
            self::$logger->info(['Starting task', $task]);
            self::triggerExperimentTaskHandler($task);
        }
    }


    /**
     * Process Scheduled Tasks
     *
     * @param $scheduled_tasks
     *
     * @return array
     */
    private static function processScheduledTasks($scheduled_tasks)
    {
        self::$logger->info('Experiment tasks: '. count($scheduled_tasks) . ' - '. date('Y-m-d H:i:s'));

        // fetch all the tasks for this scheduled
        $experiment_tasks_ids = array_unique(array_pluck($scheduled_tasks, 'task_id'));
        $experiment_tasks = Tasks::whereIn('id', $experiment_tasks_ids)->get()
            ->keyBy('id')->toArray();

        $task_to_do = [];
        $i = 0;
        foreach ($scheduled_tasks as $key => $task) {
            $task_to_do[$i] = $experiment_tasks[$task['task_id']];
            
            // Merge the params of the scheduled with the params
            // of the original task (overwrites the original)
            $_tasks_params = json_decode($experiment_tasks[$task['task_id']]['params'], true);
            $_scheduled_params = json_decode($task['params'], true);
            $task_to_do[$i]['params'] = array_merge((array) $_tasks_params, (array) $_scheduled_params);

            arrayRemoveFields($task_to_do[$i], ['updated_at', 'created_at', 'deleted_at']);
            $i++;
        }

        return $task_to_do;
    }


    /**
     * Trigger Experiment Task Handler
     * 
     * @param $task
     *
     * @return mixed
     */
    private static function triggerExperimentTaskHandler($task)
    {
        $handler_class = self::getFetchHandlerClass($task['system_id']);
        self::$logger->info(['Using handler: ' . $handler_class]);
        $handler = new $handler_class($task);
        
        try {
            $handler->fetch();
        } catch (\Exception $e) {
            self::$logger->error(['Handler failed to fetch: ' . $e->getMessage()]);
            return false;
        }
        
        //TODO add a flow which will use the handlers' results
        
        return true;
    }


    /**
     * Get Fetcher Handler Class
     *
     * @param $system_id
     *
     * @return mixed
     */
    private static function getFetchHandlerClass($system_id)
    {
        $handlers = [
            31 => HolsteredQuery::class
        ];

        return $handlers[$system_id];
    }
    

    /**
     * Schedule Experiment
     *
     * @param Experiment $experiment
     */
    private static function scheduleExperiment(Experiment $experiment)
    {
        $appointments = self::buildAppointment($experiment->toArray());
        self::$logger->info(['Scheduled appointments', $appointments]);

        $result = Scheduled::insert($appointments);
        self::$logger->info(['Insert result', $result]);
    }


    /**
     * Build Appointment
     * Builds an array of the scheduled instance
     *
     * @param $experiment
     *
     * @return array
     */
    private static function buildAppointment($experiment)
    {
        $appointment = [];
        $i = 0;
        foreach ($experiment['tasks'] as $exp) {
            $appointment[$i]['system_id'] = $experiment['system_id'];
            $appointment[$i]['task_id'] = $exp['id'];
            $appointment[$i]['params'] = $exp['params'];
            $appointment[$i]['run_time'] = date('Y-m-d H:i:s', strtotime('+'. $exp['pivot']['minutes_offset'] .' minutes'));
            $i++;
        }

        return $appointment;
    }

    
    /**
     * Init
     */
    public static function init()
    {
        self::$logger = new TpLogger();
        self::$logger->setSystem(self::$system_id);
    }
}

ExperimentLib::init();