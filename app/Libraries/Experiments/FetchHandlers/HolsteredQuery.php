<?php namespace App\Libraries\Experiments\FetchHandlers;

use App\Libraries\Experiments\FetchHandler;
use App\Libraries\HolsteredQueriesLib;

/**
 * Class HolsteredQuery
 *
 */
class HolsteredQuery extends FetchHandler
{


    /**
     * Fetch
     * 
     * @return $this
     * @throws \Exception
     */
    public function fetch()
    {
        if (!$this->task['system_task_id']) {
            throw new \Exception('No system task was passed.');
        }
        $this->fetch_result = HolsteredQueriesLib::fetch($this->task['system_task_id']);
        
        return $this;
    }
}
