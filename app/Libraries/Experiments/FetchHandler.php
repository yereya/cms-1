<?php namespace App\Libraries\Experiments;

use Doctrine\Common\Collections\Collection;


/**
 * Class Handlers
 *
 */
abstract class FetchHandler
{
    /**
     * @var array
     */
    protected $task;

    /**
     * @var array|Collection
     */
    protected $fetch_result;
    
    public function __construct(array $task)
    {
        $this->task = $task;
    }
}
