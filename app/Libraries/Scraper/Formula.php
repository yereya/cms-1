<?php namespace App\Libraries\Scraper;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountReportField;

/**
 * Class Formula
 *
 * @package App\Libraries\Scraper
 */
class Formula
{
    /**
     * @var array $row
     */
    private $row;

    /**
     * @var int $report_id
     */
    private $report_id;

    /**
     * @type string
     */
    private $account_id_column;

    /**
     * Formula constructor.
     *
     * @param array  $row
     * @param int    $report_id
     * @param string $account_id_column
     */
    public function __construct(array $row, $report_id, $account_id_column)
    {
        $this->row               = $row;
        $this->report_id         = $report_id;
        $this->account_id_column = $account_id_column;
    }

    public function get()
    {
        if ($Account = $this->getAccount()) {
            return $this->getFields($Account);
        } else {
            return [];
        }
    }

    /**
     * Get Account
     *
     * @return Account
     */
    private function getAccount()
    {
        return Account::where('source_account_id', $this->row[$this->account_id_column])->first();
    }

    /**
     * Get Fields
     *
     * @param Account $Account
     *
     * @return array
     */
    private function getFields(Account $Account)
    {
        $fields = [];
        foreach ($Account->reportFields($this->report_id)->get() as $field) {
            $fields[$field->field] = $this->getValueByOperator($field, $this->getSourceField($field), $this->getValueForOperators($field));;
        }

        return $fields;
    }

    /**
     * Get Value By Operator
     *
     * @param $field
     * @param $source_field
     * @param $operator_value
     *
     * @return float
     */
    private function getValueByOperator(AccountReportField $field, $source_field, $operator_value)
    {
        $value = false;
        switch ($field->operator) {
            case 'addition':
                $value = $source_field + $operator_value;
                break;

            case 'substraction':
                $value = $source_field - $operator_value;
                break;

            case 'multiplication':
                $value = $source_field * $operator_value;
                break;

            case 'division':
                $value = $source_field / $operator_value;
                break;
        }

        return $value;
    }

    /**
     * Get Source Field
     *
     * @param AccountReportField $field
     *
     * @return string
     */
    private function getSourceField(AccountReportField $field)
    {
        return $this->row[$field->source_field];
    }

    /**
     * Ge Value For Operators
     *
     * @param AccountReportField $field
     *
     * @return string
     */
    private function getValueForOperators(AccountReportField $field)
    {
        return $field->value_type == 'value' ? $field->value : $this->row[$field->value];
    }
}