<?php namespace App\Libraries\Scraper;

use App\Libraries\Scraper\Contracts\Provider as ProviderContract;
use App\Libraries\TpLogger;
use File;
use Storage;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper
 */
abstract class Provider implements ProviderContract
{
    /**
     * @var mixed $data
     */
    protected $data;

    /**
     * @type TpLogger $logger
     */
    protected $logger;

    /**
     * @var array $translators
     */
    protected $translators;

    /**
     * @var array $settings
     */
    protected $settings;

    /**
     * @var array $options
     */
    protected $options;

    /**
     * @var string $publisher_id
     */
    protected $publisher_id;

    /**
     * @var string $publisher_name
     */
    protected $publisher_name;

    /**
     * Will decide whether the provider of this reports needs to insert or insertOrUpdate it's new fetched records
     *
     * @options insert | insertOrUpdate
     *
     * @var string
     */
    protected $store_method = 'insert';

    /**
     * Provider constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        $this->logger      = $logger;
        $this->translators = $translators;
        $this->settings    = $settings;
        $this->options     = $options;

        $this->clean();
    }

    /**
     * Clean unneeded folders
     *
     * @return bool
     */
    public function clean()
    {
        if (!isset($this->report_folder_name)) {
            return true;
        }

        $report_path = $this->getReportFolderPath();

        return \File::deleteDirectory($report_path);
    }

    /**
     * Get Download Path
     *
     * @param bool $absolute_path -- will include the path to the storage folder or not
     *
     * @return string
     * @throws \Exception
     */
    protected function getReportFolderPath($absolute_path = true)
    {
        if (!isset($this->report_folder_name)) {
            throw new \Exception('Report folder name is not defined for this report.');
        }

        $report_folder = 'reports' . DIRECTORY_SEPARATOR . $this->report_folder_name . DIRECTORY_SEPARATOR . $this->publisher_name;

        // Make sure the directory exists
        Storage::makeDirectory($report_folder);

        if ($absolute_path) {
            return storage_path('app') . DIRECTORY_SEPARATOR . $report_folder;
        } else {
            return $report_folder;
        }
    }

    /**
     * Fetch
     */
    abstract public function fetch();

    /**
     * Prepare
     */
    public function prepare()
    {
        $this->logger->info('Preparing translators');
        foreach ((array) $this->translators as $i => $translator) {
            $this->options['store_method'] = $this->store_method;
            $this->translators[$i]         = new $translator($this->data, $this->options);
        }
    }

    /**
     * Store
     *
     * @array
     */
    public function store()
    {
        foreach ((array) $this->translators as $i => $translator) {
            $translator->store();
        }

        $this->logger->info('Finished storing provider: ' . $this->publisher_name);
    }

    /**
     * Get Report File Path
     *
     * @param        $file_name
     * @param string $file_extension
     * @param bool   $absolute_path -- will include the path to the storage folder or not
     *
     * @return string
     * @throws \Exception
     */
    protected function getReportFilePath($file_name, $file_extension = 'csv', $absolute_path = true)
    {
        if (is_null($file_extension)) {
            $file_extension = 'csv';
        }
        $report_file_path = $this->getReportFolderPath($absolute_path) . DIRECTORY_SEPARATOR . $file_name . '.' . $file_extension;

        if (!File::exists($report_file_path)) {
            Storage::put($report_file_path, '', true);
        }

        return $report_file_path;
    }

    /**
     * Has Data
     *
     * @return bool
     */
    public function hasData()
    {
        return (bool) $this->data;
    }
}
