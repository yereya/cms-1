<?php namespace App\Libraries\Scraper\Requests\Campaign\Providers\Outbrain;

use App\Libraries\Scraper\Providers\OutbrainReport;
use Exception;
use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountCampaign;
use App\Libraries\Scraper\Requests\Campaign\Providers\SegmentTrait;


class Provider extends OutbrainReport
{
    use SegmentTrait;

    /**
     * @var string
     */
    protected $report_type = 'CAMPAIGNS_REPORT';

    /**
     * @var int
     */
    protected $report_id = 14;

    /**
     * @Override parent method
     * start this script
     */
    public function fetch()
    {
        $this->logger->info('Start fetch');

        $accounts = $this->getAccounts();
        $this->set_credentials();
        $this->get_auth_token();

        foreach ($accounts as $account){
            $campaigns = $this->getCampaigns($this->account_id); // In the future, pass $account->id as parameter
            $this->storeCampaigns($account, $campaigns);
        }

//        $this->logger->info('Start fetch');
//
//        $customers = $this->getCustomers();
//
//        $this->importCampaigns($customers);
    }



    private function set_credentials()
    {
        # --- Set Credentials ---
        $credentials = include(config_path().'/outbrain.php');

        $this->account_id  = $credentials['account_id'];
        $this->encoded_credentials = $credentials['encoded_credentials'];
    }

    /**
     * Get list of campaigns
     *
     * @return array
     */
    private function getCampaigns($account_id)
    {
        $base_url = 'https://api.outbrain.com/amplify/v0.1/marketers/';
        $url = $base_url . $account_id . '/campaigns';
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "OB-TOKEN-V1:" . $this->ob_token_v1
            )
        );
        $data_as_json = file_get_contents($url, false, stream_context_create($opts));
        $data = json_decode($data_as_json, true);

        return $data['campaigns'];
    }

    /**
     * Get Customers By Advertisers, only mcc 1
     */
    private function getAccounts()
    {
        return Account::where('publisher_id', $this->publisher_id)->with('campaigns')->get();
    }


    /**
     * insert campaigns id and name to table
     *
     * @param $campaigns - must have collection
     * @param $account   - this account model
     */
    public function storeCampaigns($account,$campaigns)
    {
        if ($campaigns) {
            foreach ($campaigns as $campaign) {
                try {
                    $account_campaign = AccountCampaign::where('account_id', $account->id)
                        ->where('campaign_id', crc32($campaign['id']))->first();

                    if (!$account_campaign) {
                        // if the campaign was not found, create it
                        $values = [
                            'account_id'        => $account->id,
                            'campaign_id'       => crc32($campaign['id']),
                            'campaign_name'     => $campaign['name'],
                            'status'            => $campaign['enabled'] ? 'ENABLED' : 'PAUSED',
                            'segment'           => $this->getSegment($campaign['name'], $account),
                            'start_date'        => date('Y-m-d H:i:s', strtotime($campaign['creationTime'])),
                            'source_account_id' => $account['source_account_id']
                        ];

                        AccountCampaign::create($values);
                    } else {
                        $account_campaign->campaign_name = $campaign['name'];
                        $account_campaign->status        = $campaign['enabled']?'ENABLED':'PAUSED';
                        $account_campaign->start_date    = date('Y-m-d H:i:s', strtotime($campaign['creationTime']));
                        $account_campaign->save();
                    }
                } catch (\PDOException $e) {
                    $this->logger->info('account campaigns failure store to table (account id: ' . $account->id . ') error: ' . $e->getMessage());
                }

            }
        }
    }

    /**
     * @Override parent method
     * @return null
     */
    public function hasData()
    {
        return null;
    }

    //abstract method - not used

    public function getFields()
    {
    }

    //abstract method - not used
    protected function setSelector()
    {
        // TODO: Implement setSelector() method.
    }

    //abstract method - not used
    protected function setReportDefinition(Selector $selector)
    {
        // TODO: Implement setReportDefinition() method.
    }
}
