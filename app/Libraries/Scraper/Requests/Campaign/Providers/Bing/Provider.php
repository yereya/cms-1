<?php namespace app\Libraries\Scraper\Requests\Campaign\Providers\Bing;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountCampaign;
use App\Libraries\Scraper\Providers\BingReport;
use App\Libraries\Scraper\Requests\Campaign\Providers\SegmentTrait;

use Microsoft\BingAds\V13\Reporting\ReportRequest;

class Provider extends BingReport
{
    use SegmentTrait;

    /**
     * @var string
     */
    protected $report_type = 'CAMPAIGNS_REPORT';

    /**
     * @var int
     */
    protected $report_id = 14;

    /**
     * Fetch method
     */
    public function fetch()
    {
        $this->logger->info('Start fetch');

        $customers = $this->getCustomers();

        foreach ($customers as $account) {
            $campaigns = $this->importCampaigns($account);
            $this->storeCampaigns($campaigns, $account);
        }
    }

    /**
     * Get Customers By Advertisers
     */
    private function getCustomers()
    {
        return Account::where('publisher_id', $this->publisher_id)
            ->where('status','=','active')
            ->with('campaigns')
            ->get();
    }

    /**
     * Fetch campaigns for a given account id
     *
     * @param Account $account
     *
     * @return mixed
     */
    private function importCampaigns($account)
    {
        $this->bing_client->withAccountId($account->source_account_id);
        return $this->bing_client->getCampaigns($account->source_account_id);
    }

    /**
     * @param $campaigns
     * @param $account
     *
     * @return null
     */
    public function storeCampaigns($campaigns, $account)
    {
        if (!isset($campaigns->Campaigns->Campaign)) {
            return null;
        }

        foreach ($campaigns->Campaigns->Campaign as $campaign) {
            try {
                $account_campaign = AccountCampaign::where('campaign_id', $campaign->Id)->first();

                if (!$account_campaign) {
                    // if the campaign was not found, create it
                    $values = [
                        'account_id'        => $account->id,
                        'campaign_id'       => $campaign->Id,
                        'campaign_name'     => $campaign->Name,
                        'status'            => $campaign->Status,
                        'segment'           => $this->getSegment($campaign->Name, $account),
                        'source_account_id' => $account->source_account_id,
                        'start_date'        => date('Y-m-d H:i:s')
                    ];
                    AccountCampaign::create($values);
                } else {
                    $account_campaign->campaign_name = $campaign->Name;
                    $account_campaign->status        = $campaign->Status;
                    $account_campaign->save();
                }
            } catch (\PDOException $e) {
                $this->logger->info('account campaigns failure store to table (account id: ' . $account->id . ') error: ' . $e->getMessage());
            }
        }
    }

    /**
     * @Override parent method
     *
     * @return null
     */
    public function hasData()
    {
        return null;
    }

    protected function getReportRequestSettings(ReportRequest $request)
    {
    }
}
