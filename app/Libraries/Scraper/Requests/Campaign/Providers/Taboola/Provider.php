<?php namespace App\Libraries\Scraper\Requests\Campaign\Providers\Taboola;

use App\Libraries\Scraper\Providers\TaboolaReport;
use Exception;
use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountCampaign;
use App\Libraries\Scraper\Requests\Campaign\Providers\SegmentTrait;


class Provider extends TaboolaReport
{
    use SegmentTrait;

    /**
     * @var string
     */
    protected $report_type = 'CAMPAIGNS_REPORT';

    /**
     * @var int
     */
    protected $report_id = 14;

    /**
     * @Override parent method
     * start this script
     */
    public function fetch()
    {
        $this->logger->info('Start fetch');

        $accounts = $this->getAccounts();
        $this->set_credentials();
        $this->get_access_token();

        foreach ($accounts as $account){
            $account_id = str_replace(" ","",strtolower($account->name));
            $campaigns = $this->getCampaigns($account_id);  // In the future, pass $account->id as parameter
            $this->storeCampaigns($account, $campaigns);
        }


    }

    private function getCampaigns($account_id)
    {
        $url = 'https://backstage.taboola.com/backstage/api/1.0/'.$account_id.'/campaigns';
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Authorization:". 'Bearer ' . $this->access_token
            )
        );

        $data = file_get_contents($url,false, stream_context_create($opts));
        $campaigns = array_filter(json_decode($data,true)['results'], function($row) {
            return $row['is_active'];
        });
        return $campaigns;
    }

    /**
     * Get Customers By Advertisers, only mcc 1
     */
    private function getAccounts()
    {
        return Account::where('publisher_id', $this->publisher_id)->where('status','=','active')->with('campaigns')->get();
    }


    private function set_credentials()
    {
        # --- Set Credentials ---
        $credentials = include(config_path().'/taboola.php');

        $this->client_id     = $credentials['client_id'];
        $this->client_secret = $credentials['client_secret'];
        $this->grant_type    = $credentials['grant_type'];
        $this->account_id    = $credentials['account_id'];
    }


    /**
     * insert campaigns id and name to table
     *
     * @param $campaigns - must have collection
     * @param $account   - this account model
     */
    public function storeCampaigns($account, $campaigns)
    {
        if ($campaigns) {
            foreach ($campaigns as $campaign) {
                try {
                    $account_campaign = AccountCampaign::where('account_id', $account->id)
                        ->where('campaign_id', $campaign['id'])->first();

                    if (!$account_campaign) {
                        // if the campaign was not found, create it
                        $values = [
                            'account_id'        => $account->id,
                            'campaign_id'       => $campaign['id'],
                            'campaign_name'     => $campaign['name'],
                            'status'            => $campaign['is_active'] ? 'ENABLED' : 'PAUSED',
                            'segment'           => $this->getSegment($campaign['name'], $account),
                            'start_date'        => date('Y-m-d H:i:s', strtotime($campaign['start_date'])),
                            'source_account_id' => $account['source_account_id']
                        ];

                        AccountCampaign::create($values);
                    } else {
                        $account_campaign->campaign_name = $campaign['name'];
                        $account_campaign->status        = $campaign['is_active'] ? 'ENABLED' : 'PAUSED';
                        $account_campaign->start_date    = date('Y-m-d H:i:s', strtotime($campaign['start_date']));
                        $account_campaign->save();
                    }
                } catch (\PDOException $e) {
                    $this->logger->info('account campaigns failure store to table (account id: ' . $account->id . ') error: ' . $e->getMessage());
                }
            }
        }
    }

    /**
     * @Override parent method
     * @return null
     */
    public function hasData()
    {
        return null;
    }

    //abstract method - not used

    public function getFields()
    {
    }

    //abstract method - not used
    protected function setSelector()
    {
        // TODO: Implement setSelector() method.
    }

    //abstract method - not used
    protected function setReportDefinition(Selector $selector)
    {
        // TODO: Implement setReportDefinition() method.
    }
}
