<?php namespace App\Libraries\Scraper\Requests\Campaign\Providers;

trait SegmentTrait
{
    /**
     * Get Segment by campaign name
     *
     * @param $campaign_name
     * @param $account
     *
     * @return string
     */
    private function getSegment($campaign_name, $account)
    {
        // First "all"
        if ($this->contains($campaign_name, 'partner')) {
            return 'partner';
        } elseif ($this->contains($campaign_name, 'free')) {
            return 'free';
        } elseif ($this->contains($campaign_name, 'bonus')) {
            return 'bonus';
        } elseif ($this->contains($campaign_name, 'review')) {
            return 'review';
        }

        // if display then display
        if ($this->contains($account->name, 'display')) {
            return 'display';
        }

        // If "Casino"
        if ($this->contains($account->name, 'casino')) {
            if ($this->contains($campaign_name, 'paypal')) {
                return 'paypal';
            } elseif ($this->contains($campaign_name, 'blackjack')) {
                return 'blackjack';
            } elseif ($this->contains($campaign_name, 'roulette')) {
                return 'roulette';
            } elseif ($this->contains($campaign_name, 'slots')) {
                return 'slots';
            } elseif ($this->contains($campaign_name, 'live')) {
                return 'live';
            } else {
                return 'casino';
            }
        }

        if ($this->contains($account->name, 'website') || $this->contains($account->name, 'vpn') || $this->contains($account->name, 'antivirus') || $this->contains($account->name, 'dating')) {
            if ($this->contains($campaign_name, 'best')) {
                return 'best';
            }
            if ($this->contains($campaign_name, 'broad')) {
                return 'broad';
            }
        }

        if ($this->contains($account->name, 'website') || $this->contains($account->name, 'vpn') || $this->contains($account->name, 'antivirus')) {
            if ($this->contains($campaign_name, 'business')) {
                return 'business';
            }
        }

        if ($this->contains($account->name, 'website') || $this->contains($account->name, 'vpn')) {
            if ($this->contains($campaign_name, 'how to')) {
                return 'how to';
            }
        }

        if ($this->contains($account->name, 'dating')) {
            if ($this->contains($campaign_name, 'mature')) {
                return 'mature';
            }
        }

        // All other account are
        return 'gen';
    }

    /**
     * Checks that certain string is contains inside another
     * works case insensitive
     *
     * @param $haystack
     * @param $needle
     *
     * @return bool
     */
    private function contains($haystack, $needle)
    {
        return (strpos(strtolower($haystack), $needle) !== false);
    }
}