<?php namespace App\Libraries\Scraper\Requests\Campaign;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\Campaign\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\Campaign\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\Campaign\Providers\Google\Provider;
use App\Libraries\Scraper\Requests\Campaign\Providers\GoogleGNG\Provider as ProviderGNG;
use App\Libraries\Scraper\Requests\Campaign\Providers\Outbrain\Provider as OutbrainProvider;
use App\Libraries\Scraper\Requests\Campaign\Providers\Taboola\Provider as TaboolaProvider;

class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 36;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class' => Provider::class
        ],
//        'googleGNG' => [
//            'class' => ProviderGNG::class
//        ],
        'bing'   => [
            'class' => BingProvider::class
        ],
//        'bingGNG'   => [
//            'class' => BingGNGProvider::class
//        ],
        'taboola' => [
            'class' => TaboolaProvider::class
        ],
//        'outbrain'   => [
//            'class' => OutbrainProvider::class
//        ]
    ];
}
