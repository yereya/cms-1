<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers;

/**
 * Class CUrlLib
 */
class CUrlLib extends ScraperFetcherLib
{
    /**
     * Fetches results and normalizes them
     *
     * @return array|null
     */
    public function fetch()
    {
        //  1 Validate params
        //  2 Initiate fetch
        //      2.1. Build proper request
        //      2.2. Execute request and store result
        //  3 Translate result
        //     3.1 Validate translator params
        //     3.2 Build translator params
        //  4 Return translated result

        if (!$this->validateParams()) {
            return;
        };

        $concat_n_days_result = (int) $this->fetchers_params[$this->getFetcherIdx()]['properties']['concat_n_days_results'];

        // Loop through multiple fetches on the same fetcher
        if ($concat_n_days_result > 0) {
            $this->buildNDaysFetcherParams($concat_n_days_result, $this->getFetcherIdx());
            do {
                $this->initFetch();
                $concat_n_days_result--;
                $this->incrementFetcherIdx();
            } while ($concat_n_days_result > 0 && !$this->stop_scrape);
        } else {
            // Standard use case, one fetch per fetcher
            $this->initFetch();
        }

        // Return the translated result
        return $this->translated;
    }

    /**
     * Custom cUrl params validation
     *
     * @return bool
     */
    public function validateParams()
    {
        $rules = [
            'url'    => "required",
            'method' => "required",
        ];

        // Validate the passed params
        $validator = \Validator::make($this->fetchers_params[$this->getFetcherIdx()]['properties'], $rules);
        if ($validator->fails()) {
            $this->stop_scrape = true;
            $this->tp_logger->error("Could not build valid fetcher params", $validator->messages()->all());

            return false;
        }

        return true;
    }

    /**
     * Handler for result fetching
     */
    public function initFetch()
    {
        $fetcher_idx = $this->getFetcherIdx();
        $this->tp_logger->debug(["Fetcher params:" => $this->fetchers_params[$fetcher_idx]]);

        // Sleeping before fetch
        if ($this->fetchers_params[$fetcher_idx]['properties']['fetcher_sleep'] > 0) {
            sleep($this->fetchers_params[$fetcher_idx]['properties']['fetcher_sleep']);
            $this->tp_logger->info("Sleeping " . $this->fetchers_params[$fetcher_idx]['properties']['fetcher_sleep'] . " Seconds before fetch");
        }

        $this->readyShortCodeParams();

        if (!$this->validateParams()) {
            return;
        }

        // Stores valid fetcher parameters to be used by guzzle
        $guzzle_params = $this->buildValidFetcherParams();

        if (empty($guzzle_params)) {
            $this->tp_logger->error("No curl params to be sent.");
            $this->stop_scrape = true;

            return;
        }

        // Execute request and store response results
        $this->executeRequest(
            $this->fetchers_params[$this->getFetcherIdx()]['properties']['method'],
            $this->fetchers_params[$this->getFetcherIdx()]['properties']['url'],
            $guzzle_params
        );
        $this->buildResponseResults();

        // If there exists additional translators
        if ($this->scraper_repo->countTranslators()) {
            $this->translateResponse($this->fetchers_params[$fetcher_idx]['_response_idx'], $fetcher_idx);
        }
    }

    /**
     * Validates and builds a proper params array
     * for the Guzzle
     *
     * @return mixed
     */
    private function buildValidFetcherParams()
    {
        $fetcher      = $this->fetchers_params[$this->getFetcherIdx()];
        $result       = $fetcher['properties'];
        $result['id'] = $fetcher['id'];

        if (isset($fetcher['properties']['protocol_1_0']) AND $fetcher['properties']['protocol_1_0']) {
            $result['version'] = "1.0";
        }

        if (isset($fetcher['properties']['ssl_certificate_verification']) AND $fetcher['properties']['ssl_certificate_verification']) {
            $result['verify'] = false;
        }

        // Do we need to use User/Pass as the auth credentials
        if (isset($fetcher['properties']['use_credentials_as_auth']) && $fetcher['properties']['use_credentials_as_auth']) {
            if (isset($this->scraper_params['properties']['username']) && isset($this->scraper_params['properties']['password'])) {
                $result['auth'][] = $this->scraper_params['properties']['username'];
                $result['auth'][] = $this->scraper_params['properties']['password'];
            } else {
                $this->tp_logger->error("Could not build valid fetcher params, Auth User Password were not passed.");
            }
        }

        // if the request uses payload, in our system it can be done by putting key "Payload"
        // in the body section and the payload in the value
        if ($fetcher['properties']['form_params']['Payload'] ?? false) {
            $result['json'] = json_decode($fetcher['properties']['form_params']['Payload'], true);
        }

        if ($fetcher['properties']['disable_redirects'] ?? false) {
            $result['allow_redirects'] = false;
        }

        if ($fetcher['properties']['disable_http_errors'] ?? false) {
            $result['http_errors'] = false;
        }

        // Clear empty arrays
        $this->clearEmptyParams($result);

        $this->tp_logger->debug(["Curl params:" => $result]);

        return $result;
    }

    /**
     * Build N Days Fetcher Params
     * Builds the multi fetches for this fetcher
     *
     * @param $days
     *
     * @param $idx
     */
    private function buildNDaysFetcherParams($days, $idx)
    {
        $original_idx = $this->getFetcherIdx();
        $original_fetcher_params = $this->fetchers_params[$idx];
        for ($days_offset = 0; $days_offset < $days; $days_offset++, $this->incrementFetcherIdx()) {
            if ($days_offset != 0) {
                $this->fetchers_params[$days_offset] = $original_fetcher_params;
            }

            // Puts a meta attribute of day that will represent for which day are we running
            $span_days = ($days - 1) - $days_offset;
            $this->fetchers_params[$days_offset]['properties']['days'] = $span_days;

            // Fix the date short code to fit the multi fetch for this fetcher
            array_walk_recursive($this->fetchers_params[$days_offset]['properties'], function (&$item) use (
                $span_days
            ) {
                if (!is_array($item) AND strpos($item, '[date') !== false) {
                    $item = str_replace('[date', '[date span="-' . $span_days . ' days"', $item);
                }
            });
        }

        $this->fetcher_idx = $original_idx;
    }
}