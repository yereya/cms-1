<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers;

interface FetcherTypeContract
{
    public function validateParams();

    public function fetch();

    public function initFetch();
}