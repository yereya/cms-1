<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers;
use PhpImap\Exception;
use PhpImap\Mailbox as ImapMailbox;

class MailExtended extends ImapMailbox
{
    /**
     * Converts a string from one encoding to another.
     * @param string $string
     * @param string $fromEncoding
     * @param string $toEncoding
     * @return string Converted string if conversion was successful, or the original string if not
     * @throws Exception
     */
    protected function convertStringEncoding($string, $fromEncoding, $toEncoding) {
        if(!$string || $fromEncoding == $toEncoding) {
            return $string;
        }
        $fromEncoding = !isset($fromEncoding) || $fromEncoding == 'ks_c_5601-1987' ? 'EUC-KR' : $fromEncoding;
        $convertedString = function_exists('iconv') ? @iconv($fromEncoding, $toEncoding . '//IGNORE', $string) : null;
        if(!$convertedString && extension_loaded('mbstring')) {
            $convertedString = @mb_convert_encoding($string, $toEncoding, $fromEncoding);
        }
        if(!$convertedString) {
            throw new Exception('Mime string encoding conversion failed');
        }
        return $convertedString;
    }
}