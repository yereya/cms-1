<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers;
use App\Entities\Repositories\Reports\Publishers\BrandPerformance\ScraperRepo;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\Traits\FetcherNormalizer;
use Illuminate\Validation\Rule;
use ZipArchive;
//use PhpImap\Mailbox;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\MailExtended as Mailbox;

/**
 * Class MailLib
 */
class MailLib extends ScraperFetcherLib
{
    use FetcherNormalizer;

    /**
     * @var string $username - mail account
     */
    private $username  = '';

    /**
     * @var string $password - mail password
     */
    private $password  = '';

    /**
     * @var string $imap_gmail_connection - default gmail imap address
     */
    private $imap_gmail_connection = '';

    /**
     * @var string $storage_path - base path for mail attachment storage
     */
    private $storage_path = 'app/scraper/mail_attachments/';

    /**
     * @var Mailbox - the php imap class object
     */
    private $mailbox;

    /**
     * @var array $mail_ids - fetched mail ids to be used
     */
    private $mail_ids = [];

    /**
     * @var $mails - the fetched mails
     */
    private $mails = [];

    /**
     * @var array $valid_extensions - valid formats to be translated
     */
    private $valid_extensions;

    /**
     * @var array - array of files with a valid format
     */
    private $valid_stored_files = [];

    /**
     * @var array $fetched_content - stored file contents
     */
    private $fetched_content = [];

    public function __construct(array $fetchers_params, array $scraper_params, $tp_logger, ScraperRepo $scraper_repo, $short_codes, $client, $previously_translated = null)
    {
        parent::__construct($fetchers_params, $scraper_params, $tp_logger, $scraper_repo, $short_codes, $client, $previously_translated);

        $this->valid_extensions = config('mail.affiliates.valid_extensions');
        $this->imap_gmail_connection = config('mail.affiliates.imap_gmail_connection');
    }

    /*
     * Fetches results and normalizes them
     */
    public function fetch()
    {
        // Plan:
        // 1. Validate the fetcher parameters for mail usage
        // 2. Create the mail connection
        //      2.1 create a mailbox object
        //      2.2 Fetch all unread mails/all mails - depends - need to decide
        // 3. Filter the mails by the user's given filter
        //      3.1 Look for mails with the given details
        //      3.2 Populate filtered mails with the result set
        // 4. Translate the mail attachment results

        if (!$this->validateParams()) {
            return;
        }

        $this->buildValidMailer();

        $this->initFetch();

        return $this->translated;
    }

    /*
     * Validates email fetcher parameters
     */
    public function validateParams()
    {
        $rules = [
            'mail_query_type' => [
                'required',
                Rule::in(["body", "attachment"])
            ]
        ];

        // Validate the passed params
        $validator = \Validator::make($this->fetchers_params[$this->getFetcherIdx()]['properties'], $rules);
        if ($validator->fails()) {
            $this->stop_scrape = true;
            $this->tp_logger->error("Could not build valid fetcher params", $validator->messages()->all());

            return false;
        }

        return true;
    }

    /**
     * Handler for result fetching
     */
    public function initFetch()
    {
        $this->tp_logger->info("---- Inside initFetch | Start");

        // 1. Get mail ids
        // 2. Activate email filtering by user parameters
        // 3. Activate translation
        //      3.1 For each mail - translate and append results to translated
        // 4. Normalize results

        // Ready shortcode parameters to be processed
        $this->readyShortCodeParams();
        $this->tp_logger->info("---- Inside initFetch -> After readyShortCodeParams");

        // Filter by user-given expressions
        $this->filterMailsByCriterias();
        $this->tp_logger->info("---- Inside initFetch -> After filterMailsByCriterias");

        // Clear Attachments from previous runs related to the current scraper
        $this->clearAttachmentStorage();
        $this->tp_logger->info("---- Inside initFetch -> After clearAttachmentStorage");

        // Fetch all the mail info and append it to results
        $this->appendFilteredMails();
        $this->tp_logger->info("---- Inside initFetch -> After appendFilteredMails");

        // Handle the mail fetching type
        $this->processMails();
        $this->tp_logger->info("---- Inside initFetch -> After processMails");
        $this->tp_logger->info("---- Inside initFetch | End");
    }

    /**
     * Override the original method in order to match mail fetching
     *
     * @param $params
     * @param $file_idx
     * @return array|void
     */
    protected function buildValidTranslatorParams($params, $file_idx)
    {
        $content_result = $this->fetched_content[$file_idx];

        // Make sure we do not get the empty string ""
        if ($params !== "") {
            $translator_params = explode(',', $params);
        } else {
            $translator_params = [];
        }

        $translator_params = $this->short_codes->setShortCodes($translator_params, [
            'scraper_params'    => $this->scraper_params,
            'fetchers_params'   => $this->fetchers_params,
            'translated'        => $this->translated,
            'fetchers_response' => $this->fetchers_response
        ]);

        return array_merge([$content_result], $translator_params);
    }

    /**
     * Initial mail parameters for connection and fetching
     */
    private function buildValidMailer()
    {
        // Sets the mail's account credentials
        $this->username = config('mail.affiliates.username');
        $this->password = config('mail.affiliates.password');
        $folder_path = $this->getStoragePath();


         if (!$this->mailbox) {
            $this->mailbox = new Mailbox(
                $this->imap_gmail_connection,
                $this->username,
                $this->password,
                $folder_path
            );
        }
    }

    /**
     * Will filter $this->mails by criteria expressions
     * Returns an array of mails that match the expression
     */
    private function filterMailsByCriterias()
    {
        // Gets mail criterias
        $search_criterias = $this->buildSearchCriterias();
        $this->tp_logger->debug(["Filter criterias:", $search_criterias]);

        if ($search_criterias) {
            // Filters mails according to search criterias
            $this->mail_ids = array_merge($this->mail_ids, $this->joinedMailSearchResultsIds($search_criterias));
        }
    }

    /**
     * Uses the mailbox interface in order to fetch filtered mail data
     */
    private function appendFilteredMails()
    {
        foreach ($this->mail_ids as $mail_id) {
            $_mail = $this->mailbox->getMail($mail_id);
            if ($_mail) {
                $this->mails[] = $_mail;
            }
        }

        $this->tp_logger->debug(["Mails info:", $this->mailbox->getMailsInfo($this->mail_ids)]);
    }

    /**
     * Handler method for different mail fetching types(body or attachment)
     */
    private function processMails()
    {
        $this->tp_logger->info("---- Inside processMails | Start");
        $fetcher_idx = $this->getFetcherIdx();
        // Retrieves the mail query type
        $mail_query_type = $this->fetchers_params[$fetcher_idx]['properties']['mail_query_type'] ?? '';

        if (!$mail_query_type) {
            $this->tp_logger->error("No mail_query_type stated.");

            return;
        }

        // Creates the process method string and activates it
        $process_method = 'processFetched' . title_case($mail_query_type);
        if (!method_exists($this, $process_method)) {
            $this->tp_logger->error('No method "' . $process_method . '" was found.');

            return;
        }

        $this->{$process_method}();
        $this->tp_logger->info("---- Inside processMails | End");
    }

    /**
     * Handles the html body of the mail
     */
    private function processFetchedBody()
    {
        // Builds mail bodies
        $this->buildValidHtmlBodies();

        // Translate the fetched mails body html
        $this->translateMailBody();
    }

    /**
     * Builds the fetched mail's html bodies
     */
    private function buildValidHtmlBodies()
    {
        $this->fetched_content = array_map(function ($mail) {
            return $mail->textHtml;
        }, $this->mails);
    }

    /**
     * Translates the fetched mail htmls and appends the results
     */
    private function translateMailBody()
    {
        $altered_translations   = $this->translated;
        $fetcher_idx            = $this->getFetcherIdx();
        $translation_key        = $this->fetchers_params[$fetcher_idx]['id'];

        // Prevents redundant translations
        if (!$this->fetched_content) {
            $this->tp_logger->debug(["Nothing to translate:", $this->mails]);

            return;
        }

        // For each html body - translate the result
        foreach($this->fetched_content as $html_idx => $html_content) {
            $this->scraper_repo->rewindTranslators();

            if ($this->scraper_repo->countTranslators()) {
                $this->translateResponse($html_idx, $fetcher_idx);
            }

            $altered_translations += $this->translated[$translation_key];
        }

        // Appends the translated results to the translation key
        $this->translated[$translation_key] = $altered_translations;
        $this->appendResults();
    }

    /**
     * Handler for fetched attachment files
     * handles file paths, content translation, result normalization
     * and storage clearing
     */
    private function processFetchedAttachment()
    {
        $this->tp_logger->info("---- Inside processFetchedAttachment - MailLib.php");
        // Build file paths
        $this->buildValidAttachmentFilePaths();
        $this->tp_logger->info("----After buildValidAttachmentFilePaths | Inside processFetchedAttachment - MailLib.php");
        // Initiates file attachment translation
        $this->translateFileAttachments();
        $this->tp_logger->info("----After translateFileAttachments | Inside processFetchedAttachment - MailLib.php");
        // Cleans any temporary files left from the attachments
        $this->clearAttachmentStorage();
        $this->tp_logger->info("----After clearAttachmentStorage | Inside processFetchedAttachment - MailLib.php");
    }

    /**
     * Builds the valid attachment file paths to be translated
     * populates the valid_stored_files array
     */
    private function buildValidAttachmentFilePaths()
    {

        foreach ($this->mails as $mail){
            foreach ($mail->getAttachments() as $attachment){
                $path = $attachment->filePath;
                $this->valid_stored_files[] = $path;
            }
        }

        /*  --  Old Version --
        $folder_path = $this->getStoragePath();

        // Get all file names in the built folder path
        $scans = array_diff(scandir($folder_path), array('..', '.'));

        foreach ($scans as $scan) {
            // Filter valid formatted files and store their names
            if (in_array(pathinfo($scan, PATHINFO_EXTENSION), $this->valid_extensions)) {
                $this->valid_stored_files[] = $folder_path  . $scan;
            }
        }
        */
    }

    /**
     * Builds a folder path, and validates its creation
     *
     * @return null|string|string[]
     */
    private function getStoragePath()
    {
        // Build the folder path to be taking filenames from
        $folder_path = preg_replace("/[\\|\/]/", DIRECTORY_SEPARATOR, storage_path($this->storage_path));

        // Create directory if it doesn't exist - ..\CMS\storage\app\scraper\mail_attachments
        if (!file_exists($folder_path)) {
            mkdir($folder_path, 0777, true);
        }

        // Each scraper will have it's folder
        $folder_path = $folder_path.$this->scraper_params['id'].DIRECTORY_SEPARATOR;

        // Create directory if it doesn't exist - ..\CMS\storage\app\scraper\mail_attachments\Scraper's_ID
        if (!file_exists($folder_path)) {
            mkdir($folder_path, 0777, true);
        }

        return $folder_path;
    }

    /**
     * Gets the fetcher's mail queries
     * Extracts mail queries separately
     * If empty - adds default query to select all mails
     *
     * @return array
     */
    private function buildSearchCriterias()
    {

        return explode(';', $this->fetchers_params[$this->getFetcherIdx()]['properties']['mail_query'] ?? ['ALL']);
    }

    /**
     * Filters mails using the "AND" logic for all given criterias
     *
     * @param array $search_criterias
     * @return array
     */
    private function joinedMailSearchResultsIds(array $search_criterias)
    {
        $results = $this->mailbox->searchMailbox($search_criterias[0]);

        for ($i = 1; $i < count($search_criterias && $results); $i++) {
            // Current criteria
            $search_criteria = $search_criterias[$i];

            // Filter mails by current criteria
            $_results = $this->mailbox->searchMailbox($search_criteria);

            // Choose the ids which apply to both criterias
            $results = array_intersect($results, $_results);
        }

        return $results;
    }

    /**
     * Reads file attachments and handles translator methods
     */
    private function translateFileAttachments()
    {

        $this->tp_logger->info("---- Inside translateFileAttachments | Start");
        $altered_translations   = $this->translated;
        $fetcher_idx            = $this->getFetcherIdx();
        $translation_key        = $this->fetchers_params[$fetcher_idx]['id'];
        $empty_files            = [];

        // Prevents redundant translations
        if (!$this->valid_stored_files) {
            $this->tp_logger->debug(["Nothing to translate:", $this->mails]);

            return;
        }

        // Get the contents of each file, run the translators on the result set
        foreach ($this->valid_stored_files as $file_idx => $file) {
            // Check if the file type is zip
            if (substr($file, -3, 3) == "zip"){
                $zip = new ZipArchive();
                if($zip->open($file) === TRUE){
                    $new_folder_path = $this->getStoragePath().'unzipped'.DIRECTORY_SEPARATOR;
                    // Create directory if it doesn't exist
                    if (!file_exists($new_folder_path)) {
                        mkdir($new_folder_path, 0777, true);
                    }
                    $zip->extractTo($new_folder_path);
                    $zip->close();
                    $this->tp_logger->info("----Inside translateFileAttachments | Unzipped Process Successful! ----");

                    //$file_path = "AmeriSaveDaily_Lendstart_" . date("Y-m-d") . ".csv";
                    //List files and directories inside the specified path
                    $file_path = scandir($new_folder_path,1)[0];
                    if (str_contains($file_path,'.')) { // if it not a folder
                        $file_contents = file_get_contents($new_folder_path . $file_path, true);
                        $this->tp_logger->info("----Inside translateFileAttachments - MailLib.php | clean unzipped folder");
                        array_map('unlink', glob($new_folder_path . "*.*"));
                    }
                    else {
                        $files_paths = scandir($new_folder_path . $file_path);
                        foreach ($files_paths as $items_idx => $item_path) {
                            if(strlen($item_path) > 4){
                                $file_contents = file_get_contents($new_folder_path . $file_path . DIRECTORY_SEPARATOR . $item_path, true);
                                $this->tp_logger->info("----Inside translateFileAttachments - MailLib.php | clean unzipped folder");
                                array_map('unlink', glob($new_folder_path . "*.*"));
                            }
                        }
                    }
                }
                else{
                    $this->tp_logger->info("----Inside translateFileAttachments - MailLib.php | Unzipped Process failed! ----");
                }
            }
            else {
                // Get the file's contents as a string
                $file_contents = file_get_contents($this->valid_stored_files[$file_idx], true);
            }
            if (!$file_contents) {
                $empty_files[] = $file;
                continue;
            }

            // Add a valid file content
            $this->fetched_content[$file_idx] = $file_contents;

            // Rewinds the translators to be reiterated
            $this->scraper_repo->rewindTranslators();

            // If there exists additional translators
            if ($this->scraper_repo->countTranslators()) {
                $this->translateResponse($file_idx, $fetcher_idx);
            }

            $altered_translations += $this->translated[$translation_key];
        }

        $this->translated[$translation_key] = $altered_translations;
        $this->appendResults();

        // Inform the user on file processing state
        if ($empty_files) {
            $this->tp_logger->debug(["Empty or invalid files:", $empty_files]);
        } else {
            $this->tp_logger->info("All items have been translated successfully!");
        }
        $this->tp_logger->info("---- Inside translateFileAttachments | End");
    }

    /**
     * Will delete processed attachments from mail_attachments directory
     */
    private function clearAttachmentStorage()
    {
        $this->tp_logger->info("----Inside clearAttachmentStorage - MailLib.php | Start");
        array_map('unlink', glob($this->getStoragePath() . "*.*"));
        $this->tp_logger->info("----Inside clearAttachmentStorage - MailLib.php | End ");


        /*  -- Old Version --
        foreach ($this->valid_stored_files as $path){
            array_map('unlink', glob($path));
        }

        */
    }
}