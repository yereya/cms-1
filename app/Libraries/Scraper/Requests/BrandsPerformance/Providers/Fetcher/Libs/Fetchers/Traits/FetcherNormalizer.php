<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\Traits;

trait FetcherNormalizer
{
    /**
     * Appends all of the translated results
     * If a result does not have another result's column
     * Then null values will be given
     */
    protected function appendResults()
    {
        //  1 Iterate over all of the results and build "seen_keys" array
        //    - from unique keys
        //  2 Once again iterate over all of the translated results
        //      2.1 For each result(row):
        //      2.2 Append the row's values to the final result
        //      2.3 Add the disjoint set (seen_keys without current row keys) keys as null values
        $seen_keys          = [];
        $final_result       = [];

        foreach ($this->translated as $translator_context) {
            foreach ($translator_context as $trans_idx => $trans_row) {
                // Unseen key - add as "seen"
                $seen_keys = $seen_keys + $trans_row;
            }
        }

        // Initializes all keys as null for further processing
        $seen_keys = array_map(function ($key) {
            return null;
        }, $seen_keys);

        foreach ($this->translated as $translator_context) {
            foreach ($translator_context as $trans_idx => $trans_row) {
                // Add all of the seen values as null
                // Then merge null array with the actual result's values
                $final_result[] = array_merge($seen_keys, $trans_row);
            }
        }
        $this->translated = [$final_result];

        return $final_result;
    }
}