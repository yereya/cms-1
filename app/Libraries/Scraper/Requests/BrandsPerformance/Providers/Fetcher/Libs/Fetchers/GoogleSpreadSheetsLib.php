<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\Traits\FetcherNormalizer;

/**
 * Class GoogleSpreadSheetsLib
 */
class GoogleSpreadSheetsLib extends ScraperFetcherLib
{
    use FetcherNormalizer;

    /**
     * @var array $sheet_ids - contains related sheet ids
     */
    private $sheet_ids = [];

    /**
     * @var string
     * the prefix for the sheets api
     */
    const API_URL_PREFIX = 'https://sheets.googleapis.com/v4/spreadsheets';

    /**
     * @var array $appended_results - appended translators
     */
    private $appended_results = [];

    /**
     * @var array $excluded_spreadsheet_info_keys - excluded keys
     */
    private $excluded_spreadsheet_info_keys = ['spreadsheet_id', 'allowed_gids', 'excluded_gids'];

    /**
     * Fetches results and normalizes them
     *
     * @return array
     */
    public function fetch()
    {
        //  1 Validate params
        //  2 Initiate fetch
        //      2.1. Build proper request
        //      2.2. Execute request
        //          2.2.1. Fetch Gids and store them
        //          2.2.2. Fetch all sheets
        //      2.3 Store results
        //  3 Append translated results
        //  4 Normalize results
        //  5 Return appended result

        if (!$this->validateParams()) {
            return;
        };

        // Only one procedure option to go through
        $this->initFetch();

        return $this->translated;
    }

    /**
     * Validates required parameters
     *
     * @return bool
     */
    public function validateParams()
    {
        $rules = [
            'spreadsheet_info.spreadsheet_id'  => "required",
            'spreadsheet_info.key'             => "required"
        ];

        // Validate the passed params
        $validator = \Validator::make($this->fetchers_params[$this->getFetcherIdx()]['properties'], $rules);
        if ($validator->fails()) {
            $this->stop_scrape = true;
            $this->tp_logger->error("Could not build valid fetcher params", $validator->messages()->all());

            return false;
        }

        return true;
    }

    /**
     * Handler for result fetching
     */
    public function initFetch()
    {
        // 1. Build proper request parameters
        // 2. Execute request:
        //  2.1 Fetch all GIDs and store them
        //  2.2 Fetch all sheets by GIDs
        $fetcher_idx = $this->getFetcherIdx();
        $this->tp_logger->debug(["Fetcher params:" => $this->fetchers_params[$fetcher_idx]]);

        // Stores valid fetcher parameters to be used by guzzle
        $guzzle_params = $this->buildValidApiParams();
        $this->tp_logger->debug(["Curl params:" => $guzzle_params]);

        if (empty($guzzle_params)) {
            $this->tp_logger->error("No curl params to be sent.");
            $this->stop_scrape = true;

            return;
        }

        // Build the GID list
        $this->fetchSheetIds($guzzle_params);
        $this->tp_logger->debug(["Sheet ids:" => $this->sheet_ids]);

        if (!$this->sheet_ids) {
            $this->stop_scrape = true;
            $this->tp_logger->error("No sheet ids were found. No data to fetch.");

            return;
        }

        // Fetch and translate all sheets data
        $this->fetchAllSheets();
    }

    /**
     * Builds valid spreadsheet request parameters
     */
    public function buildValidApiParams()
    {
        $fetcher        = $this->fetchers_params[$this->getFetcherIdx()];
        $result         = $fetcher['properties'];
        $result['id']   = $fetcher['id'];

        if (isset($fetcher['properties']['ssl_certificate_verification']) AND $fetcher['properties']['ssl_certificate_verification']) {
            $result['verify'] = false;
        }

        // Clear empty arrays
        $this->clearEmptyParams($result);
        $this->tp_logger->debug(["Curl params:" => $result]);

        return $result;
    }

    /**
     * fetches a list of GIDs
     * @param array $guzzle_params
     * @return array
     */
    private function fetchSheetIds($guzzle_params)
    {
        // 1 Build api url
        // 2 Fire request and return the result list
        $url = $this->buildApiUrl();
        $this->tp_logger->debug(["Spreadsheet url:" => $url]);

        // Execute request and store response
        $this->executeRequest($this->method_get, $url, $guzzle_params);
        $this->buildResponseResults();

        // Extracts a list of ids from result set
        $result_ids = $this->extractGidsFromResult();
        $this->sheet_ids = $result_ids;

        return $result_ids;
    }

    /**
     * Build the entire url for the api request
     *
     * @return string
     */
    private function buildApiUrl()
    {
        $fetcher_idx = $this->getFetcherIdx();

        // Construct the url query parameters for the api url
        $spreadsheet_params_query = http_build_query(collect($this->fetchers_params[$fetcher_idx]['properties']['spreadsheet_info'])
            ->except($this->excluded_spreadsheet_info_keys)->toArray());

        // Append the constructed url query to the current api url prefix
        $url = self::API_URL_PREFIX . '/' . $this->fetchers_params[$fetcher_idx]['properties']['spreadsheet_info']['spreadsheet_id']
            . '?' . $spreadsheet_params_query;

        return $url;
    }

    /**
     * Extracts a list of Gids from response results
     */
    private function extractGidsFromResult()
    {
        $fetcher_idx  = $this->getFetcherIdx();
        $response_idx = $this->fetchers_params[$fetcher_idx]['_response_idx'];
        $fetched_result = json_decode($this->client->getBody($response_idx));
        $ids_list = $this->determineIdList($fetched_result);

        return $ids_list;
    }

    /**
     * Determine whether a fetched gid will be included in the result set or not
     * - given the parameters from the fetcher
     *
     * @param array $fetched_result
     * @return mixed
     */
    private function determineIdList($fetched_result)
    {
        $fetcher_idx = $this->getFetcherIdx();
        $spreadsheet_info = $this->fetchers_params[$fetcher_idx]['properties']['spreadsheet_info'];

        // Gets all of the gid documents
        $raw_result = collect($fetched_result)->flatten()
            ->pluck('properties')
            ->pluck('sheetId', 'title')
            ->toArray();

        // Returns the allowed document ids only
        if (!empty($spreadsheet_info['allowed_gids'])) {

            return array_intersect($raw_result, explode(';', $spreadsheet_info['allowed_gids']));
        }
        // Returns the filtered set of allowed document ids
        if (!empty($spreadsheet_info['excluded_gids'])) {

            return array_diff($raw_result, explode(';', $spreadsheet_info['excluded_gids']));
        }

        // The user did not set any filter
        return $raw_result;
    }

    /**
     * concat fetcher results for a list of gids. returns appended results
     *
     * @throws \Exception
     */
    private function fetchAllSheets(): void
    {
        // 1 Fetch all sheet ids
        // 2 Execute fetchSheetData for every id in the list
        //      2.1 Build url for each id
        //      2.2 Call fetchSheetData
        // 3 Append the collected data and return it
        $this->buildNIdsFetcherParams(\count($this->sheet_ids), $this->getFetcherIdx());
        $this->incrementFetcherIdx();
        $fetcher_idx          = $this->getFetcherIdx();
        $translation_key      = $this->fetchers_params[$fetcher_idx]['id'];
        $altered_translations = $this->translated;

        // Build response and store translated results for each id
        foreach ($this->sheet_ids as $sheet_name => $sheet_id) {
            $guzzle_params = $this->buildValidApiParams();
            $this->fetchSheetData($sheet_name, $guzzle_params);

            if ($this->scraper_repo->countTranslators()) {
                $this->translateResponse($translation_key, $fetcher_idx);
            }

            // Next fetcher
            $altered_translations[]             = array_map(function ($arr) use($sheet_name) {
                return $arr + ['sheet_name' => $sheet_name];
            }, $this->translated[$translation_key]);
            $this->translated[$translation_key] = null;

            // Reset translator index for the next fetcher
            $this->scraper_repo->rewindTranslators();
        }

        $this->translated[$translation_key] = $altered_translations;
        $this->appended_results             = $this->appendResults();

        // cut the $results array
        if (!isset($this->scraper_params["properties"]["amount_of_results"])) {
            $this->tp_logger->info($this->appended_results);
        }
        else {
            $length = $this->scraper_params["properties"]["amount_of_results"];
            // checking the length value
            if (is_numeric($length) and abs($length) < count($this->appended_results)) {
                if ($length > 0)
                    $this->tp_logger->info(array_slice($this->appended_results, 0, $length));
                else //if ($length<0)
                    $this->tp_logger->info(array_slice($this->appended_results, $length + $length));
            } else {
                $this->tp_logger->error([["Could not run translator because: amount_of_results value is not correct. - ScraperFetcherLib.php"]]);
            }
        }

        $this->tp_logger->info("Appended Results Count: " . count($this->appended_results));
    }

    /**
     * fetch sheet-specific data
     *
     * @param $sheet_name
     * @param array $guzzle_params
     */
    private function fetchSheetData($sheet_name, array $guzzle_params): void
    {
        // 1 Build request params
        // 2 Fire request and return the result

        $fetcher = $this->fetchers_params[$this->getFetcherIdx()]['properties'];

        $url = self::API_URL_PREFIX . '/'
            . $fetcher['spreadsheet_info']['spreadsheet_id']
            . '/values/' . urlencode($sheet_name) . '!A1:Z999999?key='
            . $fetcher['spreadsheet_info']['key'];

        $this->executeRequest($this->method_get, $url, $guzzle_params);
        $this->buildResponseResults();
    }

    /**
     * Build N ids Fetcher Params
     * Builds the following N fetchers parameters
     *
     * @param $ids
     * @param $fetcher_idx
     */
    private function buildNIdsFetcherParams($ids, $fetcher_idx): void
    {
        $original_idx = $this->getFetcherIdx();
        $original_fetcher_params = $this->fetchers_params[$fetcher_idx];
        for ($ids_offset = 1; $ids_offset <= $ids; $ids_offset++, $this->incrementFetcherIdx()) {
            if ($ids_offset != 0) {
                $this->fetchers_params[$ids_offset] = $original_fetcher_params;
            }
        }

        $this->fetcher_idx = $original_idx;
    }

    /**
     * Appends all of the translated results
     * If a result does not have another result's column
     * Then null values will be given
     */
    private function appendResults(): array
    {
        //  1 Iterate over all of the results and build "seen_keys" array
        //    - from unique keys
        //  2 Once again iterate over all of the translated results
        //      2.1 For each result(row):
        //      2.2 Append the row's values to the final result
        //      2.3 Add the disjoint set (seen_keys without current row keys) keys as null values
        $seen_keys          = [];
        $final_result       = [];
        $result_idx         = 0;

        foreach ($this->translated as $translator_context) {
            foreach ($translator_context as $sheet_idx => $sheet_context) {
                foreach ($sheet_context as $trans_idx => $trans_row) {
                    foreach (array_keys($trans_row) as $translated_key) {
                        // Unseen key - add as "seen"
                        if (!array_key_exists($translated_key, $seen_keys)) {
                            $seen_keys[$translated_key] = null;
                        }
                    }
                }
            }

        }

        foreach ($this->translated as $translator_context) {
            foreach ($translator_context as $sheet_idx => $sheet_context) {
                foreach ($sheet_context as $trans_idx => $trans_row) {
                    // Add all of the seen values as null
                    // Then merge null array with the actual result's values
                    $final_result[$result_idx] = array_merge($seen_keys, $trans_row);
                    $result_idx++;
                }
            }
        }
        $this->translated = [$final_result];

        return $final_result;
    }

}