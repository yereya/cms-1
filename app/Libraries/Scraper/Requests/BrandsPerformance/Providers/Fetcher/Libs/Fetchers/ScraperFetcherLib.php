<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers;

use App\Entities\Repositories\Reports\Publishers\BrandPerformance\ScraperRepo;
use App\Libraries\Guzzle;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\FetcherTypeContract;
use App\Libraries\TpLogger;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;

/**
 * Class ScraperFetcherLib
 */
abstract class ScraperFetcherLib implements FetcherTypeContract
{
    /**
     * @var int: current operating fetcher
     */
    protected $fetcher_idx = 0;

    /**
     * References the original tp_logger
     * @var TpLogger $tp_logger
     */
    protected $tp_logger;

    /**
     * @var array $translators_params
     * providing parameters for translator
     */
    protected $translators_params = [];

    /**
     * @var array $translated - translated results
     */
    protected $translated = [];

    /**
     * @var string fetcher parameters
     */
    protected $fetchers_params = [];

    /**
     * Related scraper parameters
     * @var $scraper_params
     */
    protected $scraper_params = [];

    /**
     * @var $processed_result
     * normalized parsed result to be returned
     */
    protected $processed_result;

    /**
     * Returned response from executed request
     * @var $response
     */
    protected $fetchers_response = [];

    /**
     * @var $client - guzzle client curl
     */
    protected $client;

    /**
     * @var ScraperRepo $scraper_repo
     */
    protected $scraper_repo;

    /**
     * @var $short_codes - inherited shortcodes lib
     */
    protected $short_codes;

    /**
     * @var $stop_scrape - stop condition for scraper
     */
    protected $stop_scrape = false;

    /**
     * @var string - request methods
     */
    protected $method_post = 'POST';
    protected $method_get = 'GET';
    protected $method_put = 'PUT';
    protected $method_delete = 'DELETE';

    /**
     * @var $previous_translated - previously translated results
     */
    protected $previously_translated;

    /**
     * ScraperFetcherLib constructor.
     *
     * @param array $fetchers_params
     * @param array $scraper_params
     * @param $tp_logger
     * @param ScraperRepo $scraper_repo
     * @param $short_codes
     * @param $client
     * @param null $previously_translated
     */
    public function __construct(array $fetchers_params, array $scraper_params, $tp_logger, ScraperRepo $scraper_repo, $short_codes, $client, $previously_translated = null)
    {
        $this->setFetcherAttributes($client, $scraper_repo, $short_codes);
        $this->tp_logger = $tp_logger;
        $this->scraper_params = $scraper_params;
        $this->previously_translated = $previously_translated;
        $this->fetchers_params[$this->fetcher_idx] = $fetchers_params;
        $this->fetchers_response[$this->fetcher_idx] = $this->client->getResponseHeaders() ?? [];
    }

    /**
     * sets the initial fetcher values
     *
     * @param $client
     * @param $scraper_repo
     * @param $short_codes
     */
    private function setFetcherAttributes($client, $scraper_repo, $short_codes)
    {
        $this->setClient($client);
        $this->setScraperRepo($scraper_repo);
        $this->setShortCodes($short_codes);
    }

    /**
     * @param $scraper_repo setter
     */
    private function setScraperRepo($scraper_repo)
    {
        $this->scraper_repo = $scraper_repo;
    }

    /**
     * scraper repo getter
     *
     * @return mixed
     */
    public function getScraperRepo()
    {
        return $this->scraper_repo;
    }

    /**
     * @param $short_codes setter
     */
    private function setShortCodes($short_codes)
    {
        $this->short_codes = $short_codes;
    }

    /**
     * shortcodes getter
     *
     * @return mixed
     */
    public function getShortCodes()
    {
        return $this->short_codes;
    }

    /**
     * @param $client setter
     */
    private function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * client getter
     *
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * returns the scraping status
     *
     * @return mixed
     */
    public function getStopScrape()
    {
        return $this->stop_scrape;
    }

    /**
     * returns current operating fetcher index
     *
     * @return int
     */
    protected function getFetcherIdx()
    {
        return $this->fetcher_idx;
    }

    /**
     * Increments fetcher index by one
     */
    protected function incrementFetcherIdx()
    {
        return $this->fetcher_idx++;
    }

    /**
     * replaces shortcode values
     */
    protected function readyShortCodeParams()
    {
        $idx = $this->getFetcherIdx();
        $this->fetchers_params[$idx]['properties'] = $this->short_codes->setShortCodes($this->fetchers_params[$idx]['properties'], [
            'scraper_params' => $this->scraper_params,
            'fetchers_params' => $this->fetchers_params,
            'translated' => $this->translated + $this->previously_translated,
            'fetchers_response' => $this->fetchers_response
        ]);
    }

    /**
     * Clears a targeted array from empty params
     *
     * @param $array - array to be cleaned
     */
    protected function clearEmptyParams(&$array)
    {
        foreach ($array as $key => $item) {
            if (is_array($item) AND count($item) === 0) {
                unset($array[$key]);
            }
        }
    }

    /**
     * execute request with given params
     *
     * @param string $method
     * @param $url
     * @param array $params
     * @return $this
     */
    protected function executeRequest($method = 'GET', $url, $params = [])
    {
        $fetched_data = $this->client->fetch(
            $url,
            $params ?? [],
            $method,
            $params['id']
        );

        if (count($this->client->getErrorMsgs())) {
            $this->tp_logger->error(["While attempting to make a curl " => $this->client->getErrorMsgs(true)]);
//            $this->stop_scrape = true;
        }
        $this->fetchers_params[$this->getFetcherIdx()]['_response_idx'] = $this->client->getLastResponseIdx();

        return $fetched_data;
    }

    /**
     * Store response object
     */
    protected function buildResponseResults()
    {
        $request_headers = $this->client->getRequestHeaders();
        $this->tp_logger->debug(
            ["Curl Request headers:", $request_headers],
            ["Response Status:", $this->client->getStatusCode()]
        );

        $this->setCookiesIntoResponse();

        $response_headers = $this->client->getResponseHeaders();
        $this->tp_logger->debug(["Curl Response headers:", $response_headers]);

        $this->fetchers_response[$this->getFetcherIdx()] = [
            'request_headers' => $request_headers,
            'response_headers' => $response_headers
        ];

        $response_idx = $this->fetchers_params[$this->getFetcherIdx()]['_response_idx'];

        $this->tp_logger->debug(["Result from fetcher:" => $this->client->getBody($response_idx)]);
    }

    /**
     * Translate the fetcher response
     *
     * @param $response_idx
     * @param $fetcher_idx
     *
     * @throws \Exception
     */
    protected function translateResponse($response_idx, $fetcher_idx)
    {
        $this->tp_logger->info("---- Inside translateResponse | Start");
        $_idx = 0;
        $translation_key = $this->fetchers_params[$fetcher_idx]['id'];
        do {
            $this->translators_params[$fetcher_idx][$_idx] = $this->scraper_repo->getTranslator();
            $fetcher_translator = $this->translators_params[$fetcher_idx][$_idx];

            if (is_array($fetcher_translator)) {
                $translator_name = key($fetcher_translator);
            } else {
                throw new \Exception('Invalid translator params passed');
            }

            // find the translator
            $current_translator = $this->findAvailableTranslator($translator_name);
            $this->tp_logger->debug([
                "Translator param:" => $fetcher_translator,
                "Translator found:" => $current_translator
            ]);

            // If we found the translator lets attempt to run it
            if ($current_translator) {
                $this->tp_logger->info("---- Inside translateResponse -> before buildValidTranslatorParams");
                // If we already translated once, use previous results as passed value
                $translator_params = $this->buildValidTranslatorParams($fetcher_translator[$translator_name], $response_idx);
                $this->tp_logger->info("---- Inside translateResponse -> After buildValidTranslatorParams, Before fireTranslator");
                // Try to execute the translator
                $_translated = $this->fireTranslator($current_translator, $translator_params, $fetcher_idx);
                $this->tp_logger->info("---- Inside translateResponse -> After fireTranslator");
                if (!isset($this->translated[$translation_key])) {
                    $this->translated[$translation_key] = [];
                }
                foreach ((array)$_translated as $key => $item) {
                    if (is_numeric($key)) {
                        $this->translated[$translation_key][] = $item;
                    } else {
                        $this->translated[$translation_key][$key] = $item;
                    }
                }
            } else {
                $this->tp_logger->error("The specified translator was not found");

                return;
            }
            $_idx++;
        } while ($this->scraper_repo->nextTranslator() !== null);
        $this->tp_logger->info("---- Inside translateResponse | End");
        $this->tp_logger->debug("Translated overall items this far: " . count($this->translated[$translation_key]));
    }

    /**
     * Find Available Translator
     *
     * @param $translator_name
     *
     * @return array|NULL
     */
    protected function findAvailableTranslator($translator_name)
    {
        $available_translators = $this->scraper_repo->getAvailableTranslators();

        $current_translator = null;
        foreach ($available_translators as $translator) {
            if ($translator['name'] == $translator_name) {
                $current_translator = $translator;
                break;
            }
        }

        return $current_translator;
    }

    /**
     * Validates and builds translator parameters
     *
     * @param $params
     * @param $response_idx
     * @return array
     */
    protected function buildValidTranslatorParams($params, $response_idx)
    {
        if ($this->getScraperRepo()->getTranslatorIndex() === 0) {
            $fetched_result = $this->client->getBody($response_idx);
        } else {
            $fetched_result = $this->translated[$response_idx];
            // Since the previous results will be parsed and not the actual fetch response,
            // We are aiming for waterfalling the results into one final result
            $this->translated[$response_idx] = [];
        }

        // Make sure we do not get the empty string ""
        if ($params !== "") {
            $translator_params = explode(',', $params);
        } else {
            $translator_params = [];
        }

        $translator_params = $this->short_codes->setShortCodes($translator_params, [
            'scraper_params' => $this->scraper_params,
            'fetchers_params' => $this->fetchers_params,
            'translated' => $this->translated,
            'fetchers_response' => $this->fetchers_response
        ]);

        return array_merge([$fetched_result], $translator_params);
    }

    /**
     * Will attempt to execute a passed translator
     * will log an exception if unsuccessful
     *
     * @param $translator
     * @param $translator_params
     * @param $fetcher_id
     *
     * @return mixed
     */
    protected function fireTranslator($translator, $translator_params, $fetcher_id)
    {
        $result = null;

        try {
            $translator_lib = new $translator['namespace']($this->scraper_params, $this->fetchers_params[$fetcher_id]);
            $result = call_user_func_array([$translator_lib, $translator['method']], $translator_params);

            if ((int)$this->fetchers_params[$this->getFetcherIdx()]['properties']['concat_n_days_results'] > 0) {
                $this->appendDateColumn($result);
            }
            // cut the $results array
            if (count($result) != 0) {
                if (!isset($this->scraper_params["properties"]["amount_of_results"])) {
                    $this->tp_logger->info($result);
                } else {
                    $length = $this->scraper_params["properties"]["amount_of_results"];
                    // checking the length value
                    if (is_numeric($length) and abs($length) < count($result)) {
                        if ($length > 0)
                            $this->tp_logger->info(array_slice($result, 0, $length));
                        else //if ($length<0)
                            $this->tp_logger->info(array_slice($result, $length + $length));
                    } else {
                        $this->tp_logger->error([["Could not run translator because: amount_of_results value is not correct. - ScraperFetcherLib.php"]]);
                    }
                }
            }
            $this->tp_logger->info("Translated Results Count: " . count($result));
        } catch (\Exception $e) {
            $this->tp_logger->error([["Could not run translator because:" => $e->getMessage()], [$e]]);
        }

        return $result;
    }

    /**
     * Build Date Column
     * adds date column for reports that are requested day by day
     *
     * @param $result_set
     */
    protected function appendDateColumn(&$result_set)
    {
        $event_date = date('Y-m-d', strtotime('-' . $this->fetchers_params[$this->getFetcherIdx()]['properties']['days'] . ' days'));

        foreach ($result_set as &$translator_result) {
            $translator_result['Date'] = $event_date;
        }
    }

    /**
     * Set Cookies Into $this->client->response_params
     */
    public function setCookiesIntoResponse(): bool
    {
        $cookies = $this->resolveCookiesFromJar();

        if (!\count($cookies)) {

            return false;
        }

        $cookies->each(/**
         * @param $cookie array
         */
            function ($cookie) {
                $this->client->setResponseHeader(null, 'Set-Cookie', $cookie);
            });

        return true;
    }

    /**
     * Get Cookies From Guzzle Cookie Jar Object
     *
     * @return mixed
     *
     */
    private function resolveCookiesFromJar()
    {
        /** @var Client $this */
        return collect($this->client->getCookieJar()
            ->toArray())
            ->unique('Name')
            ->map(function ($cookie) {
                return "{$cookie['Name']}={$cookie['Value']}";
            })
            ->flatten();
    }
}
