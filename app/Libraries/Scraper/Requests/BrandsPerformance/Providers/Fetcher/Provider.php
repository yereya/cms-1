<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher;

use App\Entities\Repositories\Reports\Publishers\BrandPerformance\ScraperRepo;
use App\Libraries\Guzzle;
use App\Libraries\Scraper\Provider as BaseProvider;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\CUrlLib;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\GoogleSpreadSheetsLib;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\MailLib;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Libs\Fetchers\ScraperFetcherLib;
use App\Libraries\ShortCodes\Contracts\ShortCodesLib;

/**
 * Class Fetcher
 */
class Provider extends BaseProvider
{
    protected $response = [];

    protected $guzzle;

    protected $scraper_repo;

    protected $short_codes;

    protected $tp_logger;

    protected $result;

    protected $scraper_params = [];

    protected $fetchers_params = [];

    protected $fetchers_response = [];

    protected $translators_params = [];

    protected $scraped_params = [];

    protected $translated = [];

    protected $processor_params = [];

    protected $final_result = [];

    protected $running_in_console = false;

    protected $system_id = 2;

    protected $stop_scrape = false;

    public function __construct($logger, $translator, $settings, $options)
    {
        // Is the script running in the console and activated using the artisan
        $this->running_in_console = \App::runningInConsole();

        $this->initScraperRepo($settings['repo'], $options);
        $this->short_codes = app(ShortCodesLib::class, [new ShortCodes]);
        $this->guzzle      = new Guzzle;
        $this->response    = [];

        // Logger
        $this->tp_logger = $logger;
    }

    private function initScraperRepo($repo, $options = null)
    {
        // were the settings passed actually
        if (is_a($repo, ScraperRepo::class)) {
            $this->scraper_repo = $repo;
        } elseif (isset($options['id'])) {
            $_tasks = explode(',', $options['id']);

            $this->scraper_repo = new ScraperRepo();
            $this->scraper_repo->whereIn('id', $_tasks)->get();
        } else {
            return null;
        }

        $this->scraper_repo->get();
    }

    /**
     * Initiator of this Scraper/s it was passed using the settings var
     */
    public function fetch()
    {
        do {
            $this->guzzle->resetResponsesHistory();
            $this->clearGlobalParams();
            $this->scraper_params = $this->scraper_repo->getScraper();

            // Init Tp Logger task
            // Set the task id and system id (2)
            $scraper_id   = isset($this->scraper_params['id']) ? $this->scraper_params['id'] : null;
            $scraper_name = isset($this->scraper_params['properties']['scraper_name']) ? $this->scraper_params['properties']['scraper_name'] : null;
            $this->tp_logger->setTask($scraper_id)->setSystem($this->system_id);
            $this->tp_logger->info("Scraper: " . $scraper_id . ' - ' . $scraper_name);
            $this->tp_logger->info("Scraper details:", $this->scraper_params);
            $this->tp_logger->info('started ' . get_class($this) . ' request');
            $this->tp_logger->debug(['settings', $this->settings]);
            $this->tp_logger->debug(['options', $this->options]);

            $this->tp_logger->info("---- Inside Fetcher/Provider.php -> fetch() | Start");
            try {
                $this->tp_logger->info("---- Inside Fetcher/Provider.php -> fetch() -> before initScrape | Start");
                $this->initScrape();
                $this->tp_logger->info("---- Inside Fetcher/Provider.php -> fetch() -> before initScrape | End");
                // Did we get translated results
                // to make sure we can continue to processing the translated
                if (!count($this->translated)) {
                    $this->tp_logger->info("No translated results, nothing to process");
                    continue;
                }

                // Process
                if (!$this->scraper_repo->countProcessors()) {
                    $this->tp_logger->info("No processors passed.");
                    continue;
                }
                $this->tp_logger->info("---- Inside Fetcher/Provider.php -> fetch() -> before processTranslatedResults | Start");
                $this->processTranslatedResults();
                $this->tp_logger->info("---- Inside Fetcher/Provider.php -> fetch() -> before processTranslatedResults | End");

                // Save to db
                if (!isset($this->scraper_params['properties']['debug']) OR !$this->scraper_params['properties']['debug']) {
                    if (count($this->final_result)) {
                        $rows_added = $this->saveFinalResults();
                        $this->tp_logger->info("Records sent to out: " . $rows_added);
                    } else {
                        $this->tp_logger->info("No records to save");
                    }
                } else {
                    $this->tp_logger->info("No records were saved to the database [Debug Mod On]");
                }
            } catch (\Exception $e) {
                $this->tp_logger->error($e->getMessage());
            }
        } while ($this->scraper_repo->nextScraper() !== null);
        $this->tp_logger->info("---- Inside Fetcher/Provider.php -> fetch() | End");
    }

    /**
     * Clears all the previously run scraper properties
     */
    private function clearGlobalParams()
    {
        $this->scraper_params     = [];
        $this->fetchers_params    = [];
        $this->translators_params = [];
        $this->processor_params   = [];
        $this->translated         = [];
        $this->final_result       = [];
        $this->stop_scrape        = false;
    }

    /**
     * Starts the scrape process of the current scraper index
     */
    private function initScrape()
    {
        // Steps:
        // 1. Create params array
        //      1. parse all the tags in each one of the params and replace it with it's true value
        // 2. Pass the first fetcher params to the guzzle object
        //      1. Pass the result to the relevant translator
        //      2. Add the translator result to the global settings of the current settings
        // 3. Create the new fetcher params using the results of the last one
        //    repeat number 2 till all out of fetchers.
        // 4. Call the processor object to process the final result and put it into the db.

        if (!$this->scraper_repo->countFetchers()) {
            $this->tp_logger->error("No fetchers found");

            return;
        }
        $fetcher_lib = null;
        $_idx        = 0;
        do {
            // Determine which fetcher lib should fire with the current params
            $this->fetchers_params[$_idx] = $this->scraper_repo->getFetcher();

            $fetcher_lib     = $this->morphFetcherByType($this->fetchers_params[$_idx]);
            $fetched_results = $fetcher_lib->fetch();

            // Appends newly translated results to previous ones
            $this->translated = array_merge($this->translated, $fetched_results);
            $this->updateScraperAttributes($fetcher_lib);

            $_idx++;
        } while ($this->scraper_repo->nextFetcher() !== null && !$this->stop_scrape);
    }

    /**
     * Updates scraper attributes
     *
     * @param ScraperFetcherLib $fetcher_lib
     */
    private function updateScraperAttributes(ScraperFetcherLib $fetcher_lib)
    {
        $this->scraper_repo = $fetcher_lib->getScraperRepo();
        $this->short_codes  = $fetcher_lib->getShortCodes();
        $this->stop_scrape  = $fetcher_lib->getStopScrape();
        $this->guzzle       = $fetcher_lib->getClient();
    }

    /**
     * Decides which fetcher should be created
     *
     * @param array $fetcher
     *
     * @return ScraperFetcherLib
     * @throws \Exception
     */
    private function morphFetcherByType(array $fetcher): ScraperFetcherLib
    {
        switch ($fetcher['properties']['type']) {
            case 'cURL':
                return (new CUrlLib($fetcher, $this->scraper_params, $this->tp_logger, $this->scraper_repo,
                    $this->short_codes, $this->guzzle, $this->translated));
                break;
            case 'Spreadsheet':
                return (new GoogleSpreadSheetsLib($fetcher, $this->scraper_params, $this->tp_logger,
                    $this->scraper_repo, $this->short_codes, $this->guzzle, $this->translated));
                break;
            case 'Mail':
                return (new MailLib($fetcher, $this->scraper_params, $this->tp_logger, $this->scraper_repo,
                    $this->short_codes, $this->guzzle, $this->translated));
                break;
            default:
                throw new \Exception('Could not determine fetcher type');
                break;
        }
    }

    /**
     * Loops through the processors and runs them
     */
    private function processTranslatedResults()
    {
        $_idx = 0;
        do {
            $this->processor_params[$_idx] = $this->scraper_repo->getProcessor();
            $this->tp_logger->info(["Processor params:", $this->processor_params[$_idx]]);

            // Make sure the processor is set to active
            if (!$this->processor_params[$_idx]['active']) {
                continue;
            }

            // Process
            $translated_idx    = endKey($this->translated);
            $_result           = $this->calculateProcess($translated_idx, $_idx);
            $locked_date_state = !empty($this->scraper_params['properties']['locked_date']);

            /*In case locked date checkbox is checked filter by this correct month*/
            if ($locked_date_state) {
                $_result = $this->getLastMonthResults($_result);

                //date passed the 5th in the same month filter results
                if ($this->isDayPassedTheFifthDay()) {
                    $_result = $this->getThisMonthResults($_result);
                }
            }

            $this->tp_logger->info($_result);
            $this->tp_logger->info("Processed results Count: " . count($_result));
            $this->final_result = array_merge(array_values($this->final_result), array_values($_result));

            $_idx++;
        } while ($this->scraper_repo->nextProcessor() !== null);
    }

    /**
     * Calculate the process params and build the data after parsing the short codes
     *
     * @param $translated_idx
     * @param $processor_idx
     *
     * @return array
     */
    private function calculateProcess($translated_idx, $processor_idx)
    {
        $_final_result = [];
        $processor     = $this->processor_params[$processor_idx];

        $_idx = 0;
        foreach ((array)$this->translated[$translated_idx] as $translated) {

            // Validates the rule of the processor
            if ($this->validateProcessorRuleCondition($processor['rule'], $translated)) {

                try {
                    // Running on each of the properties of the processor
                    foreach ($processor['properties'] as $processor_key => $processor_value) {
                        if ($short_codes = $this->short_codes->getShortCodesAtts($processor_value)) {
                            $processor_value = $this->replaceProcessShortCode($short_codes, $translated,
                                $processor_value);

                            // used for special short codes like [or]
                            $processor_value = $this->resolveProcessedValueCondition($processor_value);
                        }

                        $_final_result[$_idx][$processor_key] = $processor_value;
                    }
                    $_final_result[$_idx]['event'] = strtolower($_final_result[$_idx]['event']);

                    // TODO needs to throw exception and log and skip
                    if (!is_numeric($_final_result[$_idx]['amount'])) {
                        $_final_result[$_idx]['amount'] = 0;
                    }
                    if (!is_numeric($_final_result[$_idx]['commission_amount'])) {
                        $_final_result[$_idx]['commission_amount'] = 0;
                    }
                } catch (\Exception $e) {
                    $this->tp_logger->error("Could not translate item:" . $e->getMessage(), $translated);
                }
            }
            $_idx++;
        }

        return $_final_result;
    }

    private function validateProcessorRuleCondition($rule, $translated)
    {
        $short_codes = $this->short_codes->getShortCodesAtts($rule);
        if (!$short_codes) {
            return true;
        }

        $processor_value = $this->replaceProcessShortCode($short_codes, $translated, $rule);
        $rule_arr        = explode(',', $processor_value);

        // Go through the rule if results and calculate the final result
        // true or false;
        return $this->validateCondition($rule_arr);
    }

    /**
     * @param $short_codes
     * @param $translated
     * @param $processor_value
     *
     * @return mixed
     */
    private function replaceProcessShortCode($short_codes, $translated, $processor_value)
    {
        foreach ($short_codes as $short_code) {
            $short_code_result = $this->short_codes->doShortCode($short_code, $translated);

            // This is used for when the the value returned is actually a true or false boolean
            // instead of string
            if ($short_code_result === true OR $short_code_result === false) {
                $short_code_result = var_export($short_code_result, true);
            }

            $processor_value = str_replace($short_code['raw'], $short_code_result, $processor_value);
        }

        return $processor_value;
    }

    /**
     * Runs through a condition array and
     *
     * @param array $conditions_arr
     *
     * @return bool
     */
    private function validateCondition($conditions_arr)
    {
        if (count($conditions_arr) == 1) {
            return ($conditions_arr[0] == 'true') ? true : false;
        } elseif (count($conditions_arr) % 2 != 0) {
            for ($i = 2; $i < count($conditions_arr); $i += 2) {
                switch ($conditions_arr[$i - 1]) {
                    case 'or' :
                        if ($conditions_arr[$i - 2] == 'true' or $conditions_arr[$i] == 'true') {
                            $conditions_arr[$i] = 'true';
                        } else {
                            $conditions_arr[$i] = 'false';
                        }
                        break;
                    case 'and' :
                        if ($conditions_arr[$i - 2] == 'true' and $conditions_arr[$i] == 'true') {
                            $conditions_arr[$i] = 'true';
                        } else {
                            $conditions_arr[$i] = 'false';
                        }
                        break;
                    default:
                        return false;
                }
            }

            return ($conditions_arr[endKey($conditions_arr)] == 'true') ? true : false;
        } else {
            $this->tp_logger->info("Some weird condition was passed.");

            return false;
        }
    }

    /**
     * Resolve Processed Value Condition - Will search for the [or] short code
     * and return the first segment that has value
     *
     * @param $string
     *
     * @return mixed
     */
    private function resolveProcessedValueCondition($string)
    {
        if (strpos($string, ',or,') !== false) {
            $segments = explode(',or,', $string);
            foreach ((array)$segments as $segment) {
                if (!empty($segment)) {
                    return $segment;
                }
            }
        }

        return $string;
    }

    /**
     * Saves the final parsed and processed results
     * and returns the number of successfully updated/added records
     *
     * @return integer
     */
    private function saveFinalResults()
    {
        $save_to_adserver = false;
        if (isset($this->scraper_params['properties']['save_to_adserver'])) {
            $save_to_adserver = $this->scraper_params['properties']['save_to_adserver'] ? true : false;
        }

        return $this->scraper_repo->saveResults($this->final_result, $save_to_adserver);
    }

    /**
     * Get Fetcher By Response Id
     *
     * @param $id
     *
     * @return mixed|null
     */
    private function getFetcherByResponseId($id)
    {
        foreach ((array)$this->fetchers_params as $fetcher) {
            if ($fetcher['_response_idx'] == $id) {
                return $fetcher;
            }
        }

        return null;
    }

    /**
     * @param $results
     *
     * @return array
     */
    private function getThisMonthResults($results)
    {
        return $this->getResultsByPeriod($results, 'first day of this month');
    }

    /**
     * @param $results
     *
     * @return array
     */
    private function getLastMonthResults($results)
    {
        return $this->getResultsByPeriod($results, 'first day of last month');
    }

    /**
     * @return bool
     */
    private function isDayPassedTheFifthDay()
    {
        return date('Y-m-d') > date('Y-m-05');
    }

    /**
     * Filter results by locked date (5th in current month)
     *
     * @param $results
     * @param $period
     *
     * @return array
     */
    private function getResultsByPeriod($results, $period)
    {
        return collect($results)
            ->filter(function ($result) use ($period) {
                $start_of_the_month = date_create()->modify($period)->setTime(0,0,0);
                $current_month      = date_create($result['date']);

                return $current_month >= $start_of_the_month;
            })->toArray();
    }
}