<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators;

use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel\ExcelHelper;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel\Match;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel\MedicalGuardian;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel\MondayMail;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel\Vcita;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel\Lightspeed;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers\GeneralHelper;
use App\Libraries\TpLogger;
use PHPExcel_IOFactory;

/**
 * Class ExcelLib
 *
 * @package App\Libraries\Scraper\Brands\Translators
 */
class ExcelLib extends BaseTranslator
{
    use GeneralHelper;
    use ExcelHelper;
    use MedicalGuardian;
    use Match;
    use MondayMail;
    use Vcita;
    use Lightspeed;

    /**
     * Basic excel file translation
     *
     * @param $excel_str
     * @param string|integer $offset
     * @return null
     * @throws \PHPExcel_Reader_Exception
     */
    public function translate($excel_str, $offset = 0)
    {

        // Create excel reader object(PHPExcel)
        $php_excel_obj = $this->createExcelReader($excel_str);

        if (empty($php_excel_obj)) {

            return null;
        }

        $excel_sheets = $this->getAllSheets($php_excel_obj);


        $results = array_flatten($excel_sheets, 1);

        if ($offset) {
            $results = array_slice($results, $offset);
        }

        return $results;

    }

    /**
     * Excel file translation with partial sheets
     *
     * @param $excel_str
     * @param string|integer $offset
     * @param string|integer $limit
     * @param string|integer $specific_sheets
     * @return null
     * @throws \PHPExcel_Reader_Exception
     */
    public function translateSpecificSheets($excel_str, $offset = 0, $limit = 0, $specific_sheets = '')
    {
        // Create excel reader object(PHPExcel)
        $php_excel_obj = $this->createExcelReader($excel_str);
        $filtered_excel_sheets = [];

        if (empty($php_excel_obj)) {
            return null;
        }
        $excel_sheets = $this->getAllSheets($php_excel_obj);
        $specific_sheets = explode(';',$specific_sheets);
        foreach ($specific_sheets as $spreadsheet_id){
            try{
                $filtered_excel_sheets[] = $excel_sheets[$spreadsheet_id];
            }catch (\Exception $e){
                throw new \Exception('Error while processing specific_sheets param. translateSpecificSheets -> ExcelLib.php. Error : '.$e);
            }
        }

        $results = array_flatten($filtered_excel_sheets, 1);

        if ($offset) {
            $results = array_slice($results, $offset);
        }

        return $results;

    }
    /**
     * Indexing the results by combining the defined headers with every row's values
     *
     * @param     $excel_str
     * @param int $offset
     * @param int $headers_offset
     * @param int $limit
     *
     * @return array
     * @throws \PHPExcel_Reader_Exception
     */
    public function indexTableByHeaders($excel_str, $offset = 0, $headers_offset = 0, $limit = -1)
    {
        $tp_logger = TpLogger::getInstance();
        $final_results      = [];

        try {
            $translated_results = self::translate($excel_str, $offset, $limit);
        } catch (\Exception $e) {
            $tp_logger->warning([["Could not run translator because:" => $e->getMessage()], [$e]]);
            return $final_results;
        }

        if ($translated_results == [] or $translated_results == null)
            return $final_results;

        // Defines first row as headers
        $headers = array_values($translated_results[$headers_offset]);

        for ($i = $offset; $i < count($translated_results) + $limit; $i++) {
            $translated_result = $translated_results[$i];

            if ($translated_result == $headers) {
                continue;
            }

            // Finds if there non-existing keys
            $missing_keys = array_diff_key($headers, $translated_result);
            // Finds the complementary column data
            $complementary_data = array_fill(count($translated_result), count($missing_keys), "");
            // Appends the missing keys/values to the original keys and values
            $final_results[] = array_combine($headers, $translated_result + $complementary_data);
        }

        return $final_results;
    }

    /**
     * Creates and returns an excel reader object
     *
     * @param string $excel_str
     *
     * @return \PHPExcel
     * @throws \PHPExcel_Reader_Exception
     */
    private function createExcelReader($excel_str = '')
    {
        // Creates a temporary excel file to be parsed
        $file_name = $this->createExcelFile($excel_str);

        // Uses PHPExcel reader object to load the file as an object
        $php_reader    = PHPExcel_IOFactory::createReaderForFile($file_name);
        $reader_object = $php_reader->load($file_name);

        return $reader_object ?? false;
    }

    /**
     * Creates a temporary excel file for the PHPExcel reader object
     *
     * @param string $excel_str
     *
     * @return bool|string
     */
    private function createExcelFile($excel_str = '')
    {
        // Temporary file
        $temp_file_name = tempnam(sys_get_temp_dir(), "tmpexcel");
        // Write the excel string into the file
        file_put_contents($temp_file_name, $excel_str);

        return $temp_file_name;
    }

    /**
     * Parses all sheets as arrays and returns a collection of them
     *
     * @param \PHPExcel $php_excel_obj
     *
     * @return array
     */
    private function getAllSheets($php_excel_obj)
    {
        $result = [];
        // Get all the sheets as an object collection
        $excel_worksheets = $php_excel_obj->getAllSheets();

        // Push the excel worksheet results to the result array
        foreach ($excel_worksheets as $excel_worksheet) {
            $result[] = $excel_worksheet->toArray();
        }

        return $result;
    }

    /**
     * Excel file translation without sign and with integer numbers
     *
     * @param $excel_str
     * @param string|integer $offset
     * @param string $commission_col
     * @return null
     * @throws \PHPExcel_Reader_Exception
     */
    public function translateWithoutSign($excel_str, $offset = 0,$commission_col = null)
    {
        $tp_logger = TpLogger::getInstance();
        // Create excel reader object(PHPExcel)
        $php_excel_obj = $this->createExcelReader($excel_str);

        if (empty($php_excel_obj)) {

            return null;
        }

        $excel_sheets = $this->getAllSheets($php_excel_obj);

        $translated_results = array_flatten($excel_sheets, 1);

        if ($offset) {
            $translated_results = array_slice($translated_results, $offset);
        }

        $final_results = [];

        // Defines first row as headers
        $headers = array_values($translated_results[0]);

        for ($i = $offset; $i < count($translated_results) ; $i++) {
            $translated_result = $translated_results[$i];

            if ($translated_result == $headers) {
                continue;
            }

            $translated_result= self::removeSigns($translated_result);


            // Finds if there non-existing keys
            $missing_keys = array_diff_key($headers, $translated_result);
            // Finds the complementary column data
            $complementary_data = array_fill(count($translated_result), count($missing_keys), "");
            // Appends the missing keys/values to the original keys and values
            $combined_array = array_combine($headers, $translated_result + $complementary_data);
            if(isset($commission_col)){
                try{
                    if($combined_array[$commission_col] == 0 ){
                        $combined_array['zero_column'] = 'Yes';
                    }else{
                        $combined_array['zero_column'] = 'No';
                    }
                }catch (\Exception $e){
                    $tp_logger->info('Error in creating commission column. ' . $e);
                }
            }
            $final_results[] = $combined_array;
        }

        return $final_results;

    }

    /**
     * Remove signs and convert to integer numbers
     * @param $values
     * @return array
     *
     */
    public function removeSigns($values)
    {
        $arr = [];
        foreach($values as $param){
            $param = str_replace( "£", "",$param);
            $param = str_replace( "$", "",$param);
            $param = str_replace( "\n", "",$param);
            if (is_numeric($param))
                $param = doubleval($param);
            $arr[] = $param;
        }
        return $arr;
    }



}

