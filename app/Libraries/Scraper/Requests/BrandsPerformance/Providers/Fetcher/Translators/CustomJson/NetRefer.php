<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

trait NetRefer
{
    /**
     * NetRefer Customer New UI
     * Custom translator for netrefer Customer report in new UI version
     *
     * @param        $json
     * @param        $selector
     * @param string $key
     * @param string $value
     * @param string $date_to_compare
     * @param string $default_token
     *
     * @return array
     */
    public function netReferCustomerNewUI(
        $json,
        $selector,
        $key = 'key',
        $value = 'value',
        $date_to_compare,
        $default_token = '',
        $default_casino_token = null
    ) {
        $normalized_result   = [];
        $translated_result   = $this->netReferTranslate($json, $selector, $key, $value);
        $event_date          = date('Y-m-d', strtotime('-' . $this->fetcher['properties']['days'] . ' days'));
        $processed_date      = new \DateTime();
        $date_to_compare_obj = new \DateTime($date_to_compare);

        foreach ($translated_result as $translated_item) {
            $normalized_item = $this->normalizeItem($translated_item);

            $normalized_item['Date'] = $event_date;
            if (!empty($normalized_item['CPAProcessedDate'])) {
                $processed_date->modify($normalized_item['CPAProcessedDate']);
                $normalized_item['CPAProcessedDate'] = $processed_date->format('Y-m-d');
            } else {
                $processed_date->modify('2016-01-01');
            }

            $normalized_item['_MarketingSourcename'] = $normalized_item['MarketingSourcename'];
            if ($normalized_item['MarketingSourcename']  == $default_token) {
                $normalized_item['MarketingSourcename']  = $normalized_item['CustomerReferenceID'];
                $normalized_item['_MarketingSourcename'] = $default_casino_token ?? $normalized_item['CustomerReferenceID'];
            }

            $normalized_item['SignupDateReformatted'] = date('Y-m-d', strtotime($normalized_item['SignupDate']));

            if ($processed_date > $date_to_compare_obj) {
                $normalized_item['pass_cond'] = "true";
            } else {
                $normalized_item['pass_cond'] = "false";
            }

            $normalized_result[] = $normalized_item;
        }

        return $normalized_result;
    }

    /**
     * json translator from json format
     * [
     *      'key' => param,
     *      'value' => param
     * ],
     * [
     *      'key' => param,
     *      'value' => param
     * ]
     *
     * @param        $json - string json
     * @param null   $selector - where in json, default null, maybe array
     *        example:
     *              format 1: request.data.data, key, value
     *              format 2: ['request data data', 'request value'], key, value
     * @param string $key - what is name to field key in json, default key
     * @param string $value - what is name to field value in json, default value
     *
     * @return array
     */
    public function netReferTranslate($json, $selector = null, $key = 'key', $value = 'value')
    {
        $this->jsonToArray($json);

        if (is_null($selector)) {
            return $json;
        }

        $this->parseSelector($selector);
        $result = [];
        foreach ($selector as $sel) {
            $this->parseJson($json, $sel, 0, $result);
        }

        $this->parseNetReferJson($result, $key, $value);

        return $result;
    }

    /**
     *
     *
     * @param $json
     * @param null $selector
     * @param string $key
     * @param string $value
     * @return array
     */
    public function netReferMrGreen($json, $selector = null, $key = 'key', $value = 'value')
    {
        // 1. Build unique combinations array
        // 2. Count combination repetitions to indicate a needed counter
        // 3. Append the resulting string as [token-date-counter]
        $parsed_results = [];
        $date_token_combinations = [];

        // Normally translated results
        $results = $this->netReferTranslate($json, $selector, $key, $value);

        foreach ($results as $result) {
            $combination = $result['subid'] . '-' . $result['Date'];

            // If we have not seen this [token-date] combination yet
            if (!array_key_exists($combination, $date_token_combinations)) {
                // Initializes combination counter
                $date_token_combinations[$combination] = 0;
            } else {
                // Increment the amount of times we have seen this [token-date] combination
                $date_token_combinations[$combination]++;
            }

            // There is at least one depositing customer
            if ( ($_deposits = ($result['FirstTimeDepositingCustomers'] ?? 0)) > 0 ) {
                $parsed_results[] = $result + ['counter_col' => $combination . '-0'];

                // Adds a counter row for each deposit
                for ($i = 1; $i < $_deposits; $i++) {
                    $parsed_results[] = $result + ['counter_col' => $combination . '-' . $i];
                }
            } else {
                // Adds a row according to the accumulating combination
                $parsed_results[] = $result + ['counter_col' => $combination . '-' . $date_token_combinations[$combination]];
            }
        }

        return $parsed_results;
    }

    /**
     * combine array to key value format
     *
     * @param $result - associative array with arrays format field key and field value
     * @param $key - what is name to field key in json
     * @param $value - what is name to field value in json
     */
    private function parseNetReferJson(&$result, $key, $value)
    {
        foreach ($result as $index => $arr) {
            $temp = [];

            foreach ($arr as $el) {
                if (isset($el[$key]) && isset($el[$value])) {
                    $temp[$el[$key]] = $el[$value];
                }
                if ($el[$key] == 'MarketingSourceID'){
                    $temp[$el[$key]] = preg_replace('/<[^>]*>/','', $el[$value]);
                }
            }

            $result[$index] = $temp;
        }
    }

    /**
     * Normalize Item
     * populate all the required fields
     *
     * @param $item
     *
     * @return mixed
     */
    private function normalizeItem($item)
    {

        $normalized_item['CustomerReferenceID'] = strip_tags($this->validateField($item, 'CustomerReferenceID'));
        $normalized_item['Alias']               = $this->validateField($item, 'Alias');
        $normalized_item['Country']             = $this->validateField($item, 'Country');
        $normalized_item['SignupDate']          = $this->validateField($item, 'SignupDate');
        $normalized_item['RewardPlan']          = $this->validateField($item, 'RewardPlan');
        $normalized_item['MarketingSourcename'] = $this->validateField($item, 'MarketingSourcename');
        $normalized_item['URL']                 = $this->validateField($item, 'URL');
        $normalized_item['ExpiryDate']          = $this->validateField($item, 'ExpiryDate');
        $normalized_item['CustomerType']        = $this->validateField($item, 'CustomerType');
        $normalized_item['CPAProcessedDate']    = $this->validateField($item, 'CPAProcessedDate');
        $normalized_item['Deposits']            = $this->validateField($item, 'Deposits');
        $normalized_item['TotalNetRevenue']     = $this->validateField($item, 'TotalNetRevenue');
        $normalized_item['TotalNetRevenueMTD']  = $this->validateField($item, 'TotalNetRevenueMTD');
        $normalized_item['Details']             = $this->validateField($item, 'Details');
        $normalized_item['TotalNetRevenue']     = $this->validateField($item, 'TotalNetRevenue');

        return $normalized_item;
    }

    private function validateField($item, $field)
    {
        return isset($item[$field]) ? $item[$field] : '';
    }
}