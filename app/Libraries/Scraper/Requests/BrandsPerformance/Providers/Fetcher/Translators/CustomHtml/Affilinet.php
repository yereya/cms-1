<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait Affilinet
{
    /**
     * Affilinet
     * Affilinet custom translator
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     * @param int    $limit
     *
     * @return mixed
     */
    public function affilinet($html, $table_selector = null, $header_selector = 'th', $limit = 0)
    {
        $ioId_list = [
            'DatingCafe.de(8714)'                                     => '578c98feac3d956c483d692a',
            'GenerationGold.de(13975)'                                => '578c9954ac3d956c483d6934',
            'Secret DE - Lebe Deine Phantasie(8495)'                  => '578cfdf242364dfb1daa762a',
            'Bildkontakte.de - Top-Konditionen(2995)'                 => '578c980fac3d956c483d691c',
            'Lovepoint - Liebe oder Abenteuer für Jedermann(623)'     => '578c989aac3d956c483d6924',
            'Singletreffen.de - Partnersuche Flirten verlieben(2215)' => '578c977aac3d956c483d6916',
        ];

        $this->replaceCharsetToUTF8($html);
        $translated_result = self::translate($html, $table_selector, $header_selector, $limit);

        foreach ($translated_result as &$translated_item) {
            // set the event type
            if (strpos($translated_item['Rate mode'], 'Lead')) {
                $translated_item['event'] = 'lead';
            } elseif (strpos($translated_item['Rate mode'], 'Sale')) {
                $translated_item['event'] = 'sale';
            }

            $translated_item['ioid'] = isset($ioId_list[$translated_item['Program name']]) ? $ioId_list[$translated_item['Program name']] : '';

            // we have date in d/m/Y format which seperator is american but the order of components is european.
            // Dates in the m/d/y or d-m-y formats are disambiguated by looking at the separator
            // between the various components: if the separator is a slash (/), then the American m/d/y is assumed;
            // whereas if the separator is a dash (-) or a dot (.), then the European d-m-y format is assumed.
            $translated_item['meta_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $translated_item['Date'])));

            // remove the first char that is currency sign
            $translated_item['meta_commission'] = mb_substr($translated_item['Commission'], 1);
        }

        return $translated_result;
    }

    /**
     * Replace Charset To UTF8
     * In this brand we have meta tag charset set to UNICODE-1-1.
     * In order the html crawler to find selectors we have to change it to UTF-8
     *
     * @param $html
     */
    private function replaceCharsetToUTF8(&$html)
    {
        $html = str_replace('UNICODE-1-1', 'utf-8', $html);
    }

    /**
     * Affilinet Translate Value
     * This function get as input selectors and searches html for their value.
     * Similar to translateValue in htmlLib, except the need to change charset
     *
     * @param        $html
     * @param null   $selectors
     * @param string $var_name
     * @param null   $attribute_name
     *
     * @return mixed
     */
    public function affilinetTranslateValue(
        $html,
        $selectors = null,
        $var_name = 'HtmlLib@translateValue',
        $attribute_name = null
    ) {
        $this->replaceCharsetToUTF8($html);

        return $this->translateValue($html, $selectors, $var_name, $attribute_name);
    }
}