<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\JsonLib;

trait SendinBlue
{
    /**
     * A generic expression matching translator to get the token param from sendinBlue's system
     *
     * @param        $html
     * @param string $expressions
     * @param string $var_name
     *
     * @return mixed|string
     */
    public function sendinBlueExpressionMatch($html, $expressions = '{"token":"(.*)"[,"]',
        $var_name = 'HtmlLib@sendinBlueExpressionMatch')
    {
        // Create a key=>value array from variable names and expressions by corresponding indexes
        $expression_names_arr = array_combine(explode(';', $var_name), explode(';', $expressions));

        $results = [];
        $match   = [];
        foreach ($expression_names_arr as $expression_name => $expression) {
            // Append resulting match to the results array, prefer group regex
            preg_match('/' . $expression . '/', $html, $match);
            $results[$expression_name] = $match[1] ?? ($match[0] ?? null);
        }

        return $results;
    }

    /**
     * SandingBlue new api translator
     * 1. get json results
     * 2. iterate over sale + lead fields ( according to fields name
     * 3. divide iteration by specific field
     **
     *
     * @param        $json
     * @param        $selector
     * @param string $date_col_name
     * @param        $lead_col_name
     * @param        $sale_col_name
     * @param string $column_to_divide
     * @param        $number_to_divide_by
     * @param null   $item_group_selector
     *
     * @return mixed
     */
    public function sendinBlueNewSystem($json, $selector, $date_col_name = 'generic_date', $lead_col_name,
        $sale_col_name,
        $column_to_divide = 'approved_commission_amount', $number_to_divide_by,
        $item_group_selector = null)
    {
        $final_results = [];
        $result        = (new JsonLib())->translate($json, $selector, $item_group_selector);

        collect($result)->each(function ($record, $idx) use (
            $lead_col_name, $sale_col_name, &$final_results, $number_to_divide_by, $column_to_divide, $date_col_name
        ) {

            /*Leads*/
            if (!empty($record[$lead_col_name]) && $record[$lead_col_name] > 0) {

                $number_of_iterations = $column_to_divide === $lead_col_name
                    ? floor((int)$record[$lead_col_name] / (int)$number_to_divide_by)
                    : $record[$lead_col_name];

                for ($i = 1; $i <= $number_of_iterations; $i++) {
                    $cols_to_amount = [
                        'date'         => $record[$date_col_name],
                        'counter_col'  => $record[$date_col_name] . '-' . $i,
                        $lead_col_name => 1
                    ];
                    $temp           = $this->prepareFields($record, $cols_to_amount);

                    $final_results[] = $temp;
                }
            }

            /*Sales*/
            if (!empty($record[$sale_col_name]) && $record[$sale_col_name] > 0) {

                $number_of_iterations = $sale_col_name === $column_to_divide
                    ? floor((int)$record[$sale_col_name] / (int)$number_to_divide_by)
                    : $record[$sale_col_name];

                for ($i = 1; $i <= $number_of_iterations; $i++) {
                    $cols_to_amount = [
                        'date'         => $record[$date_col_name],
                        'counter_col'  => $record[$date_col_name] . '-' . $i,
                        $sale_col_name => $record[$sale_col_name]
                    ];

                    $temp = $this->prepareFields($record, $cols_to_amount);

                    $final_results[] = $temp;
                }
            }
        });

        return $final_results;
    }

    /**
     * @param $record
     * @param $cols_to_amount
     *
     * @return array
     */
    public function prepareFields($record, $cols_to_amount): array
    {
        $result = array_fill_keys(array_keys($record), 0);

        collect($cols_to_amount)->each(function ($amount, $name) use (&$result) {
            $result[$name] = $amount;
        });

        return $result;
    }


    /**
     * @param      $csv_string
     * @param int  $offset
     * @param int  $limit
     * @param null $delimiter
     *
     * @return array
     */
    public function sendinBlue($csv_string, $offset = 0, $limit = 0, $delimiter = null)
    {
        $results = $this->translate($csv_string, $offset, $limit, $delimiter);

        return array_filter($results, function ($record) {
            return $record['type'] == 'conversion';
        });
    }
}
