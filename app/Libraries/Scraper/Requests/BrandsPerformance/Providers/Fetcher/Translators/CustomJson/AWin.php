<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait AWin
{

    /**
     * trait AWin

     * Translate json and exstract from inner array commissions and clickRef
     *
     * @param      $json
     * @param null $offset
     *
     * @return mixed
     */


    public function AWin($json, $offset = null)
    {
        $translated_result = self::translate($json, $offset);
        $final_result = [];

        foreach ($translated_result as $res){
            $res['clickRef'] = $res['clickRefs']['clickRef'];
            $res['commission'] = doubleval($res['commissionAmount']['amount']);
            $final_result[] = $res;
        }


        return $final_result;
    }

}