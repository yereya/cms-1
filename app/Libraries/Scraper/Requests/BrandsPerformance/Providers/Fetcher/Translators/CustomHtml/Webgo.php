<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait Webgo
{
    public function webgo($html, $table_selector = null, $header_selector = 'th', $limit = 0)
    {
        $translated_result       = self::translate($html, $table_selector, $header_selector, $limit);
        $fixed_translated_result = [];

        $month = $this->fetcher['properties']['form_params']['monat'];
        $year  = $this->fetcher['properties']['form_params']['jahr'];

        foreach ($translated_result as $translated_item) {

            $_event          = $translated_item;
            $_event['date']  = "$year-$month-{$translated_item['Day']}";
            $_event['token'] = '';
            $sales_count     = $translated_item['Sales'];

            // if we have sales clone the event as the sales number
            if ($sales_count > 0) {
                for ($i = 1; $i <= $sales_count; $i++) {
                    $_event['token']           = $_event['date'] . "-$i";
                    $fixed_translated_result[] = $_event;
                }
            } else {
                $_event['token']           = "{$_event['date']}-0";
                $fixed_translated_result[] = $_event;
            }
        }

        return $fixed_translated_result;
    }
}