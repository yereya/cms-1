<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait TrendMicro
{
    /**
     * Trend Micro
     * Trend Micro custom translator which adds counter
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     * @param int    $limit
     *
     * @return array
     */
    public function trendMicro($html, $table_selector = null, $header_selector = 'th', $limit = 0)
    {
        $translated_result       = self::translate($html, $table_selector, $header_selector, $limit);
        $fixed_translated_result = [];

        foreach ($translated_result as $translated_item) {

            $_event      = $translated_item;
            $sales_count = $translated_item['Items'];

            // if we have sales>1 clone the event as the sales number
            if ($sales_count > 1) {
                for ($i = 1; $i <= $sales_count; $i++) {
                    $_event['_trx-id']         = "trendmicro-sale-{$_event['Sub ID 1']}-$i";
                    $fixed_translated_result[] = $_event;
                }
            } else {
                $_event['_trx-id']         = "trendmicro-sale-{$_event['Sub ID 1']}";
                $fixed_translated_result[] = $_event;
            }
        }

        return $fixed_translated_result;
    }
}