<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators;

use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers\GeneralHelper;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\SendinBlue;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\BeNaughty;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Brand_888;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\FreshDesk;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\freshSales;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\GoogleSpreadSheets;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\ImpactRadius;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\K8;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\NetBet;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\NetRefer;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\PostAffiliatePro;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Proggio;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\SunBingo;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\VirtualPBX;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Wrike;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Zanox;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\MondayGoogleSpreadSheet;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Hubspot;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\AWin;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Vend;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\translateWithGroupsSelector;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Cove;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Adjust;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson\Lull;


/**
 * Class JsonLib
 *
 * @package App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators
 */
class JsonLib extends BaseTranslator
{
    use Wrike;
    use Zanox;
    use NetBet;
    use SunBingo;
    use NetRefer;
    use BeNaughty;
    use Brand_888;
    use ImpactRadius;
    use GeneralHelper;
    use PostAffiliatePro;
    use GoogleSpreadSheets;
    use SendinBlue;
    use FreshDesk;
    use VirtualPBX;
    use K8;
    use Proggio;
    use MondayGoogleSpreadSheet;
    use Hubspot;
    use AWin;
    use Vend;
    use translateWithGroupsSelector;
    use freshSales;
    use Cove;
    use Adjust;
    use Lull;

    /**
     * @var string
     */
    protected $attribute = 'value';

    /**
     * @param      $json
     * @param null $selector
     * @param null $item_group_selector
     *
     * @return mixed
     */
    public function translate($json, $selector = null, $item_group_selector = null)
    {
        $this->jsonToArray($json);

        if (is_null($selector)) {
            return $json;
        }

        $this->parseSelector($selector);
        $result = [];
        foreach ($selector as $sel) {
            $this->parseJson($json, $sel, 0, $result);
        }

        // in case we have a nested structure like in documentation to flattenJson function
        if ($item_group_selector) {
            $result = $this->flattenJson($result, $item_group_selector);
        }
        return $result;
    }

    public function lightspeedTranslator($json, $selector = null, $item_group_selector = null)
    {
        $this->jsonToArray($json);

        if (is_null($selector)) {
            return $json;
        }

        $this->parseSelector($selector);
        $result = [];
        foreach ($selector as $sel) {
            $this->parseJson($json, $sel, 0, $result);
        }

        // in case we have a nested structure like in documentation to flattenJson function
        if ($item_group_selector) {
            $result = $this->flattenJson($result, $item_group_selector);
        }
        $new_arr = [];
        $idx = 0;
        foreach ($result as $res){
            foreach($res['Stat'] as $key => $val){
                $new_arr[$idx][$key] = $val;
            }
            $new_arr[$idx]['name'] = $res['Goal']['name'];
            $idx += 1;
        }
        return $new_arr;
    }

    /**
     * Translate Value
     * Assigns a given selector's attribute to $var_name for fetcher usage
     *
     * @param $json
     * @param null $selectors
     * @param string $var_name
     * @param null|string $attribute_name
     *
     * @param null $url_param
     * @return array
     * @throws \Exception
     */
    public function translateValue(
        $json,
        $selectors      = null,
        $var_name       = 'JsonLib@translateValue',
        $attribute_name = null,
        $url_param      = null
    ) {
        // Replaces a special '{' character
        $json = preg_replace('/﻿{(.*)}/', '{$1}', $json);
        if (!isJson($json)) {
            throw new \Exception('Invalid Json format. Unable to translate.');
        }

        $this->jsonToArray($json);

        if (!$json) {
            throw new \Exception('Given parameters were wrong');
        }

        $selectors_arr = explode(';', $selectors);
        $var_name_arr = explode(';', $var_name);
        $selector_name_pairs = array_combine($selectors_arr, $var_name_arr);

        $result = [];

        foreach ($selector_name_pairs as $selector => $name) {

            if ( !isset($json[$selector]) ) {
                continue;
            }

            if ( !empty($attribute_name) )
                $result[$name] = $json[$selector][$attribute_name];
            else
                $result[$name] = $json[$selector];
        }

        if ($url_param) {
            foreach ($result as $key => $item) {
                $parameter = $this->extractUrlSubQueryParam($item, $url_param, '&');

                if (!empty($parameter)) {
                    $result[$key] = $parameter;
                }
            }
        }

        return $result;
    }

    /**
     * convert json to array
     *
     * @param $json
     */
    private function jsonToArray(&$json)
    {
        $json = json_decode($json, true);
    }

    /**
     * convert string selector to array
     *
     * @param $selector
     */
    private function parseSelector(&$selector)
    {
        $temp = [];

        if (is_array($selector)) {
            foreach ($selector as $sel) {
                $temp[] = explode(' ', $sel);
            }
        } else {
            $temp[] = explode('.', str_replace(' ', '', $selector));
        }

        $selector = $temp;
    }

    /**
     * get needed data from array
     *
     * @param $json - json converted to array
     * @param $selector - array [node1, node2, ...]
     * @param $key - start default 0
     * @param $result - return result
     */
    private function parseJson(&$json, $selector, $key, &$result)
    {
        if (count($selector) == ++$key) {
            // check if the selector is set
            // otherwise break out of the recursion
            if (!isset($json[$selector[0]])) {
                return;
            }

            $temp = $json[$selector[0]];
            for ($i = 1; $i < $key; $i++) {
                // check if the sub-selector is set
                if(!isset($temp[$selector[$i]])){
                    return;
                }
                $temp = $temp[$selector[$i]];
            }
            $this->isAssoc($temp) ? $this->findEnd($temp, $result) : $result = $temp;
        } else {
            $this->parseJson($json, $selector, $key, $result);
        }
    }

    /**
     * Checked if array associative
     *
     * @param $arr
     *
     * @return bool
     */
    private function isAssoc($arr)
    {
        if (empty($arr)) {
            return false;
        } else {
            return array_keys($arr) !== range(0, count($arr) - 1) && empty($arr);
        }
    }

    /**
     * get last array
     *
     * @param $start
     * @param $result
     */
    private function findEnd(&$start, &$result)
    {
        if (next($start)) {
            foreach ((array) $start as $st) {
                $this->findEnd($st, $result);
            }
        } else {
            $temp = [];
            foreach ((array) end($start) as $key => $value) {
                $temp[$key] = $value;
            }
            $result[] = $temp;
        }
    }

    /**
     * translate from:
     *  {
     *    group_selector:
     *        {
     *            key_1: val_1,
     *            key_2: val_2
     *        }
     *  }
     *
     * To:
     * {
     *      key_1: val_1,
     *      key_2: val_2
     * }
     *
     * @param $json
     * @param $group_selector
     *
     * @return array
     */
    private function flattenJson($json, $group_selector)
    {
        $flat_arr = [];

        foreach ((array) $json as $item) {
            if (isset($item[$group_selector])) {
                $flat_arr[] = $item[$group_selector];
            }
        }

        return $flat_arr;
    }

    /**
    *
    * @param $json
    * @param $group_selectors
    *
    * @return array
    */
    private function flattenJsonManyGroups($json, $group_selectors)
    {
        if(empty($json)){
            return [];
        }
        $group_selectors = explode(';', $group_selectors);
        foreach ((array) $json as $item) {
            foreach ($group_selectors as $group_selector ) {
                if (isset($item[$group_selector])) {
                    $flat_arr[] = $item[$group_selector];
                }
            }
            $flat_arrr[] = $flat_arr[0] + $flat_arr[1];
            $flat_arr = [];
        }

        return $flat_arrr;
    }


    /**
     * this method Customize results after the method translate
     *
     * @param      $json
     * @param int  $header_row
     * @param int  $number_of_headers
     * @param int  $column_row
     * @param int  $number_of_columns
     * @param int  $lead_col_number
     * @param int  $sale_col_number
     * @param int  $limit_from_end
     *
     * @return mixed
     */
    public function CustomizeAccrodingToParams($json, $header_row = 2, $number_of_headers = 6,
        $column_row = 3, $number_of_columns = 6, $lead_col_number = null,
        $sale_col_number = null, $limit_from_end = 0)
    {
        $results = [];
        $headers = $json[$header_row];
        $headers = \array_slice($headers, 0, $number_of_headers);
        $columns = \array_slice($json, $column_row, (\count($json) - 1) - $limit_from_end);

        foreach ($columns as $idx => $row) {
            $rows           = [];
            $row            = \array_slice($row, 0, $number_of_columns);
            $translated_row = array_combine($headers, $row);

            /*iterate over fields according to there value*/
            $fields = ['lead', 'sale'];
            array_map(function ($field) use ($rows, $translated_row, $row, &$results, $lead_col_number, $sale_col_number
            ) {
                $field_col_number = ${$field . '_col_number'};
                if ($field_col_number) {
                    $field_sum = $row[$field_col_number];
                    $rows      = $this->prepareRowByField($field, $field_sum, $translated_row);
                    $results   = array_merge($results, $rows);
                }
            }, $fields);
        }

        return $results;
    }

    /**
     * @param $field_name
     * @param $field_iterations
     * @param $row
     *
     * @return array
     */
    private function prepareRowByField($field_name, $field_iterations, $row): array
    {
        $results = [];

        /*case there's no  lead/sale*/
        if ($field_iterations < 1) {
            $results[] = $this->fillDynamicColumns($row, $field_name);
        }

        /*case there's lead/sale iterate over them*/
        for ($i = 1; $i <= $field_iterations; $i++) {
            $results[] = $this->fillDynamicColumns($row, $field_name, [1, $i]);
        }

        return $results;
    }

    /**
     * @param $row
     * @param $field_name
     * @param $values
     *
     * @return mixed
     */
    private function fillDynamicColumns(&$row, $field_name, array $values = [0, 1])
    {
        $row[$field_name]   = $values[0];
        $row['counter_col'] = $values[1];

        return $row;
    }

    /**
     * @param      $results
     * @param      $col_name
     * @param      $new_name
     * @param      $action
     * @param null $search
     * @param null $replace
     * @param null $needle
     * @param null $before_needle
     *
     * @return array
     */
    public function addCustomValueToResults($results, $col_name, $new_name, $action, $search = null, $replace = null, $needle = null, $before_needle = null)
    {
        $results = collect($results);
        $results->transform(function ($result) use ($new_name, $action, $col_name, $search, $replace, $needle, $before_needle) {
            if ($action == 'str_replace') {
                $result[$new_name] = str_replace($search, $replace, $result[$col_name]);
            }
            if ($action == 'strstr') {
                $result[$new_name] = strstr($result[$col_name], $needle, $before_needle);
            }

            return $result;
        });

        return $results->toArray();
    }

}
