<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

trait ImpactRadius
{
    /**
     * impact Radius
     * after standard translate, this function checks if subid1 is empty and replaces
     * it with other parameters, to be used as token and trxid
     *
     * @param      $json
     * @param null $offset
     * @param float $double_value
     *
     * @return mixed
     */
    public function impactRadius($json, $offset = null, $double_value = 1.32)
    {
        $translated_result = self::translate($json, $offset);

        foreach ($translated_result as &$translated_item) {
            if ($translated_item['SubId1']) {
                $translated_item['token'] = $translated_item['SubId1'];
            } else {
                $translated_item['token'] = $translated_item['SharedId'] ?: $translated_item['Id'];
            }
            $translated_item['token'] = str_replace('%24', '$', $translated_item['token']);
            $translated_item['adjusted amount'] = $translated_item['Amount'] * $double_value;
        }

        return $translated_result;
    }

    public function impactRadiusWithPaging($json, $offset = null)
    {

        $translated_result = [];
        $site = "https://api.impactradius.com";
        do{
            // Translate all the data to array, including page's fields
            $json_fields = self::translate($json);
            // Extract relevant data to results
            $results = self::translate($json, $offset);
            // Merge the data from the previous page
            $translated_result = array_merge($translated_result,$results);
            // Get url of the next page
            $next_page_uri = $json_fields['@nextpageuri'];
            // If there are more pages to translate
            if($next_page_uri != ""){
                $url = $site.$next_page_uri;
                // Create a stream
                $opts = array(
                    'http'=>array(
                        'method'=>"GET",
                        'header'=>"Accept-language: en\r\n" .
                            "Cookie: foo=bar\r\n" .
                            "User-agent: BROWSER-DESCRIPTION-HERE\r\n".
                            "Authorization: Basic SVJ0RHdNaUJIWDh1MTMzMjYzRkZSdVk5Q0xHVlRteG50MTpMVGt2ZVBGeUJVZzg3VE5QTFI0aWFScnpXRTR6S1Rkdw=="
                    )
                );
                $context = stream_context_create($opts);
                // Open the file using the HTTP headers set above
                $json  = file_get_contents($url, false, $context);
            }
        }while($next_page_uri != "");

        // Set the Token
        foreach ($translated_result as &$translated_item) {
            if ($translated_item['SubId1']) {
                $translated_item['token'] = $translated_item['SubId1'];
            } else {
                $translated_item['token'] = $translated_item['SharedId'] ?: $translated_item['Id'];
            }

            $translated_item['token'] = urldecode($translated_item['token']);
            $translated_item['EventDate'] = substr($translated_item['EventDate'], 0 , strpos($translated_item['EventDate'], '+') );


        }
        return $translated_result;
    }


    public function impactRadiusVidetiWithPaging($json, $offset = null)
    {

        $translated_result = [];
        $site = "https://api.impactradius.com";
        do{
            // Translate all the data to array, including page's fields
            $json_fields = self::translate($json);
            // Extract relevant data to results
            $results = self::translate($json, $offset);
            // Merge the data from the previous page
            $translated_result = array_merge($translated_result,$results);
            // Get url of the next page
            $next_page_uri = $json_fields['@nextpageuri'];
            // If there are more pages to translate
            if($next_page_uri != ""){
                $url = $site.$next_page_uri;
                // Create a stream
                $opts = array(
                    'http'=>array(
                        'method'=>"GET",
                        'header'=>"Accept-language: en\r\n" .
                            "Cookie: foo=bar\r\n" .
                            "User-agent: BROWSER-DESCRIPTION-HERE\r\n".
                            "Authorization: Basic SVJwb05uaWN6OThIMTMzNTkwM2dLZWF0YlpiSmFHRVN2MTpjVkhLUC1iTEVzRW9NTUtfVVNYeHJwOGhhQWVQQUg0TA=="
                    )
                );
                $context = stream_context_create($opts);
                // Open the file using the HTTP headers set above
                $json  = file_get_contents($url, false, $context);
            }
        }while($next_page_uri != "");

        // Set the Token
        foreach ($translated_result as &$translated_item) {
            if ($translated_item['SubId1']) {
                $translated_item['token'] = $translated_item['SubId1'];
            } else {
                $translated_item['token'] = $translated_item['SharedId'] ?: $translated_item['Id'];
            }

            $translated_item['token'] = urldecode($translated_item['token']);
            $translated_item['EventDate'] = substr($translated_item['EventDate'], 0 , -6 );


        }
        return $translated_result;
    }
}