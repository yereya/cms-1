<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait IncomeAccess
{
    public function income_access($csv, $offset = 0, $limit = 0, $delimiter = null,
        $sales_column = 'cpacommissioncount', $token_column = 'affcustomid', $leads_column = 'downloads',
        $date = 'period')
    {
        $translated_array  = self::translate($csv, $offset, $limit, $delimiter);
        $translated_result = [];
        foreach ($translated_array as $translated_item) {
            $token       = $translated_item[$token_column] ?? '';
            $leads_count = $translated_item[$leads_column] ?? 0;
            $sales_count = $translated_item[$sales_column] ?? 0;

            /*In case we pass string to this parameter act like it 1 record*/
            if (!is_numeric($leads_count)) {
                $leads_count = 1;
            }

            /*In case we pass string to this parameter act like it 1 record*/
            if (!is_numeric($sales_count)) {
                $sales_count = 1;
            }

            // if we have leads clone the event as the leads number
            if ($leads_count > 0) {
                $_event         = $translated_item;
                $_event['lead'] = 1;
                for ($i = 1; $i <= $leads_count; $i++) {
                    // format the date to enable correct date comparison
                    $_event['formatted_date'] = formatDate($_event[$date], 'Y-m-d');
                    $_event['trx_id']         = "$token-{$_event['formatted_date']}-$i";
                    $translated_result[]      = $_event;
                }
            }

            // if we have sales clone the event as the sales number
            if ($sales_count > 0) {
                $_event         = $translated_item;
                $_event['sale'] = 1;
                for ($i = 1; $i <= $sales_count; $i++) {
                    $_event['formatted_date'] = formatDate($_event[$date], 'Y-m-d');
                    $_event['trx_id']         = "$token-{$_event['formatted_date']}-$i";;
                    $translated_result[] = $_event;
                }
            }
        }

        return $translated_result;
    }
}