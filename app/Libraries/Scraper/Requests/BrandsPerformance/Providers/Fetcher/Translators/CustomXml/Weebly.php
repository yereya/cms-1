<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomXml;

trait Weebly
{
    /**
     * Weebly
     * translates Weebly website builder brand
     *
     * @param $xml
     * @param $offset
     *
     * @return array
     */
    public function weebly($xml, $offset)
    {
        $translated_result = self::translate($xml, $offset);

        $comments = collect($translated_result)->groupBy(function ($record) {
            return preg_replace('/\D/', '', $record['COMMENT']);
        });

        /* Assign token Field to all records
         * in case the AFFCOMMENT FIELD is null it means there already been a sale or lead
           (so we need to search the the record according to the COMMENT FIELD and then assign it as the correct token).
         * else assign AFFCOMMENT FIELD as the token.
        */
        $translated_result = collect($translated_result)->map(function ($record) use ($comments) {
            $token = $record['AFFCOMMENT'];

            if (is_null($record['AFFCOMMENT'])) {
                $comment_field = preg_replace('/\D/', '', $record['COMMENT']);

                $old_lead = $comments->get($comment_field)
                    ->where('AFFCOMMENT', '!=', null)
                    ->first();

                $token = $old_lead['AFFCOMMENT'];
            }
            $record['token'] = $token;

            return $record;
        });

        return $translated_result->toArray();
    }
}