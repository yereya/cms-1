<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait VirtualPBX
{
    /**
     * - customize Stats after first translation dependency => (translateMethod)
     * - generate results according to params (headers col number , records_col_number , etc...)
     * - generate commission according to field name ( let the user pass array of fields, array of commissions)
     *
     * @param        $json
     * @param int    $headers_col_number
     * @param int    $header_row_idx
     * @param int    $records_col_number
     * @param int    $records_row_idx
     * @param int    $limit_from_end
     * @param string $fields
     * @param string $commissions
     *
     * @return array
     */
    public function getVirtualPBXStats($json, $headers_col_number = 6, $header_row_idx = 3, $records_col_number = 6,
        $records_row_idx = 4, $limit_from_end = 1, $fields = 'Tier 1;Tier 2;Tier 3;Tier 4',
        $commissions = '60;120;180;240'): array
    {
        $results               = [];
        $fields                = explode(";", $fields);
        $commissions           = explode(";", $commissions);
        $fields_to_commissions = array_combine($fields, $commissions);
        $headers               = \array_slice($json[$header_row_idx], 0, $headers_col_number);
        $records               = \array_slice($json, $records_row_idx,
            \count($json) - $records_row_idx - $limit_from_end);

        foreach ($records as $idx => $record) {
            foreach ($fields_to_commissions as $field => $commission) {
                $record           = \array_slice($record, 0, $records_col_number);
                $temp             = array_combine($headers, $record);
                $sale_or_lead_sum = (int)str_replace('$', '', $temp[$field]);

                if ($sale_or_lead_sum > 0) {
                    $fields = \array_slice($headers, 2, 4);
                    for ($i = 1; $i <= $sale_or_lead_sum; $i++) {
                        $temp['counter_col']       = $i;
                        $temp['commission_amount'] = $commission;
                        $temp['commission_name']   = str_replace(' ', '_', $field);
                        $temp[$field]              = 1;

                        /*Empty other fields*/
                        foreach ($fields as $f) {
                            if ($f !== $field) {
                                $temp[$f] = 0;
                            }
                        }
                        $results[] = $temp;
                    }
                }
            }
        }

        return $results;
    }
}