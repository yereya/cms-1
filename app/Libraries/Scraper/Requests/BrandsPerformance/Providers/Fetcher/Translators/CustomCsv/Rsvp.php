<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

/**
 * Trait RSVP
 * @package App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv
 */
trait Rsvp
{
    /**
     * @param $csv_string
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @return array
     */
    public function Rsvp($csv_string, $offset = 0, $limit = 0, $delimiter = null)
    {
        $csv = $this->translate($csv_string, $offset, $limit, $delimiter);
        $final_result = [];
        $counter = 0;

        collect($csv)
            ->sortBy('Date')
            ->values()
            ->each(function ($record) use (&$final_result, &$counter) {
                $lead = $record;
                $sum_of_leads = $record['Fairfax RSVP : New Registration - Desktop -16: Total Conversions'] + $record['Fairfax RSVP : New Registration - Mobile -16: Total Conversions'];
                $last_counted_record = date('ym', strtotime(end($final_result)['Date']));
                $formatted_date = date('ym', strtotime($record['Date']));

                /*If the month-year is different from the last record reset counter */
                if ((count($final_result) > 0) && ($last_counted_record != $formatted_date)) {
                    $counter = 0;
                }

                if ($sum_of_leads) {
                    for ($i = 1; $i <= $sum_of_leads; $i++) {
                        $lead['counter_col'] = ++$counter;
                        $lead['trx_id'] = "RSVP-lead-{$formatted_date}-{$counter}";

                        $final_result[] = $lead;
                    }
                }
            });

        return $final_result;
    }
}