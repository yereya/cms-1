<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait SunBets
{
    public function SunBets($csv, $offset = 0, $limit = 0)
    {
        $translated_array = self::translate($csv, $offset, $limit, $date_col_name = 'Date');
        $date_count_arr = [];

        // Initializes a memoization counter array for dates
        foreach ($translated_array as &$translated_item) {
            $_date = $translated_item[$date_col_name];
            // Distinguish header titles from translated items
            if ($_date == $date_col_name) {
                continue;
            }

            if (empty($date_count_arr[$_date])) {
                $date_count_arr[$_date] = 0;
            }

            $date_count_arr[$_date]++;
            $translated_item['_token'] = $translated_item[$date_col_name] . '-' . $date_count_arr[$_date];
        }

        return $translated_array;
    }
}