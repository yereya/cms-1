<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


use PhpImap\Exception;

trait taxRelief
{

    /**
     * @param        $html
     * @param string $table_wrapper
     * @param string $header_selector
     * @param int $offset
     * @param int $limit
     * @param int $date_col
     *
     * @return array
     */
    public function taxRelief($html,  $offset = 0, $limit = 0, $date_name = '')
    {

        $results = [];
        $assoc = true;
        $this->createCrawler($html);

        $results_state = strip_tags($html);
        $needle_start = 'Oppt Close Date';
        $needle_end = 'Grand Total';
        $results_state = trim(substr($results_state,strpos($results_state,$needle_start) + strlen($needle_start),strpos($results_state,$needle_end) - strpos($results_state,$needle_start) - 15));

        $results_state = preg_replace('/\r\n\r\n/',',', $results_state);
//        $results_state = preg_replace('/,,/',"\n", $results_state);

        if ($results_state) {
            $this->gettaxReliefKeys();

            $this->gettaxReliefValues($results_state, $offset);

            if ($limit != 0) {
                $this->values = array_slice($this->values, 0, $limit);
            }

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }

            $final_results = [];
            $seen_dates = [];

            // Appends every seen date incremented
            foreach ($results as $result) {
                $_date = $result[$date_name];

                if (empty($_date)) {
                    continue;
                }

                if (!isset($seen_dates[$_date])) {
                    $seen_dates[$_date] = -1;
                }

                $seen_dates[$_date]++;
                $final_results[] = $result + ['counter_col' => $_date . '-' . $seen_dates[$_date]];
            }


            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($final_results, $assoc);
            }

            # ---- Filter by the date value - check if the date column is a valid date, otherwise remove ----
            $res = array_filter($final_results,function($r){return strtotime($r['Created Date']);});

            return $res;

        }

        return [];

    }

    /**
     * @param $target_crawler
     * @param $key_selector
     */
    private function gettaxReliefKeys()
    {

        $this->keys[0] = "Lead Owner";
        $this->keys[1] = "Lead Source";
        $this->keys[2] = "Lead Status";
        $this->keys[3] = "Created Date";
        $this->keys[4] = "Last Modified Date";
        $this->keys[5] = "Opportunity Owner";
        $this->keys[6] = "Opportunity Name";
        $this->keys[7] = "Opportunity: Account";
        $this->keys[8] = "Oppt Close Date";

    }

    /**
     * @param $target
     * @param $offset
     */
    private function gettaxReliefValues($results_state, $offset = 0)
    {
        $idx = 0;
        $results_state = explode(',', $results_state);
        $length = sizeof($results_state);
        for($i = 0; $i < $length/9; $i++) {
            $this->values[$idx] = array_slice($results_state,0,9);
            $idx++;
            $results_state = array_slice($results_state,10);
        }


    }


}