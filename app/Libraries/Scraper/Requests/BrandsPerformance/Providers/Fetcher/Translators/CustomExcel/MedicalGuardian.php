<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel;


/**
 * Trait MedicalGuardian
 *
 * @package App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel
 */
trait MedicalGuardian
{
    /**
     * @param     $excel_str
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function MedicalGuardian($excel_str, $offset = 0, $limit = 0)
    {
        $final_results = [];
        $results       = $this->translate($excel_str, $offset);
        $headers       = $results[0];

        for ($i = 1; $i < count($results); $i++) {
            $final_results[] = array_combine($headers, $results[$i]);
        }

        if ($limit) {
            return array_slice($final_results, $offset, count($results) - $limit);
        }

        return $final_results;
    }
}