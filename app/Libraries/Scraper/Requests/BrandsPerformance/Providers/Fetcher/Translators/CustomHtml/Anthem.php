<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;
use App\Libraries\TpLogger;


trait Anthem
{
    /** function that translate table attached in the body of the email
     * @param $html
     * @param string $table_wrapper
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function Anthem($html, $table_wrapper = 'table', $offset = 0, $limit = -1)
    {
        $results = [];
        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);
        $results_state = $table->getNode(0);

        if ($results_state) {

            $this->getToastValues($table, $offset);
            $this->keys = array_slice($this->values, $offset, 1)[0];
            $this->values = array_slice($this->values, 1, $limit);

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else if ($this->keys && empty($this->values)) {
                $tp_logger = TpLogger::getInstance();
                $tp_logger->notice('There is no data available' . $table_wrapper);
                return [];
            }

            foreach($results as &$result){
                $result['SaleAmount'] = str_replace('$', '', $result['SaleAmount']);
                $result['SaleAmount'] = str_replace(',', '', $result['SaleAmount']);
                $result['SaleAmount'] =  intval($result['SaleAmount']);
            }
        }
        return $results;
    }
}