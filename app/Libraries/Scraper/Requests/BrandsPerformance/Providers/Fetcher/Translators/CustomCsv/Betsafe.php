<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait Betsafe
{
    /**
     * Betsafe
     * Custom translator for Betsafe
     *
     * @param     $csv
     * @param     $offset
     * @param int $limit
     *
     * @param null $delimiter
     * @param string $headers_detected_indicator
     * @return array
     */

    public function betsafe($csv, $offset, $limit = 0, $delimiter = null, $headers_detected_indicator = 'Qualified NDCs')
    {
        // Retrieve the initial results
        $translated_results = self::translate($csv, $offset, $limit, $delimiter);
        $appended_results = [];
        $seen_headers = 0;
        $headers = [];

        // If the initial headers were correct - return the results
        if (isset($translated_results[0][$headers_detected_indicator])) {

            return $translated_results;
        }

        foreach ($translated_results as &$translated_result) {
            // Determine whether the headers row has appeared
            if (in_array($headers_detected_indicator, array_values($translated_result)) || $seen_headers > 0) {
                $seen_headers++;
            }

            switch ($seen_headers) {
                case 0:
                    break;
                case 1:
                    $headers = array_values($translated_result);
                    break;
                default:
                    // If we have already seen the headers indicator
                    // Appends the current values under the seen headers
                    $appended_results[] = array_combine($headers, array_values($translated_result))
                        + ['Date' => $translated_result['Date'] ?? ''];
                    break;
            }
        }

        return $appended_results;
    }
}