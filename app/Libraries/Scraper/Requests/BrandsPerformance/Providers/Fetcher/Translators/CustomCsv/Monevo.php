<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


trait Monevo
{
    public function MonevoWithoutCurrency($csv, $offset = 0, $limit = 0, $delimiter = null,$col_name=null)
    {
        $translated = self::translate($csv, $offset, $limit, $delimiter);

        if($col_name){
            foreach ($translated as &$row){

                $num = preg_replace("/[\$]/", "", $row[$col_name]); 
                $num = preg_replace("/[\£]/", "",$num);

                $row[$col_name] = (double)$num;

                if($row[$col_name] == 0){
                    $row['zero_column'] = 'Yes';
                }else{
                    $row['zero_column'] = 'No';
                }
            }
        }
        return $translated;
    }
}