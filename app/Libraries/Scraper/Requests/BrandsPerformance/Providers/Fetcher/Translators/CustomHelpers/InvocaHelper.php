<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers;


use App\Entities\Models\Mrr\MrrFactCalls;

trait InvocaHelper
{

    /**
     * Send data from mrr_fact_calls to out
     *
     * @param       $html
     * @param       $date_from
     * @param       $date_to
     * @return array $results
     */
    public function InvocaProcessor($html,$date_from=null,$date_to=null)
    {
        # Set dates
        $from = isset($date_from)?$date_from:'-1 day';
        $to = isset($date_to)?$date_from:'now';
        $dates = [
            'from' => date('Y-m-d', strtotime($from)) . ' 00:00:00',
            'to'   => date('Y-m-d', strtotime($to)) . ' 23:59:59'
        ];
        # Fetch data from table between dates
        $results = MrrFactCalls::whereBetween('call_start_time', $dates)->get()->toArray();
        // pulling token from c3 url parameter
        $pattern = "/[&]/";
        foreach ($results as &$result) {
            if(str_contains($result['advertisers_marketing_data'],'c3') && str_contains($result['advertisers_marketing_data'],'https://try.anthemtaxes.net/Try/TaxInfo?')){
                $parts = preg_split($pattern, $result['advertisers_marketing_data']);
                $parts = preg_split("/[=]/", $parts[5]);
                $result['ads_token'] = $parts[1];
            }
        }
        return $results;

    }
}