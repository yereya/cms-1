<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


trait BuzzBingo
{
    /**
     * @param        $html
     * @param string $table_wrapper
     * @param string $header_selector
     * @param int    $offset
     * @param int    $limit
     *
     * @return array
     */
    public function BuzzBingo($html, $table_wrapper = 'table', $header_selector = 'tr', $offset = 0,
                                       $limit = 0)
    {
        $results = [];
        $assoc   = true;
        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);

        $results_state = $table->getNode(0);

        if ($results_state) {
            $this->getBuzzBingoKeys();

            $this->getBuzzBingoValues($table, $offset);


            if ($limit != 0) {
                $this->values = array_slice($this->values, 0, $limit);
            }

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc   = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }

            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($results, $assoc);
            }

            return $results;
        }

        $tp_logger = TpLogger::getInstance();
        $tp_logger->notice('There is no data available or provided table selector parameter is wrong: ' . $table_selector);

        return [];

    }


    /**
     * @param $target_crawler
     * @param $key_selector
     */
    private function getBuzzBingoKeys(): void
    {

        $this->keys[0] = "ACID";
        $this->keys[1] = "Period";
        $this->keys[2] = "Impressions";
        $this->keys[3] = "Clicks";
        $this->keys[4] = "Click-Through Ratio";
        $this->keys[5] = "Registrations";
        $this->keys[6] = "Registration Ratio";
        $this->keys[7] = "New Account Ratio";
        $this->keys[8] = "New Depositing Acc Count";
        $this->keys[9] = "New Active Acc Count";
        $this->keys[10] = "First Deposit Count";
        $this->keys[11] = "Active Accounts";
        $this->keys[12] = "Active Days";
        $this->keys[13] = "New Acc Purchases";
        $this->keys[14] = "Depositing Accounts";
        $this->keys[15] = "Wagering Accounts";
        $this->keys[16] = "Average Active Days";
        $this->keys[17] = "Deposits";
        $this->keys[18] = "Product 1 Commission";
        $this->keys[19] = "Casino Commission";
        $this->keys[20] = "Bingo Commission";
        $this->keys[21] = "CPA Commission";
        $this->keys[22] = "CPA count";
        $this->keys[23] = "Total Commission";

    }

    /**
     * @param $target
     * @param $offset
     */
    private function getBuzzBingoValues($target, $offset = 0): void
    {
        $idx = 0;
        $target->filter('tr')->reduce(function ($tr) use (&$idx, $offset) {
            if ($idx >= $offset && $tr->attr('style') != 'display:none;') {
                $tr->filter('td')->reduce(function ($td, $i) use ($idx) {
                    $this->values[$idx][$i] = $this->removeNbsp($td->text());

                });
            }
            $idx++;
        });
    }

}