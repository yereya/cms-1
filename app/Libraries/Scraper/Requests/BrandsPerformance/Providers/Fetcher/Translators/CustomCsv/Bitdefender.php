<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait Bitdefender
{
    /**
     * bitdefender
     * Custom translator for bitdefender
     *
     * @param        $csv
     * @param        $offset
     * @param int    $limit
     * @param null   $delimiter
     *
     * @return array
     */
    public function bitdefender($csv, $offset, $limit = 0, $delimiter = null)
    {
        $translated_result = self::translate($csv, $offset, $limit, $delimiter);

        $fixed_translated_result = [];

        foreach ($translated_result as &$translated_item) {
            // if 'Extensions' then it is not sale
            if (strpos($translated_item['Designation'], 'Extension') !== false || $translated_item['Fournisseur'] == 'DVD') {
                continue;
            }

            // $sales_count can be positive for sales and negative for canceled sales
            $sales_count = $translated_item['Quantite'];

            if (abs($sales_count) > 0) {
                $event_type = $sales_count > 0 ? 'sale' : 'cancel-sale';

                $_event = $translated_item;
                for ($i = 1; $i <= abs($sales_count); $i++) {
                    $_event['_trx_id']         = "bitdefender-$event_type-{$_event['Ref EPTI']}-{$_event['Date facture']}-$i";
                    $fixed_translated_result[] = $_event;
                }
            }
        }

        return $fixed_translated_result;
    }
}