<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait EliteSingles
{
    public function eliteSingles($csv, $offset, $limit, $delimiter, $event_col = 'col10')
    {
        $translated_result = self::translate($csv, $offset, $limit, $delimiter);

        foreach ($translated_result as &$translated_item) {

            if ($translated_item['Campaign'] == 'Mobile Campaigns') {
                $translated_item['ioid']     = '56f40a8c1696ed4c74d4844f';
                $translated_item['currency'] = 'EUR';
            } elseif ($translated_item['Campaign'] == 'UK IE AU CA NZ ZA ELITESINGLES') {
                $translated_item['ioid']     = '545f8828fe0af42e7bbb6977';
                $translated_item['currency'] = 'EUR';
            } elseif ($translated_item['Campaign'] == 'US ELITESINGLES (USD)' || $translated_item['Campaign'] == 'US ELITESINGLES') {
                $translated_item['ioid']     = '56b87378d495df3311c4c865';
                $translated_item['currency'] = 'USD';
            } elseif ($translated_item['Campaign'] == 'US ELITESINGLES MOBILE (USD)') {
                $translated_item['ioid']     = '56f40dfd0c669ec20bd78885';
                $translated_item['currency'] = 'USD';
            } elseif ($translated_item['Campaign'] == 'UK ELITESINGLES (GBP)') {
                $translated_item['ioid']     = '545f8828fe0af42e7bbb6977';
                $translated_item['currency'] = 'GBP';
            } elseif ($translated_item['Campaign'] == 'UK ELITESINGLES MOBILE (GBP)') {
                $translated_item['ioid']     = '56f40a8c1696ed4c74d4844f';
                $translated_item['currency'] = 'GBP';
            } elseif ($translated_item['Campaign'] == 'DACH eDarling') {
                $translated_item['ioid']     = '5741b74cb64fa0fe254f9444';
                $translated_item['currency'] = 'EUR';
            }

            if (strpos($translated_item[$event_col] ?? '', 'LEAD')) {
                $translated_item['event'] = 'lead';
            } elseif (strpos($translated_item[$event_col] ?? '', 'SALE')) {
                $translated_item['event'] = 'sale';
            }
        }

        return $translated_result;
    }
}