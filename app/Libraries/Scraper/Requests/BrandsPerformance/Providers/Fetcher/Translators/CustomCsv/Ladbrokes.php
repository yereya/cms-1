<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait Ladbrokes
{
    /**
     * ladbrokes
     * Custom translator for ladbrokes
     *
     * @param        $csv
     * @param        $offset
     * @param        $limit
     * @param string $lead_str
     * @param string $sale_str
     *
     * @return array
     */
    public function ladbrokes(
        $csv,
        $offset,
        $limit = 0,
        $lead_str = 'casino Signups Cnt',
        $sale_str = 'casino Acquired cnt'
    ) {
        $translated_result = self::translate($csv, $offset, $limit);

        $fixed_translated_result = [];

        // if the report contains other reportBy params then var2 and var9, it can be passed in report_primary_var
        // and report_secondary_var fields in advanced section of scraper
        $primary_var   = $this->scraper['properties']['report_primary_var'] ?? 'Var 9';
        $secondary_var = $this->scraper['properties']['report_secondary_var'] ?? 'Var 2';

        foreach ($translated_result as &$translated_item) {
            $translated_item['_token'] = ($translated_item[$primary_var] ?: $translated_item[$secondary_var]);

            $signups_count  = $translated_item[$lead_str];
            $acquired_count = $translated_item[$sale_str];

            // if we don't have the token, but have leads or/and sales
            if (!$translated_item['_token'] && ($signups_count > 0 || $acquired_count > 0)) {

                // if we have leads clone the event as the leads number
                if ($signups_count > 0) {
                    $_event            = $translated_item;
                    $_event[$sale_str] = 0;
                    for ($i = 1; $i <= $signups_count; $i++) {
                        $_event['_token']          = $_event['Date'] . '-' . $i;
                        $fixed_translated_result[] = $_event;
                    }
                }

                // if we have sales clone the event as the sales number
                if ($acquired_count > 0) {
                    $_event            = $translated_item;
                    $_event[$lead_str] = 0;
                    for ($i = 1; $i <= $acquired_count; $i++) {
                        $_event['_token']          = $_event['Date'] . '-' . $i;
                        $fixed_translated_result[] = $_event;
                    }
                }
            } else {
                $fixed_translated_result[] = $translated_item;
            }
        }

        return $fixed_translated_result;
    }
}