<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait PostAffiliateProGetSParam
{
    /**
     *  To login in Post Affiliate Pro we need to get S parameter.
     *  This function gets the S param and can be used as [var name="HtmlLib@postAffiliateProGetSParam"]
     *
     * @param $html
     *
     * @param string $var_name
     * @return array
     */
    public function postAffiliateProGetSParam($html, $var_name = 'HtmlLib@postAffiliateProGetSParam')
    {
        // a unique sequence before the s param value
        $needle = '[[\"name\",\"value\"],[\"S\",\"';

        $pos     = strpos($html, $needle);
        $pos_end = strpos($html, '"', $pos + strlen($needle));

        // the S param value, example: 1d32b25aa79ac9db49eca068704acfa8
        $s = substr($html, $pos + strlen($needle), $pos_end - ($pos + strlen($needle)) - 1);


        // S param should be returned with quotation, example: "1d32b25aa79ac9db49eca068704acfa8"
        return [$var_name => '"' . $s . '"'];
    }


    /**
     *  To get the JSON of Post Affiliate Pro we need to get S parameter.
     *  This function gets the S param and can be used as [var name="HtmlLib@postAffiliateProGetSParam2"]
     *
     * @param $html
     *
     * @param string $var_name
     * @return array
     */
    public function postAffiliateProGetSParam2($string, $var_name = 'HtmlLib@postAffiliateProGetSParam2')
    {
        $s = $this->getJsonValuesByFormat($string, 'S');

        // S param should be returned with quotation, example: "1d32b25aa79ac9db49eca068704acfa8"
        return [$var_name => '"' . $s . '"'];
    }

    /**
     * temp function to get the necessary value ("S") from the string
     *
     * @param $jsonInput
     * @param $requestedAttributeName
     * @return |null
     */
    private function getJsonValuesByFormat($jsonInput, $requestedAttributeName) {
        // todo- check if valid json
        $data = json_decode($jsonInput)[0];

        $attributesOrder = $data->fields[0];
        $nameIndex = 0;
        $valueIndex = 1;

        foreach ($attributesOrder as $index=>$attribute) {
            if ($attribute == 'name') {
                $nameIndex = $index;
            }
            else if ($attribute == 'value') {
                $valueIndex = $index;
            }
        }

        $requestedAttributeValue = null;
        foreach ($data->fields as $item) {
            if ($item[$nameIndex] == $requestedAttributeName) {
                $requestedAttributeValue = $item[$valueIndex];
            }
        }

        return $requestedAttributeValue;
    }



}