<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

use App\Libraries\TpLogger;

trait AmazonSlots
{
    /**
     * @param        $html
     * @param string $table_wrapper
     * @param string $header_selector
     * @param int    $offset
     * @param int    $limit
     *
     * @return array
     */


    public function getAmazonSlotStats($html, $table_wrapper = 'table', $header_selector = 'tr', $offset = 0, $limit = 0)
    {
        $results = [];
        $assoc   = true;
        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);
        $results_state = $table->getNode(0);
        if ($results_state) {
            $this->getAmazonSlotKeys($table, $header_selector);
            $this->getAmazonSlotValues($table, $offset);
            if ($limit != 0) {
                $this->values = array_slice($this->values, 0, $limit);
            }
            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc   = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }
            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($results, $assoc);
            }
            return $results;
        }
        $tp_logger = TpLogger::getInstance();
        $tp_logger->notice('There is no data available or provided table selector parameter is wrong: ' . $table_wrapper);
        return [];
    }
    /**
     * @param $target_crawler
     * @param $key_selector
     */
    private function getAmazonSlotKeys($target_crawler, $key_selector): void
    {
        $target_crawler->filter($key_selector)
            ->reduce(function ($node, $i) {
                $this->keys[$i] = $this->removeNbsp($node->text());
            });
    }
    /**
     * @param $target
     * @param $offset
     */
    private function getAmazonSlotValues($target, $offset = 0): void
    {
        $idx = 0;
        $target->filter('tr')->reduce(function ($tr) use (&$idx, $offset) {
            if ($idx >= $offset && $tr->attr('style') != 'display:none;') {
                $tr->filter('td')->reduce(function ($td, $i) use ($idx) {
                    $this->values[$idx][$i] = $this->removeNbsp($td->text());
                });
            }
            $idx++;
        });
    }

}