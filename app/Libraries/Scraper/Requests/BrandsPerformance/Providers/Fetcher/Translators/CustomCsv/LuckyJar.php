<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

/**
 * Trait LuckyJar
 * @package App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv
 */
trait LuckyJar
{
    /**
     * Translates luckyjar table and processes the subcampaigns
     *
     * @param $csv
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @return array
     */
    public function luckyJar($csv, $offset = 0, $limit = 0, $delimiter = null)
    {
        $translated_brands_table = self::translate($csv, $offset, $limit, $delimiter);
        $current_campaign = '';
        $current_brand = '';
        $result_set = [];
        $headers = [];

        foreach ($translated_brands_table as $row_idx => $row) {
            $skip_row = true;
            $indexed_row = array_values($row);
            switch ($indexed_row[0] ?? '') {
                case 'SubCampaign':
                    // Take the previous row's first value as current campaign and
                    $headers = $row;
                    $current_campaign = array_values($translated_brands_table[$row_idx - 1])[0];
                    $this->removePreviousResult($result_set);
                    break;
                case 'Campaign':
                    // Take the previous row's first value(brand's name) as current brand
                    $current_brand = array_values($translated_brands_table[$row_idx - 1])[0];
                    $this->removePreviousResult($result_set);
                    break;
                default:
                    $skip_row = false;
                    break;
            }

            if ($skip_row || empty($current_brand) || empty($current_campaign)) {
                continue;
            }

            $result_set[] = array_combine($headers, $indexed_row) + [
                'Campaign' => $current_campaign,
                'Brand' => $current_brand
            ];
        }

        return $result_set;
    }

    /**
     * Removes the last result using an efficient method
     *
     * @param $results
     */
    private function removePreviousResult(&$results)
    {
        if (key( array_slice( $results, -1, 1, TRUE ) ) > 0) {
            // Remove the previous row from the result set (since by design it is a header row and not value row)
            array_pop($results);
        }
    }
}