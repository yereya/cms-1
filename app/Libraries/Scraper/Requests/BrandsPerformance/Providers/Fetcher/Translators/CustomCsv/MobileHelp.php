<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


trait MobileHelp
{
    /**
     * @param $csv
     * @param int $offset
     * @param int $limit
     * @return mixed
     * Build a custom translator for script no. 610 Mobile help
     * Delete "-" and take only the last 6 digits of a particular column according to the processors.
     */
    public function MobileHelp($csv, $offset = 0, $limit = 0)
    {
        $translated_result = self::translate($csv, $offset, $limit);
        foreach ($translated_result as &$item) {
            $item['Primary Phone'] = preg_replace('/[-]/', '', $item['Primary Phone']);
            $item['Primary Phone'] = substr($item['Primary Phone'],5,11);
        }
        return $translated_result;
    }
}