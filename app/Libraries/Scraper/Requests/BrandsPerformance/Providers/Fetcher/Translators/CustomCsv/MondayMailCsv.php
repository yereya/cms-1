<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


use App\Libraries\TpLogger;
use DB;
use Exception;

trait MondayMailCsv
{
    /**
     * @param     $csv
     * @param     $date_offset
     * @param     $brand_id
     * @param     $countries_list
     * @param     $token_col
     * @param     $date_col
     * @param     $country_col
     * @param     $commission_col
     * @return array
     * @throws Exception
     */
    public function MondayMailCsv($csv,$date_offset,$brand_id,$countries_list,$token_col,$date_col,$country_col,$commission_col)
    {
        $tp_logger = TpLogger::getInstance();
        $countries_list = explode(';', $countries_list); // Split countries list by delimiter
        $countries_dict = [];
        # Create Dict of countries and the related commissions
        foreach ($countries_list as $i => $country){
            $splitted_str = explode("=",$country);
            if(sizeof($splitted_str) != 2){
                throw new Exception("Wrong country=>commission format entered");
            }
            $countries_dict[$splitted_str[0]] =  strtolower($splitted_str[1]);
        }

        $results = self::translate($csv);
        if(!array_key_exists($date_col,$results[0])){
            return [];
        }
        # Translate the date_offset to date format, should be of strtotime format
        if(!strtotime($date_offset)){ // Wrong format
            throw new Exception(" Error in the format of the date offset variable, use strtotime format - MondayMail.php trait.");
        }
        $date = date('Y-m-d',strtotime($date_offset));

        # Date in case the user entered wrong / too far date
        $date_for_safety = date('Y-m-d',strtotime("-1 month"));

        # Get data from db
        $track_upper_case = DB::connection('dwh')->table('dwh_fact_tracks')
            ->select('id','tokenip')
            ->where('brand_id', $brand_id)
            ->where('event', 'click')
            ->whereDate('updated_at', '>',$date)
            ->whereDate('updated_at', '>',$date_for_safety)->get()->toArray();

        # StdCalls to Array
        $track_upper_case = array_map(function($row){
            $r = json_decode(json_encode($row), true);
            return $r;
        },$track_upper_case);

        $track_lower_case = array_map('strtolower',array_column($track_upper_case, 'id'));

        # Look for the lower case values and build a new result
        $output = [];
        $dict = [];
        foreach ($results as $key => $row) {
            $token = $row[$token_col];
            $row[$token_col . '_original'] = $token;
            if ($token) { // If not EMPTY Token
                $index = array_search($token, $track_lower_case);
                if ($index) {
                    $row[$token_col] = $track_upper_case[$index]['id'];
                }
            } else if($row[$date_col]){ // EMPTY Token and EMPTY IP
                # Counter for empty tokens and ip by date
                $row_date = $row[$date_col];
                if (isset($dict[$row_date])) {
                    $dict[$row_date] = $dict[$row_date] + 1;
                } else {
                    $dict[$row_date] = 1;
                }
                $row[$token_col] = $row_date.' - '.$dict[$row_date];
            } else { // If empty line, skip to the next one
                continue;
            }

            # Check date format
            if(!(bool)strtotime($row[$date_col])){
                $row[$date_col] =  \PHPExcel_Style_NumberFormat::toFormattedString($row[$date_col], 'M/D/YYYY');
            }
            # Attach commission based on the dict
            $country_code = strtolower($row[$country_col]);
            if(array_key_exists($country_code,$countries_dict)){
                # Validate the type of the commissions
                if(!is_numeric($countries_dict[$country_code])){
                    throw new Exception("The commissions for ".$country_code." is not a correct number.");
                }
                //$row['commissions'] = intval($countries_dict[$country_code]) * $row['signups'];
                $row['commissions'] = intval($countries_dict[$country_code]);
            }else{
                throw new Exception("Country's code doesn't match. Check the country's codes you have entered in the fetcher or the codes in the excel file.");
            }
            try{
                $row[$commission_col] = str_replace('%', '', $row[$commission_col]);
                $row[$commission_col] = $row[$commission_col]*100;
            }catch (Exception $e){
                $tp_logger->warning("Error while translating Intent V12 val - MondayMail.php" . $e);
            }
            $output[] = $row;
        }
        return $output;
    }
}