<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

trait SunBingo
{
    /**
     * SunBingo
     * translates SunBingo brand
     * Compatible with the latest Mexos system
     *
     * @param      $json
     * @param null $selector
     *
     * @return mixed
     */
    public function sunbingo($json, $selector = null)
    {
        $translated_json = self::translate($json, $selector);
        $result = [];


        if($translated_json){
            $header_titles = $translated_json['headerTitles'];
            $rows = $translated_json['values'];
            foreach ($rows as $row_key => $row) {
                foreach ($header_titles as $header_key => $title) {
                    $result[$row_key][$title] = $row['data'][$header_key];
                }
            }
        }


        return $result;
    }
}
