<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


trait SelectHw
{
    /**
     * Translate html attach file from mail
     * @param $html
     * @param string $table_wrapper
     * @return array
     */
    public function SelectHw($html, $table_wrapper = 'table') {

        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);

        $results_state = $table->getNode(0);
        $new_translated_result = [];

        if ($results_state) {
            $results_array = explode("\n",$results_state->textContent);

            // Delete unnecessary signs
            $results_array = preg_replace('/\s+/', '', $results_array);
            $pos = array_search('DATE', $results_array);
            $results_array = array_slice($results_array,$pos);

            $results_array = array_chunk(array_filter($results_array,function($value) { if($value != "") {return $value;} }),9);
            $headers = $results_array[0];
            foreach (array_slice($results_array,1) as $row) {
                if(count($headers) == count($row)) {
                    $new_translated_result[] = array_combine($headers, $row);
                }
            }
        }

        return $new_translated_result;
    }
}
