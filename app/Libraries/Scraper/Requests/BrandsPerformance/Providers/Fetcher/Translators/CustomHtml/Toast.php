<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;
use App\Libraries\TpLogger;


trait Toast
{
    /**
     * @param        $html
     * @param string $table_wrapper
     * @param string $header_selector
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function Toast($html, $table_wrapper = 'table', $header_selector = 'tr', $offset = 0,
                             $limit = 0)
    {
        $results = [];
        $assoc = true;
        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);

        $results_state = $table->getNode(0);

        if ($results_state) {
            $this->getToastKeys();
            $this->getToastValues($table, $offset);


            if ($limit != 0) {
                $this->values = array_slice($this->values, 0, $limit);
            }

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }

            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($results, $assoc);
            }

            # ---- Filter by the date value - check if the date column is a valid date, otherwise remove ----
            $res = array_filter($results,function($r){return strtotime($r['Last MQL Date']);});
            return $res;

        }

        $tp_logger = TpLogger::getInstance();
        $tp_logger->notice('There is no data available or provided table selector parameter is wrong: ' . $table_wrapper);

        return [];

    }


    /**
     * @param $target_crawler
     * @param $key_selector
     */
    private function getToastKeys(): void
    {
        $this->keys[0] = "Last MQL Date";
        $this->keys[1] = "Web-initiative";
    }

    /**
     * @param $target
     * @param $offset
     */
    private function getToastValues($target, $offset = 0): void
    {
        $idx = 0;
        $target->filter('tr')->reduce(function ($tr) use (&$idx, $offset) {
            if ($idx >= $offset && $tr->attr('style') != 'display:none;') {
                $tr->filter('td')->reduce(function ($td, $i) use ($idx) {
                    $this->values[$idx][$i] = $this->removeNbsp($td->text());

                });
            }
            $idx++;
        });
    }
}