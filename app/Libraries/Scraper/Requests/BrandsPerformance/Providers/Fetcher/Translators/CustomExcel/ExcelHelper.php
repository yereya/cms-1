<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel;

use App\Libraries\TpLogger;

trait ExcelHelper
{
    /**
     * @param        $excel_str
     * @param int    $offset
     * @param        $headers_offset
     * @param string $counter_column_str
     * @param int    $limit
     * @param bool   $is_counted_by_date
     * @param string $date_col
     * @param bool   $only_date_counter
     *
     * @return array
     */
    public function excelDynamicCounter(
        $excel_str,
        $offset = 0,
        $headers_offset = 0,
        $counter_column_str = 'Lead Date',
        $limit = 0,
        $is_counted_by_date = false,
        $date_col = 'Lead Date',
        $only_date_counter = false)
    {
        $translated_results = self::translate($excel_str, $offset, $limit);
        $final_result       = [];
        $seen_dates         = [];

        // Predefines the headers
        $headers = $translated_results[$headers_offset];

        for ($i = $offset; $i < count($translated_results ) - $limit; $i++) {
            /*In case this is header row skip it*/
            if ($i == $headers_offset) {
                continue;
            }
            $translated_result = $translated_results[$i];
            // Keys will be headers and values will be actual translated values
            $_result = array_combine(array_map(function ($key) {
                if (!$key) {
                    return 'nullKey';
                } else {
                    return $key;
                }
            }, $headers), $translated_result);
            $_date   = $_result[$counter_column_str] ?? null;
            if (!$_date) {
                continue;
            }

            if ($is_counted_by_date) {
                $this->countColByDate($_result, $date_col, $seen_dates, $counter_column_str, $final_result);
            } elseif ($only_date_counter) {
                $this->countDuplicateDates($_result, $date_col, $seen_dates, $final_result);
            } else {
                $this->countByColumn($_result, $counter_column_str, $final_result);
            }
        }

        return $final_result;
    }

    /**
     * Translate Excel results for Compatible partners
     *
     * @param        $excel_str
     * @param int    $offset
     *
     * @return array
     */
    public function compatiblePartners($excel_str, $offset = 0){

        $translated_results = $this->translate($excel_str, $offset);
        $final_result       = [];
        $headers = reset($translated_results);
        $values  = array_slice($translated_results, $offset + 1);

        collect($values)
            ->each(
                function ($result) use (
                    $headers,
                    $values,
                    &$final_result
                ) {
                    /*Get keys */
                    $keys = collect($headers)->map(function ($key) {
                        if (!$key) {
                            return 'nullKey';
                        } else {
                            return $key;
                        }
                    });
                    /*Combine keys with values */
                    $final_result[] = collect($keys)->combine($result)->toArray();
                });
        return $final_result;
    }

    /**
     * Translate Excel results and add counter by Column
     *
     * @param        $excel_str
     * @param int    $offset
     *
     * @return array
     */
    public function translateWithCounterByDuplicateValue($excel_str, $counter_column_str, $offset = 0,
                                                         $limit = 0, $specific_sheets = '')
    {
        $tp_logger = TpLogger::getInstance();
        # Check if to take only part of the sheets in the spreadsheet
        try {
            if ($specific_sheets != '') {
                $translated_results = $this->translateSpecificSheets($excel_str, $offset, $limit, $specific_sheets);
            } else {
                $translated_results = $this->translate($excel_str, $offset);
            }
        }catch (\Exception $e){
            $tp_logger->info('Error in ' . $e);
            return [];
        }
        $final_result = [];
        $duplicates = [];
        /*get the first element (should be the headers)*/
        $headers = $translated_results[0];
        $values  = array_slice($translated_results, $offset + 1);

        foreach ($values as $val){
//            $new_val = array_combine($headers,$val);

            if ($val == $headers) {
                continue;
            }

            // Finds if there non-existing keys
            $missing_keys = array_diff_key($headers, $val);
            // Finds the complementary column data
            $complementary_data = array_fill(count($val), count($missing_keys), "");
            // Appends the missing keys/values to the original keys and values
            $new_val = array_combine($headers, $val + $complementary_data);

            if(array_key_exists($new_val[$counter_column_str],$duplicates)){
                $duplicates[$new_val[$counter_column_str]] += 1;
            }else{
                $duplicates[$new_val[$counter_column_str]] = 1;
            }
            $new_val['counter'] = $duplicates[$new_val[$counter_column_str]];
            $final_result[] = $new_val;
        }
        return $final_result;
    }
    /**
     * Translate Excel results with custom counter
     *
     * @param        $excel_str
     * @param int    $offset
     * @param string $counter_column_str
     * @param int    $limit
     * @param int    $specific_sheets(First sheet is 0)
     *
     * @return array
     */
    public function translateWithCustomCounter(
        $excel_str,
        $offset = 0,
        $counter_column_str = 'Date',
        $limit = 0,
        $specific_sheets = ''
    )
    {
        # Check if to take only part of the sheets in the spreadsheet
        if($specific_sheets != ''){
            $translated_results = $this->translateSpecificSheets($excel_str, $offset, $limit,$specific_sheets);
        }else{
            $translated_results = $this->translate($excel_str, $offset, $limit);
        }

        $final_result       = [];

        /*get the first element (should be the headers)*/
        $headers = reset($translated_results);
        $values  = array_slice($translated_results, $offset + 1);

        collect($values)
            ->each(/**
             * @param $result
             */
                function ($result) use (
                    $headers,
                    $values,
                    $counter_column_str,
                    &$final_result
                ) {

                    /*Get keys */
                    $keys = collect($headers)->map(function ($key) {
                        if (!$key) {
                            return 'nullKey';
                        } else {
                            return $key;
                        }
                    });

                    /*Combine keys with values */
                    $_result = collect($keys)->combine($result)->toArray();

                    /*count list with the specific column*/
                    $this->countByColumn($_result, $counter_column_str, $final_result);
                });

        return $final_result;
    }
}
