<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers\GeneralHelper;

trait GoogleSpreadSheets
{
    /**
     * @var string $ignore_indocator_str - string to indicate a row stop
     */
    private $ignore_indocator_str = '@@STOP';

    /**
     * General multi-purpose google spreadsheet translator
     *
     * @param      $json
     * @param null $selector
     * @param null $item_group_selector
     *
     * @param int $offset
     * @return array
     */
    public function googleSpreadSheet($json, $selector = null, $item_group_selector = null, $offset = 0)
    {
        $translated_result = self::translate($json, $selector, $item_group_selector);
        if (!$translated_result)
            return [];
        $headers           = $translated_result[$offset];
        $final_results     = [];

        for ($i = $offset + 1; $i < count($translated_result); $i++) {

            // Completes the truncated missing columns for each translated result so that we can append the values to the keys
            $col_num_diff = max(count($headers), count($translated_result[$i])) - min(count($headers), count($translated_result[$i]));
            $complementary_result = $translated_result[$i] + array_fill(count($translated_result[$i]), $col_num_diff, '');
            $final_results[] = array_combine($headers, $complementary_result);
        }

        return $final_results;
    }

    /**
     * General multi-purpose google spreadsheet translator
     *
     * @param      $json
     * @param null $selector
     * @param null $item_group_selector
     *
     * @return array
     */
    public function googleSpreadSheetMedicalAlert($json, $selector = null, $item_group_selector = null)
    {
        $translated_result = self::translate($json, $selector, $item_group_selector);

        if (!$translated_result or $translated_result['0']['2'] != 'Leads Total')
            return [];
        $headers           = $translated_result[0];
        $final_results     = [];
        $final_results_with_counter = [];

        for ($i = 1; $i < count($translated_result); $i++) {

            // Completes the truncated missing columns for each translated result so that we can append the values to the keys
            $col_num_diff = max(count($headers), count($translated_result[$i])) - min(count($headers), count($translated_result[$i]));
            $complementary_result = $translated_result[$i] + array_fill(count($translated_result[$i]), $col_num_diff, '');
            $final_results[] = array_combine($headers, $complementary_result);
        }

        # Remove unrelated rows
        $final_results = array_filter($final_results,function($a){
            if($a['Leads Fired through pixel'] != ''){
                return $a;
            }
        });

        # -------- split leads and sales to separate rows --------
        foreach ($final_results as $row){
            try {
                $row['counter'] = 0;
                # --- Split leads first ---
                $leads_counter = (int)$row['leads Gap'];
                if($leads_counter > 0) {
                    foreach (range(0, $leads_counter - 1) as $idx) {
                        $new_row = $row;
                        $new_row['Sales'] = 0;
                        $new_row['leads Gap'] = 1;
                        $new_row['counter'] = $idx;
                        $final_results_with_counter[] = $new_row;
                    }
                }
                # --- Split sales ---
                $sales_counter = (int)$row['Sales'];
                if($sales_counter > 0) {
                    foreach (range(0, $sales_counter - 1) as $idx) {
                        $new_row = $row;
                        $new_row['Sales'] = 1;
                        $new_row['leads Gap'] = 0;
                        $new_row['counter'] = $idx;
                        $final_results_with_counter[] = $new_row;
                    }
                }
                if($leads_counter == '0' and $sales_counter == '0'){
                    $final_results_with_counter[] = $row;
                }
            }
            catch (\Exception $e){
                $msg = $e->getMessage();
                $this->tp_logger->error(" Error in googleSpreadSheetMedicalAlert function, GoogleSpreadSheets.php . ".$msg);
            }
        }

        return $final_results_with_counter;
    }

    /**
     * @param      $json
     * @param null $selector
     * @param int  $limit_results_from_end
     * @param int  $headers_col_number
     * @param null $item_group_selector
     *
     * @return array
     */
    public function oomaSheet($json, $selector = null, $limit_results_from_end = 2, $headers_col_number = 2,
        $item_group_selector = null,$counter_column_str = null)
    {
        $final_results     = [];
        $translated_result = self::translate($json, $selector, $item_group_selector);
        $headers           = $translated_result[$headers_col_number];

        /*remove the redundant keys from translated   (remove the title and subtitle of table)*/
        $translated_result = \array_slice($translated_result, $headers_col_number + 1);


        for ($i = 0, $iMax = \count($translated_result); $i < $iMax - $limit_results_from_end; $i++) {
            $keys = array_diff_key($headers, $translated_result[$i]);

            /*if there's diff add the remaining key to the other array*/
            if ($keys) {
                foreach (array_values($keys) as $key => $val) {
                    $translated_result[$i][] = '';
                }
            }

            if (in_array($counter_column_str,$headers)) {// if there is 'Sales' header and add counter by Sales column
                $translated_result[$i] = array_combine($headers, $translated_result[$i]);

                if ($translated_result[$i]['Date']=='Total') //skip unnecessary rows
                    continue;

                $counter_amount = $translated_result[$i][$counter_column_str] ?? 1;
                for ($j = 1; $j <= $counter_amount; $j++) {
                    $translated_result[$i][$counter_column_str] = $j;
                    $final_results[] = $translated_result[$i];
                }
            }
            else{
                $final_results[] = array_combine($headers, $translated_result[$i]);
            }

        }

        return $final_results;
    }

    /**
     * RingCentral sheet translator
     *
     * @param      $json
     * @param null $selector
     * @param null $item_group_selector
     *
     * @return array
     */
    public function ringCentralSheet($json, $selector = null, $item_group_selector = null)
    {
        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        $headers               = $translated_sheet_data[0] ?? [];
        $seen_dates            = [];

        // Validates in order to run the valid tranlation
        if (empty($headers) || !in_array('Lead: SID', $headers)) {

            return [];
        }

        // Choose all rows excluding the headers and create a key-value array of [headers => row_values]
        $translated_results = collect($translated_sheet_data)->except(0)->map(function ($row, $idx) use ($headers) {
            if ($idx > 0) {
                return array_combine(array_values($headers), array_values($row + array_diff($headers, $row)));
            }
        })->toArray();

        foreach ($translated_results as &$translated_result) {
            $_date                        = date_create_from_format('m/d/Y',
                $translated_result['Fixed Date'])->format('Y-m-d');
            $translated_result['has_sid'] = 1;

            // If a row does not have an SID, make a unique date counter
            if (empty($translated_result['Lead: SID'])) {
                // Indicates token replacement with date counter
                $translated_result['has_sid'] = 0;

                if (!isset($seen_dates[$_date])) {
                    $seen_dates[$_date] = 0;
                } else {
                    $seen_dates[$_date]++;
                }

                $translated_result['Lead: SID'] = $_date . '-' . $seen_dates[$_date];
            }
            else //condition that will make the script change %21 to ! the same way it changes %24 to $.
            {
                $translated_result['Lead: SID'] = urldecode($translated_result['Lead: SID']);
            }
        }

        return $translated_results;
    }

    /**
     * Second RingCentral sheet translator
     *
     * @param      $json
     * @param null $selector
     * @param null $item_group_selector
     * @param null $token_col_name
     * @param null $date_col_name
     * @return array
     */
    public function secondRingCentralSheet($json, $selector = null, $item_group_selector = null, $token_col_name=null, $date_col_name=null)
    {
        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        $headers               = $translated_sheet_data[0] ?? [];
        $seen_dates            = [];

        // Validates in order to run the valid tranlation
        if (empty($headers) || !in_array($token_col_name, $headers)) {

            return [];
        }

        // Choose all rows excluding the headers and create a key-value array of [headers => row_values]
        $translated_results = collect($translated_sheet_data)->except(0)->map(function ($row, $idx) use ($headers) {
            if ($idx > 0) {
                return array_combine(array_values($headers), array_values($row + array_diff($headers, $row)));
            }
        })->toArray();

        foreach ($translated_results as &$translated_result) {
            $_date                        = date_create_from_format('d-M-y',
                $translated_result[$date_col_name])->format('Y-m-d');
            $translated_result['has_sid'] = 1;

            // If a row does not have an SID, make a unique date counter
            if (empty($translated_result[$token_col_name])) {
                // Indicates token replacement with date counter
                $translated_result['has_sid'] = 0;

                if (!isset($seen_dates[$_date])) {
                    $seen_dates[$_date] = 0;
                } else {
                    $seen_dates[$_date]++;
                }

                $translated_result[$token_col_name] = $_date . '-' . $seen_dates[$_date];
            }
            else //condition that will make the script change %21 to ! the same way it changes %24 to $.
            {
                $translated_result[$token_col_name] = urldecode($translated_result[$token_col_name]);
            }
        }

        return $translated_results;
    }

    /**
     * AlliancePhones sheet translator
     *
     * @param      $json
     * @param null $selector
     * @param null $item_group_selector
     *
     * @return array
     */
    public function alliancePhones($json, $selector = null, $item_group_selector = null)
    {
        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        $headers               = $translated_sheet_data[0] ?? [];
        $final_results         = [];
        $seen_dates            = [];
        collect($translated_sheet_data)->each(function ($row) use ($headers, &$final_results, &$seen_dates) {
            // row must be a valid array and not the headers array
            if (!empty($row) && is_array($row) && !empty($row[0]) && strpos($row[0],
                    'Total') === false && !in_array('Date', array_values($row)) && $row != $headers) {
                $modified = [
                    'Date'     => $row[0],
                    'Leads US' => $row[1] ?? 'None',
                    'Leads CA' => $row[2] ?? 'None'
                ];

                // Check if we can process the current row's numbers
                if (is_numeric($modified['Leads US']) && $modified['Leads US'] > 0) {
                    for ($i = 0; $i < $modified['Leads US']; $i++) {
                        $final_results[] = [
                            'Date'        => $modified['Date'],
                            'Leads US'    => $modified['Leads US'],
                            'Leads CA'    => '0',
                            'counter_col' => $i
                        ];
                    }
                }
                if (is_numeric($modified['Leads CA']) && $modified['Leads CA'] > 0) {
                    for ($i = 0; $i < $modified['Leads CA']; $i++) {
                        $final_results[] = [
                            'Date'        => $modified['Date'],
                            'Leads US'    => '0',
                            'Leads CA'    => $modified['Leads CA'],
                            'counter_col' => $i
                        ];
                    }
                }

                return $modified;
            }
            return null;
        });

        return $final_results;
    }

    /**
     * 8x8 sheet translator
     *
     * @param      $json
     * @param null $selector
     * @param int  $offset
     * @param int  $amount_of_headers
     * @param int  $leads_col_key
     * @param null $sales_col_key
     * @param null $item_group_selector
     *
     * @return array
     */
    public function eightXEight($json, $selector = null, $offset = 2, $amount_of_headers = 6, $leads_col_key = 3,
        $sales_col_key = null, $item_group_selector = null)
    {
        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        $headers               = array_slice(array_values($translated_sheet_data[$offset] ?? []), 0,
            $amount_of_headers);
        $ISO                   = $translated_sheet_data[0][0] ?? [];
        $leads_col             = $headers[$leads_col_key] ?? -1;
        $sales_col             = $headers[$sales_col_key] ?? null;
        $final_result          = [];

        for ($i = $offset + 1; $i < count($translated_sheet_data) - 1; $i++) {
            // Find the missing columns, and append them as empty strings by recovering the key difference array
            $_current_row       = array_slice($translated_sheet_data[$i], 0, $amount_of_headers);
            $missing_keys       = array_diff_key($headers, $_current_row);
            $complementary_data = array_fill(count($_current_row), count($missing_keys), "");
            $_result            = array_combine($headers, $_current_row + $complementary_data) + ['iso' => $ISO];
            $this->appendMutuallyExclusiveKeys($final_result, $_result, [$leads_col, $sales_col]);
        }

        return $final_result;
    }
    /**
     * 8x8 sheet translator
     *
     * @param      $json
     * @param null $selector
     * @param int  $offset
     * @param int  $amount_of_headers
     * @param int  $leads_col_key
     * @param null $sales_col_key
     * @param null $item_group_selector
     * @param int  $date
     * @param int  $limit
     *
     * @return array
     */
    public function eightXEightNew($json, $selector = null, $offset = 2, $amount_of_headers = 6, $leads_col_key = 3,
                                $sales_col_key = null, $item_group_selector = null, $date = 0, $limit = 0)
    {
        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        $headers               = array_slice(array_values($translated_sheet_data[$offset] ?? []), 0,
            $amount_of_headers);
        $ISO                   = $translated_sheet_data[0][0] ?? [];
        $leads_col             = $headers[$leads_col_key] ?? -1;
        $sales_col             = $headers[$sales_col_key] ?? null;
        $final_result          = [];

        for ($i = $offset + 1; $i < count($translated_sheet_data) - 1 - $limit; $i++) {
            // Find the missing columns, and append them as empty strings by recovering the key difference array
            $_current_row       = array_slice($translated_sheet_data[$i], 0, $amount_of_headers);
            if (!ctype_digit($_current_row[$date][0]))
                continue;
            $missing_keys       = array_diff_key($headers, $_current_row);
            $complementary_data = array_fill(count($_current_row), count($missing_keys), "");
            $_result            = array_combine($headers, $_current_row + $complementary_data) + ['iso' => $ISO];
            $this->appendMutuallyExclusiveKeys($final_result, $_result, [$leads_col, $sales_col]);
        }

        return $final_result;
    }
    /**
     * translateWithColumnSchema
     * Function that makes a sum between 2 columns
     * @param      $json
     * @param null $selector
     * @param int  $offset
     * @param int  $amount_of_headers
     * @param int  $leads_col_key
     * @param null $sales_col_key
     * @param null $item_group_selector
     * @param int  $date
     *
     * @return array
     */
    public function translateWithColumnSchema($json, $selector = null, $offset = 2, $amount_of_headers = 6, $leads_col_key = 3,
                                   $sales_col_key = null, $item_group_selector = null, $date = 0, $limit = 0)
    {
        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        $headers               = array_slice(array_values($translated_sheet_data[$offset] ?? []), 0,
            $amount_of_headers);
        //add the schema col
        $headers[] = 'Sum';
        $ISO                   = $translated_sheet_data[0][0] ?? [];
        $leads_col             = $headers[$leads_col_key] ?? -1;
        $sales_col             = $headers[$sales_col_key] ?? null;
        $final_result          = [];

        for ($i = $offset + 1; $i < count($translated_sheet_data) - 1 - $limit ; $i++) {
            // Find the missing columns, and append them as empty strings by recovering the key difference array
            $_current_row       = array_slice($translated_sheet_data[$i], 0, $amount_of_headers);
            $missing_keys       = array_diff_key($headers, $_current_row);
            $complementary_data = array_fill(count($_current_row), count($missing_keys), "0");
            $_result            = array_combine($headers, $_current_row + $complementary_data) + ['iso' => $ISO];

            if($_result[$headers[$leads_col_key]] == "")
                $_result[$headers[$leads_col_key]] = 0;

            if($_result[$headers[$sales_col_key]] == "")
                $_result[$headers[$sales_col_key]] = 0;

            $cur_key_value = $_result[$headers[$leads_col_key]] + $_result[$headers[$sales_col_key]];

            $_result['Sum'] = $cur_key_value;

            for ($j = 0; $j < $cur_key_value; $j++) {
                $final_result[] = $_result + ['counter_col' => $j];
            }
        }

        return $final_result;
    }
    /**
     * Freshsales translator
     *
     * @param      $json
     * @param null $selector
     * @param int  $offset
     * @param int  $amount_of_headers
     * @param int  $leads_col_key
     * @param null $sales_col_key
     * @param null $item_group_selector
     * @param null $check_header_existence
     *
     * @return array
     */
    public function freshsales($json, $selector = null, $offset = 2, $amount_of_headers = 6, $leads_col_key = 3,
                                $sales_col_key = null, $item_group_selector = null,$check_header_existence = null)
    {
        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        # check if header exists
        if ($check_header_existence) {
            if (!in_array($check_header_existence, $translated_sheet_data['1'])) {
                return [];
            }
        }
        $headers               = array_slice(array_values($translated_sheet_data[$offset] ?? []), 0,
            $amount_of_headers);
        $ISO                   = $translated_sheet_data[0][0] ?? [];
        $leads_col             = $headers[$leads_col_key] ?? -1;
        $sales_col             = $headers[$sales_col_key] ?? null;
        $final_result    = [];

        for ($i = $offset + 1; $i < count($translated_sheet_data) - 1; $i++) {
            // Find the missing columns, and append them as empty strings by recovering the key difference array
            $_current_row       = array_slice($translated_sheet_data[$i], 0, $amount_of_headers);
            $missing_keys       = array_diff_key($headers, $_current_row);
            $complementary_data = array_fill(count($_current_row), count($missing_keys), "");
            $_result            = array_combine($headers, $_current_row + $complementary_data) + ['iso' => $ISO];
            $this->appendMutuallyExclusiveKeys($final_result, $_result, [$leads_col, $sales_col]);
        }
        return $final_result;
    }

    /**
     * Generic function to create a counter by column
     *
     * @param        $json
     * @param        $selector
     * @param int    $offset             - row offset to start translating from
     * @param int    $headers_col_offset - which row are the headers present at
     * @param int    $amount_of_headers  - how many header columns should be translated
     * @param string $counter_column_str - for which column should we activate the counter
     * @param int    $limit              - offset from the end
     * @param bool   $is_counted_by_date - if we need to add a counter for each date
     * @param string $date_col           - what is the date column
     * @param bool   $only_date_counter
     * @param null   $item_group_selector
     *
     * @return array
     */
    public function jsonDynamicCounter(
        $json,
        $selector,
        $offset = 0,
        $headers_col_offset = 0,
        $amount_of_headers = 6,
        $counter_column_str = '',
        $limit = 0,
        $is_counted_by_date = false,
        $date_col = 'Date',
        $only_date_counter = false,
        $item_group_selector = null)
    {
        // Initial translation
        $translated_results = self::translate($json, $selector, $item_group_selector);
        // Checks if a counter filter is passed
        if (empty($counter_column_str)) {
            return $translated_results;
        }

        // Headers to be processed
        $headers      = $translated_results[$headers_col_offset];
        $final_result = [];
        // Initializes dates array
        $seen_dates = [];

        for ($i = $offset; $i < count($translated_results) - $limit; $i++) {
            // Find the missing columns, and append them as empty strings by recovering the key difference array
            $_current_row       = array_slice($translated_results[$i], 0, $amount_of_headers);
            $missing_keys       = array_diff_key($headers, $_current_row);
            $complementary_data = array_fill(count($_current_row), count($missing_keys), "");
            $_result            = array_combine($headers, $_current_row + $complementary_data);

            if ($is_counted_by_date) {
                $this->countColByDate($_result, $date_col, $seen_dates, $counter_column_str, $final_result);
            } elseif ($only_date_counter) {
                $this->countDuplicateDates($_result, $date_col, $seen_dates, $final_result);
            } else {
                $this->countByColumn($_result, $counter_column_str, $final_result);
            }
        }

        return $final_result;
    }


    /**
     * @param      $json
     * @param null $selector
     * @param int  $headers_idx
     * @param null $leads_col_idx
     * @param null $sales_col_idx
     * @param int  $row_removal_number_from_the_end
     * @param null $item_group_selector
     *
     * @return array
     */
    public function LifeStation($json, $selector = null, $headers_idx = 0, $leads_col_idx = null, $sales_col_idx = null,
        $row_removal_number_from_the_end = 5, $item_group_selector = null)
    {

        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        $headers               = $translated_sheet_data[$headers_idx];
        /*SLICE THE HEADERS AT THE FIRST , START COUNT FROM KEY 1 THEN GET THE LENGTH OF THE HEADER */
        $values  = array_slice($translated_sheet_data, $headers_idx + 1,
            (count($translated_sheet_data)) - ($headers_idx + 1) - $row_removal_number_from_the_end);
        $results = [];

        collect($values)->each(function ($record) use ($headers, &$results, $leads_col_idx, $sales_col_idx) {
            $_record = [];

            collect($headers)->each(function ($header, $idx) use ($record, &$_record) {
                $valid_record     = $record[$idx] ?? 0;
                $_record[$header] = $valid_record;
            });

            $this->appendMutuallyExclusiveKeys($results, $_record, [$leads_col_idx, $sales_col_idx]);
        });

        return $results;
    }
}
