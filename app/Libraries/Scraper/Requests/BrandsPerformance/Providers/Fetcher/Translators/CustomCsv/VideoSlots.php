<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

use DateTime;

trait VideoSlots
{
    /**
     * VideoSlots translator for General Daily fetched results
     *
     * @param $csv
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @return array
     */
    public function videoSlotsGeneral($csv, $column_var = 'Signups', $offset = 0, $limit = 0, $delimiter = null)
    {
        $results = [];
        $csv_result = self::translate($csv, $offset, $limit, $delimiter);

        foreach ($csv_result as $item) {
            // create a first time depositors counter for each item
            if (!($item_column_amount = $item[$column_var])) {
                continue;
            }

            for ($i = 0; $item_column_amount > $i; $i++) {
                $results[] = [
                    'Date' => $item['Date'],
                    'token' => $item['Date'] . '-' . $i
                ];
            }
        }

        return $results;
    }

    /**
     * VideoSlots translator for Daily Earnings fetched results
     *
     * @param $csv
     * @param int $divisor
     * @param string $column_var
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @return array
     */
    public function videoSlotsEarnings($csv, $divisor = 1, $column_var = 'CPA', $offset = 0, $limit = 0, $delimiter = null)
    {
        $results = [];
        $csv_result = self::translate($csv, $offset, $limit, $delimiter);

        foreach ($csv_result as $item) {
            $sales_idx = 0;
            // divide the column_var value by our divisor to figure the counter
            $sales = round( preg_replace('/[^0-9.]/', '', $item[$column_var]) / $divisor );

            for ($i = 0; $i < $sales; $i++) {
                $results[] = [
                    'Date' => $item['Date'],
                    'token' => $item['Date'] . '-' . $i
                ];
            }
        }

        return $results;
    }
}