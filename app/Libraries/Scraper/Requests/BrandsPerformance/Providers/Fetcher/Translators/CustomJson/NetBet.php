<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

trait NetBet
{
    /**
     * NetBet
     * NetBet custom translator
     *
     * @param      $json
     * @param null $selector
     * @param null $item_group_selector
     *
     * @return array
     */
    public function netBet($json, $selector = null, $item_group_selector = null)
    {
        $translated_result = self::translate($json, $selector, $item_group_selector);

        // if in the report token column is different from affiliate_custom4 it can be set in scraper advanced fields
        $token_col               = $this->scraper['properties']['token_col'] ?? 'affiliate_custom4';
        $fixed_translated_result = [];
        $unique_dates            = ['lead' => [], 'sale' => [], 'normal' => []];
        foreach ($translated_result as &$translated_item) {

            $leads_count = $translated_item['registrations'];
            $sales_count = $translated_item['valid_cpa'];

            // if we don't have the token in $token_col, but have leads or/and sales
            if (!$translated_item[$token_col] && ($leads_count > 0 || $sales_count > 0)) {

                // if we have leads clone the event as the leads number
                if ($leads_count > 0) {
                    $_event              = $translated_item;
                    $_event['valid_cpa'] = 0;
                    $date_counter = $this->appendUniqueDateCounter($_event, 'lead', $unique_dates);
                    for ($i = 1; $i <= $leads_count; $i++) {
                        $_event[$token_col]        = $_event['date_iso'] . '-' . $date_counter . '-' .$i;
                        $fixed_translated_result[] = $_event;
                    }
                }

                // if we have sales clone the event as the sales number
                if ($sales_count > 0) {
                    $_event                  = $translated_item;
                    $_event['registrations'] = 0;
                    $commission              = (float) ($translated_item['commission'] / $sales_count);
                    $date_counter = $this->appendUniqueDateCounter($_event, 'sale', $unique_dates);
                    for ($i = 1; $i <= $sales_count; $i++) {
                        $_event[$token_col]        = $_event['date_iso'] . '-' . $date_counter . '-' .$i;
                        $_event['commission']      = $commission;
                        $fixed_translated_result[] = $_event;
                    }
                }
            } else {
                $fixed_translated_result[] = $translated_item;
            }
        }

        return $fixed_translated_result;
    }

    /**
     * Processes a seen date and increments value if necessary
     *
     * @param $event
     * @param $event_name
     * @param $unique_dates
     * @return mixed
     */
    private function appendUniqueDateCounter($event, $event_name, &$unique_dates)
    {
        if (!array_key_exists($event['date_iso'], $unique_dates[$event_name])) {
            $unique_dates[$event_name][$event['date_iso']] = 0;
        } else {
            $unique_dates[$event_name][$event['date_iso']]++;
        }

        return $unique_dates[$event_name][$event['date_iso']];
    }
}