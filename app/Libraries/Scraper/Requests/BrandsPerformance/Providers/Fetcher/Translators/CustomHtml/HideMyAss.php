<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait HideMyAss
{
    /**
     * Hide My Ass
     * custom translator which main task to replace: <td><input readonly=readonly value="wvjn$pikg"></td>
     * to <td>wvjn$pikg</td>. this is needed because the general HtmlLib can't extract token from
     * <input readonly=readonly value="wvjn$pikg">.
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     * @param int    $limit
     *
     * @return mixed
     */
    public function hideMyAss($html, $table_selector = null, $header_selector = 'th', $limit = 0)
    {
        $needle        = '<input readonly=readonly value="';
        $needle_length = strlen($needle);
        $offset        = strpos($html, $needle, 0);

        while ($offset !== false) {
            $token_start = $offset + $needle_length;
            $token_end   = strpos($html, '"', $token_start);

            $token_len = $token_end - $token_start;
            $token     = substr($html, $token_start, $token_len);

            // replace for example: <input readonly=readonly value="zpj4e4wkx"> with zpj4e4wkx
            $html = substr_replace($html, $token, $offset, $needle_length + $token_len + 2);

            $offset = strpos($html, $needle);
        }

        $translated_res = self::translate($html, $table_selector, $header_selector, $limit);
        foreach ($translated_res as &$translated_item) {

            if (strpos(strtolower($translated_item['Sale type']), strtolower('New Order')) !== false) {
                if (strpos(strtolower($translated_item['Sale type']), strtolower('Refund')) !== false) {
                    $translated_item['event'] = 'cancel-sale';
                } else {
                    $translated_item['event'] = 'sale';
                }
            } elseif (strpos(strtolower($translated_item['Sale type']), strtolower('Renewal')) !== false) {
                $translated_item['event'] = 'renewal';
            }
        }

        return $translated_res;
    }
}