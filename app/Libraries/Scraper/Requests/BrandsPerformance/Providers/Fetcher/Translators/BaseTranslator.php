<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators;

abstract class BaseTranslator
{
    protected $fetcher;

    protected $scraper;

    public function __construct(array $scraper = [], array $fetcher = [])
    {
        $this->fetcher = $fetcher;
        $this->scraper = $scraper;
    }

    /**
     * Token Url Decode
     *
     * @param $result
     * @param $column_name
     */
    protected function tokenUrlDecode(&$result, $column_name)
    {
        foreach ($result as &$result_item) {
            if (isset($result_item[$column_name])) {
                $result_item[$column_name] = rawurldecode($result_item[$column_name]);
            }
        }
    }
}