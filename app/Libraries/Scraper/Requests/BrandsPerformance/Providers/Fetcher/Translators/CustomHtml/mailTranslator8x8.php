<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


trait mailTranslator8x8
{

    /**
     * @param  $html
     *
     * @return array
     */

    public function mailTranslator8x8($html)
    {

        $stripped = strip_tags($html);
        $needle = "UTM_Content:&nbsp;";
        $end_character = "ead&nbsp;";
        $pos     = strpos($stripped, $needle);
        # If the mail doesn't contain UTM Content - return
        if (!$pos) {
            return;
        }
        $pos_end = strpos($stripped, $end_character, $pos + \strlen($needle));
        $token = substr($stripped, $pos + \strlen($needle), $pos_end - ($pos + \strlen($needle)) - 1);

        $needle = "Date:&nbsp;";
        $pos = strpos($stripped, $needle);
        $date = substr($stripped, $pos + \strlen($needle), 10);

        $needle = "Date:&nbsp;";
        $pos = strpos($stripped, $needle);
        $time = substr($stripped, $pos + \strlen($needle), 19);


        $results = [];
        $results['token'] = $token;
        $results['date'] = $date;
        $results['time'] = $time;

        return ['0' => $results];

    }

}