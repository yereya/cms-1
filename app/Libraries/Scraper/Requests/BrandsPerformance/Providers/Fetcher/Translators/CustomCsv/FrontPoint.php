<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait FrontPoint
{
    /**
     * Front Point
     * Custom translator for FrontPoint
     *
     * @param        $csv
     * @param        $offset
     * @param int    $limit
     * @param null   $delimiter
     *
     * @return array
     */
    public function frontPoint($csv, $offset = 0, $limit = 0, $delimiter = null)
    {
        // in this translator we can get a comma delimited values for one column,
        // like this: ="zx1iM-09,zx1im-09". In standard csv translate process
        // it is split to 2 columns and corrupts the structure.
        // it should be replaced with: ="zx1iM-09"
        $csv = preg_replace_callback('/="[^,"]*,[^,"]*"/', function ($matches) {
            $first_occurrence = explode(',', $matches[0]);

            return $first_occurrence[0] . '"';
        }, $csv);

        $translated_result = self::translate($csv, $offset, $limit, $delimiter);

        // this brand supplies string data in the following format: '="*******"',
        // so we need to remove all this occurrences
        foreach ($translated_result as &$translated_item) {
            foreach ($translated_item as &$item) {
                $item = str_replace(['=', '"'], '', $item);
            }

            $translated_item['_token'] = $translated_item['Affiliate Sub ID'] ?: $translated_item['Referral Account ID'];
        }

        return $translated_result;
    }
}