<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


trait Jimdo
{

    public function Jimdo($csv, $offset = 0, $limit = 0, $delimiter = null)
    {
        $translated = self::translate($csv, $offset, $limit, $delimiter);

        $headers = preg_split("/[\t]/", array_keys($translated[0])[0]);
        $new_translated_result = [];
        foreach ($translated as $translated_item) {
            $row = preg_split("/[\t]/", array_values($translated_item)[0]);
            $row = array_combine($headers,$row);
            if($row['Affiliate'] =='traffic point ltd.'){
                $new_translated_result[] = $row;
            }
        }
        return $new_translated_result;
    }
}