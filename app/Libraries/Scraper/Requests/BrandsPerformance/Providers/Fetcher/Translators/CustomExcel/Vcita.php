<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel;


use Exception;

trait Vcita
{
    /**
     * @param     $excel_str
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function Vcita($excel_str,$countries_list,$date_col='',$country_col='',$token_col='',$product_col='',$offset = 0, $limit = 0)
    {
        $countries_list = explode(';', $countries_list); // Split countries list by delimiter
        $countries_dict = [];
        # Create Dict of countries and the related commissions
        foreach ($countries_list as $i => $country){
            $splitted_str = explode("=",$country);
            if(sizeof($splitted_str) != 2){
                throw new Exception("Wrong country=>commission format entered");
            }
            $countries_dict[$splitted_str[0]] =  strtolower($splitted_str[1]);
        }

        $final_results = [];
        $results       = $this->translateSpecificSheets($excel_str, $offset,0,1);
        $headers       = $results[0];
        $dict = []; //For empty tokens-> we do counter by date
        foreach (array_slice($results, 1) as $res) // Skip the headers row
        {
            $row = array_combine($headers, $res);
            $date = $row[$date_col];
            $row['commissions'] = 0;
            $row['token'] = '';

            if (strtolower($row[$product_col]) == "marketing") {
                if (strpos($row[$token_col],'token') == false ) // if token not found create counter
                {
                    if (isset($dict[$date])) {
                        $dict[$date] = $dict[$date] + 1;
                    }
                    else {
                        $dict[$date] = 1;
                    }
                    $row['token'] = $date.' - '.$dict[$date];
                }
                else {
                    //If the token shows in the middle of the row
                    if (strpos($row[$token_col],'token') < strpos($row[$token_col],'&')) {
                        $row['token'] = substr($row[$token_col], strpos($row[$token_col], '=') + 1, strpos($row[$token_col], '&') - strpos($row[$token_col], '=') - 1);
                    }//If the token shows in the end of the row
                    else {
                        $row['token'] = substr($row[$token_col], strpos($row[$token_col], 'token=') + 6, strlen($row[$token_col]) - strpos($row[$token_col], 'token=') + 4);
                    }
                }
            }

            else if (strtolower($row[$product_col]) == "billing") {
                if (strpos($row[$token_col],'Subid') == false ) {
                    if (isset($dict[$date])) {
                        $dict[$date] = $dict[$date] + 1;
                    }
                    else {
                        $dict[$date] = 1;
                    }
                    $row['token'] = $date.' - '.$dict[$date];
                }
                else {
                    $row['token'] = substr($row[$token_col], strpos($row[$token_col], 'Subid=') + 6);
                }
            }

            else { // If CRM or Null
                if (strpos($row[$token_col],'medium') == true ) {
                    $row['token'] = substr($row[$token_col], strpos($row[$token_col], 'medium=') + 7);

                }elseif (strpos($row[$token_col],'Subid') == true){
                    $row['token'] = substr($row[$token_col], strpos($row[$token_col], 'Subid=') + 6);

                } else {
                    if (isset($dict[$date])) {
                        $dict[$date] = $dict[$date] + 1;
                    }
                    else {
                        $dict[$date] = 1;
                    }
                    $row['token'] = $date.' - '.$dict[$date];
                }
                if (strtolower($row[$product_col]) == "crm") {
                    # Attach commission based on the dict
                    $country_code = strtolower($row[$country_col]);
                    if(array_key_exists($country_code,$countries_dict)){
                        # Validate the type of the commissions
                        if(!is_numeric($countries_dict[$country_code])){
                            throw new Exception("The commissions for ".$country_code." is not a correct number.");
                        }
                        $row['commissions'] = intval($countries_dict[$country_code]);
                    }else{
                        if(array_key_exists('default',$countries_dict)){
                            if(!is_numeric($countries_dict['default']))
                                throw new Exception("The commissions for ".'default'." is not a correct number.");
                            $row['commissions'] = intval($countries_dict['default']);
                        }else{
                            $row['commissions'] = 0;
                        }
                    }
                }
            }
            $final_results[] = $row;
        }
        if ($limit) {
            return array_slice($final_results, $offset, count($results) - $limit);
        }
        return $final_results;
    }
}