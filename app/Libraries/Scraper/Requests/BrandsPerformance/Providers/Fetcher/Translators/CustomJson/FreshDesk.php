<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait FreshDesk
{
    /**
     * Get Fresh desk rewords stats from api
     * - get all records except excluded keys
     *
     * @param      $json
     * @param      $excluded_keys
     * @param null $selector
     * @param null $item_group_selector
     *
     * @return mixed
     */
    public function getFreshDeskRewards($json, $selector = null, $excluded_keys, $item_group_selector = null)
    {
        $json          = $this->translate($json, $selector, $item_group_selector);
        $result        = [];
        $excluded_keys = explode(';', $excluded_keys);

        foreach ($json as $idx => $records) {
            if (\in_array($idx, $excluded_keys, true)) {
                continue;
            }

            $records = $this->prepareStats($records);
            $result  = array_merge($result, $records);
        }

        return $result;
    }


    /**
     * @param $records
     *
     * @return array
     */
    public function prepareStats($records): array
    {
        $records = collect($records['rewards']);
        $result  = collect();

        $records->each(function ($record) use ($result) {
            $temp = collect();

            $date = date('Y-m-d', $record['created_at'] / 1000);

            $temp->put('date', $date);
            $temp->put('amount', $record['amount'] / 100);
            $temp->put('token', $record['key']);

            $result->push($temp);
        });

        return $result->toArray();
    }
}