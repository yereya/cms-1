<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait translateWithGroupsSelector
{
    /**
     * @param      $json
     * @param null $selector
     * @param null $item_group_selector
     *
     * @return mixed
     */
    public function translateWithGroupsSelector($json, $selector = null, $item_group_selector = null)
    {
        $this->jsonToArray($json);

        if (is_null($selector)) {
            return $json;
        }

        $this->parseSelector($selector);
        $result = [];
        foreach ($selector as $sel) {
            $this->parseJson($json, $sel, 0, $result);
        }

        // in case we have a nested structure like in documentation to flattenJson function
        if ($item_group_selector) {
            $result = $this->flattenJsonManyGroups($result, $item_group_selector);
        }

        return $result;
    }
}