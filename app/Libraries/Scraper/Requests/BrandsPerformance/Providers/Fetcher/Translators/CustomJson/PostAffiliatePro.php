<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

trait PostAffiliatePro
{
    /**
     * postAffiliatePro converts the following json format:
     * * [
     * {
     * "rows": [
     * [
     * "id",
     * "commission",
     * "totalcost",
     * "fixedcost",
     * "t_orderid",
     * "dateinserted",
     * "countrycode",
     * "name",
     * "rtype",
     * "commissionTypeName",
     * "tier",
     * "rstatus",
     * "firstclickdata1",
     * "lastclicktime",
     * "lastclickdata1",
     * "lastclickdata2",
     * "data1",
     * "merchantnote",
     * "channel"'
     * ],
     * [
     * "jq8vovio",
     * "13",
     * "0",
     * "0",
     * "43127231",
     * "2019-09-28 23:03:05",
     * "FR",
     * "Default ExpressVPN Campaign",
     * "S",
     * "",
     * "1",
     * "A",
     * "",
     * "2019-09-28 23:02:04",
     * "a1ZqNjDScpo",
     * "",
     * "",
     * "",
     * ""
     * ], .......
     * to associated array
     *
     * @param        $json
     * @param string $header_idx
     * @param int    $offset
     * @param string $date_col_name
     *
     * @return array
     */
    public function postAffiliatePro($json, $header_idx = 'rows', $offset = 13, $date_col_name='dateinserted')
    {
        $data_array = json_decode($json, true)[0];
        $rows = $data_array[$header_idx];
        $assoc_items = [];

        $headers    = $rows[0];
        $headers[] = 'mylocaltime';
        $rows_count = count($rows);

        for ($i = 1; $i < $rows_count; $i++) {
            // column "firstclickdata1"
            $rows[$i][$offset]  = urldecode($rows[$i][$offset]);
            $date_index = array_search($date_col_name, $headers);

            $rows[$i][] = date(strtotime($rows[$i][$date_index])) != false
                ?  date("Y-m-d H:i:s",strtotime($rows[$i][$date_index].'+10 hours'))
                : null;

            $assoc_items[] = array_combine($headers, $rows[$i]);
        }

        return $assoc_items;
    }

    /**
     * @param        $json
     * @param null   $header_idx
     * @param int    $offset
     * @param string $var_names
     *
     * @return array
     */
    public function getPostAffiliateProAttributes($json, $header_idx = null, $offset = 8, $var_names = 'fileId') {
        $translated_items   = $this->postAffiliatePro($json, $header_idx, $offset);
        $var_array          = explode(';', $var_names);
        $var_atts           = [];

        foreach ($translated_items as $translated_item) {
            $_key = $translated_item['name'];
            if ( in_array( $_key, $var_array) ) {
                $var_atts[$_key] = $translated_item['value'];
            }
        }

        $combined = array_combine($var_array, $var_atts);

        return $combined;
    }
}