<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait Foxy
{
    /**
     * Foxy
     * Custom translator for Foxy
     *
     * @param $csv
     * @param $offset
     *
     * @return mixed
     */
    public function foxy($csv, $offset = 0)
    {
        $translated_result = self::translate($csv, $offset);

        $event_date = date('Y-m-d', strtotime('-' . $this->fetcher['properties']['days'] . ' days'));
        $event      = [
            'meta_date' => $event_date,
            'token'     => '',
            'event'     => '',
        ];

        $events = [];

        // column: New Registrations -> event type leads
        // column: FTD -> event type sales
        // illustration:
        // array:10 [
        //  0 => array:2 [
        //    "col0" => "New Registrations"
        //    "Amount" => "0"
        //  ]
        //  1 => array:2 [
        //    "col0" => "FTD"
        //    "Amount" => "0"
        //  ]
        //  2 => array:2 [
        //    "col0" => "Deposits"
        //    "Amount" => "0"
        //  ]
        //  3 => array:2 [
        //    "col0" => "Withdrawals"
        //    "Amount" => "0"
        //  ]....
        foreach ($translated_result as &$translated_item) {
            if ($translated_item['col0'] == 'New Registrations' || $translated_item['col0'] == 'FTD') {
                $event_num = $translated_item['Amount'];
                if ($event_num > 0) {
                    $event['event'] = ($translated_item['col0'] == 'New Registrations' ? 'lead' : 'sale');
                    for ($i = 1; $i <= $event_num; $i++) {
                        $event['token'] = $event['meta_date'] . '-' . $i;
                        $events[]       = $event;
                    }
                }
            }
        }

        return $events;
    }
}