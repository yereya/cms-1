<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators;

use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers\GeneralHelper;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers\HtmlHelper;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Affilinet;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\AmazonSlots;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Emma;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\BuzzBingo;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\LullTranslator;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Bet365;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\BVpn;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\CostaBingo;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\EHarmonyUK;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\HideMyAss;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\JDate;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Karamba;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\PostAffiliateProGetSParam;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Reimage;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\SendinBlue;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Simba;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\TrendMicro;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Webgo;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Upserve;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\mailTranslator8x8;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\VonageUS;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Toast;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\LifeFone;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\taxRelief;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Cake;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\Anthem;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\SelectHw;
use App\Libraries\TpLogger;
use QueryPath;
use Symfony\Component\DomCrawler\Crawler;


/**
 * Class BrandCsv
 *
 * @package App\Libraries\Scraper\Brands\Translators
 */
class HtmlLib extends BaseTranslator
{
    use BVpn;
    use Simba;
    use Webgo;
    use JDate;
    use Bet365;
    use Karamba;
    use Reimage;
    use Affilinet;
    use HideMyAss;
    use SendinBlue;
    use CostaBingo;
    use TrendMicro;
    use HtmlHelper;
    use EHarmonyUK;
    use GeneralHelper;
    use PostAffiliateProGetSParam;
    use AmazonSlots;
    use BuzzBingo;
    use Emma;
    use LullTranslator;
    use Upserve;
    use mailTranslator8x8;
    use VonageUS;
    use Toast;
    use LifeFone;
    use taxRelief;
    use Cake;
    use Anthem;
    use SelectHw;

    /**
     * @var crawler - Symfony Crawler class
     */
    protected $crawler;

    /**
     * @var array
     */
    protected $keys = [];

    /**
     * @var array
     */
    protected $values = [];

    /**
     * @var string
     */
    protected $attribute = 'value';

    /**
     * translate
     * init translation table
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     *
     * @return array
     * @throws \Exception
     */
    public function translate($html, $table_selector = null, $header_selector = 'th', $limit = 0)
    {
        $this->createCrawler($html);
        $table   = $this->filter($table_selector);
        $assoc   = true;
        $results = [];

        if ($table->getNode(0)) {

            $this->getKeys($table, $header_selector);

            if ($header_selector) {
                $this->getValues($table, 1);
            } else {
                $this->getValues($table);
            }

            if ($limit != 0) {
                $this->values = array_slice($this->values, 0, $limit);
            }

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc   = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }

            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($results, $assoc);
            }

            return $results;
        }
        $tp_logger = TpLogger::getInstance();
        $tp_logger->notice('There is no data available or provided table selector parameter is wrong: ' . $table_selector);

        return [];
    }

    /**
     * Translated and remove currency signs
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     *
     * @return array
     * @throws \Exception
     */
    public function translateWithoutSign($html, $table_selector = null, $header_selector = 'th', $limit = 0)
    {
        $translated_result = self::translate($html, $table_selector, $header_selector, $limit);
        $final_results = [];
        foreach ($translated_result as $result){

            $translated = self::removeSigns($result);
            $final_results[] = $translated;
        }
        return $final_results;
    }

    /**
     * Remove signs and convert to integer numbers
     * @param $values
     * @return array
     *
     */
    public function removeSigns($values)
    {
        $arr = [];
        foreach($values as $key => $val){
            $val = str_replace( "£", "",$val);
            $val = str_replace( "$", "",$val);
            $val = str_replace( ",", "",$val);
            $val = str_replace( "\n", "",$val);
            if (is_numeric($val))
                $val = doubleval($val);
            $arr[$key] = $val;
        }
        return $arr;
    }

    /**
     * Init Symfony Crawler
     *
     * @param $html
     *
     * @return Crawler
     */
    private function createCrawler($html)
    {
        if (!$this->crawler) {
            return $this->crawler = new Crawler($html);
        }

        return $this->crawler;
    }

    /**
     * find table
     *
     * @param $selectors
     *
     * @return null|Crawler
     * @throws \Exception
     */
    private function filter($selectors)
    {
        if (isset($selectors)) {
            return $this->crawler->filter($selectors);
        }

        throw new \Exception('Selector not found: ' . $selectors);
    }

    /**
     * Get keys if exists
     *
     * @param        $crawler
     * @param string $header_selector
     */
    private function getKeys($crawler, $header_selector)
    {
        $crawler->filter('tr')->first()->reduce(function ($tr) use ($header_selector) {
            $tr->filter($header_selector)->reduce(function ($node, $i) {
                $this->keys[$i] = $this->removeNbsp(trim($node->text()));
            });
        });
    }

    private function removeNbsp($str)
    {
        $str = htmlentities($str, null, 'utf-8');
        $str = str_replace("&nbsp;", "", $str);

        return html_entity_decode($str);
    }

    /**
     * Get values if exists
     *
     * @param        $crawler
     * @param int    $offset
     * @param string $selector
     */
    private function getValues($crawler, $offset = 0)
    {
        $idx = 0;
        $crawler->filter('tr')->reduce(function ($tr) use (&$idx, $offset) {
            if ($idx >= $offset && $tr->attr('style') != 'display:none;') {
                $tr->filter('td')->reduce(function ($td, $i) use ($idx) {
                    $this->values[$idx][$i] = $this->removeNbsp(trim($td->text()));
                });
            }
            $idx++;
        });
    }

    /**
     * Combine keys and values to one array
     *
     * @return array
     */
    private function keyValueToArray()
    {
        $results = [];

        foreach ($this->values as $value) {
            if (count($value) == count($this->keys)) {
                for ($i = 0; $i < count($value); $i++) {
                    $value[$i] = trim($value[$i]);
                }
                $results[] = array_combine($this->keys, $value);
            }
        }
        return $results;
    }

    /**
     * build Date Column
     * adds date column for reports that requested day by day
     *
     * @param $translated_result
     * @param $assoc
     *
     * @return mixed
     */
    private function buildDateColumn(&$translated_result, $assoc)
    {
        $event_date = date('Y-m-d', strtotime('-' . $this->fetcher['properties']['days'] . ' days'));

        if ($assoc) {
            foreach ($translated_result as &$translated_item) {
                $translated_item['meta_date'] = $event_date;
            }
        } else {
            foreach ($translated_result as &$translated_item) {
                $translated_item[] = $event_date;
            }
        }

        return $translated_result;
    }

    /**
     * Translate Value
     * Assigns given selector's attribute to $var_name for use in fetcher
     * Note: the subquery params will be appended to the result array
     * for *each* selector in which it appears
     * Example: for $selectors=a;button, $attribute_name=href and subquery_params=a,b,c,d
     * an input <a href="...&a=1&b=2"> and <button href="...&c=3&b=1>
     * will yield the result set: ["a" => 1, "b" => ["0" => 2, "1" => 1], "c" => 3]
     *
     * @param             $html
     * @param null        $selectors
     * @param string      $var_name
     * @param null        $attribute_name
     * @param null        $subquery_params
     * @param null|string $subquery_delimiter
     *
     * @return array
     * @throws \Exception
     * @internal param null $subquery_param
     */
    public function translateValue(
        $html,
        $selectors = null,
        $var_name = 'HtmlLib@translateValue',
        $attribute_name = null,
        $subquery_params = null,
        $subquery_delimiter = '&'
    )
    {
        $this->createCrawler($html);

        $this->attribute = is_null($attribute_name) ? $this->attribute : $attribute_name;

        $selectors = $this->removeBrackets($selectors);
        $var_name  = $this->removeBrackets($var_name);

        $sel_arr      = explode(';', $selectors);
        $var_name_arr = explode(';', $var_name);
        $sel_name_arr = array_combine($sel_arr, $var_name_arr);
        if (!empty($subquery_params)) {
            $url_query_params = explode(';', $subquery_params);
        }

        if (!$sel_name_arr) {
            throw new \Exception('this parameters is wrong');
        }

        $result = [];
        foreach ($sel_name_arr as $selector => $name) {
            $node = $this->filter($selector);

            if ($node->getNode(0)) {
                $node_attr_val = $this->getAttribute($node);

                if (!empty($url_query_params)) {
                    foreach ($url_query_params as $query_param) {
                        $result[$query_param] = $this->extractUrlSubQueryParam($node_attr_val, $query_param,
                            $subquery_delimiter);
                    }
                } else {
                    $result[$name] = $node_attr_val;
                }
            }
        }

        return $result;
    }

    private function removeBrackets($str)
    {
        if ($str[0] == '[' && substr($str, -1) == ']') {
            $str = substr($str, 1, -1);
        }

        return $str;
    }

    /**
     * @param $node
     *
     * @return mixed
     */
    private function getAttribute($node)
    {
        if ($this->attribute && $this->attribute != 'text') {
            return $node->attr($this->attribute);
        } else {
            if ($this->attribute == 'text') {
                return $node->text();
            }
        }
    }


    public function translateByNeedle($html, $needle, $end_character = '"',
                                      $var_name = 'HtmlLib@translateByPregMatch'): array
    {
        $pos     = strpos($html, $needle);
        $pos_end = strpos($html, $end_character, $pos + \strlen($needle));

        // the S param value, example: 1d32b25aa79ac9db49eca068704acfa8
        $s = substr($html, $pos + \strlen($needle), $pos_end - ($pos + \strlen($needle)) - 1);

        // S param should be returned with quotation, example: "1d32b25aa79ac9db49eca068704acfa8"
        return [$var_name => '"' . $s . '"'];
    }

}