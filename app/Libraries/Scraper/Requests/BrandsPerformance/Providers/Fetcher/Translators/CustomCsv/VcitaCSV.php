<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


use Exception;

trait VcitaCSV
{
    /** Special function for Vcita csv mails, cut token from string and have country dictionary
     * @param $csv
     * @param $countries_list
     * @param string $date_col
     * @param string $country_col
     * @param string $token_col
     * @param string $product_col
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @return array
     * @throws Exception
     */
    public function Vcita($csv,$countries_list,$date_col='',$country_col='',$token_col='',$product_col='',$offset = 0, $limit = 0,$delimiter = null)
    {
        $countries_list = explode(';', $countries_list); // Split countries list by delimiter
        $countries_dict = [];
        # Create Dict of countries and the related commissions
        foreach ($countries_list as $i => $country) {
            $splitted_str = explode("=", $country);
            if (sizeof($splitted_str) != 2) {
                throw new Exception("Wrong country=>commission format entered");
            }
            $countries_dict[$splitted_str[0]] = strtolower($splitted_str[1]);
        }

        $final_results = [];
        $results = self::translate($csv, $offset, $limit, $delimiter);

        $dict = []; //For empty tokens-> we do counter by date
        foreach ($results as $row) {
            $date = $row[$date_col];
            $row['commissions'] = 0;
            $row['token'] = '';

            if (strtolower($row[$product_col]) == "marketing") {
                if (strpos($row[$token_col], 'token') == false) // if token not found create counter
                {
                    if (isset($dict[$date])) {
                        $dict[$date] = $dict[$date] + 1;
                    } else {
                        $dict[$date] = 1;
                    }
                    $row['token'] = $date . ' - ' . $dict[$date];
                } else {                                              // if token found
                    $pattern = "/[&]/";
                    $parts = preg_split($pattern, $row[$token_col]);
                    $row['token'] = $parts[0];
                    $row['token'] = substr($parts[0], 60);
                }
            } else if (strtolower($row[$product_col]) == "billing") {
                if (strpos($row[$token_col], 'Subid') == false) {
                    if (isset($dict[$date])) {
                        $dict[$date] = $dict[$date] + 1;
                    } else {
                        $dict[$date] = 1;
                    }
                    $row['token'] = $date . ' - ' . $dict[$date];
                } else {
                    $row['token'] = substr($row[$token_col], strpos($row[$token_col], 'Subid') + 5);
                }
            } else { // If contact management or Null
                if (strpos($row[$token_col], 'medium') == true) {
                    $row['token'] = substr($row[$token_col], strpos($row[$token_col], 'medium') + 6);

                } elseif (strpos($row[$token_col], 'Subid') == true) {
                    $row['token'] = substr($row[$token_col], strpos($row[$token_col], 'Subid') + 5);

                } else {
                    if (isset($dict[$date])) {
                        $dict[$date] = $dict[$date] + 1;
                    } else {
                        $dict[$date] = 1;
                    }
                    $row['token'] = $date . ' - ' . $dict[$date];
                }
                if (strtolower($row[$product_col]) == "contact management") {
                    # Attach commission based on the dict
                    $country_code = strtolower($row[$country_col]);
                    if (array_key_exists($country_code, $countries_dict)) {
                        # Validate the type of the commissions
                        if (!is_numeric($countries_dict[$country_code])) {
                            throw new Exception("The commissions for " . $country_code . " is not a correct number.");
                        }
                        $row['commissions'] = intval($countries_dict[$country_code]);
                    } else {
                        if (array_key_exists('default', $countries_dict)) {
                            if (!is_numeric($countries_dict['default']))
                                throw new Exception("The commissions for " . 'default' . " is not a correct number.");
                            $row['commissions'] = intval($countries_dict['default']);
                        } else {
                            $row['commissions'] = 0;
                        }
                    }
                }
            }
            $final_results[] = $row;
        }

        return $final_results;
    }
}