<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers;

use Exception;

trait GeneralHelper
{
    /**
     * Extracts a subquery parameter value from a url query using a value delimiter
     *
     * @param $url
     * @param $subquery_param
     * @param $delimiter
     *
     * @return null
     * @throws \Exception
     * @internal param $result
     */
    public function extractUrlSubQueryParam($url, $subquery_param, $delimiter)
    {
        if (!$url) {

            return null;
        }

        $query_values = [];

        $pairs = explode($delimiter, parse_url($url)['query']);

        foreach ($pairs as $pair) {
            list($key, $value) = explode("=", $pair, 2);

            if (isset($query_values[$key])) {
                // stick multiple values into an array
                if (is_array($query_values[$key])) {
                    $query_values[$key][] = $value;
                } else {
                    $query_values[$key] = [$query_values[$key], $value];
                }
            } else {
                $query_values[$key] = $value;
            }
        }

        if (!array_key_exists($subquery_param, $query_values)) {

            throw New \Exception('Cannot find the specified URL param: ' . $subquery_param . ' in ' . $url);
        }

        return $query_values[$subquery_param];
    }

    /**
     * Appends columns as a mutually exclusive value
     *
     * @param       $final_table
     * @param       $table_row
     * @param array $exclusive_keys
     */
    public function appendMutuallyExclusiveKeys(&$final_table, $table_row, array $exclusive_keys = [])
    {
        // Remove null keys as they should not be processed
        $exclusive_keys = array_filter($exclusive_keys);

        if (!$exclusive_keys) {
            return;
        }

        // Appends every exclusive key as 1 the amount of times according to the value
        foreach ($exclusive_keys as $idx => $exclusive_key) {
            if (!isset($table_row[$exclusive_key]) || !$exclusive_key) {
                continue;
            }

            $cur_key_value = $table_row[$exclusive_key];

            if (!is_numeric($cur_key_value) || $cur_key_value <= 0) {
                continue;
            }

            // All the keys except the current one
            $complementing_keys = array_diff($exclusive_keys, [$exclusive_key]);
            $result = $table_row;

            // Fill complementary keys with 0
            foreach ($complementing_keys as $idx => $complementing_key) {
                $result[$complementing_key] = 0;
            }

            // Let the current key equal 1
            $result[$exclusive_key] = 1;

            for ($i = 0; $i < $cur_key_value; $i++) {
                $final_table[] = $result + ['counter_col' => $i];
            }
        }
    }

    /**
     * @param $array
     * @param $date_col
     * @param $seen_dates
     * @param $final_result
     */
    public function countDuplicateDates($array, $date_col, &$seen_dates, &$final_result)
    {
        $_date = $array[$date_col] ?? null;
        if (is_null($_date)) {
            return;
        }

        if (empty($seen_dates[$_date])) {
            $seen_dates[$_date] = 1;
        }

        $array['counter_col'] = $_date . '-' . $seen_dates[$_date]++;
        $final_result[] = $array;
    }

    /**
     * Appends the counter by chosen column PER date
     *
     * @param $array
     * @param $date_col
     * @param $seen_dates
     * @param $chosen_col
     * @param $final_result
     */
    public function countColByDate($array, $date_col, &$seen_dates, $chosen_col, &$final_result)
    {
        $counter_amount = $array[$chosen_col] ?? 1;
        if ($counter_amount > 1) {
            $array[$chosen_col] = 1;
        }

        // Parses the current row's date
        $_date = $array[$date_col];

        // If we have already seen this date - add its counter amount
        if (empty($seen_dates[$_date])) {
            $seen_dates[$_date] = 1;
        }

        for ($i = 1; $i <= $counter_amount; $i++) {
            $str = $_date . '-' . $seen_dates[$_date] + $i;
            $final_result[] = $array + ['counter_col' => $str];
        }

        // Adds the counter column amount to the seen dates column counter amount
        $seen_dates[$_date] += $counter_amount;
    }

    /**
     * Separates translated results by date column
     *
     * @param array $translated_results
     * @param string $cols_to_traverse
     * @param string $date_col
     * @param string $regex_when_to_count
     *
     * @return array
     */
    public function countByDate($translated_results = [], $cols_to_traverse = '', $date_col = 'Date',
                                $regex_when_to_count = '')
    {
        if (empty($translated_results)) {
            return [];
        }
        $arr = array_map(function (&$row) {
            $row['modified_result'] = '';
            return $row;
        }, $translated_results);

        $seen_dates = [];
        $modified_results = $this->customizeResults($arr, $cols_to_traverse, $regex_when_to_count);
        foreach ($modified_results as &$modified_result) {
            if (empty($seen_dates[$modified_result[$date_col]])) {
                $seen_dates[$modified_result[$date_col]] = 1;
            } else {
                $seen_dates[$modified_result[$date_col]]++;
            }

            $modified_result['modified_result'] = $modified_result[$date_col] . '-' . $seen_dates[$modified_result[$date_col]];
        }

        return $modified_results;
    }

    /**
     * Appends the counter by chosen column
     *
     * @param $array
     * @param $chosen_col
     * @param $final_result
     */
    public function countByColumn($array, $chosen_col, &$final_result)
    {
        $counter_amount = $array[$chosen_col] ?? 1;
        if ($counter_amount > 1) {
            $array[$chosen_col] = 1;
        }

        for ($i = 1; $i <= $counter_amount; $i++) {
            $final_result[] = $array + ['counter_col' => $i];
        }
    }

    public function removeSigns($values)
    {
        $arr = [];
        foreach($values as $val){
            $params = [];
            foreach ($val as $key => $param){
                $param = str_replace( "£", "",$param);
                $param = str_replace( "$", "",$param);
                $param = str_replace( "\n", "",$param);
                $params[$key] = $param;
            }
            $arr[] = $params;
        }
        return $arr;
    }

    public function customizeResults($translated_results_arr = [], $cols = '', $regex_rules = '')
    {
        // If there are no columns to execute rules upon, then everything is valid
        if (!$cols) {

            return $translated_results_arr;
        }

        // Initializes the result matcher
        $matched_results_arr = [];
        // Create a key => value array of the form: {col_name} => {rule}
        $regex_rules_arr = array_combine(explode(';', $cols), explode(';', $regex_rules));

        // For each result, check if all of the rules are true
        foreach ($translated_results_arr as $translated_row) {
            // Initialize the empty ruleset as true
            $all_rules_passed = true;

            // Iterate over the rules in order to see if one fails
            foreach ($regex_rules_arr as $col_name => $regex_rule) {
                $logical_rule = true;

                // If there is a NOT logic in the rule, remove the "!" sign and make the logical rule false
                if (strpos($regex_rule, '!') !== false) {
                    $logical_rule = false;
                    $regex_rule   = str_replace('!', '', $regex_rule);
                }

                $rule_applies = preg_match('/' . $regex_rule . '/', $translated_row[$col_name]) == $logical_rule;

                if (!($all_rules_passed = ($all_rules_passed && $rule_applies))) {
                    break;
                }
            }

            if (!$all_rules_passed) {
                continue;
            }

            $matched_results_arr[] = $translated_row;
        }

        return $matched_results_arr;
    }

    /**
     * Parses an already translated result array and counts unique column combinations
     *
     * @param $translated_results
     * @param $col_names_str
     *
     * @return array
     */
    public function uniquifyColumn($translated_results, $col_names_str = '')
    {
        $col_names_arr                  = explode(';', $col_names_str);
        $result_arr                     = [];
        $existing_combinations_counters = [];

        foreach ($translated_results as $translated_result) {
            // Creates a unique string for the current row's values to serve as a combination key
            $combination = trim(implode(array_values(array_intersect_key($translated_result, array_flip($col_names_arr)))));
            if (array_key_exists($combination, $existing_combinations_counters)) {
                $count = ++$existing_combinations_counters[$combination]['counter'];
            } else {
                $count = $existing_combinations_counters[$combination]['counter'] = 1;
            }
            $result_arr[] = $translated_result + ['counter_col' => $count];
        }

        return $result_arr;
    }

    /**
     * Build a separate row for each column value
     *
     * @param      $array
     * @param null $selected_cols_str
     *
     * @return array
     * @throws \Exception
     */
    public function flattenRows($array, $selected_cols_str = null): array
    {

        $selected_col_arr = explode(';', $selected_cols_str);
        $result_set       = [];

        // By design, every column should exist in every row such that any column check would work
        if (!array_key_exists($selected_col_arr[0], $array[0])) {
            throw new \Exception("The passed counter column, " . $selected_col_arr[0] . ", could not be found");
        }

        // For each original row, append the resulting flattened rows to the result set
        foreach ($array as $translated_item) {
            $this->appendFlattenedRowsRecursive($result_set, $translated_item, $selected_col_arr);
        }

        return $result_set;
    }

    /**
     * Appends the flattened rows to the result set recursively.
     *
     * @param array $result_set
     * @param array $referenced_row
     * @param array $column_arr
     */
    private function appendFlattenedRowsRecursive(
        &$result_set = [],
        $referenced_row = [],
        array $column_arr = []
    )
    {
        // memorize current column name and slice it from the remaining columns
//        $cur_col_name   = $column_arr[0];
//        $unchecked_cols = array_slice($column_arr, 1);
//        $sliced_col_atts = $unchecked_cols;

        foreach ($column_arr as $col_idx => $cur_col_name) {
            if (!empty($referenced_row[$cur_col_name])) {
//            $this->appendFlattenedRowsRecursive($result_set, $referenced_row, $sliced_col_atts);
                $this->flattenSingleCol($result_set, $cur_col_name, $col_idx, $referenced_row, $column_arr);
            }
        }
    }

    /**
     * Flattens the given column's value to separate rows
     *
     * @param $result_set
     * @param $cur_col_name
     * @param $cur_col_idx
     * @param $referenced_row
     * @param $column_arr
     */
    private function flattenSingleCol(&$result_set, $cur_col_name, $cur_col_idx, $referenced_row, $column_arr): void
    {
        $column_val                           = $referenced_row[$cur_col_name] ?? 0;
        $result_row                           = $referenced_row;
        $result_row['index_' . $cur_col_name] = 1;

        if (empty($column_val)) {
            return;
        }

        for ($i = 1; $i <= $column_val; $i++) {
            $result_row[$cur_col_name]            = 1;
            $result_row['index_' . $cur_col_name] = $i;

            // if the next column exists - make another recursive call from the current column
            $this->emptyUncheckedCols($result_row, $column_arr, $column_arr[$cur_col_idx]);
            $result_set[] = $result_row;
        }
    }

    /**
     * @param $referenced_row
     * @param $selected_cols
     * @param $current_col
     */
    private function emptyUncheckedCols(&$referenced_row, $selected_cols, $current_col): void
    {
        $except_current_cols = array_diff($selected_cols, [$current_col]);
        foreach ($except_current_cols as $unchecked_col) {
            $referenced_row[$unchecked_col] = 0;
        }

        // prefixes each array key with '_index'
        $altered_col_keys = array_map(function ($element) {
            return 'index_' . $element;
        }, $except_current_cols);

        // fills not existing keys
        $referenced_row = array_merge($referenced_row, array_fill_keys(array_values($altered_col_keys), 0));
    }
    /**
     * @param $sleep_time
     */
//    public function add_sleep_for_translators($html,$sleep_time = 90): void
    public function add_sleep_for_translators($html,$sleep_time = 90): void
    {
        # Check $sleep_time's type
        if(!is_numeric($sleep_time)){
            throw new Exception(" Error in the format of the sleep time variable, use numeric format - GeneralHelper.php trait.");
        }
        # For safety
        if((int)$sleep_time > 240 ){
            $sleep_time = 240;
        }
        
        sleep((int)$sleep_time);
    }

}
