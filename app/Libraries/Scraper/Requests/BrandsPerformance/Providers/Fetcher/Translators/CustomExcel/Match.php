<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel;


trait Match
{
    /**
     * @param     $excel_str
     * @param     $row_offset
     *
     * @return array|null
     */
    public function Match($excel_str, $row_offset)
    {
        // Create excel reader object(PHPExcel)
        try {
            $php_excel_obj = $this->createExcelReader($excel_str);
        } catch (\Exception $e) {
            return [];
        }

        if (empty($php_excel_obj)) {

            return null;
        }

        $excel_sheets = $this->getAllSheets($php_excel_obj);
        $results      = array_flatten($excel_sheets, 1);

        if ($row_offset) {
            $results = array_slice($results, $row_offset);
        }

        /*Update custom headers*/
        $headers = $results[0];
        /**/
        $results = collect(array_values($results))
            ->map(function ($result) use ($headers) {
                $result = collect($result)->filter(function ($val, $k) use ($headers) {
                    return !is_null($headers[$k]);
                });

                return $result;
            });

        /*remove the headers*/
        $headers = collect($results[0]);
        $results->shift();

        /*combine headers with values*/
        $results->transform(function ($result) use ($headers) {
            return $headers->combine($result);
        });

        return $results->toArray();
    }
}