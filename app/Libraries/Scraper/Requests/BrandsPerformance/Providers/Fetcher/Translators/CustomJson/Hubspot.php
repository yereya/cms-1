<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait Hubspot
{
    /**
     * google spreadsheet translator for Hubspot
     *
     * @param      $json
     * @param null $selector
     * @param      $countries_list
     * @param null $item_group_selector
     *
     * @return array
     */
    public function HubspotGoogleSpreadSheet($json, $selector = null,$countries_list, $item_group_selector = null)
    {
        $countries_list = explode(';', $countries_list); // Split countries list by delimiter
        $countries_dict = [];
        # Create Dict of countries and the related commissions
        foreach ($countries_list as $i => $country){
            $splitted_str = explode("=",$country);
            if(sizeof($splitted_str) != 2){
                throw new Exception("Wrong country=>commission format entered");
            }
            $countries_dict[$splitted_str[0]] =  strtolower($splitted_str[1]);
        }


        $translated_result = self::translate($json, $selector, $item_group_selector);
        if (!$translated_result)
            return [];
        $headers           = $translated_result[0];
        $final_results     = [];

        $headers[]="Commision";
        $dict = []; //For avoid duplicates

        for ($i = 1; $i < count($translated_result); $i++) {

            # Attach commission based on the dict
            $country_code = $translated_result[$i][2];
            if(array_key_exists($country_code,$countries_dict)){
                # Validate the type of the commissions
                if(!is_numeric($countries_dict[$country_code])){
                    throw new Exception("The commissions for ".$country_code." is not a correct number.");
                }
                $translated_result[$i][]=intval($countries_dict[$country_code]);
            }else{
                throw new Exception("Country's code doesn't match. Check the country's codes you have entered in the fetcher or the codes in the excel file.");
            }

            $headers[7] = "trx_id";
            $translated_result[$i][3] = str_replace("EM_","",$translated_result[$i][3]);
            $translated_result[$i][3] = str_replace("crm_","",$translated_result[$i][3]);
            $token = $translated_result[$i][3];

            if($token == 'crm') {
                $translated_result[$i][7] = $token;
            }
            else {
                if (isset($dict[$token])) {
                    $dict[$token] = $dict[$token] + 1;
                } else {
                    $dict[$token] = 1;
                }
                if ($dict[$token] >= 1) {
                    $translated_result[$i][7] = $token . ' - ' . $dict[$token];
                }
                else{
                    $translated_result[$i][7] = $token;
                }
            }

            // Completes the truncated missing columns for each translated result so that we can append the values to the keys
            $col_num_diff = max(count($headers), count($translated_result[$i])) - min(count($headers), count($translated_result[$i]));
            $complementary_result = $translated_result[$i] + array_fill(count($translated_result[$i]), $col_num_diff, '');
            $final_results[] = array_combine($headers, $translated_result[$i]);


        }

        return $final_results;
    }
    public function HubspotGoogleSpreadSheetWithOffset($json, $selector = null,$countries_list, $item_group_selector = null, $offset = null,$country_col_idx='Region',$token_col_idx='utm_term')
    {
        $countries_list = explode(';', $countries_list); // Split countries list by delimiter
        $countries_dict = [];
        # Create Dict of countries and the related commissions
        foreach ($countries_list as $i => $country){
            $splitted_str = explode("=",$country);
            if(sizeof($splitted_str) != 2){
                throw new Exception("Wrong country=>commission format entered");
            }
            $countries_dict[$splitted_str[0]] =  strtolower($splitted_str[1]);
        }

        $translated_result = self::translate($json, $selector, $item_group_selector);
        if (!$translated_result)
            return [];
        if($offset){
            $translated_result = array_slice($translated_result, $offset);
        }
        $headers           = $translated_result[0];
        $final_results     = [];

        $headers[]="Commision";
        $dict = []; //For avoid duplicates

        for ($i = 1; $i < count($translated_result); $i++) {

            # Attach commission based on the dict
            $country_code = $translated_result[$i][$country_col_idx];
            if(array_key_exists($country_code,$countries_dict)){
                # Validate the type of the commissions
                if(!is_numeric($countries_dict[$country_code])){
                    throw new Exception("The commissions for ".$country_code." is not a correct number.");
                }
                $translated_result[$i][]=intval($countries_dict[$country_code]);
            }else{
                $translated_result[$i][] = ($countries_dict['Other']);
                //throw new Exception("Country's code doesn't match. Check the country's codes you have entered in the fetcher or the codes in the excel file.");
            }

            $headers[7] = "trx_id";
            $token = $translated_result[$i][$token_col_idx];

            if($token == 'crm') {
                $translated_result[$i][7] = $token;
            }
            else {
                if (isset($dict[$token])) {
                    $dict[$token] = $dict[$token] + 1;
                } else {
                    $dict[$token] = 1;
                }
                if ($dict[$token] >= 1) {
                    $translated_result[$i][7] = $token . ' - ' . $dict[$token];
                }
                else{
                    $translated_result[$i][7] = $token;
                }
            }


            // Completes the truncated missing columns for each translated result so that we can append the values to the keys
            $col_num_diff = max(count($headers), count($translated_result[$i])) - min(count($headers), count($translated_result[$i]));
            $complementary_result = $translated_result[$i] + array_fill(count($translated_result[$i]), $col_num_diff, '');
            $final_results[] = array_combine($headers, $translated_result[$i]);


        }

        return $final_results;
    }
}
