<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators;

use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomXml\CJ;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomXml\Weebly;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomXml\Zoosk;
use Mockery\CountValidator\Exception;

/**
 * Class XmlLib
 *
 * @package App\Libraries\Scraper\Brands\Translators
 */
class XmlLib extends BaseTranslator
{
    use CJ;
    use Zoosk;
    use Weebly;
    const XML_TAG_TYPE_OPEN  = 'open';
    const XML_TAG_TYPE_CLOSE = 'close';
    const COMMON_SELECTOR    = 'results';

    /**
     * translate xml string to array attributes
     * The selector is a single node name which is the one we use to find
     * the collection of all the nodes
     * WARNING: xml have two type container
     * 1) $selector will be 'results'
     * <results>
     *  <result id=10 amount=90.0 currency=usd>
     *  <result id=10 amount=90.0 currency=usd>
     * </results>
     * 2) $selector will be 'result'
     * <results>
     *   <result>
     *      <id>10</id>
     *      <amount>90.0</amount>
     *      <currency>usd</currency>
     *   </result>
     *   <result>
     *      <id>10</id>
     *      <amount>90.0</amount>
     *      <currency>usd</currency>
     *   </result>
     * </results>
     * DEFAULT $selector = results
     *
     * @param        $xml_string
     * @param null   $selector
     * @param string $column_name_first
     * @param string $column_name_second
     * @param array  $exclude_columns
     *
     * @return mixed
     * @throws \Exception
     */
    public function translate($xml_string, $selector = null, $column_name_first = "", $column_name_second = "",$exclude_columns = '')
    {
        // Set the most common used selector
        if (is_null($selector) || empty($selector)) {
            $selector = self::COMMON_SELECTOR;
        }

        $exclude_columns = explode(',', strtolower($exclude_columns));
        $this->getResultParsed($xml_string,$values,null);
        if ($this->dataTypeXml($selector, $values)) {
            $this->getResultKeys($selector, $values, $keys);
            $this->getResultArrays($values, $keys, $result, $exclude_columns);
        } else {
            $this->getTagsInContainer($selector, $values, $result);
        }

        // if token url Decode needed then 'token_url_decode' parameter should be set in scraper advanced properties
        // and its value should be the token's column name
        if (isset($this->scraper['properties']['token_url_decode'])) {
            $this->tokenUrlDecode($result, $this->scraper['properties']['token_url_decode']);
        }

        // when two column names given then it's a case for choosing token from two different columns
        if ($column_name_first && $column_name_second) {
            $this->selectTokenFromColumns($result, $column_name_first, $column_name_second);
        }

        return $result;
    }

    /**
     * parse xml to two arrays - values and index's
     *
     * @param      $xml
     * @param      $result
     * @param null $encoding
     */
    private function getResultParsed(&$xml, &$result, $encoding = null)
    {
        $parser = xml_parser_create($encoding);

        try {
            xml_parse_into_struct($parser, $xml, $result);
            xml_parser_free($parser);
        } catch (Exception $e) {
            die(json_encode($e->getMessage()));
        }
    }

    /**
     * check type xml
     *
     * @param $start_search
     * @param $values
     *
     * @return bool
     */
    private function dataTypeXml($start_search, $values)
    {
        $startRead = false;
        foreach ($values as $key => $val) {
            if (is_array($val)) {
                if ($val['tag'] == strtoupper($start_search) && $val['type'] == self::XML_TAG_TYPE_CLOSE) {
                    $startRead = false;
                }

                if ($startRead) {
                    return isset($val['attributes']);
                }

                if ($val['tag'] == strtoupper($start_search) && $val['type'] == self::XML_TAG_TYPE_OPEN) {
                    $startRead = true;
                }
            }
        }
    }

    /**
     * get keys in tag
     *
     * @param $start_search
     * @param $params
     * @param $keys
     */
    private function getResultKeys($start_search, $params, &$keys)
    {
        $keys      = [];
        $startRead = false;

        foreach ($params as $key => $val) {
            if (is_array($val)) {
                if ($val['tag'] == strtoupper($start_search) && $val['type'] == self::XML_TAG_TYPE_CLOSE) {
                    $startRead = false;
                }

                if ($startRead) {
                    $keys[] = $key;
                }

                if ($val['tag'] == strtoupper($start_search) && $val['type'] == self::XML_TAG_TYPE_OPEN) {
                    $startRead = true;
                }
            }
        }
    }

    /**
     * get result array
     *
     * @param       $params
     * @param       $keys
     * @param       $result
     * @param array $exclude_fields
     */
    private function getResultArrays(&$params, &$keys, &$result,$exclude_fields = [])
    {
        foreach ($keys as $key) {
            if (is_array($params[$key]) && !in_array(strtolower($params[$key]['tag']), $exclude_fields)) {
                $result[] = $params[$key]['attributes'];
            }
        }
    }

    /**
     *
     *
     * @param $start_search
     * @param $results
     * @param $params
     */
    private function getTagsInContainer($start_search, $results, &$params)
    {
        $params    = [];
        $startRead = false;

        $temp = [];
        foreach ($results as $key => $val) {
            if (is_array($val)) {
                if ($val['tag'] == strtoupper($start_search) && $val['type'] == self::XML_TAG_TYPE_CLOSE) {
                    $startRead = false;
                    $params[]  = $temp;
                    unset($temp);
                }

                if ($startRead && isset($val['tag'])) {
                    $temp[$val['tag']] = isset($val['value']) ? $val['value'] : null;
                }

                if ($val['tag'] == strtoupper($start_search) && $val['type'] == self::XML_TAG_TYPE_OPEN) {
                    $startRead = true;
                }
            }
        }
    }

    /**
     * Select Token From Columns
     * in the case when token can appear in two different columns checks for the token and creates
     * new column 'token'
     *
     * @param $result
     * @param $column_name_first
     * @param $column_name_second
     *
     * @throws \Exception
     */
    private function selectTokenFromColumns(&$result, $column_name_first, $column_name_second)
    {
        foreach ($result as &$result_item) {
            if (isset($result_item[$column_name_first])) {
                $result_item['token'] = rawurldecode($result_item[$column_name_first]);
            } else if (isset($result_item[$column_name_second])) {
                $result_item['token'] = rawurldecode($result_item[$column_name_second]);
            } else {
                throw new \Exception("Token generation column not found");
            }
        }
    }
}