<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait NetreferCustomer
{
    public function netreferCustomer($csv, $offset, $date_to_compare, $default_token = '')
    {
        $translated_result   = self::translate($csv, $offset);
        $event_date          = date('Y-m-d', strtotime('-' . $this->fetcher['properties']['days'] . ' days'));
        $processed_date      = new \DateTime();
        $date_to_compare_obj = new \DateTime($date_to_compare);

        foreach ($translated_result as &$translated_item) {
            $translated_item['Date'] = $event_date;
            if ($translated_item['CPA Processed Date']) {
                $processed_date->modify($translated_item['CPA Processed Date']);
                $translated_item['CPA Processed Date'] = $processed_date->format('Y-m-d');
            } else {
                $processed_date->modify('2016-01-01');
            }

            if ($translated_item['Marketing Source name'] == $default_token) {
                $translated_item['Marketing Source name'] = $translated_item['Customer Reference ID'];
            }

            $translated_item['SignupDateReformatted'] = date('Y-m-d', strtotime($translated_item['Signup Date']));

            if ($processed_date > $date_to_compare_obj) {
                $translated_item['pass_cond'] = "true";
            } else {
                $translated_item['pass_cond'] = "false";
            }
        }

        return $translated_result;
    }
}