<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait Karamba
{
    /**
     * karambaUK
     * translates karamba UK
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     *
     * @return mixed
     */
    public function karambaUK($html, $table_selector = null, $header_selector = 'th')
    {
        $translated_result = self::translate($html, $table_selector, $header_selector);

        return $this->karamba_validateTokens($translated_result);
    }

    /**
     * validateTokens
     * validate tokens: replaces %24 to $, if there is no token replace it with date
     *
     * @param $translated_result
     *
     * @return mixed
     */
    private function karamba_validateTokens($translated_result)
    {
        if (!is_array($translated_result)) {
            return $translated_result;
        }

        foreach ($translated_result as &$translated_item) {
            $translated_item['AR Number'] = str_replace('%24', '$', $translated_item['AR Number']);
            if ($translated_item['AR Number'] == '') {
                $translated_item['AR Number'] = $translated_item['meta_date'];
            }
        }

        return $translated_result;
    }

    /**
     * karambaDK
     * translates karamba DK
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     *
     * @return null
     */
    public function karambaDK($html, $table_selector = null, $header_selector = 'th')
    {
        $karamba_dk_caption          = 'Karamba.dk';
        $karamba_dk_caption_selector = '#ctl00_ucTrafficGridView0_lblCaption';

        $this->createCrawler($html);
        $caption = $this->filter($karamba_dk_caption_selector);

        // check if the table exists
        if ($caption->getNode(0)) {
            if (strtolower($caption->text()) == strtolower($karamba_dk_caption)) {
                $translated_result = self::translate($html, $table_selector, $header_selector);

                return $this->karamba_validateTokens($translated_result);
            }
        }

        return null;
    }

    /**
     * dansk777
     * translates Dansk777 (karamba DK brand)
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     *
     * @return null
     */
    public function dansk777($html, $table_selector = null, $header_selector = 'th')
    {
        $dansk_caption          = 'Dansk777';
        $dansk_caption_selector = '#ctl00_ucTrafficGridView3_lblCaption';

        $this->createCrawler($html);
        $caption = $this->filter($dansk_caption_selector);

        // check if the table exists
        if ($caption->getNode(0)) {
            if (strtolower($caption->text()) == strtolower($dansk_caption)) {
                $translated_result = self::translate($html, $table_selector, $header_selector);

                return $this->karamba_validateTokens($translated_result);
            }
        }

        return null;
    }
}