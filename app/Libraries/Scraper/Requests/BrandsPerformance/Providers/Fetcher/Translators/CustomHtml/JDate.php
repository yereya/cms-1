<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait JDate
{
    /**
     * jDate
     * jDate custom translator
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     *
     * @return mixed
     */
    public function jDate($html, $table_selector = null, $header_selector = 'th')
    {
        self::translate($html, $table_selector, $header_selector, $limit = 0);

        $this->keys = [
            '',
            'Date',
            'Commission',
            'Regs',
            ''
        ];

        // Removes the first element from the array (keys)
        // to be processed later by the Htmllib translator
        array_shift($this->values);

        $results = $this->keyValueToArray();

        return $results;
    }
}