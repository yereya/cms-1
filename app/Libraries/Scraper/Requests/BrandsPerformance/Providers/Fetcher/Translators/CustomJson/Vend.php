<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait Vend
{
    /**
     * @param $json
     * @param null $selector
     * @param int $offset
     * @param int $amount_of_headers
     * @param int $leads_col_key
     * @param null $sales_col_key
     * @param null $item_group_selector
     * @return array
     */
    public function Vend($json, $selector = null, $offset = 2, $amount_of_headers = 6, $leads_col_key = 3,
                         $sales_col_key = null, $item_group_selector = null)
    {
        // Translate the relevant sheet data
        $translated_sheet_data = self::translate($json, $selector, $item_group_selector);
        if(isset($translated_sheet_data[0][1])== false  or $translated_sheet_data[0][1] != 'Country')
            return [] ;
        $result = $this->eightXEight($json, $selector, $offset, $amount_of_headers, $leads_col_key, $sales_col_key, $item_group_selector);
        return $result;
    }
}