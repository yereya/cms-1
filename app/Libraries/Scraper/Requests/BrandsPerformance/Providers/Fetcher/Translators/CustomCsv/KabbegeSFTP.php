<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


use App\Libraries\TpLogger;

trait KabbegeSFTP
{

    public function KabbageSFTP($html,$input_date)
    {
        $tp_logger = TpLogger::getInstance();
        # Set path to storage/app/scraper/kabbage/
        $path_to_folder = storage_path('app') . DIRECTORY_SEPARATOR . 'scraper'.DIRECTORY_SEPARATOR.'kabbage'.DIRECTORY_SEPARATOR;
        $full_path = $path_to_folder . $this->generate_name($input_date); // generate file name with relevant date
        $tp_logger->info("Full path to file: " . $full_path);

        if(!file_exists($full_path)){
            # Run Shell Script
            $tp_logger->info("Kabbage before shell command");
            $tp_logger->info("Current getcwd : " . getcwd());
            chdir(DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'tools'.DIRECTORY_SEPARATOR.'scripts'.DIRECTORY_SEPARATOR);
            $tp_logger->info("After changing dir, getcwd : " . getcwd());
//            $output = shell_exec('/bin/bash get_sftp_report.sh --csv_date='.date("Ymd", strtotime($input_date)));
            $output = shell_exec('/bin/bash /data/tools/scripts/get_sftp_report.sh --csv_date=20200121');
            if(!file_exists($full_path)){
                $output = $output?$output:'';
                $tp_logger->info("Error ----- 1 ". $output);
            }
            $output = shell_exec('bash /data/tools/scripts/get_sftp_report.sh --csv_date=20200121');
            if(!file_exists($full_path)){
                $output = $output?$output:'';
                $tp_logger->info("Error ----- 2 ". $output);
            }
            $output = shell_exec('/data/tools/scripts/get_sftp_report.sh --csv_date=20200121 2>&1');
            if(!file_exists($full_path)){
                $output = $output?$output:'';
                $tp_logger->info("Error ----- 3 ". $output);
            }
            $output = shell_exec('/data2/web/cms.trafficpointltd.com/storage/app/scraper/kabbage/get_sftp_report.sh --csv_date=20200121 2>&1');
            if(!file_exists($full_path)){
                $output = $output?$output:'';
                $tp_logger->info("Error ----- 4 ". $output);
            }
            $output = shell_exec('/data2/web/cms.trafficpointltd.com/storage/app/scraper/kabbage/get_sftp_report.sh --csv_date=20200121');
            if(!file_exists($full_path)){
                $output = $output?$output:'';
                $tp_logger->info("Error ----- 5 ". $output);
            }
            $output = shell_exec('bash /data2/web/cms.trafficpointltd.com/storage/app/scraper/kabbage/get_sftp_report.sh --csv_date=20200121');
            if(!file_exists($full_path)){
                $output = $output?$output:'';
                $tp_logger->info("Error ----- 6 ". $output);
            }
            $output = shell_exec('/bin/bash /data2/web/cms.trafficpointltd.com/storage/app/scraper/kabbage/get_sftp_report.sh --csv_date=20200121');
            if(!file_exists($full_path)){
                $output = $output?$output:'';
                $tp_logger->info("Error ----- 7 ". $output);
            }
            $output = shell_exec('/bin/bash /data2/web/cms.trafficpointltd.com/storage/app/scraper/kabbage/get_sftp_report.sh --csv_date=20200121 2>&1');
            if(!file_exists($full_path)){
                $output = $output?$output:'';
                $tp_logger->info("Error ----- 8 ". $output);
            }
            sleep(5);
            if(!file_exists($full_path)){
                $tp_logger->info("Error - kabbage after shell command ------ error in bringing new csv file");
                $output2 = shell_exec('/bin/bash ' . DIRECTORY_SEPARATOR.'data'.DIRECTORY_SEPARATOR.'tools'.DIRECTORY_SEPARATOR.'scripts'.DIRECTORY_SEPARATOR.'get_sftp_report.sh --csv_date='.date("Ymd", strtotime($input_date)));
                if(!file_exists($full_path)){
                    if(!file_exists($full_path)){
                        $output3 = exec('/bin/bash /data/tools/scripts/get_sftp_report.sh --csv_date=20200121');
                        $output3 = shell_exec('/bin/bash /data/tools/scripts/get_sftp_report.sh --csv_date=20200121');
                        $tp_logger->info("Error 3 - kabbage after shell command ------ error in bringing new csv file");
                        throw new \Exception('File not exists - Error in bringing new csv file');
                    }
                }else {
                    $tp_logger->info("Success - kabbage after shell command ---- new file exists");
                }
            }else{
                $tp_logger->info("Success - kabbage after shell command ---- new file exists");
            }
        }
        # Translate Csv file
        $translated_data = array_map('str_getcsv', file($full_path));
        $headers = $translated_data[0];
        $data = [];
        foreach(array_slice($translated_data,1) as $row){
            $data[] = array_combine($headers, $row);
        }

        return $data;
    }

    private function generate_name($date){
        $name_base  = 'TrafficPoint_Funnel_Report_';
        $new_date = date("Ymd", strtotime($date));

        return $name_base.$new_date.'.csv';
    }

    

}