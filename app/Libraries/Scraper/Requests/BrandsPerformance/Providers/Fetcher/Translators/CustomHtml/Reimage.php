<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait Reimage
{
    /**
     * Reimage
     * translates reimage
     *
     * @param        $html
     * @param null   $table_selector
     * @param string $header_selector
     * @param string $mult
     *
     * @return mixed
     */
    public function reimage($html, $table_selector = null, $header_selector = 'th', $mult = '1.82')
    {
        $ioId_list = [
            'display_Goog_DE'    => '543154b02f239a6c27c7dd02',
            'search_Bing_US'     => '54cf8bafe487a6be7de09f90',
            'Search_Bing_UK'     => '54d1f1e4b97a1a3136eb06d5',
            'search_Goog_UK'     => '542d73713ef5cef67e59195c',
            'direct'             => '53fc9aa797ebe932681c97f1',
            'search_Goog_UK_DLL' => '54c8b642ce3a4d9c3e0a0e32',
            'search_Goog_US'     => '54c8eed9e469eac1451dabcc',
            'Display_Goog_EN'    => '54314b902f239a6c27c7dbf5',
            'search_Bing_DE'     => '5433a2b15eb4a1bc3f106bf3-',
            'search_Goog_DE'     => '54311e95631bf190258e8067',
            'search_Goog_JP'     => '55868f64c23638c65d5412a8-',
            'display_Goog_JP'    => '558aa7d738a474a52784eaa9',
            'display_Goog_FR'    => '543243515270a9b9331b437c',
            'Search_Goog_FR'     => '54cf86d554622a35787aa8bb',
        ];

        $translated_result = self::translate($html, $table_selector, $header_selector);

        $date = new \DateTime();
        foreach ($translated_result as &$translated_item) {
            if ($translated_item['Adgroup'] == 'direct') {
                $translated_item['token'] = $ioId_list[$translated_item['Campaign']];
            } else {
                $translated_item['token'] = $translated_item['Adgroup'];
            }

            // create timestamp - it is used in processor for trxid
            $date->setTimestamp(strtotime($translated_item['Transaction Date'] . '+0200'));
            $translated_item['timestamp'] = $date->getTimestamp();

            $translated_item['Commission amount'] = $translated_item['Amount'] * $mult;
            $translated_item['io_id']             = $ioId_list[$translated_item['Campaign']];
        }

        return $translated_result;
    }
}