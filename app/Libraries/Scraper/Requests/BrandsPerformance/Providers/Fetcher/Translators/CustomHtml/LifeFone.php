<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


trait LifeFone
{

    /**
     * @param        $html
     * @param string $table_wrapper
     * @param string $header_selector
     * @param int $offset
     * @param int $limit
     *
     * @return array
     */
    public function LifeFone($html, $table_wrapper = 'table', $header_selector = 'tr', $offset = 0,
                             $limit = 0)
    {
        $results = [];
        $assoc = true;
        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);

        $results_state = $table->getNode(0);

        if ($results_state) {
            $this->getLifeFoneKeys();

            $this->getLifeFoneValues($table, $offset);

            if ($limit != 0) {
                $this->values = array_slice($this->values, 0, $limit);
            }

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }

            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($results, $assoc);
            }

            # ---- Filter by the date value - check if the date column is a valid date, otherwise remove ----
            $res = array_filter($results,function($r){return strtotime($r['Created Date']);});
            return $res;
//            return $results;
        }

        $tp_logger = TpLogger::getInstance();
        $tp_logger->notice('There is no data available or provided table selector parameter is wrong: ' . $table_selector);

        return [];

    }

    /**
     * @param $target_crawler
     * @param $key_selector
     */
    private function getLifeFoneKeys(): void
    {

        $this->keys[0] = "Lead Source Type";
        $this->keys[1] = "Lead Status";
        $this->keys[2] = "Created Date";
        $this->keys[3] = "Won";
        $this->keys[4] = "Oppt Close Date";
        $this->keys[5] = "IP Address";
        $this->keys[6] = "Lead Source Description";
        $this->keys[7] = "SubID";
        $this->keys[8] = "Lead Number (Order Number)";

    }

    /**
     * @param $target
     * @param $offset
     */
    private function getLifeFoneValues($target, $offset = 0): void
    {
        $idx = 0;
        $target->filter('tr')->reduce(function ($tr) use (&$idx, $offset) {
            if ($idx >= $offset && $tr->attr('style') != 'display:none;') {
                $tr->filter('td')->reduce(function ($td, $i) use ($idx) {
                    $this->values[$idx][$i] = $this->removeNbsp($td->text());

                });
            }
            $idx++;
        });
    }

}
