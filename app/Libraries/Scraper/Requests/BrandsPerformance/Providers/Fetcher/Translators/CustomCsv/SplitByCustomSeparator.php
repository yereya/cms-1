<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


trait SplitByCustomSeparator
{
    /**
     * @param $csv
     * @param string $separator
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @return array
     */
    public function splitByCustomSeparator($csv, $separator = ";", $offset = 0, $limit = 0, $delimiter = null)
    {
        $translated_result = self::translate($csv, $offset, $limit, $delimiter);
        $headers = preg_split("/[$separator]/", array_keys($translated_result[0])[0]);
        $new_translated_result = [];
        foreach ($translated_result as $translated_item) {
            $row = preg_split("/[$separator]/", array_values($translated_item)[0]);
            $row = array_combine($headers, $row);
            $new_translated_result[] = $row;
        }
        return $new_translated_result;
    }
}


