<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait Adjust
{
    /**
     * @param $json
     * @param null $offset
     * @return mixed
     */
    public function Adjust($json, $chosen_col = null){
        $translated_result = self::translate($json);
        $result_tracker = $translated_result['result_parameters']['trackers'];
        $i = 0;
        $final_result = [];
        foreach ($result_tracker as $track){
            $kpi = array_combine($translated_result['result_parameters']['kpis'],$translated_result['result_set']['trackers'][$i]['kpi_values']);
            $i++;
            $track = array_merge($track,$kpi);

            if ($chosen_col != null) { // counter by column
                $counter_amount = $track[$chosen_col] ?? 1;
                if ($counter_amount > 1) {
                    $track[$chosen_col] = 1;
                    for ($j = 1; $j <= $counter_amount; $j++) {
                        $final_result[] = $track + ['counter_col' => $j];
                    }
                }
                else {
                    $final_result[] = $track + ['counter_col' => 0];
                }
            }
            else {
                $final_result[] = $track + ['counter_col' => 0];
            }
        }
        return $final_result;
    }
}