<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait Cove
{
    /**
     * function for cove script that work like translateWithGroupsSelector, but needs customization
     * Example for the json structure is:
     * {"columns":[{"column_type":"sub1","id":"nYiHQ8Wo07JF","label":"nYiHQ8Wo07JF"}],"reporting":{"imp":0,"total_click":0,"unique_click":0,"invalid_click":1,"duplicate_click":0,"ctr":0,"cv":0,"view_through_cv":0,"event":0,"cvr":0,"evr":0,"rpc":0,"rpm":0,"revenue":0,"redirect_traffic_revenue":0}},{"columns":[{"column_type":"sub1","id":"BJxFs1NSWv","label":"BJxFs1NSWv"}],"reporting":{"imp":0,"total_click":0,"unique_click":0,"invalid_click":1,"duplicate_click":0,"ctr":0,"cv":0,"view_through_cv":0,"event":0,"cvr":0,"evr":0,"rpc":0,"rpm":0,"revenue":0,"redirect_traffic_revenue":0}},{"columns":[{"column_type":"sub1","id":"Hy!DcQAB!P","label":"Hy!DcQAB!P"}]
     * @param $json
     * @param null $selector
     * @param null $item_group_selector
     * @return mixed
     */
    public function cove($json, $selector = null, $item_group_selector = null)
    {
        $this->jsonToArray($json);

        if (is_null($selector)) {
            return $json;
        }

        $this->parseSelector($selector);
        $result = [];
        foreach ($selector as $sel) {
            $this->parseJson($json, $sel, 0, $result);
        }

        //We have a nested structure like in documentation to flattenJson function
        foreach ((array) $result as $item) {
            if (isset($item['columns'][0])) {
                $flat_arr[] = $item['columns'][0];
            }
            if (isset($item['reporting'])) {
                $flat_arr[] = $item['reporting'];
            }

            $final_result[] = $flat_arr[0] + $flat_arr[1];
            $flat_arr = [];
        }

        return $final_result;

    }
}