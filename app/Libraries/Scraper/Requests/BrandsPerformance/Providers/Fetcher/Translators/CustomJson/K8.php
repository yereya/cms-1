<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait K8
{
    /**\
     * @param $json
     * @return array
     */
    public function getK8PlayerActivity($json)
    {

        $this->jsonToArray($json);

        /*Specific path to the records*/
        $records = $json['responseplayer']['tbody'];
        $results = [];

        foreach ($records as $key => $record) {
            $results[$key] = $record;
            $results[$key]['token'] = str_replace('2084266_60655-', '', $record['btag']);
        }

        return $results;
    }
}