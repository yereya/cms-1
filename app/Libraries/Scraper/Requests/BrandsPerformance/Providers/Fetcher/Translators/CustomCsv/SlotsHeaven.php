<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

/**
 * Process the results and add a counter column if no token exists
 * Specific for slotsheaven brand
 *
 * Trait SlotsHeaven
 * @package App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv
 */
trait SlotsHeaven
{
    /**
     * @param $csv
     * @param $offset
     * @param string $token_col
     * @param string $sales_col
     * @param string $leads_col
     * @return mixed
     */
    public function slotsHeaven($csv, $offset, $token_col = 'Payload', $sales_col = 'Real FTD Count', $leads_col = 'Real Clicks')
    {
        $translated_result   = self::translate($csv, $offset);
        $final_result = [];

        foreach ($translated_result as $translated_row) {
            // The case where no token exists - create a unique counter by date and lead/sale count
            if (empty($translated_row[$token_col])) {
                $_date = $translated_row['Date'];

                // Adds the sale results to the counter column
                $sales_count = $translated_row[$sales_col];
                for ($i = 0; $i < $sales_count; $i++) {
                    // Creates a mutually exclusive leads/sales values so we will not have duplicate rows
                    $altered_attrs = ['trx_col' => $_date . '-' . $i, $sales_col => 1, $leads_col => 0];

                    $final_result[] = array_merge($translated_row, $altered_attrs);
                }

                // Adds the lead results to the counter column
                $leads_count = $translated_row[$leads_col];
                for ($i = 0; $i < $leads_count; $i++) {
                    // Creates a mutually exclusive leads/sales values so we will not have duplicate rows
                    $altered_attrs = ['trx_col' => $_date . '-' . $i, $leads_col => 1, $sales_col => 0];

                    $final_result[] = array_merge($translated_row, $altered_attrs);
                }

                continue;
            }
            $final_result[] = $translated_row + ['trx_col' => $translated_row[$token_col]];
        }

        return $final_result;
    }
}