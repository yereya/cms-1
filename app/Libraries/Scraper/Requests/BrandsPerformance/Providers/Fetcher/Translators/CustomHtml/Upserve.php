<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;



trait Upserve
{

    /**
     * @param  $html
     *
     * @return array
     */

    public function Upserve($html,$needle_start = 'https://app-sj01',$needle_end = '=en_US"'){
        # ---- Extract download link from html -------
        $link = substr($html,strpos($html,$needle_start) ,strpos($html,$needle_end) - strpos($html,$needle_start) + strlen($needle_end));

        # ---- Send GET request to get the data -------

        $csv_string = file_get_contents($link);

        # ---- Parse and Store the data -------
        $data = str_getcsv($csv_string, "\n");
        $rows = [];
        foreach($data as &$row) {
            $rows[] = str_getcsv($row);
        }
        $result = [];
        $headers = $rows[0];

        # --- Merge splitted rows ---
        $rows_updated_arr = []; // Store the updated rows
        $new_row = []; // Merge all the spltited parts into this var
        foreach ($rows as $idx => $row){
            if(sizeof($row) < sizeof($headers)){
                if(empty($new_row)){// First part of the splitted row
                    $new_row = $row;
                }
                else {
                    $new_row[sizeof($new_row) - 1] = $new_row[sizeof($new_row) - 1] ." ". $row[0]; // merge last row's value of the prev row with the first of the new row
                    $new_row = array_merge($new_row,array_slice($row,1));
                    if(sizeof($new_row) == sizeof($headers)) {
                        $rows_updated_arr[] = $new_row;
                        $new_row = [];
                    }
                }
            }
            else {
                $rows_updated_arr[] = $row;
            }
        }

        # --- Combine headers with rows ---
        foreach ($rows_updated_arr as $idx => $row){
            if ($row != "" and $idx > 0){  // Ignore first(headers) and empty rows
                $result[] = array_combine($headers,$row);
            }
        }
        return $result;
    }
}