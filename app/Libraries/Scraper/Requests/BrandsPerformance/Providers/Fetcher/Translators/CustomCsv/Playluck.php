<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


trait Playluck
{
    /**
     * @param $csv
     * @param int $offset
     * @param int $limit
     * @param int $headers_columns_number
     * @param null $delimiter
     */
    public function playluck($csv, $offset = 1, $headers_columns_number = 14)
    {
        $csv = str_replace("\0","",$csv); // delete hidden signs
        $appended_results = [];
        $data = str_getcsv($csv, "\t");
        $headers = array_slice($data,$offset,$headers_columns_number);
        $data = array_slice($data,$headers_columns_number + 1,-$headers_columns_number);
        $rows = array_chunk($data,$headers_columns_number);
        foreach ($rows as $row) {
            $new_row = array_combine($headers,$row);
            $appended_results[] = $new_row;
        }
        return $appended_results;
    }
}