<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators;

use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Avangate;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Amone;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\BCasino;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Betsafe;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\BetStar;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Bitdefender;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\EliteSingles;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Foxy;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\FrontPoint;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\IncomeAccess;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Jimdo;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\BlueVine;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\MobileHelp;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\SplitBySeparator;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Ladbrokes;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\LuckyJar;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Lull;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\MacKeeper;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Monevo;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\NetreferCustomer;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Rsvp;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\SlotsHeaven;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\SunBets;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\TotalAv;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Tradedoubler;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\VcitaCSV;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\VideoSlots;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\KabbegeSFTP;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers\GeneralHelper;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers\InvocaHelper;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml\SendinBlue;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\SplitByCustomSeparator;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\LuckyJarNew;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\MondayMailCsv;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv\Playluck;


use League\Csv\Reader;

/**
 * Class BrandCsv
 *
 * @package App\Libraries\Scraper\Brands\Translators
 */
class CsvLib extends BaseTranslator
{
    use Foxy;
    use SunBets;
    use Betsafe;
    use BetStar;
    use TotalAv;
    use Avangate;
    use LuckyJar;
    use MacKeeper;
    use Ladbrokes;
    use FrontPoint;
    use VideoSlots;
    use SlotsHeaven;
    use Bitdefender;
    use EliteSingles;
    use IncomeAccess;
    use Tradedoubler;
    use GeneralHelper;
    use InvocaHelper;
    use NetreferCustomer;
    use Lull;
    use Rsvp;
    use SendinBlue;
    use Jimdo;
    use Monevo;
    use BCasino;
    use SplitBySeparator;
    use KabbegeSFTP;
    use BlueVine;
    use VcitaCSV;
    use Amone;
    use SplitByCustomSeparator;
    use LuckyJarNew;
    use MondayMailCsv;
    use Playluck;
    use MobileHelp;

    /**
     * @var Reader $csv
     */
    public $csv;

    /**
     * @param        $csv_string
     * @param int    $offset
     * @param int    $limit
     * @param string $delimiter
     *
     * @return mixed
     */
    public function translate($csv_string, $offset = 0, $limit = 0, $delimiter = null)
    {
        //TODO find a better way to do the delimiters
        if (!is_null($delimiter)) {
            $csv_string = str_replace($delimiter, ',', $csv_string);
        }

        $translated_csv = $this->parseCsv($csv_string, $offset, $limit);

        if ((int) $this->fetcher['properties']['concat_n_days_results'] > 0) {
            $this->buildDateColumn($translated_csv);
        }

        // if token url Decode needed then 'token_url_decode' parameter should be set in scraper advanced properties
        // and its value should be the token's column name
        if (isset($this->scraper['properties']['token_url_decode'])) {
            $this->tokenUrlDecode($translated_csv, $this->scraper['properties']['token_url_decode']);
        }

        // if 'maximum_past_days_range' and 'maximum_past_days_range_col_name'
        // are set in scraper advanced section
        if (isset($this->scraper['properties']['maximum_past_days_range']) && isset($this->scraper['properties']['maximum_past_days_range_col_name'])) {
            $this->buildIsInRangeColumn($translated_csv);
        }

        return $translated_csv;
    }

    /**
     * Replaces abnormal signs with its equivalent normalized form character
     *
     * @param $csv_string
     */
    private function removeAbnormalSigns(&$csv_string)
    {
        $csv_string = str_replace('%24', '$', $csv_string);
        $csv_string = str_replace('&nsbp', ' ', $csv_string);

        // Replaces a comma seperated string enclosed in double quotation marks
        // Example: '0,0,"31,310",32,512' will translate to '0,0,31310,32,512'
        $csv_string = preg_replace('/"([^",]+),*([^",]+)"/', '$1$2', $csv_string);
        $csv_string = preg_replace('/[^\w\s,-:;!#&\/\.\{}$]/', '', utf8_encode($csv_string));
    }

    /**
     * Parse Csv
     *
     * @param string $csv_string
     * @param integer $offset
     *
     * @param $limit
     * @return mixed
     */
    private function parseCsv($csv_string, $offset, $limit)
    {
        $this->removeAbnormalSigns($csv_string);
        $this->csv = Reader::createFromString($csv_string);
        $fields    = $this->getFields($offset);

        if ($fields) {
            return $this->fetchAllAssoc($fields, $offset, $limit);
        } else {
            return [];
        }
    }

    /**
     * @param $offset
     *
     * @return array
     */
    private function getFields($offset)
    {
        $_fields      = array_flatten($this->csv->fetchOne($offset));
        $count_values = array_count_values($_fields);

        foreach ($_fields as $key => $item) {
            if (!$item OR $count_values[$item] != 1) {
                $_fields[$key] = "col" . $key;
            }
        }

        return $_fields;
    }

    /**
     * @param array   $fields
     * @param integer $offset
     * @param integer $limit
     *
     * @return array
     */
    protected function fetchAllAssoc($fields, $offset, $limit)
    {
        $assoc_items = [];
        $this->csv->setOffset($offset + 1); // Skip the title and header
        $rows_num = $this->count() + $limit;
        $this->csv->setOffset($offset + 1); // Skip the title and header

        foreach ($this->csv->fetchAssoc($fields) as $items) {
            if (--$rows_num < 0) {
                break;
            }
            $assoc_items[] = $items;
        }

        return $assoc_items;
    }

    private function count()
    {
        $rows_num = $this->csv->each(function () use (&$count) {
            return true;
        });

        return $rows_num;
    }

    /**
     * Build Date Column
     * adds date column for reports that requested day by day
     *
     * @param $translated_csv
     */
    private function buildDateColumn(&$translated_csv)
    {
        $event_date = date('Y-m-d', strtotime('-' . $this->fetcher['properties']['days'] . ' days'));

        foreach ($translated_csv as &$translated_item) {
            $translated_item['Date'] = $event_date;
        }
    }

    /**
     * Build IsInRange Column
     * adds 'is_in_range' column if  'maximum_past_days_range' and
     * 'maximum_past_days_range_col_name' parameters supplied in scraper advanced settings
     *
     * @param $translated_csv
     */
    private function buildIsInRangeColumn(&$translated_csv)
    {
        $date_to_compare     = date('Y-m-d', strtotime('-' . $this->scraper['properties']['maximum_past_days_range'] . ' days'));
        $date_to_compare_obj = new \DateTime($date_to_compare);

        $col_name = $this->scraper['properties']['maximum_past_days_range_col_name'];
        foreach ($translated_csv as &$translated_item) {

            $processed_date     = date('Y-m-d', strtotime($translated_item[$col_name]));
            $processed_date_obj = new \DateTime($processed_date);

            $translated_item['_is_in_range'] = ($processed_date_obj > $date_to_compare_obj) ? 'true' : 'false';
        }
    }

    /**
     * @param $csv
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @param $date_name
     * @param $token_name
     * @return array|mixed
     * Translate csv, If there is no token -> take date + counter
     */
    public function translateWithCounterByDates($csv, $date_name, $token_name, $offset = 0, $limit = 0, $delimiter = null)
    {

        $translated = self::translate($csv, $offset, $limit, $delimiter);
        $final_results = [];
        $seen_dates = [];

        // If the date column name is not correct-> return the translated data like he is.
        if (!isset($translated[0][$date_name])) {
            return $translated;
        }

        // Appends every seen date incremented
        foreach ($translated as $result) {
            $_date = $result[$date_name];
            // If the token is not empty-> then the counter_col will remain empty
            if (!empty($result[$token_name])) {
                $final_results[] = $result + ['counter_col' => ''];
                continue;
            } // If the token is empty-> then the counter_col will add date + counter
            else if (!isset($seen_dates[$_date])) {
                $seen_dates[$_date] = -1;
            }
            $seen_dates[$_date]++;
            $final_results[] = $result + ['counter_col' => $_date . '-' . $seen_dates[$_date]];
        }
        
        return $final_results;
    }

    /**
     * @param $csv
     * @param $start_date
     * @param $end_date
     * @param $date_col_name
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @return array
     * Function that translates data and returns only the results that are in the dates rage
     */
    public function translateByDatesRange($csv, $start_date, $end_date, $date_col_name, $offset = 0, $limit = 0, $delimiter = null)
    {
        $translated = self::translate($csv, $offset, $limit, $delimiter);
        $final_results = [];

        // add only the result that are in the range
        foreach ($translated as $result) {
            if (self::check_in_range($start_date, $end_date, $result[$date_col_name]))
                $final_results[] = $result;
        }
        return $final_results;
    }

    /**
     * @param $start_date
     * @param $end_date
     * @param $date_from_user
     * @return bool
     * Check if a date is in a given range
     */
    function check_in_range($start_date, $end_date, $date_from_user)
    {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
    }

    /**
     * @param        $csv_string
     * @param $search_sign
     * @param $amount_column_str
     * @param int $offset
     * @param int $limit
     * @param string $delimiter
     *
     * @return mixed
     * remove signs and covert amount from string to integer
     */
    public function translate_with_convert_amount($csv_string, $search_sign, $amount_column_str, $offset = 0, $limit = 0, $delimiter = null)
    {
        $translated = self::translate($csv_string, $offset, $limit, $delimiter);

        // remove signs from amount and convert string to integer
        foreach ($translated as &$result) {
            $result[$amount_column_str] = intval(str_replace($search_sign, '', $result[$amount_column_str]));
        }
        return $translated;
    }

}