<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

trait Zanox
{
    /**
     * Zanox
     * translates zanox brand
     *
     * @param      $json
     * @param null $selector
     *
     * @return mixed
     */
    public function zanox($json, $selector = null)
    {
        $ioid_list = [
            'KissNoFrog'       => '575edb8a5399e44a45cad50c',
            'FriendScout24 DE' => '5741b762b64fa0fe254f9445',
            'Lovescout24 DE'   => '5741b762b64fa0fe254f9445',
            'ElitePartner.de'  => '5741b73fb64fa0fe254f9443',
            'PARSHIP.de'       => '5741b733b64fa0fe254f9442',
            'Neu.de'           => '5741b770b64fa0fe254f9446',
            'Trendmicro DE'    => '577a4799fe5cbbf37530b3fe',
        ];

        $translated_result = self::translate($json, $selector);

        foreach ($translated_result as &$translated_item) {
            $translated_item['gpps']             = $translated_item['gpps']['gpp']['$'];
            $translated_item['program']          = $translated_item['program']['$'];
            $translated_item['adspace']          = $translated_item['adspace']['$'];
            $translated_item['admedium']         = $translated_item['admedium']['$'];
            $translated_item['trackingCategory'] = isset($translated_item['trackingCategory']['$']) ? $translated_item['trackingCategory']['$'] : '';
            $translated_item['io_id']            = isset($ioid_list[$translated_item['program']]) ? $ioid_list[$translated_item['program']] : '';
        }

        return $translated_result;
    }
}