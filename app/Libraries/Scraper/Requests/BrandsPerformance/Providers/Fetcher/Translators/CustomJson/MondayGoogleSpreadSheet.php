<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHelpers\GeneralHelper;
use DB;
use Exception;

trait MondayGoogleSpreadSheet
{
    /**
     * @param     $json
     * @param     $date_offset
     * @param     $selector
     * @param     $item_group_selector
     * @return array
     */
    public function MondayGoogleSpreadSheet($json, $date_offset="-1 week", $selector = null, $item_group_selector = null )
    {
        $brand_id = 2013 ; // Monday's brand id

        $translated_result = self::translate($json, $selector, $item_group_selector);
        if (!$translated_result)
            return [];

        $headers = $translated_result['values'][0];
        $results = $translated_result['values'];

        # Combine headers with values
        $results = array_map(function($row) use ($headers){
            return array_combine($headers,$row);
        },array_slice($results,1));

        # Translate the date_offset to date format, should be of strtotime format
        if(!strtotime($date_offset)){ // Wrong format
            throw new Exception(" Error in the format of the date offset variable, use strtotime format - MondayMail.php trait.");
        }
        $date = date('Y-m-d',strtotime($date_offset));

        # Date in case the user entered wrong / too far date
        $date_for_safety = date('Y-m-d',strtotime("-1 month"));

        # Get data from db
        $track_upper_case = DB::connection('dwh')->table('dwh_fact_tracks')
            ->select('id')
            ->where('brand_id', $brand_id)
            ->whereDate('updated_at', '>',$date)
            ->whereDate('updated_at', '>',$date_for_safety)->get()->toArray();

        # StdCalls to Array
        $track_upper_case = array_map(function($row){
            $r = json_decode(json_encode($row), true);
            return $r;
        },$track_upper_case);

        $track_lower_case = array_map('strtolower',array_column($track_upper_case, 'id'));

        # Look for the lower case values and build a new result
        $output = [];
        $dict = [];
        foreach ($results as $key => $row) {
                $token = $row['Token'];
                $row['Token_original'] = $token;
                if (!empty($token)) {
                    $index = array_search($token, $track_lower_case);
                    if ($index) {
                        $row['Token'] = $track_upper_case[$index]['id'];
                    }
                } else {
                    $row['Token'] = '';
                }
                # Check if the updated token already exists in the dictionary and update the counter
                $token = $row['Token'];
                if (isset($dict[$token])) {
                    $dict[$token] = $dict[$token] + 1;
                } else {
                    $dict[$token] = 1;
                }
                $row['counter'] = $dict[$token];
                $output[] = $row;
        }
        return $output;
    }
}