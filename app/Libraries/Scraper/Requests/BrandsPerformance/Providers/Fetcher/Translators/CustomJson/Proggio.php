<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait Proggio
{
    /**
     * @param $json
     * @param null $selector
     * @param int $header_offset
     * @param null $item_group_selector
     * @return array
     */
    public function Proggio($json, $selector = null, $header_offset = 0, $item_group_selector = null)
    {
        $json = $this->translate($json, $selector, $item_group_selector);

        $headers = $json[$header_offset];
        $results = [];

        collect($json)
            ->filter(function ($record, $idx) use ($header_offset) {
                return $idx > $header_offset;
            })
            ->values()
            ->each(function ($row) use ($headers, &$results) {
                $temp = [];
                collect($headers)->each(function ($header, $idx) use ($row, &$results, &$temp) {
                    $temp[$header] = $row[$idx] ?? '';
                });
                $results[] = $temp;
            });

        return $results;
    }
}