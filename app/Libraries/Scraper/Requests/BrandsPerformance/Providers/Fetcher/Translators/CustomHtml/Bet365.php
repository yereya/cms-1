<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


trait Bet365 {
    /**
     * Generate bet365 sales and leads according to given column names
     * @param $html
     * @param $table_selector
     * @param string $header_selector
     * @param int $limit
     * @param $selected_cols_str
     * @param string $lead_column
     * @return array
     */

    public function bet365($html, $table_selector = null, $header_selector = 'th', $limit = 0, $selected_cols_str = null, $lead_column = 'NewSignups')
    {
        $translated_result = self::translate($html, $table_selector, $header_selector, $limit);
        $selected_col_arr = explode(';', $selected_cols_str);
        $result_set = [];

        if ($translated_result == null){
            return $result_set;
        }

        foreach ($translated_result as $translated_item) {
            $overall_deposits = $this->sumColsValues($translated_item, $selected_col_arr);
            $overall_leads    = $translated_item[$lead_column];

            if ($overall_deposits) {
                $_event = [];
                $_event['date'] = $translated_item['Date'];

                for ($i = 1; $i <= $overall_deposits; $i++) {
                    $_event['event'] = 'sale';
                    $_event['trx_id'] = 'bet365-sale-' . $translated_item['Date'] . '-' . $i;
                    $result_set[] = $_event;
                }
            }

            if ($overall_leads) {
                $_event = [];
                $_event['date'] = $translated_item['Date'];

                for ($i = 1; $i <= $overall_leads; $i++) {
                    $_event['event'] = 'lead';
                    $_event['trx_id'] = 'bet365-lead-' . $translated_item['Date'] . '-' . $i;
                    $result_set[] = $_event;
                }
            }
        }

        return $result_set;
    }

    /**
     * Given a row and a few column names, sum all of the column's values
     * @param $row
     * @param $columns
     * @return int
     */
    private function sumColsValues($row, $columns)
    {
        $sum = 0;
        foreach ($columns as $column) {
            $sum += $row[$column] ?? 0;
        }

        return $sum;
    }

    /**
     * Generate bet365 sales and leads according to given column names
     * @param $html
     * @param $table_selector
     * @param string $header_selector
     * @param int $limit
     * @param $selected_cols_str
     * @param string $lead_column
     * @param null $date
     * @return array
     */

    public function bet365_new($html, $table_selector = null, $header_selector = 'th', $limit = 0, $selected_cols_str = null, $lead_column = 'NewSignups', $date = null)
    {
        $translated_result = self::translate($html, $table_selector, $header_selector, $limit);
        $selected_col_arr = explode(';', $selected_cols_str);
        $result_set = [];

        if ($translated_result == null){
            return $result_set;
        }

        foreach ($translated_result as $translated_item) {
            $overall_deposits = $this->sumColsValues($translated_item, $selected_col_arr);
            $overall_leads    = $translated_item[$lead_column];

            if ($overall_deposits) {
                $_event = [];
                $_event['Link Name'] = $translated_item['Link Name'];
                if($date != null){
                    $_event['Date'] = $date;
                }

                for ($i = 1; $i <= $overall_deposits; $i++) {
                    $_event['event'] = 'sale';
                    $_event['counter'] = $i;
                    $result_set[] = $_event;
                }
            }

            if ($overall_leads) {
                $_event = [];
                $_event['Link Name'] = $translated_item['Link Name'];
                if($date != null){
                    $_event['Date'] = $date;
                }

                for ($i = 1; $i <= $overall_leads; $i++) {
                    $_event['event'] = 'lead';
                    $_event['counter'] = $i;
                    $result_set[] = $_event;
                }
            }
        }

        return $result_set;
    }

}