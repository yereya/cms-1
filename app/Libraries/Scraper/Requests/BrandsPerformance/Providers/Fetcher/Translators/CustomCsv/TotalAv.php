<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

trait TotalAv
{
    public function totalAv($csv, $offset = 0, $limit = 0, $delimiter = null)
    {
        $translated_result = self::translate($csv, $offset, $limit, $delimiter);

        $new_translated_result = [];
        foreach ($translated_result as $translated_item) {

            $new_item = $translated_item;

            // if the number of acquisitions is greater than 1, generate a new token for each acquisition
            if (intval($new_item['Acquisitions.Acquisitions']) > 1) {
                $sales_amount = $new_item['Acquisitions.Acquisitions'];

                for ($i = 1; $i <= $sales_amount; $i++) {
                    $new_item['Token']       = $new_item['Sid1.Sid1'] . '-' . $i;
                    $new_translated_result[] = $new_item;
                }
            } else {
                $new_item['Token']       = $new_item['Sid1.Sid1'];
                $new_translated_result[] = $new_item;
            }
        }

        return $new_translated_result;
    }
}