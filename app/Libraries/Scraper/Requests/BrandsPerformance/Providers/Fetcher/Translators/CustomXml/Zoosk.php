<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomXml;

trait Zoosk
{
    /**
     * Zoosk
     * translates Zoosk dating brand
     *
     * @param $xml
     * @param $offset
     *
     * @return mixed
     */
    public function zoosk($xml, $offset)
    {
        $translated_result = self::translate($xml, $offset);

        $io_ids = [
            '342' => '56f40df30c669ec20bd78884',
            '354' => '56f409d30c669ec20bd78697',
            '509' => '56d30aacced9518e67f021da',
            // UK campaigns
            '6'   => '5577fe8bf2e3eb924958d2df',
            '534' => '5577fe8bf2e3eb924958d2df',
            '536' => '5577fe8bf2e3eb924958d2df',
            '537' => '5577fe8bf2e3eb924958d2df',
            // US campaigns
            '208' => '56d30aacced9518e67f021da',
            '209' => '56d30aacced9518e67f021da',
            '287' => '56d30aacced9518e67f021da',
            '288' => '56d30aacced9518e67f021da',
            '496' => '56d30aacced9518e67f021da',
            '535' => '56d30aacced9518e67f021da',

            'default' => '56f40df30c669ec20bd78884'
        ];

        foreach ($translated_result as &$translated_item) {
            if (isset($io_ids[$translated_item['ID']])) {
                $translated_item['io_id'] = $io_ids[$translated_item['ID']];
            } else {
                $translated_item['io_id'] = $io_ids['default'];
            }
        }

        return $translated_result;
    }
}