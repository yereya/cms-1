<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;


trait Lull
{
    /**
     * @param $json
     * @param string $date_offset
     * @param int $brand_id
     * @return array
     * @throws \Exception
     */
    public function Lull($json,$date_offset='-4 weeks',$brand_id=2958)
    {
        $translated_result = self::translate($json);
        $results = [];
        foreach($translated_result as $key => $value){
            foreach ($value as $item){
                $results[] = array($key => $item);
            }
        }
        $new = array_chunk($results,count($translated_result['date']));

// ----------------------------------------------------------------------------------
        # Translate the date_offset to date format, should be of strtotime format
        if (!strtotime($date_offset)) { // Wrong format
            throw new \Exception(" Error in the format of the date offset variable, use strtotime format - freshshalesTokenProcess.php trait.");
        }
        $date = date('Y-m-d', strtotime($date_offset));

        # Date in case the user entered wrong / too far date
        $date_for_safety = date('Y-m-d', strtotime("-3 month"));

        # Get data from db
        $track_upper_case = \DB::connection('dwh')->table('dwh_fact_tracks')
            ->select('id','source_track')
            ->where('brand_id', $brand_id)
            ->where('event', 'click')
            ->whereDate('updated_at', '>', $date)
            ->whereDate('updated_at', '>', $date_for_safety)->get()->toArray();

        # StdCalls to Array
        $track_upper_case = array_map(function ($row) {
            $r = json_decode(json_encode($row), true);
            return $r;
        }, $track_upper_case);

        $track_lower_case = array_map('strtolower', array_column($track_upper_case, 'id'));

// ----------------------------------------------------------------------------------

        $final_result = [];
        for($i = 0; $i < count($translated_result['date']); $i++){
            $row = [];
            for($j = 0; $j < count($new); $j++){
                $row = array_merge($row, $new[$j][$i]);
                if(key($new[$j][$i]) == 'sca'){
                    $index = array_search($new[$j][$i]['sca'], $track_lower_case,true);
                    if ($index) {
                        $row['new_token'] = $track_upper_case[$index]['id'];
                    }else{
                        $row['new_token'] = '';
                    }
                }
            }
            $final_result[] = $row;
        }

        return $final_result;
    }
}