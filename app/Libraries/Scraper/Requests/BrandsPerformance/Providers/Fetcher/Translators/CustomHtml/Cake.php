<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


trait Cake
{
    /**
     * @param $html
     * @param string $table_wrapper
     * @param int $offset
     * @return array
     * Function for Cake script, translate html body mail
     */
    public function Cake($html, $table_wrapper = 'table', $offset = 0)
    {
        $results = [];
        $assoc = true;
        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);

        $results_state = $table->getNode(1);

        if ($results_state) {
            $this->getCakeKeys();
            $this->getCakeValues($table, $offset);

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }
            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($results, $assoc);
            }

            # ---- Filter by the date value - check if the date column is a valid date, otherwise remove ----
            $res = array_filter($results,function($r){return strtotime($r['Created Date']);});
            return $res;
        }

        return [];

    }

    /**
     * @param $target_crawler
     * @param $key_selector
     */
    private function getCakeKeys()
    {
        $this->keys[0] = "Lead Status";
        $this->keys[1] = "Created Date";
        $this->keys[2] = "QSO ApprovedSum";
        $this->keys[3] = "QSO Approved Date";
        $this->keys[4] = "Oppt Close Date";
        $this->keys[5] = "UTM - Source";
        $this->keys[6] = "UTM - Medium";
        $this->keys[7] = "UTM - Campaign";
        $this->keys[8] = "UTM - Term";
        $this->keys[9] = "UTM - Content";
        $this->keys[10] = "Converted Date";
        $this->keys[11] = "Bogus Reason";
        $this->keys[12] = "DQ Notes";
        $this->keys[13] = "DQ Reason";
        $this->keys[14] = "Lead Description";
    }

    /**
     * @param $target
     * @param $offset
     */
    private function getCakeValues($target, $offset = 0)
    {
        $idx = 0;
        $target->filter('tr')->reduce(function ($tr) use (&$idx, $offset) {
            if ($idx >= $offset && $tr->attr('style') != 'display:none;') {
                $tr->filter('td')->reduce(function ($td, $i) use ($idx) {
                    $this->values[$idx][$i] = $this->removeNbsp($td->text());
                });
            }
            $idx++;
        });

        # Cut the irrelevant lines
        $this->values = array_slice($this->values, 3, -2);
        $this->values[0] = array_slice($this->values[0], 1);

    }
}



