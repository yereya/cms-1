<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


trait LullTranslator
{
    /**
     * @param        $html
     * @param string $table_wrapper
     * @param string $header_selector
     * @param int    $offset
     * @param int    $limit
     *
     * @return array
     */
    public function LullTranslator($html, $table_wrapper = 'table', $header_selector = 'tr', $offset = 0,
                              $limit = 0)
    {
        $results = [];
        $assoc   = true;
        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);

        $results_state = $table->getNode(0);

        if ($results_state) {
            $this->getLullKeys();

            $this->getLullValues($table, $offset);


            if ($limit != 0) {
                $this->values = array_slice($this->values, 0, $limit);
            }

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc   = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }

            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($results, $assoc);
            }

            return $results;
        }

        $tp_logger = TpLogger::getInstance();
        $tp_logger->notice('There is no data available or provided table selector parameter is wrong: ' . $table_selector);

        return [];

    }


    /**
     * @param $target_crawler
     * @param $key_selector
     */
    private function getLullKeys(): void
    {

        $this->keys[0] = "Date";
        $this->keys[1] = "IP";
        $this->keys[2] = "Id";
        $this->keys[3] = "Trx";
        $this->keys[4] = "Empty1";
        $this->keys[5] = "Empty2";
        $this->keys[6] = "Empty3";
        $this->keys[7] = "Empty4";
        $this->keys[8] = "Empty5";
        $this->keys[9] = "Amount";
        $this->keys[10] = "Accepted";
        $this->keys[11] = "Conversion tracked";
        $this->keys[12] = "Num1";
        $this->keys[13] = "Num2";
        $this->keys[14] = "Num3";

    }

    /**
     * @param $target
     * @param $offset
     */
    private function getLullValues($target, $offset = 0): void
    {
        $idx = 0;
        $target->filter('tr')->reduce(function ($tr) use (&$idx, $offset) {
            if ($idx >= $offset && $tr->attr('style') != 'display:none;') {
                $tr->filter('td')->reduce(function ($td, $i) use ($idx) {
                    $this->values[$idx][$i] = $this->removeNbsp($td->text());

                });
            }
            $idx++;
        });
    }

}