<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

use DateTime;

trait Tradedoubler
{
    /**
     * Tradedoubler
     * Custom translator for Tradedoubler
     *
     * @param      $csv
     * @param      $offset
     * @param int  $limit
     * @param null $delimiter
     *
     * @return
     */
    public function tradedoubler($csv, $offset, $limit = 0, $delimiter = null)
    {
        $translated_result = self::translate($csv, $offset, $limit, $delimiter);
        foreach ($translated_result as &$translated_item) {

            // remove the unneeded last part of the date and reformat it to the European format
            $date = DateTime::createFromFormat('d/m/y', strtok($translated_item['Time stamp'], ' '));

            $translated_item['Reformatted Date'] = $date->format('Y-m-d');
        }

        return $translated_result;
    }
}