<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


trait Lull
{
    /**
     * @param $csv
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @return array
     */
    public function Lull($csv, $offset = 0, $limit = 0, $delimiter = null)
    {
        $translated = self::translate($csv, $offset, $limit, $delimiter);

        return collect($translated)->transform(function ($record) {

            return [
                'Date' => $record['date'],
                'Aff ID' => $record['sca'],
                'Source' => $record['scn'],
                'Clicks' => $record['clicks'],
                'Sales' => $record['sales'],
                'Lead Rate' => $record['lead_rate'],
                'Conversion Rate' => $record['conversion_rate'],
                'Commission' => $record['payout_sum'],
            ];
        })->toArray();
    }
}