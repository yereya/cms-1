<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


trait LuckyJarNew
{
    /**
     * @param $csv_string
     * @param int $offset
     * @param int $limit
     * @param null $delimiter
     * @param null $REGISTRATION_col
     * @param null $FTD_col
     * @return array
     */
    public function LuckyJarNew($csv_string, $offset = 0, $limit = 0, $delimiter = null, $REGISTRATION_col = null, $FTD_col = null)
    {
        $translated_csv = $this->translate($csv_string, $offset, $limit, $delimiter);
        $final_result = [];

        if ($REGISTRATION_col == null or $FTD_col == null)
            return $translated_csv;

        foreach ($translated_csv as $row)
        {
            $this->appendMutuallyExclusiveKeys($final_result, $row, [$REGISTRATION_col, $FTD_col]);
        }

        return $final_result;
    }
}