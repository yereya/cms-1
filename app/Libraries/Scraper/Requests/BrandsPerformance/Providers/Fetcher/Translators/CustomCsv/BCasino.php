<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


use League\Csv\Reader;

trait BCasino
{
    /**
     * BCasino
     * Custom translator for BCasino
     *
     * @param     $csv
     * @param     $offset
     * @param int $limit
     *
     * @param null $delimiter
     * @return array
     */

    public function BCasino($csv, $offset = 0, $limit = 0, $delimiter = null)
    {

        $appended_results = [];
        $headers = [];
        $data = str_getcsv($csv, "\n");
        foreach ($data as $row){
            $translated_row = str_getcsv($row);
            if($translated_row[0] == 'Channel'){
                $headers = $translated_row;
            }
            else{
                $new_row = array_combine($headers,$translated_row);
                if(!in_array('Join date',array_keys($new_row))) {
                    $new_row['Join date'] = $new_row['Pay period'];
                }
                if(!in_array('Payload',array_keys($new_row))) {
                    $new_row['Payload'] = "";
                }
                $appended_results[]=$new_row;
            }
        }
        return $appended_results;
    }
}