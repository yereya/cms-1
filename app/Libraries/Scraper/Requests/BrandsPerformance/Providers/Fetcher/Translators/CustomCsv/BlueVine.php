<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;


trait BlueVine
{
    public function BlueVine($csv, $offset, $limit = 0)
    {
        $translated_result = self::translate($csv, $offset, $limit);
        $final_result = [];

        foreach ($translated_result as &$translated_item) {

            // extract the token
            $translated_item["Full Landing Url"] = substr($translated_item["Full Landing Url"], strpos($translated_item["Full Landing Url"], 'subid') + 5, strlen($translated_item["Full Landing Url"]) - 1);
            $final_result[] = $translated_item;
        }

        return $final_result;
    }
}