<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

use App\Libraries\TpLogger;

trait EHarmonyUK
{
    public function eHarmonyUk($html, $table_selector = 'table[border="1"] tbody', $header_selector = 'tr td[valign="bottom"]', $limit = 0)
    {
        $final_table = [];
        $this->createCrawler($html);
        $table = $this->filter($table_selector);
        $col_headers_by_rowspan = [];

        // If there exists a matching node
        if ($table->getNode(0)) {
            // First table row is the header values
            $headers_node = $table->filter('tr')->eq(0);
            // The next rows are the actual table data
            $row_nodes = $table->filter('tr:not(:first-child):not(:last-child)');
            // Groups the headers string(of the form: "(\r|\n)+{header1}(\r\n)+{header2}...") as an exploded array
            $headers = array_filter(explode(';', preg_replace('/(\v)+/', ';', $headers_node->text())));
            $headers_count = count($headers);

            // Go over all of the table's rows
            $row_nodes->each(function($tr, $row_idx) use(&$final_table, $headers, &$col_headers_by_rowspan, $headers_count) {
                $columns = array_reverse(collect($tr->filter('td'))->all());

                // Iterate over the current row's columns
                collect($columns)->map(function ($td, $col_idx) use (&$final_table, $row_idx, $headers, $headers_count) {
                    // How many rows the current column spans on
                    $current_rowspan = $td->getAttribute('rowspan');
                    $escaped_text    = preg_replace('/[\v]+/', '', $td->textContent);

                    if (empty($final_table[$row_idx])) {
                        $final_table[$row_idx] = [];
                    }

                    // Append headers to the corresponding row offset(other headers can exist as well, and will retain their status)
                    $final_table[$row_idx] += array_combine(array_values(array_slice($headers, $headers_count - $col_idx - 1, 1, true)), [$escaped_text]);

                    // If there exists a rowspan value
                    if ($current_rowspan) {
                        // Sets the headers {key => value} pairs for the next n rows(according to rowspan value)
                        foreach (range($row_idx, $row_idx + $current_rowspan - 1) as $rowpsan_offset) {
                            if (!isset($final_table[$rowpsan_offset])) {
                                $final_table[$rowpsan_offset] = [];
                            }

                            // combine the current column's corresponding header and the cell value
                            $final_table[$rowpsan_offset] += array_combine(array_values(array_slice($headers, $headers_count - $col_idx - 1, 1, true)), [$escaped_text]);
                        }
                    }
                })->all();
            });

            foreach ($final_table as &$row) {
                ksort($row);
            }

            return $final_table;
        }

        $tp_logger = TpLogger::getInstance();
        $tp_logger->notice('There is no data available or provided table selector parameter is wrong: ' . $table_selector);

        return [];
    }
}
