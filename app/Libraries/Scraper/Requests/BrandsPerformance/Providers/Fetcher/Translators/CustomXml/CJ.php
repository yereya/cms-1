<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomXml;

use App\Libraries\Guzzle;
use GuzzleHttp\Client;

trait CJ
{
    /**
     * cj Dating
     * translates Dating Commission Junction
     *
     * @param $xml
     * @param $offset
     *
     * @return mixed
     */

    /**
     * @var string $this->login_url
     */
    private $login_url = "https://members.cj.com/member/foundation/memberlogin.do";

    /**
     * @var string $base_reports_url
     */
    private $base_reports_url = "https://members.cj.com/member/publisher/4395063/commissionReport/orderId/";

    /**
     * @var string $base_reports_query
     */
    private $base_reports_query = "allowAllDateRanges=true&columnSort=publisherCommission%09DESCstartRow=1&endRow=100";

    public function cjDating($xml, $offset, $start_date = null, $end_date = null)
    {
        $translated_result = self::translate($xml, $offset);
        $result = [];
        $tmp = [];
        // Take only the advertisers we want
        foreach ($translated_result as &$translated_item) {
//            if ($translated_item['ADVERTISER-NAME'] != 'Match.com' && $translated_item['ADVERTISER-NAME'] != 'People Media'	)
//                continue;
            // Change the sale name column
            if ($translated_item['ACTION-TYPE'] == 'advanced sale') {
                $translated_item['ACTION-TYPE'] = 'sale';
            }
            // If the token is not empty, add it to the dictionary
            if (!empty($translated_item['SID'])) {
                $translated_item['suggested_token'] = $translated_item['SID'];
                $tmp[$translated_item['ORDER-ID']] = $translated_item['SID'];
            }
            else{ // If the token is empty, find it from the dictionary by order-id
                if(array_key_exists($translated_item['ORDER-ID'], $tmp)){
                    $translated_item['suggested_token'] = $tmp[$translated_item['ORDER-ID']];
                }
                else // If the order-id in not found, the token is null
                    $translated_item['suggested_token'] = '';
            }

            $this->normalizeValue($translated_item['ADVERTISER-NAME']);
            $this->normalizeValue($translated_item['SID']);
            $result[] = $translated_item;
        }

        return $result;
    }

    /**
     * Normalizes a passed value or string for correct processing normal-form
     *
     * @param $value
     * @return mixed
     */
    private function normalizeValue(&$value)
    {
        $value = preg_replace(['/\[/','/\]/'], '', $value);
        $value = preg_replace('/%24/', '$', $value);
        $value = preg_replace('/%21/', '!', $value);
    }

    /**
     * Gets the SID by order-id parameter
     * Will execute a request to perform the query for our brand's order-id
     * and determine its SID
     *
     * @param $translated_item
     * @return string
     */
    private function fetchSIDByOrderId($translated_item)
    {
        // Initialize a new guzzle client
        $guzzle      = new Guzzle();
        $fetched_sid = '';
        $order_id    = explode('-', $translated_item['ORDER-ID'])[0] ?? null;

        if (is_null($order_id)) {
            return $order_id;
        }

        // Initialize the login parameters
        $login_form_params = [
            "uname" => $this->scraper['properties']['username'],
            "pw" => $this->scraper['properties']['password']
        ];

        // Initialize the report query parameters
        $reports_query_url = $this->base_reports_url
            . $order_id . ".json?" . $this->base_reports_query;

        // Fetches the results - starts at login authentication and continues to report queries
        $fetched_json_results = $guzzle->fetch($this->login_url, ["headers" => [], "form_params" => $login_form_params, "verify" => false],"POST")
            ->fetch($reports_query_url, ["headers" => [], "verify" => false], "GET");

        if (!isJson($fetched_json_results->getBody())) {

            return $fetched_sid;
        }

        // For each fetched result, ask if the order id exists and take the result's SID identifier
        foreach ( (json_decode($fetched_json_results->getBody(), true)['records']['record'] ?? ['orderId' => null]) as $fetched_result ) {
            if ( ($fetched_result['orderId'] ?? []) == $order_id) {
                $fetched_sid = $fetched_result['sid'];

                return $fetched_sid;
            }
        }

        return $fetched_sid;
    }

    /**
     * cj Website
     * translates Website Builder Commission Junction
     *
     * @param $xml
     * @param $offset
     *
     * @return mixed
     */
    public function cjWebsite($xml, $offset)
    {
        $translated_result = self::translate($xml, $offset);

        $brandsList = [
            'Webroot Software'                                        => [
                'ioid' => '55151779df48edeb40076a37',
                'name' => 'webroot-software'
            ],
            'Panda Security'                                          => [
                'ioid' => '554a04c85561e2782fde183a',
                'name' => 'panda-security'
            ],
            'Norton by Symantec'                                      => [
                'ioid' => '55029e37ce30d7091d4b3a95',
                'name' => 'norton-by-symantec'
            ],
            'GoDaddy.com'                                             => [
                'ioid' => '550aa115cca9518c7eef01d0',
                'name' => 'godaddycom'
            ],
            'ESET North America'                                      => [
                'ioid' => '563f492a90cbdccb01bc1760',
                'name' => 'eset-north-america'
            ],
            'Bullguard'                                               => [
                'ioid' => '55029d6fce30d7091d4b3a7e',
                'name' => 'bullguard'
            ],
            'AVG Technologies'                                        => [
                'ioid' => '55151bb5df48edeb40076a47',
                'name' => 'avg-technologies'
            ],
            'bitdefender'                                             => [
                'ioid' => '5530eb903549b8711ea078d4',
                'name' => 'bitdefender'
            ],
            'Norton by Symantec - Germany'                            => [
                'ioid' => '554a06285561e2782fde185e',
                'name' => 'norton-by-symantec-germany'
            ],
            '1&1 USA Affiliate Program: Domains, Web Hosting & more.' => [
                'ioid' => '550a9b4acca9518c7eeefe5a',
                'name' => '11-usa-affiliate-program-domains-web-hosting-more'
            ],
            '1&1 Internet Inc.'                                       => [
                'ioid' => '564af09b76022de75f8ed85d',
                'name' => '11-internet-inc'
            ],
            '(s) Strikingly'                                          => [
                'ioid' => '56f8f91648aeb2ea4ce91653',
                'name' => 's-strikingly'
            ],
            'Norton by Symantec - UK'                                 => [
                'ioid' => '55029e37ce30d7091d4b3a95',
                'name' => 'norton-by-symantec-uk'
            ],
            'Norton by Symantec - France'                             => [
                'ioid' => '586e514a349815273b2626b2',
                'name' => 'norton-by-symantec-france'
            ],
            'Norton by Symantec - Netherlands' => [
                'ioid' => '58c114c403ef25ac7ab76b70',
                'name' => 'norton-by-symantec-netherlands'
            ],
            'Norton by Symantec - Finland' => [
                'ioid' => '58c114c403ef25ac7ab76b70',
                'name' => 'norton-by-symantec-finland'
            ],
            'Norton by Symantec - Sweden' => [
                'ioid' => '58c114c403ef25ac7ab76b70',
                'name' => 'norton-by-symantec-sweden'
            ],
            'Norton by Symantec - Italy' => [
                'ioid' => '58c114c403ef25ac7ab76b70',
                'name' => 'norton-by-symantec-italy'
            ],
            'Norton by Symantec - Spain' => [
                'ioid' => '58c114c403ef25ac7ab76b70',
                'name' => 'norton-by-symantec-spain'
            ],
            'VIPRE Antivirus'                                         => [
                'ioid' => '55029e37ce30d7091d4b3a95',
                'name' => 'vipre-antivirus'
            ],
            'Kaspersky Lab North America'                             => [
                'ioid' => '575699f020e5f9387a0de6d9',
                'name' => 'kaspersky-lab-north-america'
            ],
            'One.com'                                                 => [
                'ioid' => '57c422800f20411764541de5',
                'name' => 'onecom'
            ],
            'Intego Mac Security' => [
                'ioid' => '57c56c7e0f20411764541dff',
                'name' => 'intego-mac-security'
            ]
        ];

        foreach ($translated_result as &$translated_item) {
            if (isset($brandsList[$translated_item['ADVERTISER-NAME']])) {
                $translated_item['ioid'] = $brandsList[$translated_item['ADVERTISER-NAME']]['ioid'];
                $translated_item['name'] = $brandsList[$translated_item['ADVERTISER-NAME']]['name'];

                // for Panda Security with WEBSITE-ID == 8231112 (french) set other ioid
                if ($translated_item['ADVERTISER-NAME'] == 'Panda Security' && $translated_item['WEBSITE-ID'] == '8231112') {
                    $translated_item['ioid'] = '585fdab37a6a12eb34024975';
                }
            } else {
                $translated_item['ioid'] = '';
                $translated_item['name'] = $this->prepareAdverName($translated_item['ADVERTISER-NAME']);
            }

            if ($translated_item['ACTION-TYPE'] == 'advanced sale') {
                $translated_item['ACTION-TYPE'] = 'sale';
            }

            // replace '%24' with '$' + replace '%21 with '!'
            $this->normalizeValue($translated_item['SID']);
        }

        return $translated_result;
    }

    /**
     * Prepare Advertiser Name
     * convert advertiser name from how it arrives to like it appears in $brandsList
     *
     * @param $name
     *
     * @return mixed|string
     */
    private function prepareAdverName($name)
    {
        $name = strtolower($name);
        $name = preg_replace('/^\s+|\s+$/', '', $name);
        $name = preg_replace('/[^a-z0-9 -]/', '', $name);
        $name = preg_replace('/\s+/', '-', $name);
        $name = preg_replace('/-+/', '-', $name);

        return $name;
    }
}