<?php

namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

trait Wrike
{
    /**
     * Wrike translator
     *
     * @param $json
     * @param null $selector
     * @param string $date_col
     * @return array
     */
    public function wrike($json, $selector = null, $date_col = 'date')
    {
        $translated_results = self::translate($json, $selector);
        // Reformats the unix string to Y-m-d format
        $formatted_results = array_map(function(&$result) use ($date_col) {
            if (!empty($result[$date_col])) {
                $result[$date_col] = date('Y-m-d', substr($result[$date_col], 0, 10));
            }

            return $result;
        }, $translated_results);
        $final_results = [];
        $seen_dates = [];

        // Appends every seen date incremented
        foreach ($formatted_results as $formatted_result) {
            $_date = $formatted_result[$date_col] ?? '';

            if (empty($_date)) {
                continue;
            }

            if (!isset($seen_dates[$_date])) {
                $seen_dates[$_date] = -1;
            }

            $seen_dates[$_date]++;
            $final_results[] = $formatted_result + ['counter_col' => $_date . '-' . $seen_dates[$_date]];
        }

        return $final_results;
    }
}