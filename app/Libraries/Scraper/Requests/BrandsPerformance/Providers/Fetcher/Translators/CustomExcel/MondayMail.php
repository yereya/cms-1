<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel;


use App\Libraries\TpLogger;
use DB;
use Exception;


trait MondayMail
{
    /**
     * @param     $excel_str
     * @param     $date_offset
     * @param     $brand_id
     * @param     $countries_list
     * @param     $token_col
     * @param     $date_col
     * @param     $country_col
     * @return array
     */
    public function MondayMail($excel_str,$date_offset,$brand_id,$countries_list,$token_col,$date_col,$country_col)
    {
        $tp_logger = TpLogger::getInstance();
        $countries_list = explode(';', $countries_list); // Split countries list by delimiter
        $countries_dict = [];
        # Create Dict of countries and the related commissions
        foreach ($countries_list as $i => $country){
                $splitted_str = explode("=",$country);
                if(sizeof($splitted_str) != 2){
                    throw new Exception("Wrong country=>commission format entered");
                }
                $countries_dict[$splitted_str[0]] =  strtolower($splitted_str[1]);
        }

        // Create excel reader object(PHPExcel)
        try{
            $php_excel_obj = $this->createExcelReader($excel_str);
        }catch (Exception $e){
            $tp_logger->warning("Error while creating Excel Reader, MondayMail.php  -- " . $e);
        }
        if (empty($php_excel_obj)) {
            return null;
        }
        $excel_sheets = $this->getAllSheets($php_excel_obj);
        $results = $excel_sheets[0];
        $headers = $results[0];

        # There are few files attached in the email, translate the relevant one
        if($headers[0] != $date_col) return [] ;

        # Combine headers with values
        $results = array_map(function($row) use ($headers){
            return array_combine($headers,$row);
        },array_slice($results,1));

        # Translate the date_offset to date format, should be of strtotime format
        if(!strtotime($date_offset)){ // Wrong format
            throw new Exception(" Error in the format of the date offset variable, use strtotime format - MondayMail.php trait.");
        }
        $date = date('Y-m-d',strtotime($date_offset));

        # Date in case the user entered wrong / too far date
        $date_for_safety = date('Y-m-d',strtotime("-1 month"));

        # Get data from db
        $track_upper_case = DB::connection('dwh')->table('dwh_fact_tracks')
            ->select('id','tokenip')
            ->where('brand_id', $brand_id)
            ->where('event', 'click')
            ->whereDate('updated_at', '>',$date)
            ->whereDate('updated_at', '>',$date_for_safety)->get()->toArray();

        # StdCalls to Array
        $track_upper_case = array_map(function($row){
            $r = json_decode(json_encode($row), true);
            return $r;
        },$track_upper_case);

        $track_lower_case = array_map('strtolower',array_column($track_upper_case, 'id'));

        # Look for the lower case values and build a new result
        $output = [];
        $dict = [];
        foreach ($results as $key => $row) {
                $token = $row[$token_col];
                $row[$token_col . '_original'] = $token;
              if ($token) { // If not EMPTY Token
                    $index = array_search($token, $track_lower_case);
                    if ($index) {
                        $row[$token_col] = $track_upper_case[$index]['id'];
                    }
//              } else if($row['ip']) { // If EMPTY Token and Not EMPTY IP
//
//                    # Find token by ip
//                    $input = preg_quote($row['short ip'], '~');
//                    $ip = preg_grep('~' . $input . '~', array_column($track_upper_case, 'tokenip'));
//                    # Handle situation when token is empty and ip doesn't exist - set to ''
//                    try{
//                        $row[$token_col] = $track_upper_case[array_keys($ip)[0]]['id'];
//                    }
//                    catch (Exception $e){
//                        $row[$token_col] = $row['short ip'];
//                    }
                } else if($row[$date_col]){ // EMPTY Token and EMPTY IP
                      # Counter for empty tokens and ip by date
                      $row_date = $row[$date_col];
                      if (isset($dict[$row_date])) {
                          $dict[$row_date] = $dict[$row_date] + 1;
                      } else {
                          $dict[$row_date] = 1;
                      }
                  $row[$token_col] = $row_date.' - '.$dict[$row_date];
                } else { // If empty line, skip to the next one
                      continue;
               }

                # Check date format
                if(!(bool)strtotime($row[$date_col])){
                    $row[$date_col] =  \PHPExcel_Style_NumberFormat::toFormattedString($row[$date_col], 'M/D/YYYY');
                }
                # Attach commission based on the dict
                $country_code = strtolower($row[$country_col]);
                if(array_key_exists($country_code,$countries_dict)){
                    # Validate the type of the commissions
                    if(!is_numeric($countries_dict[$country_code])){
                        throw new Exception("The commissions for ".$country_code." is not a correct number.");
                    }
                    $row['commissions'] = intval($countries_dict[$country_code]) * $row['signups'];
                }else{
                    throw new Exception("Country's code doesn't match. Check the country's codes you have entered in the fetcher or the codes in the excel file.");
                }
                try{
                    $row['Intent V12'] = str_replace('%', '', $row['Intent V12']);
                }catch (Exception $e){
                    $tp_logger->warning("Error while translating Intent V12 val - MondayMail.php" . $e);
                }
                $output[] = $row;
        }
        return $output;
    }

    /** update for the old scraper MondayMail
     * @param     $excel_str
     * @param     $date_offset
     * @param     $brand_id
     * @param     $countries_list
     * @param     $token_col
     * @param     $date_col
     * @param     $country_col
     * @return array
     */
    public function MondayMailNew($excel_str, $date_offset, $brand_id, $countries_list,$token_col='attributed_medium', $date_col='touch_time',$country_col='xi_country')
    {
        $tp_logger = TpLogger::getInstance();
        $countries_list = explode(';', $countries_list); // Split countries list by delimiter
        $countries_dict = [];
        # Create Dict of countries and the related commissions
        foreach ($countries_list as $i => $country) {
            $splitted_str = explode("=", $country);
            if (sizeof($splitted_str) != 2) {
                throw new Exception("Wrong country=>commission format entered");
            }
            $countries_dict[$splitted_str[0]] = strtolower($splitted_str[1]);
        }

        // Create excel reader object(PHPExcel)
        try {
            $php_excel_obj = $this->createExcelReader($excel_str);
        }catch (Exception $e){
          $tp_logger->warning("Error while creating Excel Reader, MondayMail.php  -- " . $e);
        }
        if (empty($php_excel_obj)) {
            return null;
        }

        $excel_sheets = $this->getAllSheets($php_excel_obj);
        $results = [];
//        $headers = $excel_sheets[0][0];
        $headers = array_slice($excel_sheets[0][0],0,8);
        $headers[8] = 'sheet';

        # There are few files attached in the email, translate the relevant one
        if ($headers[0] != 'touch_time') return [];

        $i = 0;
        $new_excel_sheet = [];

        foreach ($excel_sheets as $excel_sheet) {
            $excel_sheet = array_slice($excel_sheet, 1);
            foreach ($excel_sheet as $row) {
                if ($row[0] != null) {
                    $row = array_slice($row, 0, 8);
                    $row[7] = substr($row[7], 0, -1);
                    $row[8] = $i;
                    $new_excel_sheet[] = $row;
                }
            }
            $results = array_merge($results, $new_excel_sheet);
            $new_excel_sheet = [];
            $i++;
        }

        # Combine headers with values
        $results = array_map(function ($row) use ($headers) {
            return array_combine($headers, $row);
        },$results);

        # Translate the date_offset to date format, should be of strtotime format
        if (!strtotime($date_offset)) { // Wrong format
            throw new Exception(" Error in the format of the date offset variable, use strtotime format - MondayMail.php trait.");
        }
        $date = date('Y-m-d', strtotime($date_offset));

        # Date in case the user entered wrong / too far date
        $date_for_safety = date('Y-m-d', strtotime("-1 month"));

        # Get data from db
        $track_upper_case = DB::connection('dwh')->table('dwh_fact_tracks')
            ->select('id', 'tokenip')
            ->where('brand_id', $brand_id)
            ->where('event', 'click')
            ->whereDate('updated_at', '>', $date)
            ->whereDate('updated_at', '>', $date_for_safety)->get()->toArray();

        # StdCalls to Array
        $track_upper_case = array_map(function ($row) {
            $r = json_decode(json_encode($row), true);
            return $r;
        }, $track_upper_case);

        $track_lower_case = array_map('strtolower', array_column($track_upper_case, 'id'));

        # Look for the lower case values and build a new result
        $output = [];
        $dict = [];

        foreach ($results as $key => $row) {
            $token = $row[$token_col];
            $row[$token_col . '_original'] = $token;
            if ($token) { // If not EMPTY Token
                $index = array_search($token, $track_lower_case);
                if ($index) {
                    $row[$token_col] = $track_upper_case[$index]['id'];
                }
//            } else if ($row['ip']) { // If EMPTY Token and Not EMPTY IP
//
//                # Find token by ip
//                $input = preg_quote($row['short ip'], '~');
//                $ip = preg_grep('~' . $input . '~', array_column($track_upper_case, 'tokenip'));
//                # Handle situation when token is empty and ip doesn't exist - set to ''
//                try {
//                    $row[$token_col] = $track_upper_case[array_keys($ip)[0]]['id'];
//                } catch (Exception $e) {
//                    $row[$token_col] = $row['short ip'];
//                }
            } else if ($row[$date_col]) { // EMPTY Token and EMPTY IP
                # Counter for empty tokens and ip by date
                $row_date = $row[$date_col];
                if (isset($dict[$row_date])) {
                    $dict[$row_date] = $dict[$row_date] + 1;
                } else {
                    $dict[$row_date] = 1;
                }
                $row[$token_col] = $row_date . ' - ' . $dict[$row_date];
            } else { // If empty line, skip to the next one
                continue;
            }

            # Check date format
            if (!(bool)strtotime($row[$date_col])) {
                $row[$date_col] = \PHPExcel_Style_NumberFormat::toFormattedString($row[$date_col], 'M/D/YYYY');
            }
            # Attach commission based on the dict
            $country_code = strtolower($row[$country_col]);
            if (array_key_exists($country_code, $countries_dict)) {
                # Validate the type of the commissions
                if (!is_numeric($countries_dict[$country_code])) {
                    throw new Exception("The commissions for " . $country_code . " is not a correct number.");
                }
                $row['commissions'] = intval($countries_dict[$country_code]) * $row['signups'];
            } else {
                throw new Exception("Country's code doesn't match. Check the country's codes you have entered in the fetcher or the codes in the excel file.");
            }

            $output[] = $row + ['counter_col' => 1];

            /*
            $counter_amount = $row['signups'];
            if ($counter_amount > 1) {
                $row['signups'] = 1;
            }

            for ($i = 1; $i <= $counter_amount; $i++) {
                $output[] = $row + ['counter_col' => $i];
            }
            */
        }
        return $output;
    }

}
