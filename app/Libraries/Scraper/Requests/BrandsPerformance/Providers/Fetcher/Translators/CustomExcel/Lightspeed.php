<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomExcel;


trait Lightspeed
{
    /**
     * Special function for lightspeed Scripts that cut a strings for substring to take tokens
     *
     * @param $excel_str
     * @param int $offset
     * @param int $limit
     * @return array
     */
    public function Lightspeed($excel_str, $offset = 0, $limit = 0)
    {
        $final_results = [];
        $results       = $this->translate($excel_str, $offset);
        $headers       = $results[0];

        $pattern = "/[_]/";
        for ($i = 1; $i < count($results); $i++) {
            // Remove $ signs
            $results[$i]= self::removeSigns($results[$i]);

            // Cut the token from the string by splite substrings by '_'. Example: 1145_102d38002efc6e53b7857eb733729b_rk!h6Vqt8_95_CA => rk!h6Vqt8
            if (isset($results[$i][2])) {
                $parts = preg_split($pattern, $results[$i][2]);
                $results[$i][2] = $parts[2];
            }

            $final_results[] = array_combine($headers, $results[$i]);
        }

        if ($limit) {
            return array_slice($final_results, $offset, count($results) - $limit);
        }

        return $final_results;
    }

}