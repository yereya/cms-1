<?php


namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;


trait Emma
{
    /**
     * @param        $html
     * @param string $table_wrapper
     * @param string $header_selector
     * @param int    $offset
     * @param int    $limit
     *
     * @return array
     */


    public function emmaTranslator($html, $table_wrapper = 'table', $header_selector = 'th', $offset = 0, $limit = 0)
    {
        $results = [];
        $assoc   = true;
        $this->createCrawler($html);
        $table = $this->filter($table_wrapper);
        $results_state = $table->getNode(0);
        if ($results_state) {
            $this->getEmmaKeys($table, $header_selector);
            $this->getEmmaValues($table, $offset);
            if ($limit != 0) {
                $this->values = array_slice($this->values, 0, $limit);
            }
            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else {
                if ($this->values && empty($this->keys)) {
                    $results = $this->values;
                    $assoc   = false;
                } else {
                    if ($this->keys && empty($this->values)) {
                        return $this->keys;
                    }
                }
            }
            // this if is for looping days functionality
            if ((int)($this->fetcher['properties']['concat_n_days_results'] ?? 0) > 0) {
                $this->buildDateColumn($results, $assoc);
            }
            return $results;
        }
        $tp_logger = TpLogger::getInstance();
        $tp_logger->notice('There is no data available or provided table selector parameter is wrong: ' . $table_selector);
        return [];
    }
    /**
     * @param $target_crawler
     * @param $key_selector
     */
    private function getEmmaKeys($target_crawler, $key_selector): void
    {
        $target_crawler->filter($key_selector)
            ->reduce(function ($node, $i) {
                $temp_val = $this->removeNbsp($node->text());
                $temp_val = preg_replace('/\\\\./', '', $temp_val);
                $temp_val = rtrim($temp_val);
                $this->keys[$i] = $temp_val;
            });
    }
    /**
     * @param $target
     * @param $offset
     */
    private function getEmmaValues($target, $offset = 0): void
    {
        $idx = 0;
        $target->filter('tr')->reduce(function ($tr) use (&$idx, $offset) {
            if ($idx >= $offset && $tr->attr('style') != 'display:none;') {
                $tr->filter('td')->reduce(function ($td, $i) use ($idx) {
                    $temp_val = $this->removeNbsp($td->text());
                    $temp_val = preg_replace('/\\\\./', '', $temp_val);
                    $temp_val = rtrim($temp_val);
                    $this->values[$idx][$i] = $temp_val;
                });
            }
            $idx++;
        });
    }

}