<?php namespace app\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait Simba
{
    /**
     * Simba
     * Translates SimbaGames brand
     *
     * @param $html
     *
     * @return mixed
     */
    public function simba($html)
    {
        $class = $this->simba_getTableClass($html);

        $this->createCrawler($html);
        $table = $this->filter('.' . $class);
        $assoc = true;

        if ($table->getNode(0)) {
            $this->simba_getKeys($table, 'div');
            $this->getValues($table, 2);

            if ($this->keys && $this->values) {
                $results = $this->keyValueToArray();
            } else if ($this->values && empty($this->keys)) {
                $results = $this->values;
                $assoc   = false;
            } else if ($this->keys && empty($this->values)) {
                return $this->keys;
            }

            // this if is for looping days functionality
            if ((int) $this->fetcher['properties']['concat_n_days_results'] > 0) {
                $this->buildDateColumn($results, $assoc);
            }

            return $results;
        }
    }

    /**
     * Get Table Class
     * gets the table result class from $html.
     * relevant text excerpt for illustration:
     * <table cellspacing="0" cellpadding="0" cols="10" border="0" style="border-collapse:collapse;" class="A6627d08448cf4762afae272a024aa058670">
     *
     * @param $html
     *
     * @return string
     */
    private function simba_getTableClass($html)
    {
        // 'cols="10"' is unique in this $html
        $col_pos      = stripos($html, 'cols="10"');
        $class_pos    = stripos($html, 'class', $col_pos);
        $opening_quot = stripos($html, '"', $class_pos);
        $closing_quot = stripos($html, '"', $opening_quot + 1);

        return substr($html, $opening_quot + 1, $closing_quot - $opening_quot - 1);
    }

    /**
     * Simba get keys
     * Simba has its own function because its keys located in second 'tr'
     * and not first.
     *
     * @param $crawler
     * @param $header_selector
     */
    private function simba_getKeys($crawler, $header_selector)
    {
        $crawler->filter('tr')->eq(1)->reduce(function ($tr) use ($header_selector) {
            $tr->filter($header_selector)->reduce(function ($node, $i) {
                $this->keys[$i] = $this->removeNbsp($node->text());
            });
        });
    }
}