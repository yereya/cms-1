<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomCsv;

use Illuminate\Support\Collection;

trait BetStar
{
    /**
     * BetStar
     * Custom translator for BetStar
     *
     * @param     $csv
     * @param     $offset
     * @param int $limit
     *
     * @return array
     */

    public function betStar($csv, $offset, $limit = 0)
    {
        $results                           = [];
        $translated_result                 = self::translate($csv, $offset, $limit);
        $generated_values_before_iteration = collect();

        /*SORT ALL RESULTS BY DATE AND GROUP THEM BY UNIQUE KEY OF DATE-BRAND-MARKETING CODE*/
        $grouped_results = collect($translated_result)
            ->sortBy('Date')
            ->values()
            ->groupBy(function ($record) {
                return $record['Date'] . '-' . $record['Brand'] . '-' . $record['Marketing Code or Tracker'];
            });

        /*SUM ALL LEADS AND SALES ACCORDING TO THE MARKETING CODE */
        $grouped_results->each(function ($records) use ($generated_values_before_iteration) {
            $temp = collect();
            $temp = $temp->merge($records[0]);

            /*LEAD*/
            $newAccountSum = $records->sum(function ($record) {
                return $record['New Acc.'];
            });

            /*SALE*/
            $qualityCPASum = $records->sum(function ($record) {
                return $record['Qual. CPA'];
            });

            $temp->put('New Acc.', $newAccountSum);
            $temp->put('Qual. CPA', $qualityCPASum);

            $generated_values_before_iteration->push($temp);
        });
        $this->resolveRowsAccordingToDate($generated_values_before_iteration, $results);

        return $results;
    }

    /**
     * @param Collection $generated_values_before_iteration
     * @param array      $results
     */
    private function resolveRowsAccordingToDate(Collection $generated_values_before_iteration, array &$results)
    {
        $generated_values_before_iteration
            ->groupBy('Date')
            ->each(function ($records, $date) use (&$results) {
                $current_date = $date;
                $temp         = [];

                collect($records)->each(function ($result, $idx) use ($current_date, &$temp, &$results) {
                    $temp['date']             = $current_date;
                    $temp['date_counter_col'] = $idx;

                    $lead_state = $result['New Acc.'];
                    $sale_state = $result['Qual. CPA'];

                    /*LEAD ROWS*/
                    if ($lead_state) {
                        $temp['event'] = 'lead';
                        $events_amount = $result['New Acc.'];

                        for ($lead = 0; $lead < $events_amount; $lead++) {
                            $temp['counter_col'] = $temp['date_counter_col'] . 'lead' . $lead;
                            $temp['trx_id'] = $temp['date'] . '-' . $temp['counter_col'];
                            $results[]      = $temp + $result->toArray();
                        }
                    }

                    /*SALE ROWS*/
                    if ($sale_state) {
                        $temp['event'] = 'sale';
                        $events_amount = $result['Qual. CPA'];

                        for ($sale = 0; $sale < $events_amount; $sale++) {
                            $temp['counter_col'] = $temp['date_counter_col'] . 'sale' . $sale;
                            $temp['trx_id'] = $temp['date'] . '-' . $temp['counter_col'];
                            $results[]      = $temp + $result->toArray();
                        }
                    }
                });
            });
    }

}

