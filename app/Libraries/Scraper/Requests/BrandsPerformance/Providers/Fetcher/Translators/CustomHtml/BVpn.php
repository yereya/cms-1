<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

trait BVpn
{
    /**
     * Generates a date-event-trx_id result table, given the initial table's html
     *
     * @param        $html
     * @param int    $sale_offset
     * @param int    $table_row_offset
     * @param int    $cancelled_offset
     * @param null   $table_selector
     * @param string $header_selector
     * @param int    $limit
     *
     * @return array
     */
    public function bVpn(
        $html,
        $sale_offset = 0,
        $table_row_offset = 0,
        $cancelled_offset = 0,
        $table_selector = null,
        $header_selector = 'th',
        $limit = 0
    ) {
        // Sets the targeted table columns offsets, according to each event's received param
        $final_table = [];
        $this->createCrawler($html);
        $table = $this->filter('tbody');

        $table->filter('tr')->reduce(function ($tr, $idx) use (
            &$final_table,
            $table_row_offset,
            $sale_offset,
            $cancelled_offset
        ) {
            if ($idx >= $table_row_offset) {

                //Set the columns offset for events
                $sale_counter      = 0;
                $cancelled_counter = 0;

                // The first column contains the event's date
                $current_date = $this->toYMD($tr->filter('td')->first()->text());

                $tr->filter('td')->reduce(function ($td, $column_index) use (
                    $idx,
                    $table_row_offset,
                    &$final_current_column,
                    $current_date,
                    &$final_table,
                    $sale_offset,
                    $cancelled_offset,
                    &$sale_counter,
                    &$cancelled_counter
                ) {
                    $events_number = intval(trim($td->text()));

                    // Check if the current column is in the sale event's range or the cancelled event's range
                    if ($column_index >= $sale_offset && $column_index < $cancelled_offset && $events_number > 0) {
                        $sale_counter += $events_number;
                    } elseif ($column_index >= $cancelled_offset && $column_index < 15 && $events_number > 0) {
                        $cancelled_counter += $events_number;
                    }
                });

                if ($sale_counter > 0) {
                    $this->generateSaleRows($final_table, $current_date, $sale_counter);
                }

                if ($cancelled_counter > 0) {
                    $this->generateCancelRows($final_table, $current_date, $cancelled_counter);
                }
            }
        });

        return $final_table;
    }

    /**
     * Changing the European date format string to Y-m-d
     *
     * @param $date
     *
     * @return false|string
     */
    private function toYMD($date)
    {
        $copy            = str_replace('-', '/', $date);
        $new_date_format = date("Y-m-d", strtotime($copy));

        return $new_date_format;
    }

    private function generateSaleRows(&$final_table, $current_date, $sales_amount)
    {
        $sales_counter = 1;
        $_sale_event   = [
            '_event' => 'sale',
            '_date'  => $current_date,
        ];

        do {
            $_sale_event['_trx_id'] = 'bvpn-sale-' . $current_date . '-' . $sales_counter;
            $final_table[]          = $_sale_event;
            $sales_counter++;
        } while ($sales_counter <= $sales_amount);
    }

    private function generateCancelRows(&$final_table, $current_date, $cancel_amount)
    {
        $cancels_counter = 1;
        $_cancel_event   = [
            '_event' => 'canceled-sale',
            '_date'  => $current_date,
        ];

        do {
            $_cancel_event['_trx_id'] = 'bvpn-cancel-sale-' . $current_date . '-' . $cancels_counter;
            $final_table[]            = $_cancel_event;
            $cancels_counter++;
        } while ($cancels_counter <= $cancel_amount);
    }
}