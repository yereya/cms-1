<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomHtml;

use App\Libraries\TpLogger;

trait CostaBingo
{
    /**
     * Generates a date-event-trx_id result table, given the initial table's html
     *
     * @param        $html
     * @param null $table_selector
     * @param string $header_selector
     * @param string $row_selector
     * @param int $limit
     * @return array
     */
    public function costaBingo($html, $table_selector = null, $header_selector = 'th', $row_selector = 'tbody tr', $limit = 0)
    {
        // 1. Create a crawler for the table
        // 2. Extract the data columns to an indexed array
        // 3. For each table row:
        //      3.1 create an associative array by combining two arrays (the extracted headers with static "Name" column addition
        //          and the actual given row values
        $final_table = [];
        $this->createCrawler($html);
        $table = $this->filter($table_selector);

        // Get the table rows and columns
        $rows = $table->filter($row_selector);
        $header_columns = ["Name"] + $this->crawlerToArray($table->filter($header_selector));

        // Save $this reference as $that for the closure scope
        $that = $this;

        $rows->each(function ($row, $idx) use ($header_columns, &$final_table, $that) {
            // Get the text values of each row column
            $row_values_result = $that->crawlerToArray($row->children());

            if (count($header_columns) != count($row_values_result)) {
                $tp_logger = TpLogger::getInstance();
                $tp_logger->warning(["failed row: " => ["failed row:" => $row_values_result, "reason:" => "header_columns and row_values differ in length"]]);
            } else {
                // Combine the matrix i'th and j'th values into an associative array [i => j]
                $combined = array_combine($header_columns, $row_values_result);

                // Append the combined array to the brand's name
                $final_table[] = $combined  + ['counter_col' => $idx];
            }
        });

        return $final_table;
    }

    /**
     * Get the crawler nodes as text elements
     *
     * @param $filtered_elements
     * @return array
     */
    private function crawlerToArray($filtered_elements)
    {
        $result = [];
        $filtered_elements->each(function ($element) use (&$result) {
            $result[] = $element->getNode(0)->nodeValue;
        });

        return $result;
    }
}