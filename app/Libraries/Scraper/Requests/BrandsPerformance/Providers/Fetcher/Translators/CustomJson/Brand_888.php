<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\CustomJson;

use DB;
use Exception;

trait Brand_888
{
    public function brand_888(
        $json,
        $offset = null,
        $date_col = 'DateText',
        $commission_col = 'CommissionPerformance',
        $lead_str = 'Registrations',
        $sale_str = 'MoneyPlayers'
    )
    {
        $translated_array  = self::translate($json, $offset);
        $translated_result = [];
        foreach ($translated_array as $translated_item) {
            $token          = $translated_item['Anid'] ?? '';
            $signups_count  = $translated_item[$lead_str];
            $acquired_count = $translated_item[$sale_str];

            // if we have leads clone the event as the leads number
            if ($signups_count > 0) {
                $_event            = $translated_item;
                $_event[$sale_str] = 0;
                for ($i = 1; $i <= $signups_count; $i++) {
                    // format the date to enable correct date comparison
                    $_event['_FormattedDate'] = formatDate($_event[$date_col], 'Y-m-d');
                    $_event['_Anid']     = $_event[$commission_col] . '-' . $token . '-' . $_event[$date_col] . '-' . $i;
                    $translated_result[] = $_event;
                }
            }

            // if we have sales clone the event as the sales number
            if ($acquired_count > 0) {
                $_event            = $translated_item;
                $_event[$lead_str] = 0;
                for ($i = 1; $i <= $acquired_count; $i++) {
                    $_event['_FormattedDate'] = formatDate($_event[$date_col], 'Y-m-d');
                    $_event['_Anid']     = $_event[$commission_col] . '-' . $token . '-' . $_event[$date_col] . '-' . $i;
                    $translated_result[] = $_event;
                }
            }
        }

        return $translated_result;
    }

    public function brand_888_api(
        $json,
        $from_date,$to_date,$number_brand,
        $date_col = 'Date',
        $commission_col = 'CommissionPerformance',
        $lead_str = 'Registrations',
        $sale_str = 'MoneyPlayers'
    )
    {
        if(isset($this->scraper['properties']['username'])) {
            $user_name = $this->scraper['properties']['username'];
        }else{
            throw new \Exception('You have to add username in scrapers interface(inside Settings)');
        }
        if(isset($this->scraper['properties']['token'])){
            $outh_token = $this->scraper['properties']['token'];
        }else{
            throw new \Exception('You have to put token => outh_token for api request, inside the - advanced field - in scrapers interface(under Currency)');
        }

        $data = array("UserName" => $user_name, "Password" => $outh_token);
        $data_string = json_encode($data);

        $ch = curl_init('https://api.aff-online.com/login');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $token = json_decode(curl_exec($ch));
        $accessToken = 'Bearer ' . $token->{'AccessToken'};

        # -------------------------------------------------
        $data = array("BrandGroupId" => $number_brand, "FromDate" => $from_date, "ToDate" => $to_date,"WithAnid"=>"true", "ShowCountry" => "true");
        $data_string = json_encode($data);
        $ch = curl_init('https://api.aff-online.com/reports/traffic');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json'
            ,'Authorization: ' . $accessToken//from login section
            ,'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);

        $translated_data = json_decode($result,true);
        $translated_result = [];

        foreach ($translated_data['TrafficStatRows'] as $translated_item) {
            $token = $translated_item['Anid'] ?? '';
            $signups_count = $translated_item[$lead_str];
            $acquired_count = $translated_item[$sale_str];

            // if we have leads clone the event as the leads number
            if ($signups_count > 0) {
                $_event = $translated_item;
                $_event[$sale_str] = 0;
                for ($i = 1; $i <= $signups_count; $i++) {
                    // format the date to enable correct date comparison
                    $_event['_FormattedDate'] = formatDate($_event[$date_col], 'Y-m-d');
                    $_event['_Anid'] = $token . '-' . $_event[$date_col] . '-' . $i;
                    $translated_result[] = $_event;
                }
            }

            // if we have sales clone the event as the sales number
            if ($acquired_count > 0) {
                $_event = $translated_item;
                $_event[$lead_str] = 0;
                for ($i = 1; $i <= $acquired_count; $i++) {
                    $_event['_FormattedDate'] = formatDate($_event[$date_col], 'Y-m-d');
                    $_event['_Anid'] = $token . '-' . $_event[$date_col] . '-' . $i;
                    $translated_result[] = $_event;
                }
            }
        }


        return $translated_result;
    }

    /**
     * @param $json
     * @param $from_date
     * @param $to_date
     * @param $number_brand
     * @param $brand_id
     * @param string $date_col
     * @param string $commission_col
     * @param string $lead_str
     * @param string $sale_str
     * @param string $token_str
     * @throws Exception
     */
    public function brand_888_api_with_cancellation(
        $json,
        $from_date,$to_date,$number_brand,$brand_id,
        $date_col = 'Date',
        $commission_col = 'CommissionPerformance',
        $lead_str = 'Registrations',
        $sale_str = 'MoneyPlayers',
        $token_str = 'Anid'
    )
    {
        $translated_result = $this->brand_888_api($json, $from_date,$to_date,$number_brand, $date_col, $commission_col, $lead_str, $sale_str);

        $date_offset = '-4 week';

        # Translate the date_offset to date format, should be of strtotime format
        if(!strtotime($date_offset)){ // Wrong format
            throw new Exception(" Error in the format of the date offset variable, use strtotime format - MondayMail.php trait.");
        }
        $date = date('Y-m-d',strtotime($date_offset));

        # Date in case the user entered wrong / too far date
        $date_for_safety = date('Y-m-d',strtotime("-1 month"));

        $brand_id_arr = explode(';', $brand_id);

        # Get data from db
        $tokens_from_db = DB::connection('dwh')->table('dwh_fact_tracks')
            ->select('source_track')
            ->whereIn('brand_id', $brand_id_arr)
            ->where('event', 'sale')
            ->whereDate('updated_at', '>',$date)
            ->whereDate('updated_at', '>',$date_for_safety)->get()->toArray();

        # StdCalls to Array
        $tokens_from_db = array_map(function($row){
            $r = json_decode(json_encode($row), true);
            return $r;
        },$tokens_from_db);

        $tokens_sale_from_input = [];
        foreach ($translated_result as $row){
            if ($row[$sale_str] == 1)
                $tokens_sale_from_input[] = $row[$token_str];
        }

        $output = [];
        foreach ($translated_result as $key => $row) {
            $row['cancel_sale'] = 0;
            $token = $row[$token_str];
            if ($token and $row[$lead_str] > 0) { // If the track is lead
                $index = array_search($token, array_column($tokens_from_db, 'source_track')); // check if the token existing in the DB's tokens sale
                $found = array_search($token, $tokens_sale_from_input); // check if the token existing in the output's tokens sale
                if ($index and !$found) { // And if we find a match between lead's token to old sale's token
                    $row['cancel_sale'] = 1;
                }
            }
            $output[] = $row;
        }
        return $output;
    }
}