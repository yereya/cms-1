<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher;

use App\Libraries\ShortCodes\ShortCodes as BaseShortCodes;

class ShortCodes extends BaseShortCodes
{
    /**
     * part of the rule short code
     * allow the user to an if statment
     * [if col_name="" condition=">" value=""]
     *
     * @param $atts
     * @param $array
     *
     * @return string
     * @throws \Exception
     */
    public function sc_if($atts, $array)
    {
        $atts = filterAndSetParams([
            'col_name'  => null,
            'col_num'   => null,
            'condition' => '=',
            'value'     => null
        ], $atts);

        $col_value = '';

        // Retrieve value 1
        if (!is_null($atts['col_name']) AND isset($array[$atts['col_name']])) {
            $col_value = $array[$atts['col_name']];
        } elseif (!is_null($atts['col_num'])) {
            $_idx = 0;
            foreach ($array as $item) {
                if ($_idx++ == $atts['col_num']) {
                    $col_value = $item;
                }
            }
        }

        // Valida the value for when integer is passed instead of variable
        $col_value     = is_numeric($col_value) ? (int) $col_value : $col_value;
        $atts['value'] = is_numeric($atts['value']) ? (int) $atts['value'] : $atts['value'];
        // Retrieve condition
        $condition = $this->resolveCondition($col_value, $atts['condition'], $atts['value'],$atts['col_name']);

        return $condition;
    }

    /**
     * Part of the rule short codes
     * returns an AND operator
     *
     * @return string
     */
    public function sc_and()
    {
        return ',and,';
    }

    /**
     * Part of the rule short codes
     * returns an AND operator
     *
     * @return string
     */
    public function sc_or()
    {
        return ',or,';
    }

    /**
     * Short code to compare two given dates
     * used as a rule condition
     * example: [dateCompare col_name="RMP Date" condition=">=" value="04-JUL-2017"]
     *
     * @param $atts
     * @param null $array
     * @return bool
     * @throws \Exception
     */
    public function sc_dateCompare($atts, $array = null)
    {
        $atts   = filterAndSetParams([
            'from_format'       => 'm/d/y', // Used when the format of month and day is not the way php expects it
            'col_name'          => null,
            'condition'         => '=',
            'value'             => null
        ], $atts);

        // Default american format for strtotime
        $from_format = $atts['from_format'];

        // Date must be compared with another, otherwise it is irrelevant
        if ( !isset($array) || is_null($atts['col_name']) || is_null($atts['value']) ) {
            return false;
        }

        $comparison_atts = [];
        $translated_date = $this->sc_translated($atts, $array);

        if ( empty($translated_date) ) {
            return false;
        }

        // date_create returns false for invalid formats
        if ( (($translated_date = date_create_from_format($from_format, $translated_date)) !== false)
            AND (($compared_date = date_create_from_format($from_format, $atts['value'])) !== false) ) {
            $translated_date    = $translated_date->format('U');
            $compared_date      = $compared_date->format('U');
            $comparison_atts = [
                'date'          => $translated_date,
                'condition'     => $atts['condition'],
                'compared_date'    => $compared_date,
                'col_name'=> $atts['col_name']
            ];
        }

        // strtotime returns false for invalid formats
        if ( empty($comparison_atts['date']) OR empty($comparison_atts['compared_date']) ) {
            $this->tp_logger->warning([
                "Warning message:" => "Date formats are invalid. please enter valid dates",
                "Failed value:" => $this->sc_translated($atts, $array),
                "Resulting translation" => $translated_date
            ]);
            return false;
        }

        // Retrieve condition
        $condition =  $this->resolveCondition($comparison_atts['date'], $comparison_atts['condition'], $comparison_atts['compared_date'],$comparison_atts['col_name']);

        return $condition;
    }

    /**
     * Search for a variable/property
     * Also allows you to pass type which will allow you search more specifically
     *
     * @param $atts
     *
     * @return bool|string
     * @throws \Exception
     */
    public function sc_var($atts)
    {
        $atts   = filterAndSetParams([
            'name' => '', // the name of the returned variable
            'type' => ''  // scraper/fetcher/translated/response
        ], $atts);
        $result = false;

        // search in haystack['scraper_params'], example:
        /* "scraper_params" => array:2 [
            "id" => "161"
            "properties" => array:16 [
              "debug" => true
              "scraper_name" => "[Casino] Winner"
              "advertiser_id" => "544e15b52c6dfd0e1da022f8"
              "run_time_start" => "4:21"
              ...
            ]
          ]
        */
        if (!$atts['type'] OR $atts['type'] == 'scraper') {
            $scraper = $this->haystack['scraper_params'][endKey((array) $this->haystack['scraper_params'])];
            $result  = $this->getKeyFromHaystack($atts['name'], $scraper);
        }

        // search in haystack['fetchers_params']
        /*
         * "fetchers_params" => array:2 [
                0 => array:3 [
                  "id" => 181995
                  "properties" => array:12 [
                    "headers" => array:1 [
                      "Authorization" => "Basic dHJhZmZpY3BvaW50cHBjOjEyMzQ1Ng=="
                    ]
                    "form_params" => []
                    "query" => []
                    "_token" => "f80pjQDXBQAq7dq7z2ZNh5t2m7QaBYPikgIvrPZB"
                    "order" => "1"
                    "fetcher_id" => "335"
                    "url" => "https://portal.nethive.com/portal/rest/login"
                    .....
                  ]
                  "_response_idx" => 181995
                ]
                1 => array:2 [
                  "id" => 143838
                  "properties" => array:13 [
                    "headers" => []
                    "form_params" => array:2 [
                      "X-Auth-Token" => "[var name="value"]"
                        .....
         */
        if ((!$atts['type'] AND $result === false) OR $atts['type'] == 'fetcher') {
            $result = $this->getKeyFromHaystack($atts['name'], $this->haystack['fetchers_params']);
        }

        // search in haystack['translated']
        /*
         "translated" => array:1 [
            181995 => array:1 [
              "value" => "7931309e-7855-437b-a066-62a43e876088"
            ]
          ]
         */
        if ((!$atts['type'] AND $result === false) OR $atts['type'] == 'translated') {
            if (is_array($this->haystack['translated'])) {
                $result = $this->getKeyFromHaystack($atts['name'], $this->haystack['translated']);
            }
        }

        // in fetcher response we can find item by dot notation, for example Location header: [var name="response_headers.Location.0"]
        // if not dot notation supplied it will search for cookie with supplied name, for example: [var name="csrf" type="response"]
        /*
         "fetchers_response" => array:1 [
            0 => array:2 [
              "request_headers" => array:1 [
                0 => ""
              ]
              "response_headers" => array:7 [
                "Content-Type" => array:1 [
                  0 => "application/json"
                ]
                "Date" => array:1 [
                  0 => "Tue, 21 Feb 2017 12:37:18 GMT"
                ]
                "Server" => array:1 [
                  0 => "GlassFish Server Open Source Edition 3.1.2.2"
                ]
                ....
         */
        if ((!$atts['type'] AND ($result === false OR $result === null)) OR $atts['type'] == 'response') {
            if (is_array($this->haystack['fetchers_response'])) {
                $result = strpos($atts['name'], '.') ? array_get(end($this->haystack['fetchers_response']), $atts['name']) : arrayCookieQuerySearch($atts['name'], $this->haystack['fetchers_response']);
            }
        }

        if ((!$atts['type'] AND ($result === false OR $result === null)) OR $atts['type'] == 'flat-response') {
            if (is_array($this->haystack['fetchers_response'])) {
                $result = arrayCookieQuerySearch($atts['name'], $this->haystack['fetchers_response']);
            }
        }

        return $result;
    }

    /**
     * Get Key From Haystack
     *
     * @param $var_name
     * @param $haystack
     *
     * @return bool|mixed|string
     */
    private function getKeyFromHaystack($var_name, $haystack)
    {
        return strpos($var_name, '.') ? array_get($haystack, $var_name) : arraySearchKey($var_name, $haystack);
    }

    /**
     * Runs php date function
     *
     * @param $atts
     * @param $array
     *
     * @return string
     * @throws \Exception
     */
    public function sc_date($atts, $array = null)
    {
        $atts = filterAndSetParams([
            'from_format' => null, // Used when the format of month and day is not the way php expects it
            'format' => 'Y-m-d',    // the php format for the date
            // http://php.net/manual/en/function.date.php

            'span'       => null,         // Same as the params accepted in strtotime()
            // http://php.net/manual/en/function.strtotime.php
            // Ex: "+1 week", "last Monday"
            'col_name'   => null,
            'col_number' => null
        ], $atts);

        // Set current time
        $now = time();

        // Try parsing the date
        if (isset($array) AND ($atts['col_name'] OR $atts['col_number'])) {
            $_time_string = $this->sc_translated($atts, $array);

            if ($_time_string) {
                if (($_from_format = $atts['from_format'])) {
                    $now = date_create_from_format($_from_format, $_time_string)->format('U');
                }
                // date_create on failure returns false on success returns DateTime object
                elseif (($_date = date_create($_time_string)) !== false) {
                    $now = $_date->format('U');
                } else {
                    $this->logger->warning([
                        "Short code " . $atts['raw'] . " incorrect strtotime param, could not parse value to date",
                        $_time_string,
                        $atts
                    ]);
                    throw new \Exception("** Fatal error" . __CLASS__ . '@' . __FUNCTION__ . '():' . __LINE__);
                }
            } else {
                $this->logger->warning("Short code " . $atts['raw'] . " incorrect find column");
                throw new \Exception("** Fatal error" . __CLASS__ . '@' . __FUNCTION__ . '():' . __LINE__);
            }
        }

        if (isset($atts['span'])) {
            // Test for bad span formatting
            if (($timestamp = strtotime($atts['span'], $now)) !== false) {
                $now = $timestamp;
            } else {
                $this->logger->warning("Short code " . $atts['raw'] . " incorrect strtotime param");
            }
        }

        return date($atts['format'], $now);
    }

    /**
     * Short Code translated will work on the passed array
     * and search the requested value according to the passed attributes
     *
     * @param $atts
     * @param $array
     *
     * @return string
     */
    public function sc_translated($atts, $array)
    {
        $atts = filterAndSetParams([
            'col_name' => '',
            'col_num'  => ''
        ], $atts);

        if ($atts['col_name'] AND isset($array[$atts['col_name']])) {
            return $array[$atts['col_name']];
        } elseif ($atts['col_num']) {
            $_idx = 0;
            foreach ($array as $item) {
                if ($_idx == $atts['col_num']) {
                    return $item;
                }
                $_idx++;
            }
        }

        return '';
    }

    /**
     * Short Code String Replace
     * Which uses the standard str_replace params
     * http://php.net/manual/en/function.str-replace.php
     *
     * @param $atts
     * @param $array
     *
     * @return string
     * @throws \Exception
     */
    public function sc_strreplace($atts, $array = null)
    {
        $atts = filterAndSetParams([
            'search' => null,   // The value being searched for, otherwise known as the needle.
            // An array may be used to designate multiple needles.

            'replace' => '',    // The replacement value that replaces found search values.
            // An array may be used to designate multiple replacements.

            'col_name'   => null,
            'col_number' => null
        ], $atts);

        if (!is_null($atts['search'])) {
            $subject = $this->sc_translated($atts, $array);
            $result  = str_replace($atts['search'], $atts['replace'], $subject);
        } else {
            $this->logger->warning("Short code " . $atts['raw'] . " incorrect search param");
        }

        return $result;
    }

    /**
     * Short Code String Position
     * Which uses the standard strpos params
     * https://www.php.net/manual/en/function.strpos
     *
     * @param $atts
     * @param $array
     *
     * @return string
     * @throws \Exception
     */
    public function sc_strpos($atts, $array = null)
    {
        $atts = filterAndSetParams([
            'search' => null,   // The value being searched for, otherwise known as the needle.
            // An array may be used to designate multiple needles.
            'col_name'   => null,
            'col_number' => null,
            'condition' => 'in',
        ], $atts);

        if (!is_null($atts['search'])) {
            $subject = $this->sc_translated($atts, $array);
            $result  = strpos($subject, $atts['search']);
        } else {
            $this->logger->warning("Short code " . $atts['raw'] . " incorrect search param");
        }
        if ($result !== false and $atts['condition'] == 'in')
            return true;
        else if ($result === false and $atts['condition'] == 'in')
            return '';
        else if ($result === false and $atts['condition'] == 'not in')
            return true;
        else
            return '';
    }

    /**
     * Short Code Preg Match
     * Using the default pattern parameter and col_name as our subject
     * http://php.net/manual/en/function.preg-match.php
     *
     * @param $atts
     * @param $array
     *
     * @return string
     * @throws \Exception
     */
    public function sc_pregMatch($atts, $array = null)
    {
        $atts = filterAndSetParams([
            'pattern'    => null,   // The searched pattern inside the current subject
            'col_name'   => null,
            'col_number' => null,
            'in_case_sensitive' => null
        ], $atts);

        // Deductive condition for preg match
        $condition = false;

        if (!is_null($atts['pattern'])) {
            // extract the checked subject from the current row's values
            $subject = $this->sc_translated($atts, $array);

            $condition  = preg_match('/' . $atts['pattern'] . '/', $subject) ? true : false;

            /*not case sensitive*/
            if(!empty($atts['in_case_sensitive'])){
                $condition  = preg_match('/' . $atts['pattern'] . '/i', $subject) ? true : false;
            }

        } else {
            $this->logger->warning("Short code " . $atts['pattern'] . " incorrect pattern param");
        }

        return $condition;
    }

    /**
     * pregReplace shortcode
     * Uses the preg_replace native function for regex.
     * Documentation: http://php.net/manual/en/function.preg-replace.php
     *
     * @param $atts
     * @param null $array
     * @return null|string|string[]
     */
    public function sc_pregReplace($atts, $array = null)
    {
        $atts = filterAndSetParams([
            'pattern'    => null,   // The searched pattern inside the current subject
            'col_name'   => null,
            'col_number' => null,
            'replacement' => null // The value you wish to replace the pattern matches with
        ], $atts);

        $result = '';

        // Validates the pattern exists
        if (is_null($pattern = $atts['pattern'])) {
            return $result;
        }

        // Parses the translated value
        $subject = $this->sc_translated($atts, $array);
        // Uses the preg_replace function according to the $pattern variable
        $result = preg_replace('/' . $pattern . '/', $atts['replacement'] ?? '', $subject);

        return $result ?? '';
    }

    /**
     * Short Code Sub String
     * Which uses the standard substr params
     * http://php.net/manual/en/function.substr.php
     *
     * @param $atts
     * @param $array
     *
     * @return string
     * @throws \Exception
     */
    public function sc_substr($atts, $array = null)
    {
        $atts = filterAndSetParams([
            'start' => null,
            // The input string. Must be one character or longer.

            'length' => null,
            // If start is non-negative, the returned string will start at the start'th position in string,
            // counting from zero. For instance, in the string 'abcdef',
            // the character at position 0 is 'a', the character at position 2 is 'c', and so forth.

            'col_name'   => null,
            'col_number' => null
        ], $atts);

        if (!is_null($atts['start'])) {
            $string = $this->sc_translated($atts, $array);
            if (!is_null($atts['length'])) {
                $result = substr($string, $atts['start'], $atts['length']);
            } else {
                $result = substr($string, $atts['start']);
            }
        } else {
            $this->logger->warning("Short code " . $atts['raw'] . " incorrect start param");
        }

        return $result;
    }

    /**
     * Short Code Weebly Hash
     * Generates signature hash for Weebly API
     *
     * @return string
     */
    public function sc_weeblyHash()
    {
        // taken from weebly site
        $api_token      = "FhcPINPzUvDSibHv";
        $api_secret_key = "XJv3za0j6LPkwk6lDZf6th2a2QQzzr5w";

        // because we need the same time stamp here and in fetcher x-ShareASale-Authentication field
        // we do this trick: generating time stamp without seconds and then concatenating with
        // constant number of seconds in both places
        $my_time_stamp = date('D, d M Y H:i') . ':00 -0400';
        $action_verb   = "activity";

        $sig = $api_token . ':' . $my_time_stamp . ':' . $action_verb . ':' . $api_secret_key;

        $sig_hash = hash("sha256", $sig);

        return $sig_hash;
    }

    /**
     * Short Code Weebly Hash
     * Generates signature hash for Weebly API
     * New one for the new script of ShareAsale
     * @return string
     */
    public function sc_weeblyHashNew()
    {
        // taken from weebly site
        $api_token      = "sLWVKTRXssK0ecMr";
        $api_secret_key = "BHo3bz7u9EHrfd3hBFb8gj8k9OQnmc9s";

        // because we need the same time stamp here and in fetcher x-ShareASale-Authentication field
        // we do this trick: generating time stamp without seconds and then concatenating with
        // constant number of seconds in both places
        $my_time_stamp = date('D, d M Y H:i') . ':00 -0400';
        $action_verb   = "activity";

        $sig = $api_token . ':' . $my_time_stamp . ':' . $action_verb . ':' . $api_secret_key;

        $sig_hash = hash("sha256", $sig);

        return $sig_hash;
    }

    /**
     * Short Code Weebly Hash
     * Generates signature hash for Weebly API
     * New second one for the new script of ShareAsale
     * @return string
     */
    public function sc_weeblyHashNewSecond()
    {
        // taken from weebly site
        $api_token      = "iJtO69F33nw1S5AM";
        $api_secret_key = "CXe2ju0e1FQrrj2sZFi0av8p7NXvcp7w";

        // because we need the same time stamp here and in fetcher x-ShareASale-Authentication field
        // we do this trick: generating time stamp without seconds and then concatenating with
        // constant number of seconds in both places
        $my_time_stamp = date('D, d M Y H:i') . ':00 -0400';
        $action_verb   = "activity";

        $sig = $api_token . ':' . $my_time_stamp . ':' . $action_verb . ':' . $api_secret_key;

        $sig_hash = hash("sha256", $sig);

        return $sig_hash;
    }

    /**
     * Short Code Zanox Hash
     * Generates signature hash for Zanox API
     * documentation at:
     * https://developer.zanox.com/web/guest/authentication/zanox-oauth/oauth-rest
     *
     * @param $atts
     *
     * @return string
     */
    public function sc_zanoxHash($atts)
    {
        $atts = filterAndSetParams([
            'days_num' => '', // number of days before today, used in $date and for $nonce suffix
            'type'     => 'leads', // sales or leads
        ], $atts);

        $secretKey = '3c56baeB251144+c9f94110f8e0b1c/18a047444';

        $http_verb  = 'GET';
        $date       = date('Y-m-d', strtotime('-' . $atts['days_num'] . " days"));
        $uri        = '/reports/' . $atts['type'] . '/date/' . $date;
        $time_stamp = date('D, d M Y H:i') . ':00 GMT';

        // unique random string, generated at the time of request, valid once, 20 or more characters
        $nonce = date('lldMYHi') . $atts['days_num'];

        $string_to_sign = mb_convert_encoding($http_verb . $uri . $time_stamp . $nonce, 'UTF-8');

        return base64_encode(hash_hmac('sha1', $string_to_sign, $secretKey, true));
    }

    /**
     * Resolves a condition in relation to $param and $compare_to
     *
     * @param $param
     * @param $condition
     * @param $compare_to
     * @param $col_name
     *
     * @return bool
     * @throws \Exception
     */
    private function resolveCondition($param, $condition, $compare_to,$col_name)
    {
        if (in_array($condition, ['=', '==', '<=', '>=', '>', '<', '!=', '!==', '===','%%','!%','%!'])) {
            switch ($condition) {
                case in_array($condition, ['=', '==']):
                    return $param == $compare_to;
                case "===":
                    return $param === $compare_to;
                case "!=":
                    return $param != $compare_to;
                case '!==':
                    return $param !== $compare_to;
                case ">=":
                    return $param >= $compare_to;
                case "<=":
                    return $param <= $compare_to;
                case ">":
                    return $param > $compare_to;
                case "<":
                    return $param < $compare_to;
                case '%%':
                    return $col_name.' LIKE "%'.$compare_to .'%"' ;
                case '!%':
                    return $col_name.' LIKE "'.$compare_to .'%"' ;
                case '%!':
                    return $col_name.' LIKE "%'.$compare_to .'"' ;
                default:
                    return false;
            }
        } else {
            throw new \Exception("** Fatal error" . __CLASS__ . '@' . __FUNCTION__ . '():' . __LINE__ . ' Invalid condition operator.');
        }
    }
}