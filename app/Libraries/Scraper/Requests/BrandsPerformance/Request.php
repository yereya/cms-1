<?php namespace App\Libraries\Scraper\Requests\BrandsPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Provider;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 2;

    /**
     * @var array $providers
     */
    protected $providers = [
        'fetcher' => [
            'class' => Provider::class,
        ]
    ];
}