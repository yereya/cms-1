<?php namespace App\Libraries\Scraper\Requests\ChangesLog;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\ChangesLog\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\ChangesLog\Providers\Bing\Translators\BIPublishersReport as BingBIPublishersReport;
use App\Libraries\Scraper\Requests\ChangesLog\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\ChangesLog\Providers\Google\Translators\BIPublishersReport as GoogleBIPublishersReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 23;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleBIPublishersReport::class
            ]
        ],
        'bing'   => [
            'class'       => BingProvider::class,
            'translators' => [
                BingBIPublishersReport::class
            ]
        ]
    ];
}