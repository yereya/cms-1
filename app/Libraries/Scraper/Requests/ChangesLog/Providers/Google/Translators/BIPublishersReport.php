<?php namespace App\Libraries\Scraper\Requests\ChangesLog\Providers\Google\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Google\Translators
 */
class BIPublishersReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    protected $report_id = 8;

    /**
     * @var string $table_name
     */
    protected $table_name = 'changes_scraper';
}