<?php namespace App\Libraries\Scraper\Requests\ChangesLog\Providers\Google;

use App\Entities\Models\Bo\ReportFieldSource;
use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\Selector;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'KEYWORDS_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 8;

    /**
     * @var string
     */
    protected $report_folder_name = 'ChangesLog';

    /**
     * @var string
     */
    protected $store_method = 'insert';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));
        $selector->setPredicates([new Predicate('AdNetworkType1', PredicateOperator::EQUALS, ['SEARCH'])]);

        return $selector;
    }

    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)
            ->where('publisher_id', $this->publisher_id)
            ->where('status', true)
            ->get()
            ->pluck('source_name')
            ->filter()
            ->toArray();
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);
        $reportDefinition->setReportName('Keywords Performance Report #' . uniqid());
        $reportDefinition->setDateRangeType($this->date_range_type);
        $reportDefinition->setReportType($this->report_type);
        $reportDefinition->setDownloadFormat('CSV');

        return $reportDefinition;
    }
}