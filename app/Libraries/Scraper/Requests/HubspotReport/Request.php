<?php namespace App\Libraries\Scraper\Requests\HubspotReport;

use App\Libraries\Scraper\Requests\HubspotReport\Providers\Hubspot\Provider as HubspotProvider;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\HubspotReport\Providers\Hubspot\Translators\BIPublishersReport as HubspotBIPublishersReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 122;

    /**
     * @var array $providers
     */
    protected $providers = [
        'hubspot' => [
            'class'       => HubspotProvider::class,
            'translators' => [
                HubspotBIPublishersReport::class,
            ]
        ]
    ];
}