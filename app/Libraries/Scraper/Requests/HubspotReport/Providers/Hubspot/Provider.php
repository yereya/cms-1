<?php namespace App\Libraries\Scraper\Requests\HubspotReport\Providers\Hubspot;

use App\Libraries\Scraper\Providers\HubspotReport;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends HubspotReport
{
    /**
     * @var int
     */
    protected $report_id = 35;

    /**
     * @var string $report_type
     */
    protected $report_type = 'HUBSPOT_REPORT';

    /**
     * @var string $report_request
     */

    /**
     * @var string
     */
    protected $report_folder_name = 'HubspotReport';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

}