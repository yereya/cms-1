<?php namespace App\Libraries\Scraper\Requests\ShareOfVoice;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\ShareOfVoice\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\ShareOfVoice\Providers\Bing\Translators\ShareOfVoiceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 116;

    /**
     * @var array $providers
     */
    protected $providers = [

        'bing'   => [
            'class'       => BingProvider::class,
            'translators' => [
                ShareOfVoiceReport::class,
            ]
        ]
    ];
}