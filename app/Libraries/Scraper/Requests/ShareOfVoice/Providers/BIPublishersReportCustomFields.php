<?php namespace app\Libraries\Scraper\Requests\ShareOfVoice\Providers;


use App\Entities\Models\Mrr\MrrFactPublisher;
use App\Libraries\Scraper\Translator;


class BIPublishersReportCustomFields extends Translator
{

    /**
     * Build Rows
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        $row_count = 0; // Count how many rows exists in mrr_fact_publisher
        foreach ($stream as $row) {
            $row_count += 1;
            $new_row = $this->prepareRow($row);
            # Check if row exists in DB
            $exists = MrrFactPublisher::
                  WHERE('account_id','=',$row['AccountId'])
                ->WHERE('stats_date_tz','=',$row['TimePeriod'])
                ->WHERE('keyword_id','=',$row['KeywordId'])
                ->WHERE('ad_group_id','=',$row['AdGroupId'])
                ->WHERE('device','=', $new_row['device'])
                ->FIRST();
            if($exists){
                $rows[] = $new_row;
            }
        }
        $this->logger->info('buildRows - found '.count($rows).' rows in mrr_fact_publisher out of '.$row_count);
        return $rows;
    }


    /**
     * Prepare Row
     *
     * @param array $row
     *
     * @return mixed
     */
    private function prepareRow($row)
    {
        # Get keys based on bo.report_fields
        $row = $this->getRowValues($row);

        return $row;
    }


}