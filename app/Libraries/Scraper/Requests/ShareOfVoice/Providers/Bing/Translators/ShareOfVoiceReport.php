<?php namespace App\Libraries\Scraper\Requests\ShareOfVoice\Providers\Bing\Translators;

use App\Libraries\Scraper\Requests\ShareOfVoice\Providers\BIPublishersReportCustomFields;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\ShareOfVoice\Providers\Bing\Translators
 */
class ShareOfVoiceReport extends BIPublishersReportCustomFields
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_fact_publishers';

    /**
     * @var int $report_id
     */
    protected $report_id = 27;

    /**
     * @var string $provider
     */
    protected $provider = 'Bing';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'source_report_uid',
            'report_id',
            'publisher_id',
            'ad_group_id',
            'device',
            'stats_date_tz',

            // Should not be updated
            'sid',
            'destination_url',
            'max_cpc',
            'quality_score',

            'keyword_id',
            'keyword_name',
            'ad_id',
            'report_name',
            'publisher_name',
            'account_id',
            'keyword_name',
            'account_name',
            'campaign_id',
            'campaign_name',
            'ad_group_name',
            'keyword_status',
            'clicks',

        ];
    }
}










