<?php namespace App\Libraries\Scraper\Requests\ShareOfVoice\Providers\Bing;

use App\Libraries\Scraper\Providers\BingReport;


use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\ShareOfVoiceReportRequest;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{
    /**
     * @var int
     */
    protected $report_id = 27;

    /**
     * @var string $report_type
     */
    protected $report_type = 'SHARE_OF_VOICE_REPORT';

    /**
     * @var string $report_request
     */
    protected $report_request = ShareOfVoiceReportRequest::class;

    /**
     * @var string
     */
    protected $report_folder_name = 'ShareOfVoice';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Share Of Voice Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Daily;
    }
}



