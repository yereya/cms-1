<?php namespace App\Libraries\Scraper\Requests\GeoPerformance\Providers\BingGNG\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class GeoPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\GeoPerformance\Providers\Bing\Translators
 */
class GeoPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_geo_performance';

    /**
     * @var string $provider
     */
    protected $provider = 'BingGNG';

    /**
     * @var int $report id
     */
    protected $report_id = 13;

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'account_id',
            'campaign_id',
            'device',
            'city',
            'region',
            'country',
            'stats_date_tz'
        ];
    }
}

