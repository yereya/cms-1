<?php namespace App\Libraries\Scraper\Requests\GeoPerformance\Providers\GoogleGNG;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class ConversionProvider extends Provider
{
    /**
     * @var int $report_id
     */
    protected $report_id = 34;

    /**
     * @var string
     */
    protected $report_folder_name = 'GeoConversionsPerformance';
}
