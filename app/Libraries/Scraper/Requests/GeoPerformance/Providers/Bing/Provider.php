<?php namespace App\Libraries\Scraper\Requests\GeoPerformance\Providers\Bing;

use App\Libraries\Scraper\Providers\BingReport;

use Microsoft\BingAds\V13\Reporting\UserLocationPerformanceReportRequest;

use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'GEO_PERFORMANCE_REPORT';

    /**
     * @var int $report_id
     */
    protected $report_id = 13;

    /**
     * @var string $report_request
     */
    protected $report_request = UserLocationPerformanceReportRequest::class;

    /**
     * @var string $report_folder_name
     */
    protected $report_folder_name = 'GeoPerformance';

    /**
     * @var string $store_method
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'User Location Performance Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Daily;
    }
}
