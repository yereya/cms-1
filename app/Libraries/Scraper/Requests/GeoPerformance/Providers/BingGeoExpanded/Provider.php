<?php namespace App\Libraries\Scraper\Requests\GeoPerformance\Providers\BingGeoExpanded;

use App\Libraries\Scraper\Providers\BingReport;

use Microsoft\BingAds\V13\Reporting\GeographicPerformanceReportColumn;

use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\GeographicPerformanceReportRequest;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'GEO_EXPANDED_PERFORMANCE_REPORT';

    /**
     * @var int $report_id
     */
    protected $report_id = 39;

    /**
     * @var string $report_request
     */
    protected $report_request = GeographicPerformanceReportRequest::class;

    /**
     * @var string $report_folder_name
     */
    protected $report_folder_name = 'BingGeoPerformance';

    /**
     * @var string $store_method
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Geographic Performance Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Daily;
    }
}


