<?php namespace App\Libraries\Scraper\Requests\GeoPerformance\Providers\Google;

use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'GEO_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 13;

    /**
     * @var string
     */
    protected $report_folder_name = 'GeoPerformance';

    /**
     * @var string $store_method
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);
        $reportDefinition->setReportName('Geo Performance Report #' . uniqid());
        $reportDefinition->setDateRangeType($this->date_range_type);
        $reportDefinition->setReportType($this->report_type);
        $reportDefinition->setDownloadFormat('CSV');

        return $reportDefinition;
    }
}