<?php namespace App\Libraries\Scraper\Requests\AnalyticsPerformance\Providers\Google;


use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google_Client;
use Google_Service_Analytics;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_GetReportsRequest;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_ReportRequest;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Requests\AnalyticsConversions\Providers\Google
 */
class Provider extends GoogleReport
{
    protected $report_type = "ANALYTICS_PERFORMANCE_REPORT";

    protected $report_id = 31;

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * @var array $data - store the data
     */
    protected $data  = [];
    /**
     * @var array $views - store views' ids for web sites
     */
    protected $views = [];

    /**
     * Provides the required fetch method for Request class's "run" method
     */
    public function fetch()
    {
        $client = $this->initializeAnalytics();
        $this->setViewsList($client);
        $response = $this->getReportAnalytics($client);
        $this->saveResults($response);
    }
    /**
     * Get all the views for the account
     */
    private function setViewsList($client)
    {
        $analytics = new Google_Service_Analytics($client);
        $accounts = $analytics->management_accounts->listManagementAccounts();
        $views = [];
        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            $firstAccountId = $items[0]->getId();
            // Get the list of properties for the authorized user.
            $properties = $analytics->management_webproperties
                ->listManagementWebproperties($firstAccountId);
            foreach ($properties->getItems() as $profile) {
                $row['name'] = $profile['name'];
                $row['view_id'] = $profile['defaultProfileId'];
                $row['UA'] = $profile['id'];
                $views[] = $row;
            }
        }

        $this->views = $views;

        return $views;
    }
    /**
     * Initialize analytics object
     */
    private function initializeAnalytics()
    {

        $KEY_FILE_LOCATION = config_path() . '/google_analytics_nth-canto-236308-909ac8ce3225.json';
        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("Hello Analytics Reporting");
        $client->setAuthConfig($KEY_FILE_LOCATION);
        $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
        return $client;
    }
    /**
     * Get data for all the views
     */
    private function getReportAnalytics($client)
    {

        $analytics = new Google_Service_AnalyticsReporting($client);

        $batchs = []; // Used to store the returned data

        foreach ($this->views as $view) {

            $next_page_token = "0"; // Used for pagination

            while($next_page_token !== null){

                $view_id = $view['view_id'];
                // Create the DateRange object.
                $dateRange = new Google_Service_AnalyticsReporting_DateRange();
                // Set dates
                $from = $this->options['date-range-start'];
                $to = $this->options['date-range-end'];
                $dateRange->setStartDate($from);
                $dateRange->setEndDate($to);

                // Create the Metrics object.
                $sessions = new Google_Service_AnalyticsReporting_Metric();
                $sessions->setExpression("ga:sessions");
                $sessions->setAlias("sessions");

                $sourceMedium = new Google_Service_AnalyticsReporting_Dimension();
                $sourceMedium->setName("ga:sourceMedium");

                $dimension3 = new Google_Service_AnalyticsReporting_Dimension();
                $dimension3->setName("ga:dimension3");

                // Create the ReportRequest object.
                $request = new Google_Service_AnalyticsReporting_ReportRequest();
                $request->setViewId($view_id);
                $request->setDateRanges($dateRange);
                $request->setMetrics(array($sessions));
                $request->setDimensions(array($dimension3, $sourceMedium));
                $request->setPageSize(10000);
                $request->setPageToken($next_page_token);

                $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
                $body->setReportRequests(array($request));

                try{
                    $row = $view; // Keep the info about this view
                    $row['data'] = $analytics->reports->batchGet( $body ); // Store the data
                    $next_page_token = $row['data']->getReports()[0]->getNextPageToken(); // Check if there is another page
                    $batchs[] = $row;

                }catch (\Exception $e){   // If there is no data for the specific view
                    $next_page_token = null;
                    $this->logger->warning('Error processing the following view - Name : '.$view['name']. ', view_id : '. $view['view_id'] .', UA : '. $view['UA'] );
                }
            }
        }
        return $batchs;
    }

    /**
     * Parse and save the results in $this->data
     *
     */
    function saveResults($reports)
    {
        foreach ($reports as $single_report) {
            $data = $single_report['data'];
            for ($reportIndex = 0; $reportIndex < count($data); $reportIndex++) {
                $array_row = [];
                $report = $data[$reportIndex];
                $header = $report->getColumnHeader();
                $dimensionHeaders = $header->getDimensions();
                $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
                $rows = $report->getData()->getRows();

                for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                    $row = $rows[$rowIndex];
                    $dimensions = $row->getDimensions();
                    $metrics = $row->getMetrics();
                    for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                        $array_row[$dimensionHeaders[$i]] = $dimensions[$i];
                        print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
                    }

                    for ($j = 0; $j < count($metrics); $j++) {
                        $values = $metrics[$j]->getValues();
                        for ($k = 0; $k < count($values); $k++) {
                            $entry = $metricHeaders[$k];
                            $array_row[$entry->getName()] = $values[$k];
                            print($entry->getName() . ": " . $values[$k] . "\n");
                        }
                    }
                    $array_row['name'] = $single_report['name'];
                    $array_row['view_id'] = $single_report['view_id'];
                    $array_row['UA'] = $single_report['UA'];
                    $this->data[$array_row['view_id']][] = $array_row;
                }
            }
         }
    }
    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {

    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {

    }
}