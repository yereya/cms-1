<?php namespace App\Libraries\Scraper\Requests\AnalyticsPerformance\Providers\Google\Translators;

use App\Libraries\Scraper\Translator;
use App\Facades\ShortCode;

class PerformanceReport extends Translator
{
    /**
     * @var int $report_id
     */
    protected $report_id = 31;

    /**
     * @var string $provider
     */
    protected $provider = 'Google';

    /**
     * @var string
     */
    protected $connection = 'mrr';

    /**
     * @var string
     */
    protected $table_name = 'mrr_analytics_performance';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'site_ua',
            'view_id',
            'gid',
            'source_medium'
        ];
    }


    /**
     * Store
     *
     * @return void
     */
    public function store()
    {
        if (!$this->data) {
            return;
        }
        # Each $request represents a website ( view_id )
        foreach ($this->data as $request) {
            $this->storeRequest($request);
        }
    }

    /**
     * Store Request
     *
     * @param array $request
     *
     * @return array
     */
    protected function storeRequest($request)
    {
        $store_method = 'store_' . $this->options['store_method'];
        $rows = $this->buildRows($request);
        $this->$store_method($rows);
    }

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            if (is_null($row)) {
                continue;
            }
            $rows[] = $this->getRowValues($row);
        }
        return $rows;
    }

    /**
     * Get Row Value
     *
     * @param array $row
     *
     * @return array
     */
    protected function getRowValues($row)
    {
        $new_row = [];
        $fields  = $this->getReportFields();

        foreach ($fields as $field) {
            $new_row[$field->name] = ShortCode::setShortCodes($field->value, $row);
        }
        return $new_row;
    }
}

