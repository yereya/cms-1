<?php namespace App\Libraries\Scraper\Requests\AnalyticsPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\AnalyticsPerformance\Providers\Google\Provider;
use App\Libraries\Scraper\Requests\AnalyticsPerformance\Providers\Google\Translators\PerformanceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 120;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => Provider::class,
            'translators' => [
                PerformanceReport::class,
            ]
        ]
    ];
}


