<?php


namespace App\Libraries\Scraper\Requests\AdPerformance\Providers\Google;


class DsaProvider extends DisplayProvider
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'AD_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 38; // DSA_AD_PERFORMANCE_REPORT

}
