<?php namespace App\Libraries\Scraper\Requests\AdPerformance\Providers\Bing;

use App\Libraries\Scraper\Providers\BingReport;

use Microsoft\BingAds\V13\Reporting\AdPerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;

/**
 * Class DisplayProvider
 *
 * @package App\Libraries\Scraper\Providers
 */
class DisplayProvider extends BingReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'AD_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 2;

    /**
     * @var string $report_request
     */
    protected $report_request = AdPerformanceReportRequest::class;

    /**
     * @var string
     */
    protected $report_folder_name = 'AdPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Ad Performance Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Daily;
    }
}