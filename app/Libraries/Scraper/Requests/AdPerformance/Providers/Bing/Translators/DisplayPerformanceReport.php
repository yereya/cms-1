<?php namespace App\Libraries\Scraper\Requests\AdPerformance\Providers\Bing\Translators;


use App\Libraries\Scraper\Translator;

/**
 * Class AdDisplayPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\AdPerformance\Providers\Bing\Translators
 */

class DisplayPerformanceReport extends Translator
{
    /**
 * @var string $connection
 */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_fact_publishers';

    /**
     * @var int $report_id
     */
    protected $report_id = 2;

    /**
     * @var string $provider
     */
    protected $provider = 'Bing';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'report_id',
            'publisher_id',
            'account_id',
            'ad_id',
            'stats_date_tz',
            'device'
        ];
    }

}


