<?php

namespace App\Libraries\Scraper\Requests\AdPerformance\Providers\Facebook;

use App\Libraries\Scraper\Providers\FacebookReport;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends FacebookReport
{
    /**
     * @var int
     */
    protected $report_id = 16;

    /**
     * @var string $report_type
     */
    protected $report_type = 'AD_PERFORMANCE_REPORT';

    /**
     * @var string $report_request
     */

    /**
     * @var string
     */
    protected $report_folder_name = 'FacebookAdPerformanceReport';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

}
