<?php


namespace App\Libraries\Scraper\Requests\AdPerformance\Providers\Facebook\Translators;

use App\Entities\Models\Bo\ReportField;
use App\Libraries\Scraper\Translator;
use Illuminate\Support\Collection;
use App\Facades\ShortCode;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\FacebookReport\Providers\Facebook\Translators
 */
class BIPublishersReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_ad_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 16;

    /**
     * @var string $provider
     */
    protected $provider = 'Facebook';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'source_report_uid',
            'report_id',
            'publisher_id',
            'ad_group_id',
            'device',
            'stats_date_tz',

        ];
    }
    public function store()
    {
        if (!$this->data) {
            return;
        }
        $rows = $this->buildRows($this->data);

        $store_method = 'store_' . $this->options['store_method'];
        $this->$store_method($rows);

    }
    /**
     * Build Rows
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($data)
    {
        $rows = [];
        foreach ($data as $ad_account) {
            foreach ($ad_account as $ad_id => $ad_obj) {  // Each ad_obj contains an array of data for each day in the range
                foreach ($ad_obj as $key_idx => $row){
                    $rows[] = $this->prepareRow($row);
                }
            }
        }
        return $rows;
    }

    /**
     * Prepare Row
     *
     * @param array $row
     *
     * @return mixed
     */
    private function prepareRow($row)
    {
        $fields  = $this->getReportFields();
        $new_row = [];
        foreach ($fields as $field) {
            $new_row[$field->name] = ShortCode::setShortCodes($field->value, $row);
        }
        $new_row = $this->setDeviceField($new_row);

        return $new_row;
    }
    /**
     * Set Device Field
     *
     * @return array
     */
    protected function setDeviceField($new_row)
    {
        # ---- Set Device field based on campaign name ----
        if(!isset($new_row['ad_group_name']))
            return $new_row;
        $exploded_ad_group_name = implode(' ',explode('_',$new_row['ad_group_name']));
        if(strpos($exploded_ad_group_name,'Mobile')){
            $new_row['device'] = 'm';
        }
        if(strpos($exploded_ad_group_name,'Desktop')){
            $new_row['device'] = 'c';
        }
        if (strpos($exploded_ad_group_name,'All')){
            $new_row['device'] = 'a';
        }
        return $new_row;
    }
    /**
     * Get Report Fields
     *
     * @return Collection
     */
    protected function getReportFields()
    {
        if (!isset($this->fieldRows) || (!isset($this->current_report) || $this->current_report != $this->report_id)) {
            $this->current_report = $this->report_id;
            $this->fieldRows      = ReportField::where('report_id', $this->report_id)
                ->where('provider', $this->provider)->get();
        }

        return $this->fieldRows;
    }


}



