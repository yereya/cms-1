<?php namespace App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG\Translators;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountReportField;
use App\Libraries\Scraper\Translator;
use Cache;
use Carbon\Carbon;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\AdPerformance\Providers\Google\Translators
 */
class BIPublishersReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_fact_publishers';

    /**
     * @var int $report_id
     */
    protected $report_id = 2;

    /**
     * @var string $provider
     */
    protected $provider = 'GoogleGNG';

    /**
     * @var string $cache_key
     */
    private $cache_key = 'networks_report_2';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'source_report_uid',
            'report_id',
            'publisher_id',
            'ad_group_id',
            'device',
            'stats_date_tz',

            // Should not be updated
            'sid',
            'destination_url',
            'max_cpc',
            'quality_score',
        ];
    }

    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            $rows[] = $this->prepareRow($row);;
        }

        return $rows;
    }

    private function prepareRow($row)
    {
        $row = $this->getRowValues($row);
        $this->getNetworkNetRevenue($row);
        arrayRemoveFields($row, ['FinalUrls', 'TrackingUrlTemplate']);

        return $row;
    }

    /**
     * Get Network Revenue value
     *
     * @param $row
     *
     * @return mixed
     */
    protected function getNetworkNetRevenue(&$row)
    {
        $networks = $this->getNetworkCache($this->cache_key);

        if (isset($networks[$row['account_id']])) {
            return $row['network_net_revenue'] = $row['cost'] * $networks[$row['account_id']];
        }

        return $row['network_net_revenue'] = 0;
    }

    /**
     * Get Network Revenue Cache
     *
     * array [
     *     source_account_id => value,
     *     ...
     * ]
     *
     * @param $key
     *
     * @return mixed|void
     */
    private function getNetworkCache($key)
    {
        return !empty(Cache::get($key)) ? Cache::get($key) : $this->createNetworkCache($key);
    }

    /**
     * Create Network Revenue Cache if not exists
     *
     * @param $key
     *
     * @return mixed
     */
    private function createNetworkCache($key)
    {
        //get all active accounts google and bing - to Cache
        $accounts = Account::whereHas('advertiser', function ($query) {
            $query->where('advertisers.status', 'active');
        })->whereHas('publisher', function ($query) {
            $query->whereIn('publishers.id', ['95', '97','163','164']);
        })->with('fields')->get();

        $networks = []; //build array [account_id => account_reports_id]
        foreach ($accounts as $account) {
            $networks[$account->source_account_id] = $account->id;
        }

        $values = $this->fetchValues(cleanArray($networks));

        Cache::put($key, $values, Carbon::now()->addMinutes(10));

        return Cache::get($key);
    }

    /**
     * Fetch values
     *
     * @param $ids
     *
     * @return array
     */
    private function fetchValues($ids)
    {
        $networks = AccountReportField::select('account_id', 'value', 'report_id')
                                      ->whereIn('account_id', array_values($ids))->where('report_id', $this->report_id)
                                      ->get()->keyBy('account_id')->toArray();

        $array_flip = array_flip($ids);

        $values = [];
        foreach ($networks as $key => $data) {
            $values[$array_flip[$key]] = $data['value'];
        }

        return $values;
    }
}
