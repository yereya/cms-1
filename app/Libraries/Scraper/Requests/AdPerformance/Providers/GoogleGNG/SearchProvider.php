<?php namespace App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG;

/**
 * Class SearchProvider
 *
 * @package App\Libraries\Scraper\Providers
 */
class SearchProvider extends DisplayProvider
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'AD_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 16;
}
