<?php namespace App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG;

use App\Libraries\Scraper\Providers\GoogleGNGReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class DisplayProvider extends GoogleGNGReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'AD_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 2;

    /**
     * @var string
     */
    protected $report_folder_name = 'AdPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $ReportDefinition = new ReportDefinition();
        $ReportDefinition->setSelector($selector);
        $ReportDefinition->setReportName('Ad Performance Report #' . uniqid());
        $ReportDefinition->setDateRangeType($this->date_range_type);
        $ReportDefinition->setReportType($this->report_type);
        $ReportDefinition->setDownloadFormat('CSV');

        return $ReportDefinition;
    }
}
