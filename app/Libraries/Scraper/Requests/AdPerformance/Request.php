<?php namespace App\Libraries\Scraper\Requests\AdPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
//Bing - Search
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Bing\SearchProvider as BingSearchProvider;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Bing\Translators\AdPerformanceReport as BingAdPerformanceReport;
//Bing - Display
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Bing\DisplayProvider as BingDisplayProvider;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Bing\Translators\DisplayPerformanceReport as BingAdDisplayPerformanceReport;
// Bing GNG
use App\Libraries\Scraper\Requests\AdPerformance\Providers\BingGNG\SearchProvider as BingGNGSearchProvider;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\BingGNG\Translators\AdPerformanceReport as BingGNGAdPerformanceReport;
// Google
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Google\DisplayProvider as GoogleDisplayProvider;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Google\SearchProvider as GoogleSearchProvider;

use App\Libraries\Scraper\Requests\AdPerformance\Providers\Google\DsaProvider as GoogleDsaProvider;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Google\Translators\DsaAdPerformanceReport as GoogleDsaAdPerformanceReport;

use App\Libraries\Scraper\Requests\AdPerformance\Providers\Google\Translators\AdPerformanceReport as GoogleAdPerformanceReport;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Google\Translators\BIPublishersReport as GoogleBIPublishersReport;
// GNG
use App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG\DisplayProvider as GoogleGNGDisplayProvider;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG\SearchProvider as GoogleGNGSearchProvider;

use App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG\DsaProvider as GoogleGNGDsaProvider;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG\Translators\DsaAdPerformanceReport as GoogleGNGDsaAdPerformanceReport;

use App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG\Translators\AdPerformanceReport as GoogleGNGAdPerformanceReport;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\GoogleGNG\Translators\BIPublishersReport as GoogleGNGBIPublishersReport;
// Facebook
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Facebook\Provider as FacebookProvider;
use App\Libraries\Scraper\Requests\AdPerformance\Providers\Facebook\Translators\BIPublishersReport as FacebookBIPublishersReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 27;

    /**
     * @var array $providers
     */
    protected $providers = [
        'bing-search'    => [
            'class'       => BingSearchProvider::class,
            'translators' => [
                BingAdPerformanceReport::class
            ]
        ],
        'bing-display'    => [
            'class'       => BingDisplayProvider::class,
            'translators' => [
                BingAdDisplayPerformanceReport::class
            ]
        ],
        'bing-GNG-search'    => [
            'class'       => BingGNGSearchProvider::class,
            'translators' => [
                BingGNGAdPerformanceReport::class
            ]
        ],
        'google-display' => [
            'class'       => GoogleDisplayProvider::class,
            'translators' => [
                GoogleBIPublishersReport::class
            ]
        ],
        'google-search'  => [
            'class'       => GoogleSearchProvider::class,
            'translators' => [
                GoogleAdPerformanceReport::class
            ]
        ],
        'google-dsa'  => [
            'class'       => GoogleDsaProvider::class,
            'translators' => [
                GoogleDsaAdPerformanceReport::class
            ]
        ],
          'googleGNG-display' => [
            'class'       => GoogleGNGDisplayProvider::class,
            'translators' => [
                GoogleGNGBIPublishersReport::class
            ]
        ],
        'googleGNG-search'  => [
            'class'       => GoogleGNGSearchProvider::class,
            'translators' => [
                GoogleGNGAdPerformanceReport::class
            ]
        ],
        'googleGNG-dsa'  => [
            'class'       => GoogleGNGDsaProvider::class,
            'translators' => [
                GoogleGNGDsaAdPerformanceReport::class
            ]
        ],
        'facebook'  => [
            'class'       => FacebookProvider::class,
            'translators' => [
                FacebookBIPublishersReport::class
            ]
        ],

    ];
}
