<?php namespace App\Libraries\Scraper\Requests\TaboolaReport\Providers\Taboola;

use App\Libraries\Scraper\Providers\TaboolaReport;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends TaboolaReport
{
    /**
     * @var int
     */
    protected $report_id = 37;

    /**
     * @var string $report_type
     */
    protected $report_type = 'TABOOLA_REPORT';

    /**
     * @var string $report_request
     */

    /**
     * @var string
     */
    protected $report_folder_name = 'TaboolaReport';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

}
