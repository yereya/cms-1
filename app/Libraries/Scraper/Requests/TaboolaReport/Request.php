<?php namespace App\Libraries\Scraper\Requests\TaboolaReport;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\TaboolaReport\Providers\Taboola\Provider as TaboolaProvider;
use App\Libraries\Scraper\Requests\TaboolaReport\Providers\Taboola\Translators\BIPublishersReport as TaboolaBIPublishersReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 126;

    /**
     * @var array $providers
     */
    protected $providers = [
        'taboola' => [
            'class'       => TaboolaProvider::class,
            'translators' => [
                TaboolaBIPublishersReport::class,
            ]
        ]
    ];
}
