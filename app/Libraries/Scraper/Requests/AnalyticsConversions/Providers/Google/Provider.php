<?php namespace App\Libraries\Scraper\Requests\AnalyticsConversions\Providers\Google;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AdvertiserAnalytics;
use App\Entities\Models\Bo\Conversion;
use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use TheIconic\Tracking\GoogleAnalytics\Analytics;
use TheIconic\Tracking\GoogleAnalytics\AnalyticsResponse;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Requests\AnalyticsConversions\Providers\Google
 */
class Provider extends GoogleReport
{
    protected $report_type = "ANALYTICS_CONVERSION_UPLOAD";

    protected $report_id = 25;

    /**
     * @var Collection $conversions - Collections of conversions
     *
     * example: [{source_account_id} => [{values}], {...} => [...]]
     */
    private $conversions;

    /**
     * @var array $conversions_by_advertisers - array of conversions keyed by advertiser_id
     */
    private $conversions_by_advertisers = [];

    /**
     * @var array $brand_name_by_advertiser - [advertiser_id => brand_name]
     */
    private $brand_name_by_advertiser = [];

    /**
     * @var array $selected_keys - conversion keys to send to google
     */
    protected $selected_keys = ['track_id', 'clkid', 'price', 'timestamp', 'type', 'event', 'source_account_id'];

    /**
     * @var Analytics $analytics - The analytics library we will be using
     */
    protected $analytics;

    /**
     * @var array $selected_analytics_data - Conversions with only the selected keys
     */
    protected $selected_analytics_data = [];

    /**
     * @var int $num_correct_hits - number of correct hits
     */
    protected $num_correct_hits = 0;

    /**
     * @var array $event_vars - populates the google analytics event variables
     */
    protected $event_category = 'conversion';

    /**
     * Provides the required fetch method for Request class's "run" method
     */
    public function fetch()
    {
        $this->conversions = $this->getConversions();
        $this->validateConversions();
        $this->beginProcessExecution();
    }

    /**
     * Gets valid conversions
     *
     * @return mixed
     */
    private function getConversions()
    {
        return Conversion::where('source_account_id', '!=', 0)
            ->whereDate('created_at', '>=', '2018-01-01 12:00:00')
            ->where('ga_status', 0)->whereNull('error')->limit(200)
            ->get()->groupBy('source_account_id');
    }

    /**
     * Validates processed conversions
     */
    private function validateConversions()
    {
        $this->removeConversionsWithIncorrectAccounts();

        $this->pluckConversionsByAdvertiser();
    }

    /**
     * Filters accounts which don't belong to google
     * Remove conversions from accounts with faulty criterias
     *
     * @return array
     */
    private function removeConversionsWithIncorrectAccounts()
    {
        // source_account_id array
        $source_account_ids = $this->conversions->keys()->toArray();

        $invalid_accounts = Account::where('publisher_id', '<>', $this->publisher_id)
            ->whereIn('source_account_id', array_values($source_account_ids))->get()
            ->pluck('source_account_id')->filter();

        foreach ($invalid_accounts as $account) {
            $this->conversions->forget($account);
        }
    }

    /**
     * Groups conversions by advertiser id based on the source_account_id parameter
     */
    private function pluckConversionsByAdvertiser()
    {
        $conversions_by_advertisers = [];
        $brand_name_by_advertiser = [];
        // Transforms the conversions to an array
        $conversions_by_source = $this->conversions->toArray();


        // Retrieves accounts, keyed by their 'advertiser_id' for our valid source_account_ids
        $accounts_by_advertisers = DB::table('bo.accounts')
            ->whereIn('source_account_id', $this->conversions->keys())
            ->groupBy('advertiser_id')->get()->keyBy('advertiser_id');

        // Iterate over the accounts collection keyed by advertiser_id
        $accounts_by_advertisers->each(function ($account, $advertiser_id) use (&$conversions_by_advertiser, $conversions_by_source, &$brand_name_by_advertiser) {
            // Appends the conversion to its corresponding advertiser, by using the connected account's advertiser_id
            $conversions_by_advertiser += [$advertiser_id => $conversions_by_source[$account->source_account_id]];
            $brand_name_by_advertiser[$advertiser_id] = $account->name ?? 'invalid';
        });

        // Stores the advertiser-keyed conversions
        $this->conversions_by_advertisers = $conversions_by_advertisers;
        $this->brand_name_by_advertiser = $brand_name_by_advertiser;
    }

    /**
     * Handler for google analytics hit builder
     */
    private function beginProcessExecution()
    {
        // Build the conversion data we wish to push to analytics
        $this->buildAnalyticsGeneralSettings();

        // Prepares hits properties
        $this->populateAnalyticsHitData();

        // Commences the upload conversion process
        $this->startUploadConversionProcess();
    }

    /**
     * Creates an Analytics object containing the data to be sent to Google Analytics
     */
    private function buildAnalyticsGeneralSettings()
    {
        // Initializes Analytics library class
        $analytics = new Analytics();

        $analytics->setProtocolVersion('1')
            // Will add response parsing
            ->setDebug(true)
            // Will not wait for every transaction to complete before sending another
            // - Turn on only if needed!
//            ->setAsyncRequest(true)
            // Get an anonymous ip address to avoid ip filtering
            ->setAnonymizeIp('1')
            // Sets the click to be non interactive
            ->setNonInteractionHit('1')
            // Transaction(conversion) type hit
            ->setHitType('event')
            // Event variables
            ->setEventCategory($this->event_category);

        $this->analytics = $analytics;
    }

    /**
     * Populates the analytics array with our custom conversion data
     */
    private function populateAnalyticsHitData()
    {
        $analytics_hits_data = [];

        foreach ($this->conversions_by_advertisers as $advertiser_id => $conversions) {
            $analytics_hits_data[$advertiser_id] = array_map(function ($conversion) {
                // Converts conversions to array and returns the selected keys only to append to hits data
                return array_intersect_key((array) $conversion, array_flip($this->selected_keys));
            }, $conversions);
        }

        $this->selected_analytics_data = $analytics_hits_data;
    }

    /**
     * Uploads user transactions based on our conversions
     */
    private function startUploadConversionProcess()
    {
        foreach ($this->selected_analytics_data as $advertiser_id => $selected_conversions) {
            // Retrieve the analytics account model
            $analytics_model = AdvertiserAnalytics::where('advertiser_id', $advertiser_id)->get()->first() ?? null;

            if (is_null($analytics_model)) {
                $this->logger->warning("Could not find the google analytics account connected to advertiser id: " . $advertiser_id);

                continue;
            }

            $this->analytics
                // Account site id
                ->setTrackingId($analytics_model->ga_tracking_id)
                // View id
                ->setClientId($analytics_model->ga_client_id);

            foreach ($selected_conversions as $conv_idx => $current_selected_data) {
                // Send hit transaction for every final conversion
                $this->sendCurrentHitToAnalytics($current_selected_data, $advertiser_id);
            }
        }

        $this->logger->debug(["Count of valid hit results: " => $this->num_correct_hits]);
    }

    /**
     * Sends the selected current hit data to google analytics
     *
     * @param $current_selected_data
     * @param $advertiser_id
     */
    private function sendCurrentHitToAnalytics($current_selected_data, $advertiser_id)
    {
        $this->analytics->setDebug(false);

        // Send hit transaction for every final conversion
        $this->analytics
            // The linked adwords click id
            ->setGoogleAdwordsId($current_selected_data['clkid'])
            // Mentions the transaction price
            ->setItemPrice($current_selected_data['price'])
            // Our event type (the brand's name)
            ->setEventLabel($this->brand_name_by_advertiser[$advertiser_id])
            // Event action as our conversion type will mostly be sale/lead
            ->setEventAction($current_selected_data['event'])
            // Sends the processed data to google analytics
            ->sendEvent();

        $analytics_response = $this->analytics->setDebug(true)->sendEvent();
        // Handle the hit transaction
        $this->handleAnalyticsResponseDebugData($analytics_response);

        // Update conversion data status to parsed
        Conversion::where('track_id', $current_selected_data['track_id'])
            ->update(['ga_status' => 1]);
    }

    /**
     * Handles debug data
     *
     * @param AnalyticsResponse $analytics_response
     * @return bool|void
     */
    private function handleAnalyticsResponseDebugData($analytics_response)
    {
        $analytics_debugger_data = $analytics_response->getDebugResponse();
        // Debugger returned an empty object/array
        if (!$analytics_debugger_data) {
            // Log a warning - empty response object
            return;
        }

        foreach ($analytics_debugger_data['hitParsingResult'] ?? [] as $parsed_hit) {
            $parser_messages = $parsed_hit['parserMessage'] ?? [];

            // If a hit is valid we do not display messages
            if ($parsed_hit['valid'] || !$parser_messages) {
                $this->num_correct_hits++;

                continue;
            }

            // Logs the correct logger form per message
            foreach ($parser_messages as $parser_message) {
                $message_type = $parser_message['messageType'] ?? [];
                $message_description = $parser_message['description'] ?? 'No message to display';

                // A message type does not exist
                if (!$message_type) {
                    continue;
                }

                // Logs the correct message
                switch ($message_type) {
                    case 'INFO':
                        // Logs info
                        $this->logger->debug(['INFO message returned: ' => $message_description]);
                        break;
                    case 'WARN':
                        // Logs warn
                        $this->logger->warning('WARNING message returned: ' . $message_description);
                        break;
                    case 'ERROR':
                        // Logs error
                        $this->logger->error('ERROR message returned: ' . $message_description);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        // TODO: Implement setSelector() method.
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        // TODO: Implement setReportDefinition() method.
    }
}