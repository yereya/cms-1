<?php namespace App\Libraries\Scraper\Requests\AnalyticsConversions\Providers\GoogleGNG\Translators;

use App\Libraries\Scraper\Translator;

class Conversion extends Translator
{
    /**
     * @var int $report_id
     */
    protected $report_id = 25;

    /**
     * @var string $provider
     */
    protected $provider = 'GoogleGNG';
}
