<?php namespace App\Libraries\Scraper\Requests\AnalyticsConversions;

use App\Libraries\Scraper\Request as BaseRequest;
// Google
use App\Libraries\Scraper\Requests\AnalyticsConversions\Providers\Google\Provider;
use App\Libraries\Scraper\Requests\AnalyticsConversions\Providers\Google\Translators\Conversion;
// GNG
use App\Libraries\Scraper\Requests\AnalyticsConversions\Providers\GoogleGNG\Provider as GNGProvider;
use App\Libraries\Scraper\Requests\AnalyticsConversions\Providers\GoogleGNG\Translators\Conversion as GNGConversion;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_idGoogle
     */
    protected $logger_system_id = 103;

    // * * *
    // Deprecated, there is a new task for it - Libraries / GoogleAnalytics
    // * * *
    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => Provider::class,
            'translators' => [
                Conversion::class
            ]
        ],
//        'googleGNG' => [
//            'class'       => GNGProvider::class,
//            'translators' => [
//                GNGConversion::class
//            ]
//        ],
    ];
}
