<?php namespace App\Libraries\Scraper\Requests\SearchQueryPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Bing
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Bing\Translators\SearchQueryPerformanceReport as BingSearchQueryPerformanceReport;
// Bing GNG
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\BingGNG\Translators\SearchQueryPerformanceReport as BingGNGSearchQueryPerformanceReport;
// Google
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators\SearchQueryPerformanceReport as GoogleSearchQueryPerformanceReport;
// Google Conversions
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\ConversionProvider as GoogleConversionProvider;
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators\SearchQueryConversionPerformanceReport as GoogleSearchQueryConversionPerformanceReport;
// GNG
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\GoogleGNG\Translators\SearchQueryPerformanceReport as GoogleGNGSearchQueryPerformanceReport;
// GNG Conversions
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\GoogleGNG\ConversionProvider as GoogleGNGConversionProvider;
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\GoogleGNG\Translators\SearchQueryConversionPerformanceReport as GoogleGNGSearchQueryConversionPerformanceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 39;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google'            => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleSearchQueryPerformanceReport::class
            ]
        ],
        'google-conversion' => [
            'class'       => GoogleConversionProvider::class,
            'translators' => [
                GoogleSearchQueryConversionPerformanceReport::class
            ]
        ],
        'googleGNG'            => [
            'class'       => GoogleGNGProvider::class,
            'translators' => [
                GoogleGNGSearchQueryPerformanceReport::class
            ]
        ],
        'googleGNG-conversion' => [
            'class'       => GoogleGNGConversionProvider::class,
            'translators' => [
                GoogleGNGSearchQueryConversionPerformanceReport::class
            ]
        ],
        'bing'              => [
            'class'       => BingProvider::class,
            'translators' => [
                BingSearchQueryPerformanceReport::class
            ]
        ],
        'bingGNG'              => [
            'class'       => BingGNGProvider::class,
            'translators' => [
                BingGNGSearchQueryPerformanceReport::class
            ]
        ],
    ];
}
