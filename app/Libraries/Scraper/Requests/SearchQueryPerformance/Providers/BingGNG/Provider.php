<?php namespace App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\BingGNG;

use App\Libraries\Scraper\Providers\BingGNGReport;


use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\SearchQueryPerformanceReportRequest;

/**
 * Class DisplayProvider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingGNGReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'SEARCH_QUERY_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 15;

    /**
     * @var string $report_request
     */
    protected $report_request = SearchQueryPerformanceReportRequest::class;

    /**
     * @var string
     */
    protected $report_folder_name = 'SearchQueryPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Search Query Performance Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Daily;
    }
}


