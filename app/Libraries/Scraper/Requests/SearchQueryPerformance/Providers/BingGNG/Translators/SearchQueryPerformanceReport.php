<?php namespace App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\BingGNG\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class SearchQueryPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators
 */
class SearchQueryPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_search_query_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 15;

    /**
     * @var string $provider
     */
    protected $provider = 'BingGNG';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'report_id',
            'publisher_id',
            'ad_group_id',
            'keyword_id',
            'device',
            'query',
            'stats_date_tz'
        ];
    }
}
