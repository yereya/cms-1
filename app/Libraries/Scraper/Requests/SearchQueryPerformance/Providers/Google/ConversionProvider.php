<?php namespace App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class ConversionProvider extends Provider
{
    /**
     * @var int $report_id
     */
    protected $report_id = 17;
}