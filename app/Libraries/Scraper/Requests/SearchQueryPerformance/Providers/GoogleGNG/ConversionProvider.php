<?php namespace App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\GoogleGNG;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class ConversionProvider extends Provider
{
    /**
     * @var int $report_id
     */
    protected $report_id = 17;
}
