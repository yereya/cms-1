<?php namespace App\Libraries\Scraper\Requests\FacebookReport;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\FacebookReport\Providers\Facebook\Provider as FacebookProvider;
use App\Libraries\Scraper\Requests\FacebookReport\Providers\Facebook\Translators\BIPublishersReport as FacebookBIPublishersReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 112;

    /**
     * @var array $providers
     */
    protected $providers = [
        'facebook' => [
            'class'       => FacebookProvider::class,
            'translators' => [
                FacebookBIPublishersReport::class,
            ]
        ]
    ];
}