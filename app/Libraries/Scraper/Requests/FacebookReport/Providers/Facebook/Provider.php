<?php namespace App\Libraries\Scraper\Requests\FacebookReport\Providers\Facebook;

use App\Libraries\Scraper\Providers\FacebookReport;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends FacebookReport
{
    /**
     * @var int
     */
    protected $report_id = 26;

    /**
     * @var string $report_type
     */
    protected $report_type = 'FACEBOOK_REPORT';

    /**
     * @var string $report_request
     */

    /**
     * @var string
     */
    protected $report_folder_name = 'FacebookReport';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

}