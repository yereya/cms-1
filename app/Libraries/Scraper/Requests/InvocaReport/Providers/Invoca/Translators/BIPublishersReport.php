<?php


namespace App\Libraries\Scraper\Requests\InvocaReport\Providers\Invoca\Translators;

use App\Entities\Models\Bo\ReportField;
use App\Libraries\Scraper\Translator;
use Illuminate\Support\Collection;
use App\Facades\ShortCode;
use Illuminate\Support\Facades\DB;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\InvocaReport\Providers\Invoca\Translators
 */
class BIPublishersReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_fact_calls';
    /**
     * @var string $connection
     */
    protected $water_mark_connection = 'dwh';

    /**
     * @var string $table_name
     */
    protected $water_mark_table_name = 'high_water_mark_tbl';

    /**
     * @var int $report_id
     */
    protected $report_id = 28;

    /**
     * @var string $provider
     */
    protected $provider = 'Invoca';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'transaction_id',


        ];
    }
    public function store()
    {
        if (!$this->data) {
            return;
        }
        $rows = $this->buildRows($this->data);
        $rows = $this->betterment($rows);
        $store_method = 'store_' . $this->options['store_method'];
        $this->$store_method($rows);
    }
    public function betterment($rows)
    {
        foreach ($rows as &$row){
            if(isset($row['caller_id']))
                $row['caller_id'] = str_replace('-','',$row['caller_id']);
        }
        return $rows;
    }
    /**
     * Build Rows
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($data)
    {
        $rows = [];
        foreach ($data as $data_row) {
            foreach ($data_row as $row){
                $rows[] = $this->prepareRow($row);
            }
        }
        return $rows;
    }

    /**
     * Prepare Row
     *
     * @param array $row
     *
     * @return mixed
     */
    private function prepareRow($row)
    {
        $fields  = $this->getReportFields();
        foreach ($fields as $field) {
            $new_row[$field->name] = ShortCode::setShortCodes($field->value, $row);
        }

        return $new_row;
    }

    /**
     * Get Report Fields
     *
     * @return Collection
     */
    protected function getReportFields()
    {
        if (!isset($this->fieldRows) || (!isset($this->current_report) || $this->current_report != $this->report_id)) {
            $this->current_report = $this->report_id;
            $this->fieldRows      = ReportField::where('report_id', $this->report_id)
                ->where('provider', $this->provider)->get();
        }

        return $this->fieldRows;
    }

    /**
     * Store Insert Or Update
     *
     * @param $rows
     */
    public function store_insertOrUpdate($rows)
    {
        $count_insert = 0;
        foreach (array_chunk($rows, $this->store_chunk_size) as $records) {
            $builder = \DB::connection($this->connection)->table($this->table_name);
            $count_insert += insertOnDuplicateKeyUpdate($builder, $records, $this->getExcludeAbleFields());
            //$this->setHighWaterMark(end($records));

        }

        $this->logger->info(['Records inserted or updated: ' . $count_insert, 'Counts rows: ' . count($rows)]);
    }
    /**
     * Set DWH High Water Mark
     *
     * @return Collection
     */
    protected function setHighWaterMark($last_row)
    {
        $hwm_id = $last_row['transaction_id'];
        $hwm_date = $last_row['call_start_time'];

        $high_water_mark_obj = DB::connection($this->water_mark_connection)
            ->table($this->water_mark_table_name)
            ->where('table_name','=','mrr_fact_calls')
            ->update(['field_name' => 'transaction_id','high_water_mark_id' => $hwm_id , 'high_water_mark' => $hwm_date]);

        $this->logger->info('Updated High Water Mark for mrr_fact_calls to - date : ' . $hwm_date . ' and id : ' . $hwm_id);
    }

}
