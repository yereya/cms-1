<?php namespace App\Libraries\Scraper\Requests\InvocaReport;

use App\Libraries\Scraper\Requests\InvocaReport\Providers\Invoca\Provider as InvocaProvider;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\InvocaReport\Providers\Invoca\Translators\BIPublishersReport as InvocaBIPublishersReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 117;

    /**
     * @var array $providers
     */
    protected $providers = [
        'invoca' => [
            'class'       => InvocaProvider::class,
            'translators' => [
                InvocaBIPublishersReport::class,
            ]
        ]
    ];
}