<?php namespace App\Libraries\Scraper\Requests\BudgetDailyPerformance\Providers\Bing;

use DB;
use GuzzleHttp\Client;
use App\Libraries\Scraper\Providers\BingReport;
use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\CampaignManagement\GetCampaignsByIdsRequest;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'BUDGET_SUMMARY_REPORT';

    /**
     * @var int
     */
    protected $report_id = 12;

    /**
     * @var string - database name
     */
    private $db = 'mrr';

    /**
     * @var string - table name
     */
    private $table = 'mrr_budget_performance';

    /**
     * @var
     */
    private $account_id;

    /**
     * Fetch scraper
     */
    public function fetch()
    {
        $budgets = $this->getBudgets();

        $response = $this->budgetsBing($budgets);

        $this->updateResults($response);
    }

    /**
     * Get Budget Collection
     *
     * accoutn_id => [
     *  publisher_id,
     *  account_id,
     *  budget_id
     * ]
     *
     * @return static
     */
    private function getBudgets()
    {
        $date = date('Y-m-d', strtotime('-2 days', time()));
        $budgets = DB::connection($this->db)->table($this->table)
            ->select('publisher_id', 'account_id', 'budget_id', 'campaign_id')->where('updated_at', '>', $date)
            ->where('publisher_id', 97)->get();

        return collect($budgets)->groupBy('account_id');
    }

    /**
     * Get all budgets per account
     *
     * @param $budgets
     *
     * @return array
     */
    private function budgetsBing($budgets)
    {
        $response = [];
        foreach ($budgets as $account_id => $items) {
            $this->account_id = $account_id;
            $request = $this->buildBudgetRequest($account_id, $items->groupBy('campaign_id')->keys()
                ->toArray());
            $records = $this->getRecords('GetCampaignsByIds', $request);
            $response[$account_id] = $this->convertResponse($account_id, $records);
            $this->logger->info("download customer $account_id report successful");
        }

        return $response;
    }

    /**
     * @Override client
     */
    private function initClient()
    {
        $this->client = new Client();
        $this->setRefreshToken();
        $this->setAccessToken();
    }

    /**
     * Build Request to Bing
     *
     * @param $account_id
     * @param $campaigns_id
     *
     * @return GetCampaignsByIdsRequest
     */
    private function buildBudgetRequest($account_id, $campaigns_id)
    {
        $this->bing_client->withAccountId($account_id);

        $request = new GetCampaignsByIdsRequest();
        $request->AccountId = $account_id;
        $request->CampaignIds = $campaigns_id;

        return $request;
    }

    /**
     * Get Records
     *
     * @param $operations
     * @param $request
     *
     * @return mixed
     */
    public function getRecords($operations, $request)
    {

        try {
            //$records = $this->proxy->GetService()->$operations($request);
            $records = $this->bing_client->campaignsService()->getService()->$operations($request);
        } catch (\SoapFault $e) {
            $this->logger->warning($e->detail->ApiFaultDetail->OperationErrors);
        }

        return $records;
    }

    /**
     * @Override
     * Set Proxy
     *
     * @param string $wsdl
     *
     * @return ClientProxy
     */
    protected function setProxy($wsdl)
    {

    }

    /**
     * Convert Response to array
     *
     * @param $account_id
     * @param $records
     *
     * @return array
     */
    private function convertResponse($account_id, $records)
    {
        $converted = [];
        foreach ($records as $items) {
            $campaigns = isset($items->Campaign) ? $items->Campaign : [];
            foreach ($campaigns as $campaign) {
                if(is_null($campaign)){
                    continue;
                }
                $converted[] = [
                    'account_id' => $account_id,
                    'budget_id' => $campaign->Id,
                    'budget' => $campaign->DailyBudget
                ];
            }
        }

        return $converted;
    }

    /**
     * Update results
     *
     * @param $response
     *
     * @return \App\Libraries\TpLogger
     */
    private function updateResults($response)
    {
        $date = date('Y-m-d', strtotime('-2 days', time()));
        $records_updated = 0;
        $this->logger->info('prepare translator');
        foreach ($response as $items) {
            $account_updates = 0;
            $account_id = null;
            foreach ($items as $item) {
                $account_id = $item['account_id'];
                try {
                    $upd_res = DB::connection($this->db)->table($this->table)->where('publisher_id', 97)
                        ->where('account_id', $item['account_id'])->where('campaign_id', $item['budget_id'])
                        ->where('updated_at', '>', $date)->update(['budget' => $item['budget']]);
                    $records_updated += $upd_res;
                    $account_updates += $upd_res;

                } catch (\Exception $e) {
                    $this->logger->warning(['Error to insert to table', $item, $e->getMessage()]);
                }
            }
            if ($account_updates > 0) {
                $this->logger->info(['Customer', $account_id, 'update', $account_updates, 'records successfully']);
                continue;
            }

            $this->logger->info(['No updates for customer', $account_id]);
        }

        if ($records_updated > 0) {
            return $this->logger->info(['Updated successful records', $records_updated]);
        }

        $this->logger->info('No records to update');
    }

    /**
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
    }
}