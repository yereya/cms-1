<?php namespace App\Libraries\Scraper\Requests\BudgetDailyPerformance\Providers\Bing\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class BudgetPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Bing\Translators
 */
class BudgetPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_budget_performance';

    /**
     * @var int
     */
    protected $report_id = 12;

    /**
     * @var string $provider
     */
    protected $provider = 'Bing';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'stats_date_tz',
            'budget_id',
            'campaign_id',
        ];
    }
}