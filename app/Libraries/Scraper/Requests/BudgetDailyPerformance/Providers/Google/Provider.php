<?php namespace App\Libraries\Scraper\Requests\BudgetDailyPerformance\Providers\Google;

use DB;
use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\v201809\cm\BudgetService;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\Selector;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'BUDGET_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 12;

    /**
     * @var string - database name
     */
    private $db = 'mrr';

    /**
     * @var string - table name
     */
    private $table = 'mrr_budget_performance';

    /**
     * @var array - responses data from adwords
     */
    private $response = [];

    /**
     * Fetch scraper
     */
    public function fetch()
    {
        $budgets = $this->getBudgets();

        $this->response = $this->budgetsAdwords($budgets);
        $converted      = $this->convertResponse();
    }

    /**
     * Get Budget Collection
     * accoutn_id => [
     *  publisher_id,
     *  account_id,
     *  budget_id
     * ]
     *
     * @return static
     */
    private function getBudgets()
    {
        $date    = date('Y-m-d', strtotime('-2 days', time()));
        $budgets = DB::connection($this->db)->table($this->table)->select('publisher_id', 'account_id', 'budget_id')
            ->where('updated_at', '>', $date)->where('publisher_id', 95)->get();

        return collect($budgets)->groupBy('account_id');
    }

    /**
     * Function Budget Adwords
     *
     * @param $budgets
     *
     * @return array
     */
    public function budgetsAdwords($budgets)
    {
        $response = [];
        foreach ($budgets as $account_id => $item) {
            $ids = $item->groupBy('budget_id')->keys()->toArray();

            $selector = $this->createSelector($ids);

            $response[$account_id] = $this->runServiceAdWords($account_id, $selector);
        }

        return $response;
    }

    /**
     * Create Adwords Selector
     *
     * @param $ids
     *
     * @return Selector
     */
    private function createSelector($ids)
    {
        if (!is_array($ids)) {
            $ids = [$ids];
        }

        $selector = new Selector();
        $selector->setFields(['BudgetId', 'Amount']);
        $selector->setPredicates([new Predicate('BudgetId', PredicateOperator::IN, $ids)]);

        return $selector;
    }

    /**
     * Run service Budget
     *
     * @param string   $account_id
     * @param Selector $selector
     *
     * @return mixed
     */
    private function runServiceAdWords($account_id, Selector $selector)
    {
        try {
            $adWordsServices = new AdWordsServices();
            $budgetService   = $adWordsServices->get($this->makeSession($account_id), BudgetService::class);

            return $$budgetService->get($selector);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * Convert Response to array
     *
     * @return array
     */
    private function convertResponse()
    {
        $parse = [];
        foreach ($this->response as $account_id => $items) {
            foreach ($items->entries as $item) {
                $parse[] = [
                    'account_id' => $account_id,
                    'budget_id'  => $item->budgetId,
                    'budget'     => $item->amount->microAmount / 1000000
                ];
            }
        }

        return $parse;
    }

    /**
     * Set Selector
     */
    protected function setSelector()
    {
        // TODO: Implement setReportDefinition() method.
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        // TODO: Implement setReportDefinition() method.
    }
}