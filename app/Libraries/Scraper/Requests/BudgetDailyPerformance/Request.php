<?php namespace App\Libraries\Scraper\Requests\BudgetDailyPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\BudgetDailyPerformance\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\BudgetDailyPerformance\Providers\Bing\Translators\BudgetPerformanceReport as BingBudgetPerformanceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 109;

    /**
     * @var array $providers
     */
    protected $providers = [
        'bing' => [
            'class'       => BingProvider::class,
            'translators' => [
                BingBudgetPerformanceReport::class
            ]
        ]
    ];
}