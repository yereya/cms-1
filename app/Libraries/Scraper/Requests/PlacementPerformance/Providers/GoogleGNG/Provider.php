<?php namespace App\Libraries\Scraper\Requests\PlacementPerformance\Providers\GoogleGNG;

use App\Libraries\Scraper\Providers\GoogleGNGReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleGNGReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'PLACEMENT_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 11;

    /**
     * @var string
     */
    protected $report_folder_name = 'PlacementPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);
        $reportDefinition->setReportName('Placement Performance Report #' . uniqid());
        $reportDefinition->setDateRangeType($this->date_range_type);
        $reportDefinition->setReportType($this->report_type);
        $reportDefinition->setDownloadFormat('CSV');

        return $reportDefinition;
    }
}
