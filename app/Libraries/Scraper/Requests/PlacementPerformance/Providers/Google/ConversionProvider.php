<?php namespace App\Libraries\Scraper\Requests\PlacementPerformance\Providers\Google;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Requests\PlacementPerformance\Providers\Google
 */
class ConversionProvider extends Provider
{
    /**
     * @var int
     */
    protected $report_id = 19;

    /**
     * @var string
     */
    protected $report_folder_name = 'PlacementConversionsPerformance';
}