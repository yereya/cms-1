<?php namespace App\Libraries\Scraper\Requests\PlacementPerformance\Providers\Google\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class PlacementPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Google\Translators
 */
class PlacementPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_placement_performance';

    /**
     * @var int
     */
    protected $report_id = 11;

    /**
     * @var string $provider
     */
    protected $provider = 'Google';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'report_id',
            'publisher_id',
            'ad_group_id',
            'placement_id',
            'device',
            'criteria',
            'stats_date_tz',
        ];
    }
}