<?php namespace App\Libraries\Scraper\Requests\PlacementPerformance\Providers\Google\Translators;

/**
 * Class PlacementConversionPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators
 */
class PlacementConversionPerformanceReport extends PlacementPerformanceReport
{
    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_placement_conversion_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 19;
}