<?php namespace App\Libraries\Scraper\Requests\PlacementPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Google
use App\Libraries\Scraper\Requests\PlacementPerformance\Providers\Google\Translators\PlacementConversionPerformanceReport as GoogleSearchQueryConversionPerformanceReport;
use App\Libraries\Scraper\Requests\PlacementPerformance\Providers\Google\Translators\PlacementPerformanceReport as GooglePlacementPerformanceReport;
// Google Conversions
use App\Libraries\Scraper\Requests\PlacementPerformance\Providers\Google\ConversionProvider as GoogleConversionProvider;
use App\Libraries\Scraper\Requests\PlacementPerformance\Providers\Google\Provider as GoogleProvider;
// GNG
use App\Libraries\Scraper\Requests\PlacementPerformance\Providers\GoogleGNG\Translators\PlacementConversionPerformanceReport as GoogleGNGSearchQueryConversionPerformanceReport;
use App\Libraries\Scraper\Requests\PlacementPerformance\Providers\GoogleGNG\Translators\PlacementPerformanceReport as GoogleGNGPlacementPerformanceReport;
// GNG Conversions
use App\Libraries\Scraper\Requests\PlacementPerformance\Providers\GoogleGNG\ConversionProvider as GoogleGNGConversionProvider;
use App\Libraries\Scraper\Requests\PlacementPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 51;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google'            => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GooglePlacementPerformanceReport::class
            ]
        ],
        'google-conversion' => [
            'class'       => GoogleConversionProvider::class,
            'translators' => [
                GoogleSearchQueryConversionPerformanceReport::class
            ]
        ],
//        'googleGNG'            => [
//            'class'       => GoogleGNGProvider::class,
//            'translators' => [
//                GoogleGNGPlacementPerformanceReport::class
//            ]
//        ],
//        'googleGNG-conversion' => [
//            'class'       => GoogleGNGConversionProvider::class,
//            'translators' => [
//                GoogleGNGSearchQueryConversionPerformanceReport::class
//            ]
//        ],
    ];
}
