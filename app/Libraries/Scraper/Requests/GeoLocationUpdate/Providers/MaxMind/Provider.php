<?php namespace App\Libraries\Scraper\Requests\GeoLocationUpdate\Providers\MaxMind;

use App\Entities\Models\GeoLocation\City;
use App\Entities\Models\GeoLocation\CityBlock;
use App\Entities\Models\GeoLocation\Country;
use App\Entities\Models\GeoLocation\CountryBlock;
use App\Libraries\Scraper\Provider as BaseProvider;
use GuzzleHttp\Client;
use Illuminate\Database\Query\Builder;
use League\Csv\Reader;
use ZipArchive;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BaseProvider
{
    /**
     * @var string $report_folder_name
     */
    protected $report_folder_name = 'GeoIP';

    /**
     * @var string $publisher_name
     */
    protected $publisher_name = 'MaxMind';

    /**
     * @var int $csv_chunk
     */
    private $csv_chunk = 2000;

    /**
     * Fetch
     *
     * Executes both countries and cities fetching
     */
    public function fetch()
    {
        $this->fetchCountries();
        $this->fetchCities();
    }

    /**
     * Fetch Countries
     *
     * Downloads the ZIP, extracts to CSV, and stores to DB
     */
    private function fetchCountries()
    {
        $url      = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-Country-CSV.zip';
        $zip_file = $this->getReportFilePath(date('Y-m-d') . '-Country', 'zip');

        $this->downloadAndExtract($url, $zip_file);

        $this->storeCountriesLocations('GeoLite2-*/GeoLite2-Country-Locations-en.csv');
        $this->storeCountriesBlocks('GeoLite2-*/GeoLite2-Country-Blocks-IPv4.csv');
    }

    /**
     * Download & Extract
     *
     * @param string $url
     * @param string $target
     */
    private function downloadAndExtract($url, $target)
    {
        with(new Client())->get($url, [
            'sink' => $target
        ]);

        $zip = new ZipArchive();
        $zip->open($target);
        $zip->extractTo($this->getReportFolderPath());
        $zip->close();
    }

    /**
     * Store Countries Locations
     *
     * Tries to find the locations CSV file inside extracted ZIP and stores in DB
     *
     * @param string $file
     */
    private function storeCountriesLocations($file)
    {
        foreach (glob($this->getReportFolderPath() . DIRECTORY_SEPARATOR . $file) as $path) {
            /** @var Reader $csv */
            $csv     = Reader::createFromPath($path);
            $builder = \DB::table(Country::getTableName());

            $this->storeCsvChunk($csv, $builder);
        }
    }

    /**
     * Store CSV Chunk
     *
     * @param Reader        $csv
     * @param Builder       $builder
     * @param null|callable $callback
     */
    private function storeCsvChunk(Reader $csv, Builder $builder, callable $callback = null)
    {
        $items = [];
        foreach ($csv->fetchAssoc(0, $callback) as $i => $item) {
            $items[] = $item;

            if (count($items) >= $this->csv_chunk) {
                insertOnDuplicateKeyUpdate($builder, $items);
                $items = [];
            }
        }

        if ($items) {
            insertOnDuplicateKeyUpdate($builder, $items);
        }
    }

    /**
     * Store Countries Blocks
     *
     * Tries to find the locations CSV file inside extracted ZIP and stores in DB.
     * Ignores rows without `geoname_id` because it represents the location (we don't need IP range without location)
     *
     * @param string $file
     */
    private function storeCountriesBlocks($file)
    {
        foreach (glob($this->getReportFolderPath() . DIRECTORY_SEPARATOR . $file) as $path) {
            /** @var Reader $csv */
            $csv     = Reader::createFromPath($path);
            $builder = \DB::table(CountryBlock::getTableName());

            // Only when geoname_id field is not empty, we'll want the block
            $csv->addFilter(function ($row, $i) {
                return $i && isset($row[1]) && $row[1];
            });

            $this->storeCsvChunk($csv, $builder, [$this, 'getBlockEntry']);
        }
    }

    /**
     * Get Countries Block Entry
     *
     * @param array $row
     *
     * @return array
     */
    public function getBlockEntry($row)
    {
        $network_data = explode('/', $row['network']); // Example: 10.0.0.100/27

        $network_base   = $network_data[0]; // Example: 10.0.0.100
        $network_subnet = $network_data[1]; // Example: 27

        $row['network_subnet'] = $network_subnet;

        $range_start = ip2long($network_base); // Example: 167772260
        $range_end   = ip2long($network_base) + (1 << (32 - $network_subnet)); // Example: 167772292

        $row['range_start'] = $range_start;
        $row['range_end']   = $range_end;

        return $row;
    }

    /**
     * Fetch Cities
     *
     * Downloads the ZIP, extracts to CSV, and stores to DB
     */
    private function fetchCities()
    {
        $url      = 'http://geolite.maxmind.com/download/geoip/database/GeoLite2-City-CSV.zip';
        $zip_file = $this->getReportFilePath(date('Y-m-d') . '-City', 'zip');

        $this->downloadAndExtract($url, $zip_file);

        $this->storeCitiesLocations('GeoLite2-*/GeoLite2-City-Locations-en.csv');
        $this->storeCitiesBlocks('GeoLite2-*/GeoLite2-City-Blocks-IPv4.csv');
    }

    /**
     * Store Cities Locations
     *
     * Tries to find the locations CSV file inside extracted ZIP and stores in DB
     *
     * @param string $file
     */
    private function storeCitiesLocations($file)
    {
        foreach (glob($this->getReportFolderPath() . DIRECTORY_SEPARATOR . $file) as $path) {
            /** @var Reader $csv */
            $csv     = Reader::createFromPath($path);
            $builder = \DB::table(City::getTableName());

            $this->storeCsvChunk($csv, $builder);
        }
    }

    /**
     * Store Cities Blocks
     *
     * Tries to find the locations CSV file inside extracted ZIP and stores in DB.
     * Ignores rows without `geoname_id` because it represents the location (we don't need IP range without location)
     *
     * @param string $file
     */
    private function storeCitiesBlocks($file)
    {
        foreach (glob($this->getReportFolderPath() . DIRECTORY_SEPARATOR . $file) as $path) {
            /** @var Reader $csv */
            $csv     = Reader::createFromPath($path);
            $builder = \DB::table(CityBlock::getTableName());

            // Only when geoname_id field is not empty, we'll want the block
            $csv->addFilter(function ($row, $i) {
                return $i && isset($row[1]) && $row[1];
            });

            $this->storeCsvChunk($csv, $builder, [$this, 'getBlockEntry']);
        }
    }
}