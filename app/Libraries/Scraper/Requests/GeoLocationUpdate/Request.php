<?php namespace App\Libraries\Scraper\Requests\GeoLocationUpdate;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\GeoLocationUpdate\Providers\MaxMind\Provider as MaxMindProvider;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 38;

    /**
     * @var array $providers
     */
    protected $providers = [
        'maxmind_country' => [
            'class'       => MaxMindProvider::class
        ],
    ];
}