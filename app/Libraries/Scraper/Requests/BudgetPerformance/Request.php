<?php namespace App\Libraries\Scraper\Requests\BudgetPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Bing
use App\Libraries\Scraper\Requests\BudgetPerformance\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\BudgetPerformance\Providers\Bing\Translators\BudgetPerformanceReport as BingBudgetPerformanceReport;
//BingGNG
use App\Libraries\Scraper\Requests\BudgetPerformance\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\BudgetPerformance\Providers\BingGNG\Translators\BudgetPerformanceReport as BingGNGBudgetPerformanceReport;
// Google
use App\Libraries\Scraper\Requests\BudgetPerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\BudgetPerformance\Providers\Google\Translators\BudgetPerformanceReport as GoogleBudgetPerformanceReport;
// Google GNG
use App\Libraries\Scraper\Requests\BudgetPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\BudgetPerformance\Providers\GoogleGNG\Translators\BudgetPerformanceReport as GoogleGNGBudgetPerformanceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 41;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleBudgetPerformanceReport::class
            ]
        ],
        'googleGNG' => [
            'class'       => GoogleGNGProvider::class,
            'translators' => [
                GoogleGNGBudgetPerformanceReport::class
            ]
        ],
        'bing'   => [
            'class'       => BingProvider::class,
            'translators' => [
                BingBudgetPerformanceReport::class
            ]
        ],
        'bingGNG'   => [
            'class'       => BingGNGProvider::class,
            'translators' => [
                BingGNGBudgetPerformanceReport::class
            ]
        ]
    ];
}
