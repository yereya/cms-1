<?php namespace App\Libraries\Scraper\Requests\BudgetPerformance\Providers\Bing\Translators;

use App\Libraries\Scraper\Translator;
use App\Facades\ShortCode;


/**
 * Class BudgetPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Bing\Translators
 */
class BudgetPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_budget_performance';

    /**
     * @var int
     */
    protected $report_id = 12;

    /**
     * @var string $provider
     */
    protected $provider = 'Bing';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'report_id',
            'publisher_id',
            'campaign_id',
            'budget_id',
            'stats_date_tz'
        ];
    }

    /**
     * Store Request
     *
     * @param array $request
     *
     * @return array
     */
    protected function storeRequest($request)
    {
        $store_method = 'store_' . $this->options['store_method'];
        $rows = $this->buildRows($request);
        $this->$store_method($rows);
    }

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            if (is_null($row)) {
                continue;
            }
            if (property_exists($stream, 'date')) {
                $row['_ReportDate'] = $stream->date;
            }
            $rows[] = $this->getRowValues($row);
        }

        try{
            $days_remaining = (int)date('t', strtotime("today")) - (int)date('j', strtotime("today"));
//            $days_in_month = date('t', strtotime("today"));
            $days_in_month = 30.4; // Bing's new calculation is based on 30.4 days each month

            $yesterdays_budgets = \DB::connection('mrr')->table('mrr_budget_performance')
                ->selectRaw('budget_id, budget_name, budget,stats_date_tz')
                ->where('publisher_id','=','97')
                ->where('stats_date_tz','=',date("Y-m-d", strtotime("yesterday")))
                ->get()
                ->toArray();

            $prev_cost = \DB::connection('mrr')->table('mrr_budget_performance')
                ->selectRaw('budget_id, budget_name, sum(cost) as cost')
                ->where('publisher_id','=','97')
                ->where('stats_date_tz','>=',date("Y-m-d", strtotime("first day of this month")))
                ->where('stats_date_tz','<',date("Y/m/d"))
                ->groupBy('budget_id','budget_name')
                ->get()
                ->toArray();


            $yesterday_budgets_arr = array_map(function($r) {
                $arr = [];
                $arr['budget_id'] = $r->budget_id;
                $arr['budget_name'] = $r->budget_name;
                $arr['budget'] = $r->budget;
                $arr['stats_date_tz'] = $r->stats_date_tz;
                return $arr;
            },$yesterdays_budgets);

            $prev_cost_arr = array_map(function($r) {
                $arr = [];
                $arr['budget_id'] = $r->budget_id;
                $arr['budget_name'] = $r->budget_name;
                $arr['cost'] = $r->cost;
                return $arr;
            },$prev_cost);

            foreach ($rows as &$row){
                $row['monthly_budget'] = $row['budget'];
                $daily_budget = $row['budget'] / $days_in_month;
                $search_index_budget = array_search($row['budget_id'],array_pluck($yesterday_budgets_arr,'budget_id')); // Find yesterdays budget for current row's budget_id
                if(date("Y/m/d", strtotime("first day of this month")) === date("Y/m/d") // Check if it is the first day of the month
                     || (is_numeric($search_index_budget) && $yesterday_budgets_arr[$search_index_budget]['budget'] == $daily_budget))  // Or there is previous budgets and yesterdays budget equals today's budget
                {
                    $new_budget = $daily_budget;
                }else{ // The budget has been changed in the middle of the month or started in the middle
                    $search_index_cost = array_search($row['budget_id'],array_pluck($prev_cost_arr,'budget_id')); // Find total cost for current row's budget_id since the beginning of the month
                    if(is_numeric($search_index_cost)) // If budget has changed in the middle of the month
                        $new_budget = ($row['budget'] - $prev_cost_arr[$search_index_cost]['cost']) / ($days_remaining + 1); // Monthly budget - total cost, divide the result by days remained
                    else // If budget started in the middle of the month
                        $new_budget = $row['budget'] / ($days_remaining + 1);
                }
                $row['budget'] = (int)$new_budget;
            }
        }catch (\Exception $e){
            $this->logger->error('Error in build rows -> BudgetPerformanceRepot.php for Bing. Error : ' . $e);
        }

        return $rows;
    }

    /**
     * Get Row Value
     *
     * @param array $row
     *
     * @return array
     */
    protected function getRowValues($row)
    {
        $new_row = [];
        $fields  = $this->getReportFields();

        foreach ($fields as $field) {
            $new_row[$field->name] = ShortCode::setShortCodes($field->value, $row);
        }

        return $new_row;
    }

}
