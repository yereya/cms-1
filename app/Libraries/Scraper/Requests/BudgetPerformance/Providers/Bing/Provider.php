<?php namespace App\Libraries\Scraper\Requests\BudgetPerformance\Providers\Bing;

use App\Libraries\Scraper\Providers\BingReport;


use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\BudgetSummaryReportRequest;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'BUDGET_SUMMARY_REPORT';

    /**
     * @var int
     */
    protected $report_id = 12;

    /**
     * @var string $report_request
     */
    protected $report_request = BudgetSummaryReportRequest::class;

    /**
     * @var string
     */
    protected $report_folder_name = 'BudgetPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Budget Summary Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Daily;
    }
}