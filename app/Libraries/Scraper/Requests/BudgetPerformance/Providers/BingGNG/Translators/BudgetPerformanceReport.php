<?php namespace App\Libraries\Scraper\Requests\BudgetPerformance\Providers\BingGNG\Translators;

use App\Libraries\Scraper\Translator;
use App\Facades\ShortCode;


/**
 * Class BudgetPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Bing\Translators
 */
class BudgetPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_budget_performance';

    /**
     * @var int
     */
    protected $report_id = 12;

    /**
     * @var string $provider
     */
    protected $provider = 'BingGNG';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'report_id',
            'publisher_id',
            'campaign_id',
            'budget_id',
            'stats_date_tz',
            'budget'
        ];
    }

    /**
     * Store Request
     *
     * @param array $request
     *
     * @return array
     */
    protected function storeRequest($request)
    {
        $store_method = 'store_' . $this->options['store_method'];
        $rows = $this->buildRows($request);
        $this->$store_method($rows);
    }

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            if (is_null($row)) {
                continue;
            }
            if (property_exists($stream, 'date')) {
                $row['_ReportDate'] = $stream->date;
            }
            $rows[] = $this->getRowValues($row);
        }

        try{
            $prev_budgets = \DB::connection('mrr')->table('mrr_budget_performance')
                ->selectRaw('budget_id, budget_name, sum(budget) as budget')
                ->where('publisher_id','=','164')
                ->where('stats_date_tz','>=',date("Y-m-d", strtotime("first day of this month")))
                ->where('stats_date_tz','<',date("Y/m/d"))
                ->groupBy('budget_id','budget_name')
                ->get()
                ->toArray();


            $prev_budgets_arr = array_map(function($r) {
                $arr = [];
                $arr['budget_id'] = $r->budget_id;
                $arr['budget_name'] = $r->budget_name;
                $arr['budget'] = $r->budget;
                return $arr;
            },$prev_budgets);

            foreach ($rows as $row){
                $daysRemaining = (int)date('t', strtotime("today")) - (int)date('j', strtotime("today"));
                $search_index_res = array_search($row['budget_id'],array_pluck($prev_budgets_arr,'budget_id'));
                if($search_index_res){
                    $prev_budget = $prev_budgets_arr[$search_index_res];
                    $new_budget = ($row['budget'] - $prev_budget['budget'])/$daysRemaining;
                }else{
                    $new_budget = $row['budget']/$daysRemaining;
                }
                $row['budget'] = (int)$new_budget;
            }
        }catch (\Exception $e){
            $this->logger->error('Error in build rows -> BudgetPerformanceRepot.php for BingGNG. Error : ' . $e);
        }

        return $rows;
    }

    /**
     * Get Row Value
     *
     * @param array $row
     *
     * @return array
     */
    protected function getRowValues($row)
    {
        $new_row = [];
        $fields  = $this->getReportFields();

        foreach ($fields as $field) {
            $new_row[$field->name] = ShortCode::setShortCodes($field->value, $row);
        }

        return $new_row;
    }

}




