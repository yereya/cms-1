<?php namespace App\Libraries\Scraper\Requests\BudgetPerformance\Providers\Google;

use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'BUDGET_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 12;

    /**
     * @var string
     */
    protected $report_folder_name = 'BudgetPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * @var bool $fetch_report_by_day
     */
    protected $fetch_report_by_day = true;

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);
        $reportDefinition->setReportName('Budget Performance Report #' . uniqid());
        $reportDefinition->setDateRangeType($this->date_range_type);
        $reportDefinition->setReportType($this->report_type);
        $reportDefinition->setDownloadFormat('CSV');

        return $reportDefinition;
    }
}