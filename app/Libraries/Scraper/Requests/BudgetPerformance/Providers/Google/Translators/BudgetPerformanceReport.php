<?php namespace App\Libraries\Scraper\Requests\BudgetPerformance\Providers\Google\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class BudgetPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Google\Translators
 */
class BudgetPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_budget_performance';

    /**
     * @var int
     */
    protected $report_id = 12;

    /**
     * @var string $provider
     */
    protected $provider = 'Google';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'report_id',
            'publisher_id',
            'campaign_id',
            'budget_id',
            'stats_date_tz'
        ];
    }
}