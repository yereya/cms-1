<?php namespace App\Libraries\Scraper\Requests\ChangeLogState\Providers;

use App\Entities\Models\Bo\Account;

trait TranslatorHelperTrait
{
    /**
     * @var current source account_id
     */
    private $account_id;

    /**
     * @var current local advertiser id
     */
    private $advertiser_id;

    /**
     * @var current advertiser name
     */
    private $advertiser_name;

    /**
     * Change type status value from string to boolean
     *
     * @param $row
     *
     * @return mixed
     */
    abstract protected function changeStatusToBoolean($row);

    /**
     * Get parent function getRowValues
     *
     * @param $row
     *
     * @return mixed
     */
    abstract protected function rowValues($row);

    /**
     * Remove not used fields
     *
     * @param $row
     *
     * @return mixed
     */
    abstract protected function removeFields($row);

    /**
     * Change additional fields
     *
     * @param $row
     *
     * @return mixed
     */
    abstract protected function additionalFields($row);

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'advertiser_id',
            'ad_group_id',
            'report_id',
            'publisher_id',
            'advertiser_id',
            'ad_group_id',
            'keyword_id',
            'account_id',
            'device'
        ];
    }

    /**
     * Build Rows
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            $rows[] = $this->prepareRow($row);
        }

        return $rows;
    }

    /**
     * Prepare Row
     *
     * @param array $row
     *
     * @return mixed
     */
    private function prepareRow($row)
    {
        $row = $this->rowValues($row);

        if (empty($this->account_id) || $this->account_id != $row['account_id']) {
            $this->account_id = $row['account_id'];
            $this->foundAdvertiser($this->account_id);
        }

        //add advertiser id
        $row['advertiser_id'] = $this->advertiser_id;

        //add advertiser name
        $row['advertiser_name'] = $this->advertiser_name;

        return $row;
    }

    /**
     * Found Advertisers
     *
     * @param $account_id
     */
    private function foundAdvertiser($account_id)
    {
        $account               = Account::where('source_account_id', $account_id)->with('advertiser')->first();
        $this->advertiser_id   = $account->advertiser->id;
        $this->advertiser_name = $account->advertiser->name;
    }
}