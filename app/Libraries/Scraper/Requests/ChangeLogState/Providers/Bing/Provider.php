<?php namespace App\Libraries\Scraper\Requests\ChangeLogState\Providers\Bing;

use App\Libraries\Scraper\Providers\BingReport;


use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportRequest;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{
    /**
     * @var int
     */
    protected $report_id = 20;

    /**
     * @var string $report_type
     */
    protected $report_type = 'CHANGE_LOG_KEYWORDS_REPORT';

    /**
     * @var string $report_request
     */
    protected $report_request = KeywordPerformanceReportRequest::class;

    /**
     * @var string
     */
    protected $report_folder_name = 'ChangeLogState';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Keyword Performance Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Daily;
    }
}