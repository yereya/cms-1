<?php namespace App\Libraries\Scraper\Requests\ChangeLogState\Providers\Bing;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountCampaign;
use App\Entities\Models\Mrr\MrrChangeLogState;
use App\Libraries\PublisherEditor\PublisherEditorFactory;
use App\Libraries\TpLogger;

class AdditionProvider
{
    /**
     * @var int
     */
    private $publisher_id = 97;

    /**
     * @var array
     */
    private $report_id = [20];

    /**
     * @var array
     */
    private $accounts = [];

    /**
     * @var int
     */
    private $size_identifiers = 100;

    /**
     * @var logger
     */
    private $logger;

    /**
     * Override main fetch
     */
    public function fetch()
    {
        $this->logger = TpLogger::getInstance();
        $this->getAccounts();
        foreach ($this->accounts as $account) {
            $this->campaignData($account);
            $this->adGroupData($account);
        }
    }

    /**
     * Get accounts by publisher and report
     */
    private function getAccounts()
    {
        $this->accounts = Account::whereHas('advertiser', function ($query) {
            $query->where('advertisers.status', 'active');
        })->whereHas('publisher', function ($query) {
            $query->where('publishers.id', $this->publisher_id);
        })->whereHas('reports.report', function ($query) {
            $query->where('reports.id', $this->report_id);
        })->get()->pluck('source_account_id')->filter()->toArray();
    }

    /**
     * Get and insert campaign budget and campaign device bid adjustment
     *
     * @param $account
     */
    private function campaignData($account)
    {
        $campaign_ids = $this->getChangeLogIds($account);
        if (!empty($campaign_ids)) {
            $this->logger->info(['Get Budgets to ', count($campaign_ids) . ' campaigns. Account(' . $account . ')']);
            $budgets = $this->getBudget($account, $campaign_ids);
            $this->logger->info(['Update Budgets to ', count($campaign_ids) . ' campaigns. Account(' . $account . ')']);
            $this->updateBudgetRecords($account, $budgets);

            $this->logger->info([
                'Get Campaign Adjustment to ',
                count($campaign_ids) . ' campaigns. Account(' . $account . ')'
            ]);
            $campaign_adjustment = $this->getCampaignBidAdjustment($account, $campaign_ids);
            $this->logger->info([
                'Update Campaign Adjustment to ',
                count($campaign_ids) . ' campaigns. Account(' . $account . ')'
            ]);
            $this->updateCampaignBidAdjustment($account, $campaign_adjustment);
        }
    }

    /**
     * Get campaigns by account id
     *
     * @param $account
     *
     * @return mixed
     */
    private function getChangeLogIds($account)
    {
        return AccountCampaign::where('source_account_id', $account)->get()->pluck('campaign_id')->toArray();
    }

    /**
     * Get campaign budget
     *
     * @param $account_id
     * @param $campaign_ids
     *
     * @return mixed
     */
    private function getBudget($account_id, $campaign_ids)
    {
        $budget = PublisherEditorFactory::campaign(Account::where('source_account_id', $account_id)->first())
                                        ->setCampaignId($campaign_ids)->getBudget();

        return $budget;
    }

    /**
     * Update campaign budget
     *
     * @param $account
     * @param $budgets
     */
    private function updateBudgetRecords($account, $budgets)
    {
        try {
            foreach ($budgets as $key => $budget) {
                MrrChangeLogState::where('publisher_id', $this->publisher_id)->where('account_id', $account)
                                 ->where('campaign_id', $key)->update(['campaign_budget' => $budget]);
            }
        } catch (\Exception $e) {
            $this->logger->error(['Update Budgets', $e->getMessage()]);
        }
    }

    /**
     * Get campaign bid adjustment
     *
     * @param $account_id
     * @param $campaign_ids
     *
     * @return mixed
     */
    private function getCampaignBidAdjustment($account_id, $campaign_ids)
    {
        $adjustment = PublisherEditorFactory::campaign(Account::where('source_account_id', $account_id)->first())
                                            ->setCampaignId($campaign_ids)->getTarget();

        return $adjustment;
    }

    /**
     * Update campaign bid adjustment only smartphone
     *
     * @param $account
     * @param $adjustments
     */
    private function updateCampaignBidAdjustment($account, $adjustments)
    {
        try {
            foreach ($adjustments as $key => $adjustment) {
                MrrChangeLogState::where('publisher_id', $this->publisher_id)->where('account_id', $account)
                                 ->where('campaign_id', $key)->where('device', 'm')
                                 ->update(['campaign_device_bid_adjustment' => $adjustment]);
            }
        } catch (\Exception $e) {
            $this->logger->error(['Update Campaign Bid Adjustment', $e->getMessage()]);
        }
    }

    /**
     * Get and insert adgroup device bid adjustment
     *
     * @param $account
     */
    private function adGroupData($account)
    {
        $ad_group_ids = $this->getChangeLogAdGroupIds($account);
        if (!empty($ad_group_ids)) {
            $this->logger->info([
                'Start Insert Ad Group Ids Adjustment ',
                count($ad_group_ids) . ' ad group ids. Account(' . $account . ')'
            ]);
            foreach (array_chunk($ad_group_ids, $this->size_identifiers) as $ad_groups) {
                $ad_group_adjustment = $this->getAdGroupBidAdjustment($account, $ad_groups);
                $this->updateAdGroupBidAdjustment($account, $ad_group_adjustment);
            }
        }
    }

    /**
     * Get ad groups by account id
     *
     * @param $account
     *
     * @return mixed
     */
    private function getChangeLogAdGroupIds($account)
    {
        $ad_group_ids = MrrChangeLogState::select('ad_group_id')->where('publisher_id', $this->publisher_id)
                                         ->where('account_id', $account)->groupBy('ad_group_id')->get()
                                         ->pluck('ad_group_id')->toArray();

        return $ad_group_ids;
    }

    /**
     * Get ad group bid adjustment
     *
     * @param $account_id
     * @param $ad_group_ids
     *
     * @return mixed
     */
    private function getAdGroupBidAdjustment($account_id, $ad_group_ids)
    {
        $adjustment = PublisherEditorFactory::adGroup(Account::where('source_account_id', $account_id)->first())
                                            ->setAdGroupId($ad_group_ids)->getTarget();

        return $adjustment;
    }

    /**
     * Update ad group bid adjustment
     *
     * @param $account
     * @param $adjustments
     */
    private function updateAdGroupBidAdjustment($account, $adjustments)
    {
        try {
            foreach ($adjustments as $key => $adjustment) {
                MrrChangeLogState::where('publisher_id', $this->publisher_id)->where('account_id', $account)
                                 ->where('ad_group_id', $key)->where('device', 'm')
                                 ->update(['ad_group_device_bid_adjustment' => $adjustment]);
            }
        } catch (\Exception $e) {
            $this->logger->error(['Update Ad Group Bid Adjustment', $e->getMessage()]);
        }
    }

    /**
     * Override request method hasData
     *
     * @return null
     */
    public function hasData()
    {
        return null;
    }
}