<?php namespace App\Libraries\Scraper\Requests\ChangeLogState\Providers\Bing\Translators;

use App\Libraries\Scraper\Requests\ChangeLogState\Providers\TranslatorHelperTrait;
use App\Libraries\Scraper\Translator;

/**
 * Class ChangeLogStateReport
 *
 * @package App\Libraries\Scraper\Requests\ChangeLogState\Providers\Bing\Translators
 */
class ChangeLogStateReport extends Translator
{
    use TranslatorHelperTrait;

    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_change_log_state';

    /**
     * @var int $report_id
     */
    protected $report_id = 20;

    /**
     * @var string $provider
     */
    protected $provider = 'Bing';

    /**
     * Parent method, need to trait
     *
     * @param $row
     *
     * @return array
     */
    protected function rowValues($row)
    {
        return $this->getRowValues($row);
    }

    /*
     * abstract trait method
     */
    protected function additionalFields($row)
    {
    }

    /*
     * abstract trait method
     */
    protected function changeStatusToBoolean($row)
    {
    }

    /*
     * abstract trait method
     */
    protected function removeFields($row)
    {
    }
}