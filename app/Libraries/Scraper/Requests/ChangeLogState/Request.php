<?php namespace App\Libraries\Scraper\Requests\ChangeLogState;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\ChangeLogState\Providers\Bing\AdditionProvider as AdditionBingProvider;
use App\Libraries\Scraper\Requests\ChangeLogState\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\ChangeLogState\Providers\Bing\Translators\ChangeLogStateReport as BingChangeLogStateReport;
use App\Libraries\Scraper\Requests\ChangeLogState\Providers\Google\AdditionProvider as AdditionGoogleProvider;
use App\Libraries\Scraper\Requests\ChangeLogState\Providers\Google\AdProvider as GoogleAdProvider;
use App\Libraries\Scraper\Requests\ChangeLogState\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\ChangeLogState\Providers\Google\Translators\AdChangeLogStateReport as GoogleAdChangeLogStateReport;
use App\Libraries\Scraper\Requests\ChangeLogState\Providers\Google\Translators\ChangeLogStateReport as GoogleChangeLogStateReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 63;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google'        => [ // keyword performance report
                             'class'       => GoogleProvider::class,
                             'translators' => [
                                 GoogleChangeLogStateReport::class
                             ]
        ],
        'google-ad'     => [ // ad performance report
                             'class'       => GoogleAdProvider::class,
                             'translators' => [
                                 GoogleAdChangeLogStateReport::class
                             ]
        ],
        'bing'          => [
            'class'       => BingProvider::class,
            'translators' => [
                BingChangeLogStateReport::class
            ]
        ],
        'bing-insert'   => [ // addition fields [campaign_budget, campaign_bid_adjustment, ad_group_bid_adjustment]
                             'class' => AdditionBingProvider::class
        ],
        'google-insert' => [
            'class' => AdditionGoogleProvider::class
        ]
    ];
}