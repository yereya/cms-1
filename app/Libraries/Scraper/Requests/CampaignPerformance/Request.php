<?php namespace App\Libraries\Scraper\Requests\CampaignPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Bing
use App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Bing\Translators\TranslatorReport as BingTranslatorReport;
// Bing GNG
use App\Libraries\Scraper\Requests\CampaignPerformance\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\CampaignPerformance\Providers\BingGNG\Translators\TranslatorReport as BingGNGTranslatorReport;
// Google
use App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Google\Translators\TranslatorReport as GoogleTranslatorReport;
// Google GNG
use App\Libraries\Scraper\Requests\CampaignPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\CampaignPerformance\Providers\GoogleGNG\Translators\TranslatorReport as GoogleGNGTranslatorReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 69;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleTranslatorReport::class
            ]
        ],
//        'googleGNG' => [
//            'class'       => GoogleGNGProvider::class,
//            'translators' => [
//                GoogleGNGTranslatorReport::class
//            ]
//        ],
        'bing'   => [
            'class'       => BingProvider::class,
            'translators' => [
                BingTranslatorReport::class
            ]
        ],
//        'bingGNG'   => [
//            'class'       => BingGNGProvider::class,
//            'translators' => [
//                BingGNGTranslatorReport::class
//            ]
//        ],
    ];
}
