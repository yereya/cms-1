<?php namespace App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Bing\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class CampaignPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Google\Translators
 */
class TranslatorReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_campaign_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 23;

    /**
     * @var string $provider
     */
    protected $provider = 'Bing';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'report_id',
            'publisher_id',
            'campaign_source_id',
            'device',
            'stats_date_tz',
            'hour',
            'network'
        ];
    }
}