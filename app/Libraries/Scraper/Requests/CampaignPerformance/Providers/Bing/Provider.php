<?php namespace App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Bing;

use App\Libraries\Scraper\Providers\BingReport;

use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\CampaignPerformanceReportRequest;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'CAMPAIGN_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 23;

    /**
     * @var string $report_request
     */
    protected $report_request = CampaignPerformanceReportRequest::class;

    /**
     * @var string $report_folder_name
     */
    protected $report_folder_name = 'CampaignPerformance';

    /**
     * @var string $store_method
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * @var array $additional_fields
     */
    protected $additional_fields = ['Hour'];

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Campaign Performance Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Hourly;
    }
}