<?php namespace App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Google\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class TranslatorReport
 *
 * @package App\Libraries\Scraper\Requests\CampaignPerformance\Providers\Google\Translators
 */
class TranslatorReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_campaign_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 23;

    /**
     * @var string $provider
     */
    protected $provider = 'Google';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'report_id',
            'publisher_id',
            'campaign_source_id',
            'device',
            'stats_date_tz',
            'hour',
            'network'
        ];
    }

    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            $rows[] = $this->getRowValues($row);
        }

        return $rows;
    }
}