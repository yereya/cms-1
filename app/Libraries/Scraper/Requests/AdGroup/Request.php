<?php namespace App\Libraries\Scraper\Requests\AdGroup;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\AdGroup\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\AdGroup\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\AdGroup\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\AdGroup\Providers\GoogleGNG\Provider as GoogleGNGProvider;

class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 36;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class' => GoogleProvider::class
        ],
//        'googleGNG' => [
//            'class' => GoogleGNGProvider::class
//        ],
        'bing'   => [
            'class' => BingProvider::class
        ],
//        'bingGNG'   => [
//            'class' => BingGNGProvider::class
//        ]
    ];
}
