<?php namespace App\Libraries\Scraper\Requests\AdGroup\Providers\BingGNG;

use App\Entities\Models\Bo\Account;
use App\Libraries\PublisherEditor\PublisherEditorFactory;
use App\Libraries\Scraper\Providers\BingGNGReport;

use Microsoft\BingAds\V13\Reporting\ReportRequest;

class Provider extends BingGNGReport
{
    /**
     * @var string
     */
    protected $report_type = 'AD_GROUP_REPORT';

    /**
     * @var int
     */
    protected $report_id = 22;

    /**
     * @var string
     */
    private $connection = 'bo';

    /**
     * @var string
     */
    private $table_name = 'account_campaign_ad_groups';

    /**
     * @var array
     */
    private $campaigns = [];

    /**
     * Fetch method
     */
    public function fetch()
    {
        $this->logger->info('Start fetch');

        $customers = $this->getCustomers();

        foreach ($customers as $source_account_id => $account) {
            $this->importAdGroups($account);
            $this->storeCampaigns();
        }
    }

    /**
     * Get Customers By Advertisers
     */
    private function getCustomers()
    {
        return Account::where('publisher_id', $this->publisher_id)->with('campaigns')->get()
                      ->keyBy('source_account_id');
    }

    /**
     * Import Ad Groups
     * Store Campaigns or Update
     *
     * @param $account - account model
     */
    private function importAdGroups($account)
    {
        foreach ($account->campaigns as $campaign) {
            $ad_groups = PublisherEditorFactory::adGroup($account)->setCampaignId($campaign->campaign_id)
                                               ->getAdGroups();
            $this->insertAdGroups($account->id, $campaign->id, $ad_groups);
        }
    }

    /**
     * Insert ad groups to array
     *
     * @param $account_id
     * @param $campaign_id
     * @param $ad_groups
     *
     * @return null
     */
    private function insertAdGroups($account_id, $campaign_id, $ad_groups)
    {
        $values = [];

        if (!isset($ad_groups->entries)) {
            return null;
        }

        foreach ($ad_groups->entries as $ad_group) {
            $values[] = [
                'account_id'         => $account_id,
                'campaign_id'        => $campaign_id,
                'campaign_source_id' => null, //TODO after add bing accounts
                'publisher_id'       => $this->publisher_id,
                'ad_group_source_id' => $ad_group->id,
                'ad_group_name'      => $ad_group->name,
                'status'             => $ad_group->status == 'ENABLED' ? 1 : 0
            ];
        }

        $this->campaigns = array_merge($values, $this->campaigns);
    }

    /**
     * Store or update db
     */
    private function storeCampaigns()
    {
        $count_insert = 0;
        foreach (array_chunk($this->campaigns, 2000) as $records) {
            $builder = \DB::connection($this->connection)->table($this->table_name);
            $count_insert += insertOnDuplicateKeyUpdate($builder, $records);
        }

        $this->logger->info(['Records inserted: ' . $count_insert . 'Counts rows: ' . count($this->campaigns) . PHP_EOL]);
    }

    /**
     * @Override parent method
     *
     * @return null
     */
    public function hasData()
    {
        return null;
    }

    protected function getReportRequestSettings(ReportRequest $request)
    {
    }
}
