<?php namespace App\Libraries\Scraper\Requests\AgePerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Google
use App\Libraries\Scraper\Requests\AgePerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\AgePerformance\Providers\Google\ConversionProvider as GoogleConversionProvider;

use App\Libraries\Scraper\Requests\AgePerformance\Providers\Google\Translators\AgeConversionPerformanceReport as GoogleAgeConversionPerformanceReport;
use App\Libraries\Scraper\Requests\AgePerformance\Providers\Google\Translators\AgePerformanceReport as GoogleAgePerformanceReport;
// GNG
use App\Libraries\Scraper\Requests\AgePerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\AgePerformance\Providers\GoogleGNG\ConversionProvider as GoogleGNGConversionProvider;

use App\Libraries\Scraper\Requests\AgePerformance\Providers\GoogleGNG\Translators\AgeConversionPerformanceReport as GoogleGNGAgeConversionPerformanceReport;
use App\Libraries\Scraper\Requests\AgePerformance\Providers\GoogleGNG\Translators\AgePerformanceReport as GoogleGNGAgePerformanceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 121;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google'            => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleAgePerformanceReport::class
            ]
        ],
        'google-conversion' => [
            'class'       => GoogleConversionProvider::class,
            'translators' => [
                GoogleAgeConversionPerformanceReport::class
            ]
        ],
//        'googleGNG'            => [
//            'class'       => GoogleGNGProvider::class,
//            'translators' => [
//                GoogleGNGAgePerformanceReport::class
//            ]
//        ],
//        'googleGNG-conversion' => [
//            'class'       => GoogleGNGConversionProvider::class,
//            'translators' => [
//                GoogleGNGAgeConversionPerformanceReport::class
//            ]
//        ]
    ];
}




