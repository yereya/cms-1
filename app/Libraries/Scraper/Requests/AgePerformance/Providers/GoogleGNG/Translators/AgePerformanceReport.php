<?php namespace App\Libraries\Scraper\Requests\AgePerformance\Providers\GoogleGNG\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class SearchQueryPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators
 */
class AgePerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_age_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 32;

    /**
     * @var string $provider
     */
    protected $provider = 'GoogleGNG';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'age_range',
            'account_id',
            'device',
            'ad_group_id',
            'stats_date_tz'
        ];
    }

}
