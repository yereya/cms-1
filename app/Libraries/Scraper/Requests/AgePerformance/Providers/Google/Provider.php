<?php namespace App\Libraries\Scraper\Requests\AgePerformance\Providers\Google;

use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleReport
{

    /**
     * @var string $report_type
     */
    protected $report_type = 'AGE_RANGE_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 32;

    /**
     * @var string
     */
    protected $report_folder_name = 'AgePerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $ReportDefinition = new ReportDefinition();
        $ReportDefinition->setSelector($selector);
        $ReportDefinition->setReportName('Age Range Performance Report #' . uniqid());
        $ReportDefinition->setDateRangeType($this->date_range_type);
        $ReportDefinition->setReportType($this->report_type);
        $ReportDefinition->setDownloadFormat('CSV');

        return $ReportDefinition;
    }

}