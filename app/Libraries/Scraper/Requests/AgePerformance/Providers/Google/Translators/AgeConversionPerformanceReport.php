<?php namespace App\Libraries\Scraper\Requests\AgePerformance\Providers\Google\Translators;

/**
 * Class SearchQueryConversionPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators
 */
class AgeConversionPerformanceReport extends AgePerformanceReport
{
    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_age_conversion_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 33;

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            if (is_null($row)) {
                continue;
            }
            if (property_exists($stream, 'date')) {
                $row['_ReportDate'] = $stream->date;
            }
            //in this report conversion value in format 1,097.45 
            $row['ConversionValue'] = str_replace(',', '', $row['AllConversionValue']);

            $rows[] = $this->getRowValues($row);
        }

        return $rows;
    }

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'age_range',
            'account_id',
            'device',
            'ad_group_id',
            'conversion_type',
            'stats_date_tz'
        ];
    }
}


