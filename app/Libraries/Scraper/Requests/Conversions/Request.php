<?php namespace App\Libraries\Scraper\Requests\Conversions;

use App\Libraries\Scraper\Request as BaseRequest;
// Bing
use App\Libraries\Scraper\Requests\Conversions\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\Conversions\Providers\Bing\Translators\Conversion as BingConversion;
// Bing GNG
use App\Libraries\Scraper\Requests\Conversions\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\Conversions\Providers\BingGNG\Translators\Conversion as BingGNGConversion;
// Google
use App\Libraries\Scraper\Requests\Conversions\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\Conversions\Providers\Google\Translators\Conversion as GoogleConversion;
// Google GNG
use App\Libraries\Scraper\Requests\Conversions\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\Conversions\Providers\GoogleGNG\Translators\Conversion as GoogleGNGConversion;
// Outbrain
use App\Libraries\Scraper\Requests\Conversions\Providers\Outbrain\Provider as OutbrainProvider;
use App\Libraries\Scraper\Requests\Conversions\Providers\Outbrain\Translators\Conversion as OutbrainConversion;
// Taboola
use App\Libraries\Scraper\Requests\Conversions\Providers\Taboola\Provider as TaboolaProvider;
use App\Libraries\Scraper\Requests\Conversions\Providers\Taboola\Translators\Conversion as TaboolaConversion;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 26;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class' => GoogleProvider::class,
            'translators' => [
                GoogleConversion::class
            ]
        ],
        'googleGNG' => [
            'class' => GoogleGNGProvider::class,
            'translators' => [
                GoogleGNGConversion::class
            ]
        ],
        'bing' => [
            'class' => BingProvider::class,
            'translators' => [
                BingConversion::class
            ]
        ],
        'bingGNG' => [
            'class' => BingGNGProvider::class,
            'translators' => [
                BingGNGConversion::class
            ]
        ],
        'taboola' => [
            'class' => TaboolaProvider::class,
            'translators' => [
                TaboolaConversion::class
            ]
        ],
        'outbrain' => [
            'class' => OutbrainProvider::class,
            'translators' => [
                OutbrainConversion::class
            ]
        ]
    ];
}
