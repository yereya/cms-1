<?php namespace App\Libraries\Scraper\Requests\Conversions\Providers\Outbrain\Translators;

use App\Libraries\Scraper\Translator;

class Conversion extends Translator
{
    /**
     * @var int $report_id
     */
    protected $report_id = 36;

    /**
     * @var string $provider
     */
    protected $provider = 'Outbrain';
}
