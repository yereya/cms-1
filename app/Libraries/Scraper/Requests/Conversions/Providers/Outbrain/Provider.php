<?php namespace App\Libraries\Scraper\Requests\Conversions\Providers\Outbrain;

//use Google\AdsApi\AdWords\v201809\cm\ConversionTrackerOperation;
//use Google\AdsApi\AdWords\v201809\cm\ConversionTrackerService;
//use Google\AdsApi\AdWords\v201809\cm\OfflineConversionFeed;
//use Google\AdsApi\AdWords\v201809\cm\OfflineConversionFeedOperation;
//use Google\AdsApi\AdWords\v201809\cm\OfflineConversionFeedService;
//use Google\AdsApi\AdWords\v201809\cm\Selector;
//use Google\AdsApi\AdWords\v201809\cm\UploadConversion;
use App\Entities\Repositories\Bo\ConversionRepo;
use Illuminate\Support\Collection;
use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\Conversion;
//use Google\AdsApi\AdWords\AdWordsServices;
use App\Libraries\Scraper\Providers\OutbrainReport;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends OutbrainReport
{
    /**
     * @var int
     */
    protected $report_id = 36;

    /**
     * Report name
     *
     * @var string
     */
    protected $report_type = 'CONVERSION_UPLOAD';

    /**
     * @var - Collections of conversions
     */
    private $conversions;

    /**
     * Marketer Id
     *
     * @var string
     */
    private $marketer_id;
    /*
     * TODO - test account with duplicate name
     */
    protected $account_id = null;

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->marketer_id = '00049c19e8bc8b0ac2023b413c0cb7a3cf';
        $this->conversions = $this->getConversions();
        $this->execute();
    }

    /**
     * Get Conversions
     *
     * @return Collection
     */
    private function getConversions()
    {
        return Conversion::where('source_account_id', '!=', 0)
            ->whereStatus(0)
            ->whereNull('error')
            ->wherePublisherIs('outbrain')
            ->limit(200)
            ->get()
            ->groupBy('source_account_id');
    }

    /**
     * Execute functions
     * @method SetClientCustomerId - init google client server per account
     * @method createConversionTracker - create operations and request to google (only google click id is null)
     * @method createOfflineConversionFeed - create offline operations
     * @method removeSentConversions - change status
     */
    private function execute()
    {
        /** @var Collection $this */
        if (!empty($this->conversions)) {
            if (!$this->conversions->count()) {
                print("\n No conversions to add\n\n");

                return false;
            }
        }

        foreach ($this->conversions as $account_id => $items) {

            $this->account_id = $account_id;
            foreach ($items as $item){
                try{
                    $conversionUrl = $this->buidConversionUrl($item);
                    $response = $this->sendConversion($conversionUrl);
                    $this->updateConversionStatus($item, $response);
                }catch (\Exception $e){
                    $this->logger->warning([
                        'account: ' . $account_id,
                        'thrown with the following error: ',
                        $e->getMessage()
                    ]);
                }

            }

            $this->parseString("Finish execution with " . count($items) . " affected accounts");
        }
    }
    /**
     * Create Conversion URL
     * @param $item
     * @return string
     */
    private function buidConversionUrl($item){

        $data = $item->attributesToArray();
        $url = 'https://tr.outbrain.com/pixel?'
            .  'ob_click_id=' . $data['clkid']
            . '&name=' . $data['event'] //; # .'&orderValue=' . $data['price'] ;
            . '&marketerId=' .  $this->marketer_id;
        return $url;
    }
    /**
     * Send Conversion to Outbrain
     * @param $item
     * @return string
     */
    private function sendConversion($conversionUrl){
        $response = file_get_contents($conversionUrl);
        return $response;
    }
    /**
     * Update conversion's status in the DB from 0 to 1
     * @param $response
     */
    private function updateConversionStatus($conversion, $response){
        $model_repo   = (new ConversionRepo());
        $model_repo->_save($conversion);

    }
    /**
     * @param $string
     * @param $id
     */
    public function parseString($string, $id = null)
    {
        print("\n\n $string " . ($id ?? ' ') . "\n\n");
        $this->logger->info(["$string ", $id ?? $id]);
    }


    /**
     * Get Conversion Tracker Known Errors
     *
     * @param \SoapFault $exception
     * @param            $items
     * @param            $operations
     *
     * @throws \Exception
     */
    private function getConversionTrackerKnownErrors($exception, $items, $operations = null)
    {
        $errors = $exception->getErrors();
        foreach ($errors as $error) {
            $reason = $error->getReason();
            switch ($reason) {
                case 'RATE_EXCEEDED':
                    // Rate exceeded means we tried to connect to early
                    // try again in 30 seconds
                    $this->logger->warning($error);
                    break;
                case 'REQUIRED':
                    // remove from current list
                    // not change status
                    $position = $this->extractOperandIndex($error);
                    $items->forget($position);
                    break;
                case 'CUSTOMER_NOT_FOUND':
                    // account id not found
                    $this->logger->error(['account' => $this->account_id, 'error' => $error]);
                    break;
                case 'TOO_LOW': // Do nothing - this error how DUPLICATE_NAME
                case 'DUPLICATE_NAME': //this error duplicate sent conversions type
                case 'EXPIRED_CLICK': // this error old click
                case 'TOO_RECENT_CLICK': //This click occurred less than 24 hours ago, so it's too recent to be uploaded.
                case 'UNPARSEABLE_GCLID':
                case 'INVALID_CONVERSION_TYPE':
                case 'CONVERSION_PRECEDES_CLICK':
                case 'FUTURE_CONVERSION_TIME':
                case 'CLICK_MISSING_CONVERSION_LABEL':
                    $position = $this->extractOperandIndex($error);
                    $this->saveTooRecentClick($items->get($position), $error->getErrorString());
                    $items->forget($position);
                    break;
                default:
                    throw new \Exception($error->getErrorString());
            }
        }
    }

    /**
     * Get error positions in operation
     *
     * @param $error
     *
     * @return mixed
     */
    private function extractOperandIndex($error)
    {
        // syntax example: "operations[1].operand"
        return preg_replace('/[^0-9]/', '', $error->getFieldPath());
    }

    /**
     * Save conversions with error TOO_RECENT_CLICK
     *
     * @param $item - conversion
     * @param $error - error in google
     */
    private function saveTooRecentClick($item, $error)
    {
        $item->status = 1;
        $item->error = $error;
        $item->save();
    }


    /**
     * Get Offline Conversion Feed Operations
     *
     * @param $items
     *
     * @return array
     */
    private function getOfflineConversionFeedOperations($items)
    {
        $operations = [];
        foreach ($items as $item) {
            $operations[] = $this->getOfflineConversionFeedOperation($item);
        }

        return $operations;
    }

    /**
     * Get Offline Conversion Feed Operation
     *
     * @param array $item
     *
     * @return OfflineConversionFeedOperation
     */
    private function getOfflineConversionFeedOperation($item)
    {
        $operation = new OfflineConversionFeedOperation();
        $operation->setOperator('ADD');
        $operation->setOperand($this->getOfflineConversionFeedOperationFeed($item));

        return $operation;
    }

    /**
     * Get Offline Conversion Feed Operation Feed
     *
     * @param $item
     *
     * @return OfflineConversionFeed
     */
    private function getOfflineConversionFeedOperationFeed($item): OfflineConversionFeed
    {
        $feed = new OfflineConversionFeed();
        $feed->setConversionName($item['event']);
        $feed->setConversionTime(date('Ymd His', strtotime($item['timestamp'])) . ' UTC');
        $feed->setConversionValue($item['price']);
        $feed->setGoogleClickId($item['clkid']);

        return $feed;
    }

    /**
     * Remove Sent Conversions
     *
     * @param $items
     * @param $response
     */
    private function removeSentConversions($items, $response = null)
    {
        if (!is_null($response) && !empty($response)) {
            $response_items = $this->createResponseResult($response);
        }
        foreach ($items as $item) {
            if (!is_null($response) && !empty($response)) {
                $item->response = 'Success';
                $item->status = 1;
                $item->save();
            }
        }

        $this->logger->info(['Updated status conversions count', count($items), 'account: ' . $this->account_id]);
    }

    /**
     * Create Response Collection from UploadConversion object
     *
     * @param $response - UploadConversion object
     *
     * @return Collection
     */
    private function createResponseResult($response)
    {
        $collection = new Collection();
        foreach ($response->getValue() as $res) {
            $collection->put($res->getGoogleClickId(), $res);
        }

        return $collection;
    }

    /**
     * Set Selector
     */
    protected function setSelector()
    {
        // TODO: Implement setReportDefinition() method.
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        // TODO: Implement setReportDefinition() method.
    }

    /**
     * Remove Duplicate
     * 1) found operation
     * 2) found type and event
     * 3) get conversion
     * 4) remove
     *
     * @param $error
     * @param $operations
     */
    private function removeSentDuplicateConversion($error, $operations)
    {
        $operation = $operations[$this->extractOperandIndex($error)];
        $category = $operation->operand->category;
        $type = $this->getTypeByCategory($category);
        $event = $this->getEventByCategory($category, $type);

        $this->removeDuplicateConversion($this->getConversionsToChange($type, $event));
    }

    /**
     * Get type name
     *
     * @param $category
     *
     * @return string
     */
    private function getTypeByCategory($category)
    {
        return $category == 'PAGE_VIEW' ? 'click' : 'event';
    }

    /**
     * Get event name
     *
     * @param $category
     * @param $type
     *
     * @return null|string
     */
    private function getEventByCategory($category, $type)
    {
        if ($type == 'event') {
            return $category == 'PURCHASE' ? 'sale' : 'lead';
        } else {
            return null;
        }
    }

    /**
     * Remove Duplicate Conversions
     *
     * @param $items
     */
    private function removeDuplicateConversion($items)
    {
        foreach ($items as $item) {
            $item->status = 1;
            $item->save();
        }

        $this->logger->info(['Updated status conversions with error "Duplicate name" count', count($items)]);
    }

    /**
     * Get conversion
     *
     * @param $type
     * @param $event
     *
     * @return mixed
     */
    private function getConversionsToChange($type, $event)
    {
        $model = Conversion::where('source_account_id', $this->account_id)->where('type', $type);

        if (!is_null($event)) {
            $model->where('event', $event);
        }

        return $model->get();
    }
}
