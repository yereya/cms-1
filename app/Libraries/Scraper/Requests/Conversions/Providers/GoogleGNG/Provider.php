<?php namespace App\Libraries\Scraper\Requests\Conversions\Providers\GoogleGNG;

use Google\AdsApi\AdWords\v201809\cm\ConversionTrackerOperation;
use Google\AdsApi\AdWords\v201809\cm\ConversionTrackerService;
use Google\AdsApi\AdWords\v201809\cm\OfflineConversionFeed;
use Google\AdsApi\AdWords\v201809\cm\OfflineConversionFeedOperation;
use Google\AdsApi\AdWords\v201809\cm\OfflineConversionFeedService;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\cm\UploadConversion;
use Illuminate\Support\Collection;
use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\Conversion;
use Google\AdsApi\AdWords\AdWordsServices;
use App\Libraries\Scraper\Providers\GoogleGNGReport;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleGNGReport
{
    /**
     * @var int
     */
    protected $report_id = 18;

    /**
     * Report name
     *
     * @var string
     */
    protected $report_type = 'CONVERSION_UPLOAD';

    /**
     * @var - Collections of conversions
     */
    private $conversions;

    /*
     * TODO - test account with duplicate name
     */
    private $account_id = null;

    /**
     * @var - Path to logs' storage path
     */
    private $requests = [];
    /**
     * Fetch
     */
    public function fetch()
    {
        $this->conversions = $this->getConversions();
        $this->execute();
    }

    /**
     * Get Conversions
     *
     * @return Collection
     */
    private function getConversions()
    {
        return Conversion::where('source_account_id', '!=', 0)
            ->whereStatus(0)
            ->whereNull('error')
            ->wherePublisherIs('googleGNG')
            ->limit(200)
            ->get()
            ->groupBy('source_account_id');
    }

    /**
     * Execute functions
     * @method SetClientCustomerId - init google client server per account
     * @method createConversionTracker - create operations and request to google (only google click id is null)
     * @method createOfflineConversionFeed - create offline operations
     * @method removeSentConversions - change status
     */
    private function execute()
    {
        foreach ($this->conversions as $account_id => $items) {
            $this->account_id = $account_id;

            try {
                $this->createConversionTracker($items, $account_id);
                $response = $this->createOfflineConversionFeed($items, $account_id);
                $this->removeSentConversions($items, $response);
            } catch (\Exception $e) {
                $this->logger->warning(['Not saved this account: ' . $account_id, $e->getMessage()]);
            }
        }
    }

    /**
     * Create Conversion Tracker
     *
     * @param array  $items
     * @param string $account_id
     *
     * @return bool
     */
    private function createConversionTracker($items, $account_id)
    {
        $ad_words_services = new AdWordsServices();
        $service = $ad_words_services->get($this->makeSession($account_id), ConversionTrackerService::class);

        $operations = $this->getConversionTrackerOperations($items);

        try {
            if (count($operations) > 0) {
                $service->mutate($operations);
            }
        } catch (\Exception $exception) {
            $this->getConversionTrackerKnownErrors($exception, $items, $operations);
        }
    }

    /**
     * Get Conversion Tracker Operations
     *
     * @param $items
     *
     * @return array
     */
    private function getConversionTrackerOperations($items)
    {
        $operations = [];

        foreach ($items->unique('event') as $item) {
            if (empty($item->clkid)) {
                $operations[] = $this->getConversionTrackerOperation($item);
            }
        }

        return $operations;
    }

    /**
     * Get Conversion Tracker Operation
     *
     * @param $item
     *
     * @return ConversionTrackerOperation
     */
    private function getConversionTrackerOperation($item)
    {
        $operation = new ConversionTrackerOperation();
        $operation->setOperand($this->getUploadConversion($item));
        $operation->setOperator('ADD');

        return $operation;
    }

    /**
     * Get Upload Conversion
     *
     * @param array $item
     *
     * @return UploadConversion
     */
    private function getUploadConversion($item): UploadConversion
    {
        $conversion = new UploadConversion();
        $conversion->setName($item['event']);
        $conversion->setCategory($this->getConversionTrackerCategory($item));
        $conversion->setCtcLookbackWindow(90);
        $conversion->setViewthroughLookbackWindow(30);

        return $conversion;
    }

    /**
     * Get Conversion Tracker Category
     *
     * @param $item
     *
     * @return string
     */
    private function getConversionTrackerCategory($item)
    {
        if ($item['type'] == 'click') {
            $category = 'PAGE_VIEW';
        } elseif ($item['type'] == 'event') {
            if ($item['event'] == 'sale') {
                $category = 'PURCHASE';
            } else {
                $category = 'LEAD';
            }
        } else {
            $category = 'DEFAULT';
        }

        return $category;
    }

    /**
     * Get Conversion Tracker Known Errors
     *
     * @param \SoapFault $exception
     * @param            $items
     * @param            $operations
     *
     * @throws \Exception
     */
    private function getConversionTrackerKnownErrors($exception, $items, $operations = null)
    {
        $errors = $exception->getErrors();
        foreach ($errors as $error) {
            $reason = $error->getReason();

            # -------------------------------  Store Log  ---------------------------------

            try{
                $items_with_errors = [];
                $report_file_path_full = storage_path('logs') . DIRECTORY_SEPARATOR . 'conversion_logs.csv';
                $conversion_req_headers = array('googleClickId','conversionName','conversionTime','conversionValue',
                    'conversionCurrencyCode','externalAttributionCredit','externalAttributionModel',
                    'operator','OperationType','partialFailureErrors','ListReturnValueType','uploadConversionTime','error');

                if(!\File::exists($report_file_path_full)){
                    \File::put($report_file_path_full, implode(',',$conversion_req_headers), true);
                }
//            $time = date("Y/m/d H:i:s");
                $time = date('Ymd His', strtotime(date("Y/m/d H:i:s"))) . ' UTC';
                $error_index = $error->getFieldPathElements()[0]->getIndex();
                $item = $items[$error_index]->toArray();
                $items_with_errors['clickId'] =  $item['clkid']; # Todo

                $row['googleClickId'] =  $item['clkid'];
                $row['conversionName'] =  $item['event'];
                $row['conversionTime'] =  date('Ymd His', strtotime($item['timestamp'])) . ' UTC';
                $row['conversionValue'] =  $item['price'];
                $row['conversionCurrencyCode'] =  '';
                $row['externalAttributionCredit'] =  '';
                $row['externalAttributionModel'] =  '';
                $row['operator'] =  'ADD';
                $row['OperationType'] =  '';
                $row['partialFailureErrors'] =  '';
                $row['ListReturnValueType'] =  '';
                $report_file_path_full = storage_path('logs') . DIRECTORY_SEPARATOR . 'conversion_logs.csv';
                file_put_contents($report_file_path_full, PHP_EOL . implode(',',$row).','.$time.','.$error->getErrorString(), FILE_APPEND);
            }catch (\Exception $e){
                $this->logger->warning(['Error in saving log(line 239 Conversions Google Provider) : ' . $e->getMessage()]);
            }

            # -------------------------------  / Store Log  ---------------------------------

            switch ($reason) {
                case 'RATE_EXCEEDED':
                    // Rate exceeded means we tried to connect to early
                    // try again in 30 seconds
                    $this->logger->warning($error);
                    break;
                case 'REQUIRED':
                    // remove from current list
                    // not change status
                    $position = $this->extractOperandIndex($error);
                    $items->forget($position);
                    break;
                case 'CUSTOMER_NOT_FOUND':
                    // account id not found
                    $this->logger->error(['account' => $this->account_id, 'error' => $error]);
                    break;
                case 'TOO_LOW': // Do nothing - this error how DUPLICATE_NAME
                case 'DUPLICATE_NAME': //this error duplicate sent conversions type
                case 'EXPIRED_CLICK': // this error old click
                case 'TOO_RECENT_CLICK': //This click occurred less than 24 hours ago, so it's too recent to be uploaded.
                case 'UNPARSEABLE_GCLID':
                case 'INVALID_CONVERSION_TYPE':
                case 'CONVERSION_PRECEDES_CLICK':
                case 'FUTURE_CONVERSION_TIME':
                case 'CLICK_MISSING_CONVERSION_LABEL':
                    $position = $this->extractOperandIndex($error);
                    $this->saveTooRecentClick($items->get($position), $error->getErrorString());
                    $items->forget($position);
                    break;
                default:
                    throw new \Exception($error->getErrorString());
            }
        }
        # -------------------------------    Store Log For items which haven't been sent  ---------------------------------

        $row = [];
        foreach ($items as $item){
            if(!in_array($item['clkid'],$items_with_errors)){
                $row['googleClickId'] =  $item['clkid'];
                $row['conversionName'] =  $item['event'];
                $row['conversionTime'] =  date('Ymd His', strtotime($item['timestamp'])) . ' UTC';
                $row['conversionValue'] =  $item['price'];
                $row['conversionCurrencyCode'] =  '';
                $row['externalAttributionCredit'] =  '';
                $row['externalAttributionModel'] =  '';
                $row['operator'] =  'ADD';
                $row['OperationType'] =  '';
                $row['partialFailureErrors'] =  '';
                $row['ListReturnValueType'] =  '';
                $report_file_path_full = storage_path('logs') . DIRECTORY_SEPARATOR . 'conversion_logs.csv';
                file_put_contents($report_file_path_full, PHP_EOL . implode(',',$row).','.$time.',,'. 'Have not been sent because of an error in the chunk', FILE_APPEND);
            }
        }

        # -------------------------------  / Store Log  ---------------------------------
    }

    /**
     * Get error positions in operation
     *
     * @param $error
     *
     * @return mixed
     */
    private function extractOperandIndex($error)
    {
        // syntax example: "operations[1].operand"
        return preg_replace('/[^0-9]/', '', $error->getFieldPath());
    }

    /**
     * Save conversions with error TOO_RECENT_CLICK
     *
     * @param $item - conversion
     * @param $error - error in google
     */
    private function saveTooRecentClick($item, $error)
    {
        $item->status = 1;
        $item->error = $error;
        $item->save();
    }

    /**
     * Create Offline Conversion Feed
     *
     * @param array  $items
     * @param string $account_id
     *
     * @return bool
     */
    private function createOfflineConversionFeed($items, $account_id)
    {
        $response = [];
        $ad_words_services = new AdWordsServices();
        $service = $ad_words_services->get($this->makeSession($account_id), OfflineConversionFeedService::class);

        try {
            $operations = $this->getOfflineConversionFeedOperations($items);
            $response = $service->mutate($operations); # TODO uncomment
        } catch (\Exception $exception) {
            $this->getConversionTrackerKnownErrors($exception, $items, $operations);
        }

        return $response; # TODO remove
//        return true;
    }

    private function store_log($response = null)
    {
        try{
            $requests = [];
//        $time = date("Y/m/d H:i:s");
            $time = date('Ymd His', strtotime(date("Y/m/d H:i:s"))) . ' UTC';
            $report_file_path_full = storage_path('logs') . DIRECTORY_SEPARATOR . 'conversion_logs.csv';
            $conversion_req_headers = array('googleClickId','conversionName','conversionTime','conversionValue',
                'conversionCurrencyCode','externalAttributionCredit','externalAttributionModel',
                'operator','OperationType','partialFailureErrors','ListReturnValueType','uploadConversionTime','error');

            if(!\File::exists($report_file_path_full)){
                \File::put($report_file_path_full, implode(',',$conversion_req_headers), true);
            }
            if($response){
                foreach ($response->getValue() as $index => $item) {
                    $request = [];
//            $operand = $item->getOperand();
                    $request['googleClickId'] = $item->getGoogleClickId();
                    $request['conversionName'] = $item->getConversionName();
                    $request['conversionTime'] = $item->getConversionTime();
                    $request['conversionValue'] = $item->getConversionValue();
                    $request['conversionCurrencyCode'] = $item->getConversionCurrencyCode();
                    $request['conversionCurrencyCode'] = $item->getConversionCurrencyCode();
                    $request['externalAttributionCredit'] = $item->getExternalAttributionCredit();
                    $request['externalAttributionModel'] = $item->getExternalAttributionModel();
                    $request['operator'] =  'ADD';
                    $request['OperationType'] =  '';
                    if($response->getPartialFailureErrors()){
                        $request['partialFailureErrors'] = 'Partial Errors';
                    }else{
                        $request['partialFailureErrors'] = '';
                    }
                    $request['ListReturnValueType'] = $response->getListReturnValueType();
                    $request['time'] = $time;
                    file_put_contents($report_file_path_full, PHP_EOL . implode(',',$request), FILE_APPEND);
                    $requests[$index] = $request;
                }
            }
        }catch (\Exception $e){
            $this->logger->warning(['Error in saving log(line 239 Conversions Google Provider) : ' . $e->getMessage()]);
        }


    }
    /**
     * Get Offline Conversion Feed Operations
     *
     * @param $items
     *
     * @return array
     */
    private function getOfflineConversionFeedOperations($items)
    {
        $operations = [];
        foreach ($items as $item) {
            $operations[] = $this->getOfflineConversionFeedOperation($item);
        }

        return $operations;
    }

    /**
     * Get Offline Conversion Feed Operation
     *
     * @param array $item
     *
     * @return OfflineConversionFeedOperation
     */
    private function getOfflineConversionFeedOperation($item)
    {
        $operation = new OfflineConversionFeedOperation();
        $operation->setOperator('ADD');
        $operation->setOperand($this->getOfflineConversionFeedOperationFeed($item));

        return $operation;
    }

    /**
     * Get Offline Conversion Feed Operation Feed
     *
     * @param $item
     *
     * @return OfflineConversionFeed
     */
    private function getOfflineConversionFeedOperationFeed($item): OfflineConversionFeed
    {
        $feed = new OfflineConversionFeed();
        $feed->setConversionName($item['event']);
        $feed->setConversionTime(date('Ymd His', strtotime($item['timestamp'])) . ' UTC');
        $feed->setConversionValue($item['price']);
        $feed->setGoogleClickId($item['clkid']);

        return $feed;
    }

    /**
     * Remove Sent Conversions
     *
     * @param $items
     * @param $response
     */
    private function removeSentConversions($items, $response = null)
    {
        if (!is_null($response) && !empty($response)) {
            $response_items = $this->createResponseResult($response);
        }
        foreach ($items as $item) {
            if (!is_null($response) && !empty($response)) {
                $item->response = 'Success';
                $item->status = 1;
                $item->save();
            }
        }
        # Store conversion's logs in csv file for google

        if($response){
            $this->store_log($response);
        }


        $this->logger->info(['Updated status conversions count', count($items), 'account: ' . $this->account_id]);
    }

    /**
     * Create Response Collection from UploadConversion object
     *
     * @param $response - UploadConversion object
     *
     * @return Collection
     */
    private function createResponseResult($response)
    {
        $collection = new Collection();
        foreach ($response->getValue() as $res) {
            $collection->put($res->getGoogleClickId(), $res);
        }

        return $collection;
    }

    /**
     * Set Selector
     */
    protected function setSelector()
    {
        // TODO: Implement setReportDefinition() method.
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        // TODO: Implement setReportDefinition() method.
    }

    /**
     * Remove Duplicate
     * 1) found operation
     * 2) found type and event
     * 3) get conversion
     * 4) remove
     *
     * @param $error
     * @param $operations
     */
    private function removeSentDuplicateConversion($error, $operations)
    {
        $operation = $operations[$this->extractOperandIndex($error)];
        $category = $operation->operand->category;
        $type = $this->getTypeByCategory($category);
        $event = $this->getEventByCategory($category, $type);

        $this->removeDuplicateConversion($this->getConversionsToChange($type, $event));
    }

    /**
     * Get type name
     *
     * @param $category
     *
     * @return string
     */
    private function getTypeByCategory($category)
    {
        return $category == 'PAGE_VIEW' ? 'click' : 'event';
    }

    /**
     * Get event name
     *
     * @param $category
     * @param $type
     *
     * @return null|string
     */
    private function getEventByCategory($category, $type)
    {
        if ($type == 'event') {
            return $category == 'PURCHASE' ? 'sale' : 'lead';
        } else {
            return null;
        }
    }

    /**
     * Remove Duplicate Conversions
     *
     * @param $items
     */
    private function removeDuplicateConversion($items)
    {
        foreach ($items as $item) {
            $item->status = 1;
            $item->save();
        }

        $this->logger->info(['Updated status conversions with error "Duplicate name" count', count($items)]);
    }

    /**
     * Get conversion
     *
     * @param $type
     * @param $event
     *
     * @return mixed
     */
    private function getConversionsToChange($type, $event)
    {
        $model = Conversion::where('source_account_id', $this->account_id)->where('type', $type);

        if (!is_null($event)) {
            $model->where('event', $event);
        }

        return $model->get();
    }
}
