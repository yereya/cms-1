<?php namespace App\Libraries\Scraper\Requests\Conversions\Providers\Taboola;


use App\Entities\Repositories\Bo\ConversionRepo;
use Illuminate\Support\Collection;
use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\Conversion;
use App\Libraries\Scraper\Providers\TaboolaReport;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends TaboolaReport
{
    /**
     * @var int
     */
    protected $report_id = 37;

    /**
     * Report name
     *
     * @var string
     */
    protected $report_type = 'CONVERSION_UPLOAD';

    /**
     * @var - Collections of conversions
     */
    private $conversions;

    /*
     * TODO - test account with duplicate name
     */
    protected $account_id = null;

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->conversions = $this->getConversions();
        $this->execute();
    }

    /**
     * Get Conversions
     *
     * @return Collection
     */
    private function getConversions()
    {
        return Conversion::where('source_account_id', '!=', 0)
            ->whereStatus(0)
            ->whereNull('error')
            ->wherePublisherIs('taboola')
            ->limit(200)
            ->get()
            ->groupBy('source_account_id');
    }

    /**
     * Execute functions
     * @method SetClientCustomerId - init google client server per account
     * @method createConversionTracker - create operations and request to google (only google click id is null)
     * @method createOfflineConversionFeed - create offline operations
     * @method removeSentConversions - change status
     */
    private function execute()
    {
        /** @var Collection $this */
        if (!empty($this->conversions)) {
            if (!$this->conversions->count()) {
                print("\n No conversions to add\n\n");
                return false;
            }
        }

        foreach ($this->conversions as $account_id => $items) {

            $this->account_id = $account_id;
            foreach ($items as $item){
                try{
                    $conversionUrl = $this->buidConversionUrl($item);
                    $response = $this->sendConversion($conversionUrl);
                    $this->updateConversionStatus($item, $response);
                }catch (\Exception $e){
                    $this->logger->warning([
                        'account: ' . $account_id,
                        'thrown with the following error: ',
                        $e->getMessage()
                    ]);
                }
            }
            $this->parseString("Finish execution with " . count($items) . " affected accounts");
        }
    }

    /**
     * Create Conversion URL
     * @param $item
     * @return string
     */
    private function buidConversionUrl($item){
//        Example : https://trc.taboola.com/actions-handler/log/3/s2s-action?click-id=CLICK_ID&name=EVENT_NAME
        $data = $item->attributesToArray();
        $url = 'https://trc.taboola.com/actions-handler/log/3/s2s-action?';
        $event = '';
        switch ($data['event']){
            case 'clickout':
                $event = 'view_content';
                break;
            case 'sale':
                $event = 'make_purchase';
                break;
            case 'lead':
                $event = 'lead';
                break;
        }
        $url .=  'click-id=' . $data['clkid'] . '&name=' . $event . '&revenue=' . $data['price'];
        return $url;
    }

    /**
     * Send Conversion to Outbrain
     * @param $item
     * @return string
     */
    private function sendConversion($conversionUrl){
        $response = file_get_contents($conversionUrl);
        return $response;
    }

    /**
     * Update conversion's status in the DB from 0 to 1
     * @param $response
     */
    private function updateConversionStatus($conversion, $response){
        $model_repo   = (new ConversionRepo());
        $model_repo->_save($conversion);
    }
    /**
     * @param $string
     * @param $id
     */
    public function parseString($string, $id = null)
    {
        print("\n\n $string " . ($id ?? ' ') . "\n\n");
        $this->logger->info(["$string ", $id ?? $id]);
    }

}
