<?php namespace App\Libraries\Scraper\Requests\Conversions\Providers\Taboola\Translators;

use App\Libraries\Scraper\Translator;

class Conversion extends Translator
{
    /**
     * @var int $report_id
     */
    protected $report_id = 37;

    /**
     * @var string $provider
     */
    protected $provider = 'Taboola';
}
