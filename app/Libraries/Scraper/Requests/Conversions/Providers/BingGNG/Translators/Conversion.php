<?php namespace App\Libraries\Scraper\Requests\Conversions\Providers\BingGNG\Translators;

use App\Libraries\Scraper\Translator;

class Conversion extends Translator
{
    /**
     * @var int $report_id
     */
    protected $report_id = 18;

    /**
     * @var string $provider
     */
    protected $provider = 'BingGNG';
}
