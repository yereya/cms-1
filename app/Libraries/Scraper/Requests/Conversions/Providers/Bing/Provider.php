<?php namespace App\Libraries\Scraper\Requests\Conversions\Providers\Bing;

use App\Entities\Repositories\Bo\ConversionRepo;
use App\Libraries\PublisherEditor\Operations\Bing\ParseBingResponse;
use App\Libraries\Scraper\Providers\BingReport;
use Exception;
use Illuminate\Support\Collection;


use Microsoft\BingAds\V13\CampaignManagement\ApplyOfflineConversionsRequest;
use Microsoft\BingAds\V13\CampaignManagement\OfflineConversion;
use Microsoft\BingAds\V13\Reporting\ReportRequest;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{

    /**
     *  Parse Exception trait
     */
    use ParseBingResponse;

    /**
     * @var int
     */
    protected $report_id = 18;

    /**
     * Report name
     *
     * @var string
     */
    protected $report_type = 'CONVERSION_UPLOAD';

    /**
     * @var Collection  Collections of conversions
     */
    private $conversions;

    /*
     * TODO - test account with duplicate name
     */
    private $account_id = null;


    /**
     * @var string
     */
    const START_PROCESS = 'Start conversion process for account id';

    /**
     * @var string
     */
    protected const START_BUILD_OFFLINE_CONVERSION = 'Start building offline conversions...';

    /**
     * @var string
     */
    protected const FINISH_BUILDING_OFFLINE_CONVERSION = 'Finish building offline conversions...';

    /**
     * @var string
     */
    protected const START_UPLOADING_OFFLINE_CONVERSIONS = "Start uploading offline conversions...";

    /**
     * @var string
     */
    protected const FINISH_UPLOADING_OFFLINE_CONVERSION = "Finish uploading offline conversion...";

    /**
     * @var string
     */
    protected const UPLOADING_WAS_FAILED_BECAUSE_OF_THE_FOLLOWING_ERROR = 'Uploading was failed because of the following error: ';


    /**
     * Fetch
     * 1 - provider name
     * 2 - conversions by provider name
     * 3 -  call execute method
     */
    public function fetch()
    {
        $this->conversions = (new ConversionRepo())->getByProvider('bing');
        $this->execute();
    }

    /**
     * Execute:
     * 1.Iterate over the list of conversions per account,   and run over the following methods:
     * @method $this->setAccountId->set account id per conversions
     * @method $this->build->build conversion
     * @method $this->upload->upload conversion
     * @method $this->removeSentRecords->remove the uploaded conversion from list
     */
    private function execute()
    {
        $affected_accounts = [];

        /*Set customer id to bing client*/
        $this->setCustomerId();

        /** @var Collection $this */
        if (!empty($this->conversions)) {
            if (!$this->conversions->count()) {
                print("\n No conversions to add\n\n");

                return false;
            }
        }

        /** @var Collection $this */
        $this->conversions->each(function ($conversion) use (&$affected_accounts) {
            $account_id = $conversion["source_account_id"];
            $this->bing_client->withAccountId($account_id);
            $this->parseString(self::START_PROCESS, $account_id);
                try {
                    $offline_conversion = $this->build($conversion);
                    $response = $this->upload($offline_conversion);
                    $this->actAccordingToResponse($response, $conversion);
                    $affected_accounts[] = $account_id;

                } catch (Exception $e) {
                    $this->logger->warning([
                        'account: ' . $account_id,
                        'thrown with the following error: ',
                        $e->getMessage()
                    ]);
                }
        });

        $this->parseString("Finish execution with " . count($affected_accounts) . " affected accounts");
    }

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        // TODO: Implement getReportRequestSettings() method.
    }


    /**
     * Build conversion before upload
     *
     * @param $conversion
     *
     * @internal $event_to_goal - change the string so it will match the goal name in account goals example: =>
     *           cancelled
     * @return array
     */
    private function build($conversion)
    {
        $this->parseString(self::START_BUILD_OFFLINE_CONVERSION);

        $event_to_goal       = [
            'sale'          => 'Sale',
            'lead'          => 'Lead',
            'call'          => 'Call',
            'canceled_sale' => 'Cancelled Sale',
            'canceled-sale',
            'Cancelled Sale',
            'canceled_lead' => 'Cancelled Lead',
            'canceled-lead',
            'Cancelled Lead',
            'clickout'      => 'Click Out',
            'adjustment' => 'Adjustment'
        ];

        $offline_conversions = [];

        //$conversions->each(function ($conversion) use (&$offline_conversions, $event_to_goal) {
            $offline_conversion = new OfflineConversion();
            // If you do not specify an offline conversion currency code,
            // then the 'CurrencyCode' element of the goal's 'ConversionGoalRevenue' is used.
            $offline_conversion->ConversionCurrencyCode = "USD";

            // The conversion name must match the 'Name' of the 'OfflineConversionGoal'.
            // If it does not match you won't observe any error, although the offline
            // conversion will not be counted.
            $offline_conversion->ConversionName = $event_to_goal[$conversion->event];

            // The date and time must be in UTC, should align to the date and time of the
            // recorded click (MicrosoftClickId), and cannot be in the future.
            $offline_conversion->ConversionTime = gmdate("Y-m-d\TH:i:s", strtotime($conversion->timestamp));

            // If you do not specify an offline conversion value,
            // then the 'Value' element of the goal's 'ConversionGoalRevenue' is used.
            $offline_conversion->ConversionValue = floatval($conversion->price);

            //the MSCLKID for the offline conversion.
            $offline_conversion->MicrosoftClickId = $conversion->clkid;
            $offline_conversions[]                = $offline_conversion;
       // });

        $this->parseString(self::FINISH_BUILDING_OFFLINE_CONVERSION);

        return $offline_conversions;
    }

    /**
     * Set customer id to the bing client
     */
    private function setCustomerId()
    {
        $customer_id = $this->bing_client->getUser()->User->CustomerId;
        $this->bing_client->withCustomerId($customer_id);
    }


    /**
     * @param array $conversions
     *
     * @return mixed
     */
    private
    function upload(array $conversions)
    {
            $this->parseString(self::START_UPLOADING_OFFLINE_CONVERSIONS);

            $request                     = new ApplyOfflineConversionsRequest();
            $request->OfflineConversions = $conversions;

            $response = $this->bing_client->campaignsService()->GetService()->ApplyOfflineConversions($request);

            return $response;
    }

    /**
     * @param $response
     * @param $conversion
     */
    private function actAccordingToResponse($response, $conversion)
    {
        $model_repo   = (new ConversionRepo());
        $error_state  = !empty($response->PartialErrors) && !empty($response->PartialErrors->BatchError);
        $batch_errors = $error_state
            ? (array)$response->PartialErrors->BatchError
            : false;

        //$conversions->each(function ($conversion) use ($model_repo, $batch_errors) {
            $model_repo->_save($conversion, $batch_errors);
            $this->parseOfflineConversions($batch_errors);
        //});
    }
}