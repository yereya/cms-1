<?php namespace App\Libraries\Scraper\Requests\CompetitorReport;

use App\Libraries\Scraper\Requests\CompetitorReport\Providers\SemRush\Provider as SemRushProvider;
use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\CompetitorReport\Providers\SemRush\Translators\BIPublishersReport as SemRushBIPublishersReport;
/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 138;

    /**
     * @var array $providers
     */
    protected $providers = [
        'semrush' => [
            'class'       => SemRushProvider::class,
            'translators' => [
                SemRushBIPublishersReport::class,
            ]
        ],
    ];
}
