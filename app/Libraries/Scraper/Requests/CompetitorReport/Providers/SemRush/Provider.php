<?php namespace  App\Libraries\Scraper\Requests\CompetitorReport\Providers\SemRush;

use App\Libraries\Scraper\Providers\SemRushReport;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends SemRushReport
{
    /**
     * @var int
     */
    protected $report_id = 40;

    /**
     * @var string $report_type
     */
    protected $report_type = 'COMPETITOR_REPORT';

    /**
     * @var string $report_request
     */

    /**
     * @var string
     */
    protected $report_folder_name = 'SemRushReport';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

}
