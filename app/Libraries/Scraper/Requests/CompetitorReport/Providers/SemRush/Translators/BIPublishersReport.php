<?php


namespace App\Libraries\Scraper\Requests\CompetitorReport\Providers\SemRush\Translators;

use App\Entities\Models\Bo\ReportField;
use App\Libraries\Scraper\Translator;
use Illuminate\Support\Collection;
use App\Facades\ShortCode;
use Illuminate\Support\Facades\DB;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\SemRushReport\Providers\SemRush\Translators
 */
class BIPublishersReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_competitor_report_semrush';


    /**
     * @var int $report_id
     */
    protected $report_id = 40;

    /**
     * @var string $provider
     */
    protected $provider = 'Semrush';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'created_at',
            'updated_at'
        ];
    }

    public function store()
    {
        if (!$this->data) {
            return;
        }
        $rows = $this->buildRows($this->data);

        $store_method = 'store_' . $this->options['store_method'];
        $this->$store_method($rows);

    }
    /**
     * Build Rows
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($data)
    {
        $rows = [];
        foreach ($data as $data_row) {
            foreach ($data_row as $domain){
                foreach ($domain as $row){
                    $rows[] = $this->prepareRow($row);
                }
            }
        }
        return $rows;
    }

    /**
     * Prepare Row
     *
     * @param array $row
     *
     * @return mixed
     */
    private function prepareRow($row)
    {
        $fields  = $this->getReportFields();
        foreach ($fields as $field) {
            $new_row[$field->name] = ShortCode::setShortCodes($field->value, $row);
        }

        return $new_row;
    }


}
