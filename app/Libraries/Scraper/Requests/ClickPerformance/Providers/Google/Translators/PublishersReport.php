<?php namespace App\Libraries\Scraper\Requests\ClickPerformance\Providers\Google\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\ClickPerformance\Providers\Google\Translators
 */
class PublishersReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_click_report';

    /**
     * @var int $report_id
     */
    protected $report_id = 7;

    /**
     * @var string $provider
     */
    protected $provider = 'Google';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'google_click_id',
            'account_id',
            'google_click_id',
        ];
    }
}