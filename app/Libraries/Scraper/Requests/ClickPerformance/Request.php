<?php namespace App\Libraries\Scraper\Requests\ClickPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Google
use App\Libraries\Scraper\Requests\ClickPerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\ClickPerformance\Providers\Google\Translators\PublishersReport as GoogleBIPublishersReport;
// GNG
use App\Libraries\Scraper\Requests\ClickPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\ClickPerformance\Providers\GoogleGNG\Translators\PublishersReport as GoogleGNGBIPublishersReport;
/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 29;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleBIPublishersReport::class
            ]
        ],
        'googleGNG' => [
            'class'       => GoogleGNGProvider::class,
            'translators' => [
                GoogleGNGBIPublishersReport::class
            ]
        ]
    ];
}
