<?php namespace App\Libraries\Scraper\Requests\GenderPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Google
use App\Libraries\Scraper\Requests\GenderPerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\GenderPerformance\Providers\Google\ConversionProvider as GoogleConversionProvider;
// Google Conversions
use App\Libraries\Scraper\Requests\GenderPerformance\Providers\Google\Translators\GenderConversionPerformanceReport as GoogleGenderConversionPerformanceReport;
use App\Libraries\Scraper\Requests\GenderPerformance\Providers\Google\Translators\GenderPerformanceReport as GoogleGenderPerformanceReport;
// GNG
use App\Libraries\Scraper\Requests\GenderPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\GenderPerformance\Providers\GoogleGNG\ConversionProvider as GoogleGNGConversionProvider;
// GNG Conversions
use App\Libraries\Scraper\Requests\GenderPerformance\Providers\GoogleGNG\Translators\GenderConversionPerformanceReport as GoogleGNGGenderConversionPerformanceReport;
use App\Libraries\Scraper\Requests\GenderPerformance\Providers\GoogleGNG\Translators\GenderPerformanceReport as GoogleGNGGenderPerformanceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 50;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google'            => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleGenderPerformanceReport::class
            ]
        ],
        'google-conversion' => [
            'class'       => GoogleConversionProvider::class,
            'translators' => [
                GoogleGenderConversionPerformanceReport::class
            ]
        ],
//        'googleGNG'            => [
//            'class'       => GoogleGNGProvider::class,
//            'translators' => [
//                GoogleGNGGenderPerformanceReport::class
//            ]
//        ],
//        'googleGNG-conversion' => [
//            'class'       => GoogleGNGConversionProvider::class,
//            'translators' => [
//                GoogleGNGGenderConversionPerformanceReport::class
//            ]
//        ],
    ];
}
