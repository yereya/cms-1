<?php namespace App\Libraries\Scraper\Requests\GenderPerformance\Providers\Bing\Translators;

use App\Entities\Models\Bo\ChangesLog;
use App\Libraries\Scraper\Translator;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Bing\Translators
 */
class BIPublishersReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string $table_name
     */
    protected $table_name = 'changes_scraper';

    /**
     * Store Row
     *
     * @param array $row
     */
    protected function storeRow($row)
    {
        try {
            ChangesLog::create($row);
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }

    /**
     * Get Row Value
     *
     * @param array $row
     *
     * @return array
     */
    //    protected function getRowValue($row)
    //    {
    //        return [
    //            'publisher_id'          => 97,
    //            'publisher_name'        => 'MSN',
    //            'account_id'            => $row['AccountId'],
    //            'account_name'          => $row['AccountName'],
    //            'campaign_id'           => $row['CampaignId'],
    //            'campaign_name'         => $row['CampaignName'],
    //            'ad_group_id'           => $row['AdGroupId'],
    //            'ad_group_name'         => $row['AdGroupName'],
    //            'keyword_id'            => $row['KeywordId'],
    //            'keyword'               => $row['Keyword'],
    //            'match_type'            => $row['BidMatchType'],
    //            'time_zone'             => date('Y-m-d H:i:s', strtotime($row['TimePeriod'])),
    //            'cost'                  => $this->getCostColumn($row['Spend']),
    //            'max_cpc'               => $this->getMaxCPCColumn($row['CurrentMaxCpc']),
    //            'keyword_state'         => $row['KeywordStatus'],
    //            'campaign_state'        => $row['CampaignStatus'],
    //            'ad_group_state'        => $row['AdGroupStatus'],
    //            'mobile_adjustment_bid' => null,
    //        ];
    //    }
    //    
    //    /**
    //     * Get Cost Column
    //     *
    //     * @param string $value
    //     *
    //     * @return int
    //     */
    //    private function getCostColumn($value)
    //    {
    //        return ((int) $value) / 1000000;
    //    }
    //
    //    /**
    //     * Get Max CPC Column
    //     *
    //     * @param $value
    //     *
    //     * @return int
    //     */
    //    private function getMaxCPCColumn($value)
    //    {
    //        return ((int) $value) / 1000000;
    //    }
}