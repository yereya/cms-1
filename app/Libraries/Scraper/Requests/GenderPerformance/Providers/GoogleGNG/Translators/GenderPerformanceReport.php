<?php namespace App\Libraries\Scraper\Requests\GenderPerformance\Providers\GoogleGNG\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class SearchQueryPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\SearchQueryPerformance\Providers\Google\Translators
 */
class GenderPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_gender_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 10;

    /**
     * @var string $provider
     */
    protected $provider = 'GoogleGNG';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'publisher_id',
            'gender',
            'account_id',
            'device',
            'ad_group_id',
            'stats_date_tz'
        ];
    }

    /**
     * Store
     *
     * @return void
     */
    public function store()
    {
        if (!$this->data) {
            return;
        }

        foreach ($this->data as $account_id => $request) {
            $this->storeRequest($request,$account_id);
        }
    }
    /**
     * Store Request
     *
     * @param array $request
     *
     */
    protected function storeRequest($request, $account_id = null)
    {
        $store_method = 'store_' . $this->options['store_method'];
        $rows = $this->buildRowsWithAccount($request,$account_id);
        $this->$store_method($rows);
    }

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRowsWithAccount($stream,$account_id)
    {
        $rows = [];
        foreach ($stream as $row) {
            if (is_null($row)) {
                continue;
            }
            if(isset($account_id)){
                $row['account_id'] = $account_id;
            }

            if (property_exists($stream, 'date')) {
                $row['_ReportDate'] = $stream->date;
            }

            $rows[] = $this->getRowValues($row);
        }

        return $rows;
    }
}
