<?php namespace App\Libraries\Scraper\Requests\GenderPerformance\Providers\Google;

use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleReport
{

    /**
     * @var string $report_type
     */
    protected $report_type = 'GENDER_PERFORMANCE_REPORT';

    /**
     * @var int
     */
    protected $report_id = 10;

    /**
     * @var string
     */
    protected $report_folder_name = 'GenderPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $ReportDefinition = new ReportDefinition();
        $ReportDefinition->setSelector($selector);
        $ReportDefinition->setReportName('Gender Performance Report #' . uniqid());
        $ReportDefinition->setDateRangeType($this->date_range_type);
        $ReportDefinition->setReportType($this->report_type);
        $ReportDefinition->setDownloadFormat('CSV');

        return $ReportDefinition;
    }

    /**
     * Get Report
     *
     * @param int  $customer_id
     * @param null $date
     *
     * @return void
     */
    protected function getReport($customer_id, $date = null)
    {
        $path_suffix = $date ? '_' . $date : null;

        $this->downloadReport($this->prepareReportDefinition(), $customer_id, $path_suffix);
        $parsed_csv       = $this->parseCsv($this->getReportFilePath($customer_id . $path_suffix));
        $parsed_csv->date = $date;
        $this->data[$customer_id]     = $parsed_csv;

        $this->logger->info('Download customer ' . $customer_id . ' report successful');
    }
}