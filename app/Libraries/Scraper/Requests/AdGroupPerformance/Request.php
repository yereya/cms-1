<?php namespace App\Libraries\Scraper\Requests\AdGroupPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Bing
use App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\Bing\Translators\AdGroupPerformanceReport as BingAdGroupPerformanceReport;
// Google
use App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\Google\Translators\AdGroupPerformanceReport as GoogleAdGroupPerformanceReport;
// Bing GNG
use App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\BingGNG\Translators\AdGroupPerformanceReport as BingGNGAdGroupPerformanceReport;
// Google GNG
use App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\GoogleGNG\Translators\AdGroupPerformanceReport as GoogleGNGAdGroupPerformanceReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 28;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleAdGroupPerformanceReport::class
            ]
        ],
        'bing'   => [
            'class'       => BingProvider::class,
            'translators' => [
                BingAdGroupPerformanceReport::class
            ]
        ],
//        'googleGNG' => [
//            'class'       => GoogleGNGProvider::class,
//            'translators' => [
//                GoogleGNGAdGroupPerformanceReport::class
//            ]
//        ],
//        'bingGNG'   => [
//            'class'       => BingGNGProvider::class,
//            'translators' => [
//                BingGNGAdGroupPerformanceReport::class
//            ]
//        ],
    ];
}

