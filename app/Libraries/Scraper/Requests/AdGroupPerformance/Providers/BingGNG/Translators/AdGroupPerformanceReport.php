<?php namespace App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\BingGNG\Translators;

use App\Libraries\Scraper\Translator;

/**
 * Class AdPerformanceReport
 *
 * @package App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\Google\Translators
 */
class AdGroupPerformanceReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_adgroup_performance';

    /**
     * @var int $report_id
     */
    protected $report_id = 6;

    /**
     * @var string $provider
     */
    protected $provider = 'BingGNG';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            'source_report_uid',
            'account_id',
            'device',
            'stats_date_tz',
        ];
    }
}
