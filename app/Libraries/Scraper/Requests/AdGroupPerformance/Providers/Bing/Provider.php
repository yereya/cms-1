<?php namespace App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\Bing;

use App\Libraries\Scraper\Providers\BingReport;


use Microsoft\BingAds\V13\Reporting\AdGroupPerformanceReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'ADGROUP_PERFORMANCE_REPORT';

    /**
     * @var int $report_id
     */
    protected $report_id = 6;

    /**
     * @var string $report_request
     */
    protected $report_request = AdGroupPerformanceReportRequest::class;

    /**
     * @var string $report_folder_name
     */
    protected $report_folder_name = 'AdGroupPerformance';

    /**
     * @var string $store_method
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * @var array $additional_fields
     */
    protected $additional_fields = ['Hour'];

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Ad Group Performance Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Hourly;
    }
}