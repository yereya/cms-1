<?php namespace App\Libraries\Scraper\Requests\AdGroupPerformance\Providers\GoogleGNG;

use App\Libraries\Scraper\Providers\GoogleGNGReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleGNGReport
{
    /**
     * @var string $report_type
     */
    protected $report_type = 'ADGROUP_PERFORMANCE_REPORT';

    /**
     * @var int $report_id
     */
    protected $report_id = 6;

    /**
     * @var string $report_folder_name
     */
    protected $report_folder_name = 'AdGroupPerformance';

    /**
     * @var string $store_method
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector         = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $ReportDefinition                 = new ReportDefinition();
        $ReportDefinition->setSelector($selector);
        $ReportDefinition->setReportName('Ad group Performance Report #' . uniqid());
        $ReportDefinition->setDateRangeType($this->date_range_type);
        $ReportDefinition->setReportType($this->report_type);
        $ReportDefinition->setDownloadFormat('CSV');

        return $ReportDefinition;
    }
}
