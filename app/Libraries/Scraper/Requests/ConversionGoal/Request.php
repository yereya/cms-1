<?php namespace App\Libraries\Scraper\Requests\ConversionGoal;

use App\Libraries\Scraper\Request as BaseRequest;
// Bing
use App\Libraries\Scraper\Requests\ConversionGoal\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\ConversionGoal\Providers\Bing\Translators\ConversionGoal as BingTranslator;
// Bing GNG
use App\Libraries\Scraper\Requests\ConversionGoal\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\ConversionGoal\Providers\BingGNG\Translators\ConversionGoal as BingGNGTranslator;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 107;

    /**
     * @var array $providers
     */
    protected $providers = [
        'bing' => [
            'class' => BingProvider::class,
            'translators' => [
                BingTranslator::class
            ]
        ],
        'bingGNG' => [
            'class' => BingGNGProvider::class,
            'translators' => [
                BingGNGTranslator::class
            ]
        ],
    ];
}
