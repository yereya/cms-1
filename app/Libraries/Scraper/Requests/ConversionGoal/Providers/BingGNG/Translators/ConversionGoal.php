<?php


namespace App\Libraries\Scraper\Requests\ConversionGoal\Providers\BingGNG\Translators;


use App\Libraries\Scraper\Translator;

class ConversionGoal extends Translator
{

    /**
     * @var int $report_id
     */
    protected $report_id = 25;

    /**
     * @var string $provider
     */
    protected $provider = 'BingGNG';
}
