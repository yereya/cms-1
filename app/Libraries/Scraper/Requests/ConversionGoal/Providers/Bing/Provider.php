<?php


namespace App\Libraries\Scraper\Requests\ConversionGoal\Providers\Bing;


use App\Libraries\PublisherEditor\Operations\Bing\ParseBingResponse;
use App\Libraries\Scraper\Providers\BingReport;
use Illuminate\Support\Collection;

use SoapVar;

use Microsoft\BingAds\V13\CampaignManagement\UpdateConversionGoalsRequest;
use Microsoft\BingAds\V13\CampaignManagement\AddConversionGoalsRequest;
use Microsoft\BingAds\V13\CampaignManagement\CampaignManagementServiceSettings;
use Microsoft\BingAds\V13\CampaignManagement\ConversionGoalCountType;
use Microsoft\BingAds\V13\CampaignManagement\ConversionGoalRevenue;
use Microsoft\BingAds\V13\CampaignManagement\ConversionGoalRevenueType;
use Microsoft\BingAds\V13\CampaignManagement\ConversionGoalStatus;
use Microsoft\BingAds\V13\CampaignManagement\ConversionGoalType;
use Microsoft\BingAds\V13\CampaignManagement\EntityScope;
use Microsoft\BingAds\V13\CampaignManagement\GetConversionGoalsByIdsRequest;
use Microsoft\BingAds\V13\CampaignManagement\OfflineConversionGoal;
use Microsoft\BingAds\V13\Reporting\ReportRequest;

class Provider extends BingReport
{

    use ParseBingResponse;
    /**
     * @var int
     */
    protected $report_id = 25;

    /**
     * @var int
     */
    protected $account_id;

    /**
     * @var array
     */
    protected $goals;

    /**
     * Report name
     *
     * @var string
     */
    protected $report_type = 'CONVERSION_GOALS';

    /**
     * @var array
     */
    protected $required_goals = ['Lead', 'Sale', 'Cancelled Sale', 'Click Out', 'Adjustment' , 'Cancelled Lead', 'Call'];

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        // TODO: Implement getReportRequestSettings() method.
    }

    /**
     * Get Accounts according to Customer Id
     *
     * @return mixed
     * @throws \Exception
     */
    private function getAccountsByPublisher()
    {
        $user = $this->bing_client->getUser()->User;
        $this->bing_client->withCustomerId($user->CustomerId);

        return $this->bing_client
            ->getAccountsByUserId($user->Id)
            ->Accounts
            ->AdvertiserAccount;
    }

    /**
     * Get Conversion Goal For each account
     *  - if ids passed get only the specific goals
     *  - else bring all goals from api.
     *
     * @param null $conversionGoalIds
     *
     * @return mixed
     * @throws \Exception
     */
    private function getConversionGoalsById($conversionGoalIds = null)
    {
        $request                      = new GetConversionGoalsByIdsRequest();
        $request->ConversionGoalIds   = $conversionGoalIds;
        $request->ConversionGoalTypes = [ConversionGoalType::OfflineConversion];

        return $this->bing_client->campaignsService()->GetService()->GetConversionGoalsByIds($request);
    }

    /**
     *  Get the names of goals that need to be add per account.
     *
     * @param array $goals (existing goals per account)
     *
     * @return \Illuminate\Support\Collection
     */
    private function getGoalsToAdd(array $goals)
    {
        $required_goals = collect($this->required_goals);
        $existing_goals = collect($goals)->pluck('Name');

        return $required_goals->diff($existing_goals);
    }

    /**
     * Create goals according to the  $goals collection
     *
     * @param Collection $goals -> goals list
     *
     * @return mixed
     */
    private function createGoals(Collection $goals)
    {
        $conversion_goals = $this->_prepare($goals);
        $response         = $this->save($conversion_goals);

        return $response;
    }

    /**
     * Create goals according to the  $goals collection
     *
     * @return mixed
     */
    private function updateGoals()
    {
        $goals = collect($this->goals->ConversionGoals->ConversionGoal)->pluck('Name', 'Id');

        $conversion_goals = $this->_prepare($goals, true);
        $response         = $this->update($conversion_goals);

        return $response;
    }

    /**
     * Add conversion Goal to Bind System
     *
     * @param array $conversionGoals
     *
     * @return mixed
     * @throws \Exception
     */
    private function save(array $conversionGoals)
    {
        /*Prepare request */
        $request                  = new AddConversionGoalsRequest();
        $request->ConversionGoals = $conversionGoals;

        /*Return response*/
        $response = $this->bing_client->campaignsService()->GetService()->AddConversionGoals($request);

        return $response;
    }

    /**
     * @param array $conversionGoals
     *
     * @return mixed
     * @throws \Exception
     */
    private function update(array $conversionGoals)
    {
        /*Prepare request */
        $request                  = new UpdateConversionGoalsRequest();
        $request->ConversionGoals = $conversionGoals;

        /*Return response*/
        $response = $this->bing_client->campaignsService()->GetService()->UpdateConversionGoals($request);

        return $response;
    }

    /**
     * @param      $name
     * @param null $update
     * @param null $id
     *
     * @return SoapVar
     */
    private function build($name, $update = null, $id = null)
    {
        $ogRevenue             = $this->createOfflineConversionGoalRevenue();
        $offlineConversionGoal = new OfflineConversionGoal();

        // Determines how long after a click that you want to count offline conversions.
        $offlineConversionGoal->ConversionWindowInMinutes = 129600;

        // If the count type is 'Unique' then only the first offline conversion will be counted.
        // By setting the count type to 'All', then all offline conversions for the same
        // MicrosoftClickId with different conversion times will be added cumulatively.
        $offlineConversionGoal->CountType = ConversionGoalCountType::All;
        $offlineConversionGoal->Name      = $name;
        $offlineConversionGoal->Scope     = EntityScope::Account;
        $offlineConversionGoal->Status    = ConversionGoalStatus::Active;
        $offlineConversionGoal->TagId     = null;
        $offlineConversionGoal->Revenue   = $ogRevenue;

        if ($update) {
            $offlineConversionGoal->Id = $id;
        }

        return new SoapVar($offlineConversionGoal, SOAP_ENC_OBJECT, 'OfflineConversionGoal',
            $this->bing_client->campaignsService()->GetNamespace());
    }

    /**
     * @return ConversionGoalRevenue
     */
    private function createOfflineConversionGoalRevenue()
    {
        // The default conversion currency code and value. Each offline conversion can override it.
        $offlineConversionGoalRevenue               = new ConversionGoalRevenue();
        $offlineConversionGoalRevenue->Type         = ConversionGoalRevenueType::VariableValue;
        $offlineConversionGoalRevenue->Value        = 0;
        $offlineConversionGoalRevenue->CurrencyCode = 'USD';

        return $offlineConversionGoalRevenue;
    }

    /*Throw errors*/
    private function throw($errors)
    {
        collect($errors)->each(function ($error) {
            $this->logger->error(['Error has occurred:', $error->Message]);
        });
    }

    /**
     * @param      $goals
     * @param null $update
     *
     * @return array
     */
    private function _prepare($goals, $update = null)
    {
        $conversion_goals = [];

        /*in case of more then one goal for each over them */
        /*prepare each goal*/
        $goals->each(function ($name, $id) use (&$conversion_goals, $update) {

            $goal = $this->build($name, $update, $id);

            /*Add goal to the list*/
            $conversion_goals[] = $goal;
        });

        $this->parseString(" Add conversion goals to account -> {$this->account_id} ...");

        return $conversion_goals;
    }

    /**
     * @param Collection $offline_goals
     * @param            $updated_accounts
     *
     * @return mixed
     * @throws \Exception
     */
    private function createGoalsProcess(Collection $offline_goals, &$updated_accounts)
    {
        $response = $this->createGoals($offline_goals);
        $this->parseOfflineResults($response);

        return $response;
    }

    /**
     *  get Accounts related to this publisher.
     */
    public function fetch()
    {
        $this->accounts = $this->getAccountsByPublisher();
        $this->execute();
    }


    /**
     * Execute the following steps
     *  1 .Iterate on each account
     * @method $this->withAccountId->set accountId into client per iteration
     * @method $this->getConversionGoalsById->return all conversion if no id passed
     * @method $this->getGoalsToAdd->get the remaining goals need to add after get the ones that already assigned
     * @method $this->resolveGoalProcess->create or update goals according to params
     *  parse log according to each account
     */
    public function execute()
    {
        $updated_accounts = [];

        collect($this->accounts)->each(function ($account) use ($updated_accounts) {

            /*Update request with the current account*/
            $this->account_id = $account->Id;
            $this->bing_client->withAccountId($this->account_id);
            $this->goals   = $this->getConversionGoalsById();
            $goals         = $this->resolveGoalsList($this->options['update']);
            $offline_goals = $this->getGoalsToAdd($goals);

            if ($offline_goals->count()) {
                $this->resolveAction($offline_goals, $updated_accounts);
                $updated_accounts[] = $this->account_id;

                return true;
            }

            $this->parseString("no need to add goals to account", $account->Id);
        });

        $updated_account_number = count($updated_accounts);
        $message                = $updated_account_number . " => Accounts was updated successfully \n\n";

        if (!$updated_account_number) {
            $message = 'All goals already added';
        }

        $this->logger->info(
            [
                [$message],
                $updated_account_number
                    ? ['These following account ids was update successfully =>', $updated_accounts]
                    : ['All accounts already added']
            ]
        );

        print(" \n\n $message \n\n");
    }

    /**
     * Resolve goals by $update param
     *
     * @param $update
     *
     * @return array
     */
    private function resolveGoalsList($update = false)
    {
        $results = [];

        if (count((array)$this->goals->ConversionGoals) && !$update) {
            return $this->goals->ConversionGoals->ConversionGoal;
        }

        return $results;
    }

    /**
     * @param $offline_goals
     * @param $updated_accounts
     *
     * @return mixed
     * @throws \Exception
     */
    private function resolveAction($offline_goals, $updated_accounts)
    {
        if ($this->options['update']) {
            return $this->updateGoals();
        }

        return $this->createGoalsProcess($offline_goals, $updated_accounts);
    }
}
