<?php namespace App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\BingGNG;

use App\Libraries\Scraper\Providers\BingGNGReport;


use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportFormat;
use Microsoft\BingAds\V13\Reporting\ReportAggregation;
use Microsoft\BingAds\V13\Reporting\KeywordPerformanceReportRequest;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends BingGNGReport
{
    /**
     * @var int
     */
    protected $report_id = 1;

    /**
     * @var string $report_type
     */
    protected $report_type = 'KEYWORDS_PERFORMANCE_REPORT';

    /**
     * @var string $report_request
     */
    protected $report_request = KeywordPerformanceReportRequest::class;

    /**
     * @var string
     */
    protected $report_folder_name = 'KeywordsPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        $request->Format                 = ReportFormat::Csv;
        $request->ReportName             = 'Keyword Performance Report';
        $request->ReturnOnlyCompleteData = false;
        $request->Aggregation            = ReportAggregation::Daily;
    }
}
