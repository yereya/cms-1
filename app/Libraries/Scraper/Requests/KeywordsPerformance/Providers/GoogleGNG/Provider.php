<?php namespace App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\GoogleGNG;

use App\Libraries\Scraper\Providers\GoogleGNGReport;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\v201809\cm\Selector;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends GoogleGNGReport
{
    /**
     * @var int
     */
    protected $report_id = 1;

    /**
     * @var string $report_type
     */
    protected $report_type = 'KEYWORDS_PERFORMANCE_REPORT';

    /**
     * @var string
     */
    protected $report_folder_name = 'KeywordsPerformance';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

    /**
     * Set Selector
     *
     * @return Selector
     */
    protected function setSelector()
    {
        $selector = new Selector;
        $selector->setFields(array_unique(array_flatten($this->getFields())));

        return $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        $reportDefinition = new ReportDefinition();
        $reportDefinition->setSelector($selector);
        $reportDefinition->setReportName('Keywords Performance Report #' . uniqid());
        $reportDefinition->setDateRangeType($this->date_range_type);
        $reportDefinition->setReportType($this->report_type);
        $reportDefinition->setDownloadFormat('CSV');

        return $reportDefinition;
    }
}


