<?php namespace App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\GoogleGNG\Translators;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Mrr\MrrFactPublisher;
use App\Entities\Repositories\Repository;
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\BIPublishersReportCustomFields;
use App\Entities\Repositories\Mrr\MrrFactPublisherRepo;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Google\Translators
 */
class BIPublishersReport extends BIPublishersReportCustomFields
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_fact_publishers';

    /**
     * @var int $report_id
     */
    protected $report_id = 1;

    /**
     * @var string $provider
     */
    protected $provider = 'GoogleGNG';

    /**
     * @var int $publisher_id
     */
    protected  $publisher_id = 163;
    /**
     * @var array $country_account_dic
     */
    protected  $country_account_list = [];
    /**
     *
     */
    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'source_report_uid',
            'report_id',
            'publisher_id',
            'ad_group_id',
            'device',
            'stats_date_tz',

            // Should not be updated
            'sid',
            'destination_url',
            'max_cpc',
            'quality_score',
        ];
    }

    private function fetchCountryForAccount()
    {
        $this->country_account_list = array_column(Account::select('source_account_id')
            ->where('publisher_id', '=', $this->publisher_id)
            ->where('country', '=', 'GB')
            ->get()->toArray(), 'source_account_id');
    }

    public function store()
    {
        //Check if there's records in google
        if (!$this->data) {
            return;
        }

        $days_diff = $this->getDateDiffInDays();
        // Retrieve account-country list
        $this->fetchCountryForAccount();
        /*Check if days diff is longer then 1*/
        if ($days_diff <= 1) {
            foreach ($this->data as $request) {
                $this->storeRequest($request);
            }

            return;
        }

        // store according to insertion type
        $this->custom_store();
    }


    /**
     *
     * Return Collection grouped by specific field combination:
     *
     * key return is combination of: (date+site-id+keyword-id+device+account)
     *
     * @param $rows Collection
     * @return Collection
     */
    private function generateKeyBy(Collection $rows): Collection
    {
        return $rows->keyBy(function ($row) {
            return $row['stats_date_tz'] . $row['sid'] . $row['keyword_id'] . $row['device'] . $row['account_id'];
        });
    }

    /**
     * Reset keys in table mrr. (if they only exist in this table and not in google records);
     *
     *  it mean that they already been used.
     *
     * @param $rows
     * @param $repo Repository
     * @return mixed
     */
    private function resetRows($rows, $repo)
    {

        $idColumns = array_column($rows, 'id');

        MrrFactPublisher::whereIn('id',$idColumns)
            ->update([
                'clicks' => 0,
                'impressions' => 0,
                'cost' => 0,
                'currency' => 0,
                'max_cpc' => 0,
                'avg_position' => 0,
                'quality_score' => 0,
                'search_absolute_top_impression_share' => 0,
                'search_impression_share' => 0,
                'AbsoluteTopImpressionPercentage' => 0,
                'TopImpressionPercentage' => 0,
                'network_net_revenue' => 0
            ]);

    }

    /**
     * return records with keys according to insertion type
     *
     * 1. build base array according to insertion type (create,update,reset)
     * 2. get all records that exist only in mrr_table
     * 3. merge all records (google + mrr table)
     * 4. iterate on them and append records according to there insertion type
     *
     * @param $mrr_reports Collection
     * @param $google_reports Collection
     * @return Collection
     */
    private function resolveInsertionType($google_reports, $mrr_reports): Collection
    {
        //Records divided by insertion type
        $grouped_reports = ['reset' => [], 'create' => [], 'update' => []];

        //Records that only in mrr (need to be reset )
        $reports_that_exist_only_in_mrr = $mrr_reports->diffKeys($google_reports);

        /*IF THERE'S RECORD IN MRR_REPORT*/
        if (\count($mrr_reports)) {

            $keys_to_update = [];
            $google_keys = $google_reports->values()[0];
            $mrr_keys = $mrr_reports->first()->toArray();

            /*GET KEYS TO UPDATE  - build new array with the key structure to iterate over  to update the fields*/
            foreach ($mrr_keys as $key => $val) {
                $key_exist = array_key_exists($key, $google_keys);
                if ($key_exist) {
                    $keys_to_update[] = $key;
                }
                continue;
            }

            /*FILTER BY SPECIFIC FIELDS*/
            $mrr_reports = $mrr_reports->map(function ($report) use ($keys_to_update) {
                $newRecord = [];
                if (!empty($report['id'])) {
                    $newRecord['id'] = $report->id;
                }
                foreach ($keys_to_update as $key => $val) {
                    $newRecord[$val] = $report[$val];
                }

                return $newRecord;
            });
        }

        /*
         * Merge all rows (only for iteration purpose:
         *
         *  When we merging the mrr-reports with the google-reports the ids in mrr will be gone.))
         *
         * */
        $reports = \count($mrr_reports) ? $mrr_reports->merge($google_reports) : $google_reports;

        //Iterate over all reports and filter them according to insertion type
        $reports->each(/**
         * @param $reports
         * @param $token
         * @return bool
         */
            function ($reports, $idx) use (
                $reports_that_exist_only_in_mrr,
                $google_reports,
                $mrr_reports,
                &$grouped_reports
            ) {

                //Records in mrr table but not in google records
                if ($reports_that_exist_only_in_mrr->has($idx)) {
                    $grouped_reports['reset'][$idx] = $reports;

                    return true;
                }

                /*Records intersect in mrr and google*/
                if ($google_reports->has($idx) && $mrr_reports->has($idx)) {
                    $grouped_reports['update'][$idx] = $reports;

                    return true;
                }

                //Records  that in google but not in mrr
                $grouped_reports['create'][$idx] = $reports;

                return true;
            });

        $grouped_reports = collect($grouped_reports);

        return $grouped_reports;
    }


    /**
     * Save Update or reset records according to there insertion type
     *
     * @param $mrr_repo
     * @param $reports Collection
     * @return array
     */
    private function takeActionAccordingToInsertionType($mrr_repo, $reports)
    {
        $store_method = 'store_' . $this->options['store_method'];
        $inserted = ['reset' => [], 'update' => [], 'create' => []];

        //Store according to insertion type
        $reports->each(function ($reports, $insertionType) use ($store_method, $mrr_repo, &$inserted) {

            //If no records in specific group continue to the next iteration
            if (!\count($reports)) {
                return true;
            }

            //Split records to chunks and insert them
            foreach (array_chunk($reports, $this->store_chunk_size) as $rows) {

                if ($insertionType === 'reset') {
                    $this->resetRows($rows, $mrr_repo);

                    continue;
                }

                $this->$store_method($rows);
            }

            /*Only for information */
            $inserted[$insertionType] = count($reports);

            return true;
        });

        return $inserted;
    }


    /**
     * Get the date range In days
     *
     * @return int
     */
    private function getDateDiffInDays()
    {
        $from = $this->options['date-range-start'];
        $to = $this->options['date-range-end'];

        $date_range_start = Carbon::createFromFormat('Y-m-d', $from);
        $date_range_end = Carbon::createFromFormat('Y-m-d', $to);

        return $date_range_end->diffInDays($date_range_start);
    }

    /*
     * Store records according to there insertion type
     *
     * 1. prepare records for insertion
     * 2. insert according to type
     *
    */
    private function custom_store()
    {
        //Get mrr repo
        $mrr_repo = new MrrFactPublisherRepo();

        try {

            //Build rows according to google report
            foreach ($this->data as $request) {
                $rows = $this->buildRows($request);

                if (!\count($rows)) {
                    continue;
                }

                $this->logger->info([count($rows) . ' Rows returned from googleGNG']);

                $attr = [
                    'ranges' => [
                        'from' => $this->options['date-range-start'],
                        'to' => $this->options['date-range-end']
                    ],
                    'account_id' => $rows[0]['account_id']
                ];

                /*Key By Specific combination*/
                $mrr_reports = $mrr_repo->getByDateAndAccount($attr);
                $mrr_reports = $mrr_reports ? $this->generateKeyBy($mrr_reports) : collect();
                $google_records = $this->generateKeyBy(collect($rows));

                /*Sanitize records by insertion type*/
                $reports = $this->resolveInsertionType($google_records, $mrr_reports);

                /*Insert Data according to groups*/
                $this->takeActionAccordingToInsertionType($mrr_repo, $reports);

                $this->logger->info([
                        "customer ' {$attr['account_id']} successfully : ",
                        "Create: " . count($reports['create']) . " records",
                        "Update: " . count($reports['update']) . " records",
                        "Reset: " . count($reports['reset']) . " records"
                    ]
                );
            }
        } catch (\Exception $exception) {

            $this->logger->info([
                ['Caught exception: ', $exception->getMessage()],
                ['On File Name: ', $exception->getFile()],
                ['On Line Number: ', $exception->getLine()]
            ]);
        }
    }
}
