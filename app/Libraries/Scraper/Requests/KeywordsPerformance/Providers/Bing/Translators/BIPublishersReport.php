<?php namespace App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Bing\Translators;

use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\BIPublishersReportCustomFields;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Bing\Translators
 */
class BIPublishersReport extends BIPublishersReportCustomFields
{
    /**
     * @var string $connection
     */
    protected $connection = 'mrr';

    /**
     * @var string $table_name
     */
    protected $table_name = 'mrr_fact_publishers';

    /**
     * @var int $report_id
     */
    protected $report_id = 1;

    /**
     * @var string $provider
     */
    protected $provider = 'Bing';

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return [
            // Default unique columns
            'source_report_uid',
            'report_id',
            'publisher_id',
            'ad_group_id',
            'device',
            'stats_date_tz',

            // Should not be updated
            //'sid',
            //'destination_url',
            //'max_cpc',
            'quality_score',

            'keyword_id',
            'ad_id',
            'account_id',
            'campaign_id',
            'TopImpressionPercentage',
            'AbsoluteTopImpressionPercentage',
            'search_impression_share',


        ];
    }
}