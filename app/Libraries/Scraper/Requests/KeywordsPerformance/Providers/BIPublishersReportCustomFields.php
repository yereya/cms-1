<?php namespace app\Libraries\Scraper\Requests\KeywordsPerformance\Providers;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountReportField;
use App\Libraries\Scraper\Translator;
use Cache;
use Carbon\Carbon;

class BIPublishersReportCustomFields extends Translator
{
    /**
     * @var string $cache_key
     */
    private $cache_key = 'networks';

    /**
     * Build Rows
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            $rows[] = $this->prepareRow($row);
        }

        # For UK Google accounts multiple the cost by 102% starting from November 2020
        if($rows && ($this->provider == 'Google' || $this->provider == 'GoogleGNG')){
            if(array_search($rows[0]['account_id'],$this->country_account_list) !== false){
                $rows = array_map(function($row){
                    if($row['stats_date_tz'] >= '2020-11-01'){
                        $row['cost'] =  $row['cost'] * 1.02;
                        $row['network_net_revenue'] = $row['cost'] * (-1);

                    }
                    return $row;
                },$rows);
            }
        }
        return $rows;
    }

    /**
     * Prepare Row
     *
     * @param array $row
     *
     * @return mixed
     */
    private function prepareRow($row)
    {
        $row = $this->getRowValues($row);
        $this->setNetworkNetRevenue($row);
        $this->setMatchTypeFour($row);
        $this->setSearchAbsoluteTopImpressionShare($row);
        arrayRemoveFields($row, ['FinalUrls', 'TrackingUrlTemplate', 'DestinationUrl', 'TrackingTemplate']);
        $row['keyword_name'] = trim($row['keyword_name']);

        return $row;
    }

    /**
     * Calculate Search_absolute_top_impression_share
     *
     * @param array $row
     */
    protected function setSearchAbsoluteTopImpressionShare(&$row)
    {
        if(isset($row['search_absolute_top_impression_share']) and $row['search_absolute_top_impression_share'] == '< 10%'){
            $search_impression_share_as_num = filter_var($row['search_impression_share'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $AbsoluteTopImpressionPercentage_as_num = filter_var($row['AbsoluteTopImpressionPercentage'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
            $search_absolute_top_impression_share_as_num = $AbsoluteTopImpressionPercentage_as_num * $search_impression_share_as_num;
            $row['search_absolute_top_impression_share'] = $search_absolute_top_impression_share_as_num.'%';
        }
    }

    /**
     * Get Network Revenue
     *
     * @param array $row
     */
    protected function setNetworkNetRevenue(&$row)
    {
        $networks = $this->getNetworksFromAccounts();

        if (isset($networks[$row['account_id']])) {
            $row['network_net_revenue'] = $row['cost'] * $networks[$row['account_id']];
        } else {
            $row['network_net_revenue'] = 0;
        }
    }

    /**
     * Get Networks from accounts.
     *
     * Stores in cache to prevent always querying it.
     *
     * @return array
     */
    private function getNetworksFromAccounts()
    {
        if ($cache = Cache::get($this->cache_key)) {
            return $cache;
        }

        $networks = [];
        foreach ($this->getActiveAccountsWithFields() as $account) {
            $networks[$account->source_account_id] = $account->id;
        }

        $values = $this->fetchValues(cleanArray($networks));

        Cache::put($this->cache_key, $values, Carbon::now()->addMinutes(10));

        return $values;
    }

    /**
     * Get Active Accounts With Fields
     *
     * @return mixed
     */
    private function getActiveAccountsWithFields()
    {
        return Account::whereHas('advertiser', function ($query) {
            $query->where('advertisers.status', 'active');
        })->whereHas('publisher', function ($query) {
            $query->whereIn('publishers.id', ['95', '97','163','164']);
        })->with('fields')->get();
    }

    /**
     * Fetch values
     *
     * @param $ids
     *
     * @return array
     */
    private function fetchValues($ids)
    {
        $networks = AccountReportField::select('account_id', 'value', 'report_id')
            ->whereIn('account_id', array_values($ids))->where('report_id', $this->report_id)
            ->get()->keyBy('account_id')->toArray();

        $array_flip = array_flip($ids);

        $values = [];
        foreach ($networks as $key => $data) {
            $values[$array_flip[$key]] = $data['value'];
        }

        return $values;
    }

    /**
     * Add new field match_type_four
     *
     * @param array $row
     */
    private function setMatchTypeFour(&$row)
    {
        $match_type_four = $row['match_type'];
        if (strpos($row['keyword_name'], '+') !== false) {
            if ($row['match_type'] == 'Broad') {
                $match_type_four = 'BMM';
            }
        }

        $row['match_type_four'] = $match_type_four;
    }
}

