<?php namespace App\Libraries\Scraper\Requests\KeywordsPerformance;

use App\Libraries\Scraper\Request as BaseRequest;
// Bing
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Bing\Translators\BIPublishersReport as BingBIPublishersReport;
// Bing GNG
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\BingGNG\Translators\BIPublishersReport as BingGNGBIPublishersReport;
// Google
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Google\Translators\BIPublishersReport as GoogleBIPublishersReport;
// Google GNG
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\GoogleGNG\Translators\BIPublishersReport as GoogleGNGBIPublishersReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 25;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleBIPublishersReport::class,
            ]
        ],
        'googleGNG' => [
            'class'       => GoogleGNGProvider::class,
            'translators' => [
                GoogleGNGBIPublishersReport::class,
            ]
        ],
        'bing'   => [
            'class'       => BingProvider::class,
            'translators' => [
                BingBIPublishersReport::class,
            ]
        ],
        'bingGNG'   => [
            'class'       => BingGNGProvider::class,
            'translators' => [
                BingGNGBIPublishersReport::class,
            ]
        ]

    ];
}
