<?php namespace App\Libraries\Scraper\Requests\Accounts\Providers\GoogleGNG\Translators;

use Illuminate\Support\Collection;
use App\Entities\Models\Bo\Account;
use App\Libraries\Scraper\Translator;

/**
 * Class BIAccounts
 *
 * @package App\Libraries\Scraper\Requests\Accounts\Providers\Google\Translators;
 */
class BIAccounts extends Translator
{
    /**
     * @var string $model_name
     */
    protected $model_name = Account::class;

    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string $table_name
     */
    protected $table_name = 'accounts';

    /**
     * @var int $report_id
     */
    protected $report_id = 5;

    /**
     * @var string $provider
     */
    protected $provider = 'Google';

    /**
     * @var int provider id
     */
    protected $provider_id = 163;

    /**
     * @var string $account_id_column
     */
    protected $account_id_column = 'source_account_id';

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $local_accounts = $this->getLocalAccounts();
        $rows = [];
        foreach ($stream as $row) {
            if (!$local_accounts->get($row->customerId)) {
                $new_row = $this->getRowValues($row);
                if (isset($row->dateTimeZone)) {
                    $new_row['time_zone_location'] = $row->dateTimeZone;
                    $new_row['time_zone']          = convertTimeZoneNameToTimeZoneTime($row->dateTimeZone);
                }
                $rows[] = $new_row;
            }
        }

        return $rows;
    }

    /**
     * Get Local Accounts
     *
     * @return mixed
     */
    private function getLocalAccounts() {
        $accounts = Account::where('publisher_id', $this->provider_id)->get();

        if ($accounts){
            return $accounts->keyBy('source_account_id');
        }

        return new Collection();
    }
}
