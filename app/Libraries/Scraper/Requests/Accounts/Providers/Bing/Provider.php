<?php namespace App\Libraries\Scraper\Requests\Accounts\Providers\Bing;

use App\Libraries\Scraper\Providers\BingReport;


use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\CustomerManagement\GetAccountsInfoRequest;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Requests\Accounts\Providers\Bing;
 */
class Provider extends BingReport
{
    /**
     * @var int
     */
    protected $report_id = 5;

    /**
     * Report name
     *
     * @var string
     */
    protected $report_type = 'ACCOUNTS';

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->data = [$this->getAllAccounts()];
    }

    /**
     * Get All Accounts
     */
    protected function getAllAccounts()
    {
        $request             = new GetAccountsInfoRequest();
        $request->CustomerId = config('bing-ads.customer_id');

        $response = $this->bing_client->customerService()->GetService()->GetAccountsInfo($request);

        return $response->AccountsInfo->AccountInfo;
    }

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestSettings(ReportRequest $request)
    {
        // TODO to remove this method
        // The reason it is still here is because it is an abstract method in the parent
    }
}