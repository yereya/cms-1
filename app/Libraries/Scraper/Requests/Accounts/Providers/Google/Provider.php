<?php namespace App\Libraries\Scraper\Requests\Accounts\Providers\Google;

use Google\AdsApi\AdWords\AdWordsServices;
use App\Libraries\Scraper\Providers\GoogleReport;
use Google\AdsApi\AdWords\v201809\cm\Predicate;
use Google\AdsApi\AdWords\v201809\cm\PredicateOperator;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use Google\AdsApi\AdWords\v201809\mcm\ManagedCustomerService;

/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Requests\Accounts\Providers\Google;
 */
class Provider extends GoogleReport
{
    /**
     * @var int
     */
    protected $report_id = 5;

    /**
     * Report name
     *
     * @var string
     */
    protected $report_type = 'ACCOUNTS';

    /**
     * Selector instance
     *
     * @var
     */
    private $selector;

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->setSelector();
        $this->getAllCustomers();
    }

    /**
     * Get All Customers
     */
    protected function getAllCustomers()
    {
        $adWordsServices = new AdWordsServices();
        $managedCustomerService = $adWordsServices->get($this->makeSession(), ManagedCustomerService::class);
        $customers = $managedCustomerService->get($this->getSelector())->getEntries();

        $this->data = [];

        foreach($customers as $customer){
            $this->data[] = (object)[
                'name' => $customer->getName(),
                'customerId' => $customer->getCustomerId(),
                'canManageClients' => $customer->getCanManageClients(),
                'currencyCode' => $customer->getCurrencyCode(),
                'dateTimeZone' => $customer->getDateTimeZone(),
                'testAccount' => $customer->getTestAccount(),
                'accountLabels' => $customer->getAccountLabels(),
                'excludeHiddenAccounts' => $customer->getExcludeHiddenAccounts()
            ];
        }

        $this->data = [$this->data];
    }

    /**
     * Get Selector
     *
     * @return mixed
     */
    protected function getSelector()
    {
        return $this->selector;
    }

    /**
     * Set Selector
     */
    protected function setSelector()
    {
        $selector  = new Selector();
        $selector->setFields(array_values($this->getFields()));
        $selector->setPredicates([new Predicate('CanManageClients', PredicateOperator::EQUALS, ['false'])]);

        $this->selector = $selector;
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    protected function setReportDefinition(Selector $selector)
    {
        // TODO: Implement setReportDefinition() method.
    }
}