<?php namespace App\Libraries\Scraper\Requests\Accounts;

use App\Libraries\Scraper\Request as BaseRequest;
// Bing
use App\Libraries\Scraper\Requests\Accounts\Providers\Bing\Provider as BingProvider;
use App\Libraries\Scraper\Requests\Accounts\Providers\Bing\Translators\BIAccounts as BingBIAccounts;
// Bing GNG
use App\Libraries\Scraper\Requests\Accounts\Providers\BingGNG\Provider as BingGNGProvider;
use App\Libraries\Scraper\Requests\Accounts\Providers\BingGNG\Translators\BIAccounts as BingGNGBIAccounts;
// Google
use App\Libraries\Scraper\Requests\Accounts\Providers\Google\Provider as GoogleProvider;
use App\Libraries\Scraper\Requests\Accounts\Providers\Google\Translators\BIAccounts as GoogleBIAccounts;
// Google GNG
use App\Libraries\Scraper\Requests\Accounts\Providers\GoogleGNG\Provider as GoogleGNGProvider;
use App\Libraries\Scraper\Requests\Accounts\Providers\GoogleGNG\Translators\BIAccounts as GoogleGNGBIAccounts;


/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 4;

    /**
     * @var array $providers
     */
    protected $providers = [
        'google' => [
            'class'       => GoogleProvider::class,
            'translators' => [
                GoogleBIAccounts::class
            ]
        ],
//        'googleGNG' => [
//            'class'       => GoogleGNGProvider::class,
//            'translators' => [
//                GoogleGNGBIAccounts::class
//            ]
//        ],
        'bing'   => [
            'class'       => BingProvider::class,
            'translators' => [
                BingBIAccounts::class
            ]
        ],
        'bingGNG'   => [
            'class'       => BingGNGProvider::class,
            'translators' => [
                BingGNGBIAccounts::class
            ]
        ]
    ];
}
