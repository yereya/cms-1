<?php namespace App\Libraries\Scraper\Requests\OutbrainReport\Providers\Outbrain;

use App\Libraries\Scraper\Providers\OutbrainReport;


/**
 * Class Provider
 *
 * @package App\Libraries\Scraper\Providers
 */
class Provider extends OutbrainReport
{
    /**
     * @var int
     */
    protected $report_id = 36;

    /**
     * @var string $report_type
     */
    protected $report_type = 'OUTBRAIN_REPORT';

    /**
     * @var string $report_request
     */

    /**
     * @var string
     */
    protected $report_folder_name = 'OutbrainReport';

    /**
     * @var string
     */
    protected $store_method = 'insertOrUpdate';

}
