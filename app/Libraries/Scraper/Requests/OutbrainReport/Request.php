<?php namespace App\Libraries\Scraper\Requests\OutbrainReport;

use App\Libraries\Scraper\Request as BaseRequest;
use App\Libraries\Scraper\Requests\OutbrainReport\Providers\Outbrain\Provider as OutbrainProvider;
use App\Libraries\Scraper\Requests\OutbrainReport\Providers\Outbrain\Translators\BIPublishersReport as OutbrainBIPublishersReport;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
class Request extends BaseRequest
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id = 125;

    /**
     * @var array $providers
     */
    protected $providers = [
        'outbrain' => [
            'class'       => OutbrainProvider::class,
            'translators' => [
                OutbrainBIPublishersReport::class,
            ]
        ]
    ];
}
