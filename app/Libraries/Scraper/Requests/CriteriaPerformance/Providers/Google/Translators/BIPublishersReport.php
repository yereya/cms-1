<?php namespace App\Libraries\Scraper\Requests\CriteriaPerformance\Providers\Google\Translators;

use App\Entities\Models\Bo\ChangesLog;
use App\Libraries\Scraper\Translator;

/**
 * Class BIPublishersReport
 *
 * @package App\Libraries\Scraper\Requests\KeywordsPerformance\Providers\Google\Translators
 */
class BIPublishersReport extends Translator
{
    /**
     * @var string $connection
     */
    protected $connection = 'bo';

    /**
     * @var string $table_name
     */
    protected $table_name = 'changes_scraper';

    /**
     * @var int $report_id
     */
    protected $report_id = 9;

    /**
     * @var string $provider
     */
    protected $provider = 'Google';

    /**
     * Store Row
     *
     * @param array $row
     */
    protected function storeRow($row)
    {
        try {
            ChangesLog::create($row);
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
        }
    }
}