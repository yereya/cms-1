<?php namespace App\Libraries\Scraper\Exceptions;

/**
 * Class RequestNotExistsException
 *
 * @package App\Libraries\Scraper\Exceptions
 */
class TranslatorNotExistsException extends \Exception
{
    //
}