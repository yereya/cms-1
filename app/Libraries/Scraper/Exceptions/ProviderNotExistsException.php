<?php namespace App\Libraries\Scraper\Exceptions;

/**
 * Class ProviderNotExistsException
 *
 * @package App\Libraries\Scraper\Exceptions
 */
class ProviderNotExistsException extends \Exception
{
    //
}