<?php namespace App\Libraries\Scraper\Exceptions;

/**
 * Class InvalidDateRangeException
 *
 * @package App\Libraries\Scraper\Exceptions
 */
class InvalidDateRangeException extends \Exception
{
    //
}