<?php namespace App\Libraries\Scraper;

use App\Entities\Models\Bo\ReportField;
use App\Entities\Repositories\Mrr\MrrFactPublisherRepo;
use App\Entities\Repositories\Repository;
use App\Events\AssignAccounts;
use App\Events\AssignConversion;
use App\Facades\ShortCode;
use App\Libraries\Scraper\Contracts\Translator as TranslatorContract;
use App\Libraries\TpLogger;
use Illuminate\Support\Collection;

/**
 * Class Translator
 *
 * @package App\Libraries\Scraper\Translators
 */
abstract class Translator implements TranslatorContract
{
    /**
     * @var string $connection
     */
    protected $connection;

    /**
     * @var string $table_name
     */
    protected $table_name;

    /**
     * @var string $model_name
     */
    protected $model_name;

    /**
     * @var array $data
     */
    protected $data;

    /**
     * @var int $report_id
     */
    protected $report_id;

    /**
     * @var string $provider
     */
    protected $provider;

    /**
     * @var TpLogger $logger
     */
    protected $logger;

    /**
     * @var string $account_id_column
     */
    protected $account_id_column = 'account_id';

    /**
     * @var null
     */
    protected $date_report = null;

    /**
     * @var int $store_chunk_size
     */
    protected $store_chunk_size = 1000;

    /**
     * @var array $options
     */
    protected $options;

    /**
     * @var
     */
    protected $fieldRows;

    /**
     * @var
     */
    protected $current_report;

    /**
     * @var array - reports to init listener
     *
     * @param assign_reports_ids - to assign accounts listener
     */
    private $assign_reports_ids = [5];

    /**
     * Google constructor.
     *
     * @param array $data
     * @param          $options
     *
     * @internal param TpLogger $logger
     */
    public function __construct(array $data, $options = null)
    {
        $this->data = $data;
        $this->logger = TpLogger::getInstance();
        $this->options = $options;

        if ($options && $options['date-range-type'] == "CUSTOM_DATE") {
            $this->date_report = $options["date-range-end"];
        }
    }

    /**
     * Store
     *
     * @return void
     */
    public function store()
    {
        if (!$this->data) {
            return;
        }

        foreach ($this->data as $request) {
            $this->storeRequest($request);
        }
    }

    /**
     * Store Request
     *
     * @param array $request
     *
     * @return array
     */
    protected function storeRequest($request)
    {
        $store_method = 'store_' . $this->options['store_method'];
        $rows = $this->buildRows($request);
        $this->$store_method($rows);
    }

    /**
     * Build Rows from stream
     *
     * @param $stream
     *
     * @return array
     */
    protected function buildRows($stream)
    {
        $rows = [];
        foreach ($stream as $row) {
            if (is_null($row)) {
                continue;
            }
            if (property_exists($stream, 'date')) {
                $row['_ReportDate'] = $stream->date;
            }
            $rows[] = $this->getRowValues($row);
        }

        return $rows;
    }

    /**
     * Get Row Value
     *
     * @param array $row
     *
     * @return array
     */
    protected function getRowValues($row)
    {
        $new_row = [];
        $fields  = $this->getReportFields();

        foreach ($fields as $field) {
            $new_row[$field->name] = ShortCode::setShortCodes($field->value, $row);
        }

        return $new_row;
    }

    /**
     * Get Report Fields
     *
     * @return Collection
     */
    protected function getReportFields()
    {
        if (!isset($this->fieldRows) || (!isset($this->current_report) || $this->current_report != $this->report_id)) {
            $this->current_report = $this->report_id;
            $this->fieldRows      = ReportField::where('report_id', $this->report_id)
                                               ->where('provider', $this->provider)->get();
        }

        return $this->fieldRows;
    }

    /**
     * Store Insert Or Update
     *
     * @param $rows
     */
    public function store_insertOrUpdate($rows)
    {
        $count_insert = 0;
        foreach (array_chunk($rows, $this->store_chunk_size) as $records) {
            $builder = \DB::connection($this->connection)->table($this->table_name);
            $count_insert += insertOnDuplicateKeyUpdate($builder, $records, $this->getExcludeAbleFields());
        }

        $this->logger->info(['Records inserted or updated: ' . $count_insert, 'Counts rows: ' . count($rows)]);
    }

    /**
     * Get Exclude Able Fields
     * This method needs to be overwritten when you need to limit the columns you want to update
     * otherwise all will be updated
     *
     * @return null|array
     */
    protected function getExcludeAbleFields()
    {
        return null;
    }

    /**
     * Store Insert
     *
     * @param $rows
     */
    public function store_insert($rows)
    {
        foreach (array_chunk($rows, $this->store_chunk_size) as $records) {
            if ($this->connection) {
                \DB::connection($this->connection)->table($this->table_name)->insert($records);
            } else {
                \DB::table($this->table_name)->insert($records);
            }
        }

        $this->initListeners($rows);

        $this->logger->info(['Records inserted', count($rows)]);
    }

    /**
     * Init events if need
     *
     * @param $rows
     */
    private function initListeners($rows)
    {
        if (in_array($this->report_id, $this->assign_reports_ids)) {
            //event listener to assigned accounts to permitted users
            \Event::fire(new AssignAccounts($rows, $this->logger));
            //event listener adds new conversions to account
            if ($this->provider == 'Google' || $this->provider == 'GoogleGNG') {
                \Event::fire(new AssignConversion($rows, $this->logger));
            }
        }
    }

    /**
     * Get ShortCodeLib instance
     *
     * @return ShortCodesLib|\Illuminate\Foundation\Application|mixed
     */
    public function getShortCodeLib()
    {
        return $this->short_code_lib;
    }
}

