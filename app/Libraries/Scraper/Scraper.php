<?php namespace App\Libraries\Scraper;

use App\Entities\Models\Users\User;
use App\Libraries\Scraper\Exceptions\RequestNotExistsException;
use App\Libraries\Scraper\Requests\Accounts\Request as Accounts;
use App\Libraries\Scraper\Requests\AdGroup\Request as AdGroup;
use App\Libraries\Scraper\Requests\AdGroupPerformance\Request as AdGroupPerformance;
use App\Libraries\Scraper\Requests\AdPerformance\Request as AdPerformance;
use App\Libraries\Scraper\Requests\BrandsPerformance\Request as BrandsPerformance;
use App\Libraries\Scraper\Requests\BudgetDailyPerformance\Request as BudgetDailyPerformance;
use App\Libraries\Scraper\Requests\BudgetPerformance\Request as BudgetPerformance;
use App\Libraries\Scraper\Requests\Campaign\Request as Campaign;
use App\Libraries\Scraper\Requests\CampaignPerformance\Request as CampaignPerformance;
use App\Libraries\Scraper\Requests\ChangeLogState\Request as ChangeLogState;
use App\Libraries\Scraper\Requests\ChangesLog\Request as ChangesLog;
use App\Libraries\Scraper\Requests\ClickPerformance\Request as ClickPerformance;
use App\Libraries\Scraper\Requests\Conversions\Request as UploadConversions;
use App\Libraries\Scraper\Requests\CriteriaPerformance\Request as CriteriaPerformance;
use App\Libraries\Scraper\Requests\GenderPerformance\Request as GenderPerformance;
use App\Libraries\Scraper\Requests\GeoLocationUpdate\Request as GeoLocationUpdate;
use App\Libraries\Scraper\Requests\GeoPerformance\Request as GeoPerformance;
use App\Libraries\Scraper\Requests\KeywordsPerformance\Request as KeywordsPerformance;
use App\Libraries\Scraper\Requests\PlacementPerformance\Request as PlacementPerformance;
use App\Libraries\Scraper\Requests\SearchQueryPerformance\Request as SearchQueryPerformance;
use App\Libraries\Scraper\Requests\AnalyticsConversions\Request as AnalyticsUploadConversions;
use App\Libraries\Scraper\Requests\ConversionGoal\Request as ConversionGoal;
use App\Libraries\Scraper\Requests\FacebookReport\Request as FacebookReport;
use App\Libraries\Scraper\Requests\ShareOfVoice\Request as ShareOfVoice;
use App\Libraries\Scraper\Requests\InvocaReport\Request as InvocaReport;
use App\Libraries\Scraper\Requests\AnalyticsPerformance\Request as AnalyticsReport;
use App\Libraries\Scraper\Requests\AgePerformance\Request as AgePerformance;
use App\Libraries\Scraper\Requests\HubspotReport\Request as HubspotReport;
use App\Libraries\Scraper\Requests\OutbrainReport\Request as OutbrainReport;
use App\Libraries\Scraper\Requests\TaboolaReport\Request as TaboolaReport;
use App\Libraries\Scraper\Requests\CompetitorReport\Request as CompetitorReport;


/**
 * Class Scraper
 *
 * @package App\Libraries\Scraper
 */
class Scraper
{
    /**
     * @var array $requests
     */
    protected $requests = [
        'accounts'                      => Accounts::class,
        'ad_performance'                => AdPerformance::class,
        'keywords_performance'          => KeywordsPerformance::class,
        'brands_performance'            => BrandsPerformance::class,
        'changes_log'                   => ChangesLog::class,
        'ad_group_performance'          => AdGroupPerformance::class,
        'click_performance'             => ClickPerformance::class,
        'criteria_performance'          => CriteriaPerformance::class,
        'gender_performance'            => GenderPerformance::class,
        'placement_performance'         => PlacementPerformance::class,
        'budget_performance'            => BudgetPerformance::class,
        'budget_daily'                  => BudgetDailyPerformance::class,
        'geo_performance'               => GeoPerformance::class,
        'campaign'                      => Campaign::class,
        'ad_groups'                     => AdGroup::class,
        'search_query_performance'      => SearchQueryPerformance::class,
        'change_log_state'              => ChangeLogState::class,
        'campaign_performance'          => CampaignPerformance::class,
        'geo_location_update'           => GeoLocationUpdate::class,
        'analytics_upload_conversions'  => AnalyticsUploadConversions::class,
        'upload_conversions'            => UploadConversions::class,
        'conversion_goals'              => ConversionGoal::class,
        'facebook_report'               => FacebookReport::class,
        'share_of_voice'                => ShareOfVoice::class,
        'invoca_report'                 => InvocaReport::class,
        'analytics_report'              => AnalyticsReport::class,
        'age_performance'               => AgePerformance::class,
        'hubspot_report'                => HubspotReport::class,
        'outbrain_report'               => OutbrainReport::class,
        'taboola_report'                => TaboolaReport::class,
        'competitors_report'            => CompetitorReport::class
    ];

    /**
     * @var array $settings
     */
    private $settings;

    /**
     * @var array $options
     */
    private $options;

    // TODO - remove once auth is solved
    const USER_PERMISSION = 'scheduler';

    /**
     * Scraper constructor.
     *
     * @param array $settings
     */
    public function __construct($settings = [])
    {
        $this->settings = $settings;
    }

    /**
     * Run All Requests
     *
     * @return array
     */
    public function runAllRequests()
    {
        foreach ($this->requests as $request => $class) {
            $this->runRequest($request);
        }
    }

    /**
     * Run Request
     *
     * @param        $name
     *
     * @throws RequestNotExistsException
     */
    public function runRequest($name)
    {
        // TODO - Replace as quick as possible
        if (!auth()->check()) {
            \Auth::login(User::where('username', self::USER_PERMISSION)->first());
        }
        $request = $this->tryToInitRequest($name);
        $request->run();
    }

    /**
     * Try To Init Request
     *
     * @param $name
     *
     * @return mixed
     * @throws RequestNotExistsException
     */
    private function tryToInitRequest($name)
    {
        if (isset($this->requests[$name])) {
            return new $this->requests[$name]($this->getRequestSettings($name), $this->options);
        } else {
            throw new RequestNotExistsException('The request "' . $name . '"" does not exists, check request name.');
        }
    }

    /**
     * Get Request Settings
     *
     * @param $name
     *
     * @return array|null
     */
    private function getRequestSettings($name)
    {
        return isset($this->settings[$name]) ? $this->settings[$name] : null;
    }

    /**
     * Set Options
     *
     * @param $options
     *
     * @return $this
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }
}
