<?php namespace App\Libraries\Scraper\Contracts;

/**
 * Interface Translator
 *
 * @package App\Libraries\Scraper\Contracts
 */
interface Translator
{
    /**
     * Translator constructor.
     *
     * @param array $data
     * @param null  $options
     *
     * @internal param TpLogger $logger
     */
    public function __construct(array $data, $options = null);

    /**
     * Store
     *
     * @return void
     */
    public function store();
}