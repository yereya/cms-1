<?php namespace App\Libraries\Scraper\Contracts;

/**
 * Interface Provider
 *
 * @package App\Libraries\Scraper\Contacts
 */
interface Provider
{
    /**
     * Fetch
     *
     * @return void
     */
    public function fetch();

    /**
     * Prepare
     *
     * @return void
     */
    public function prepare();

    /**
     * Store
     *
     * @return void
     */
    public function store();

    /**
     * Has Data
     *
     * @return bool
     */
    public function hasData();
}