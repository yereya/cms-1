<?php


namespace App\Libraries\Scraper\Providers;


use App\Libraries\Scraper\Provider;
use App\Libraries\TpLogger;

abstract class Outbrain extends Provider
{
    /**
     * @var string
     */
    protected $account_id;
    /**
     * @var string
     */
    protected $encoded_credentials;
    /**
     * @var string
     */
    protected $ob_token_v1;
    /**
     * @var string
     */
    protected $campaigns;



    /**
     * @var string
     */
    protected $water_mark_connection = 'dwh';

    /**
     * @var string $table_name
     */
    protected $water_mark_table_name = 'high_water_mark_tbl';


    /**
     * Outbrain constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        parent::__construct($logger, $translators, $settings, $options);

        # --- Set Credentials ---
        $credentials = include(config_path().'/outbrain.php');

        $this->account_id  = $credentials['account_id'];
        $this->encoded_credentials = $credentials['encoded_credentials'];

    }
}
