<?php


namespace App\Libraries\Scraper\Providers;


use App\Libraries\Scraper\Provider;
use App\Libraries\TpLogger;

abstract class Taboola extends Provider
{

    /**
     * @var string
     */
    protected $account_id;
    /**
     * @var string
     */
    protected $client_id;
    /**
     * @var string
     */
    protected $client_secret;
    /**
     * @var string
     */
    protected $grant_type;
    /**
     * @var string
     */
    protected $access_token;
    /**
     * @var string
     */
    protected $token_type;
    /**
     * @var array
     */
    protected $campaigns;

    /**
     * @var string
     */
    protected $water_mark_connection = 'dwh';

    /**
     * @var string $table_name
     */
    protected $water_mark_table_name = 'high_water_mark_tbl';


    /**
     * Taboola constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        parent::__construct($logger, $translators, $settings, $options);

        # --- Set Credentials ---
        $credentials = include(config_path().'/taboola.php');

        $this->client_id     = $credentials['client_id'];
        $this->client_secret = $credentials['client_secret'];
        $this->grant_type    = $credentials['grant_type'];

    }
}



