<?php


namespace App\Libraries\Scraper\Providers;


use App\Libraries\Scraper\Provider;
use App\Libraries\TpLogger;

abstract class Hubspot extends Provider
{
    /**
     * @var string
     */
    protected $hapikey;
    /**
     * @var string
     */
    protected $num_of_results;

    /**
     * Hubspot constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        parent::__construct($logger, $translators, $settings, $options);

        # --- Set Credentials ---
        $credentials = include(config_path().'/hubspot.php');

        $this->hapikey  = $credentials['hapikey'];
        $this->num_of_results  = $credentials['num_of_results'];

    }
}








