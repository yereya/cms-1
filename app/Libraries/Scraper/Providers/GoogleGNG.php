<?php namespace App\Libraries\Scraper\Providers;

use App\Libraries\TpLogger;
use App\Libraries\Scraper\Provider;
use App\Entities\Repositories\Sites\SettingsRepo;

use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;


/**
 * Class Google
 *
 * @package App\Libraries\Scraper\Providers
 */
abstract class GoogleGNG extends Provider
{
    /**
     * @var mixed
     */
    protected $user;

    /**
     * @ver AdWordsSession
     */
    protected $session;

    /**
     * @var null - header options
     */
    protected $header_options;

    /**
     * @ver Google\Auth\Credentials\UserRefreshCredentials
     */
    protected $user_credentials;


    /**
     * Google constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        parent::__construct($logger, $translators, $settings, $options);

        $this->logger->info('Started provider: GoogleGNG');
        if (isset($this->options['date-range-start'])) {
            $this->logger->info('Range: [' . $this->options['date-range-start'] . ' - ' . $this->options['date-range-end'] . ']');
        }

        $this->user_credentials = (new OAuth2TokenBuilder())
            ->fromFile(config_path() . '/adwords-auth-gng.ini')
            ->build();

        $this->header_options = null;
    }

    /**
     * @param string $client_customer_id
     *
     * @return AdWordsSession
     */
    protected function makeSession($client_customer_id = null)
    {
        $session = (new AdWordsSessionBuilder())
            ->fromFile(config_path() . '/adwords-auth-gng.ini')
            ->withOAuth2Credential($this->user_credentials);

        if ($client_customer_id) {
            $session->withClientCustomerId($client_customer_id);
        }

        return $session->build();
    }

    /**
     * Request Authorization
     */
    private function requestAuthorization()
    {
        $OAuth2Handler      = $this->user->GetOAuth2Handler();
        $authorization_url  = $OAuth2Handler->GetAuthorizationUrl($this->user->GetOAuth2Info(), null, true);
        $authorization_code = $this->requestAuthorizationCode($authorization_url);
        $access_token       = $OAuth2Handler->GetAccessToken($this->user->GetOAuth2Info(), $authorization_code);

        $this->user->SetOAuth2Info($access_token);
    }

    /**
     * Request Authorization Code
     *
     * @param $authorizationUrl
     *
     * @return mixed
     */
    private function requestAuthorizationCode($authorizationUrl)
    {
        print "Log in to your AdWords account and open the following URL:\n{$authorizationUrl}\n\n";
        print "After approving the token enter the authorization code here: ";

        $stdin = fopen('php://stdin', 'r');
        $code  = trim(fgets($stdin));
        fclose($stdin);

        return $code;
    }

    /**
     * Setter headers options
     *
     * @param $options
     */
    protected function setHeaderOptions($options)
    {
        $this->header_options = $options;
    }
}
