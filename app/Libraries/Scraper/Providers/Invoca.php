<?php


namespace App\Libraries\Scraper\Providers;


use App\Libraries\Scraper\Provider;
use App\Libraries\TpLogger;

abstract class Invoca extends Provider
{

    /**
     * @var string
     */
    protected $account_id;
    /**
     * @var string
     */
    protected $account_name;
    /**
     * @var string
     */
    protected $invoca_network_id;
    /**
     * @var string
     */
    protected $invoca_publisher_id;
    /**
     * @var string
     */
    protected $invoca_advertiser_id_temp;
    /**
     * @var string
     */
    protected $invoca_account_type;
    /**
     * @var string
     */
    protected $invoca_api_version;
    /**
    * @var string
     */
    protected $invoca_outh_token;
    /**
     * @var string
     */
    protected $water_mark_connection = 'dwh';

    /**
     * @var string $table_name
     */
    protected $water_mark_table_name = 'high_water_mark_tbl';


    /**
     * Invoca constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        parent::__construct($logger, $translators, $settings, $options);

        # --- Set Credentials ---
        $credentials = include(config_path().'/invoca.php');

        $this->invoca_outh_token  = $credentials['outh_token'];
        $this->invoca_network_id  = $credentials['network_id'];
        $this->account_name       = $credentials['account_name'];
        $this->invoca_api_version = $credentials['api_version'];
        $this->invoca_account_type = 'networks';


    }
}