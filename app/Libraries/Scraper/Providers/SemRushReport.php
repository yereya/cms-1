<?php


namespace App\Libraries\Scraper\Providers;


use App\Entities\Models\Bo\ReportFieldSource;
use App\Entities\Models\Mrr\MrrCompetitorReportSemrush;
use App\Libraries\Scraper\ReportTrait;
use League\Csv\Reader;
use mysql_xdevapi\Exception;

abstract class SemRushReport extends SemRush
{
    use ReportTrait;

    /**
     * @var int
     */
    protected $publisher_id = 165;
    /**
     * @var string
     */
    protected $publisher_name = 'Semrush';

    /**
     * @var int $report_id
     */
    protected $report_id = 40;
    /**
     * @var array $date_ranges
     */
    protected $date_ranges = [
        'CUSTOM_DATE',
        'Today',
        'Yesterday',
        'LastSevenDays',
        'ThisWeek',
        'LastWeek',
        'LastFourWeeks',
        'ThisMonth',
        'LastMonth',
        'LastThreeMonths',
        'LastSixMonths',
        'ThisYear',
        'LastYear'
    ];
    /**
     * @var string $report_type
     */
    protected $report_type;

    private $SEMRushDomain = 'https://api.semrush.com/';
    private $key = '7eb89faaf4ab766c304d93bac1bfc88c';
    private $domainColumns = 'Db,Dn,Rk,Or,Ot,Oc,Ad,At,Ac,Sh,Sv';
    private $domainRankHistoryColumns = 'Rk,Or,Xn,Ot,Oc,Ad,At,Ac,Dt,FKn,FPn';
    private $displayLimit;


    /**
     * Fetch
     */
    public function fetch()
    {
        $this->logger->info('Started fetching Semrush Report Type: ' . $this->report_type);
        $date_to_run_at = explode(',',$this->options["days_to_run_at"]);
        $this->displayLimit = $this->options["limit"] ?? 1;
        if($date_to_run_at && !in_array(date('d'),  $date_to_run_at)){
            $this->data = [];
            $this->logger->info('The report is set to run only on the ' .  json_encode($date_to_run_at) . ' of each month. ( as you set in the --days parameter )');
            return;
        }
        $this->logger->info('Before getting domains');
        $domains = $this->getDomainsFromSpreadsheet();
        $this->logger->info('After getting domains and before fetching metrics');

        $this->data[] = $this->fetchMetricsFromSemrush($domains);
        $this->logger->info('After fetching metrics');

    }

    private function getDomainsFromSpreadsheet()
    {
        $results = [];
        $spreadsheetId = '1qjirRLxTaDSKEuZn7TcgOneVLDRuewUAA1uEEEnUo-I';
        $url = 'https://sheets.googleapis.com/v4/spreadsheets/' . $spreadsheetId . '?fields=sheets&key=AIzaSyCFgxl_XWHOo1aEOkfNoJ7cewasQkQnUAs';
        $response = file_get_contents($url);
        $data = json_decode($response,true)['sheets'][0]['data'][0]['rowData'];
        $headers = array_map(function($row) {
            return $row['formattedValue'];
        },$data[0]['values']) ;

        foreach (array_slice($data,1) as $row){
            $rowParsedData = array_map(function($row){
                return $row['formattedValue'];
            },$row['values']) ;

            $results[] = array_combine($headers,$rowParsedData);
        }
        return $results;

    }


    private function fetchMetricsFromSemrush($domains){
        $stats = [];
        foreach ($domains as $domain){
            $domain_url = $domain['url'];
            if(!$domain_url || !$domain['geo']){
                throw new Exception('No values for domain - ' . $domain_url . ' in Google Spreadsheet');
            }

            $url = $this->SEMRushDomain . '?key=' . $this->key . '&type=domain_rank_history&export_columns=' . $this->domainRankHistoryColumns . '&domain=' . $domain_url
                . '&database=' .$domain['geo']. '&display_limit=' . $this->displayLimit;

            $res = file_get_contents($url);
            $splittedRes = preg_split("/\r\n/",$res);

            if(strpos($splittedRes[0],'ERROR') === false){
                $headersArray  = explode(';',$splittedRes[0]);
                for($i = 1; $i < sizeof($splittedRes) - 1; $i++) {

                    $body = explode(';', $splittedRes[$i]);
                    $semrushData = array_combine($headersArray, $body);
                    $semrushData['error'] = 0;
                    $stats[$domain_url][] = array_merge($semrushData, $domain);
                }
            }else{
                $domain['Adwords Traffic'] = null;
                $domain['Adwords Cost'] = null;
                $domain['error'] = 1;

                $stats[$domain_url] = $domain ;
            }

        }
        return $stats;
    }



    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)
            ->where('publisher_id', $this->publisher_id)
            ->where('status', true)
            ->get()
            ->pluck('source_name')
            ->toArray();
    }
    /**

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @param $key
     * @param $value
     */
    public function setOptions($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * Get Logger
     *
     * @return \App\Libraries\TpLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    protected function queryCsv(Reader $csv)
    {
        $csv->setOffset(2); // Skip the title and header
        $csv->addFilter(function ($row) {
            return strpos($row[0], 'Total') === false;
        });
    }
}
