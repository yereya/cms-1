<?php namespace App\Libraries\Scraper\Providers;

use Exception;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDefinition;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;
use Google\AdsApi\AdWords\v201809\cm\DateRange;
use Google\AdsApi\AdWords\v201809\cm\Selector;
use League\Csv\Reader;
use App\Entities\Models\Bo\Account;
use App\Libraries\Scraper\ReportTrait;
use App\Entities\Models\Bo\ReportFieldSource;

/**
 * Class GoogleReport
 *
 * @package App\Libraries\Scraper\Providers
 */
abstract class GoogleReport extends Google
{
    use ReportTrait;

    /**
     * @var array $customers
     */
    protected $customers = [];

    /**
     * This variable is used for defining when the fetching of the reports
     * process should pull each report on it's own instead of an entire batch
     * used for adding a suffix for each report file.
     *
     * @var bool fetch_report_by_day
     */
    protected $fetch_report_by_day = false;

    /**
     * @var
     */
    protected $report_id;

    /**
     * @var int
     */
    protected $publisher_id = 95;

    /**
     * @var string
     */
    protected $publisher_name = 'Google';

    /**
     * @var array $date_ranges
     */
    protected $date_ranges = [
        'CUSTOM_DATE',
        'TODAY',
        'YESTERDAY',
        'LAST_WEEK',
        'LAST_BUSINESS_WEEK',
        'THIS_MONTH',
        'LAST_MONTH',
        'ALL_TIME',
        'LAST_14_DAYS',
        'LAST_30_DAYS',
        'THIS_WEEK_SUN_TODAY',
        'THIS_WEEK_MON_TODAY',
        'LAST_WEEK_SUN_SAT',
    ];

    /**
     * @var string $report_type
     */
    protected $report_type;

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->logger->info('Started fetching Google. Report Type: ' . $this->report_type);

        $account_id = $this->options['account_id'] ?? null;

        $this->getCustomersByAdvertisers($account_id);
        $this->setReportDateRange();
        $this->getReportsForAllCustomers();
    }

    /**
     * Get Customers By Advertisers
     *
     * @param $account_id
     */
    protected function getCustomersByAdvertisers($account_id)
    {
        if (is_null($account_id)) {
            $this->customers = Account::where('status','active')
                ->where('mcc', 1)->whereHas('advertiser', function ($query) {
                $query->where('advertisers.status', 'active');
            })->whereHas('publisher', function ($query) {
                $query->where('publishers.id', $this->publisher_id);
            })->whereHas('reports.report', function ($query) {
                $query->where('reports.id', $this->report_id);
            })->get()->pluck('source_account_id')->filter()->toArray();
        } else {
            $this->customers = Account::where('id', $account_id)->pluck('source_account_id')->filter()->toArray();
        }

        $this->logger->debug(['Getting customers', $this->customers]);
    }

    /**
     * Get Report For All Customers
     *
     * @return void
     */
    private function getReportsForAllCustomers()
    {
        $dates = null;
        if ($this->fetch_report_by_day) {
            $dates = $this->setSingleDateReportDateRange();
        }
        foreach ($this->customers as $customer_id) {
            if ($dates) {
                $this->getReportDateRangeByDay($customer_id, $dates);
            } else {
                $this->getReport($customer_id);
            }
        }
    }

    /**
     * Get Report For Single Date
     *
     * @param int   $customer_id
     * @param array $dates
     */
    private function getReportDateRangeByDay($customer_id, $dates)
    {
        foreach ($dates as $date) {
            $this->options['date-range-end']   = $date;
            $this->options['date-range-start'] = $date;
            $this->setDateRangeStartAndEnd();

            $this->getReport($customer_id, $date);
        }
    }

    /**
     * Download Report
     *
     * @param ReportDefinition $reportDefinition
     * @param int              $customer_id
     * @param string|null      $path_suffix
     *
     * @throws \Exception
     */
    protected function downloadReport(ReportDefinition $reportDefinition, $customer_id, $path_suffix = null)
    {
        $this->session    = $this->makeSession($customer_id);

        $reportDownloader = new ReportDownloader($this->session);

            try {
                $reportDownloadResult = $reportDownloader->downloadReport($reportDefinition);
                $reportDownloadResult->saveToFile($this->getReportFilePath($customer_id . $path_suffix));
            } catch (Exception $e) {
                $this->logger->error(['Report Download Error: ', $e->getMessage()], $e);

                throw $e;
            }

    }

    /**
     * Get Report
     *
     * @param int  $customer_id
     * @param null $date
     *
     * @return void
     */
    protected function getReport($customer_id, $date = null)
    {
        $path_suffix = $date ? '_' . $date : null;

        $this->downloadReport($this->prepareReportDefinition(), $customer_id, $path_suffix);
        $parsed_csv       = $this->parseCsv($this->getReportFilePath($customer_id . $path_suffix));
        $parsed_csv->date = $date;
        $this->data[]     = $parsed_csv;

        $this->logger->info('Download customer ' . $customer_id . ' report successful');
    }

    /**
     * Prepare Report Definition
     *
     * @return ReportDefinition
     */
    protected function prepareReportDefinition()
    {
        $selector = $this->setSelector();
        $this->setSelectorDateRange($selector);

        return $this->setReportDefinition($selector);
    }

    /**
     * Set Selector
     *
     * @return Selector
     */
    abstract protected function setSelector();

    /**
     * Set Selector Date Range
     *
     * @param Selector $selector
     */
    private function setSelectorDateRange(Selector $selector)
    {
        switch($this->date_range_type){
            case 'CUSTOM_DATE':
                $selector->setDateRange(new DateRange($this->date_range_start->format('Ymd'),
                    $this->date_range_end->format('Ymd')));
                break;
        }
    }

    /**
     * Set Report Definition
     *
     * @param Selector $selector
     *
     * @return ReportDefinition
     */
    abstract protected function setReportDefinition(Selector $selector);

    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)
            ->where('publisher_id', $this->publisher_id)
            ->where('status', true)
            ->get()
            ->pluck('source_name')
            ->filter()
            ->toArray();
    }

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @param $key
     * @param $value
     */
    public function setOptions($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * Get Logger
     *
     * @return \App\Libraries\TpLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    protected function queryCsv(Reader $csv)
    {
        $csv->setOffset(2); // Skip the title and header
        $csv->addFilter(function ($row) {
            return strpos($row[0], 'Total') === false;
        });
    }
}
