<?php


namespace App\Libraries\Scraper\Providers;


use App\Entities\Models\Bo\ReportFieldSource;
use App\Libraries\Scraper\ReportTrait;
use League\Csv\Reader;

abstract class HubspotReport extends Hubspot
{

    use ReportTrait;

    /**
     * @var int
     */
    protected $publisher_id = 160;
    /**
     * @var string
     */
    protected $publisher_name = 'Hubspot';

    /**
     * @var int $report_id
     */
    protected $report_id = 35;

    /**
     * @var array $date_ranges
     */
    protected $date_ranges = [
        'CUSTOM_DATE',
        'Today',
        'Yesterday',
        'LastSevenDays',
        'ThisWeek',
        'LastWeek',
        'LastFourWeeks',
        'ThisMonth',
        'LastMonth',
        'LastThreeMonths',
        'LastSixMonths',
        'ThisYear',
        'LastYear'
    ];

    /**
     * @var array $associations
     */
    protected $associations = [
        'companies',
        'contacts'
    ];

    /**
     * @var string $report_type
     */
    protected $report_type;

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->logger->info('Started fetching Hubspot. Report Type: ' . $this->report_type);

        $this->setReportDateRange(); // Not used in the meantime

        $this->data[]  = $this->fetchData();

    }
    /**
     * Fetch data
     */
    private function fetchData()
    {
        $results = [];
        $after_index = null;
         do {   // Used for pagination
            # Build URL
            $url = $this->buildUrl($after_index);
            # Send GET request and get response as json
            $data_as_json = file_get_contents($url);
            # Parse the data
            $data = json_decode($data_as_json, true);
            # Flatten each row
            foreach ($data['results'] as $row){
                $new_row = array_merge($row['properties'],array_map(function($val){
                    return is_array($val)?'':$val;
                    },$row));

                # Extract Associations from the nested array
                $companies_array = data_get($row,'associations.companies.results') ? data_get($row,'associations.companies.results') : [];
                $contacts_array  = data_get($row,'associations.contacts.results')  ? data_get($row,'associations.contacts.results' ) : [];

                foreach ($companies_array as $company)
                    $companies[] = $company['id'];
                foreach ($contacts_array as $contact)
                    $contacts[] = $contact['id'];
                # Combine all the nested arrays
                $new_row = array_merge(
                    $new_row,
                    array('companies' => implode(' ',$companies)),
                    array('contacts' => implode(' ' ,$contacts))
                );
                $results[] = $new_row;
            }
            if (isset($data['paging'])) {
                $after_index = $data['paging']['next']['after'];
            } else {
                $after_index = null;
            }
        }while($after_index);

        $this->logger->info('Fetching data is finished, returned : ' . sizeof($results) . ' results.' );
        return $results;
    }

    /**
     * Build URL
     *
     * @return string
     */
    public function buildUrl($after_index)
    {
        # -- Example --
        //https://api.hubapi.com/crm/v3/objects/deals?hapikey=214e947a-e541-4dd6-801c-668b0738f0be&properties=hs_object_id,hs_lastmodifieddate,budget_start_date,closedate,createdate&includeAssociations=true

        $url = 'https://api.hubapi.com/crm/v3/objects/deals?hapikey='. $this->hapikey; // Base URL
        $properties = implode($this->getFields(),','); // Get fields from DB
        $url .= '&properties='. $properties; // Add properties
        $url .= '&limit=' . $this->num_of_results; // Set number of returned results
        $url .= '&associations=' . implode(",",$this->associations); // Set associations
        if($after_index){  // Used for pagination, index to start from
            $url.= '&after='.$after_index;
        }
        $this->logger->info('Generated URL : ' . $url );

        return $url;
    }
    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)
            ->where('publisher_id', $this->publisher_id)
            ->where('status', true)
            ->get()
            ->pluck('source_name')
            ->toArray();
    }

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @param $key
     * @param $value
     */
    public function setOptions($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * Get Logger
     *
     * @return \App\Libraries\TpLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    protected function queryCsv(Reader $csv)
    {
        $csv->setOffset(2); // Skip the title and header
        $csv->addFilter(function ($row) {
            return strpos($row[0], 'Total') === false;
        });
    }

}



