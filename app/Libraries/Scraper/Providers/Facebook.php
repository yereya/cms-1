<?php


namespace App\Libraries\Scraper\Providers;

use App\Libraries\Scraper\Provider;
use Cache;
use FacebookAds\Api as Api;
use GuzzleHttp\Client;
use App\Libraries\TpLogger;
use Config;


abstract class Facebook extends Provider
{

    /**
     * @var string
     */
    protected $access_token_default;
    /**
     * @var string
     */
    protected $access_token;
    /**
     * @var string
     */
    protected $app_id;
    /**
     * @var string
     */
        protected $app_secret;
    /**
     * @var string
     */
     protected $account_id;


    /**
     * @var Api
     */
    protected $api;

    /**
     * @var Client
     */
    protected $client;

    /**
     * Facebook constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        parent::__construct($logger, $translators, $settings, $options);
        # --- Set Credentials ---
        $credentials                = include(config_path().'/facebook.php');
        $this->access_token_default = $credentials['access_token'];
        $this->app_id               = $credentials['app_id'];
        $this->app_secret           = $credentials['app_secret'];
        $this->account_id           = $credentials['account_id'];

        # --- Init and set Api ---
        $this->api = Api::init($this->app_id, $this->app_secret, $this->access_token);

        # ---Update logger's info ---
        $this->logger->info('Started provider: Facebook');
        if (isset($this->options['date-range-start'])) {
            $this->logger->info('Range: [' . $this->options['date-range-start'] . ' - ' . $this->options['date-range-end'] . ']');
        }
    }
}
