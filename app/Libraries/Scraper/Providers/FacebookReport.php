<?php



namespace App\Libraries\Scraper\Providers;


use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountCampaign;
use App\Entities\Models\Bo\ReportFieldSource;
use App\Entities\Models\Dwh\DwhFactTracks;
use App\Entities\Repositories\Bo\ConversionRepo;
use App\Libraries\Scraper\ReportTrait;
use App\Libraries\TpLogger;
use League\Csv\Reader;
use App\Entities\Models\Bo\Conversion;


abstract class FacebookReport extends Facebook
{
    use ReportTrait;

    /**
     * @var int
     */
    protected $publisher_id = 98;
    /**
    * @var string
    */
    protected $publisher_name = 'Facebook';

    /**
     * @var array $ad_accounts
     */
    protected $ad_accounts;


    /**
     * @var array $campaigns
     */
    protected $campaigns;

    /**
     * @var int $report_id
     */
    protected $report_id = 26;
    /**
    * @var array $date_ranges
     */
    protected $date_ranges = [
        'CUSTOM_DATE',
        'Today',
        'Yesterday',
        'LastSevenDays',
        'ThisWeek',
        'LastWeek',
        'LastFourWeeks',
        'ThisMonth',
        'LastMonth',
        'LastThreeMonths',
        'LastSixMonths',
        'ThisYear',
        'LastYear'
    ];
    /**
     * @var string $report_type
     */
    protected $report_type;
    /**
     * @var int $chunk_size
     */
    protected $chunk_size = 500;

    /**
     * Fetch
     */
    public function fetch()
    {

        $this->getAdAccounts();

        if (!$this->ad_accounts) {
            return;
        }

        $this->logger->info('Started fetching Facebook. Report Type: ' . $this->report_type);

        $this->setReportDateRange();

        # --- Update Access Token ---
        $this->getAndUpdateAccessToken();

        foreach ($this->ad_accounts as $ad_account){

            $this->fetchData($ad_account);
        }
    }

        /**
     * Get Main Accounts
     *
     * @param $account_id
     */
    private function getAdAccounts()
    {
        $ad_accounts = Account::where('status','active')->whereHas('publisher', function ($query) {
            $query->whereIn('publishers.id', [$this->publisher_id]);
        })->with('fields')->get();

         $this->ad_accounts = $ad_accounts->toArray();
    }

    public function OlduploadConversions($conversions)
    {
        if(!$conversions)
            return;
        $data = array(); // Will hold the parsed conversions
        foreach($conversions->toArray() as $account_records) { // The records are grouped by source account id
            foreach (array_chunk($account_records, $this->chunk_size) as $partial_conversions) { // Loop over the data by small chunks
                foreach ($partial_conversions as $conversion) { // For a single conversion
                    $record = array();
//                    $fbc = preg_replace('/[{}]/u', '', $conversion['clkid']);
                    $fbc = $conversion['clkid'];
                    $order_date = $conversion['source_click_date_tz'];
                    $order_total = $conversion['price'];

//                    $record["match_keys"]["country"] = hash("sha256", 'Australia');
                    $record["match_keys"]["fbc"] = $fbc;
                    $record["event_time"] = strtotime($order_date);
//                    $record["event_name"] = "Purchase"; // TODO replace correctly
                    $record["event_name"] = "Lead"; // TODO replace correctly
//                    $record["event_name"] = $conversion['event']; // TODO replace correctly
                    $record["currency"] = "USD"; // TODO extract Currency
                    $record["value"] = $order_total;
                    $record["custom_data"] = array('fbc' => $fbc);
                    $data[] = $record;
                }
            }
        }
//        $data1=array();
//        $order_date = '2020-11-29 03:50:06';
//        $order_total = '1';
//
//
//        $data1["match_keys"]["country"]=hash("sha256","Australia");
//        $data1["event_time"] = strtotime($order_date);
//        $data1["event_name"] = "Purchase";
//        $data1["currency"] = "AUD";
//        $data1["value"] = $order_total;
//        $data1["custom_data"] = array('fbc' => 'fb.1.1606579759784.IwAR1aY7WT5J5EgWoawqA5ya901wkMGWZovBaCvqYQWbspaRVTcBftbygKvMk');
//
//        $data2=array();
//        $order_date = '2020-11-29 03:50:06';
//        $order_total = '1';
//
//        $data2["match_keys"]["country"]=hash("sha256","Australia");
//        $data2["event_time"] = strtotime($order_date);
//        $data2["event_name"] = "Purchase";
//        $data2["currency"] = "AUD";
//        $data2["value"] = $order_total;
//        $data2["custom_data"] = array('fbc' => 'fb.1.1606606971276.IwAR1v3PlIyW7Km6Ma2yPDrEJXkPAoESvSSV5ZNslcP7DOabQJPJIGteyMuoA');
//        $data = array($data1,$data2);

        $access_token = '';

//        $data_json = json_encode(array($data));
        $data_json = json_encode($data);
        $fields = array();
        $fields['access_token'] = $access_token;
//        $fields['upload_tag'] = 'upload_' . date("Y-m-d_H:i:s");//uniqid(); // You should set a tag here (feel free to adjust)
        $fields['upload_tag'] = uniqid(); // You should set a tag here (feel free to adjust)
        $fields['data'] = $data_json;


        $ch = curl_init();
        curl_setopt_array($ch, array(
            // Replace with your offline_event_set_id
            CURLOPT_URL => "https://graph.facebook.com/v9.0/4033315853347643/events",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>  http_build_query($fields),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
//                "content-type: multipart/form-data",
                "Accept: application/json"  ),
        ));
//        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);

    }

    public function sendConversionAPI(){
        // Equal to Send Pixel, Working also for offline conversion, only regular conversion / pixel
    $curl = curl_init();

        $data = array( // main object
            "data" => array( // data array
                array(
                        "event_name"=> "Lead",
                        "event_time"=> time(),
                        "user_data" => [
                            "fbc" => "fb.1.1611439606340.IwAR1AvHIWwVzeKXofITNJa6fID6erJDTkHjdorWXqD4-9Wfa6snDvF7NOwi0",
                            "fbp" => "fb.1.1611439606354.44399384"
                        ],
                        "custom_data" => [
                            "value" => 100.2,
                            "currency" => "USD",
                        ]
                    ), // single data array entry

            ),
//            "upload_tag" => "test_upload_anton_24_01_2021_01", // Relevant for offline conversions
            "access_token" => ""
        );

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://graph.facebook.com/v9.0/4033315853347643/events',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    echo $response;
    }

    public function improveConversion($conversion){
        switch ($conversion['event']) {
            case 'lead':
                $conversion['event'] = 'Lead';
                break;
            case 'sale':
                $conversion['event'] = 'Purchase';
                break;
            default:
                break;
        }
        return $conversion;
    }

    public function appendPbpByTrackIdToConversions()
    {
        // Extract track_id
        $conversions_track_ids = array_pluck($this->conversions,'track_id');

        // Find rows by track_id in dwh_fact_tracks
        $tracks = DwhFactTracks::select('track_id','fbp','currency')->whereIn('track_id',$conversions_track_ids)->get()->toArray();

        // add pbp from fact_tracks to the conversions
        foreach($tracks as $track){
            $conversion_idx = array_search($track['track_id'],$conversions_track_ids);
            $this->conversions[$conversion_idx]['fbp'] = $track['fbp'];
            $this->conversions[$conversion_idx]['currency'] = $track['currency'];
        }

    }

    public function updateSuccessulUpload($conversion){
        Conversion::where('id', $conversion['id'])
            ->update(['status' => 1,'response' => 'Success']);
    }

    public function updateFailedUpload($conversion, $error, $upload_process = ''){
        Conversion::where('id', $conversion['id'])
            ->update(['status' => 1,'error' => $upload_process . ' - ' .$error->getMessage()]);
    }

    public function extractFbcToken($token){
        if (strpos($token, '{{') !== false) {
            throw new \Exception('Bad clkid : ' . $token );
        }
        $fbc = str_replace('{','',$token);
        $fbc =str_replace('}','',$fbc);

        return $fbc;
    }

    public function sendPixel($conversion)
    {
//        $params = array(
//            'data' => array(
//                array(
//                    'event_name' => 'Lead',
//                    'event_time' => '1611500534',
//                    "currency" => "USD",
//                    "value" => "3",
//                    'user_data' => array(
//                        'fbc' => 'fb.1.1611437132545.IwAR03YmhOuwpuJV3t3SlILdcJdDEVrAeqFmSx_HEtl4o3b9-Yc-39j3yijRc',
//                        'fbp' => 'fb.1.1611437132554.221762622'
//                    ),
//                    "custom_data" => array(
//                        "value" => 100.2,
//                        "currency" => "USD",
//                    )
//                )
//            )
//        );
        try {

            $params = array(
                'data' => array(
                    array(
                        'event_name' => $conversion['event'],
                        'event_time' => strtotime($conversion['source_click_date_tz']),
                        'user_data' => array(
                            'fbc' => $this->extractFbcToken($conversion['clkid']),
                            'fbp' => $conversion['fbp']
                        ),
                        "custom_data" => array(
                            "currency" => $conversion['currency'],
                            "value" => $conversion['price'],
                        )
                    )
                )
            );

            $fb = new \Facebook\Facebook([
                'app_id' => '425659281950369',
                'app_secret' => '788dbedd5724e9a68e0736b1e918fe0f',
                'default_graph_version' => 'v9.0',
                'default_access_token' => $this->access_token,
            ]);
            $response = $fb->post($this->pixel_endpoint, $params);
            return $response;
        }catch (\Facebook\Exceptions\FacebookResponseException $e) {
            $this->logger->warning('--- Error in Pixels Method : ' . $e->getMessage());
            $this->updateFailedUpload($conversion, $e, 'Pixel');
            return false;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            $this->logger->warning('--- Error in Pixels Method : ' . $e->getMessage());
            $this->updateFailedUpload($conversion, $e, 'Pixel');
            return false;
        }catch (\Exception $e){
            $this->logger->warning('--- Error in Pixels Method : ' . $e->getMessage());
            $this->updateFailedUpload($conversion, $e, 'Pixel');
            return false;
        }


    }
    public function filterByEvents($conversions){


        try{
            $filtered_conversions = array_filter($conversions, function($row) {
                if(!isset($row['event']) || $row['event'] == 'clickout')
                    return false;
                return true;
            });

            return $filtered_conversions;
        }catch (\Exception $e){
            $this->logger->error('Error : ' . $e);
            throw new \Exception($e);
        }

    }

     public function getConversions(){
         $conversions = [];
         $this->logger->info('Before get conversions');
//         $conversions_by_source_account =  (new ConversionRepo())->getByProvider('facebook');
         $conversions_by_source_account =  (new ConversionRepo())->getByProviderWithoutClickout('facebook');
         $this->logger->info('After get conversions' );
         foreach ($conversions_by_source_account as $source_account_id => $conversion_by_source_account){
             $conversions = array_merge($conversions,$conversion_by_source_account->toArray());
         }
         $this->logger->info('After foreach , array size :' . sizeof($conversions));
         $filtered_conversions = $this->filterByEvents($conversions);
         $this->logger->info('After filterByEvents, array size :' . sizeof($filtered_conversions));
         return $filtered_conversions;
    }

     public function uploaldOfflineConversions($conversion){
         try {

             $upload_tag = 'upload_anton_' . date("Y_m_d_H_i_s");

//             $params = array(
//                 'upload_tag' => $upload_tag,
//                 'data' => array(
//                     array(
//                         'match_keys' => array(
//                             'fbc' => 'fb.1.1611437132545.IwAR03YmhOuwpuJV3t3SlILdcJdDEVrAeqFmSx_HEtl4o3b9-Yc-39j3yijRc',
//                             'fbp' => 'fb.1.1611437132554.221762622'
//                         ),
//                         'event_name' => 'Lead',
//                         'event_time' => '1606694194',
//                         "currency" => "USD",
//                         "value" => "2",
//                         'user_data' => array(
//                             'fbp' => 'fb.1.1611437132554.221762622'
//                         ),
//                     )
//                 )
//             );
             $params = array(
                 'upload_tag' => $upload_tag,
                 'data' => array(
                     array(
                         'match_keys' => array(
                             'fbc' => $this->extractFbcToken($conversion['clkid']),
                             'fbp' => $conversion['fbp'],
                         ),
                         'event_name' => $conversion['event'],
                         'event_time' => strtotime($conversion['source_click_date_tz']),
                         "currency"   => $conversion['currency'],
                         "value"      => $conversion['price'],
                         'user_data' => array(
                             'fbc' => $this->extractFbcToken($conversion['clkid']),
                             'fbp' => $conversion['fbp']
                         ),
                         'custom_data' => array(
                             'event_source' => 'website'
                         )
                     )
                 )
             );

             $fb = new \Facebook\Facebook([
                 'app_id' => '425659281950369',
                 'app_secret' => '788dbedd5724e9a68e0736b1e918fe0f',
                 'default_graph_version' => 'v9.0',
                 'default_access_token' => $this->access_token,
             ]);

             $response = $fb->post($this->offline_conversions_endpoint,$params );
             return $response;
         }catch (\Exception $e){
             $this->logger->warning('--- Error in Offline Conversion Method : ' . $e->getMessage());
             $this->updateFailedUpload($conversion, $e, 'offline_conversions');
//             throw new Exception($e);
             return false;
         }
     }
    /**
     * Get Data
     *
     * @param $fields
     * @param $params
     * @param $ad_account_id
     *
     */

    private function getData($fields,$params,$ad_account_id)
    {
        # ---- Request data from facebook's Api ----

        $fb = new \Facebook\Facebook([
            'app_id' => $this->app_id,
            'app_secret' => $this->app_secret,
            'default_graph_version' => 'v9.0',
            'default_access_token' => $this->access_token, // optional
        ]);

        # - Set dates -
        $start_date = $this->options['date-range-start'];
        $end_date = $this->options['date-range-end'];
        ;

        // Returns a `FacebookFacebookResponse` object
        try {
        $response = $fb->get(
            '/act_'.$ad_account_id.'?fields=name,id,ads.limit(500){id,name,spend,insights.limit(500).time_range({"since":"'.$start_date.'","until":"'.$end_date.'"}).time_increment(1){'.$fields.'}}',
            $this->access_token
        );


        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            $this->logger->error('Error in FacebookReport.php getData function - facebook->get.');
            throw $e;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            $this->logger->error('Error in FacebookReport.php getData function - facebook->get.');
            throw $e;
        }

        # Extract data from FB Object to $graphNodeArr
        $graphNode = $response->getGraphNode();
        $graphNodeArr = $graphNode->asArray();
        $data_ads = [];
        $cursor = $response->getDecodedBody()['ads']['paging'];

        # Loop over all the ads and store them inside $data_ads ( Using Paging )
        do{
            foreach ($graphNodeArr['ads'] as $key_ad => $ad) {
                $ad_id = $ad['id'];
                if (array_key_exists('insights', $ad) and !empty($ad['insights'])) {  // if insights section exists for this ad
                    $ad_data = [];
                    # Check whether there is paging field inside insights
                    if(array_key_exists('data', $ad['insights'])){
                        $insights = $ad['insights']['data'];
                    }
                    else{
                        $insights = $ad['insights'];
                    }
                    # Store ad's insights into $ad_data array
                    foreach ($insights as $key_insight => $insight) {
                        foreach ($insight as $key_param => $param){
                            $ad_data[$key_insight][$key_param] = $param;
                        }
                    }
                    # Append to the final data's array
                    $data_ads[$ad_id] = $ad_data;
                }
            }
            # Paging - If there is a next page
            if(array_key_exists('next',$cursor)) {
                $next_url = $cursor['next'];
                $next_content = file_get_contents($next_url); // Send Request to FB
                $decoded_next_content = json_decode($next_content, true);
                $graphNodeArr['ads'] = $decoded_next_content['data']; // Extract the data
                $cursor = $decoded_next_content['paging']; // set cursor to next page
            }
            else{
                $next_url= null;
            }

        }while($next_url); // Loop until there isn't next page

        #-----------------    Get campaigns    ----------------------------------------------------------------
        try {
            // Returns a `FacebookFacebookResponse` object
            $response = $fb->get(
                '/act_'.$ad_account_id.'?fields=name,id,campaigns.limit(500){id,name,status,start_time,created_time.time_range({"since":"'.$start_date.'","until":"'.$end_date.'"}).time_increment(1){'.$fields.'}}',
                $this->access_token
            );

        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            $this->logger->error('Error in FacebookReport.php getData function - facebook->get.');
            throw $e;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            $this->logger->error('Error in FacebookReport.php getData function - facebook->get.');
            throw $e;
        }
        $cursor = $response->getDecodedBody()['campaigns']['paging'];

        $graphNode = $response->getGraphNode();
        $graphNodeArr = $graphNode->asArray();
        $next_url = null;
        do{
            foreach ($graphNodeArr['campaigns'] as $key_campaign => $campaign) {

                # Store campaigns only for later processing
                $this->campaigns[] = array_slice($campaign, 0, 5);
            }

            # Paging - If there is a next page
            if(array_key_exists('next',$cursor)) {
                $next_url = $cursor['next'];
                $next_content = file_get_contents($next_url); // Send Request to FB
                $decoded_next_content = json_decode($next_content, true);
                $graphNodeArr['campaigns'] = $decoded_next_content['data']; // Extract the data
                $cursor = $decoded_next_content['paging']; // set cursor to next page
            }
            else{
                $next_url= null;
            }

        }while($next_url);


        return $data_ads;

    }

    /**
     * Container to soap connection
     */
    private function fetchData($ad_account)
    {
        $ad_account_id = $ad_account['source_account_id'];

        # --- Set fields and Params for Facebook's request ---
        $fields_ads = $this->getFields();
        $fields_ads_implode = implode(",",$fields_ads);
        $params_ads = array('breakdown' => 'publisher_platform',);

        # --- Get the data from Facebook ---
        $data = $this->getData($fields_ads_implode,$params_ads,$ad_account_id);
        $this->updateCampaigns($ad_account);
        $this->data[$ad_account_id] = $data;
        $this->logger->info('Ad Account : ' . $ad_account_id . ' added successfully');
    }

    /**
     * Update Access Token
     */
    public function getAndUpdateAccessToken()
    {
        # Read previous access_token from facebook-token.txt file
        # In case of error ( txt file doesn't exist or the token in the txt file has expired ) try to read token from config
        try{
            $base_url  = 'https://graph.facebook.com/v9.0/oauth/access_token?grant_type=fb_exchange_token';
            $base_url .= '&client_id='         . $this->app_id;
            $base_url .= '&client_secret='     . $this->app_secret;

            // Read token from local file
            $this->access_token = file_get_contents(storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'facebook-token.txt');

            $url = $base_url .  '&fb_exchange_token=' . $this->access_token ;
            $response = file_get_contents(trim($url));

        }catch (\Exception $e){
            // In case of error use config's access token
            $this->access_token = $this->access_token_default;

            $url = $base_url . '&fb_exchange_token=' . $this->access_token ;

            $response = file_get_contents(trim($url));
        }

        $new_access_token = json_decode($response,true)['access_token'];

        # Store the new access token
        $this->access_token = $new_access_token;
        file_put_contents(storage_path().DIRECTORY_SEPARATOR.'app'.DIRECTORY_SEPARATOR.'facebook-token.txt',
            $this->access_token);

    }

    /**
     * Get updateCampaigns
     *
     * @param $ad_account
     *
     */
    public function updateCampaigns($ad_account)
    {
        # Get Account id for the Ad_Account
        $acc = Account::where('source_account_id',$ad_account['source_account_id'])->first();
        if(!$acc){
            throw new \Exception("Account wasn't found when quary Account DB by source_account_id- FacebookReport.php");
        }
        $account_id = $acc->toArray()['id'];
        $source_account_id = $acc->toArray()['source_account_id'];
        foreach ($this->campaigns as $campaign){
            try{
                $account_campaign = AccountCampaign::where('campaign_id', $campaign['id'])
                    ->where('account_id', $account_id)->first();
                $campaign_status = $campaign['status']=="ACTIVE"?"ENABLED":"PAUSED";
                $values = [
                    'account_id'        => $account_id,
                    'campaign_id'       => $campaign['id'],
                    'campaign_name'     => $campaign['name'],
                    'status'            => $campaign_status,
                    'segment'           => "",
                    'source_account_id' => $source_account_id,
                    'start_date'        => $campaign['created_time']
                ];
                # If the campaign was not found, create it
                if (!$account_campaign) {
                    AccountCampaign::create($values);
                }else{
                    $stored_account_campaign = $account_campaign->toArray(); // Campaign as it stored in DB
                    # Check if there is difference between the stored and the new one
                    if($values['account_id'] != $stored_account_campaign['account_id'] or
                        $values['campaign_id'] != $stored_account_campaign['campaign_id'] or
                        $values['campaign_name'] != $stored_account_campaign['campaign_name'] or
                        $values['status'] != $stored_account_campaign['status'] or
                         $values['source_account_id'] != $stored_account_campaign['source_account_id']
                    ){
                        $account_campaign->update($values);
                    }

                }
             } catch (\Exception $e) {
                throw new \Exception("Failed to store campaigns in AccountCampaign table - FacebookReport.php");
            }
        }
        $this->campaigns = [];
    }

    /**
     * Get Logger
     *
     * @return \App\Libraries\TpLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }
    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }
    /**
    * Set options
    *
    * @param $key
    * @param $value
    */
    public function setOptions($key, $value)
    {
        $this->options[$key] = $value;
    }
    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)
            ->where('publisher_id', $this->publisher_id)
            ->where('status', true)
            ->get()
            ->pluck('source_name')
            ->toArray();
    }
    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    protected function queryCsv(Reader $csv)
    {
        $csv->setOffset(2); // Skip the title and header
        $csv->addFilter(function ($row) {
            return strpos($row[0], 'Total') === false;
        });
    }

}
