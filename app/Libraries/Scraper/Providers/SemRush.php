<?php


namespace App\Libraries\Scraper\Providers;


use App\Libraries\Scraper\Provider;
use App\Libraries\TpLogger;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use mysql_xdevapi\Exception;

abstract class SemRush extends Provider

{
    /**
     * @var string
     */
    protected $account_id;
    /**
     * @var string
     */
    protected $encoded_credentials;
    /**
     * @var string
     */
    protected $campaigns;


    protected $GoogleService;



    /**
     * SemRush constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        parent::__construct($logger, $translators, $settings, $options);

        # --- Set Credentials ---
        $this->user_credentials = (new OAuth2TokenBuilder())
            ->fromFile(config_path() . '/adwords-auth.ini')
            ->build();

        // Get the API client and construct the service object.

//        $client = $this->getClient();
//        $this->GoogleService = new \Google_Service_Sheets($client);

    }
    private function getClient()
    {
        $client = new \Google_Client();
        $client->setApplicationName('Google Sheets API PHP Quickstart');
        $client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
//        $client->setAuthConfig(config_path(). '\google_credentials.json');
        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');


        return $client;
    }
}
