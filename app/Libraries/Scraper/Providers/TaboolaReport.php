<?php


namespace App\Libraries\Scraper\Providers;


use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\ReportFieldSource;
use App\Libraries\Scraper\ReportTrait;
use League\Csv\Reader;

abstract class TaboolaReport extends Taboola
{
    use ReportTrait;

    /**
     * @var int
     */
    protected $publisher_id = 162;
    /**
     * @var string
     */
    protected $publisher_name = 'Taboola';


    /**
     * @var int $report_id
     */
    protected $report_id = 37;
    /**
     * @var array $date_ranges
     */
    protected $date_ranges = [
        'CUSTOM_DATE',
        'Today',
        'Yesterday',
        'LastSevenDays',
        'ThisWeek',
        'LastWeek',
        'LastFourWeeks',
        'ThisMonth',
        'LastMonth',
        'LastThreeMonths',
        'LastSixMonths',
        'ThisYear',
        'LastYear'
    ];
    /**
     * @var string $report_type
     */
    protected $report_type;


    /**
     * Fetch
     */
    public function fetch()
    {
        $this->logger->info('Started fetching Taboola. Report Type: ' . $this->report_type);
        $this->get_access_token();
        $this->setReportDateRange();
        $accounts = $this->getAccounts();
        foreach ($accounts as $account){
            $this->account_id = strtolower(str_replace(' ','', $account['name']));
            $this->data[]  = $this->fetchData();
        }

    }

    protected function getAccounts()
    {
        $ad_accounts = Account::where('status','active')->whereHas('publisher', function ($query) {
            $query->whereIn('publishers.id', [$this->publisher_id]);
        })->with('fields')->get();

        return $ad_accounts->toArray();
    }
    /**
     * Get Auth Token
     */
    protected function get_access_token()
    {

        $base_url = 'https://backstage.taboola.com/backstage/oauth/token';
        $url = $base_url . '?client_id='     . $this->client_id
                         . '&client_secret=' . $this->client_secret
                         . '&grant_type='    . $this->grant_type;

        $opts = array('http'=>
                    array(
                        'method' => 'POST',
                        'header' => 'Content-Type: application/x-www-form-urlencoded'
                    ));
        $context  = stream_context_create($opts);
        $response = file_get_contents($url, false, $context);

        $this->access_token = json_decode($response,true)['access_token'];
//        $this->access_token = 'CZMRAAAAAAAAEczOAgAAAAAAGAEgACnwFtVvcgEAADooMGIwOTA0MjU5ZTA0NGVlODhmYzBlYzdiN2ViNTdiMmI3Yzc3MDc1M0AC::316695::51dab1';
        $this->token_type = json_decode($response,true)['token_type'];
//        $this->token_type = 'bearer';
     }

    private function getCampaigns()
    {
        $url = 'https://backstage.taboola.com/backstage/api/1.0/'.$this->account_id.'/campaigns';
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Authorization:". 'Bearer ' . $this->access_token
            )
        );
        $this->logger->info('Before Data response: ' . $url); //for check
        $data = file_get_contents($url,false, stream_context_create($opts));
        $this->logger->info('Data response: ' . $data); //for check
        $campaigns = array_filter(json_decode($data,true)['results'], function($row) {
            return $row['is_active'];
        });
        $this->campaigns = $campaigns;
    }

    /**
     * Fetch data
     */
    private function fetchData()
    {
//        $report_name = 'campaign-summary';
        $report_name = 'top-campaign-content';
//        $dimension_id = 'day';
        $dimension_id = 'item_breakdown';
        $parameters = '';
        $final_results = [];

        $start_date = $this->getOptions()['date-range-start'];
        $end_date = $this->getOptions()['date-range-end'];

//        $parameters .= 'start_date=' . $start_date . '&end_date=' . $end_date;

        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Authorization:". 'Bearer ' . $this->access_token
            )
        );

        $this->logger->info('before $this->getCampaigns()'); //for check
        $campaigns  = $this->getCampaigns();
        $this->logger->info('after $this->getCampaigns()'); //for check

        foreach($this->campaigns as $campaign){

            $begin = new \DateTime($start_date);
            $end = new \DateTime($end_date);

            $interval = \DateInterval::createFromDateString('1 day');
            $period = new \DatePeriod($begin, $interval, $end);

            foreach ($period as $dt) {
                $date = $dt->format("Y-m-d");
                $parameters = 'start_date=' . $date . '&end_date=' . $date;

                $url = 'https://backstage.taboola.com/backstage/api/1.0/';
                $url .= $this->account_id . '/reports/' .$report_name. '/dimensions/' . $dimension_id . '?'  .$parameters ;
                $url .= '&campaign_id=' . $campaign['id'];

                $data_as_json = file_get_contents($url,false, stream_context_create($opts));
                $data = json_decode($data_as_json,true);
                $res = $data['results'];
                foreach ($res as &$row){
//                $row['campaign_id'] = $campaign['campaign'];
//                $row['campaign_name'] = $campaign['campaign_name'];
//                $row['account_id'] = $campaign['content_provider'];
                    $row['account_name'] = $this->account_id;
                    $row['device'] = 'c';
                    $row['advertiser_net_revenue'] = $row['spent'] * (-1);
                    $row['date'] = $date;
                }

                $final_results[$campaign['id'].'_'.$dt->format("Y_m_d")] = $res;
            }



        }

        $this->logger->info('Fetching data is finished. '  );

        return $final_results;
    }

    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)
            ->where('publisher_id', $this->publisher_id)
            ->where('status', true)
            ->get()
            ->pluck('source_name')
            ->toArray();
    }

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @param $key
     * @param $value
     */
    public function setOptions($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * Get Logger
     *
     * @return \App\Libraries\TpLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    protected function queryCsv(Reader $csv)
    {
        $csv->setOffset(2); // Skip the title and header
        $csv->addFilter(function ($row) {
            return strpos($row[0], 'Total') === false;
        });
    }
}
