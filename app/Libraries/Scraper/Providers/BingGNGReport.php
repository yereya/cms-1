<?php namespace App\Libraries\Scraper\Providers;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\ReportFieldSource;
use App\Libraries\Scraper\ReportTrait;

use League\Csv\Reader;
use Storage;
use ZipArchive;

use Microsoft\BingAds\V13\Reporting\Date;
use Microsoft\BingAds\V13\Reporting\ReportTime;
use Microsoft\BingAds\V13\Reporting\AccountThroughAdGroupReportScope;
use Microsoft\BingAds\V13\Reporting\ReportRequest;
use Microsoft\BingAds\V13\Reporting\ReportRequestStatusType;
use Microsoft\BingAds\V13\Reporting\SubmitGenerateReportRequest;
use Microsoft\BingAds\V13\Reporting\SubmitGenerateReportResponse;
use Microsoft\BingAds\V13\Reporting\PollGenerateReportRequest;

/**
 * Class BingReport
 *
 * @package App\Libraries\Scraper\Providers
 */
abstract class BingGNGReport extends BingGNG
{
    use ReportTrait;
    /**
     * Wsdl Reports
     */
//    const WSDL_REPORTS = 'https://api.bingads.microsoft.com/Api/Advertiser/Reporting/V9/ReportingService.svc?singleWsdl';
    const WSDL_REPORTS = 'https://api.bingads.microsoft.com/Api/Advertiser/Reporting/V13/ReportingService.svc?singleWsdl';

    /**
     * @var array $accounts
     */
    protected $accounts;

    /**
     * @var int $report_id
     */
    protected $report_id;

    /**
     * @var int $publisher_id
     */
    protected $publisher_id = 164;

    /**
     * @var string $publisher_name
     */
    protected $publisher_name = 'BingGNG';

    /**
     * @var array $date_ranges
     */
    protected $date_ranges = [
        'CUSTOM_DATE',
        'Today',
        'Yesterday',
        'LastSevenDays',
        'ThisWeek',
        'LastWeek',
        'LastFourWeeks',
        'ThisMonth',
        'LastMonth',
        'LastThreeMonths',
        'LastSixMonths',
        'ThisYear',
        'LastYear'
    ];

    /**
     * @var string $report_type
     */
    protected $report_type;

    /**
     * @var string $report_request
     */
    protected $report_request;

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->getAccountsByAdvertisers($this->options['account_id'] ?? null);

        if (!$this->accounts) {
            return;
        }

        $this->logger->info('Started fetching BingGNG. Report request: ' . $this->report_request);

        $this->setReportDateRange();

        $this->fetchData();
    }

    /**
     * Get Accounts By Advertisers
     *
     * @param $account_id
     */
    private function getAccountsByAdvertisers($account_id)
    {
        if (is_null($account_id)) {
            $this->accounts = Account::where('status','active')
                ->whereHas('advertiser', function ($query) {
                $query->where('advertisers.status', 'active');
            })->whereHas('publisher', function ($query) {
                $query->where('publishers.id', $this->publisher_id);
            })->whereHas('reports.report', function ($query) {
                $query->where('reports.id', $this->report_id);
            })->get()->pluck('source_account_id')->filter()->toArray();
        } else {
            $this->accounts = Account::where('id', $account_id)->pluck('source_account_id')->filter()->toArray();
        }

        $this->logger->debug(["Accounts by advertiser:", $this->accounts]);
    }

    /**
     * Container to soap connection
     */
    private function fetchData()
    {
        //$this->setProxy(self::WSDL_REPORTS);
        try {
            $request    = $this->getPollRequest($this->getSubmitRequest());
            $this->data = $this->getReportFromUrl($this->getReportDownloadUrl($request));
        } catch (\Exception $e) {
            $this->logger->warning(['SOAP fault', json_encode($e)]);
        }
    }

    /**
     * Get Poll Request
     *
     * @param SubmitGenerateReportRequest $submit_request
     *
     * @return PollGenerateReportRequest
     */
    private function getPollRequest(SubmitGenerateReportRequest $submit_request)
    {
        $request                  = new PollGenerateReportRequest();
        $request->ReportRequestId = $this->getReportRequestId($submit_request)->ReportRequestId;

        return $request;
    }

    /**
     * Get Report Request ID
     *
     * @param SubmitGenerateReportRequest $request
     *
     * @return SubmitGenerateReportResponse
     * @throws \SoapFault
     */
    private function getReportRequestId(SubmitGenerateReportRequest $request)
    {
        try {
            return $this->bing_client->reportingService()->GetService()->SubmitGenerateReport($request);
        } catch (\SoapFault $e) {
            $this->logger->debug(["Soap Fault exception for report request ID", $e->getMessage(), $e->detail]);

            throw $e;
        }
    }

    /**
     * Get Submit Request
     *
     * @return SubmitGenerateReportRequest
     */
    private function getSubmitRequest()
    {
        $report_request         = $this->getReportRequest();
        $request                = new SubmitGenerateReportRequest();
        $request->ReportRequest = new \SoapVar($report_request, SOAP_ENC_OBJECT, (new \ReflectionClass($report_request))->getShortName(), $this->bing_client->reportingService()->GetNamespace());

        return $request;
    }

    /**
     * Get Report Request
     *
     * @return ReportRequest
     */
    protected function getReportRequest()
    {
        $request = new $this->report_request;
        $this->getReportRequestSettings($request);
        $this->getReportRequestScope($request);
        $this->getReportRequestTime($request);
        $this->getReportRequestColumns($request);

        return $request;
    }

    /**
     * Get Report Request Settings
     *
     * @param ReportRequest $request
     */
    abstract protected function getReportRequestSettings(ReportRequest $request);

    /**
     * Get Report Request Scope
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestScope(ReportRequest $request)
    {
        $request->Scope             = new AccountThroughAdGroupReportScope();
        $request->Scope->AccountIds = $this->accounts;
    }

    /**
     * Get Report Request Time
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestTime(ReportRequest $request)
    {
        $request->Time = new ReportTime();

        if ($this->date_range_type == 'CUSTOM_DATE') {
            $request->Time->CustomDateRangeStart = $this->getReportRequestTimeStartDate();
            $request->Time->CustomDateRangeEnd   = $this->getReportRequestTimeEndDate();
        } else {
            $request->Time->PredefinedTime = $this->date_range_type;
        }
    }

    /**
     * Get Report Request Time Start Date
     *
     * @return Date
     */
    private function getReportRequestTimeStartDate()
    {
        $this->logger->info(['Report request time start', $this->date_range_start]);

        $date        = new Date;
        $date->Day   = $this->date_range_start->day;
        $date->Month = $this->date_range_start->month;
        $date->Year  = $this->date_range_start->year;

        return $date;
    }

    /**
     * Get Report Request Time End Date
     *
     * @return Date
     */
    private function getReportRequestTimeEndDate()
    {
        $this->logger->info(['Report request time end', $this->date_range_end]);

        $date        = new Date;
        $date->Day   = $this->date_range_end->day;
        $date->Month = $this->date_range_end->month;
        $date->Year  = $this->date_range_end->year;

        return $date;
    }

    /**
     * Get Report Request Columns
     *
     * @param ReportRequest $request
     */
    protected function getReportRequestColumns(ReportRequest $request)
    {
        $flattened_fields       = array_unique(array_flatten($this->getFields()));
        $fields_with_reset_keys = array_values($flattened_fields);
        $request->Columns       = $fields_with_reset_keys;
    }

    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)->where('publisher_id', $this->publisher_id)
                                ->where('status', true)->get()->pluck('source_name')->filter()->toArray();
    }

    /**
     * Get Report From Url
     *
     * @param string $url
     *
     * @return mixed
     */
    private function getReportFromUrl($url)
    {
        if($url){
            $this->downloadReport($url);
            $this->unzipDownloadedReport();

            return $this->getReportsFromUnzippedFolder();
        }
        return null;
    }

    /**
     * Download Report
     *
     * @param $url
     *
     * @return string
     */
    private function downloadReport($url)
    {
        Storage::put($this->getReportFilePath(date('Y-m-d'), 'zip', false), file_get_contents($url));

        $this->logger->info('Download finished');
    }

    /**
     * Unzip Downloaded Report
     **
     *
     * @return string
     */
    private function unzipDownloadedReport()
    {
        $zip = new ZipArchive();
        $zip->open($this->getReportFilePath(date('Y-m-d'), 'zip'));
        $zip->extractTo($this->getReportFolderPath());
        $zip->close();

        $this->logger->info('Unzipped report');
    }

    /**
     * Get Reports From Unzipped Folder
     *
     * @return array
     */
    private function getReportsFromUnzippedFolder()
    {
        // TODO is this the best way to do this ?
        // it seems like if we didn't delete the files in the folder for some reason
        // we will reopen and load up the same files

        $reports = [];
        foreach (glob($this->getReportFolderPath() . DIRECTORY_SEPARATOR . '*.csv') as $path) {
            $reports[] = $this->parseCsv($path);
        }

        $this->logger->info('Report fetched, loaded and translated');

        return $reports;
    }

    /**
     * Get Report Download URL
     *
     * @param PollGenerateReportRequest $request
     * @param integer $failed_count
     *
     * @return mixed
     */
    private function getReportDownloadUrl(PollGenerateReportRequest $request,$failed_count = 0)
    {
        try{
            $report_request_status = $this->getReportRequestStatus($request);

            while ($report_request_status->Status == ReportRequestStatusType::Pending) {
                $this->logger->notice('Report not ready yet to download. waiting 3 sec');

                sleep(3);
                $report_request_status = $this->getReportRequestStatus($request);
            }

            $this->logger->info('Report download url generated.');

            return $report_request_status->ReportDownloadUrl;
        }catch(\Exception $ex){
            $this->logger->error(['getReportRequestStatus Exception, failed count is ' . $failed_count,$ex->getMessage()]);

            if ($failed_count > 9){
                return $this->logger->error(['Failed more then 10 times.']);
            }

            return $this->getReportDownloadUrl($request,$failed_count+1);
        }
    }

    /**
     * Get Report Request Status
     *
     * @param PollGenerateReportRequest $request
     *
     * @return mixed
     */
    private function getReportRequestStatus(PollGenerateReportRequest $request)
    {
        return $this->bing_client
                    ->reportingService()
                    ->GetService()
                    ->PollGenerateReport($request)
                    ->ReportRequestStatus;
    }

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @param $key
     * @param $value
     */
    public function setOptions($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * Get Logger
     *
     * @return \App\Libraries\TpLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    protected function queryCsv(Reader $csv)
    {
        $csv->setOffset(10); // Skip the headers
        $csv->addFilter(function ($row) {
            return strpos($row[0], 'Microsoft') === false;
        });
    }
}
