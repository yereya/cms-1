<?php namespace App\Libraries\Scraper\Providers;

use Cache;
use GuzzleHttp\Client;
use App\Libraries\TpLogger;
use BingAds\Proxy\ClientProxy;
use App\Libraries\Scraper\Provider;
use App\Libraries\Bing\Client as BingClient;

/**
 * Class Bing
 *
 * @package App\Libraries\Scraper\Providers
 */
abstract class BingGNG extends Provider
{
    /**
     * Wsdl Customer
     */
//    const WSDL_CUSTOMER = 'https://clientcenter.api.bingads.microsoft.com/Api/CustomerManagement/v9/CustomerManagementService.svc?singleWsdl';
    const WSDL_CUSTOMER = 'https://clientcenter.api.bingads.microsoft.com/Api/CustomerManagement/v13/CustomerManagementService.svc?singleWsdl';

    /**
     * @var string
     */
    protected $refresh_token;

    /**
     * @var string
     */
    protected $access_token;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ClientProxy
     */
    protected $proxy;

    /**
     * @ver BingClient
     */
    protected $bing_client;

    /**
     * Bing constructor.
     *
     * @param TpLogger $logger
     * @param array    $translators
     * @param array    $settings
     * @param array    $options
     */
    public function __construct($logger, $translators, $settings, $options)
    {
        parent::__construct($logger, $translators, $settings, $options);
        $this->logger->info('Started provider: BingGNG');
        if (isset($this->options['date-range-start'])) {
            $this->logger->info('Range: [' . $this->options['date-range-start'] . ' - ' . $this->options['date-range-end'] . ']');
        }

        $this->bing_client = new BingClient(
            config('bing-gng-ads.client_id'),
            config('bing-gng-ads.client_secret'),
            config('bing-gng-ads.developer_token'),
            config('bing-gng-ads.refresh_token_storage_path'));

        //$this->setRefreshToken();
        //$this->setAccessToken();
        //$this->setProxy(self::WSDL_CUSTOMER);
    }

    /**
     * Set Refresh Token
     */
    protected function setRefreshToken()
    {
        $this->refresh_token = config('bing-gng-ads.refresh_token');
    }

    /**
     * Set Access Token
     */
    protected function setAccessToken()
    {
        $request  = $this->client->get($this->getRefreshTokenUrl());
        $response = json_decode($request->getBody()->getContents(), true);

        $this->access_token  = $response['access_token'];
        $this->refresh_token = $response['refresh_token'];

        Cache::forever('bing-gng-ads.refresh_token', $this->refresh_token);
    }

    /**
     * Get Refresh Token URL
     *
     * @return string
     */
    private function getRefreshTokenUrl()
    {
        $url = 'https://login.live.com/oauth20_token.srf?';
        $url .= 'client_id=' . config('bing-gng-ads.client_id');
        $url .= '&refresh_token=' . $this->refresh_token;
        $url .= '&grant_type=refresh_token';
        $url .= '&redirect_uri=https%3A%2F%2Flogin.live.com%2Foauth20_desktop.srf';

        return $url;
    }

    /**
     * Set Proxy
     *
     * @param string $wsdl
     *
     * @return ClientProxy
     */
    protected function setProxy($wsdl)
    {
        //$this->proxy = ClientProxy::ConstructWithCredentials($wsdl, null, null, config('bing-gng-ads.developer_token'), $this->access_token);
    }

    /**
     * To refresh token when get client error expired date
     *
     * 1) run --https://login.live.com/oauth20_authorize.srf?client_id=000000004C178591&scope=bingads.manage&response_type=code&redirect_uri=https://login.live.com/oauth20_desktop.srf&state=ClientStateGoesHere
     * 2) run this method with response code (world 'code' in url query)
     *      to example - Ma1fad57a-465c-7c65-0e66-727bf0100646
     * 3) run this method in __construct in terminal after
     *      $this->client = new Client, comment setRefreshToken and setAccessToken and , you get url
     * 4) link to this url - you get new access token and refresh token
     * 5) rollback __construct method
     * 6) change access_token and refresh_token in setAccessToken
     * 7) change refresh_token in config bing-ads.php
     * 8) rollback all and run all scrapers
     *
     * @return string
     */
    private function getExpired()
    {
        $url = 'https://login.live.com/oauth20_token.srf?';
        $url .= 'client_id=' . config('bing-gng-ads.client_id');
        $url .= '&code=' . 'Change This to new code'; // change code
        $url .= '&grant_type=authorization_code';
        $url .= '&redirect_uri=https%3A%2F%2Flogin.live.com%2Foauth20_desktop.srf';

        return $url;
    }
}
