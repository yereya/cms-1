<?php



namespace App\Libraries\Scraper\Providers;


use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountCampaign;
use App\Entities\Models\Bo\ReportFieldSource;
use App\Libraries\Scraper\ReportTrait;
use League\Csv\Reader;


abstract class OutbrainReport extends Outbrain
{
    use ReportTrait;

    /**
     * @var int
     */
    protected $publisher_id = 161;
    /**
     * @var string
     */
    protected $publisher_name = 'Outbrain';

    /**
     * @var int $report_id
     */
    protected $report_id = 36;
    /**
     * @var array $date_ranges
     */
    protected $date_ranges = [
        'CUSTOM_DATE',
        'Today',
        'Yesterday',
        'LastSevenDays',
        'ThisWeek',
        'LastWeek',
        'LastFourWeeks',
        'ThisMonth',
        'LastMonth',
        'LastThreeMonths',
        'LastSixMonths',
        'ThisYear',
        'LastYear'
    ];
    /**
     * @var string $report_type
     */
    protected $report_type;

//    protected $include_columns = '$invoca_custom_columns,$invoca_default_columns,$invoca_custom_source_columns';

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->logger->info('Started fetching Outbrain. Report Type: ' . $this->report_type);
        $this->get_auth_token();
        $this->setReportDateRange();
        $this->data[]  = $this->fetchData();

    }
    /**
     * Get Auth Token
     */
    protected function get_auth_token()
    {
        $url = 'https://api.outbrain.com/amplify/v0.1/login';
        // Create a stream
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"Authorization: Basic ". $this->encoded_credentials
            )
        );

        $context = stream_context_create($opts);

        // Open the file using the HTTP headers set above
         $auth_token = file_get_contents($url, false, $context); // Get Token from server
         $this->ob_token_v1 = json_decode($auth_token,true)['OB-TOKEN-V1']; // Extract Token from json

        # New Token
//        $this->ob_token_v1 ='MTU5MTE3NDYyOTg2MzowMTQ2YmI2YTEwMGU3MzBmMGY0NWEyMGE0MTgyMGEyY2E5NGJlMDQ4NjNiMTk3YTJiZmJjYzRjNGY3YmM5MmMxNWY5ZTUyODlmYTkyMmY1ZjhhMDEzMzk3MjVhYjg0OTg1MTg4MzYxZDVlNTJhMjllYjZkZTJkMjhhNjM4NWY0ZWIwMGM5ZDYxZTY3Zjc1MTRhOGVhZGU1MGYzNjVjNWUwNWYyNGQ5MGQ3ZWJmZDAzNzZjMGUyMmEzMzg5ZGU0ZGQ0YjMwMzU0MWY4NWI0NDA2OTU3OTliNWQwM2VmOThiZTJmODMwMzJmZDdkYzlkZDA1MjZkMGFjMjJlZjhkOGJiNmMxMjkzOTljNTY0MjFjZTM0MDNkMTZhNTZkY2MzN2M3YzVmMWFiODAwYzE3YjU2OGViMGFmZTMwZWI4Mjg1ZjI1NDljY2IzNzk3ZThkNTY2ZmYyZWUxMGI0MDRiZWE4YjFmM2RkNGNhNzEyMmE5ZTk3YjkyMmJiNDdkMGYyMGIxZWJhOWZlNzdkODU5OGJhYjQ5YjM5N2Y5MTYwMGUzYjI5ZjIzOTFjN2M1MjgzN2UzNmNkNjc2ZDQyMWRiYTg1YmRkMzNhMDczZGZiOTJlNjk5ZThiODllMTcxNTBlMmU5Y2VlNzY0M2Q5YWI5OTE5Y2Q3MWZlZmU1ZjA4M2E4ZTp7ImNhbGxlckFwcGxpY2F0aW9uIjoiQW1lbGlhIiwiaXBBZGRyZXNzIjoiMzEuMTU0LjU2LjIwMiIsImJ5cGFzc0FwaUF1dGgiOiJmYWxzZSIsInVzZXJOYW1lIjoianVsaWV0dGVAdHJhZmZpY3BvaW50LmlvIiwidXNlcklkIjoiMTA2NDMzMzEiLCJkYXRhU291cmNlVHlwZSI6Ik1ZX09CX0NPTSJ9OmJmZDZmN2YwMzBiMGQ4OWY4N2Q5ZDE5MGNhYTgyYTI4OTc0NmRmMDc3N2U1Zjc4NjdhNGE3NWU1MmYxMzkwYTljN2JiYWU0NDcwNjdhYzZlNjFhNDI2NzBjM2E5ZmUxMzdkODRiOTQ2NWI3MWRlOTY0ZDY0MTdjNWY0Y2ZmNmQ2';
//        $this->ob_token_v1 ='MTU4OTc5MTg4ODQ0OTo1NmNmNzZiMGM3NjMyZjVjMGU1NzUxNjBkODBiODFkZGVlYjgzOWRjNmQwMjFkYzcwZjcyMTE1MzYwZGEzY2E2M2VhOTA0MGVkZjQzMTYzYmY0MGE4ZDFlNzlkNWYzY2NmNzE5MGY1YTNiYmY3ZmQ4OTE5ZGFmY2RiZmY5M2ZjZTg1MWY1ZTBmOWY1ZTgwMDUzN2ZmOTU2NzE0NzA1MTJkYWEzNDg1YThhMjZiMWE2YWEwYjAwOTdmZTE4YTIwMjM0ZWE1ZDJmNzYzYjg0ZjhjZmVjNzQ0ZDMwMDRjMTQ3MjY4OTVjM2JhYzBjMTUxZGVmZWQwOTg5NjFjNTE3NzE2NjU4YjgxMmQxMjU2YzE3MGVmOThhZmY2ZjVmNjhhNjQ1ODQ5ZjFhYTcxNTRiMDNiYWU3MTU1MTgxOGZiYTkwNjhjMWZlZWE0OTA1NjRmYTVmNzU0YzI3NjVmNWIxZmI5ZTUwM2RhNTM3NDAwN2FiZTg4OTVjYmE1YWM2Y2VkNzExZTc1ZThhMjQ0OGNhNWEzZDk5MzFmMTQ4ZjRjY2VkMjdhZTk3NzE3MWRlZmY4ODk5MTdkODI1ZGYyNTY3ZWQ1ZDNhZTYwYmQyOGZlYzAzN2U3OGQ3NDc4MjcwZWUxOTRhNDgyZWEyZThjYzIyN2Y4Nzc1NGUyZmU2MGFmMTU4Mjp7ImNhbGxlckFwcGxpY2F0aW9uIjoiQW1lbGlhIiwiaXBBZGRyZXNzIjoiMzEuMTU0LjU2LjIwMiIsImJ5cGFzc0FwaUF1dGgiOiJmYWxzZSIsInVzZXJOYW1lIjoianVsaWV0dGVAdHJhZmZpY3BvaW50LmlvIiwidXNlcklkIjoiMTA2NDMzMzEiLCJkYXRhU291cmNlVHlwZSI6Ik1ZX09CX0NPTSJ9OjA0YjA5YzBjYmQ5ZDU4NTQ1OGYxOGU2NDk1YjdkNmNhNmQwNTU3MTI1NjQ2MmM2MGE3NjU2Mjc4ODA0MTRmMDVmOTkxODU3NzU0MzgxYWUxNTdhYjg4MjYzMTUzNDhmNWMzMjE0YTA5MmM4Yzc4MDZkYTk5ODIwNTQyYzUzZjFl';
    }

    /**
     * Fetch data
     */
    private function fetchData()
    {
        $this->campaigns = $this->getCampaigns(); // Get list of all the existing campaigns
        $opts = array(
            'http' => array(
                'method' => "GET",
                'header' => "OB-TOKEN-V1:" . $this->ob_token_v1
            )
        );
        $url = $this->buildUrl();

        # --- Loop over dates ---
        $begin = new \DateTime($this->getOptions()['date-range-start']);
        $end = new \DateTime($this->getOptions()['date-range-end']);
        $end->modify('1 day'); // Add 1 day to the end date in order to include the last day in the foreach loop

        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);
        $parsed_data = [];

        foreach ($period as $dt) {
            $date = $dt->format("Y-m-d");
            $url_with_date = $url . '&from=' . $date;
            $url_with_date = $url_with_date . '&to=' . $date;

            $data_as_json = file_get_contents($url_with_date, false, stream_context_create($opts));
            $data = json_decode($data_as_json, true);

            # --- Loop over campaigns ---
            foreach ($data['results'] as $results) {
                $row = array_merge($results['metadata'], $results['metrics']);
                $row['date'] = $date;
                $row['source_report_uid'] = crc32($row['id']);
                $row['account_id'] = $this->account_id;
                $row['account_id_int'] = crc32($this->account_id);
                $row['campaign_id_int'] = crc32($row['campaignId']);
                if($row['impressions'] > 0){
                    $parsed_data[$date][] = $row;
                }
            }
        }
        # Todo validate sizeof $data - decide which size to calculate
        $this->logger->info('Fetching data is finished, returned : ' . sizeof($data) . ' results.' );

        return $parsed_data;
    }
    /**
     * Get list of campaigns
     *
     * @return array
     */
    private function getCampaigns(){
    $base_url = 'https://api.outbrain.com/amplify/v0.1/marketers/';
    $url = $base_url . $this->account_id . '/campaigns';
    $opts = array(
        'http'=>array(
            'method'=>"GET",
            'header'=>"OB-TOKEN-V1:". $this->ob_token_v1
        )
    );
    $data_as_json = file_get_contents($url,false, stream_context_create($opts));
    $data = json_decode($data_as_json,true);
    $campaigns = [];
    foreach($data['campaigns'] as $campaign){
        $row = [];
        $row['id'] = $campaign['id'];
        $row['name'] = $campaign['name'];
        $row['enabled'] = $campaign['enabled'];
        $row['cpc'] = $campaign['cpc'];
        $row['currency'] = $campaign['currency'];
        $campaigns[$campaign['id']] = $row;
    }
    return $campaigns;
    }
    /**
     * Build URL
     *
     * @return string
     */
    public function buildUrl()
    {
    $base_url =  "https://api.outbrain.com/amplify/v0.1/";
    $breakdown = 'daily';
    $limit = '500'; // Max limit is 500
    $includeArchivedCampaigns = 'true';
    $includeConversionDetails = 'true';
    $conversionsByClickDate = 'true';
    # -- Example --
        // https://api.outbrain.com/amplify/v0.1/reports/marketers/00049c19e8bc8b0ac2023b413c0cb7a3cf/content?from=2020-05-18&to=2020-05-20&limit=10&offset=3&includeArchivedCampaigns=true&campaignId=00de1958f08a6880b8afc60fc8707e88e0&includeConversionDetails=true&conversionsByClickDate=true

        # --- New Report ---
        $url = $base_url . 'reports/marketers/' . $this->account_id . '/content'
            .'?limit=' . $limit
            .'&conversionsByClickDate=' . $conversionsByClickDate
            .'&includeArchivedCampaigns=' . $includeArchivedCampaigns
            .'&includeConversionDetails=' . $includeConversionDetails;

        $this->logger->info('Generated URL : ' . $url );
        return $url;

    }

    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)
            ->where('publisher_id', $this->publisher_id)
            ->where('status', true)
            ->get()
            ->pluck('source_name')
            ->toArray();
    }
    /**

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @param $key
     * @param $value
     */
    public function setOptions($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * Get Logger
     *
     * @return \App\Libraries\TpLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    protected function queryCsv(Reader $csv)
    {
        $csv->setOffset(2); // Skip the title and header
        $csv->addFilter(function ($row) {
            return strpos($row[0], 'Total') === false;
        });
    }
}

