<?php


namespace App\Libraries\Scraper\Providers;


use App\Entities\Models\Bo\ReportFieldSource;
use App\Libraries\Scraper\ReportTrait;
use Illuminate\Support\Facades\DB;
use League\Csv\Reader;

abstract class InvocaReport extends Invoca
{
    use ReportTrait;

    /**
     * @var int
     */
    protected $publisher_id = 159;
    /**
     * @var string
     */
    protected $publisher_name = 'Invoca';

    /**
     * @var array $ad_accounts
     */
    protected $ad_accounts;

    /**
     * @var array $campaigns
     */
    protected $campaigns;

    /**
     * @var int $report_id
     */
    protected $report_id = 28;
    /**
     * @var array $date_ranges
     */
    protected $date_ranges = [
        'CUSTOM_DATE',
        'Today',
        'Yesterday',
        'LastSevenDays',
        'ThisWeek',
        'LastWeek',
        'LastFourWeeks',
        'ThisMonth',
        'LastMonth',
        'LastThreeMonths',
        'LastSixMonths',
        'ThisYear',
        'LastYear'
    ];
    /**
     * @var string $report_type
     */
    protected $report_type;

    protected $include_columns = '$invoca_custom_columns,$invoca_default_columns,$invoca_custom_source_columns';

    /**
     * Fetch
     */
    public function fetch()
    {
        $this->logger->info('Started fetching Invoca. Report Type: ' . $this->report_type);

        $this->setReportDateRange();
        $this->data[]  = $this->fetchData();

    }

    /**
     * Container to soap connection
     */
    private function fetchData()
    {

        $url = $this->buildUrl();
        $data_as_json = file_get_contents($url);
        $data = json_decode($data_as_json,true);

        $this->logger->info('Fetching data is finished, returned : ' . sizeof($data) . ' results.' );
        return $data;
    }
    /**
     * Build URL
     *
     * @return string
     */
     public function buildUrl()
     {

         # -- Example --
         //mynetwork.invoca.net/api/2019-05-01/affiliates/transactions/33.csv?limit=20&start_after_transaction_id=C624DA2C-CF3367C3&oauth_token=YbcFH';

         $id_by_type = '';
         switch ($this->invoca_account_type){
             case 'advertisers':
                 $id_by_type = $this->invoca_advertiser_id_temp;
                 break;
             case 'networks':
                 $id_by_type = $this->invoca_network_id;
                 break;
             case 'publishers':
                 $id_by_type = $this->invoca_publisher_id;
                 break;
         }

         $url = "https://" . $this->account_name . ".invoca.net/api/" . $this->invoca_api_version . "/" . $this->invoca_account_type . "/transactions/" .
             $id_by_type . ".json?" . "oauth_token=" . $this->invoca_outh_token . '&limit=4000'; 
         # Set start and end date
         $start_date = $this->getOptions()['date-range-start'];
         $end_date = $this->getOptions()['date-range-end'];
         if(isset($start_date)){
             $url = $url . '&from=' . $start_date;
         }
         if(isset($end_date)){
             $url = $url . '&to=' . $end_date;
         }
         # Set start_after_transaction_id
         $high_water_mark_obj = DB::connection($this->water_mark_connection)
                                ->table($this->water_mark_table_name)
                                ->where('table_name','=','mrr_fact_calls')
                                ->get()
                                ->toArray();

         if($high_water_mark_obj){
             $high_water_mark_arr = json_decode(json_encode($high_water_mark_obj[0]), True);
             if($high_water_mark_arr['high_water_mark_id'] != null){
                 $url .= '&start_after_transaction_id=' . $high_water_mark_arr['high_water_mark_id'];
                 $this->logger->info('Building URL, Start After transaction ID : ' . $high_water_mark_arr['high_water_mark_id'] .
                  ' , date : ' .$high_water_mark_arr['high_water_mark']);
             }
         }
         # Set include columns for custom data
         $url .= '&include_columns=' . $this->include_columns;

         $this->logger->info('Generated URL : ' . $url );
         return $url;
     }

    /**
     * Get Fields
     *
     * @return array
     */
    public function getFields()
    {
        return ReportFieldSource::where('report_id', $this->report_id)
            ->where('publisher_id', $this->publisher_id)
            ->where('status', true)
            ->get()
            ->pluck('source_name')
            ->toArray();
    }
    /**

    /**
     * Get Options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @param $key
     * @param $value
     */
    public function setOptions($key, $value)
    {
        $this->options[$key] = $value;
    }

    /**
     * Get Logger
     *
     * @return \App\Libraries\TpLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    protected function queryCsv(Reader $csv)
    {
        $csv->setOffset(2); // Skip the title and header
        $csv->addFilter(function ($row) {
            return strpos($row[0], 'Total') === false;
        });
    }

}