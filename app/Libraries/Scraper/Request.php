<?php namespace App\Libraries\Scraper;

use App\Entities\Models\System;
use App\Libraries\Scraper\Contracts\Request as RequestContract;
use App\Libraries\Scraper\Exceptions\ProviderNotExistsException;
use App\Libraries\TpLogger;

/**
 * Class Request
 *
 * @package App\Libraries\Scraper\Requests
 */
abstract class Request implements RequestContract
{
    /**
     * @var int $logger_system_id
     */
    protected $logger_system_id;

    /**
     * @var TpLogger $logger
     */
    protected $logger;

    /**
     * @var array|null $settings
     */
    protected $settings;

    /**
     * @var array|null $options
     */
    protected $options;

    /**
     * @var array $providers
     */
    protected $providers = [];

    /**
     * @var array $instances
     */
    protected $instances = [];

    /**
     * Request constructor.
     *
     * @param array $settings
     * @param array $options
     */
    public function __construct($settings = null, $options = null)
    {
        $this->settings = $settings;
        $this->options  = $options;
        $this->initLogs();
        $this->initProvider($this->options['provider']);
    }

    /**
     * Init Logs
     *
     * @throws \Exception
     */
    private function initLogs()
    {
        if (!$this->logger_system_id) {
            throw new \Exception('missing logger system id for request ' . get_class($this));
        }

        $this->logger = new TpLogger($this->logger_system_id);
        $system = System::find($this->logger_system_id);
        if ($system->parent_id == 1) { // only for scrapers
            $provider = $this->options['provider'] ?? 'all providers';
            $this->logger->info('Start run ' . $system->name . '| providers: ' . $provider);
        }
    }

    /**
     * Init Provider
     *
     * @param $provider
     *
     * @throws ProviderNotExistsException
     */
    private function initProvider($provider)
    {
        if ($provider) {
            $this->setProvider($provider);
        } else {
            $this->setAllProviders();
        }
    }

    /**
     * Set Provider
     *
     * @param string $name
     *
     * @throws ProviderNotExistsException
     */
    protected function setProvider($name)
    {
        if (isset($this->providers[$name])) {
            $provider_class         = $this->providers[$name]['class'];
            $translators            = isset($this->providers[$name]['translators']) ? $this->providers[$name]['translators'] : null;
            $this->instances[$name] = new $provider_class($this->logger, $translators, $this->getProviderSettings($name), $this->options);
        } else {
            $this->logger->error('provider ' . $name . ' not exists');
            throw new ProviderNotExistsException;
        }
    }

    /**
     * @param $name
     *
     * @return null
     */
    private function getProviderSettings($name)
    {
        return isset($this->settings[$name]) ? $this->settings[$name] : null;
    }

    /**
     * Set All Providers
     *
     * @throws ProviderNotExistsException
     */
    protected function setAllProviders()
    {
        foreach ($this->providers as $name => $provider) {
            $this->setProvider($name);
        }
    }

    /**
     * Run
     */
    public function run()
    {
        foreach ($this->instances as $name => $instance) {
            $instance->fetch();
            if ($instance->hasData()) {
                $instance->prepare();
                $instance->store();
            }


            //Clean the folders when supported in the particular report
            if (method_exists($instance, 'clean')) {
                $instance->clean();
            }
        }
    }
}