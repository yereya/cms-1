<?php namespace App\Libraries\Scraper;

use App\Libraries\ShortCodes\Contracts\CustomShortCodes;

class ShortCodes extends CustomShortCodes
{
    /**
     * Search for a variable/property
     * Also allows you to pass type which will allow you search more specifically
     *
     * @param $atts
     *
     * @return bool|string
     * @throws \Exception
     */
    public function sc_var($atts)
    {
        $atts = filterAndSetParams([
            'col_name' => '', // the name of the returned variable
        ], $atts);

        $this->validateRequiredAttributes($atts, ['col_name']);

        return arraySearchKey($atts['col_name'], $this->haystack);
    }

    //    /**
    //     * Url Query Value
    //     * Will find a query in the haystack and extract the value
    //     *
    //     * @param $atts
    //     *
    //     * @return string
    //     * @throws InvalidShortCodeAttributesException
    //     */
    //    public function sc_destUrl($atts)
    //    {
    //        $atts = filterAndSetParams([
    //            'key' => null,   // The key of the query value you are looking for
    //            // key=val&key2=val the key would be the key of the value we are interested in getting
    //            'col_name' => null, // the name of key inside the haystack to search for
    //        ], $atts);
    //
    //        $this->validateRequiredAttributes($atts, ['key', 'col_name']);
    //
    //        // Extract the query
    //        $query_str = $this->sc_var($atts);
    //
    //        if ($query_str === false && strpos($query_str, $atts['key']) === false) {
    //            return '';
    //        }
    //
    //        print_r($query_str . PHP_EOL);
    //
    //        return isset($query_str) ? $query_str : '';
    //    }

}