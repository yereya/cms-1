<?php namespace App\Libraries\Scraper;

use App\Libraries\Scraper\Exceptions\InvalidDateRangeException;
use Carbon\Carbon;
use Exception;
use League\Csv\Reader;

/**
 * Class ReportTrait
 *
 * @package App\Libraries\Scraper
 */
trait ReportTrait
{
    /**
     * @var string $date_range_type
     */
    protected $date_range_type;

    /**
     * @var Carbon $date_range_start
     */
    protected $date_range_start;

    /**
     * @var Carbon $date_range_end
     */
    protected $date_range_end;

    /**
     * Get Fields
     *
     * @return mixed
     */
    abstract public function getFields();

    /**
     * Get options
     *
     * @return mixed
     */
    abstract public function getOptions();

    /**
     * Set options
     */
    abstract public function setOptions();

    /**
     * Get Logger
     *
     * @return mixed
     */
    abstract public function getLogger();

    /**
     * Set Report Date Range
     */
    protected function setReportDateRange()
    {
        $this->setTodayYesterdayDateRangeType();
        $this->setDateRangeType();
        $this->setDateRangeStartAndEnd();
    }

    /**
     * Set Today Yesterday Date Range Type
     */
    protected function setTodayYesterdayDateRangeType()
    {
        $options = $this->getOptions();
        if ($options['date-range-type'] == 'today_yesterday') {
            $this->setOptions('date-range-type', 'CUSTOM_DATE');
            $this->setOptions('date-range-start', Carbon::now()->subDay()->toDateString());
            $this->setOptions('date-range-end', Carbon::now()->toDateString());
        }
    }

    /**
     * Set Date Range Type
     *
     * @throws InvalidDateRangeException
     */
    protected function setDateRangeType()
    {
        $options = $this->getOptions();
        $logger  = $this->getLogger();
        $range   = strtolower($options['date-range-type']);
        $ranges  = array_map('strtolower', $this->date_ranges);

        if (!in_array($range, $ranges)) {
            $logger->error('the range ' . $range . ' is not valid');
            throw new InvalidDateRangeException;
        }

        $range_index           = array_search($range, $ranges);
        $this->date_range_type = $this->date_ranges[$range_index];

        $logger->debug(['date range type:', $this->date_range_type]);
    }

    /**
     * Set Date Range Start And End
     */
    protected function setDateRangeStartAndEnd()
    {
        $options = $this->getOptions();
        if ($options['date-range-start'] && $options['date-range-end']) {
            $dateStart = $options['date-range-start'] == 'yesterday' ? date('Y-m-d', strtotime('-1 days')) : $options['date-range-start'];
            $dateEnd   = $options['date-range-end'] == 'today' ? date('Y-m-d') : $options['date-range-end'];

            $this->date_range_start = Carbon::createFromFormat('Y-m-d', $dateStart);
            $this->date_range_end   = Carbon::createFromFormat('Y-m-d', $dateEnd);
        }
    }

    protected function getSingleDateReportDateRangeCount()
    {
        $options = $this->getOptions();
        if ($options['date-range-type'] == 'custom_date') {
            $diff = strtotime($options['date-range-end']) - strtotime($options['date-range-start']);

            return $diff / (60 * 60 * 24);
        }
    }

    /**
     * Set Single Date Report Date Range
     *
     * @return array
     */
    protected function setSingleDateReportDateRange()
    {
        $days    = 0;
        $options = $this->getOptions();
        if ($options['date-range-type'] == 'custom_date') {
            $diff = strtotime($options['date-range-end']) - strtotime($options['date-range-start']);
            $days = $diff / (60 * 60 * 24);
        }

        $dates = [];
        for ($i = 0; $i <= $days; $i++) {
            $dates[] = date('Y-m-d', strtotime($options['date-range-end']) - 60 * 60 * 24 * $i);
        }

        return $dates;
    }

    /**
     * Parse Csv
     *
     * @param string $file
     *
     * @return \Iterator
     * @throws Exception
     */
    protected function parseCsv($file)
    {
        $csv = Reader::createFromPath($file);
        $this->queryCsv($csv);
        $parsed = $csv->fetchAssoc($this->normalizeFields(), $this->escapeFetchedRow());

        return $parsed;
    }

    /**
     * Normalize Fields
     *
     * @return array
     */
    protected function normalizeFields()
    {
        $additional = isset($this->additional_fields) ? (array) $this->additional_fields : [];

        return array_merge(array_unique(array_flatten($this->getFields())), $additional);
    }

    /**
     * Escape Fetched Row
     *
     * @return \Closure
     */
    protected function escapeFetchedRow()
    {
        return function ($row) {
            foreach ($row as $key => $value) {
                if (str_contains($value, '\\"') && count($temp = explode("\\", $value)) > 1) {
                    return null;
                }
            }

            return $row;
        };
    }

    /**
     * Query Csv
     *
     * @param Reader $csv
     */
    abstract protected function queryCsv(Reader $csv);
}