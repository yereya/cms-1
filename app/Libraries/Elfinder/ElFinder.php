<?php

namespace App\Libraries\Elfinder;

use App\Entities\Models\Sites\Media;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use App\Libraries\Media\FileInfo;
use App\Libraries\Media\MediaUploader;
use File;
use Illuminate\Support\Collection;


class ElFinder extends \elFinder
{
    /**
     * @var Media Collection
     */
    private $media;

    /**
     * @override
     * ElFinder constructor
     *
     * @param $parameters
     */
    public function __construct($parameters)
    {
        parent::__construct($parameters);

        $this->media = new ElFinderLib(request()->get('site'));
    }

    /**
     * @override
     * "Open" directory
     * Return array with following elements
     *  - cwd          - opened dir info
     *  - files        - opened dir content [and dirs tree if $parameters[tree]]
     *  - api          - api version (if $parameters[init])
     *  - uplMaxSize   - if $parameters[init]
     *  - error        - on failed
     *
     * @param  array  command arguments
     *
     * @return array
     * @author Dmitry (dio) Levashov
     **/
    protected function open($parameters)
    {
        $result = parent::open($parameters);

        return $this->media->openMedia($result);
    }

    /**
     * @override
     * Save uploaded files
     *
     * @param  array
     *
     * @return array
     **/
    protected function upload($parameters)
    {
        $result = parent::upload($parameters);

        return $this->media->updateMedia($result);
    }

    /**
     * @override
     * Paste
     *
     * @param $parameters
     *
     * @return array
     */
    protected function paste($parameters)
    {
        $destination = $this->destinationPath($parameters['dst']);
        $targets  = [];

        foreach ($parameters['targets'] as $file) {
            $path = $this->destinationPath($file);
            $filename = $this->media->getFileName($path);
            $targets[$filename]  = [
                'path' => $path,
                'filename' => $filename,
                'destination' => $destination,
                'is_cut' => $parameters['cut'],
                'is_folder' => $this->isFolder($path)
            ];
        }

        $result = parent::paste($parameters);

        foreach ($result['added'] as $file) {
            $path = $this->destinationPath($file['hash']);
            $targets[$file['name']]['new_path'] = $path;
        }

        return $this->media->pasteMedia($targets, $result);
    }

    /**
     * @override
     * Rename file
     *
     * @param  array $parameters
     *
     * @return array
     * @author Dmitry (dio) Levashov
     **/
    protected function rename($parameters)
    {
        $path = $this->destinationPath($parameters['target']);

        $target = [
            'path' => $path,
            'current_name' => $this->media->getFileName($path),
            'new_name' => $parameters['name'],
            'is_folder' => $this->isFolder($path)
        ];

        $result = parent::rename($parameters);

        foreach ($result['added'] as $file) {
            $target['new_path'] = $this->destinationPath($file['hash']);
        }

        return $this->media->renameMedia($target, $result);
    }

    /**
     * @override
     * Remove dirs/files
     *
     * @param array  command arguments
     *
     * @return array
     **/
    protected function rm($parameters)
    {
        $targets = [];

        foreach ($parameters['targets'] as $target) {
            $path           = $this->destinationPath($target);
            $targets[] = [
                'path'      => $path,
                'is_folder' => $this->isFolder($path)
            ];
        }

        $result = parent::rm($parameters);

        return $this->media->rmMedia($targets, $result);
    }

    /**
     * If this folder
     *
     * @param $path
     *
     * @return mixed
     */
    private function isFolder($path): bool
    {
        return File::isDirectory(public_path($this->media->getMediaFolderPath()) . $path);
    }


    /**
     * Get destination path
     *
     * @param $destination
     *
     * @return bool|string
     */
    private function destinationPath(string $destination): string
    {
        $volume    = $this->volume($destination);
        $full_path = $volume->realpath($destination);
        $position = strpos($full_path, '/images');

        if ($position !== false) {
            $length = $position + strlen('/images');
            if ($full_path[$length] == '/' || empty($full_path[$length])) {
                $full_path = substr($full_path, $position + strlen('/images'));
            }
        }

        return $full_path;
    }
}