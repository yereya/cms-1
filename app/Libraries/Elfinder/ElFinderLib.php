<?php

namespace App\Libraries\Elfinder;


use App\Entities\Models\Sites\Media;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use App\Libraries\ImageOptimizer\Optimizer;
use App\Libraries\Media\FileInfo;
use App\Libraries\Media\MediaUploader;
use File;
use Illuminate\Support\Collection;

class ElFinderLib
{
    /**
     * @var Collection $media
     */
    private $media;

    /**
     * @var Model $site
     */
    private $site;

    /**
     * @var string media folder
     */
    private $media_folder_path = "top-assets/sites/%d/images";

    /**
     * ElFinderLib constructor.
     *
     * @param $site_id
     */
    public function __construct($site_id)
    {
        $this->initMedia();
        $this->initSite($site_id);
        $this->setMediaFolderPath($site_id);
    }

    /**
     * Open media
     *
     * @param $result
     *
     * @return mixed
     */
    public function openMedia($result): array
    {
        $options = $result['options'] ?? [];
        $folder  = $this->getImageFolder($options);
        $media   = $this->getMediasByFolder($folder);

        $result['files'] = $this->insertDataToMedia($result['files'], $media);

        return $result;
    }

    /**
     * Update new media
     *
     * @param $result
     *
     * @return mixed
     */
    public function updateMedia($result): array
    {
        if (isset($result['added'])) {
            $this->storeMedia($result['added']);
        }

        return $result;
    }

    /**
     * Paste files or folders
     *
     * @param $targets
     * @param $result
     *
     * @return mixed
     */
    public function pasteMedia($targets, $result): array
    {
        foreach ($targets as $target) {
            if ($target['is_folder']) {
                $this->pasteFolder($target);
            } else {
                $this->pasteFile($target);
            }
        }

        return $result;
    }

    /**
     * Rename folder or files
     *
     * @param $target
     * @param $result
     *
     * @return mixed
     */
    public function renameMedia($target, $result): array
    {
        if ($target['is_folder']) {
            $medias = $this->getMediasByFolder($target['path']);

            foreach ($medias as $media) {
                $media->folder = $target['new_path'];
                $media->path   = $target['new_path'] . '/' . $media->filename;

                $media->save();
            }
        } else {
            $media = $this->getMediaByPath($target['path']);

            if ($media) {
                $media->path     = $target['new_path'];
                $media->filename = $target['new_name'];

                $media->save();
            }
        }

        return $result;
    }

    /**
     * Remove file or folder
     *
     * @param $targets
     * @param $result
     *
     * @return mixed
     */
    public function rmMedia($targets, $result): array
    {
        foreach ($targets as $target) {
            if ($target['is_folder']) {
                $medias = $this->getMediasByFolder($target['path']);

                foreach ($medias as $media) {
                    $media->delete();
                }
            } else {
                $media = $this->getMediaByPath($target['path']);

                if ($media) {
                    $media->delete();
                }
            }
        }

        return $result;
    }

    /**
     * Copy|Cut folder
     *
     * @param $target
     */
    private function pasteFolder($target)
    {
        $medias = $this->getMediasByFolder($target['path']);

        foreach ($medias as $media) {
            if ($target['is_cut']) {
                $media->folder = $target['new_path'];
                $media->path   = $target['new_path'] . '/' . $media->filename;

                $media->save();
            } else {
                $new_media         = $media->replicate();
                $new_media->folder = $target['new_path'];
                $new_media->path   = $target['new_path'] . '/' . $media->filename;

                $new_media->save();
            }
        }
    }

    /**
     * Copy|Cut file
     *
     * @param $target
     */
    private function pasteFile($target)
    {
        $media = $this->getMediaByPath($target['path']);

        if ($media) {
            if ($target['is_cut']) {
                $media->folder = $target['destination'];
                $media->path   = $target['new_path'];

                $media->save();
            } else {
                $new_media         = $media->replicate();
                $new_media->folder = $target['destination'];
                $new_media->path   = $target['new_path'];

                $new_media->save();
            }
        }
    }

    /**
     * Insert data to elfinder file
     * currently only media id
     *
     * @param $files
     * @param $media
     *
     * @return array
     */
    private function insertDataToMedia($files, $media)
    {
        $files_by_folder = $media->keyBy('filename');

        foreach ($files as $index => &$image) {
            if ($image['mime'] != 'directory') {
                $row = $files_by_folder->get($image['name']);
                if ($row) {
                    $image['media_id'] = $row->id;
                } else {
                    unset($files[$index]);
                }
            }
        }

        return array_values($files);
    }

    /**
     * Return path by url
     *
     * @param $url
     *
     * @return bool|null|string
     */
    public function getPath($url)
    {
        $path        = null;
        $index_start = strpos($url, 'images');
        $index_end   = strlen('images');

        if ($index_start !== false) {
            $path = substr($url, $index_start + $index_end);
        }

        return $path == '/' ? null : $path;
    }

    /**
     * Return last name in path
     * filename or folder name
     *
     * @param $path
     *
     * @return mixed
     */
    public function getFileName($path)
    {
        $path_to_array = explode('/', $path);

        return end($path_to_array);
    }

    /**
     * Get image folder name
     *
     * @param $options
     *
     * @return bool|null|string
     */
    public function getImageFolder($options)
    {
        $folder = $options['path'] ?? null;

        if (strpos($folder, 'images') !== false) {
            $index_start = strpos($folder, 'images');
            $index_end   = strlen('images');

            $folder = substr($folder, $index_start + $index_end);

            $folder = empty($folder) ? null : $folder;
        }

        return $folder;
    }

    /**
     * Return folder by url
     *
     * @param $url
     * @param $name
     *
     * @return bool|null|string
     */
    public function getFolderByUrl($url, $name)
    {
        $path        = $this->getPath($url);
        $index_start = strpos($path, $name);
        $folder      = null;

        if ($index_start !== false) {
            $folder = substr($path, 0, $index_start - 1);
        }

        return $folder;
    }

    /**
     * Get medias by folder
     *
     * @param $folder
     *
     * @return mixed
     */
    private function getMediasByFolder($folder)
    {
        return $this->media->filter(function ($item) use ($folder) {
            return $item->folder == $folder;
        });
    }

    /**
     * Get media by path
     *
     * @param $path
     *
     * @return mixed
     */
    private function getMediaByPath($path)
    {
        return $this->media->filter(function ($item) use ($path) {
            return $item->path == $path;
        })->first();
    }

    /**
     * Store new media
     *
     * @param $files
     */
    private function storeMedia($files)
    {
        foreach ($files as $file) {
            if ($file['mime'] != 'directory') {
                $path = $this->getMediaPath(true) .'/' . $this->getPath($file['url']);

                $images = $this->createImages($path);

                $media_file = new FileInfo($this->site);
                $media_file->setFilename($file['name']);
                $media_file->setSiteId($this->site->id);
                $media_file->setSiteName($this->site->name);
                $media_file->setMime($file['mime']);
                $media_file->setPath($this->getPath($file['url']));
                $media_file->setFolder($this->getFolderByUrl($file['url'], $file['name']));
                $dimensions = getimagesize($images['1x']);
                $media_file->setWidth($dimensions[0]);
                $media_file->setHeight($dimensions[1]);

                $media = new Media();
                $media->fill($media_file->out());
                $media->save();
            }
        }
    }

    /**
     * Copy some image - 3 times
     * resize to retina display
     * optimize
     *
     * @param string $path
     *
     * @return array
     */
    private function createImages(string $path) :array {
        $optimizer = new Optimizer();
        $created = [];

        $images = [
            '3x' => [
                'file' => $optimizer->resizeAndOptimize($path, 100),
                'name' => $this->createImageDimensionName($path, '@3x')
            ],
            '2x' => [
                'file' => $optimizer->resizeAndOptimize($path, ((100 / 3) * 2)),
                'name' => $this->createImageDimensionName($path, '@2x')
            ],
            '1x' => [
                'file' => $optimizer->resizeAndOptimize($path, ((100 / 3) * 1)),
                'name' => $this->createImageDimensionName($path, '')
            ]
        ];

        foreach ($images as $key => $image) {
            try {
                File::put($image['name'], $image['file']);
                $created[$key] = $image['name'];
            } catch (\Exception $e) {
                \Log::info('error image: ' . $e->getMessage());
            }
        }

        return $created;
    }

    /**
     * Create file name
     *
     * @param string $current_path
     * @param string $dimension_suffix
     *
     * @return string
     */
    private function createImageDimensionName(string $current_path, string $dimension_suffix) {
        $extension_file = substr($current_path, strrpos($current_path, '.') + 1);
        $path_without_extension = substr($current_path, 0, strlen($current_path) - strlen($extension_file) - 1);

        return $path_without_extension . $dimension_suffix . '.' . $extension_file;
    }

    /**
     * Update files to site
     *
     * @param        $medias
     * @param string $action
     *
     * @return $this
     */
    private function updateSite($medias, $action = 'upload')
    {
        if (count($medias) == 0) {
            return null;
        }

        $paths = [];
        foreach ($medias as $media) {
            $paths[] = url($this->getMediaPath()) . $media['path'];
        }

        $domain = (new SiteDomainRepo)->getBySite($this->site->id);

        if ($domain) {
            $client = new MediaUploader($domain->domain, $action);
            $result = $client->uploadFile($paths, MediaUploader::MEDIA_TYPE_FILE, $this->site->id);

            return $result;
        }
    }

    /**
     * Return media path
     *
     * @param bool $full
     *
     * @return string
     */
    private function getMediaPath(bool $full = false)
    {
        return $full
            ? public_path() . config('media.directory') . $this->site->id . DIRECTORY_SEPARATOR . 'images'
            : config('media.directory') . $this->site->id . DIRECTORY_SEPARATOR . 'images';
    }

    /**
     * Init media models
     */
    private function initMedia()
    {
        $this->media = Media::all();
    }

    /**
     * Init Site
     *
     * @param $site_id
     */
    private function initSite($site_id)
    {
        $this->site = Site::find($site_id);
    }

    /**
     * Set Media folder
     *
     * @param $site_id
     */
    private function setMediaFolderPath($site_id)
    {
        $this->media_folder_path = str_replace('%d', $site_id, $this->media_folder_path);
    }

    /**
     * Get media folder
     *
     * @return string
     */
    public function getMediaFolderPath()
    {
        return $this->media_folder_path;
    }
}