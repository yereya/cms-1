<?php /** @noinspection ALL */

namespace App\Libraries\PerconaMigration;

use App\Libraries\TpLogger;
use DB;
use Illuminate\Database\Query\Builder;

class OutMigration
{
    /**
     * @var \Illuminate\Database\Connection
     */
    private $from_connection;

    /**
     * @var \Illuminate\Database\Connection
     */
    private $to_connection;

    /**
     * @var \Illuminate\Database\Connection
     */
    private $dwh_connection;

    /**
     * @var int count records for each table
     */
    private $records_inserted = 0;

    /**
     * @var int
     */
    private $record_chunk = 200;

    /**
     * @var string
     */
    private $table_from;

    /**
     * @var string
     */
    private $table_to;

    /**
     * @var string
     */
    private $scraper_name;

    /**
     * @var string
     */
    private $scraper_to_date_limit;

    /**
     * @var array $truncated_tables_haystack - indicator for which tables have already been addressed
     */
    private $truncated_tables_haystack = [];

    /**
     * @var array
     */
    private $select_tracks_viewer_vw = [
        'trackId as track_id',
        'id',
        'timestamp',
        'source_track',
        'viewerId',
        'vwr_browser_ver as browser_version',
        'type',
        'event',
        'pid',
        'lpid',
        'campaignId',
        'internal',
        'base_commission_amount',
        'commission_amount',
        'currency',
        'base_amount',
        'amount',
        'sid',
        'device',
        'keyword',
        'placement',
        'network',
        'creative',
        'gclid',
        'msclkid',
        'tokenIP',
        'sourceClickDate as source_click_date',
        'action',
        'dynamic_list',
        'page',
        'position',
        'QueryString as query_string',
        'DynamicP as dynamicp',
        'adposition',
        'matchtype as match_type',
        'creationdate as creation_date',
        'updatedate as update_date',
        'pAdGroupId as pub_ad_group_id',
        'pTargetId as pub_target_id',
        'pCampaignId as pub_campaign_id',
        'vwr_viewerIndex',
        'vwr_viewerId',
        'vwr_browser',
        'vwr_device',
        'vwr_os',
        'vwr_country',
//        'vwr_city',
        'vwr_ip'
    ];

    /**
     * Sync new out tracks to dwh tracks
     *
     * @var array
     */
    private $select_tracks_out_new = [
        'id as track_id',
        'timestamp',
        'token as id',
        'source_token as source_track',
        'browser_version',
//        'source_token as vwr_viewerId',
        'viewer_id as viewerId',
        'event',
        'event as type',
        'placement_id as pid',
        'impression_token',
        'landing_page_id as landingpage_id',
//        'landing_pages.mongodb_id as lpid',
        'campaign_id',
//        'campaigns.campaign_mongo_id as campaignId',
        'internal',
        'base_commission_amount',
        'commission_amount',
        'orig_currency as currency',
        'amount',
        'sid',
        'list_id',
        'gid as gid_var',
        'display_device as device',
        'keyword',
        'network',
        'creative',
        'click_id',
        'token_ip as tokenIP',
        'display_placement as placement',
        'action',
        'dynamic_list',
        'page',
        'position',
        'query_string',
        'dynamicp',
        'adposition',
        'match_type',
        'timestamp as source_click_date',
        'createdAt as creation_date',
        'updatedAt as update_date',
        'p_ad_group_id as pub_ad_group_id',
        'p_target_id as pub_target_id',
        'p_campaign_id as pub_campaign_id',
        'viewer_id as vwr_viewerIndex',
        'viewer_browser as vwr_browser',
        'viewer_device as vwr_device',
        'viewer_os as vwr_os',
        'country as vwr_country',
        'fbp',
        'fbc',
//        'city as vwr_city',
//        'viewer_ip as vwr_ip'
    ];

    /**
     * @var array
     */
    private $select_impressions_new_out = [
        'id',
        'site_id',
        'site_name',
        'page_id',
        'page_name',
        'test_id',
        'version_id',
        'test_description',
//        'page_version',
        'referrer',
        'url',
        'lists',
//        'rendered_lists',
//        'query_params',
        'token',
        'impression_token',
        'viewer_id',
        'gid',
        'gclid',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];

    /**
     * @var array
     */
    private $select_brands_groups_out_new = [
        'id',
        'name',
        'description',
        'company_id',
        'createdAt',
        'updatedAt'
    ];

    /**
     * @var array
     */
    private $select_companies_out_new = [
        'id',
        'name',
        'product_description',
        'SAP_id',
        'createdAt',
        'updatedAt'
    ];

    /**
     * @var array
     */
    private $select_advertiser_dbl = [
        'id',
        'name',
        'status',
        'type',
        'timezone'
    ];

    private $select_advertiser_dbl_new_out = [
        'mongodb_id',
        'id',
        'name',
        'status',
        'timezone'
    ];

    /**
     * @var array
     */
    private $select_brands = [
        'id',
        'name',
        'status',
        'advId'
    ];

    private $select_brands_new_out = [
        'id',
        'mongodb_id',
        'brands_group_id',
        'name',
        'status',
        'cpa',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];

    /**
     * @var array
     */
    private $select_campaign = [
        'id',
        'name',
        'status',
        'timezone as time_zone',
        'ioId'
    ];

    private $select_campaign_new_out = [
        'id',
        'mongodb_id',
        'name',
        'status',
        'brand_id',
        'brand_mongodb_id as brandId_mongodb_id',
        'timezone as time_zone',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];

    /**
     * @var array
     */
    private $select_landingpages = [
        'id as mongodb_id',
        'name',
        'url',
        'campaignId as campId_mogodb_id',
        'weight',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];

    private $select_landingpages_new_out = [
        'id',
        'mongodb_id',
        'campaign_mongodb_id as campId_mogodb_id',
        'campaign_id',
        'weight',
        'page',
        'name',
        'url',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];

    /**
     * @var array
     */
    private $select_publisher = [
        'id',
        'name',
        'status'
    ];

    /**
     * @var array
     */
    private $select_media = [
        'id',
        'name',
        'pubid'
    ];

    /**
     * @var array
     */
    private $select_placements = [
        'id',
        'name',
        'mediaId',
        'pubId'
    ];

    /**
     * @var array
     */
    private $select_placements_new_out = [
        'id',
        'mongodb_id',
        'name as placement_name',
        'campaign_id',
        'campaign_mongodb_id as campaign_mongo_id',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];

    /**
     * @var array
     */
    private $select_scheduler_tasks = [
        'id',
        'field_query_params_id',
        'task_id',
        'task_name',
        'repeat_delay',
        'active'
    ];

    /**
     * @var array
     */
    private $select_users = [
        'id',
        'email',
        'username',
        'first_name',
        'last_name',
        'bi_email',
        'status'
    ];

    /**
     * @var array
     */
    private $select_user_assigned_accounts = [
        'user_id',
        'account_id'
    ];


    /**
     * @var array
     */
    private $select_site_user_role = [
        'site_id',
        'user_id',
        'role_id'
    ];

    /**
     * @var array
     */
    private $select_user_roles = [
        'id',
        'name',
        'description'
    ];

    private $scraper_json_columns = [
        'scraper_bo' => [],
        'scraper_tracks' => [],
        'scraper_bo_out_new' => [],
        'scraper_impressions_out_new' => [
            'lists' => [
                'parserFunction' => 'parseImpressionLists',
                'targetTable' => 'mrr_fact_impression_brands_lists'
            ]
        ]
    ];

    private $scraper_option_groups = [
        // for scraper_name=scraper_bo
        'scraper_bo' => [

        ],
        // for sraper_name=scraper_tracks
        'scraper_tracks' => [
            [
                'from_table' => 'tracks',
                'to_table' => 'mrr_fact_tracks',
                'from_connection' => 'putin',
                'to_connection' => 'mrr'
            ]
        ],
        // for scraper_name=scraper_bo_out_new
        'scraper_bo_out_new' => [
            [
                'from_table' => 'advertisers',
                'to_table' => 'advertiser_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'brands',
                'to_table' => 'io_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'campaigns',
                'to_table' => 'out_campaigns_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'landing_pages',
                'to_table' => 'out_landingpages_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'placements',
                'to_table' => 'out_placements_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'scheduler_tasks',
                'to_table' => 'scheduler_tasks_tmp',
                'from_connection' => 'cms',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'users',
                'to_table' => 'bo_users_tmp',
                'from_connection' => 'cms',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'site_user_role',
                'to_table' => 'bo_site_user_role_tmp',
                'from_connection' => 'cms',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'user_roles',
                'to_table' => 'user_roles',
                'from_connection' => 'cms',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'user_assigned_accounts',
                'to_table' => 'bo_user_assigned_accounts_tmp',
                'from_connection' => 'cms',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'companies',
                'to_table' => 'companies_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'brands_groups',
                'to_table' => 'brands_groups_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ]
        ],
        'scraper_impressions_out_new' => [
            [
                'from_table' => 'impressions',
                'to_table' => 'mrr_fact_impressions',
                'from_connection' => 'putin',
                'to_connection' => 'mrr'
            ]
        ]
    ];

    /**
     * @var array $high_water_marks_field_map - indicates which water mark field name fits which column
     */
    private $high_water_marks_field_map = [
        'tracks_process' => [
            'src_updated'   => 'high_water_mark',
//            'track_id'      => 'high_water_mark_id'
        ],
        'impressions_process' => [// dwh_fact_impressions
            'src_updated'    => 'high_water_mark'
        ]
    ];

    /**
     * @var array $hwm_col_map - maps the hwm table columns to the class's properties
     */
    private $hwm_col_map = [
        'high_water_mark' => 'from_update_date',
        'high_water_mark_id' => 'last_track_id_loaded'
    ];

    /**
     * Join mapping for join decisions
     */
    const JOIN_MAPS = [
        'tracks' => [
            'campaigns' => ['id' => 'campaign_id'],
            'landing_pages' => ['id' => 'landing_page_id']
        ],
        'brands' => [
            'advertisers' => ['id' => 'advertiser_id']
        ]
    ];

    const JOIN_ADD_SELECT_FIELDS = [
        'brands' => [
//            'advertisers' => ['mongodb_id as advId']
            'advertisers' => ['id as advertiser_id']
        ],
        'tracks' => [
            'landing_pages' => ['mongodb_id as lpId'],
            'campaigns' => ['mongodb_id as campaignId']
        ]
    ];

    /**
     * out bo scraper names
     */
    const SCRAPER_BO = 'scraper_bo';
    const SCRAPER_TRACKS = 'scraper_tracks';
    const SCRAPER_BO_OUT_NEW = 'scraper_bo_out_new';
    const SCRAPER_BO_OUT_IMPRESSIONS = 'scraper_impressions_out_new';

    /**
     * will determine where the track id comes from(new out or old out connections)
     */
    const OLD_OUT_TRACK_ID_LIMIT = 100000000000;

    /**
     * will determine where the placement id comes from(new out or old out connection)
     */
    const OLD_OUT_PLACEMENT_ID_LIMIT = 100000;

    const DWH_HIGH_WATER_MARKS_TABLE = 'high_water_mark_tbl';

    /**
     * @var default values use in where query
     */
    private $from_update_date;
    private $last_track_id_loaded;
    private $last_contextual_id;

    /**
     * @var indicator for an empty table
     */
    private $table_empty = false;

    /**
     * @var TpLogger
     */
    private $logger;
    private $logger_system_id = 101;

    /**
     * OutMigration constructor.
     */
    public function __construct()
    {
        $this->logger = new TpLogger($this->logger_system_id);
    }

    /**
     * Run scrapers
     *
     * @param $scraper_name
     */
    public function run($scraper_name,$scraper_to_date) {
        $this->logger->info(['Start', 'Scraper name: ' . $scraper_name]);

        // NOTICE: temporarily switches old out to bo process with new out to bo process
        $scraper_name = $scraper_name == self::SCRAPER_BO ? self::SCRAPER_BO_OUT_NEW : $scraper_name;

        $this->scraper_name = $scraper_name;
        # Set date limit
        $this->scraper_to_date_limit = ($scraper_to_date!='null'?$scraper_to_date:date('Y-m-d',strtotime('+1 Days')));

        // Gets the correct connections/tables options for the scraper
        $options_group = ($this->scraper_option_groups[$scraper_name] ?? []);

        if (empty($options_group)) {
            $this->logger->error('Could not find scraper "' . $scraper_name . '"');
            return;
        }
        try {
            // Executes the scraper process passed as an option to the command
            foreach ($options_group as $option) {
                $this->executeScraperOptionProcess($option, (in_array($scraper_name ,[self::SCRAPER_TRACKS, self::SCRAPER_BO_OUT_IMPRESSIONS])) );
            }
            $this->logger->info('-- Finished moving data to mrr  --');
            // will determine which dwh process method will be called
            $this->determineProcedureCall($scraper_name);
            $this->logger->info('-- Finished procedure call --');
            // NOTICE: Temporarily commented out for disabling old out out_to_bo process
//            if ($scraper_name == self::SCRAPER_BO) {
//                foreach ($this->scraper_option_groups[self::SCRAPER_BO_OUT_NEW] as $option) {
//                    $this->executeScraperOptionProcess($option, ($scraper_name == self::SCRAPER_TRACKS));
//                }
//
//                $this->determineProcedureCall(self::SCRAPER_BO_OUT_NEW);
//            }
        } catch (\Exception $e) {
            $this->logger->error('Process Error: ' . $e->getMessage());
            $this->closeConnection();
        }

        $this->logger->info(['End', 'Scraper name: ' . $scraper_name]);
        $this->closeConnection();
    }

    private function executeScraperOptionProcess($option, $tracks_query = false)
    {
        $this->init($option, $tracks_query);
        if ($this->from_connection->getName() != 'putin' || ($this->truncated_tables_haystack[$this->table_to] ?? false) !== true) {

//            if($this->table_from == 'impressions'){
//                $time_offset = "-6 Months";
//                $this->to_connection->table($option['to_table'])->whereDate('created_at','<', date('Y-m-d',strtotime($time_offset)))->delete();
//                foreach ($this->scraper_json_columns[$this->scraper_name] as $jsonColumn) {
//                    $tableToTruncate = $jsonColumn['targetTable'];
//                    $this->to_connection->table($tableToTruncate)->whereDate('updated_at','<', date('Y-m-d',strtotime($time_offset)))->delete();
//                }
//                $this->logger->info(['deleted successfully records before ',date('Y-m-d',strtotime($time_offset)) ,', Table name: ' . $option['to_connection'] . '.' .$option['to_table']]);
//                $this->truncated_tables_haystack[$this->table_to] = true;
//            }
//            else{

            $this->to_connection->table($option['to_table'])->truncate();

            foreach ($this->scraper_json_columns[$this->scraper_name] as $jsonColumn) {
                $tableToTruncate = $jsonColumn['targetTable'];
                $this->to_connection->table($tableToTruncate)->truncate();
            }

            $this->logger->info(['truncate success', 'Table name: ' . $option['to_connection'] . '.' .$option['to_table']]);
            $this->truncated_tables_haystack[$this->table_to] = true;
//        }
        }

        $this->startMigration($this->shouldDetermineIdSegregation() || $tracks_query);

        $this->logger->info(['To table: ', $option['to_table'], 'inserted: ' . $this->records_inserted . ' new',
            'From table: ', $option['from_table'] ?? 'tracks_viewer_vw', 'connection: ', $this->from_connection->getName()]);
        $this->closeConnection();
    }

    /**
     * Init
     * connections
     * tables
     *
     * @param      $options
     * @param bool $should_tracks_query
     */
    private function init($options, $should_tracks_query = false)
    {
        $this->table_from = $options['from_table'] ?? 'tracks_viewer_vw';
//        $this->table_to = $options['to_table'] ?? 'mrr_fact_tracks_tmp';
        $this->table_to = $options['to_table'] ?? 'mrr_fact_tracks'; // TODO: TEST THIS
        $this->from_connection = DB::connection($options['from_connection'] ?? 'out');
        $this->to_connection = DB::connection($options['to_connection'] ?? 'mrr');
        $this->dwh_connection = DB::connection('dwh');
        $this->records_inserted = 0;

        // Solves the empty db case
        if ($this->to_connection->table($options['to_connection'] . '.' . $options['to_table'])->first() === null) {
            $this->table_empty = true;
        }

        if ($this->shouldDetermineIdSegregation()) {
            $this->populateLastContextualId();
        }

        if ($should_tracks_query) {
            $this->setDwhTrackHighWaterMarks($options);
            $this->validateToDate();
        }
    }


    /**
     *  Make sure the date which was given by the user is valid.
     */
    public function validateToDate()
    {
            $date = strtotime($this->scraper_to_date_limit);
            if(!$date or $date < $this->from_update_date){
                throw new \Exception("You entered wrong date or not in the range [between water mark and today] for to_date option in the CLI command, please enter Y-m-d. OutMigration.php -> setToDate");
        }
    }

    /**
     * Determines the where clause for old out and new out, as well as its selected values
     * populates the mapped columns (updated_date, last_loaded_track_id)
     *
     * @param $options
     */
    public function setDwhTrackHighWaterMarks($options)
    {
        $table_name = self::DWH_HIGH_WATER_MARKS_TABLE;
        $table_by_connection = ($options['from_connection'] == 'putin')
            ? /*self::OUTNEW_DWH_HWM_TBL*/'outnew_dwh_fact_tracks'
            : /*self::OUT_OLD_DWH_HWM_TBL*/'out_dwh_fact_tracks';

        $hwm_field_map_modified = $this->high_water_marks_field_map['tracks_process'];
        if ($options['to_table'] == 'mrr_fact_impressions') {
            $table_by_connection = 'dwh_fact_impressions';
            $hwm_field_map_modified = $this->high_water_marks_field_map['impressions_process'];
        }

        foreach ($hwm_field_map_modified as $field => $mapped_col) {
            $where_query = [
                ['table_name', '=', $table_by_connection],
                ['field_name','=', $field]
            ];

            $this->{$this->hwm_col_map[$mapped_col]} =  $this->dwh_connection->table($table_name)->select($mapped_col)
                    ->where($where_query)->first()->{$mapped_col} ?? null;
        }
    }

    /**
     * startMigration
     *
     * @param bool $should_where_query
     * @param Builder $active_builder
     */
    public function startMigration($should_where_query = false, Builder $active_builder = null) {
        /**
         * build query builder
         */
        $builder = !empty($active_builder) ? $active_builder
            : $this->from_connection->table($this->table_from)->select($this->unambiguize($this->getSelectValues()));

        if (self::shouldJoin() && !$active_builder) {
            $this->determineJoinParams($builder);
        }

        /**
         * if command have parameter values
         * insert where
         */
        if ($should_where_query) {
            $this->aggregateWhereQuery($builder);
        }

        /**
         * get data records by chunk and insert to table to
         */
        $_this = $this;
        $builder->chunk($this->record_chunk, function($records) use ($_this) {
            $values = $_this->parseTrackViewerMigrate($records);
            $parsedRecordsToInsertMap = [];
            $parsedRecordsToInsertMap[$_this->table_to] = $values;
            if (!empty(array_keys($this->scraper_json_columns[$this->scraper_name]))) {
                foreach ($this->scraper_json_columns[$this->scraper_name] as $columnName=>$parsingInfo) {
                    $parserFunction = $parsingInfo['parserFunction'];
                    $targetTable = $parsingInfo['targetTable'];
                    $parsedRecordsToInsertMap[$targetTable] = [];
                    foreach ($records as $record) {
                        $parsedJsonRecords = call_user_func(array($this, $parserFunction),$record);
                        if (!empty($parsedJsonRecords)) {
                            array_push($parsedRecordsToInsertMap[$targetTable], ...$parsedJsonRecords);
                        }
                    }
                }
            }
            $this->insertRecordsTransaction($parsedRecordsToInsertMap);
        });
    }

    private function parseImpressionLists($impressionRecord)
    {
        $parsedRecords = [];
        // check if valid json value
        $lists = json_decode($impressionRecord->lists, true);
        if (json_last_error() !== JSON_ERROR_NONE) {
//            print_r(json_last_error());
            $this->logger->warning("Could not parse to json column. in scraper $this->scraper_name");
            return false;
        }

        foreach ($lists as $listId => $list) {
            $listBrandsRecords = [];
            $brandsList = $list['brands'] ? $list['brands'] : [];
            $linupId = $list['drawnOrderId'] ?? null;
            $lineupLabel = $list['lineupLabel'] ?? null;
            $rotationId = $list['rotation_id'] ?? null;
            $list_order = $list['listOrder'] ?? null;
            for ($i = 0; $i < count($brandsList); $i++) {
                try {
                    $brandInfo = $brandsList[$i];
                    $newBrandRecord = [];//                $result = array_merge($newBrandRecord, $list['brands'][$i]);
//                    $newBrandRecord['brand_name'] = array_key_exists ( 'name' , $brandInfo) ? $brandInfo['name'] : null;
                    $newBrandRecord['brand_id'] = array_key_exists ( 'brand_id' , $brandInfo) ? (!empty($brandInfo['brand_id']) ? $brandInfo['brand_id'] : null) : null;
                    $newBrandRecord['brand_score'] = array_key_exists ( 'score' , $brandInfo) ? (string)$brandInfo['score'] : null;
                    $newBrandRecord['brand_stars'] = array_key_exists ( 'stars' , $brandInfo) ? (string)$brandInfo['stars'] : null;
//                    $newBrandRecord['brand_score_text'] = array_key_exists ( 'scoreText' , $brandInfo) ? $brandInfo['scoreText'] : null;
                    $newBrandRecord['brand_position'] = $i + 1; //starts from 1 instead of 0
                    $newBrandRecord['impression_id'] = $impressionRecord->id ?? null;
                    $newBrandRecord['impression_token'] = $impressionRecord->impression_token ?? null;
                    $newBrandRecord['list_id'] = $listId;
                    $newBrandRecord['rotation_id'] = $rotationId;
                    $newBrandRecord['lineup_id'] = $linupId;
                    $newBrandRecord['lineup_label'] = $lineupLabel;
                    $newBrandRecord['list_order'] = $list_order;
                    $newBrandRecord['created_at'] = $impressionRecord->created_at;
                    $newBrandRecord['updated_at'] = $impressionRecord->updated_at;
                    $listBrandsRecords[] = $newBrandRecord;
                } catch (\Exception $e) {
                    $this->logger->error('Json Parsing Error: ' . $e->getMessage());
                    print_r($e);
                }
            }
            if (!empty($listBrandsRecords)) {
                array_push($parsedRecords, ...$listBrandsRecords); // append the newly parsed records to the total parsed records array.
            }
        }

        return $parsedRecords;

    }

    /**
     * Close connections
     */
    private function closeConnection() {
        $this->from_connection->disconnect();
        $this->to_connection->disconnect();
        $this->dwh_connection->disconnect();
    }

    /**
     * Build where query
     *
     * @return array
     */
    private function getWhereValues()
    {
        $from_connection = $this->from_connection->getName();
        $where_query_values = [];
        // Determine the correct connection values
        if ($from_connection == 'out') { // Fetching from old out connection
            $where_query_values = $this->getOldOutConnectionWhereValues();
        } else if( $from_connection == 'putin') { // Fetching from new out connection
            $where_query_values = $this->getNewOutConnectionWhereValues();
            // All other connections
        }
        else {
            $where_query_values = $this->determineWhereValuesByCurConnection();
        }

        return $where_query_values;
    }

    /**
     * Retrieves the old out conection where query values
     */
    private function getOldOutConnectionWhereValues()
    {
//        if ($this->table_from == 'tracks_viewer_vw' && $this->table_to == 'mrr_fact_tracks_tmp') {
//            return [
//                [
//                    ['trackId', '>', $this->last_track_id_loaded],
//                    ['trackId', '<', self::OLD_OUT_TRACK_ID_LIMIT]
//                ]
//            ];
//        }
//
//        if ($this->table_from == 'tracks_viewer_vw' && $this->table_to == 'mrr_fact_tracks_tmp_upd') {
//            return [
//                [
//                    ['trackId', '>', $this->last_track_id_loaded],
//                    ['trackId', '<', self::OLD_OUT_TRACK_ID_LIMIT]
//                ], [
//                    ['updateDate', '>=', $this->from_update_date]
//                ]
//            ];
//        }

        // TODO: TEST THIS
        if ($this->table_from == 'tracks_viewer_vw' && $this->table_to == 'mrr_fact_tracks') {
            return [
                [
                    ['updateDate', '>=', $this->from_update_date],
                    ['updateDate', '<=', $this->scraper_to_date_limit]
                ]
            ];
        }

//        if ($this->table_from == 'tracks' && $this->table_to == 'mrr_fact_tracks_tmp') {
//            return [
//                [
//                    ['id', '>', $this->last_track_id_loaded],
//                    ['id', '<', self::OLD_OUT_TRACK_ID_LIMIT]
//                ]
//            ];
//        }
//
//        if ($this->table_from == 'tracks' && $this->table_to == 'mrr_fact_tracks_tmp_upd') {
//            return [
//                [
//                    ['id', '>', $this->last_track_id_loaded],
//                    ['id', '<', self::OLD_OUT_TRACK_ID_LIMIT]
//                ], [
//                    ['updatedAt', '>=', $this->from_update_date]
//                ]
//            ];
//        }

        // invaluable from/to tables
        return [];
    }

    /**
     * Retrieves the new out connection where query values
     */
    private function getNewOutConnectionWhereValues()
    {
//        if ($this->table_from == 'tracks' && $this->table_to == 'mrr_fact_tracks_tmp') {
//            return [
//                [
//                    ['tracks.id', '>', max($this->last_track_id_loaded, self::OLD_OUT_TRACK_ID_LIMIT)]
//                ], [
//                    ['tracks.updatedAt', '>=', $this->from_update_date]
//                ]
//            ];
//        }
//
//        if ($this->table_from == 'tracks' && $this->table_to == 'mrr_fact_tracks_tmp_upd') {
//            return [
//                [
//                    ['tracks.id', '>=', max($this->last_track_id_loaded, self::OLD_OUT_TRACK_ID_LIMIT)]
//                ], [
//                    ['tracks.updatedAt', '>=', $this->from_update_date]
//                ]
//            ];
//        }

        // TODO: TEST THIS
        if ($this->table_from == 'tracks' && $this->table_to == 'mrr_fact_tracks') {
            return [
                [
                    ['tracks.updatedAt', '>=', $this->from_update_date],
                    ['tracks.updatedAt', '<=', $this->scraper_to_date_limit]
                ]
            ];
        }

        if ($this->table_from == 'impressions' && $this->table_to == 'mrr_fact_impressions') {
            return [
                [
                    ['impressions.updatedAt', '>=', $this->from_update_date],
                    ['impressions.updatedAt', '<=', $this->scraper_to_date_limit]
                ]
            ];
        }

        if ($this->shouldDetermineIdSegregation()) {
            $id = 'id';
            $ambiguity_prefix = '';

            if (in_array($this->table_from, ['placements', 'brands'])) {
//                $id = 'auto_inc_id';
                $ambiguity_prefix .= $this->table_from . '.';
            }

            return [
//                [
//                    [$ambiguity_prefix . $id, '>', $this->last_contextual_id]
//                ], [
//                    [$ambiguity_prefix . 'updatedAt', '>=', $this->from_update_date]
//                ]
            ];
        }

        return [];
    }

    /**
     * Determine the where query values that will be returned for each connnection
     * connections have different logic rules for insertion
     *
     * @return array
     */
    private function determineWhereValuesByCurConnection()
    {
        // currently there is no logic for different connections than out
        return [];
    }

    /**
     * Populates the last id from the target table to increment from
     */
    private function populateLastContextualId()
    {
        // Last percona id under 100,000 or last percona id above 100,000(redundant)
        $condition = $this->from_connection->getName() == 'putin' ? '>=' : '<';

        // Set the last contextual_id to be the maximum between the target table and the retrieved table, else 100,000
        $this->last_contextual_id = $this->getMaximalTableToId($condition) ?? self::OLD_OUT_PLACEMENT_ID_LIMIT;
        $this->from_update_date = $this->getMaximalUpdatedDate($condition) ?? date('Y-m-d H:i:s', strtotime('today'));
    }

    /**
     * Will retrieve the maximal id between the tmp target table and the original table(original's id will be larger in case the tmp is empty)
     *
     * @param string $condition
     * @return mixed
     */
    private function getMaximalTableToId($condition = '<')
    {

        return $this->to_connection->table($this->getOriginTableToName())->select(DB::raw('max(id) as id'))->where([
            ['id', $condition, self::OLD_OUT_PLACEMENT_ID_LIMIT]
        ])->first()->id;
    }

    /**
     * populates maximal update date for comparison
     *
     * @param string $condition
     * @return mixed
     */
    private function getMaximalUpdatedDate($condition = '<')
    {
        return $this->to_connection->table($this->getOriginTableToName())->select(DB::raw('max(updated_at) as updatedAt'))->where([
            ['id', $condition, self::OLD_OUT_PLACEMENT_ID_LIMIT]
        ])->first()->updatedAt;
    }

    /**
     * returns the original table that the stored procedure targets
     *
     * @return null|string|string[]
     */
    private function getOriginTableToName()
    {
        $table_str_suffixes = ['s', 'io', 'vw'];
        if ($this->from_connection->getName() == 'putin') {
            $table_str_suffixes = array_merge([], $table_str_suffixes);
        } elseif ($this->from_connection->getName() == 'out') {
            $table_str_suffixes = array_merge([], $table_str_suffixes);
        }

        if (in_array($this->table_to, ['io_tmp', 'io_outnew_tmp'])) {

            return /*$this->table_to*/'out_brands';
        }

        $concat_table_name = preg_replace('/_outnew|_tmp|_upd/', '', $this->table_to);
        $concat_table_name .= ends_with($concat_table_name, $table_str_suffixes) ? '' : 's';

        return $concat_table_name;
    }

    /**
     * Get Select values
     *
     * @return array
     */
    private function getSelectValues()
    {
//        if ($this->table_from == 'tracks_viewer_vw' && $this->table_to == 'mrr_fact_tracks_tmp') {
//            return $this->select_tracks_viewer_vw;
//        }
//
//        if ($this->table_from == 'tracks_viewer_vw' && $this->table_to == 'mrr_fact_tracks_tmp_upd') {
//            return $this->select_tracks_viewer_vw;
//        }

        if ($this->table_from == 'impressions' && $this->table_to == 'mrr_fact_impressions') {
            return $this->select_impressions_new_out;
        }

        // TODO: TEST THIS
        if ($this->table_from == 'tracks_viewer_vw' && $this->table_to == 'mrr_fact_tracks') {
            return $this->select_tracks_viewer_vw;
        }

        if ($this->table_from == 'advertiser' && $this->table_to == 'advertiser_tmp') {
            return $this->select_advertiser_dbl;
        }

        // new out
        if ($this->table_from == 'advertisers' && $this->table_to == 'advertiser_outnew_tmp') {
            return $this->select_advertiser_dbl_new_out;
        }

        if ($this->table_from == 'io' && $this->table_to == 'io_tmp') {
            return $this->select_brands;
        }

        // new out
        if ($this->table_from == 'brands' && $this->table_to == 'io_outnew_tmp') {
            return $this->select_brands_new_out;
        }

        if ($this->table_from == 'campaign' && $this->table_to == 'out_campaigns_tmp') {
            return $this->select_campaign;
        }

        // new out
        if ($this->table_from == 'campaigns' && $this->table_to == 'out_campaigns_outnew_tmp') {
            return $this->select_campaign_new_out;
        }

        if ($this->table_from == 'landingpage' && $this->table_to == 'out_landingpages_tmp') {
            return $this->select_landingpages;
        }

        // new out
        if ($this->table_from == 'landing_pages' && $this->table_to == 'out_landingpages_outnew_tmp') {
            return $this->select_landingpages_new_out;
        }

        if ($this->table_from == 'publisher' && $this->table_to == 'publisher_tmp') {
            return $this->select_publisher;
        }

        if ($this->table_from == 'media' && $this->table_to == 'media_tmp') {
            return $this->select_media;
        }

        if ($this->table_from == 'placement' && $this->table_to == 'out_placements_tmp') {
            return $this->select_placements;
        }

        // new out
        if ($this->table_from == 'placements' && $this->table_to == 'out_placements_outnew_tmp') {
            return $this->select_placements_new_out;
        }

        if ($this->table_from == 'scheduler_tasks' && $this->table_to == 'scheduler_tasks_tmp') {
            return $this->select_scheduler_tasks;
        }

        if ($this->table_from == 'users' && $this->table_to == 'bo_users_tmp') {
            return $this->select_users;
        }

        if ($this->table_from == 'user_assigned_accounts' && $this->table_to == 'bo_user_assigned_accounts_tmp') {
            return $this->select_user_assigned_accounts;
        }

        if ($this->table_from == 'site_user_role' && $this->table_to == 'bo_site_user_role_tmp') {
            return $this->select_site_user_role;
        }

        if ($this->table_from == 'user_roles' && $this->table_to == 'user_roles') {
            return $this->select_user_roles;
        }

//        if ($this->table_from == 'tracks' && $this->table_to == 'mrr_fact_tracks_tmp') {
//            return $this->select_tracks_out_new;
//        }
//
//        if ($this->table_from == 'tracks' && $this->table_to == 'mrr_fact_tracks_tmp_upd') {
//            return $this->select_tracks_out_new;
//        }

        // TODO: TEST THIS
        if ($this->table_from == 'tracks' && $this->table_to == 'mrr_fact_tracks') {
            return $this->select_tracks_out_new;
        }

        if ($this->table_from == 'companies' && $this->table_to == 'companies_tmp') {
            return $this->select_companies_out_new;
        }
        if ($this->table_from == 'brands_groups' && $this->table_to == 'brands_groups_tmp') {
            return $this->select_brands_groups_out_new;
        }
    }

    /**
     * removes any ambiguity we might have in our selection
     *
     * @param array $select_values
     * @return array
     */
    private function unambiguize(array $select_values)
    {
        $unambiguized_select_values = [];
        foreach ($select_values as $key => $select_value) {
            $unambiguized_select_values[$key] = $this->table_from . '.' . $select_value;
        }

        return $unambiguized_select_values;
    }

    /**
     * Determines whether a current query builder should perform a join according to certain rules
     *
     * @return bool
     */
    private function shouldJoin()
    {
        // Build the current process data for validation decision
        $_process_data = [
            'from_connection'   => $this->from_connection->getName(),
            'to_connection'     => $this->to_connection->getName(),
            'table_from'        => $this->table_from,
            'table_to'          => $this->table_to
        ];

        return self::validateJoinRules($_process_data, $this->logger);
    }

    /**
     * Validates process data according to our custom rules
     *
     * @param $_process_data
     * @param TpLogger $logger
     * @return bool
     */
    private static function validateJoinRules($_process_data, TpLogger $logger)
    {
        // Build custom rules
        $rules = [
            'from_connection'   => 'in:putin',
            'to_connection'     => 'in:bo,mrr',
            'table_from'        => 'in:brands,tracks',
//            'table_to'          => 'in:io_outnew_tmp,mrr_fact_tracks_tmp,mrr_fact_tracks_tmp_upd'
            'table_to'          => 'in:io_outnew_tmp,mrr_fact_tracks'// TODO: TEST THIS
        ];

        // Validates process data should activate a specific join
        $validator = \Validator::make($_process_data, $rules);
        if ($validator->fails()) {

            return false;
        }

        // Spit the messages to notify a join has been performed
        $logger->info($_process_data, $validator->messages()->all(), 'Successfully performed the join');

        return true;
    }

    private function determineJoinParams(Builder &$builder, $complementary_scope_table = null)
    {
        // Target: determine which joins should be performed, and join the current builder on them

        // 1. Build an array of joins according to the current run's parameters
        //   - for the current from_table and to_table combination: get the tables to join and their "on" params
        // 2. Perform the join on a given query builder

        $join_map = !is_null($complementary_scope_table) ? array_except(self::JOIN_MAPS[$this->table_from], $complementary_scope_table) : self::JOIN_MAPS[$this->table_from];
        $select_values = $this->unambiguize($this->getSelectValues());

        foreach ($join_map as $table_target => $target_map) {
            foreach ($target_map as $right_table_key => $left_table_key) {
                $complementary_query = $this->from_connection->table($this->table_from)->select($select_values);

                $builder->join($table_target, $this->table_from . '.' . $left_table_key, '=', $table_target . '.' . $right_table_key);

                if ( !empty( ($additional_selections = self::JOIN_ADD_SELECT_FIELDS[$this->table_from][$table_target]) ?? false) ) {
                    $builder->addSelect(array_map(function($selection) use($table_target) {return $table_target . '.' . $selection;},$additional_selections));
                }

                // As long as we are in the root join scope
                if (!$complementary_scope_table) {
                    $this->determineJoinParams($complementary_query, $table_target);
                    // Takes care of tracks that were not joint
                    $this->aggregateWhereQuery($complementary_query)->whereNull($this->table_from . '.' . $left_table_key);
                    // Migrate the partial results
                    $this->startMigration(false, $complementary_query);
                }
            }
        }
    }

    /**
     * Builds the where query for the current builder
     *
     * @param Builder $builder
     * @return Builder
     */
    private function aggregateWhereQuery(Builder &$builder) : Builder
    {
        $_this = $this;
        $builder->where(function($query) use($_this) {
            foreach ($_this->getWhereValues() as $idx => $where_query_values) {

                //check for null value in
                foreach ($where_query_values as $where_condition) {
                    if (array_search(null , $where_condition)) {
                        $error = 'Got null value in a `Where` clause, for condition: ' . json_encode($where_condition);
                        $this->logger->error($error);
                        $this->logger->error('Ending run due to an Error that occured in aggregateWhereQuery');
                        $this->closeConnection();
                        exit($error);
                    }
                }
                if ($idx == 0) {
                    // todo test bug
                    $query->where($where_query_values);
                    continue;
                }
                $query->orWhere($where_query_values);
            }
        });
        return $builder;
    }

    /**
     * Parse to array
     *
     * @param $viewers
     *
     * @return array
     */
    private function parseTrackViewerMigrate($viewers) {
        $records = [];
        foreach ($viewers as $viewer) {
            $record = [];
            foreach ($viewer as $key => $value) {
                if ($key == 'msclkid' && (is_null($value) || $value == '{msclkid}')) {
                    $record[$key] = NULL;
                } elseif ($key == 'internal' && in_array($value, [0, 1])) {
                    $record[$key] = var_export(boolval($value), true);
                } elseif ($key == 'click_id') {
                    if (!empty($value) && in_array(($nw = $viewer->network ?? ''), ['msn', 'g', 's', 'd', 'FB'])) {
                        // Determine which key should be taken and be given the $value var
                        $altered_key = ( $nw == 'msn' ) ? 'msclkid' : (preg_match('/g|s|d|FB/', $nw) ? 'gclid' : NULL);
                        $record[$altered_key] = $value;
                        $diff = array_diff(['gclid', 'msclkid'], [$altered_key]);
                        $complementary_key = end($diff);
                        $record[$complementary_key] = NULL;
                    } else {
                        if(empty($value)){
                            $record['gclid'] = NULL;
                        }else{
                            $record['gclid'] = $value;
                        }
                        $record['msclkid'] = NULL;
                    }
                } elseif ( ($key == 'source_token' || $key == 'source_track') && $this->from_connection->getName() == 'putin' ) {
                    $record[$key] = $value;
                    $fixed_value = !empty($viewer->viewerId) ? $viewer->viewerId : NULL;
                    $record['vwr_viewerId'] = $fixed_value;
                    $record['viewerId'] = $fixed_value;
                } elseif (in_array($key, ['currency', 'orig_currency']) && empty($value)) {
                    if (empty($value)) {
                        $record[$key] = 'USD';
                    }
                } else {
                    $record[$key] = $value ?? NULL;
                }
            }

            $records[] = $record;
        }

        return $records;
    }

    /**
     * Determines whether there is a rule for separating ids
     *
     * @return bool
     */
    private function shouldDetermineIdSegregation()
    {
        $allowed_from_tables = collect($this->scraper_option_groups)->pluck('*.from_table')->flatten()->filter(function($val, $key) {
            return !in_array($val, [
                "tracks_viewer_vw",
                "tracks",
                "publisher",
                "media",
                "scheduler_tasks",
                "users",
                "site_user_role",
                "user_roles",
                "user_assigned_accounts",
                "impressions",
                'brands_groups',
                'companies'
            ]);
        })->toArray();

        $allowed_to_tables   = collect($this->scraper_option_groups)->pluck('*.to_table')->flatten()->filter(function($val, $key) {
            return !in_array($val, [
                "scheduler_tasks_tmp",
                "bo_users_tmp",
                "bo_site_user_role_tmp",
                "user_roles",
                "bo_user_assigned_accounts_tmp",
//                "mrr_fact_tracks_tmp_upd",
//                "mrr_fact_tracks_tmp",
                "mrr_fact_tracks",
                "publisher_tmp",
                "media_tmp",
                "mrr_fact_impressions",
                "mrr_fact_impression_brands_lists"
            ]);
        })->toArray();
        return ( in_array($this->table_from, $allowed_from_tables) && in_array($this->table_to, $allowed_to_tables) );
    }

    /**
     * Separates the new out ids from old out ids by incrementing the original id by 100,000
     *
     * @param $records
     */
    private function incrementIdsByRule(&$records)
    {
        $incremental_offset = $this->last_contextual_id;
        $above_limit = false;

        if ($this->table_from == 'placement') {
            $above_limit = false;
        } else if ($this->table_from == 'placements') {
            $above_limit = true;
        }

        foreach ($records as &$record) {
            $record = ['id' => ++$incremental_offset] + $record;
        }

        // Makes sure the next batch will know of the last placement id parsed
        $this->last_contextual_id = $incremental_offset;
    }

    /**
     * Insert to
     *
     * @param $mapTableRecords
     */
    private function insertRecordsTransaction($mapTableRecords) {
        try {
            $_this = $this;
            $this->to_connection->transaction(function () use ($mapTableRecords){
                foreach ($mapTableRecords as $tableName => $records) {
                    $this->to_connection->table($tableName)->insert($records);
                    $this->records_inserted += count($records);
                }
            });

            $_this->table_empty = false;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            print_r($e->getMessage());
        }
    }

    /**
     * Insert to
     *
     * @param $values
     */
    private function insert($values) {
        try {
            $this->to_connection->table($this->table_to)->insert($values);
            $this->records_inserted += count($values);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            print_r($e->getMessage());
        }
    }

    /**
     * Insert to
     *
     * @param $values
     */
    private function insertToTable($values, $tableName) {
        try {
            $this->to_connection->table($tableName)->insert($values);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            print_r($e->getMessage());
        }
    }


    private function determineProcedureCall($scraper_name)
    {
        switch ($scraper_name) {
            case self::SCRAPER_TRACKS:
                $this->dwh_connection->statement('call tracks_control_sp_nodblink(139, 73, 16, @err)');
                $this->logger->info(['Procedure', 'call tracks_control_sp_nodblink(139, 73, 16, @err)']);
                break;
            case self::SCRAPER_BO:
                $this->dwh_connection->statement('call out_to_bo_sync_control_sp_nodblink(142, 73, 16, @err)');
                $this->logger->info(['Procedure', 'call out_to_bo_sync_control_sp_nodblink(142, 73, 16, @err)']);
                break;
            case self::SCRAPER_BO_OUT_NEW:
                // fill the method for the job we will define
                $this->dwh_connection->statement('call outnew_to_bo_sync_control_sp_nodblink(142, 73, 16, @err)');
                $this->logger->info(['Procedure', 'call outnew_to_bo_sync_control_sp_nodblink(142, 73, 16, @err)']);
                break;
            case self::SCRAPER_BO_OUT_IMPRESSIONS:
                $this->dwh_connection->statement('call dwh.impressions_control_sp(590, 73, 279,@err)');
                $this->logger->info(['Procedure', 'call dwh.impressions_control_sp(590, 73, 279,@err)']);
                break;
            default:
                $this->logger->warning('Could not find stored procedure for ' . $scraper_name);
                break;
        }
    }
}
