<?php
/**
 * Created by PhpStorm.
 * User: azusdex
 * Date: 23/01/2018
 * Time: 15:11
 */

namespace App\Libraries\PerconaMigration;


use App\Entities\Models\Log;
use DB;
use Illuminate\Support\Collection;

class LogMigrations
{
    private $dwh_connection;
    private $dwh_table = 'logs';
    private $last_record_id;

    /**
     * LogMigrations constructor.
     */
    public function __construct()
    {
        $this->dwh_connection = DB::connection('dwh');
    }

    /**
     * Run migrations
     */
    public function run()
    {
        $records = $this->getRemoteRecords();
        $parsed = $this->parseRemoteRecords($records);
        $inserted = $this->insertToLogs($parsed);

        return $inserted;
    }

    /**
     * Get cms.logs last record
     *
     * @return mixed null | YYYY-m-d H:M:S
     */
    private function getLocalLastRecord()
    {
        if (!$this->last_record_id) {
            $this->last_record_id = Log::whereNotNull('uuid_entry_record_id')->max('created_at');
        }

        return $this->last_record_id;
    }

    /**
     * Get last records from dwh.logs
     *
     * @return \Illuminate\Support\Collection
     */
    private function getRemoteRecords(): Collection
    {
        $builder = $this->dwh_connection->table($this->dwh_table);

        if ($this->getLocalLastRecord()) {
            $records = $builder->where('created_at', '>', $this->getLocalLastRecord())->get();
        } else {
            $records = $builder->get();
        }

        return $records;
    }

    /**
     * Parse to log structure
     *
     * @param Collection $records
     *
     * @return array
     */
    private function parseRemoteRecords(Collection $records): array
    {
        $parsed = [];

        foreach ($records as $record) {
            $parsed[$record->entry_record_id][] = [
                'entry_record_id'      => null,
                'system_id'            => $record->system_id,
                'task_id'              => $record->task_id,
                'class_name'           => $record->class_name,
                'function_name'        => $record->function_name,
                'message'              => $record->message,
                'error_type'           => $record->error_type,
                'run_duration'         => $record->run_duration,
                'created_at'           => $record->created_at,
                'updated_at'           => $record->updated_at,
                'uuid_entry_record_id' => $record->entry_record_id
            ];
        }

        return $parsed;
    }

    /**
     * Insert to cms.logs
     *
     * @param array $records
     *
     * @return int
     */
    private function insertToLogs(array $records): int
    {
        $inserted = 0;

        foreach ($records as $item) {
            $parent_id = null;
            foreach ($item as $record) {
                $log                       = new Log();
                $log->system_id            = $record['system_id'];
                $log->task_id              = $record['task_id'];
                $log->class_name           = $record['class_name'];
                $log->function_name        = $record['function_name'];
                $log->message              = $record['message'];
                $log->error_type           = $record['error_type'];
                $log->run_duration         = $record['run_duration'];
                $log->created_at           = $record['created_at'];
                $log->updated_at           = $record['updated_at'];
                $log->uuid_entry_record_id = $record['uuid_entry_record_id'];
                $log->parent_id            = $parent_id;
                $log->save();

                if (!$parent_id) {
                    $parent_id = $log->id;
                }
            }

            $inserted++;
        }

        return $inserted;
    }

    /**
     * Close connection
     */
    public function __destruct()
    {
        $this->dwh_connection->disconnect();
    }
}