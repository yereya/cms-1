<?php


namespace App\Libraries\PerconaMigration;

use App\Libraries\TpLogger;
use DB;
use Illuminate\Database\Query\Builder;


class OutCmsMigration
{
    /**
     * @var \Illuminate\Database\Connection
     */
    private $from_connection;
    /**
     * @var \Illuminate\Database\Connection
     */
    private $to_connection;
    /**
     * @var \Illuminate\Database\Connection
     */
    private $dwh_connection;
    /**
     * @var int count records for each table
     */
    private $records_inserted = 0;
    /**
     * @var int
     */
    private $record_chunk = 500;
    /**
     * @var string
     */
    private $table_from;

    /**
     * @var string
     */
    private $table_to;
    /**
     * @var string
     */
    private $scraper_name;
    /**
     * @var array $truncated_tables_haystack - indicator for which tables have already been addressed
     */
    private $truncated_tables_haystack = [];
    /**
     * @var array
     */
    private $select_brands_groups_out_new = [
        'id',
        'name',
        'description',
        'company_id',
        'createdAt',
        'updatedAt'
    ];
    /**
     * @var array
     */
    private $select_companies_out_new = [
        'id',
        'name',
        'product_description',
        'SAP_id',
        'createdAt',
        'updatedAt'
    ];
    /**
     * @var array
     */
    private $select_advertiser_dbl = [
        'id',
        'name',
        'status',
        'type',
        'timezone'
    ];
    private $select_advertiser_dbl_new_out = [
        'mongodb_id',
        'id',
        'name',
        'status',
        'timezone',
        'createdAt AS updated_at',
        'updatedAt AS created_at'
    ];
    /**
     * @var array
     */
    private $select_brands = [
        'id',
        'name',
        'status',
        'advId'
    ];
    private $select_brands_new_out = [
        'id',
        'mongodb_id',
        'brands_group_id',
        'name',
        'status',
        'cpa',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];
    /**
     * @var array
     */
    private $select_campaign = [
        'id',
        'name',
        'status',
        'timezone as time_zone',
        'ioId'
    ];
    private $select_campaign_new_out = [
        'id',
        'mongodb_id',
        'name',
        'status',
        'brand_id',
        'brand_mongodb_id as brandId_mongodb_id',
        'timezone as time_zone',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];
    /**
     * @var array
     */
    private $select_landingpages = [
        'id as mongodb_id',
        'name',
        'url',
        'campaignId as campId_mogodb_id',
        'weight',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];
    private $select_landingpages_new_out = [
        'id',
        'mongodb_id',
        'campaign_mongodb_id as campId_mogodb_id',
        'campaign_id',
        'weight',
        'page',
        'name',
        'url',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];
    /**
     * @var array
     */
    private $select_publisher = [
        'id',
        'name',
        'status'
    ];
    /**
     * @var array
     */
    private $select_media = [
        'id',
        'name',
        'pubid'
    ];
    /**
     * @var array
     */
    private $select_placements = [
        'id',
        'name',
        'mediaId',
        'pubId'
    ];
    /**
     * @var array
     */
    private $select_placements_new_out = [
        'id',
        'mongodb_id',
        'name as placement_name',
        'campaign_id',
        'campaign_mongodb_id as campaign_mongo_id',
        'createdAt as created_at',
        'updatedAt as updated_at'
    ];
    /**
     * @var array
     */
    private $select_scheduler_tasks = [
        'id',
        'field_query_params_id',
        'task_id',
        'task_name',
        'repeat_delay',
        'active'
    ];
    /**
     * @var array
     */
    private $select_users = [
        'id',
        'email',
        'username',
        'first_name',
        'last_name',
        'bi_email',
        'status'
    ];
    /**
     * @var array
     */
    private $select_user_assigned_accounts = [
        'user_id',
        'account_id'
    ];
    /**
     * @var array
     */
    private $select_site_user_role = [
        'site_id',
        'user_id',
        'role_id'
    ];
    /**
     * @var array
     */
    private $select_user_roles = [
        'id',
        'name',
        'description'
    ];
    /**
     * @var array
     */
    private $select_wp_site_posts_to_advertisers = [
        'site_id',
        'post_type',
        'post_type_description',
        'advertiser_id',
        'createdAt'
    ];

    /**
     * @var array
     */
    private $select_currencies = [
        'id',
        'currency',
        'rate',
        'UPDATE',
        'createdAt',
        'updatedAt'
    ];

    private $scraper_option_groups = [
        'scraper_out' => [
            [
                'from_table' => 'advertisers',
                'to_table' => 'advertiser_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'brands',
                'to_table' => 'io_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'campaigns',
                'to_table' => 'out_campaigns_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'landing_pages',
                'to_table' => 'out_landingpages_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'placements',
                'to_table' => 'out_placements_outnew_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'companies',
                'to_table' => 'companies_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'brands_groups',
                'to_table' => 'brands_groups_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'currencies',
                'to_table' => 'currencies_tmp',
                'from_connection' => 'putin',
                'to_connection' => 'bo'
            ],
            [
                'from_table' => 'wp_site_posts_to_advertisers',
                'to_table' => 'wp_site_posts_to_advertisers',
                'from_connection' => 'putin',
                'to_connection' => 'cms'
            ]
        ],
        'scraper_cms' => [
                [
                    'from_table' => 'scheduler_tasks',
                    'to_table' => 'scheduler_tasks_tmp',
                    'from_connection' => 'cms',
                    'to_connection' => 'bo'
                ],
                [
                    'from_table' => 'users',
                    'to_table' => 'bo_users_tmp',
                    'from_connection' => 'cms',
                    'to_connection' => 'bo'
                ],
                [
                    'from_table' => 'site_user_role',
                    'to_table' => 'bo_site_user_role_tmp',
                    'from_connection' => 'cms',
                    'to_connection' => 'bo'
                ],
                [
                    'from_table' => 'user_roles',
                    'to_table' => 'user_roles',
                    'from_connection' => 'cms',
                    'to_connection' => 'bo'
                ],
                [
                    'from_table' => 'user_assigned_accounts',
                    'to_table' => 'bo_user_assigned_accounts_tmp',
                    'from_connection' => 'cms',
                    'to_connection' => 'bo'
                ]
        ]
    ];
    /**
     * Join mapping for join decisions
     */
    const JOIN_MAPS = [
        'brands' => [
            'advertisers' => ['id' => 'advertiser_id']
        ]
    ];

    const JOIN_ADD_SELECT_FIELDS = [
        'brands' => [
//            'advertisers' => ['mongodb_id as advId']
            'advertisers' => ['id as advertiser_id']
        ]
    ];
    /**
     * out bo scraper names
     */
    const SCRAPER_OUT = 'scraper_out';
    const SCRAPER_CMS = 'scraper_cms';
    /**
     * will determine where the track id comes from(new out or old out connections)
     */
    const OLD_OUT_TRACK_ID_LIMIT = 100000000000;
    /**
     * will determine where the placement id comes from(new out or old out connection)
     */
    const OLD_OUT_PLACEMENT_ID_LIMIT = 100000;
    /**
     * @var default values use in where query
     */
    private $from_update_date;
    private $last_contextual_id;
    /**
     * @var indicator for an empty table
     */
    private $table_empty = false;
    /**
     * @var TpLogger
     */
    private $logger;
    private $logger_system_id = 101;
    /**
     * OutMigration constructor.
     */
    public function __construct()
    {
        $this->logger = new TpLogger($this->logger_system_id);
    }
    /**
     * Run scrapers
     *
     * @param $scraper_name
     */
    public function run($scraper_name) {
        $this->logger->info(['Start', 'Scraper name: ' . $scraper_name]);
        $this->scraper_name = $scraper_name;
        // Gets the correct connections/tables options for the scraper
        $options_group = ($this->scraper_option_groups[$scraper_name] ?? []);

        if (empty($options_group)) {
            $this->logger->error('Could not find scraper "' . $scraper_name . '"');
            return;
        }
        try {
            // Executes the scraper process passed as an option to the command
            foreach ($options_group as $option) {
                $this->executeScraperOptionProcess($option, false ); # TODO remove
            }

            // will determine which dwh process method will be called
            $this->determineProcedureCall($scraper_name);
        } catch (\Exception $e) {
            $this->logger->error('Process Error: ' . $e->getMessage());
            $this->closeConnection();
        }

        $this->logger->info(['End', 'Scraper name: ' . $scraper_name]);
        $this->closeConnection();
    }

    private function executeScraperOptionProcess($option, $tracks_query = false)
    {
        $this->init($option, $tracks_query);
        # TODO check if it is relavent for cms also
        if ($this->from_connection->getName() != 'putin' || ($this->truncated_tables_haystack[$this->table_to] ?? false) !== true) {
            $this->to_connection->table($option['to_table'])->truncate();


            $this->logger->info(['truncate success', 'Table name: ' . $option['to_connection'] . '.' .$option['to_table']]);
            $this->truncated_tables_haystack[$this->table_to] = true;
        }

        $this->startMigration($this->shouldDetermineIdSegregation() || $tracks_query);

        $this->logger->info(['To table: ', $option['to_table'], 'inserted: ' . $this->records_inserted . ' new',
            'From table: ', $option['from_table'] , 'connection: ', $this->from_connection->getName()]);

        $this->closeConnection();
    }

    /**
     * Init
     * connections
     * tables
     *
     * @param      $options
     * @param bool $should_tracks_query
     */
    private function init($options, $should_tracks_query = false)
    {
        $this->table_from = $options['from_table'] ;
        $this->table_to = $options['to_table'] ;
        $this->from_connection = DB::connection($options['from_connection'] ?? 'out');
        $this->to_connection = DB::connection($options['to_connection'] );
        $this->dwh_connection = DB::connection('dwh');
        $this->records_inserted = 0;

        // Solves the empty db case
        if ($this->to_connection->table($options['to_connection'] . '.' . $options['to_table'])->first() === null) {
            $this->table_empty = true;
        }

        if ($this->shouldDetermineIdSegregation()) {
            $this->populateLastContextualId();
        }

    }

    /**
     * startMigration
     *
     * @param bool $should_where_query
     * @param Builder $active_builder
     */
    public function startMigration($should_where_query = false, Builder $active_builder = null) {
        /**
         * build query builder
         */
        $builder = !empty($active_builder) ? $active_builder
            : $this->from_connection->table($this->table_from)->select($this->unambiguize($this->getSelectValues()));

        if (self::shouldJoin() && !$active_builder) {
            $this->determineJoinParams($builder);
        }
        /**
         * get data records by chunk and insert to table to
         */
        $_this = $this;
        $builder->chunk($this->record_chunk, function($records) use ($_this) {
            $values = $_this->parseTrackViewerMigrate($records);

            $parsedRecordsToInsertMap = [];
            $parsedRecordsToInsertMap[$_this->table_to] = $values;

            $this->insertRecordsTransaction($parsedRecordsToInsertMap);
        });
    }

    /**
     * Close connections
     */
    private function closeConnection() {
        $this->from_connection->disconnect();
        $this->to_connection->disconnect();
        $this->dwh_connection->disconnect();
    }

    /**
     * Build where query
     *
     * @return array
     */
    private function getWhereValues()
    {
        $from_connection = $this->from_connection->getName();
        $where_query_values = [];
        // Determine the correct connection values
        if( $from_connection == 'putin') { // Fetching from new out connection
            $where_query_values = $this->getNewOutConnectionWhereValues();
            // All other connections
        }
        else {
            $where_query_values = $this->determineWhereValuesByCurConnection();
        }
        return $where_query_values;
    }
    /**
     * Retrieves the new out connection where query values
     */
    private function getNewOutConnectionWhereValues()
    {


        if ($this->shouldDetermineIdSegregation()) {
            $id = 'id';
            $ambiguity_prefix = '';

            if (in_array($this->table_from, ['placements', 'brands'])) {
//                $id = 'auto_inc_id';
                $ambiguity_prefix .= $this->table_from . '.';
            }

            return [

            ];
        }

        return [];
    }

    /**
     * Determine the where query values that will be returned for each connnection
     * connections have different logic rules for insertion
     *
     * @return array
     */
    private function determineWhereValuesByCurConnection()
    {
        // currently there is no logic for different connections than out
        return [];
    }

    /**
     * Populates the last id from the target table to increment from
     */
    private function populateLastContextualId()
    {
        // Last percona id under 100,000 or last percona id above 100,000(redundant)
        $condition = $this->from_connection->getName() == 'putin' ? '>=' : '<';

        // Set the last contextual_id to be the maximum between the target table and the retrieved table, else 100,000
        $this->last_contextual_id = $this->getMaximalTableToId($condition) ?? self::OLD_OUT_PLACEMENT_ID_LIMIT;
        $this->from_update_date = $this->getMaximalUpdatedDate($condition) ?? date('Y-m-d H:i:s', strtotime('today'));
    }

    /**
     * Will retrieve the maximal id between the tmp target table and the original table(original's id will be larger in case the tmp is empty)
     *
     * @param string $condition
     * @return mixed
     */
    private function getMaximalTableToId($condition = '<')
    {

        return $this->to_connection->table($this->getOriginTableToName())->select(DB::raw('max(id) as id'))->where([
            ['id', $condition, self::OLD_OUT_PLACEMENT_ID_LIMIT]
        ])->first()->id;
    }

    /**
     * populates maximal update date for comparison
     *
     * @param string $condition
     * @return mixed
     */
    private function getMaximalUpdatedDate($condition = '<')
    {
        return $this->to_connection->table($this->getOriginTableToName())->select(DB::raw('max(updated_at) as updatedAt'))->where([
            ['id', $condition, self::OLD_OUT_PLACEMENT_ID_LIMIT]
        ])->first()->updatedAt;
    }

    /**
     * returns the original table that the stored procedure targets
     *
     * @return null|string|string[]
     */
    private function getOriginTableToName()
    {
        $table_str_suffixes = ['s', 'io', 'vw'];
        if ($this->from_connection->getName() == 'putin') {
            $table_str_suffixes = array_merge([], $table_str_suffixes);
        } elseif ($this->from_connection->getName() == 'out') {
            $table_str_suffixes = array_merge([], $table_str_suffixes);
        }

        if (in_array($this->table_to, ['io_tmp', 'io_outnew_tmp'])) {

            return /*$this->table_to*/'out_brands';
        }

        $concat_table_name = preg_replace('/_outnew|_tmp|_upd/', '', $this->table_to);
        $concat_table_name .= ends_with($concat_table_name, $table_str_suffixes) ? '' : 's';

        return $concat_table_name;
    }

    /**
     * Get Select values
     *
     * @return array
     */
    private function getSelectValues()
    {
        if ($this->table_from == 'advertiser' && $this->table_to == 'advertiser_tmp') {
            return $this->select_advertiser_dbl;
        }
        // new out
        if ($this->table_from == 'advertisers' && $this->table_to == 'advertiser_outnew_tmp') {
            return $this->select_advertiser_dbl_new_out;
        }
        if ($this->table_from == 'io' && $this->table_to == 'io_tmp') {
            return $this->select_brands;
        }
        // new out
        if ($this->table_from == 'brands' && $this->table_to == 'io_outnew_tmp') {
            return $this->select_brands_new_out;
        }
        if ($this->table_from == 'campaign' && $this->table_to == 'out_campaigns_tmp') {
            return $this->select_campaign;
        }
        // new out
        if ($this->table_from == 'campaigns' && $this->table_to == 'out_campaigns_outnew_tmp') {
            return $this->select_campaign_new_out;
        }
        if ($this->table_from == 'landingpage' && $this->table_to == 'out_landingpages_tmp') {
            return $this->select_landingpages;
        }
        // new out
        if ($this->table_from == 'landing_pages' && $this->table_to == 'out_landingpages_outnew_tmp') {
            return $this->select_landingpages_new_out;
        }
        if ($this->table_from == 'publisher' && $this->table_to == 'publisher_tmp') {
            return $this->select_publisher;
        }
        if ($this->table_from == 'media' && $this->table_to == 'media_tmp') {
            return $this->select_media;
        }
        if ($this->table_from == 'placement' && $this->table_to == 'out_placements_tmp') {
            return $this->select_placements;
        }
        // new out
        if ($this->table_from == 'placements' && $this->table_to == 'out_placements_outnew_tmp') {
            return $this->select_placements_new_out;
        }
        if ($this->table_from == 'scheduler_tasks' && $this->table_to == 'scheduler_tasks_tmp') {
            return $this->select_scheduler_tasks;
        }
        if ($this->table_from == 'users' && $this->table_to == 'bo_users_tmp') {
            return $this->select_users;
        }
        if ($this->table_from == 'user_assigned_accounts' && $this->table_to == 'bo_user_assigned_accounts_tmp') {
            return $this->select_user_assigned_accounts;
        }
        if ($this->table_from == 'site_user_role' && $this->table_to == 'bo_site_user_role_tmp') {
            return $this->select_site_user_role;
        }
        if ($this->table_from == 'user_roles' && $this->table_to == 'user_roles') {
            return $this->select_user_roles;
        }
        if ($this->table_from == 'companies' && $this->table_to == 'companies_tmp') {
            return $this->select_companies_out_new;
        }
        if ($this->table_from == 'brands_groups' && $this->table_to == 'brands_groups_tmp') {
            return $this->select_brands_groups_out_new;
        }
        if ($this->table_from == 'wp_site_posts_to_advertisers' && $this->table_to == 'wp_site_posts_to_advertisers') {
            return $this->select_wp_site_posts_to_advertisers;
        }
        if ($this->table_from == 'currencies' && $this->table_to == 'currencies_tmp') {
            return $this->select_currencies;
        }
    }

    /**
     * removes any ambiguity we might have in our selection
     *
     * @param array $select_values
     * @return array
     */
    private function unambiguize(array $select_values)
    {
        $unambiguized_select_values = [];
        foreach ($select_values as $key => $select_value) {
            $unambiguized_select_values[$key] = $this->table_from . '.' . $select_value;
        }

        return $unambiguized_select_values;
    }

    /**
     * Determines whether a current query builder should perform a join according to certain rules
     *
     * @return bool
     */
    private function shouldJoin()
    {
        // Build the current process data for validation decision
        $_process_data = [
            'from_connection'   => $this->from_connection->getName(),
            'to_connection'     => $this->to_connection->getName(),
            'table_from'        => $this->table_from,
            'table_to'          => $this->table_to
        ];

        return self::validateJoinRules($_process_data, $this->logger);
    }

    /**
     * Validates process data according to our custom rules
     *
     * @param $_process_data
     * @param TpLogger $logger
     * @return bool
     */
    private static function validateJoinRules($_process_data, TpLogger $logger)
    {
        // Build custom rules
        $rules = [
            'from_connection'   => 'in:putin',
            'to_connection'     => 'in:bo',
            'table_from'        => 'in:brands',
            'table_to'          => 'in:io_outnew_tmp'
        ];

        // Validates process data should activate a specific join
        $validator = \Validator::make($_process_data, $rules);
        if ($validator->fails()) {

            return false;
        }

        // Spit the messages to notify a join has been performed
        $logger->info($_process_data, $validator->messages()->all(), 'Successfully performed the join');

        return true;
    }

    private function determineJoinParams(Builder &$builder, $complementary_scope_table = null)
    {
        // Target: determine which joins should be performed, and join the current builder on them

        // 1. Build an array of joins according to the current run's parameters
        //   - for the current from_table and to_table combination: get the tables to join and their "on" params
        // 2. Perform the join on a given query builder

        $join_map = !is_null($complementary_scope_table) ? array_except(self::JOIN_MAPS[$this->table_from], $complementary_scope_table) : self::JOIN_MAPS[$this->table_from];
        $select_values = $this->unambiguize($this->getSelectValues());

        foreach ($join_map as $table_target => $target_map) {
            foreach ($target_map as $right_table_key => $left_table_key) {
                $complementary_query = $this->from_connection->table($this->table_from)->select($select_values);

                $builder->join($table_target, $this->table_from . '.' . $left_table_key, '=', $table_target . '.' . $right_table_key);

                if ( !empty( ($additional_selections = self::JOIN_ADD_SELECT_FIELDS[$this->table_from][$table_target]) ?? false) ) {
                    $builder->addSelect(array_map(function($selection) use($table_target) {return $table_target . '.' . $selection;},$additional_selections));
                }

                // As long as we are in the root join scope
                if (!$complementary_scope_table) {
                    $this->determineJoinParams($complementary_query, $table_target);
                    // Takes care of tracks that were not joint
                    $this->aggregateWhereQuery($complementary_query)->whereNull($this->table_from . '.' . $left_table_key);
                    // Migrate the partial results
                    $this->startMigration(false, $complementary_query);
                }
            }
        }
    }

    /**
     * Builds the where query for the current builder
     *
     * @param Builder $builder
     * @return Builder
     */
    private function aggregateWhereQuery(Builder &$builder) : Builder
    {
        $_this = $this;
        $builder->where(function($query) use($_this) {
            foreach ($_this->getWhereValues() as $idx => $where_query_values) {

                //check for null value in
                foreach ($where_query_values as $where_condition) {
                    if (array_search(null , $where_condition)) {
                        $error = 'Got null value in a `Where` clause, for condition: ' . json_encode($where_condition);
                        $this->logger->error($error);
                        $this->logger->error('Ending run due to an Error that occured in aggregateWhereQuery');
                        $this->closeConnection();
                        exit($error);
                    }
                }
                if ($idx == 0) {
                    // todo test bug
                    $query->where($where_query_values);
                    continue;
                }
                $query->orWhere($where_query_values);
            }
        });
        return $builder;
    }

    /**
     * Parse to array
     *
     * @param $viewers
     *
     * @return array
     */
    private function parseTrackViewerMigrate($viewers) {
        $records = [];
        foreach ($viewers as $viewer) {
            $record = [];
            foreach ($viewer as $key => $value) {
                    $record[$key] = $value ?? NULL;
            }

            $records[] = $record;
        }

        return $records;
    }

    /**
     * Determines whether there is a rule for separating ids
     *
     * @return bool
     */
    private function shouldDetermineIdSegregation()
    {
        $allowed_from_tables = collect($this->scraper_option_groups)->pluck('*.from_table')->flatten()->filter(function($val, $key) {
            return !in_array($val, [
                "publisher",
                "media",
                "scheduler_tasks",
                "users",
                "site_user_role",
                "user_roles",
                "user_assigned_accounts",
                'brands_groups',
                'companies',
                'wp_site_posts_to_advertisers',
                'currencies'
            ]);
        })->toArray();

        $allowed_to_tables   = collect($this->scraper_option_groups)->pluck('*.to_table')->flatten()->filter(function($val, $key) {
            return !in_array($val, [
                "scheduler_tasks_tmp",
                "bo_users_tmp",
                "bo_site_user_role_tmp",
                "user_roles",
                "bo_user_assigned_accounts_tmp",
                "publisher_tmp",
                "media_tmp",
                "wp_site_posts_to_advertisers",
                "currencies_tmp"
            ]);
        })->toArray();
        return ( in_array($this->table_from, $allowed_from_tables) && in_array($this->table_to, $allowed_to_tables) );
    }

    /**
     * Separates the new out ids from old out ids by incrementing the original id by 100,000
     *
     * @param $records
     */
    private function incrementIdsByRule(&$records)
    {
        $incremental_offset = $this->last_contextual_id;
        $above_limit = false;

        if ($this->table_from == 'placement') {
            $above_limit = false;
        } else if ($this->table_from == 'placements') {
            $above_limit = true;
        }

        foreach ($records as &$record) {
            $record = ['id' => ++$incremental_offset] + $record;
        }

        // Makes sure the next batch will know of the last placement id parsed
        $this->last_contextual_id = $incremental_offset;
    }
    /**
     * Insert to
     *
     * @param $mapTableRecords
     */
    private function insertRecordsTransaction($mapTableRecords) {
        try {
            $_this = $this;
            $this->to_connection->transaction(function () use ($mapTableRecords){
                foreach ($mapTableRecords as $tableName => $records) {
                    $this->to_connection->table($tableName)->insert($records);
                    $this->records_inserted += count($records);
                }
            });

            $_this->table_empty = false;
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            print_r($e->getMessage());
        }
    }

    /**
     * Insert to
     *
     * @param $values
     */
    private function insert($values) {
        try {
            $this->to_connection->table($this->table_to)->insert($values);
            $this->records_inserted += count($values);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            print_r($e->getMessage());
        }
    }

    /**
     * Insert to
     *
     * @param $values
     */
    private function insertToTable($values, $tableName) {
        try {
            $this->to_connection->table($tableName)->insert($values);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            print_r($e->getMessage());
        }
    }

    private function determineProcedureCall($scraper_name)
    {
        switch ($scraper_name) {
            case self::SCRAPER_CMS:
                // fill the method for the job we will define
                $this->dwh_connection->statement('call only_cms_to_bo_control_sp(721, 73, 298, @err)');
                $this->logger->info(['Procedure', 'call only_cms_to_bo_control_sp(721, 73, 298, @err)']);
                break;
            case self::SCRAPER_OUT:
                // fill the method for the job we will define
                $this->dwh_connection->statement('call only_out_to_bo_control_sp(722, 73, 299, @err)');
                $this->logger->info(['Procedure', 'call only_out_to_bo_control_sp(722, 73, 299, @err)']);
                break;

            default:
                $this->logger->warning('Could not find stored procedure for ' . $scraper_name);
                break;
        }
    }

}
