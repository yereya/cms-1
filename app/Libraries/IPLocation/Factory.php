<?php namespace App\Libraries\IPLocation;

use App\Entities\Models\GeoLocation\City;
use App\Entities\Models\GeoLocation\CityBlock;
use App\Entities\Models\GeoLocation\Country;
use App\Entities\Models\GeoLocation\CountryBlock;

class Factory
{
    /**
     * @var string $ip
     */
    private $ip;

    /**
     * Factory constructor.
     *
     * @param string $ip
     */
    public function __construct($ip = null)
    {
        $this->setIP($ip ?: request()->ip());
    }

    /**
     * Set IP
     *
     * @param string $ip
     */
    public function setIP($ip)
    {
        $this->ip = $ip;
    }

    /**
     * Get Country
     *
     * @return Country|null
     */
    public function getCountry()
    {
        if ($block = $this->getCountryBlock()) {
            return $block->country;
        }

        return null;
    }

    /**
     * Get Country Block
     *
     * @return CountryBlock
     */
    private function getCountryBlock()
    {
        $ip = ip2long($this->ip);

        return CountryBlock::where('range_start', '<=', $ip)->where('range_end', '>=', $ip)->with('country')->first();
    }

    /**
     * Get City
     *
     * @return City|null
     */
    public function getCity()
    {
        if ($block = $this->getCityBlock()) {
            return $block->city;
        }

        return null;
    }

    /**
     * Get City Block
     *
     * @return CityBlock
     */
    private function getCityBlock()
    {
        $ip = ip2long($this->ip);

        return CityBlock::where('range_start', '<=', $ip)->where('range_end', '>=', $ip)->with('city')->first();
    }
}