<?php namespace App\Libraries;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Cookie\CookieJar;

/**
 * Class Fetcher
 *
 * @package App\Console\Commands\ReportsScraper\Providers
 */
class Guzzle
{

    /**
     * Used to save the response array
     *
     * @var $response
     */
    protected $response = [];
    protected $response_headers = [];
    protected $request_headers = [];
    protected $status_code = 500;

    /**
     * Will hold the index of the last response index id
     */
    protected $last_reponse_idx;

    /**
     * Guzzle object
     *
     * @var $guzzle
     */
    protected $guzzle;

    protected $cookie_jar;
    protected $error_msgs = null;


    public function __construct()
    {
        //Sets the number of the call
        $this->call_number = 0;

        $this->response = [];

        $this->cookie_jar = new CookieJar();
        $this->guzzle     = new GuzzleClient(['cookies' => true]);
    }


    public function fetch($url, $params = [], $method = 'GET', $request_idx = 0)
    {
        //Documentation: http://docs.guzzlephp.org/en/latest/request-options.html
        $params = filterAndSetParams([
            'cookies'         => $this->cookie_jar,
            'connect_timeout' => 40,
            // Seconds
            'allow_redirects' => [
                'max' => 10,
            ],
            'auth'            => null,
            // ['username', 'password']
            'body'            => null,
            /*
            * when debug mode is true - it break the publish All function.
            * - warning message:
             * curl_setopt_array cannot represent a stream of type Output
           */
            'debug'           => false,
            '_conditional'    => [],
//in develop
//            'debug' => true,
            'headers'         => null,
            // Associative array of headers to add to the request. Each key is the name of a header, and each value is a string or array of strings representing the header field values. exp: ['Accept-Encoding' => 'gzip']
            'query'           => null,
            // Associative array of query string values or query string to add to the request.
            'decode_content'  => true,
            // Specify whether or not Content-Encoding responses (gzip, deflate, etc.) are automatically decoded
            'delay'           => true,
            // The number of milliseconds to delay before sending the request.
            'form_params'     => null,
            //Used to send an application/x-www-form-urlencoded POST request. exp: ['foo' => 'bar']
            'http_errors'     => empty($params['http_errors']) ? true : false,
            //Set to false to disable throwing exceptions on an HTTP protocol errors (i.e., 4xx and 5xx responses). Exceptions are thrown by default when HTTP protocol errors are encountered.
            'json'            => null,
            // The json option is used to easily upload JSON encoded data as the body of a request. A Content-Type header of application/json will be added if no Content-Type header is already present on the message.
            'sink'            => null,
            // Specify where the body of a response will be saved. (save_to deprecated)
            'version'         => '1.1',
            // Protocol version to use with the request.
            'verify'          => true,
            // Describes the SSL certificate verification behavior of a request:
//            in develop
//            'verify'  => false, // Describes the SSL certificate verification behavior of a request:
            // true - to enable SSL certificate verification and use the default CA bundle provided by operating system
            // false - to disable certificate verification (this is insecure!)
            // string - to provide the path to a CA bundle to enable verification using a custom certificate
            'curl'            => [
                CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1
                // set preferred TLS/SSL version, more info: https://curl.haxx.se/libcurl/c/CURLOPT_SSLVERSION.html
            ],
        ], $params);


        ob_start();
        try {
            $response = $this->guzzle->request($method, $url, $params);
            $this->parseRequestHeaders($request_idx, ob_get_contents());
            $this->saveResponse($response, $request_idx);
            $this->setStatusCode($response->getStatusCode());
        } catch (\Exception $e) {
            if (!empty($e->getCode())) {
                $this->setStatusCode($e->getCode());
            }

            $this->error_msgs[] = $e->getMessage();
        }
        ob_clean();

        return $this;
    }

    /**
     * Set response status code
     *
     * @param $code
     */
    private function setStatusCode($code)
    {
        $this->status_code = $code;
    }

    /**
     * Get response status code
     *
     * @return int
     */
    public function getStatusCode()
    {
        return $this->status_code;
    }

    /**
     * Returns the errors saved in the global var
     *
     * @param $clear
     *
     * @return null
     */
    public function getErrorMsgs($clear = false)
    {
        $_errors = $this->error_msgs;
        if ($clear) {
            $this->error_msgs = null;
        }

        return $_errors;
    }


    public function getRequestHeaders($response_idx = null)
    {
        if (is_null($response_idx) && is_array($this->response_headers) && !is_null($this->last_reponse_idx)) {
            return $this->request_headers[$this->last_reponse_idx];
        } elseif (isset($this->response[$response_idx])) {
            return $this->request_headers[$response_idx];
        }

        return null;
    }


    /**
     * Returns the raw body of the request
     * if not response index were passed, that last response saved will be returned
     *
     * @param int $response_idx
     *
     * @return mixed
     */
    public function getBody($response_idx = null, $response_recurrence_idx = 0)
    {
        if (is_null($response_idx) && is_array($this->response_headers) && !is_null($this->last_reponse_idx)) {
            return $this->response[$this->last_reponse_idx];
        } elseif (isset($this->response[$response_idx])) {
            return $this->response[$response_idx];
        }

        return;
    }


    public function emptyBody($response_idx, $response_recurrence_idx = 0)
    {
        if (isset($this->response[$response_idx])) {
            if (empty($this->response[$response_idx][$response_recurrence_idx])) {
                return true;
            }
        }

        return false;
    }


    /**
     * Returns the header of requested response
     * if not response index were passed, that last response saved will be returned
     *
     * @param int $response_idx
     *
     * @return mixed
     */
    public function getResponseHeaders($response_idx = null)
    {
        if (is_null($response_idx) && is_array($this->response_headers) && !is_null($this->last_reponse_idx)) {
            return $this->response_headers[$this->last_reponse_idx];
        } elseif (isset($this->response[$response_idx])) {
            return $this->response_headers[$response_idx];
        }

        return false;
    }


    /**
     * Returns the last_response_idx variable which is used to find the last call result
     * for cases when the request_idx was not passed.
     *
     * @return mixed
     */
    public function getLastResponseIdx()
    {
        return $this->last_reponse_idx;
    }


    /**
     * Resets the history of the guzzle responses
     */
    public function resetResponsesHistory()
    {
        $this->response         = [];
        $this->response_headers = [];
        $this->last_reponse_idx = null;
        $this->cookie_jar       = new CookieJar();
    }


    /**
     * Saves the response to the to our global response object
     * and updates the last_response_idx variable which is used to find the last call result
     * for cases when the request_idx was not passed.
     *
     * @param $response
     * @param $request_idx
     */
    private function saveResponse($response, $request_idx)
    {
        // Create new request index if none were passed
        if (is_null($request_idx)) {
            $this->response[]         = $response->getBody()->getContents();
            $this->response_headers[] = $response->getHeaders();
//            $this->request_headers[] = $response->getReasonPhrase();
        } else {
            $this->response[$request_idx]         = $response->getBody()->getContents();
            $this->response_headers[$request_idx] = $response->getHeaders();
//            $this->request_headers[$request_idx] = $response->getReasonPhrase();
        }

        $this->last_reponse_idx = endKey($this->response);
    }


    /**
     * The debug log is returned a plain text
     * This method breaks it into an array per line so that it will better visible
     *
     * @param $request_idx
     */
    private function parseRequestHeaders($request_idx, $request_headers)
    {
        $this->request_headers[$request_idx] = explode("\n", $request_headers);
    }

    /**
     * @return CookieJar
     */
    public function getCookieJar(): CookieJar
    {
        return $this->cookie_jar;
    }

    /**
     * @param null $idx
     * @param      $key
     * @param      $val
     */
    public function setResponseHeader($idx = null, $key, $val): void
    {
        $response_index = $idx ?? $this->last_reponse_idx;

        /*Enter only if key exist */
        if (!empty($this->response_headers[$response_index][$key])) {
            $existence_state = array_filter($this->response_headers[$response_index][$key], function ($v) use ($val) {
                /*if there's already value like this*/
                return strpos($v, $val) !== false;
            });

            /*if val not exist add it to the response */
            if (!$existence_state) {
                $this->response_headers[$response_index][$key][] = $val;
            }

            return;
        }

        $this->response_headers[$response_index][$key]   = [];
        $this->response_headers[$response_index][$key][] = $val;
    }


    public function resetCookies(): void
    {
        $this->response_headers[$this->last_reponse_idx]['Set-Cookie'] = [];
    }
}