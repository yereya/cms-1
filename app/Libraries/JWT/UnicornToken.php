<?php namespace App\Libraries\JWT;

class UnicornToken extends Token
{
    /**
     * UnicornToken constructor.
     * @param $user
     */
    public function __construct ($user)
    {
        $configParams = config('services.unicorn');
        $this->secret = $configParams['jwt-secret'];
        $this->hoursForExpiration = $configParams['hours-for-token-expiration'];
        $this->subject = $user->email;
        parent::__construct();
    }

    /**
     * getRedirectUrl
     *
     * @return string
     */
    public function getRedirectUrl ()
    {
        $token = $this->get();
        $url = config('services.unicorn.home-url') . "/#/?token=$token";

        return $url;
    }
}
