<?php namespace App\Libraries\JWT;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Carbon\Carbon;

class Token
{
    /**
     * @var string
     */
    protected $secret;

    /**
     * @var array
     */
    protected $customClaims = [];

    /**
     * @var $subject
     * The main subject of the
     */
    protected $subject = null;

    /**
     * @var int
     * set this variable for expiration time different for 4 hours
     * This will work only if $expirationTimeStamp is not set
     */
    protected $hoursForExpiration = 4;

    /**
     * @var null| unix timestamp
     * set this variable to create custom expiration time
     */
    protected $expirationTimeStamp = null;

    /**
     * Token constructor.
     */
    public function __construct()
    {
        $this->secret = $this->secret ? $this->secret : config('jwt.secret');
    }

    /**
     * Get
     *
     * @return String
     */
    public function get()
    {
        JWTAuth::getJWTProvider()->setSecret($this->secret);
        $this->setCustomClaims();
        $payload = JWTFactory::make($this->customClaims);
        $token = JWTAuth::encode($payload)->get();

        return $token;
    }

    /**
     * setCustomClaims
     */
    private function setCustomClaims ()
    {
        $this->customClaims['sub'] = $this->subject;

        $this->customClaims['exp'] = $this->expirationTimeStamp
            ? $this->expirationTimeStamp
            :  Carbon::now()->addHours($this->hoursForExpiration)->timestamp;
    }
}
