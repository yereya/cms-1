<?php

namespace App\Libraries\JWT;

use App\Entities\Repositories\Users\AccessControl\PermissionRepo;
use App\Entities\Repositories\Users\UserRepo;

/**
 * @property string serviceName
 */
class ServicesToken extends Token
{
    /**
     * ServicesToken constructor.
     *
     * @param $user
     * @param $service_name
     */
    public function __construct($user, $service_name)
    {
        $this->service              = $service_name;
        $config_params              = config('services.' . $this->service);
        $this->secret               = $config_params['jwt-secret'];
        $this->hoursForExpiration   = $config_params['hours-for-token-expiration'];
        $this->subject              = $user->email;
        $this->customClaims['nbf']  = time() - 300;
        $this->customClaims['permissions']
                                    = (new PermissionRepo())->getAllServicePermissionsForUser($this->service);
        $this->customClaims['role'] = (new UserRepo())->getRoleByEmail($user->email);

        parent::__construct();
    }

    /**
     * getRedirectUrl
     *
     * @return string
     */
    public function getRedirectUrl()
    {
        $token = $this->get();
        $url   = config('services.' . $this->service . '.home-url') . "/#/?token=$token";

        return $url;
    }
}
