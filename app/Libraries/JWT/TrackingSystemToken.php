<?php

namespace App\Libraries\JWT;


class TrackingSystemToken extends Token
{
    /**
     * TrackingSystemToken constructor.
     */
    public function __construct()
    {
        $this->createToken();

        parent::__construct();
    }

    /**
     * Create token
     */
    private function createToken() {
        $this->secret = config('services.tracking_system.jwt-secret');
        $this->hoursForExpiration = config('services.tracking_system.hours-for-token-expiration');
        $this->customClaims = $this->getUser();
    }

    /**
     * Get user params
     *
     * @return array
     */
    private function getUser() {
        $user = auth()->user();

        return ['email' => $user->email, 'username' => $user->username];
    }
}