<?php

namespace App\Http\Middleware;

use App\Entities\Models\CMSUserActions;
use App\Entities\Models\Users\User;
use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class Authenticate
 * @package App\Http\Middleware
 */
class Authenticate
{
    /**
     * The Guard implementation.
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Validate the Ip permission for users 
        // located in table "users" column "permitted_ips"
        if (!$this->validateUserIp()) {
            Auth::logout();
            return redirect()->guest(route('login'));
        }

        // Check if the session came up successfully
        if (session('user_account_access') === null){
            Auth::logout();
            return redirect()->guest(route('login'));
        }

        if (auth()->check() && !$this->checkPasswordExpiry()) {
            Auth::logout();
            return redirect()->guest(route('login'));
        }

        // Make sure user is logged in and block ajax calls
        if ($this->auth->guest()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest(route('login'));
            }
        }
        return $next($request);
    }


    /**
     * Validate the user IP
     * @return bool
     */
    private function validateUserIp()
    {
        $permitted_ips = [];

        if (env('APP_ENV') == 'local') {
            return true;
        }

        $user_ip = request()->ip();
        $office_ips = collect(config('auth.permitted_ips'));

        $office_ips = $this->generateSpecificIpInCaseOfAstrixCharacter($office_ips, $user_ip);

        if (auth()->check()) {
            $permitted_ips = explode(',', auth()->user()->permitted_ips);
        }

        // Take the base permitted ips from the auth config file
        $permitted_ips = array_merge($office_ips, $permitted_ips);

        foreach ($permitted_ips as $ips) {
            if ($ips === '*') {
                return true;
            }

            if ($ips === $user_ip) {
                return true;
            }
        }

        return false;
    }

    /**
     * Makes that the users' password did not expire yet
     *
     * @return bool
     */
    private function checkPasswordExpiry()
    {
        return strtotime(auth()->user()->password_expiry_date) >= time();
    }


    /**
     *
     *  In case we pass astrix in office ip:
     *  (it mean it could be the all numbers inside specific range = example : 192.1.1.* => * => means 1-255):
     *
     *  1. check if the office ips have astrix in them.
     *  2. replace the specific placement with the user ip (at the same place)
     *
     * @param $office_ips
     * @param $user_ip
     * @return mixed
     */
    public function generateSpecificIpInCaseOfAstrixCharacter($office_ips = null, $user_ip= null)
    {
        $office_ips = $office_ips ?? collect(config('auth.permitted_ips'));
        $user_ip = $user_ip ?? request()->ip();

        return $office_ips->transform(
            function (&$ip) use ($user_ip) {
                $new_ip = null;
                if (strpos($ip, '.*') !== false) {
                    $user_ip_quartets = collect(explode('.', $user_ip));
                    $office_ip_quartets = collect(explode('.', $ip));

                    $office_ip_quartets->transform(function ($office_quartet, $idx) use ($user_ip_quartets) {
                        return ($office_quartet == '*') ? $user_ip_quartets[$idx] : $office_quartet;
                    });

                    $ip = $office_ip_quartets->implode('.');
                }

                return $ip;
            })->toArray();
    }

    public function isUserPermitted(){
        return $this->validateUserIp();
    }
}
