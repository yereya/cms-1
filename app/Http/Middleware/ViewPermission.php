<?php namespace App\Http\Middleware;

use App\Entities\Models\CMSUserActions;
use Closure;
use App\Http\Controllers;
use Illuminate\Support\Facades\Auth;

/**
 * Class ViewPermission
 *
 * @package App\Http\Middleware
 */
class ViewPermission
{
    /**
     * @var array $exception_permission
     */
    private $exception_permission = ['dashboard', 'entity_lock'];

    /**
     * actions not needle permissions
     *
     * @var array $executable_actions
     */
    private $executable_actions = [
        'showModal'
    ];

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $controller_name = explode('@', $request->route()->getActionName());

        if($controller_name[0] == 'App\Http\Controllers\DashboardController' && $controller_name[1] == 'index' ){
            CMSUserActions::create([
                "user_id" => auth()->user()->getAuthIdentifier(),
                "user_name" => auth()->user()->username,
                "cms_users_action_id" => "2",
            ]);
        }

        /** check if action need permissions name */
        if (isset($controller_name[1]) && in_array($controller_name[1], $this->executable_actions)) {
            return $next($request);
        }

        /** if controller not have permission name - redirect */
        if (!defined($controller_name[0] . '::PERMISSION')) {
            return $next($request);
        }

        /** get permission name from class */
        $permission = constant($controller_name[0] . '::PERMISSION');

        /** if user have permission or this permission in exception array - redirect */
        if (in_array($permission, $this->exception_permission) || Auth::user()->can($permission . '.view')) {
            return $next($request);
        }

        /** ERRORS */
        /** if ajax return ajax connection to controller */
        if ($request->ajax()) {
            return [
                'status'  => 'error',
                "warning" => "You are not permitted to view this page"
            ];
        } /** if not ajax return to back page and show message */ else {
            return redirect()->back()->with('toastr_msgs', ["warning" => "You are not permitted to this action"]);
        }
    }
}