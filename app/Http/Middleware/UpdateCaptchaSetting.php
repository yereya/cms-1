<?php

namespace App\Http\Middleware;

use Closure;

class UpdateCaptchaSetting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        config([
            'recaptcha.public_key' => getSiteSetting('recaptcha_public_key'),
            'recaptcha.private_key' => getSiteSetting('recaptcha_secret_key')
        ]);

        return $next($request);
    }
}
