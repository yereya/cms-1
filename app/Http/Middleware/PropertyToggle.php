<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PropertyToggle
{
    //TODO create a validation level for the propertyToggle feature
    private $exception_permission = array('dashboard');

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     * @throws \Exception
     */
    public function handle($request, Closure $next)
    {
        $controller_name = explode('@', $request->route()->getActionName());
        $class = new $controller_name[0]();

        /** if controller not have permission name - redirect */
        if (!isset($class->permission_name)) {
            return $next($request);
        }

        /** get permission name from class */
        $permission = $class->permission_name;

        /** if user have permission or this permission in exception array - redirect */
        if (in_array($permission, $this->exception_permission) ||
            $this->hasPermission($permission)
        ) {
            return $next($request);
        }

        /** ERRORS */
        /** if ajax return ajax connection to controller */
        if ($request->ajax()) {
            return [
                'status' => 'error',
                "warning" => "You are not permitted to view this page"
            ];
        } /** if not ajax return to back page and show message */
        else {
            return redirect()->back()
                ->with('toastr_msgs', ["warning" => "You are not permitted to this action"]);
        }
    }

    private function hasPermission($permission)
    {
        return Auth::user()->can($permission . '.view');
    }
}
