<?php


namespace App\Http\Middleware;


use Closure;

class DashboardAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Closure                   $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*Dashboards list*/
        $permissions = [
            'daily-stats'         => 'dashboards.daily-stats.view',
            'advertisers-targets' => 'dashboards.advertisers-targets.view',
        ];

        foreach ($permissions as $key => $permission) {
            if (auth()->user()->can($permission)) {

                return redirect()
                    ->route('dashboards.'.$key);
            }
        }

        return $next($request);
    }
}