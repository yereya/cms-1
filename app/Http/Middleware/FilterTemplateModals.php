<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Sites\Widgets\SiteWidgetsDataController;
use Closure;
use Illuminate\Http\Request;

class FilterTemplateModals
{
    /**
     * @param Request $request
     * @param Closure $next
     *
     * @return \App\Http\Traits\view|array
     */
    public function handle(Request $request, Closure $next)
    {
        if (strpos(request()->input('method'), 'widget') !== false) {
            $app        = app();
            $controller = $app->make(SiteWidgetsDataController::class);
            return $controller->ajaxModal($request);
        }
        return $next($request);
    }
}