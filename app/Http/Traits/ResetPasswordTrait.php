<?php


namespace app\Http\Traits;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

trait ResetPasswordTrait
{
    use RedirectsUsers;


    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string|null $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        $action_url = route('reset_password');
        $token = $token ?? '';
        $email = $request->email ?? '';
        $product_name = 'CMS';
        $product_address_line1 = '';
        $product_address_line2 = '';

        $sender_name = '';


        return view('auth.passwords.reset')->with(
            compact('action_url', 'token', 'email', 'product_name', 'product_address_line1', 'product_address_line2', 'sender_name')
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
            $this->resetPassword($user, $password);
            $this->updateExpiryDate($user);
        }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($response)
            : $this->sendResetFailedResponse($request, $response);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword $user
     * @param  string $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $user->forceFill([
            'password' => $password,
            'remember_token' => Str::random(60),
        ]);

        $user->save();

        $this->guard()->login($user);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  string $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetResponse($response)
    {
        return redirect()
            ->route('login')
            ->send()
            ->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request
     * @param  string $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return redirect()->back()
            ->withInput($request->only('email'))
            ->withErrors(['email' => trans($response)]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker();
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Update user expiry Date according to auth settings (auth.passwords.expiry_dates)
     * @param $user
     * @return mixed
     */
    protected function updateExpiryDate($user)
    {
        $date = Carbon::now();

        /*Reset password manually before expiry date comes*/
        $date_is_before_expiry = date('Y-m-d',strtotime($date)) < $user->password_expiry_date;

        $quarter = $date->quarter > 3 ? 1 : $date->quarter + 1;
        $year = $quarter == 1 ? ($date->addYear())->year : $date->year;

        if($date_is_before_expiry) {
            $quarter = $date->quarter;
            $year =  $date->year;
        }

        $expiry_periods = config('auth.passwords.users.expiry_dates');
        $expiry_date = $year . '-' . $expiry_periods[$quarter];

        /*update expiry Date according to hard coded dates*/
        $user->password_expiry_date = $expiry_date;

        return $user->save();
    }


    /**
     * Is User Password expire ?
     *
     * @param $user
     * @return bool
     */
    protected function isExpired($user)
    {
        return strtotime($user->password_expiry_date) < time();

    }

    /**
     * Is User Password about to be expired ? (according to auth.password.notification settings)
     *
     * @param $user
     * @return bool
     */
    protected function isAboutToBeExpired($user)
    {
        $notification_period = config('auth.passwords.users.notification_period');

        return strtotime($user->password_expiry_date) < strtotime('+ ' . $notification_period . ' days');
    }
}