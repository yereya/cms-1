<?php

namespace App\Http\Traits;

use App\Entities\Models\CMSUserActions;
use App\Entities\Models\Reports\Queries\ReportsQueryField;
use App\Entities\Models\Reports\Query;
use App\Entities\Models\Users\AccessControl\AssignedAccount;
use App\Libraries\ReportQueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use SplFileObject;
use Illuminate\Support\Facades\DB;

/**
 * Class DatatableFilterTrait
 *
 * @package App\Http\Traits
 */
trait DatatableFilterTrait
{
    /**
     * @var null
     */
    protected $query_id = null;

    /**
     * @var
     */
    protected $save_file_name;

    /**
     * @var array
     */
    protected $query_results = [];

    protected $unlimited_download_permission = 'reports.unlimited_download.download';
    /**
     * @var array
     */
    private $group_by_unique = [
        'date_format(stats_date_tz,\'%Y%V\')',
        'date_format(stats_date_tz,\'%Y%m\')',
        'stats_date_tz',
        'date_format(timestamp_tz,\'%Y%m\')'
    ];

    /**
     * @param Request $request
     * @param         $report_id
     *
     * @return mixed|string
     * @throws \Exception
     */
    public function filter(Request $request, $report_id)
    {
        $this->getReportParams(Query::find($report_id));
        $form = $request->all();

        if (is_null($form) || empty($form)) {
            throw new \Exception('Field form not found');
        }
        if (in_array($report_id, array('1','3','27'))
            && session('last_download_report_' . $report_id . '_time' )
            && ((strtotime('now') - strtotime(session('last_download_report_' . $report_id . '_time' ))) / 60 < $this->report_download_time_limit) // Check the time that passed since the last download request
            && !auth()->user()->can($this->unlimited_download_permission)) // Allow users to skip this limitation
        {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You have to wait 5 minutes before running the same report again."]);
        }
        $this->parseQueryName($form, $this->query_id); // query_id is being resolved
        /** parse all data to array $query_results */
        $this->convertFilterData($form);

        /** insert current user id */
        $this->addPermissionsUserId();

        /** get query and insert parameters $query */
        $query = $this->getQuery();

        $started = microtime(true);
        /** get results from db */
        $results = $this->connectToDb($query);

        /** Update last run time */
        session(['last_download_report_'. $report_id .'_time' => date('Y-m-d H:i:s')]);

        /** get the results summary**/
        if (isset($this->query_results['summary']) && !empty($this->query_results['summary'])) {

            /** build the summary query**/
            $summary_query = ReportQueryBuilder::buildSummaryQuery($query, $this->query_results['summary']);

            /** run query**/


            /** run query**/
            $summary_results = $this->connectToDb($summary_query);
            if (isset($summary_results) && $summary_results['status']) {
                /** convert the summary values according to their required type which
                 * is defined in the relevant report controller
                 **/
                $results['summary'] = $this->convert($summary_results)['data'][0];
            }
        }

        $end = microtime(true);
        $runTime = $end - $started;
        if (isset($form['csv']) && $form['csv'] && isset($results['data'])) {
            CMSUserActions::create([
                "user_id" => auth()->user()->getAuthIdentifier(),
                "user_name" => auth()->user()->username,
                "cms_users_action_id" => "3",
                "report_date_from" => $form["date_from"],
                "report_date_to" =>  $form["date_to"],
                "report_id" => $report_id,
                "report_name" => $this->report_name,
                "run_duration" => $runTime,
                "sql_query" => $query
            ]);
            $this->save_file_name = $this->dtFilter_getFileName($form);
            return $this->buildCsv($results['data']);
        }

        if (app()->bound('debugbar')) {
            \Debugbar::addMessage($query);
        }
        //id, user_id, user_name, cms_users_action_id, report_id, report_name, run_duration, sql_query, created_at, updated_at

        CMSUserActions::create([
            "user_id" => auth()->user()->getAuthIdentifier(),
            "user_name" => auth()->user()->username,
            "cms_users_action_id" => "4",
            "report_date_from" => $form["date_from"],
            "report_date_to" =>  $form["date_to"],
            "report_id" => $report_id,
            "report_name" => $this->report_name,
            "run_duration" => $runTime,
            "sql_query" => $query
        ]);
        $results['tooltips'] = ReportQueryBuilder::getTooltipLabels($this->query_id);
        $results['sort']     = $this->buildSort();
        return $this->convert($results);
    }

    /**
     * @return array
     */
    private function buildSort()
    {
        $sort = [];

        if (array_key_exists('order_by', $this->query_results)) {

            foreach ($this->query_results['order_by'] as $order) {

                $sort[str_replace('`', '', $order['column'])] = strtolower($order['order']);
            }

        }


        return $sort;
    }

    /**
     * Checked if need convert data
     * if in parent class not method $datatable_convertData return query result only
     * $datatable_convertData - not Required
     *
     * @param $results
     *
     * @return mixed
     */
    protected function convert($results)
    {
        if (method_exists($this, 'datatable_convertData')) {
            $method_name = 'datatable_convertData';
            return $this->datatable_convertData($results);
        } else {
            return $results;
        }
    }


    /**
     * Builds the file name and return it
     *
     * @param array $form
     *
     * @return string
     */
    protected function dtFilter_getFileName($form)
    {
        $name_str = snakeToWords($this->report_name) . ' Report - ';

        if (isset($form['group_by'][0])) {
            $name_str .= 'group_by: ' . preg_replace('/[^a-zA-Z0-9-_]/', '', $form['group_by'][0]) . ' - ';
        }

        // Add the date range to the file name
        if (isset($form['date_from']) && $form['date_to']) {
            $name_str .= $form['date_from'] . '-' . $form['date_to'];
        }

        return $name_str;
    }


    /**
     * Convert Filter Data
     *
     * @param $form
     */
    private function convertFilterData(&$form)
    {
        if (isset($form['where'])) {
            $form['where'] = $this->queryWhere($form['where']);
        }

        if (isset($form['group_by'])) {
            $form['group_by'] = $this->queryGroupBy($form['group_by']);
        }

        if (isset($form['order_by'])) {
            $form['order_by'] = $this->queryOrderBy($form['order_by']);
        }

        if (isset($form['having'])) {
            $form['having'] = $this->queryWhere($form['having']);
        }

        if (isset($form['date_range'])) {
            $form['date_from'] = $this->queryRange($form['date_range'], 0);
            $form['date_to']   = $this->queryRange($form['date_range'], 1);
            unset($form['date_range']); //remove from form
            $this->query_fields[] = 'date_from'; //add to keys
            $this->query_fields[] = 'date_to'; //add to keys
            $position_to_remove   = array_search('date_range', $this->query_fields); //remove from keys
            unset($this->query_fields[$position_to_remove]);
        }
        $this->parseResults($form, $this->query_fields);

        if (in_array('select', $this->query_fields)) {
            $this->querySelect($form, $this->query_results);
            $this->addDefaultSelect($form, $this->query_results);
            //add the relevant summary fields
            $this->addSummaryFields($form, $this->query_results);
        }
    }

    /**
     * find and return query
     *
     * @return mixed
     * @throws \Exception
     */
    private function getQuery()
    {
        if (!$this->query_id) {
            throw new \Exception('Query name not found in request');
        }

        $row = Query::where('id', $this->query_id)->first();

        if (!$row->query) {
            throw new \Exception('Query name not found in database');
        }

        $query = $row->query;

        foreach ($this->query_fields as $field) {
            $insert[] = $field;
            if ($field == 'user_advertiser_access' && !is_null(session('user_advertiser_access'))) {
                $this->insertData($query, $field, implode(',', session('user_advertiser_access')));
            } else {
                if ($field == 'user_publisher_access' && !is_null(session('user_publisher_access'))) {
                    $this->insertData($query, $field, implode(',', session('user_publisher_access')));
                } else {
                    if ($field == 'user_account_access' && !is_null(session('user_account_access'))) {
                        $this->insertData($query, $field, implode(',', session('user_account_access')));
                    } else {
                        if ($field == 'user_source_account_access' && !is_null(session('user_source_account_access'))) {
                            $this->insertData($query, $field, implode(',', session('user_source_account_access')));
                        } else {
                            $this->insertData($query, $field, $this->query_results[$field]);
                        }
                    }
                }
            }
        }

        // Clear query
        $query = preg_replace('/\s+/S', " ", $query);
        return $query;
    }

    /**
     * Insert search data to query
     *
     * @param $query
     * @param $selector
     * @param $data
     */
    private function insertData(&$query, $selector, $data)
    {
        switch ($selector) {
            case 'having':
                if (empty($data)) {
                    $query = str_replace("[$selector]", '', $query);
                } else {
                    $query = str_replace("[$selector]", 'HAVING ' . $data, $query);
                }
                break;
            case 'where':
                if (empty($data)) {
                    $query = str_replace("[$selector]", '', $query);
                } else {
                    $query = str_replace("[$selector]", ' AND ' . $data, $query);
                }
                break;
            case 'select':
                $data = implode(',', $data);

                $query = str_replace("[$selector]", $data, $query);
            case 'date_from':
            case 'date_to':
                if (is_array($data)) {
                    $data = implode($data);
                }
                $query = str_replace("[$selector]", $data, $query);
                break;
            case 'group_by':
                $str = implode(', ', $data);
                if (strlen($str) > 1) {
                    $str = "GROUP BY " . $str;
                }
                $query = str_replace("[$selector]", $str, $query);
                break;
            case 'order_by':
                $str = implode(', ', $this->getOrderBy($data));
                if (strlen($str) > 1) {
                    $str = "ORDER BY " . $str;
                }
                $query = str_replace("[$selector]", $str, $query);
                break;
            case 'user_id':
            case 'user_advertiser_access':
            case 'user_publisher_access':
            case 'user_account_access':
            case 'user_source_account_access':
                $query = str_replace("[$selector]", $data, $query);
                break;
            case 'user_assigned_accounts':
                $user_assigned_accounts_list = AssignedAccount::where('user_id', Auth::user()->id)->pluck('account_id');
                $query                       = str_replace('[user_assigned_accounts]', $user_assigned_accounts_list,
                    $query);
                break;
        }
    }

    /**
     * create order by in format sql
     *
     * @param $data
     *
     * @return array
     */
    private function getOrderBy($data)
    {
        $result = [];

        foreach ($data as $d) {
            $result[] = implode(' ', $d);
        }

        return $result;
    }

    /**
     * create where in format sql
     *
     * @param $data
     *
     * @return array
     */
    private function getWhere($data)
    {
        $result = [];

        foreach ($data as $d) {
            $result[] = implode(' ', $d);
        }

        return $result;
    }

    /**
     * connect to db and run query
     *
     * @param $query
     *
     * @return string
     */
    private function connectToDb($query)
    {

        try {

            $connection = DB::connection($this->query_db_connect);

            # ----- Change Sessions Isolation Level ----
            $result = $connection->select("SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");


            $result = $connection->select($query);

            /*normalize results that came from db that is not utf8*/
            $result = convertObjectToUtf8($result);

            $connection->disconnect($this->query_db_connect);
        } catch (\Exception $e) {
            if (app()->bound('debugbar')) {
                \Debugbar::addMessage($e->getMessage());
            }
            return ['status' => 0, 'message' => "Query Error. Show more info in Debugbar. \n\n {$e->getMessage()}"];
        }
        return ['status' => 1, 'data' => $result];
    }

    /**
     * find query name
     *
     * @param $request
     * @param $name
     *
     * @return mixed
     */
    private function parseQueryName($request, &$name)
    {
        if (!isset($request['query_id']) || empty($request['query_id'])) {
            return $name;
        }

        return $name = $request['query_id'];
    }

    /**
     * parse this string by default params
     *
     * @param $request
     * @param $keywords
     */
    private function parseResults($request, $keywords)
    {
        foreach ($keywords as $keyword) {
            $this->findByKeyword($request, $keyword, $this->query_results);
        }
    }

    /**
     * Function Helper to parse by specific params
     *
     * @param $request
     * @param $keyword
     * @param $result
     *
     * @return array
     */
    private function findByKeyword($request, $keyword, &$result)
    {
        if (!isset($request[$keyword]) || empty($request[$keyword])) {
            $result[$keyword] = [];
        } else {
            $result[$keyword] = $request[$keyword];
        }
    }

    /**
     * @param $request
     * @param $result
     *
     * @return string
     */
    private function querySelect($request, &$result)
    {
        $select = [];

        if (isset($request['group_by'])) {
            foreach ($request['group_by'] as $group_by_item) {
                $group_by_item = str_replace('`', '', $group_by_item);

                if (!in_array($group_by_item, $select)) {
                    if (in_array($group_by_item, $this->group_by_unique)) {
                        $field         = ReportQueryBuilder::getByFieldName($group_by_item, $this->query_id);
                        $group_by_item = "$group_by_item as `$field->display_name`";
                    }
                    $select[] = $group_by_item;
                }
            }
        }

        return $result['select'] = $select;
    }

    /**
     * Add default select to query
     *
     * @param $request
     * @param $result
     */
    private function addDefaultSelect($request, &$result)
    {
        $select = [];

        if (isset($request['select'])) {
            foreach ($request['select'] as $sel) {
                if (isset($sel)) {
                    $select[] = ReportQueryBuilder::getFieldName($sel);
                }
            }
        }
        $result['select'] = array_merge($result['select'], $select);
    }

    /**
     * create array wheres from request
     *
     * @param $order
     *
     * @return array
     */
    private function queryOrderBy($order)
    {
        if (!isset($order['key']) || !isset($order['value'])) {
            return [];
        }
        $min     = min(count($order['key']), count($order['value']));
        $results = [];
        for ($i = 0; $i < $min; $i++) {
            $results[] = [
                'column' => "`" . ReportQueryBuilder::getFieldName($order['key'][$i]) . "`",
                'order'  => $order['value'][$i]
            ];
        }

        return $results;
    }

    /**
     * create group by
     *
     * @param $group
     *
     * @return array
     */
    private function queryGroupBy($group)
    {
        $result = [];
        if (!isset($group) || count($group) == 0) {
            return $result;
        }

        foreach ($group as $value) {
            $title = ReportQueryBuilder::getFieldName($value);
            if (!in_array($title, $this->group_by_unique)) {
                $title = "`" . $title . "`";
            }
            $result[] = $title;
        }

        return $result;
    }

    /**
     * @param $value
     * @param $position
     *
     * @return mixed
     */
    private function queryRange($value, $position)
    {
        if (is_array($value) && isset($value['value'])) {
            $data = explode(' ', $value['value']);
        } else {
            $data = explode(' ', $value);
        }


        if ($data && isset($data[$position])) {
            return $data[$position];
        }
    }

    /**
     * create array order by from request
     *
     * @param $wheres
     *
     * @return array
     */
    private function queryWhere($wheres)
    {
        $converted = [];
        foreach ($wheres as $where) {
            if (isset($where['column']) && isset($where['operator']) && isset($where['value'])) {
                $where_name = ReportQueryBuilder::getFieldName($where['column']);
                $operator   = ReportQueryBuilder::operations()[$where['operator']];

                $or = [];
                foreach ($where['value'] as $value) {
                    if (strlen(trim($value)) > 0) {
                        $comparison_operator = [
                            '5', // operator "Like"
                            '6'  // operator "Not Like"
                        ];
                        if (in_array($where['operator'], $comparison_operator)) {
                            // for example if we have $where_name == 'account_id' and operator "like"
                            // we should search by secondary_field_name that is "account_name" instead of "account_id"
                            $where_name = ReportQueryBuilder::getFieldName($where['column'],
                                    'secondary_field_name') ?? $where_name;
                            $or[]       = $where_name . ' ' . $operator . ' \'%' . $value . '%\'';
                        } else {
                            $or[] = $where_name . ' ' . $operator . ' ' . (is_string($value) ? '"' . $value . '"' : $value);
                        }
                    }
                }

                if ($or) {
                    $converted[] = '(' . implode(' OR ', $or) . ')';
                }
            }
        }

        return implode(' AND ', $converted);
    }

    /**
     * add user id
     */
    private function addPermissionsUserId()
    {
        $this->query_results['user_id'] = 'user_id=' . Auth::user()->id;
    }

    /**
     * @param $results
     *
     * @return string
     */
    private function buildCsv($results)
    {
        $path = $this->getDownloadPath();
        $fp   = new SplFileObject(public_path() . DIRECTORY_SEPARATOR . $path, 'w');

        if ($results) {
            $keys = array_keys((array)$results[0]);
            $fp->fputcsv($keys);
        }

        foreach ($results as $res) {
            $data = (array)$res;
            $fp->fputcsv($data);
        }
        return asset($path);
    }

    /**
     * @return array|string
     */
    private function getDownloadPath()
    {
        $directory = 'export' . DIRECTORY_SEPARATOR . 'ReportCsv';
        if (!file_exists(public_path() . DIRECTORY_SEPARATOR . $directory)) {
            if (!mkdir(public_path() . DIRECTORY_SEPARATOR . $directory, 0777, true)) {
                return ['file' => 0, 'desc' => 'Cannot create folder'];
            }
        }


        if (file_put_contents(public_path() . DIRECTORY_SEPARATOR . $directory . DIRECTORY_SEPARATOR . $this->save_file_name . '.csv',
            '')) {
            return ['file' => 0, 'desc' => 'Cannot create file'];
        }
        return $directory . DIRECTORY_SEPARATOR . $this->save_file_name . '.csv';
    }

    /**
     * add the summary results to the global result
     *
     * @param $request
     * @param $result
     */
    private function addSummaryFields($request, &$result)
    {
        $summary = [];
        if (isset($request['select'])) {
            foreach ($request['select'] as $sel) {
                if (isset($sel)) {
                    $summary_field = ReportQueryBuilder::getSummaryFields($sel);
                    if (!is_null($summary_field['select'])) {
                        $summary[] = $summary_field;
                    }
                }
            }
        }
        $result['summary'] = $summary;
    }
}
