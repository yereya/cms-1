<?php namespace App\Http\Traits;

use App\Entities\Models\Sites\Section;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\SiteRepo;
use Request;
use Assets;
use Auth;
use Layout;
use Route;
use View;

/**
 * Class SetupLayout
 *
 * @package App\Http\Controllers\Traits
 */
trait SetupLayout
{
    /**
     * @var array $assets
     */
    protected $assets = [];

    /**
     * @var string $page_title
     */
    protected $page_title = '';

    /**
     * @var array $page_body_classes
     */
    protected $page_body_classes = [];

    /**
     * SetupLayout constructor.
     */
    public function __construct()
    {
        $this->setPageAssets();
        $this->setPageTitle();
        $this->setPageBodyClasses();
        $this->shareDefaultParams();
    }

    /**
     * Set Page Assets
     */
    private function setPageAssets()
    {
        // load global first
        Assets::add(['global']);

        foreach ((array) $this->assets as $asset) {
            Assets::add((string) $asset);
        }

        // Add the basic
        Assets::add(['theme']);
    }

    /**
     *  Sets default param values for the view
     */
    private function shareDefaultParams()
    {
        $this->shareCurrentSiteParams();

        $this->shareCurrentPermission();

        $this->shareSites();
    }

    /**
     * Set Page Title
     */
    private function setPageTitle()
    {
        $action = null;

        if (Route::currentRouteName()) {
            $action = last(explode('.', Route::currentRouteName()));
        } else if (Route::currentRouteAction()) {
            $action = last(explode('@', Route::currentRouteAction()));
        }

        $action = ucfirst(strtolower($action));
        
        Layout::setPageTitle($action ?: $this->page_title);
    }

    /**
     * Set Page Body Classes
     */
    private function setPageBodyClasses()
    {
        Layout::setBodyClass((array) $this->page_body_classes);
    }

    /**
     *  Share Current Site Params
     */
    private function shareCurrentSiteParams()
    {
        if (Route::getCurrentRoute() && !Request::ajax()) {
            $site = Route::getCurrentRoute()->getParameter('site') ?? config('app.cms_site_id');

            $current_site = is_object($site) ? $site : Site::find($site);
            View::share('current_site', $current_site);

            if (session('site.id') != $current_site->id) {
                session(['site.id' => $current_site->id, 'site.section_id' => null]);
            }

            if ($site !== config('app.cms_site_id')) {
                View::share('site_sections', Section::all());
            }
        }
    }

    /**
     *  Share Current Permission with view
     */
    private function shareCurrentPermission()
    {
        $action = explode('@', Route::currentRouteAction());

        if (defined($action[0] . '::PERMISSION')) {
            View::share('permission_name', constant($action[0] . '::PERMISSION'));
        }
    }

    /**
     * Share Sites
     */
    private function shareSites()
    {
        $sites = session('sites');
        if (!$sites) {
            insertSitesToSession();
            $sites = session('sites');
        }

        View::share('sites', $sites);
    }
}