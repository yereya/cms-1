<?php


namespace App\Http\Traits;


use App\Entities\Repositories\Bo\AccountsRepo;
use App\Libraries\Dashboards\DashboardFactory;
use DB;
use Illuminate\Http\Request;

/**
 * Class StatsTrait
 *
 * @package App\Http\Traits
 */
trait StatsTrait
{

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\Collection|mixed
     */
    public function stats(Request $request)
    {
        $widget = $request->input('widget');
        $factory = new DashboardFactory();
        $states  = $factory->make($widget, $request);
        $states->build();

        return $states->get();
    }
}