<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;

trait ChoiceTrait
{
    /**
     * Standardised method of working with server side Ajax alert
     *
     * @param Request $request
     * @return array|view
     */
    public function choice(Request $request)
    {
        try {
            $method_prefix = 'choice_';

            $result = [
                'status' => 0,
                'desc' => '',
                'data' => [],
            ];

            if ($request->has('method') && method_exists($this, $method_prefix . $request->get('method'))) {
                $method_name = $method_prefix . $request->get('method');
                return $this->{$method_name}($request);
            }

            $result['desc'] = 'Fatal: Getter method was not found';
        } catch (\Exception $e) {
            $result['desc'] = $e->getMessage();
        }


        return $result;
    }
}