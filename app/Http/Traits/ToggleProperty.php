<?php namespace App\Http\Traits;

use Illuminate\Http\Request;

trait ToggleProperty
{
    /**
     * Searches for the specific toggeling method and executing it if it exists
     *
     * @param Request $request
     * @return string
     */
    public function toggleProperty(Request $request) {
        if (method_exists($this, 'toggleCheckbox_'. $request->input('action'))) {
            return $this->{'toggleCheckbox_'. $request->input('action')}($request, func_get_args());
        }
        else {
            return [
                'status' => 0,
                'desc' => 'Seems like you are using the wrong action',
                'toggleState' => ''
            ];
        }
    }
}
