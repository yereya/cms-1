<?php

namespace App\Http\Traits;

use Creativeorange\Gravatar\Gravatar;

trait HasGravatar {
    private $fallbackUrl = 'https://secure.gravatar.com/avatar';

    /*
     * Gets the gravatar associated with the email property
     * */
    public function getGravatarUrlAttribute()
    {
        return (new Gravatar())->fallback($this->fallbackUrl)->get($this->email, ['size' => 50]);
    }
}