<?php namespace App\Http\Traits;

use App\Entities\Models\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

trait Select2Trait
{

    /**
     * Standardised method of working with server side select2
     * support where/limit/dependencies
     *
     * @param Request $request
     * @return array
     */
    public function select2(Request $request)
    {
        $result = [
            'status' => 0,
            'desc' => '',
            'data' => [],
        ];

        $params = $request->all();
        if (isset($params['method']) && method_exists($this, 'select2_'. $params['method'])) {
            
            $params['where'] = $this->buildWhereParams($params);

            try {
                $_method_name = "select2_".$params['method'];
                $query = $this->$_method_name($params, func_get_args());

                // Result can be 1 of 3 things
                if (is_a($query, 'Illuminate\Database\Eloquent\Builder')) {
                    return [
                        'status' => 1,
                        'data' => $this->select2_getRecords($params, $query)
                    ];
                }
                elseif (is_a($query, 'Illuminate\Database\Eloquent\Collection') || is_array($query)) {
                    return [
                        'status' => 1,
                        'data' => $query
                    ];
                }
                else {
                    $result['desc'] = 'No results returned';
                }
            }
            catch (\Exception $e) {
                $result['desc'] = $e->getMessage();
            }

        }
        elseif (method_exists($this, 'select2_getter')
            AND isset($params['search_field'])) {
            
            $params['where'] = $this->buildWhereParams($params);

            try {
                $query = $this->select2_getter($params, func_get_args());

                // Result can be 1 of 3 things
                if (is_a($query, 'Illuminate\Database\Eloquent\Builder')) {
                    return [
                        'status' => 1,
                        'data' => $this->select2_getRecords($params, $query)
                    ];
                }
                elseif (is_array($query)) {
                    return [
                        'status' => 1,
                        'data' => $query
                    ];
                }
                else {
                    $result['desc'] = 'No results returned';
                }
            }
            catch (\Exception $e) {
                $result['desc'] = $e->getMessage();
            }
        }
        else {
            $result['desc'] = 'Fatal: Getter method was not found';
        }

        return $result;
    }


    /**
     * Generates the query results
     *
     * @param $params
     * @param $query
     * @return mixed
     */
    private function select2_getRecords($params, $query) {
        return $this->select2_setQBParams($params, $query)->get();
    }


    /**
     * Adds limitation for when the result is an query builder
     *
     * @param $params
     * @param $query
     * @return mixed
     */
    private function select2_setQBParams($params, $query)
    {
        if (isset($params['limit']) && intval($params['limit'])) {
            $query->limit(intval($params['limit']));
        }

        return $query;
    }


    /**
     * Builds us the where params to a certain standard
     * 
     * @param $params
     * @return array
     */
    private function buildWhereParams($params)
    {
        $_where = [];
        foreach ((array) $params as $key => $val) {
            if (substr($key, 0, 6) === "where_") {
                $_where[substr($key, 6)] = $val;
            }
        }

        return $_where;
    }

    /**
     * select2FilterByCurrentValue
     *
     * @param Builder  $model
     * @param string $id_field_name
     *
     * @return Builder
     * @throws \Exception
     */
    private function select2FilterByCurrentValue(Builder $model, $id_field_name = 'id')
    {
        if (request()->get('getCurrentValue')) {
            if (!request()->get('value')) {
                throw new \Exception('Preset select data-value was not passed.');
            }

            if (isJson(request()->get('value'))) {
                $model->whereIn($id_field_name, json_decode(request()->get('value'), true));
            } else {
                $model->where($id_field_name, request()->get('value'));
            }
        }

        return $model;
    }

    /**
     * select2FilterBySearchQuery
     *
     * @param Builder  $model
     * @param string $search_field_name
     *
     * @return Builder
     */
    private function select2FilterBySearchQuery(Builder $model, $search_field_name = 'name')
    {
        if (count(request()->get('query')) > 0) {
            $model->where($search_field_name, 'LIKE', '%' . request()->get('query') . '%');
        }

        return $model;
    }
}
