<?php namespace App\Http\Traits;

use Illuminate\Http\Request;

trait AjaxModalTrait
{

    /**
     * Standardised method of working with server side Ajax Modal
     *
     * @param Request $request
     * @return array|view
     */
    public function ajaxModal(Request $request)
    {
        $method_prefix = 'ajaxModal_';

        $result = [
            'status' => 0,
            'desc'   => '',
            'data'   => [],
        ];

        try {
            $params = $request->except(['csrf', '_method']);
            if (isset($params['method']) && method_exists($this, $method_prefix . $params['method'])) {

                $_method_name = $method_prefix . $params['method'];
                $query_args   = func_get_args();
                array_shift($query_args);

                return $this->$_method_name($params, array_values($query_args));
            } else {
                $result['desc'] = 'Fatal: Getter method was not found';
                $result['params'] = $params;
            }
        } catch (\Exception $e) {
            $result['desc'] = $e->getMessage();
        }

        return $result;
    }
}
