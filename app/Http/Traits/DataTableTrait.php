<?php namespace App\Http\Traits;

use App\Libraries\Datatable\DatatableLib;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

trait DataTableTrait
{

    /**
     * Standardised method of working with server side Data Table
     * this trait need name method in controller dataTable_[method_name] - in html datatable data-route_method
     * this trait need by default:
     * 1) recordsTotal - count all results
     * 2) query - query to run, example - Task::select($datatable->getColumns())->with('fieldsQueryParams')
     * 3) if need add columns use method - addColumn in DatatableLib
     * 4) if need change columns name or add addition field use - changeColumns in DatatableLib
     * 5) if need change filter use - changeFilters in Datatable
     * 6) to run use fill method
     *
     * @see example in TaskController - dataTable_taskQuery
     *
     * @param Request $request
     *
     * @return array
     */
    public function dataTable(Request $request)
    {
        $result = [
            'status' => 0,
            'desc'   => '',
            'data'   => [],
        ];

        $uri_params = func_get_args();
        array_shift($uri_params);
        $params = $request->except(['csrf', '_method', '_token']);

        if (isset($params['method']) &&
            method_exists($this, 'dataTable_' . $params['method']) &&
            isset($params['cols'])
        ) {

            try {
                $_method_name              = "dataTable_" . $params['method'];
                $params['permission_name'] = self::PERMISSION;
                $query                     = $this->$_method_name($params, new DatatableLib($params), $uri_params);
                if (is_a($query, Collection::class) || is_array($query)) {

                    if (isset($query['state']) && $query['state'] === 0) {
                        $result = $query;
                    } else {

                        // For media usage
                        if (isset($query['file']) && $query['file']) {
                            return asset($query['path']);
                        }

                        return $query;
                    }
                } else {
                    $result['desc'] = 'No results returned';
                }
            } catch (\Exception $e) {
                abort(500, $e->getMessage());
            }

        } else {
            $result['desc'] = "Fatal: Getter method was not found {$params['method']}";
        }

        return $result;
    }
}
