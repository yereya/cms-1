<?php

namespace App\Http\Traits;

use App\Entities\Repositories\Sites\EntityLockRepo;
use View;

trait EntityLockTrait
{
    /**
     * Add Lock
     * @param $type
     * @param $entity_id
     */
    protected function addLock($entity_id, $site_id = null, $type = null)
    {
        $lock_params = json_encode([
            'type' => $this->getLockType($type),
            'entity_id' => $entity_id,
            'site_id' => $site_id,
            'polling_interval' => config('app.lock_polling_interval')
        ]);

        View::share('lock_params', $lock_params);
    }

    /**
     * @param $type
     * @param $entity_id
     * @param $site_id
     *
     * @return bool
     */
    protected function isMyLock($entity_id, $site_id = null, $type = null)
    {
        $entity_lock_repo = new EntityLockRepo();
        $user = auth()->user();
        $type = $this->getLockType($type);

        return $entity_lock_repo->isMyLock($user, $type, $entity_id, $site_id);
    }

    /**
     * Get Lock Type
     *
     * @param $type
     * @return string
     */
    private function getLockType($type = null)
    {
        if ($type) {

            return $type;
        }

        return get_class($this);
    }
}