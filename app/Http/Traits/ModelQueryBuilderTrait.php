<?php namespace App\Http\Traits;

trait ModelQueryBuilderTrait
{
    protected $query;


    /**
     * @param $key - column name in table
     * @param $value_one - column value or mysql rules
     * @param null $value_two - column value
     * @return $this
     */
    public function where($key, $value_one, $value_two = null) {
        if (is_null($value_two)) {
            $this->query->where($key, $value_one);
        } else {
            $this->query->where($key, $value_one, $value_two);
        }

        return $this;
    }

    /**
     * @param $key - column name in table
     * @param $values - array values
     * @return $this
     */
    public function whereIn($key, $values) {
        $this->query->whereIn($key, $values);
        return $this;
    }

    /**
     * @param $key - column name in table
     * @param $values - array values
     * @return $this
     */
    public function whereNotIn($key, $values) {
        $this->query->whereNotIn($key, $values);
        return $this;
    }

    /**
     * @param $key - column name in table
     * @param $between - array 2 values [start, end]
     * @return $this
     */
    public function whereBetween($key, $between) {
        $this->query->whereBetween($key, $between);
        return $this;
    }

    /**
     * @param $key - column name in table
     * @param $value_one - column value or mysql rules
     * @param null $value_two - column value
     * @return $this
     */
    public function orWhere($key, $value_one, $value_two = null) {
        if (is_null($value_two)) {
            $this->query->orWhere($key, $value_one);
        } else {
            $this->query->orWhere($key, $value_one, $value_two);
        }
        return $this;
    }

    /**
     * @param $offset - integer
     * @return $this
     */
    public function skip($offset) {
        $this->query->skip($offset);
        return $this;
    }

    /**
     * @param $limit - integer
     * @return $this
     */
    public function take($limit) {
        $this->query->take($limit);
        return $this;
    }

    /**
     * @param $select : array
     * @return $this
     */
    public function select($select) {
        $this->query->select($select);
        return $this;
    }

    /**
     * @param $column : string
     * @return $this
     */
    public function whereNull($column) {
        $this->query->whereNull($column);
        return $this;
    }

    /**
     * @param $column : string
     * @param $direction : asc /desc (default asc)
     * @return $this
     */
    public function orderBy($column, $direction = 'asc') {
        $this->query->orderBy($column, $direction);
        return $this;
    }

    /**
     * @param $column
     * @return $this
     */
    public function groupBy($column) {
        $this->query->groupBy($column);
        return $this;
    }

    /**
     * @param $column
     * @param $operator
     * @param $value
     * @param string $boolean
     * @return $this
     */
    public function having($column, $operator, $value, $boolean = 'and') {
        $this->query->having($column, $operator, $value, $boolean);
        return $this;
    }

}