<?php namespace App\Http\Traits;

use Illuminate\Http\Request;

trait AjaxOrderTrait
{
    /**
     * Standardised method of working with server side Ajax Order
     *
     * @param Request $request
     *
     * @return array|view
     */
    public function ajaxOrder(Request $request)
    {
        $method_prefix = 'ajaxOrder_';

        $result = [
            'status' => 0,
            'desc'   => '',
            'data'   => [],
        ];

        $params = $request->all();
        try {
            if (isset($params['method']) && method_exists($this, $method_prefix . $params['method'])) {

                $_method_name = $method_prefix . $params['method'];
                $query_args   = func_get_args();
                unset($query_args[0]);
                return $this->$_method_name($params, array_values($query_args));
            }

        } catch (\Exception $e) {
            return [
                'status' => 0,
                'desc'   => json_encode($e->getMessage())
            ];
        }

        $result['desc']   = 'Fatal: Getter method was not found';
        $result['params'] = $params;

        return $result;
    }
}