<?php


namespace App\Http\Traits;


use App\Entities\Models\Bo\Account;
use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Bo\AdvertiserRepo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

/**
 * Trait DashboardTrait
 *
 * @package App\Http\Traits
 */
trait DashboardTrait
{
    /**
     * Filter By  Advertisers
     *
     * @param      $query
     * @param null $params
     */
    public function whereAdvertisers($query, $params = null)
    {
        $field_name = 'advertiser_id';
        $account_repo = new AccountsRepo();
        $advertisers_id = request()->input('advertisers') ?? $account_repo->getAdvertisersId();

        if (!empty($params['inner_table_alias'])) {
            $field_name = "{$params['inner_table_alias']}.$field_name";
        }

        $query->whereIn($field_name, $advertisers_id);
    }

    /**
     * Filter By Source Advertisers
     *
     * @param      $query
     * @param null $params
     */
    public function whereSAdvertisers($query, $params = null)
    {
        $field_name = 'advertiser_id';
        /* // Old Version
//        $account_repo = new AccountsRepo();
//        $advertisers_id = request()->input('advertisers') ?? $account_repo->getAdvertisersId();
        */
        if(request()->input('advertisers')){
            $advertisers = request()->input('advertisers');
        }else{
            $user_id = auth()->user()->id;
            $bo_connection = DB::connection('bo');
            $accounts = $bo_connection->table('user_assigned_accounts')
                ->select('account_id')
                ->where('user_id','=',$user_id)
                ->get()->toArray();
            $advertisers = $bo_connection->table('accounts')
                ->selectRaw('distinct(advertiser_id)')
                ->whereIn('id',array_column($accounts,'account_id'))
                ->get()->toArray();
            $advertisers = array_column($advertisers,'advertiser_id');
            $advertisers  = array_filter($advertisers,function($row) {
                return $row;
            });
        }

        if (!empty($params['inner_table_alias'])) {
            $field_name = "{$params['inner_table_alias']}.$field_name";
        }


//        $query->whereIn('s_advertiser_id', $advertisers_id); // Old
        $query->whereIn('s_advertiser_id', $advertisers);
    }

    /**
     *  Filter Account Params if Exist
     *
     * @param $query
     */
    public function whereAccounts(Builder $query)
    {
        $accounts = request()->input('accounts');

        if (!empty($accounts)) {
            $query->whereIn('account_id', $accounts);
        }
    }
    /**
     *  Filter Account Params if Exist
     *
     * @param $query
     */
    public function whereChannel(Builder $query)
    {
        $channel = request()->input('channel');
        $bo_connection = DB::connection('bo');
        switch ($channel){
            case "Only Display":
                $accounts = Account::selectRaw('source_account_id')->where('chanel','=','display')->get()->toArray();

                if (!empty($accounts)) {
                    $query->whereIn('account_id', array_column($accounts,'source_account_id'));
                }
                break;
            case "Without Display":
                try{

                    $advertisers = request()->input('advertisers') ?? session('user_advertiser_access');

                    $date_range_str = request()->input()['date_range']['value'];
                    $from = explode(' ',$date_range_str)[0];
                    $to = explode(' ',$date_range_str)[1];

                    $accounts_with_display = $bo_connection->table('accounts')->selectRaw('source_account_id')->where('chanel','=','display')->get()->toArray();

                    $query->whereRaw('(account_id not in (' . implode(',',array_column($accounts_with_display,'source_account_id')) . ') or (account_name is NULL and advertiser_id in ('
                        . implode(',',$advertisers) .')  and stats_date_tz between "'. $from . '" and "'. $to . '"))' );

                }catch (\Exception $e){
                    throw new \Exception('whereChannel method : ' . $e);
                }
                break;
            default:
                break;
        }
        $bo_connection->disconnect();
    }

    /**
     *  Filter Account Params if Exist
     *
     * @param $query
     */
    public function whereTrafficSource(Builder $query)
    {
        $traffic_source = request()->input('traffic_source');
        if(!$traffic_source)
            return;
        if($query->getQuery()->from == 'dash_dailyStats'){
            switch ($traffic_source){

                case 'Search':

                    $accounts = Account::select('source_account_id')
                                    ->whereNotIn('chanel',array('display','social'))
                                    ->get()->pluck('source_account_id')->toArray();

                    $query->whereRaw('(account_id in (' . implode(',',$accounts)
                        . ') or (account_name is NULL and s_advertiser_id in ('
                        . implode(',',session('user_advertiser_access')) .')))' );
                    break;

                case 'Without Search':

                    $accounts = Account::select('source_account_id')
                        ->whereIn('chanel',array('display','social'))
                        ->get()->pluck('source_account_id')->toArray();

                    $query->whereIn('account_id',$accounts);
                    break;
                default:
                    break;
            }

        }else if ($query->getQuery()->from == 'advertisers_targets'){

            switch ($traffic_source){
                case 'Search':
                    $query->where('traffic_source','=', 'search');
                    break;
                case 'Without Search':
                    $query->where('traffic_source','=', 'without search');
                    break;
                default:
                    break;
            }

        }else {
            return;
        }
    }
    /**
     * Filter Dates Params if Exist
     *
     * @param $query
     */
    public function whereDates(Builder $query)
    {
        $from_date = request()->input('from') ?? date('Y-m-d', time());
        $to_date = request()->input('to') ?? date('Y-m-d', time() + (7 * 24 * 60 * 60));
        $date_range = request()->input('date_range');
        $column_name = 'stats_date_tz';
        $table_name = $query->getModel()->getTable();


        if (!empty($date_range)) {
            $date_range = explode(' ', $date_range['value']);
            $from_date = date('Y-m-d', strtotime($date_range[0]));
            $to_date = date('Y-m-d', strtotime($date_range[1]));
        }

        if ($table_name == 'advertisers_targets') {
            $column_name = 'date';
        }
        $query->whereBetween($column_name, [$from_date, $to_date]);
    }

    /**
     * Filter Device Params if Exist
     *
     * @param $query
     */
    public function whereDevices(Builder $query)
    {
        $devices = request()->input('devices');

        if (!empty($devices)) {
            $devices = $this->getUnknownAliases($devices) ?? $devices;
            $devices = $this->getMobileAliases($devices) ?? $devices;

            $query->whereIn('device', $devices);
        }
    }

    /**
     * Filter Vertical Params if Exist
     *
     * @param $query
     */
    public function whereVerticals($query)
    {
        $verticals = request()->input('verticals');
        if (!empty($verticals)) {

            $query->whereIn('product_name', $verticals);
        }
    }

    /**
     * Filter by Sale Group
     *
     * @param $query
     */
    public function whereSaleGroup($query)
    {
        $sale_group = request()->input('sale_group');

        if ($sale_group) {
            $advertiser_id = (new AccountsRepo())->getAdvertiserIdsBySaleGroup($sale_group);

            $query->whereIn('advertiser_id', $advertiser_id);
        }
    }
    /**
     * Filter by Business Group
     *
     * @param $query
     */
    public function whereBusinessGroup($query)
    {
        $business_group = request()->input('business_group');

        if ($business_group) {
            $advertiser_id = (new AdvertiserRepo()) ->getUserAdvertisersByBusinessGroup($business_group);
            $query->whereIn('advertiser_id', $advertiser_id);
        }
    }

    /**
     * Get
     *
     * @param  $query
     *
     * @return  void
     */
    public function whereYesterday($query)
    {
        $query->where('stats_date_tz',
            DB::raw('date_sub(cast(now() as date), interval 1 day)')
        );
    }

    /**
     * Get All Devices
     *
     * @return \Illuminate\Support\Collection
     */
    public function getDevices()
    {
        $devices = $this->query()
            ->pluck('device', 'device');

        $devices = $devices->map(function ($device) {
            return translateDevices($device);
        });

        return $devices->unique();
    }

    /**
     * get Unknown Aliases
     *
     * @param $devices
     *
     * @return array
     */
    private function getUnknownAliases($devices)
    {
        if (in_array('UNKNOWN', $devices)) {
            $deviceGroup = ['u', 'unknown', ''];

            return array_merge($devices, $deviceGroup);
        }
    }

    /**
     * get Mobile Aliases
     *
     * @param $devices
     *
     * @return array
     */
    private function getMobileAliases($devices)
    {
        if (in_array('m', $devices)) {
            $devices[] = 'p';

            return $devices;
        }
    }


    /**
     * Get Budget Stats
     *
     * @param $params
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getYesterdayStats($params)
    {
        /*build inner query*/
        $inner_query = $this->buildInnerQuery($params);

        /*join Tables according to table name*/
        $this->resolveJoinsByTableName($params['table'], $inner_query);

        /*filter only related advertisers */
        $this->whereAdvertisers($inner_query, $params);

        /*filter alerts occurred  yesterday*/
        $this->whereYesterday($inner_query);

        /*build outer query*/
        $outer_query = $this->buildOuterQuery($params['fields']['outer'], $inner_query);

        /*group by advertiser*/
        $outer_query->groupBy($params['groupBy']);
        $outer_query->orderBy($params['orderBy']);

        /*return query results*/
        return $outer_query->get();
    }


    /**
     * @param $query
     */
    private function joinAccount($query)
    {
        $query->join('bo.accounts as accounts', 'account_id', 'accounts.source_account_id');
    }

    /**
     * @param $query
     */
    private function joinAdvertiser($query)
    {
        $query->join('bo.advertisers as adv', 'accounts.advertiser_id', 'adv.id');
    }

    /**
     * @param $params
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function buildInnerQuery($params)
    {
        $model = $this->model();
        $table_name = $model->getConnection()->getDatabaseName() . '.' . $model->getTable();

        $query = $this->query();
        $query->selectRaw($params['fields']['inner']);
        $query->from(DB::raw($table_name . ' b'));

        return $query;
    }

    /**
     * @param $fields
     * @param $inner_query
     *
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    private function buildOuterQuery($fields = null, $inner_query)
    {
        $connection = $inner_query->getConnection()->getName();
        $query = DB::connection($connection)->table(DB::raw("({$inner_query->toSql()}) as sub"))
            ->mergeBindings(is_a($inner_query, Builder::class) ? $inner_query->getQuery() : $inner_query);
        if ($fields) {
            $query->selectRaw($fields);
        }

        return $query;
    }

    /**
     * @param $tableName
     * @param $inner_query
     */
    private function resolveJoinsByTableName($tableName, $inner_query)
    {
        if ($tableName != 'budget') {
            $this->joinAccount($inner_query);
            $this->joinAdvertiser($inner_query);

            return;
        }

        $this->joinAdvertiser($inner_query);
    }

    /**
     * Get the number of passed days according to date-range field
     *
     * @return mixed
     */
    private function resolveDaysPassed()
    {
        $date_range = request()->input('date_range')['value']
            ? explode(' ', request()->input('date_range')['value'])
            : [0 => date('Y-m-01', time()), 1 => date('Y-m-d', strtotime('-1 Day'))];


        $start_date = date_create(date('Y-m-d', strtotime($date_range[0])));
        $current_date = date_create(date('Y-m-d', strtotime($date_range[1])));

        return date_diff($current_date, $start_date)->days + 1;
    }


    /**
     * Get Start and End Dates according to date range field
     *
     * @return array
     */
    private function resolveStartEndDate()
    {
        $date_range = request()->input('date_range')['value']
            ? explode(' ', request()->input('date_range')['value'])
            : [0 => date('Y-m-01', time()), 1 => date('Y-m-d', strtotime('-1 Day'))];

        $date = [
            'start_date' => [
                'date' => $date_range[0],
                'month' => (int)date('m', strtotime($date_range[0])),
                'days' => (int)date('t', strtotime($date_range[0])),
            ],
            'end_date' => [
                'date' => $date_range[1],
                'month' => (int)date('m', strtotime($date_range[1])),
                'days' => (int)date('t', strtotime($date_range[1])),
            ]
        ];


        /*if there's only one month so */
        if ($date['start_date']['month'] === $date['end_date']['month']) {
            $start_date_from = date_create(date('Y-m-d', strtotime($date['start_date']['date'])));
            $end_date_to = date_create(date('Y-m-d', strtotime($date['end_date']['date'])));
            $date['end_date']['range'] = date_diff($start_date_from, $end_date_to)->days + 1;
            $date['start_date']['range'] = $date['end_date']['range'];
        } else {
            $start_date_from = date_create(date('Y-m-d', strtotime($date['start_date']['date'])));
            $start_date_to = date_create(date('Y-m-t', strtotime($date['start_date']['date'])));
            $date['start_date']['range'] = date_diff($start_date_from, $start_date_to)->days == 0
                ? 1
                : date_diff($start_date_from, $start_date_to)->days + 1;

            $start = date('Y-m-01', strtotime($date['end_date']['date']));
            $end_date_from = date_create($start);
            $end = date('Y-m-d', strtotime($date['end_date']['date']));
            $end_date_to = date_create($end);
            $date['end_date']['range'] = date_diff($end_date_from, $end_date_to)->days + 1;
        }

        return $date;
    }

    /**
     * Get days range
     *
     * @param $start_n_end_date
     * @param $current_date
     * @param $days_of_month
     * @return mixed
     */
    private function resolveDaysInRange($start_n_end_date, $current_date, $days_of_month)
    {
        $start_date = $start_n_end_date['start_date'];
        $end_date = $start_n_end_date['end_date'];

        /*IF THE MONTH IS IN THE FIRST OR THE LAST RANGE RETURN THE DAYS IN IT */
        if (($start_date['month'] == date('n', strtotime($current_date)))
            || ($end_date['month'] == date('n', strtotime($current_date)))) {
            if ($start_date['month'] == date('n', strtotime($current_date))) {

                return $start_date['range'];
            }
            return $end_date['range'];
        }

        /*Return the days of month*/
        return $days_of_month;
    }

    /**
     * Get records daily avarage
     *
     * @param        $records
     * @param string $prefix
     * @return mixed
     */
    private function getAvg($records, $prefix = 't_')
    {
        $days_passed = $this->resolveDaysPassed();
        $records->transform(function ($record) use ($prefix, $days_passed) {
            $fields = [$prefix . 'cost', $prefix . 'revenue', $prefix . 'profit', $prefix . 'conversions'];
            array_map(function ($field) use ($record, $days_passed) {
                /** @var TYPE_NAME $record */
                $no_zero_values =  !empty($record[$field]) && $record[$field] != 0;
                $in_the_same_day = $days_passed == 0;
                $record[$field] = $no_zero_values ? (!$in_the_same_day ? round($record[$field] / $days_passed) : $record[$field]) : 0;
            }, $fields);

            return $record;
        });

        return $records;
    }

    /**
     * Get records according to display values
     *
     * 1. filter all records by advertisers
     * 2. if avarage results needed (if display value is daily) then divide results by range days
     *
     * @param $records
     * @param $display_value
     * @param $fields
     * @return \Illuminate\Support\Collection
     */
    private function getRecordsFromMonthlyAccordingToDisplayValue($records, $display_value, $fields): \Illuminate\Support\Collection
    {
        $_records = collect();
        $advertisers = $records->keyBy('advertiser');
        $start_n_end_date = $this->resolveStartEndDate();

        $advertisers->each(function ($adv_record, $advertiser)
        use ($_records, $records, $start_n_end_date, $fields) {

            $records->each(function ($record) use ($_records, $advertiser, $start_n_end_date, $fields) {
                $temp = collect();
                $days_of_month = $record->days_in_month;
                $date = $record->stats_date_tz != null ? $record->stats_date_tz : $record->date;

                /*Days in range*/
                $days_in_range = $this->resolveDaysInRange($start_n_end_date, $date, $days_of_month);

                /*Create Advertiser*/
                $temp->put('advertiser', $record->advertiser);
                $temp->put('advertiser_id', $record->advertiser_id);
                $is_actual_result = preg_grep('/a_/', $fields);

                /*Iterate on same advertiser */
                if ($advertiser === $record->advertiser) {
                    $params = [
                        'temp' => &$temp,
                        'record' => &$record,
                        'advertiser' => &$advertiser,
                        'records' => &$_records,
                        'days_of_month' => &$days_of_month,
                        'days_in_range' => &$days_in_range,
                        'actual_results' => &$is_actual_result
                    ];

                    array_map(/**
                     * @param $field
                     */
                        function ($field) use (&$params) {
                            $params['field'] = $field;

                            /*Resolve record value according to field and display value*/
                            $record = $this->resolveRecordByDisplayValueAndType($params);

                            /*If value is already assign - update the value*/
                            $target_state = count($params['records']->where('advertiser', $params['advertiser'])->first()[$field]);
                            if ($target_state) {
                                $_record_model = $params['records']->where('advertiser', $params['advertiser'])->first();
                                $current_record = $_record_model[$field];
                                $updated_record = $current_record + $record;
                                $_record_model->put($field, $updated_record);
                            } else {
                                $params['temp']->put($field, $record);
                            }
                        }, $fields);


                    if (count($temp) > 2) {
                        $_records->push($temp);
                    }
                }
            });
        });

        /*ADD CPA TO RECORDS*/
        $_records->transform(function ($record) use ($fields) {
            $field_name = in_array('a_cost', $fields) ? 'a_cpa' : 't_cpa';
            $costs = in_array('a_cost', $fields) ? ($record['a_cost'] ?? 0) : ($record['t_cost'] ?? 0);
            $conversions = in_array('a_conversions', $fields) ? ($record['a_conversions'] ?? 0) : ($record['t_conversions'] ?? 0);

            $record[$field_name] = $this->resolveCpaField($costs, $conversions);

            return $record;
        });

        /*IF DAILY AVG */
        if ($display_value === 'daily') {

            if (in_array('a_cost', $fields)) {
                $_records = $this->getAvg($_records, 'a_');
            } else {
                $_records = $this->getAvg($_records);
            }
        }

        return $_records;
    }


    public function getTheTimeStampOfStartAndEndDate()
    {
        $date_range = explode(' ', request()->input('date_range')['value']);

        return [
            'from' => strtotime($date_range[0]),
            'to' => strtotime($date_range[1])
        ];
    }

    /**
     * @param array $params
     * @return float|int
     */
    public function resolveRecordByDisplayValueAndType(array $params)
    {
        $record = $params['record']->{$params['field']};

        return count($params['actual_results'])
        || $params['days_in_range'] == $params['days_of_month']
            ? $record
            : round(($record / $params['days_of_month']) * $params['days_in_range']);
    }

    public function resolveCpaField($costs, $sales)
    {
        $valid_values = $costs != 0 && $sales != 0;

        return $valid_values
            ? round($costs / $sales, 1)
            : 0;
    }
}
