<?php

namespace App\Http\Traits;

trait TreeToArrayTrait
{
    /**
     * Convert Page Tree To Select Format
     *
     * @param        $tree
     * @param string $prefix
     * @param array  $list
     *
     * @return array
     */
    private static function convertTreeToSelectFormat($tree, $prefix = '', Array &$list = [])
    {
        foreach ($tree as $item) {
            $list[] = ['id' => $item->id, 'text' => $prefix . $item->name];
            if ($item->children->count()) {
                $new_prefix = $prefix . '- ';
                self::convertTreeToSelectFormat($item->children, $new_prefix, $list);
            }
        }

        return $list;
    }
}