<?php

namespace App\Http\Traits;

use Illuminate\Http\Request;

trait AjaxNestableTrait
{
    /**
     * Standardised method of working with server side Ajax Modal
     *
     * @param Request $request
     * @return array|view
     */
    public function ajaxNestable(Request $request)
    {
        $method_prefix = 'ajaxNestable_';

        $params = $request->all();
        if (isset($params['method']) && method_exists($this, $method_prefix . $params['method'])) {

            $_method_name = $method_prefix . $params['method'];
            //take the route params using function params
            $query_args = func_get_args();
            unset($query_args[0]);
            return $this->$_method_name($params, array_values($query_args));
        }
        $result['desc'] = 'Fatal: Getter method was not found';
        $result['params'] = $params;
        return $result;
    }
}