<?php namespace App\Http\Requests;

/**
 * Class LoginFormRequest
 *
 * @package App\Http\Requests
 */
class LoginFormRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|exists:users,username,status,1',
            'password' => 'required',
        ];
    }
}