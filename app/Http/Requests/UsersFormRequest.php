<?php namespace App\Http\Requests;

/**
 * Class UsersFormRequest
 *
 * @package App\Http\Requests
 */
class UsersFormRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'username' => 'required|alpha_dash|unique:users,username',
                    'first_name' => 'required',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'min:8',
                    'password_repeat' => 'required_with:password|same:password',
                    'password_expiry_date' => 'required|date'
                ];
            case 'PUT':
                return [
                    'username' => 'required|alpha_dash|unique:users,username,' . $this->route()->getParameter('users'),
                    'first_name' => 'required',
                    'email' => 'required|email|unique:users,email,' . $this->route()->getParameter('users'),
                    'password' => 'min:8',
                    'password_repeat' => 'required_with:password|same:password',
                    'password_expiry_date' => 'required|date'
                ];
        }
    }

}