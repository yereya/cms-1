<?php namespace App\Http\Requests;

/**
 * Class ProfileFormRequest
 *
 * @package App\Http\Requests
 */
class ProfileFormRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'alpha|required',
            'last_name' => 'alpha|required',
            'password' => 'min:8',
            'password_repeat' => 'required_with:password|same:password'
        ];
    }

}