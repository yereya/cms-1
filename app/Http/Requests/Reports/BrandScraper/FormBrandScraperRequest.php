<?php

namespace App\Http\Requests\Reports\BrandScraper;

use App\Http\Requests\Request;

class FormBrandScraperRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'advertiser_id' => 'required',
            'currency' => 'required',
        ];
    }
}