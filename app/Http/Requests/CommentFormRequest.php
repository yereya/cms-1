<?php

namespace App\Http\Requests;

use App\Entities\Repositories\Sites\CommentRepo;

class CommentFormRequest extends Request
{
    private $rules = [
        'type'    => "required",
        'post_id' => "required",
        'author'  => 'required',
        'email'   => 'required|email'
    ];

    /**
     * Rules
     *
     * @return array
     */
    public function rules()
    {
        $type                = request()->get('type');
        $type_list           = implode(',', array_keys(CommentRepo::TYPES));
        $this->rules['type'] .= '|in:' . $type_list;

        if ($type) {
            return $this->getRulesByType($type);
        }

        return $this->rules;
    }

    /**
     *  Add Rules By Type
     *
     * @param $type
     *
     * @return array
     */
    public function getRulesByType($type)
    {
        return array_merge($this->rules, CommentRepo::RULES_BY_TYPE[$type]);
    }

}



