<?php namespace App\Http\Requests;

/**
 * Class PermissionsFormRequest
 *
 * @package App\Http\Requests
 */
class PermissionsFormRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'name' => 'required'
        ];
    }

}