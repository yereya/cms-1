<?php


namespace App\Http\Requests;


class UserActionFeedbackRequest extends Request
{

    function rules()
    {
        return [
	        'record_id'  => "required_without:records_id",
	        'records_id' => "required_without:record_id",
	        'user_id'    => "required",
	        'state'      => "required"
        ];
    }
}