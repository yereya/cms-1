<?php


namespace App\Http\Requests\Bo;


use App\Http\Requests\Request;

class BayesianTasksFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'days_for_prior' => 'numeric'
                ];
            case 'PUT':
                return [
                    'days_for_prior' => 'numeric'
                ];
        }
    }
}
