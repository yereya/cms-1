<?php


namespace App\Http\Requests\Bo;


use App\Http\Requests\Request;

class DiscrepancyFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'brand_group_id' => 'required',
                    'currency' => 'required',
                    'base_commission_amount' => 'required|numeric',
                    'date' => 'required',
                    'currency_rate'=>'required|numeric'

                ];
            case 'PUT':
                return [
                    'brand_group_id' => 'required',
                    'currency' => 'required',
                    'base_commission_amount' => 'required|numeric',
                    'date' => 'required',
                    'currency_rate'=>'required|numeric'

                ];
        }
    }
}