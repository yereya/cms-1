<?php

namespace App\Http\Requests\Bo\Advertisers;

use App\Http\Requests\Request;

class DomainRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'status' => 'required',
                    'domain' => 'required'
                ];
            case 'PUT':
                return [
                    'status' => 'required',
                    'domain' => 'required'
                ];
        }
    }
}
