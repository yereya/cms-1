<?php

namespace App\Http\Requests\Bo\Publishers;

use App\Http\Requests\Request;

class PixelsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'name' => 'required|unique:bo.publisher_account_pixels,name',
                    'url' => 'required|url'
                ];
            case 'PUT':
                return [
                    'name' => 'required|unique:bo.publisher_account_pixels,name,' . $this->route()->getParameter('pixels'),
                    'url' => 'required|url'
                ];
        }
    }
}
