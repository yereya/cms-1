<?php

namespace App\Http\Requests\Bo\Publishers;

use App\Http\Requests\Request;

class MediaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'media_name' => 'required|unique:bo.medias,media_name',
                    'status' => 'required',
                    'type' => 'required',
                    'url' => 'url'
                ];
            case 'PUT':
                return [
                    'media_name' => 'required|unique:bo.medias,media_name,' . $this->route()->getParameter('media'),
                    'status' => 'required',
                    'type' => 'required',
                    'url' => 'url'
                ];
        }
    }
}
