<?php namespace App\Http\Requests;

/**
 * Class RolesFormRequest
 *
 * @package App\Http\Requests
 */
class TasksFormRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
        ];
    }

}