<?php namespace App\Http\Requests;

/**
 * Class ForgetFormRequest
 *
 * @package App\Http\Requests
 */
class ForgetFormRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email'
        ];
    }

}