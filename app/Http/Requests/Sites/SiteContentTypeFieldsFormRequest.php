<?php namespace App\Http\Requests\Sites;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\SitesModel;
use App\Http\Requests\Request;
use Illuminate\Validation\Factory;
use Route;

/**
 * Class SiteContentTypeFieldsFormRequest
 *
 * @package App\Http\Requests
 */
class SiteContentTypeFieldsFormRequest extends Request
{
    public function __construct(Factory $factory)
    {
        $this->registerSlugRule($factory);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $table_name = SitesModel::DB_SITES_CONNECTION . '.' . SiteContentType::getTableName();

        switch ($this->getMethod()) {
            case 'POST':
                return [
                    'type_id' => 'required|exists:' . $table_name . ',id',
                    'name'    => 'required|slug',
                    'type'    => 'required'
                ];
            case 'PUT':
                return [
                    'type_id' => 'required|exists:' . $table_name . ',id',
                    'name'    => 'slug',
                    'type'    => 'required'
                ];
        }
    }

    /**
     * Register Slug Rule
     *
     * @param Factory $factory
     */
    private function registerSlugRule(Factory $factory) {
        $content_type = Route::getCurrentRoute()->getParameter('content_type');

        if (Route::getCurrentRoute()->hasParameter('fields')) {
            //PUT method

            $field = Route::getCurrentRoute()->getParameter('fields');
            $factory->extend('slug', function($attribute, $value, $parameters) use ($content_type, $field) {
                return (($field->name == $value && $field->type_id == $content_type->id ||
                    !SiteContentTypeField::where('name', $value)->where('type_id', $content_type->id)->first()) &&
                    !empty($value));
            }, 'Column name already exists');
        } else {
            //POST method

            $factory->extend('slug', function($attribute, $value, $parameters) use ($content_type) {
                $field = SiteContentTypeField::where('name', $value)->where('type_id', $content_type->id)->first();
                return !$field;
            }, 'Column name already exists');

        }
    }
}