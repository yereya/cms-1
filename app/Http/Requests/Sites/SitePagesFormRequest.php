<?php namespace App\Http\Requests\Sites;

use App\Entities\Repositories\Sites\PostRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Requests\Request;
use Illuminate\Validation\Factory;
use Route;

/**
 * Class SitePagesFormRequest
 *
 * @package App\Http\Requests
 */
class SitePagesFormRequest extends Request
{
    /**
     * SitePagesFormRequest constructor.
     *
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $site = SiteConnectionLib::getSite();

        $this->registerSlugRule($factory, $site->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'type' => 'required',
            'slug' => 'required|regex:"^[a-zA-Z0-9_-]*$"|slug:parent_id',
            'seo_post_title' => 'max:255',
            'description' => 'max:2200',
        ];
    }

    private function registerSlugRule(Factory $factory, $site_id) {
        $factory->extend('slug', function($attribute, $value, $parameters) use ($site_id) {
            if (is_null($value) || empty($value)) {
                return 'Slug name empty';
            }

            $parent_id = empty($this->get('parent_id')) ? null : $this->get('parent_id');

            $route = Route::getCurrentRoute();
            $post = $route->getParameter('post');
            $post_id = $post->id ?? null;
            $isSlugExists = (new PostRepo)->isPageSlugExists($value, $post_id, $parent_id);

            return ! $isSlugExists;

        }, 'Slug name already exists');
    }
}