<?php

namespace App\Http\Requests\Sites;

use App\Entities\Repositories\Sites\PostRepo;
use App\Http\Requests\Request;
use Illuminate\Validation\Factory;
use Route;

class SitePostFormRequest extends Request
{

    public function __construct(Factory $factory)
    {
        $this->registerRedirectLinksRules($factory);

        $this->registerSlugRule($factory);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'bail|required|regex:"[a-zA-Z0-9_-]+$"|slug',
            'seo_post_title' => 'max:255',
            'description' => 'max:2000',
            'likes_count' => 'integer',
            'redirect_links' => 'required_slug| unique_slug| required_url'
        ];
    }

    /**
     * Register Slug Rule
     *
     * @param Factory $factory
     * @param         $site
     */
    private function registerSlugRule(Factory $factory)
    {
        $factory->extend('slug', function ($attribute, $slug, $parameters) {

            $route = Route::getCurrentRoute();
            $post = $route->getParameter('post');
            if ($post) {
                $content_type_id = $post->content_type_id;
                $post_id = $post->id;
            } else {
                $content_type_id = request()->get('content_type_id');
                $post_id = null;
            }

            $post_repo = new PostRepo();
            $slug_exists = $post_repo->isSlugExists($content_type_id, $slug, $post_id);

            return !$slug_exists;

        }, 'Slug already exists');
    }

    /**
     * Register Unique Slug
     *
     * @param Factory $factory
     */
    private function registerRedirectLinksRules(Factory $factory)
    {
        $factory->extend('unique_slug', function ($attribute, $value, $parameters) use ($factory) {
            $is_unique_slug = true;
            $slug_helper = [];
            foreach ($value['slug'] as $slug) {
                if (isset($slug_helper[$slug])) {
                    $is_unique_slug = false;
                    break;
                }
                $slug_helper[$slug] = true;
            }

            return $is_unique_slug;

        }, 'Link slug must be unique ');

        $factory->extend('required_slug', function ($attribute, $value, $parameters) {
            $is_slug_empty = in_array(null, $value['slug']);
            if ($is_slug_empty) { //if didn't fill anything than it is a pass only when filled partly it fails
                if (count($value['slug']) > 1 || !empty($value['url'][0])) {

                    return false;
                }
            }

            return true;
        }, 'Link Slug is required');

        $factory->extend('required_url', function ($attribute, $value, $parameters) {
            $is_url_empty = in_array(null, $value['url']);
            if ($is_url_empty) { //if it is the only one and all are empty than it is ok
                if (count($value['url']) > 1 || !empty($value['slug'][0])) {

                    return false;
                }
            }

            return true;
        }, 'Link Url is required');
    }
}