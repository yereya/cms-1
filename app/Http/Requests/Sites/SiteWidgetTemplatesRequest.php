<?php


namespace App\Http\Requests\Sites;


use App\Entities\Models\Sites\SitesModel;
use App\Http\Requests\Request;

class SiteWidgetTemplatesRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // cms.site_widget_templates,file_name - on create
        // cms.site_widget_templates,file_name,TEST,file_name,site_id,1,widget_id,1 - on edit
        $file_name_unique_validation = implode(',', [
            'table' => SitesModel::DB_SITES_CONNECTION . '.' . 'site_widget_templates,file_name',
            'except' => ($this->widget_template ? $this->widget_template->file_name : '') . ',file_name',
            'where' => implode(',', [
                'site_id,' . $this->route('site'),
                'widget_id,' . $this->get('widget_id')
            ])
        ]);

        return [
            'name'      => 'required',
            'file_name' => 'required|unique:' . $file_name_unique_validation,
            'widget_id' => 'required',
            'published' => 'required'
        ];
    }

}