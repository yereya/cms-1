<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteMediaFormRequest
 *
 * @package App\Http\Requests
 */
class SiteMediaFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filename' => 'file|max:' . config('media.max_size') . '|mimes:' . config('media.media_types')
        ];
    }
}