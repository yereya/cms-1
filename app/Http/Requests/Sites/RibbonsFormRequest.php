<?php

namespace App\Http\Requests\Sites;

use App\Entities\Models\Sites\Ribbon;
use App\Entities\Models\Sites\Site;
use App\Http\Requests\Request;
use App\Facades\SiteConnectionLib;
use Illuminate\Validation\Factory;


class RibbonsFormRequest extends Request
{
    /**
     * RibbonsFormRequest constructor.
     *
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $site = SiteConnectionLib::getSite();

        $this->registerUnique($factory, $site);
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique_ribbon'
        ];
    }

    /**
     * Register new rule - unique_ribbon
     *
     * @param Factory $factory
     * @param Site $site
     */
    private function registerUnique(Factory $factory, Site $site) {
        $factory->extend('unique_ribbon', function($attribute, $value, $parameters) use ($site) {
            if (is_null($value) || empty($value)) {
                return false;
            }

            $ribbon = [];
            foreach ($parameters as $parameter) {
                $ribbon = Route::getCurrentRoute()->getParameter($parameter);
            }

            $unique = Ribbon::where($attribute, $value)->first();

            if ($ribbon && $unique) {
                return $ribbon->id == $unique->id ? true : false;
            } else {
                return true;
            }
        }, 'Ribbon with this name already exists or is field empty');
    }
}
