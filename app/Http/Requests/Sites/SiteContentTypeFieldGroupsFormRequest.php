<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteContentTypeFieldGroupsFormRequest
 *
 * @package App\Http\Requests
 */
class SiteContentTypeFieldGroupsFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required',
        ];
    }
}