<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteWidgetFieldsFormRequest
 *
 * @package App\Http\Requests
 */
class SiteWidgetFieldsFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'widget_id'  => 'required|exists:site_widgets,id',
            'type_id'    => 'required|exists:site_widget_field_types,id',
            'title'      => 'required',
            'help'       => 'required',
            'permission' => 'required'
        ];
    }
}