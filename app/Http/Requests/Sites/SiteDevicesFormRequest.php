<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteDevicesFormRequest
 *
 * @package App\Http\Requests
 */
class SiteDevicesFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required',
            'user_agent' => 'required'
        ];
    }
}