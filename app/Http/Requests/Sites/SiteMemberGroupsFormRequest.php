<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteMembersFormRequest
 *
 * @package App\Http\Requests
 */
class SiteMemberGroupsFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'alpha|required'
        ];
    }
}