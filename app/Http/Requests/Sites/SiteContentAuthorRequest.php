<?php

namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteContentAuthorRequest
 *
 * @package App\Http\Requests\Sites
 */
class SiteContentAuthorRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required',
            'last_name' => 'required'
        ];
    }
}
