<?php namespace App\Http\Requests\Sites;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\SitesModel;
use App\Http\Requests\Request;
use Illuminate\Validation\Factory;
use Route;

/**
 * Class SiteContentTypesFormRequest
 *
 * @package App\Http\Requests
 */
class SiteContentTypesFormRequest extends Request
{
    public function __construct(Factory $factory)
    {
        $site_id = Route::getCurrentRoute()->getParameter('site')->id;

        $this->registerContentTypeNameRule($factory, $site_id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $table_prefix = SitesModel::DB_SITES_CONNECTION . '.';

        return [
            'site_id' => 'required|exists:'. $table_prefix . 'sites,id',
            'table'   => 'content_type_name:content_type' . $this->route()->getParameter('content-types'),
            'name'    => 'required|content_type_name:content_type' . $this->route()->getParameter('content-types'),
        ];
    }

    private function registerContentTypeNameRule(Factory $factory, $site_id) {
        $factory->extend('content_type_name', function($attribute, $value, $parameters) use ($site_id) {
            if (is_null($value) || empty($value)) {
                return false;
            }

            $content_type = [];
            foreach ($parameters as $parameter) {
                $content_type = Route::getCurrentRoute()->getParameter($parameter);
            }

            $unique = SiteContentType::where($attribute, $value)->where('site_id', $site_id)->first();

            if ($content_type && $unique) {

                return $content_type->id == $unique->id ? true : false;
            } else {
                return true;
            }
        }, 'Content type name already exists or is field empty');
    }
}