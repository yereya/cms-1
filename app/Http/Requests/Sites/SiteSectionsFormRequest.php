<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteSectionsFormRequest
 *
 * @package App\Http\Requests
 */
class SiteSectionsFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'media_id' => 'required|numeric'
        ];
    }
}