<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteWidgetsFormRequest
 *
 * @package App\Http\Requests
 */
class SiteWidgetsFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'       => 'required',
            'controller' => 'required'
        ];
    }
}