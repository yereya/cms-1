<?php

namespace App\Http\Requests\Sites;

use App\Entities\Repositories\Sites\PostRepo;
use App\Http\Requests\Request;
use Illuminate\Validation\Factory;
use Route;

class SiteContentTypeDataFormRequest extends Request
{

    public function __construct(Factory $factory)
    {
        $site = Route::getCurrentRoute()->getParameter('site');

        $this->registerSlugRule($factory, $site);
    }


    /**
     * Rules
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'slug' => 'bail|required|regex:"^[a-zA-Z0-9_-]*$"|slug'
        ];
    }


    /**
     * Register Slug Rule
     *
     * @param Factory $factory
     * @param         $site
     */
    private function registerSlugRule(Factory $factory, $site) {
        $factory->extend('slug', function($attribute, $value, $parameters) use ($site) {

            $route = Route::getCurrentRoute();
            $content_type = $route->getParameter('content_type');
            $post_id = $route->getParameter('data');

            $post_repo = new PostRepo();
            $slug_exists = $post_repo->isSlugExists($content_type->id, $value, $post_id);

            return ! $slug_exists;

        }, 'Slug already exists');
    }
}