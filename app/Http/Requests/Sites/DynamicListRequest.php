<?php


namespace App\Http\Requests\Sites;


use App\Http\Requests\Request;

class DynamicListRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content_type_id' => 'required',
        ];
    }
}