<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteMigrationsFormRequest
 *
 * @package App\Http\Requests
 */
class SiteMigrationsFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required',
            'sql_up'   => 'required',
            'sql_down' => 'required'
        ];
    }
}