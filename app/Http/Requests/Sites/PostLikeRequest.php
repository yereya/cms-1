<?php


namespace App\Http\Requests;


class PostLikeRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id' => 'required',
            'type'    => 'required'
        ];
    }
}