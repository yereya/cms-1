<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class UsersFormRequest
 *
 * @package App\Http\Requests
 */
class SitesFormRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'domain' => [
                'required',
                'regex:/^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/'
            ],
            'db_host' => 'string',
            'db_name' => 'regex:"^[a-zA-Z0-9_]*$"',
        ];
    }

}