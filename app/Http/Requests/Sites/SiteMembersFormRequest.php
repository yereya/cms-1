<?php namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;

/**
 * Class SiteMembersFormRequest
 *
 * @package App\Http\Requests
 */
class SiteMembersFormRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'email|required',
            'status' => 'alpha|required'
        ];
    }
}