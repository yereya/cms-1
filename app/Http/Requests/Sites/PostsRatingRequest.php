<?php


namespace App\Http\Requests\Sites;


use App\Http\Requests\Request;

class PostsRatingRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'  => 'required',
            'rate' => 'required'
        ];
    }
}