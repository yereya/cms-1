<?php

namespace App\Http\Requests\Sites;

use App\Http\Requests\Request;


class ContactUsRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'    => 'required',
            'email'   => 'required',
            'message' => 'required'
        ];
    }
}