<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

/**
 * Class Kernel
 *
 * @package App\Http
 */
class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Session\Middleware\StartSession::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ]
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'                   => \App\Http\Middleware\Authenticate::class,
        'auth.basic'             => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest'                  => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'roles'                  => \App\Http\Middleware\ViewPermission::class,
        'trim.null'              => \App\Http\Middleware\ConvertEmptyRequestValuesToNull::class,
        'filter_template_modals' => \App\Http\Middleware\FilterTemplateModals::class,
        'dashboard_auth'         => \App\Http\Middleware\DashboardAuth::class,
        'captcha'                => \App\Http\Middleware\UpdateCaptchaSetting::class,
        'throttle'               => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'can'                    => \Illuminate\Auth\Middleware\Authorize::class,
    ];
}
