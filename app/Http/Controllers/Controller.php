<?php

namespace App\Http\Controllers;

use App\Http\Traits\SetupLayout;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

/**
 * Class Controller
 *
 * @package App\Http\Controllers
 */
abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, SetupLayout;

    const MESSAGE_TOAST = 'toastr_msgs';
    const OPEN_TAB = 'open_new_tab';
    const OPEN_AJAX_MODAL = 'open_new_modal';

    /*
     * Create permissions
     */
    protected function buildCanPermissions($suffix = null)
    {
        $suffix          = isset($suffix) ? '.' . $suffix : '';
        $can             = [];
        $can['view']     = auth()->user()->can(static::PERMISSION . $suffix . '.view');
        $can['edit']     = auth()->user()->can(static::PERMISSION . $suffix . '.edit');
        $can['add']      = auth()->user()->can(static::PERMISSION . $suffix . '.add');
        $can['delete']   = auth()->user()->can(static::PERMISSION . $suffix . '.delete');
        $can['download'] = auth()->user()->can(static::PERMISSION . $suffix . '.download');

        return $can;
    }
}
