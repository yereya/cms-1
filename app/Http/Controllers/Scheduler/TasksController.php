<?php namespace App\Http\Controllers\Scheduler;

use App\Entities\Models\FieldsQueryParam;
use App\Entities\Models\Scheduler\Task;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Datatable\DatatableLib;
use Illuminate\Http\Request;
use App\Http\Traits\Select2Trait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

/**
 * Class TaskController
 *
 * @package App\Http\Controllers
 */
class TasksController extends Controller
{
    use Select2Trait;
    use DataTableTrait;
    use AjaxModalTrait;
    use AjaxAlertTrait;

    const PERMISSION = 'scheduler.tasks';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Scheduled Tasks';

    /**
     * Index
     *
     * @return string
     */
    public function index()
    {
        $tasks = Task::with('fieldsQueryParams')->get()->toArray();
        return view('pages.scheduler.tasks.index')
            ->with('systems', FieldsQueryParam::select('id', 'display_name as text')->get())
            ->with('field_query_params', FieldsQueryParam::where('type', 'scheduler_tasks')->get()->keyBy('id'))
            ->with('tasks', $tasks);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.scheduler.tasks.form')
            ->with('field_query_params', FieldsQueryParam::select('id', 'display_name as text')->where('type', 'scheduler_tasks')->get());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $params = $request->all();

        $field_query_param_model = FieldsQueryParam::find($params['field_query_params_id']);
        if (!is_null($field_query_param_model)) {
            $params['cli_command'] = FieldsQueryParam::buildCliCommand($field_query_param_model);
        }
        $res = Task::create($params);

        //Redirect back to create when create_another is set
        return redirect()->route(isset($params['create_another']) ? 'scheduler.tasks.create' : 'scheduler.tasks.index')
            ->with('toastr_msgs', ["success" => "New scheduled task was saved."]);

    }


    /**
     * Edit resource form
     *
     * @param $task_id
     * @return $this
     */
    public function edit($task_id)
    {
        //Let's make sure authenticated user has edit permissions first
        if(!auth()->user()->can(self::PERMISSION . ".edit")){
            abort(403,'Authentitcated user lacks the required permissions to perform the request');
        }

        $task = Task::find($task_id);

        return view('pages.scheduler.tasks.form')
            ->with('field_query_params', FieldsQueryParam::select('id', 'display_name as text')->where('type', 'scheduler_tasks')->get())
            ->with('task', $task);
    }


    /**
     * Update existing resource
     *
     * @param Request $request
     * @param $task_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $task_id)
    {
        $params = $request->all();

        // Unchecked checkbox input isn't  sent as part of the form data, therefore
        // if active parameter doesn't exist, it means that it was unchecked
        if(!isset($params['active'])){
            $params['active']=0;
        }

        $task = Task::find($task_id);

        $field_query_param_model = FieldsQueryParam::find($params['field_query_params_id']);
        if (!is_null($field_query_param_model)) {
            $params['cli_command'] = FieldsQueryParam::buildCliCommand($field_query_param_model);
        }
        $task->fill($params)->save();

        //Redirect back to create when create_another is set
        return redirect()->route('scheduler.tasks.index')
            ->with('toastr_msgs', ["success" => "Scheduled task updated."]);

    }


    /**
     * Delete Resource
     *
     * @param Request $request
     * @param $task_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $task_id)
    {
        if (auth()->user()->can(self::PERMISSION.'.delete')) {
            Task::destroy($task_id);

            return redirect()->back()
                ->with('toastr_msgs', ["success" => "Task deleted"]);
        }

        //Redirect back to create when create_another is set
        return redirect()->back('scheduler.tasks.index')
            ->with('toastr_msgs', ["warning" => "You do not seem to have permission to do this action."]);

    }


    /**
     * Select2 Trait getter method
     *
     * @param $params
     * @return null
     * @throws \Exception
     */
    private function select2_getter($params)
    {
        $query = null;
        $dependency_id = $params['dependency'][0]['value'];

        // Task ID
        if ($params['search_field'] == "task_id") {
            if (isset($dependency_id)) {
                $task = FieldsQueryParam::find($dependency_id);
                if (isset($task) AND !is_null($task->options_query)) {

                    $query = DB::select(DB::raw($task->options_query));

                    // When a value was passed lets return only it
                    if (isset($params['getCurrentValue'])) {
                        $query = object2Array($query);
                        foreach ((array) $query as $item) {
                            if ($item['id'] == $params['value']) {
                                $query = [
                                    0 => $item
                                ];
                                break;
                            }
                        }
                    }
                } else {
                    throw new \Exception('Task could not be found.');
                }
            }
            else {
                throw new \Exception('Dependency was not field was not selected.');
            }
        }

        return $query;
    }


    /**
     * Select2 Trait getter method - Task Name
     *
     * @param $params
     * @return null
     */
    private function select2_taskName($params)
    {
        if (isset($params['query']) AND !empty($params['query'])) {
            return Task::select('id', 'task_name AS text')->where('task_name', 'like', '%'. $params['query'] .'%')->get();
        }
        return Task::select('id', 'task_name AS text')->get();
    }

    /**
     * Datatable Task Query
     * 
     * @param $params
     * @param $datatable
     *
     * @return mixed
     */
    private function dataTable_taskQuery($params, DatatableLib $datatable) {
        $params['recordsTotal'] = Task::count();

        //check if user has edit permissions
        $canUserEdit = auth()->user()->can(self::PERMISSION . ".edit");
        $params['searchColumns'] = ['task_name'];
        $datatable->addColumn('id');
        $datatable->renderColumns([
                'system'     => [
                    'db_name'  => 'field_query_params_id',
                    'callback' => function ($obj, $id) use ($canUserEdit) {
                        //Only show a clickable title if user has edit permissions
                        if($canUserEdit){
                            return '<a href="' . route('scheduler.tasks.edit', $id) . '">' . $obj['fields_query_params']['display_name']. '</a>';
                        }
                        return $obj['fields_query_params']['display_name'];
                    }
                ],
                'task_name'          => 'task_name',
                'cli_custom_command' => 'cli_custom_command',
                'repeat'             => 'repeat_delay',
                'start_time' => [
                    'db_name'  => 'run_time_start',
                    'callback' => function ($obj) {
                        return formatDate($obj['run_time_start'], 'H:i');
                    }
                ],
                'active'     => [
                    'db_name'  => 'active',
                    'callback' => function ($obj) {
                        $status = $obj['active'];

                        if ($status) {
                            $status = 'Active';
                            $class  = 'label-success';
                        } else {
                            $status = 'Inactive';
                            $class  = 'label-danger';
                        }
                        return "<span class='label label-sm $class'>$status</span>";
                    }
                ],
                'actions'    => [
                    [
                        'permissions' => 'edit',
                        'type'        => 'link',
                        'route'       => 'scheduler.tasks.edit',
                        'icon'        => 'icon-pencil',
                        'name'        => 'edit'
                    ],
                    [
                        'type'         => 'alert',
                        'name'         => 'Run',
                        'icon'         => 'icon-control-play',
                        'route'        => 'scheduler.tasks.{task_id}.ajax-alert',
                        'route_params' => 'method=runTask'
                    ],
                    [
                        'type'         => 'alert',
                        'name'         => 'Delete Task',
                        'icon'         => 'icon-trash',
                        'route'        => 'scheduler.tasks.{task_id}.ajax-alert',
                        'route_params' => 'method=deleteTask'
                    ]
                ]
            ]
        );

        $datatable->changeFilters([
            'system' => 'field_query_params_id',
            'task_name' => 'id'
        ]);

        $params['query'] = Task::select($datatable->getColumns())->with('fieldsQueryParams');
        
        return $datatable->fill($params);
    }

    /**
     * Ajax Alert Run Task
     * Run confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxAlert_runTask($params, $extra_params)
    {
        $task_id = $extra_params[0]; //get the task id from the passed params

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', 'Please confirm running task #'.$task_id)
            ->with('action', 'confirm')
            ->with('on_confirm', 'App.runCmdByTaskId('.$task_id.')');
    }

    /**
     * Ajax Modal Delete Task
     * Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxAlert_deleteTask($params, $extra_params)
    {
        $task_id = $extra_params[0]; //get the task id from the passed params

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', 'Please confirm deleting task #'.$task_id)
            ->with('route', 'scheduler.tasks.destroy')
            ->with('action', 'delete')
            ->with('route_params', $task_id);
    }


    /**
     * Run Cli Command
     *
     * @param Request $request
     *
     * @return string
     */
    public function runCmd(Request $request)
    {
        $params = $request->all();


        if (!isset($params['cmd'])) {
            return 'Bad command';
        }
        if (!in_array(auth()->user()->id, explode(',',env('CMS_DEVELOPERS','1,2,9,26')))) {
            return 'Not permitted';
        }

        if (isset($params['cmd']) && (strpos($params['cmd'], ';') !== false || strpos($params['cmd'], '&') !== false || strpos($params['cmd'], '|')  !== false)) {
            return 'Illegal command';
        }

        $command_string = '/usr/bin/php ' . base_path() . '/artisan ' . $request->input('cmd');
        $res = shell_exec($command_string);

        return $res;
    }


    /**
     * Run Cli Command by task id
     *
     * @param Request $request
     * @param FieldsQueryParam $fieldsQueryParam
     * @param $params
     * @return Response
     */
    public function runCmdByTaskId(Request $request, FieldsQueryParam $fieldsQueryParam, $params)
    {
        if (isset($params) && auth()->user()->can(self::PERMISSION . '.button[run-task].' . 'view')) {

            //get task cli command by task id
            $task = Task::find($params);

            //Flag for Capturing both STDOUT & STDERR outputs
            $output_command_flag = ' 2>&1';

            $command_string = '/usr/bin/php ';

            //run command
            if(env('APP_ENV') === 'local'){
                $command_string ='php ';
            }

            $command_string .=  base_path() . DIRECTORY_SEPARATOR . 'artisan ' . $task->cli_custom_command
            . $output_command_flag;

            exec($command_string, $output);

            // build log link params
            $link_params = ["task_id" => $task->id];

            // add the task's relevant system to the link_params
            $fieldQueryParamItem = $fieldsQueryParam->find($task->field_query_params_id);
            if(!is_null($fieldQueryParamItem)){
                $link_params['system_id'] = $fieldQueryParamItem->system_id;
            }

            // build the link for watching the tasks log
            $link = route('reports.logs.index', $link_params);

            return response(["output" => $output, "link"=> $link]);
        }

        dd('Bad command');
    }
}