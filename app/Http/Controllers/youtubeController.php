<?php


namespace App\Http\Controllers;
use Response;

class youtubeController extends Controller
{

    /**
     * Index
     *
     * @return string
     */
    public function index()
    {
        return view('pages.experiments.youtubeSearch.index');
    }

    /**
     * Process
     *
     * @return string
     */
    public function process(\Request $request){

        if(isset($_POST['file_name']) and $_POST['search'] != 1){

            return response()->download($_POST['file_name'],'output_'.date("Y-m-d_H:i:s").'.csv');

        }

        return view('pages.experiments.youtubeSearch.index');

    }


}