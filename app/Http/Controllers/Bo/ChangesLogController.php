<?php


namespace App\Http\Controllers\Bo;

use App\Entities\Models\Alerts\ChangesLog;
use App\Entities\Models\Bo\Advertiser;
use App\Http\Controllers\Controller;
use App\Http\Requests\UsersFormRequest;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class ChangesLogController extends Controller
{
    use DataTableTrait;
    use Select2Trait;

    const PERMISSION = 'bo.changes_log';
    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'ChangesLog';

    /**
     * Index - main screen
     *
     * @param Request $request
     *
     * @return $this
     */
    public function index(Request $request)
    {

        $url_query = $request->all();

        // Check .view permission for the user ( based on cms.user_permissions)
        if (!auth()->user()->can(self::PERMISSION.'.view')) {
            return response()->json(['error' => "You don't have permission to view rows"]);
        }
        $allowed_advertisers = session('user_advertiser_access');
        $changes_log_data = ChangesLog::select('*')->whereIn('advertiser_id',$allowed_advertisers)->get()->toArray();
        $advertisers_full_list = Advertiser::select('id', 'name')->whereIn('id',$allowed_advertisers)->groupBy('name')->get()->toArray();
        $advertiser_names = array_unique(array_column($changes_log_data,'advertiser_name'));

        // Create list of existing dates (Month - Year)
        $dates_list = array_map(function($row){
            return date("M-Y", strtotime($row));
        },array_column($changes_log_data,'event_date')) ;
        $dates_list = array_unique($dates_list);
        // Sort the dates
        usort($dates_list, function ($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        return view('pages.bo.changes_log.index')
            ->with('advertisers',  $advertiser_names)
            ->with('advertisers_full_list',  $advertisers_full_list)
            ->with('changes_log_data',json_encode($changes_log_data))
            ->with('dates_list',$dates_list);
    }

    public function getData()
    {
        if (!auth()->user()->can(self::PERMISSION.'.view')) {
            return response()->json(['error' => "You don't have permission to view rows"]);
        }
        $allowed_advertisers = session('user_advertiser_access');
        $changes_log_data = ChangesLog::select('*')->whereIn('advertiser_id',$allowed_advertisers)->get()->toArray();

        return \Response::json($changes_log_data);
    }

    /**
     * Store
     *
     * @param UsersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $req = $_POST;
        if (!auth()->user()->can(self::PERMISSION.'.add')) {
            return response()->json(['error' => "You don't have permission to add rows"]);
        }

        $this->improveStoreRequest($req,true);

        # Create new record in DB, on duplicate error redirect back
        try{
            ChangesLog::create($req);
        }catch(\Exception $e){
            if(strpos($e, 'Duplicate entry') !== false){
                return response()->json(['error' => "Duplicate Entry Error."]);
            }else {
                return response()->json(['error' => $e]);
            }
        }
        return response()->json(new \stdClass());
    }
    /**
     * Store
     *
     * @param UsersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete()
    {
        $req = $_POST;

        if (!auth()->user()->can(self::PERMISSION.'.delete')) {
            return response()->json(['error' => "You don't have permission to delete rows"]);
        }

        # Create new record in DB, on duplicate error redirect back
        try{
            ChangesLog::where('id','=',$req['id'])->delete();
        }catch(\Exception $e){
            return response()->json(['error' => $e]);
        }
        return response()->json(new \stdClass());
    }
    /**
     * Modify request
     * @param array $req
     */
    private function improveStoreRequest(array &$req, $is_new=false)
    {
        try{
            $req['advertiser_id'] = Advertiser::select('id')->where('name',$req['advertiser_name'])->first()->toArray()['id'];

            if($is_new)
                $req['id'] = ChangesLog::max('id') + 1;

            $req = array_map(function($row){
                if ($row == 'null' || !$row) {
                    return NULL;
                }else{
                    return $row;
                }
            },$req);

        }catch (\Exception $e){

            throw new Exception('Error in improveStoreRequest function, ChangesLogController.php, error message : ' . $e);
        }
    }

    /**
     * Update
     *
     * @param UsersFormRequest $request
     * @param int $user_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update()
    {
        $req = $_POST;
        if (!auth()->user()->can(self::PERMISSION.'.edit')) {
            return response()->json(['error' => "You don't have permission to edit rows"]);
        }

        $this->improveStoreRequest($req);
        $ignored_columns = ['updated_at','created_at'];

        try{
            $builder = \DB::connection('alert')->table('changes_log');
            insertOnDuplicateKeyUpdate($builder,array($req),$ignored_columns);
        }catch (\Exception $e){
            throw new \Exception('Error in Update method for changes_log');
        }
        return response()->json(new \stdClass());
    }

}
