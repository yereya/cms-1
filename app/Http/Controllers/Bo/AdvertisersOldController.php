<?php

namespace App\Http\Controllers\Bo;

use App\Entities\Models\BO\Advertiser;
use App\Entities\Models\Bo\AdvertisersActions;
use App\Entities\Models\Bo\AdvertisersBrands;
use App\Http\Requests\BO\Advertisers\AdvertisersRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Bo\OldOutInterfaces\OutAdvertiser;
use App\Libraries\Datatable\DatatableLib;

class AdvertisersOldController extends Controller
{
    use DataTableTrait;

    const PERMISSION = 'bo.advertisers';

    /**
     * @var array $assets
     */
    protected $assets = ['datatables', 'x-editable', 'form', 'bo-advertisers'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pages.bo.old.advertisers.index')
            ->with('advertisers', Advertiser::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.bo.old.advertisers.advertiser_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdvertisersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertisersRequest $request)
    {
        $advertiser = new Advertiser();
        $values = $request->all();

        try {
            $mongo = $this->out($values);
            if ($mongo) {
                $advertiser->mongodb_id = $mongo;
                $advertiser->fill($values);
                $advertiser->save();

                return redirect()->route('bo.old.advertisers.index')
                    ->with(self::MESSAGE_TOAST, ["success" => "Advertise (Name: $advertiser->name) updated!"]);
            } else {
                return redirect()->route('bo.old.advertisers.create')
                    ->with(self::MESSAGE_TOAST, ["Error OUT: " => $mongo]);
            }
        } catch (\Exception $e) {
            return redirect()->route('bo.old.advertisers.create')
                ->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advertiser = Advertiser::find($id);
        $brands = AdvertisersBrands::where('advertiser_id', $id)->get();
        $action = AdvertisersActions::where('advertiser_id', $id)->first();

        return view('pages.bo.old.advertisers.brands.index')
            ->with('advertiser', $advertiser)
            ->with('action', $action)
            ->with('ios', $brands);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.bo.old.advertisers.advertiser_form')
            ->with('advertiser', Advertiser::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AdvertisersRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertisersRequest $request, $id)
    {
        $advertiser = Advertiser::find($id);
        $values = $request->all();
        try {
            $mongo = $this->out($values, $advertiser->mongodb_id);

            if ($mongo) {
                $advertiser->fill($values);
                $advertiser->save();
                return redirect()->route('bo.old.advertisers.index')
                    ->with(self::MESSAGE_TOAST, ["success" => "Advertise (Name: $advertiser->name) updated!"]);
            } else {
                return redirect()->route('bo.old.advertisers.create')
                    ->with(self::MESSAGE_TOAST, ["Error OUT: " => $mongo]);
            }
        } catch (\Exception $e) {
            return redirect()->route('bo.old.advertisers.index')
                ->with(self::MESSAGE_TOAST, ["error" => "Advertise not updated: " . $e->getMessage()]);
        }
    }

    /**
     * Interface with out
     * create/update
     *
     * @param array $values
     * @param null $mongo_id
     * @return $this|mixed
     */
    private function out($values = [], $mongo_id = null) {
        $params = [
            'adserver' => $values['domain'],
            'name' => $values['name'],
            'category' => $values['category'],
            'status' => $values['status'],
            'timezone' => isset($values['time_zone']) ? $values['time_zone'] : null,
            'type' => $values['type']
        ];
        $out = new OutAdvertiser();

        if ($mongo_id) {
            return $out->setAdvertiser($mongo_id, $params);
        } else {
            return $out->storeAdvertiser($params);
        }
    }

    private function dataTable_advertisers($params, DatatableLib $datatable) {
        $query = Advertiser::whereNotNull('status');
        $params['recordsTotal'] = $query->count();

        $datatable->addColumn('id');

        $params['searchColumns'] = ['name', 'category', 'mongodb_id'];

        $datatable->renderColumns([
            'name' => [
                'db_name' => 'name',
                'callback' => function ($obj) {
                    $res = '';

                    if (auth()->user()->can(self::PERMISSION .'.brands.view')) {
                        $res .= "<a href='" . route('bo.old.advertisers.show', $obj['_id']) . "' class='tooltips' data-original-title='Show brands'>" . $obj['name'] . "</a>";
                    } else {
                        $res .= $obj['name'];
                    }

                    return $res;
                }
            ],
            'advertiser_id' => 'mongodb_id',
            'active' => [
                'db_name' => 'status',
                'callback' => function ($obj) {
                    $status = $obj['status'];
                    $class = 'label-success';

                    if ($status == 'inactive') {
                        $class = 'label-danger';
                    }

                    return "<span class='label label-sm $class'>$status</span>";
                }
            ],
            'actions' => [
                [
                    'name' => 'Edit advertiser',
                    'icon' => 'icon-pencil',
                    'route' => 'bo.old.advertisers.edit',
                    'permissions' => 'edit',
                    'type' => 'link'
                ],
                [
                    'name' => 'Show brands',
                    'icon' => 'icon-magnifier fa-fw',
                    'route' => 'bo.old.advertisers.show',
                    'permissions' => 'brands.view',
                    'type' => 'link'
                ],
                [
                    'name' => 'Show accounts',
                    'icon' => 'fa fa-newspaper-o fa-fw',
                    'url' => 'bo/accounts?advertiser=',
                    'permissions' => 'accounts.view',
                    'type' => 'link'
                ],
            ]
        ]);

        $datatable->changeFilters([
            'active' => 'status'
        ]);

        $params['query'] = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }
}

