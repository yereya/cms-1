<?php

namespace App\Http\Controllers\Bo\Accounts;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\BlockQuery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BlockQueriesController extends Controller
{
    const PERMISSION = 'bo.advertisers.accounts.block_queries';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * Index
     *
     * @param $account_id
     *
     * @return \Illuminate\View\View
     */
    public function index($account_id)
    {
        $Account = Account::findOrFail($account_id);

        return view('pages.bo.accounts.block-queries.index')
            ->with('account', Account::find($account_id))
            ->with('block_queries', $Account->blockQueries);
    }

    /**
     * Create
     *
     * @param $account_id
     *
     * @return \Illuminate\View\View
     */
    public function create($account_id)
    {
        return view('pages.bo.accounts.block-queries.form')
            ->with('types', array_combine(BlockQuery::$types, BlockQuery::$types))
            ->with('account', Account::find($account_id));
    }

    /**
     * Store
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function store(Request $request, $account_id)
    {
        $BlockQuery = new BlockQuery();

        $request->merge(['account_id' => $account_id]);

        $BlockQuery->fill($request->all());
        $BlockQuery->save();

        return redirect()
            ->route('bo.accounts.{account_id}.block-queries.index', [$account_id])
            ->with(self::MESSAGE_TOAST, ["success" => "Block Query (Name: $BlockQuery->name) saved!"]);
    }

    /**
     * Edit
     *
     * @param $account_id
     * @param $query_id
     * @return \Illuminate\View\View
     */
    public function edit($account_id, $query_id)
    {
        $BlockQuery = BlockQuery::findOrFail($query_id);
        return view('pages.bo.accounts.block-queries.form')
            ->with('types', array_combine(BlockQuery::$types, BlockQuery::$types))
            ->with('account', Account::find($account_id))
            ->with('BlockQuery', $BlockQuery);
    }

    /**
     * Update
     *
     * @param Request $request
     * @param         $account_id
     * @param         $query_id
     *
     * @return mixed
     */
    public function update(Request $request, $account_id, $query_id)
    {
        $BlockQuery = BlockQuery::findOrFail($query_id);

        $BlockQuery->fill($request->all());
        $BlockQuery->save();

        return redirect()
            ->route('bo.accounts.{account_id}.block-queries.index', [$account_id])
            ->with(self::MESSAGE_TOAST, ["success" => "Block Query (Name: $BlockQuery->name) saved!"]);
    }
}
