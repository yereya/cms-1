<?php

namespace App\Http\Controllers\Bo\Accounts;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\BlockedIp;
use App\Entities\Models\Bo\BlockQuery;
use App\Http\Controllers\Controller;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Blocker\Blocker;
use App\Libraries\Blocker\TypeHandlers\Ip\Extract\BlockedIps;
use App\Libraries\Datatable\DatatableLib;
use App\Libraries\TpLogger;
use Illuminate\Http\Request;

class BlockedIpsController extends Controller
{
    use DataTableTrait;

    const PERMISSION = 'bo.advertisers.accounts.block_ips';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * Index
     *
     * @param $account_id
     * @return \Illuminate\View\View
     */
    public function index($account_id)
    {
        return view('pages.bo.accounts.blocked-ips.index')
            ->with('url', "bo/accounts/$account_id/blocked-ips/datatable")
            ->with('account_id', $account_id);
    }

    /**
     * Refresh this ip
     *
     * @param Request $request
     * @param $account_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function refresh(Request $request, $account_id) {
        $ip = BlockedIp::find($request->input('id'));

        $blocker = new Blocker(new TpLogger(21));

        try {
            $blocker->refresh($account_id, $ip->ip);

            return redirect()->route('bo.accounts.{account_id}.blocked-ips.index', [$account_id])
                ->with(self::MESSAGE_TOAST, ["success" => "Refreshed (IP: $ip->ip) successful!"]);
        } catch (\Exception $e) {
            return redirect()->route('bo.accounts.{account_id}.blocked-ips.index', [$account_id])
                ->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }
    
    public function release($account_id) {
        $queries = BlockQuery::where('account_id', $account_id)->get();
        $account = Account::find($account_id);

        try {
            $release = new BlockedIps($queries, $account_id);
            $release->run(true);

            return redirect()->route('bo.accounts.{account_id}.blocked-ips.index', [$account_id])
                ->with(self::MESSAGE_TOAST, ["success" => "Released all ips (Account ID: $account->name) successful!"]);
        } catch (\Exception $e) {
            return redirect()->route('bo.accounts.{account_id}.blocked-ips.index', [$account_id])
                ->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }

    /**
     * Create datatable
     *
     * @param $params
     * @param DatatableLib $datatable
     * @return array
     */
    public function dataTable_blockedIps($params, DatatableLib $datatable)
    {
        $account_id = $datatable->getFilters()['account_id'];

        $datatable->renderColumns([
            'block_query' => [
                'db_name' => 'block_query_id',
                'callback' => function ($obj) use ($account_id) {
                    $str = '';
                    if (auth()->user()->can(self::PERMISSION . '.edit')) {
                        $str .= '<a href="' . route('bo.accounts.{account_id}.block-queries.edit', [$account_id, $obj['block_query']['id']]) . '">' . $obj['block_query']['name'] . '</a>';
                    } else {
                        $str = $obj['block_query']['name'];
                    }

                    return $str;
                }
            ],
            'status' => [
                'db_name' => 'status',
                'callback' => function ($obj) {
                    $status = $obj['status'];
                    $class = 'label-success';

                    if ($status == 'released') {
                        $class = 'label-danger';
                    } elseif ($status == 'pending') {
                        $class = 'label-default';
                    }

                    return "<span class='label label-sm $class'>$status</span>";
                }
            ],
            'actions' => [
                [
                    'name' => 'Refresh IP',
                    'icon' => 'fa fa-refresh fa-fw',
                    'url' => "bo/accounts/$account_id/blocked-ips/refresh?id=",
                    'permissions' => 'edit',
                    'type' => 'link'
                ]
            ]
        ]);

        $datatable->changeFilters([
            'account_id' => 'id'
        ]);

        $datatable->changeFilters(['account_id' => false]);

        $queries = BlockQuery::where('account_id', $account_id)
            ->where('status', true)->get()->pluck('id')->filter()->toArray();

//        SELECT * FROM bo.blocked_ips where block_query_id in (14,15,16) or release_query_id in (14,15,16);
        $params['query'] = BlockedIp::whereIn('release_query_id', array_values($queries))
            ->orWhereIn('block_query_id', array_values($queries))->with('blockQuery');
        $params['recordsTotal'] = $params['query']->count();

        return $datatable->fill($params);
    }
} 
