<?php

namespace App\Http\Controllers\Bo\Accounts;

use App\Entities\Models\Bo\Account;
use App\Http\Controllers\Controller;

class CampaignsController extends Controller
{
    const PERMISSION = 'bo.advertisers.accounts.campaigns';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables'];

    /**
     * Index
     *
     * @param $account_id
     * @return \Illuminate\View\View
     */
    public function index($account_id)
    {
        $account     = Account::find($account_id);

        return view('pages.bo.accounts.campaigns.index')
            ->with('account_id', $account_id)
            ->with('campaigns', $account->campaigns);
    }
}
