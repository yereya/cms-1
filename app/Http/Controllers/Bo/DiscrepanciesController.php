<?php


namespace App\Http\Controllers\Bo;


use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Bo\AdvertisersBrands;
use App\Entities\Models\Bo\BrandGroup;
use App\Entities\Models\Bo\BrandsGroupDiscrepancy;
use App\Entities\Models\Bo\BrandsGroupDiscrepancyBizDev;
use App\Entities\Models\Dwh\brands_group_discrepancies_vw;
use App\Entities\Models\Bo\Companies;
use App\Entities\Models\Bo\Publishers\Brand;
use App\Entities\Models\Users\AccessControl\SiteUserRole;
use App\Http\Controllers\Controller;
use App\Http\Requests\UsersFormRequest;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class DiscrepanciesController extends Controller
{
    use DataTableTrait;
    use Select2Trait;

    const PERMISSION = 'bo.discrepancies';
    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Discrepancies';

    /**
     * Index - main screen
     *
     * @param Request $request
     *
     * @return $this
     */
    public function index(Request $request)
    {

        $url_query = $request->all();

        if (!auth()->user()->can(self::PERMISSION.'.view')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }
        $status_list = BrandsGroupDiscrepancyBizDev::getPossibleStatuses();

        // Get Discrepancies by Advertiser permissions
        $brand_group_ids  = $this->getBrandGroupByAdvertiser();
        $data = brands_group_discrepancies_vw::select('*')->whereIn('brand_group_id',$brand_group_ids->pluck('id'))->get()->toArray();
//        $result['users'] = $data;

        $result = array();
        if($data){
            $idx_cms_commision = array_search('cms_commission_amount', array_keys($data[0]));
            $idx_payment = array_search('payment', array_keys($data[0]));
            foreach ($data as $key => $item) {
                # Add diff between cms and base commissions
                $base_cms_diff = $item['base_commission_amount'] - $item['cms_commission_amount'];
                $item = $this->array_insert($item,$base_cms_diff, $idx_cms_commision,'base_cms_diff');
                $payment_cms_diff = $item['payment'] - $item['cms_commission_amount'];
                $item = $this->array_insert($item,$payment_cms_diff, $idx_payment,'payment_cms_diff');
                $result[$item['date']][$key] =  $item;
            }
            ksort($result, SORT_NUMERIC);
        }

        # --- / Sort data by date ---

        $edit_permissions    = auth()->user()->can(self::PERMISSION.'.owner.edit')    ? true : false ;
        $approve_permissions = auth()->user()->can(self::PERMISSION.'.reviewer.edit') ? true : false ;
        $owners_list = brands_group_discrepancies_vw::select('id', 'owner')->groupBy('owner')->get()->pluck('owner','owner')->push('none');
        $revieres_list = brands_group_discrepancies_vw::select('id', 'reviewer')->groupBy('reviewer')->get()->pluck('reviewer','reviewer')->push('none');

        # Get Users role
        $user_id = auth()->user()->getAuthIdentifier();
        $site_user_role = array_column(SiteUserRole::selectRaw('distinct role_id')->where('user_id', $user_id)->get()->toArray(),'role_id');
        $finance_permission = in_array(13,$site_user_role) || in_array(1,$site_user_role) ;

        # Using View dwh.brands_group_discrepancy_vw
        return view('pages.bo.discrepancies.index')
            ->with('discrepancies',json_encode($result))
            ->with('edit_permissions',$edit_permissions)
            ->with('approve_permissions',$approve_permissions)
            ->with('owners', $owners_list)
            ->with('reviewers', $revieres_list)
            ->with('group_names', brands_group_discrepancies_vw::select('id', 'brand_group_name')->groupBy('brand_group_name')->get()->pluck('brand_group_name','brand_group_name'))
            ->with('status_list', $status_list)
            ->with('dates', brands_group_discrepancies_vw::select('id', 'date')->groupBy('date')->get()->pluck('date','date'))
            ->with('business_groups',Advertiser::select('id','business_group')->groupBy('business_group')->get()->pluck('business_group','business_group'))
            ->with('advertisers',  brands_group_discrepancies_vw::select('id', 'advertiser_name')->groupBy('advertiser_name')->get()->pluck('advertiser_name','advertiser_name'))
            ->with('invoice_group',  brands_group_discrepancies_vw::select('id', 'invoice_group')->groupBy('invoice_group')->get()->pluck('invoice_group','invoice_group'))
            ->with('finance_permission',$finance_permission)
            ->with('url_query',$url_query);
    }

    # --- Sort data by date ---
    private function array_insert(&$array, $value, $index, $key='diff')
    {
        $first_part = $array;
        array_splice($first_part,0,$index + 1);
        $second_part = array_splice($array,$index + 1,count($array) - $index);

        return array_merge($array,array($key => $value),$first_part);
    }

    private function getDiscrepanciesByAdvertiser()
    {
        $brand_group_ids = $this->getBrandGroupByAdvertiser();
        $discrepancies   = BrandsGroupDiscrepancy::select('*')->whereIn('brand_group_id',$brand_group_ids->pluck('id'))->get();
        return $discrepancies;
    }

    private function getBrandGroupByAdvertiser(){
        $allowed_advertisers = session('user_advertiser_access');
        $brand_group_ids     = Brand::select('brands_group_id')->whereIn('advertiser_id',$allowed_advertisers)->get();
        $brand_groups        = BrandGroup::select('id','name','company_id')->whereIn('id',$brand_group_ids)->get();
        return $brand_groups;
    }
    /**
     * Edit
     *
     * @param int $discrepancy_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */

    public function edit($discrepancy_id)
    {
        if (!auth()->user()->can(self::PERMISSION.'.owner.edit')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }

        $discrepancy = brands_group_discrepancies_vw::find($discrepancy_id);

        if (!$discrepancy) {
            throw new \Exception('the discrepancy you have provided does not exists');
        }

        return view('pages.bo.discrepancies.form')
            ->with('discrepancy', $discrepancy)
            ->with('brands_list',$this->getBrandGroupByAdvertiser())
            ->with('user_role','owner');
    }

    public function getData()
    {

        # --- Sort data by date ---

        // Get Discrepancies by Advertiser permissions
        $brand_group_ids  = $this->getBrandGroupByAdvertiser();
        $data = brands_group_discrepancies_vw::select('*')->whereIn('brand_group_id',$brand_group_ids->pluck('id'))->get()->toArray();


        if($data){
            $idx_cms_commision = array_search('cms_commission_amount', array_keys($data[0]));
            $idx_payment = array_search('payment', array_keys($data[0]));
            foreach ($data as $key => $item) {
                # Add diff between cms and base commissions
                $base_cms_diff = $item['base_commission_amount'] - $item['cms_commission_amount'];
                $item = $this->array_insert($item,$base_cms_diff, $idx_cms_commision,'base_cms_diff');
                $payment_cms_diff = $item['payment'] - $item['cms_commission_amount'];
                $item = $this->array_insert($item,$payment_cms_diff, $idx_payment,'payment_cms_diff');
                $result[$item['date']][$key] =  $item;
            }
            ksort($result, SORT_NUMERIC);
        }

        return \Response::json($result);
    }

    /**
     * Approve
     *
     * @param int $discrepancy_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function approve($discrepancy_id)
    {
        if (!auth()->user()->can(self::PERMISSION.'.reviewer.edit')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }

        $discrepancy = brands_group_discrepancies_vw::find($discrepancy_id);

        if (!$discrepancy) {
            throw new \Exception('the discrepancy you have provided does not exists');
        }

        return view('pages.bo.discrepancies.form')
            ->with('discrepancy', $discrepancy)
            ->with('user_role','reviewer');
    }
    /**
     * Store
     *
     * @param UsersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
//    public function store(DiscrepancyFormRequest $request)
    public function store()
    {
        $req = $_POST;
        $this->improveStoreRequest($req,true);

        # Create new record in DB, on duplicate error redirect back
        try{
            BrandsGroupDiscrepancyBizDev::create($req);
        }catch(\Exception $e){
            if(strpos($e, 'Duplicate entry') !== false){
                return response()->json(['error' => "Brand group for this month with current currency already exists."]);
            }else {
                return response()->json(['error' => $e]);
            }
        }

        return response()->json(new \stdClass());
    }

    /**
     * Modify request
     * @param array $req
     */
    private function improveStoreRequest(array &$req, $is_new=false)
    {
        try{
            # Updated owner/reviewer name based on role
            if($req['user_role']=='owner'){
                $req['owner']  = \Auth::user()->username;
                //$req['status'] = 'Pending';
            }else{
                $req['reviewer'] = \Auth::user()->username;
            }
            if($req['id'])
                $brand_data = BrandGroup::select('id','name','company_id')->where('id',$req['brand_group_id'])->get()->toArray()[0];
            else{
                $brand_data = BrandGroup::select('id','name','company_id')->where('name',$req['brand_group_name'])->get()->toArray()[0];
                $req['brand_group_id'] = $brand_data['id'];
            }

            $req['brand_group_name'] = $brand_data['name'];
            $req['company_id']   = $brand_data['company_id'];
            $req['company_name'] = Companies::select('name')->where('id',$req['company_id'])->get()->toArray()[0]['name'];

            $advertiser_q = AdvertisersBrands::select('advertiser_id')
//                ->where('brands_group_id','=',$req['brand_group_id'])
                ->where('brands_group_id','=',$brand_data['id'])
                ->whereNotIn('advertiser_id',array('101','121','124'))
                ->get()
                ->first();

            if(!$advertiser_q){ // If advertiser is one of the ('101','121','124') or not exists at all

                $advertiser_q = AdvertisersBrands::select('advertiser_id')
                    ->where('brands_group_id','=',$brand_data['id'])
//                    ->where('brands_group_id','=',$req['brand_group_id'])
                    ->get()
                    ->first();

                // Advertiser not found in bo.out_brands
                if(!$advertiser_q){
                    throw new Exception('Error in improveStoreRequest function, advertiser was not found in bo.out_brands ');
                }
            }
            $advertiser = $advertiser_q->advertiser_id;

            $req['business_group'] = Advertiser::select('business_group')->where('id','=',$advertiser)->get()->first()->business_group;
//            $req['invoice_group'] = BrandGroup::select('description')->where('id',$req['brand_group_id'])->get()->toArray()[0]['description'];
            $req['invoice_group'] = BrandGroup::select('description')->where('id',$brand_data['id'])->get()->toArray()[0]['description'];
            $req['advertiser_id'] = $advertiser;
            $req['advertiser_name'] = Advertiser::select('name')->where('id','=',$advertiser)->get()->toArray()[0]['name'];
            if($is_new)
                $req['id'] = BrandsGroupDiscrepancyBizDev::max('id') + 1;
            $req = array_map(function($row){
                if ($row == 'null' || !$row) {
                    return NULL;
                }else{
                    return $row;
                }
            },$req);

        }catch (\Exception $e){

            throw new Exception('Error in improveStoreRequest function, DiscrepanciesController.php, error message : ' . $e);
        }
    }


    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if(auth()->user()->can('bo.discrepancies.owner.add')){
            return view('pages.bo.discrepancies.form')
                ->with('user_role','owner')
                ->with('brands_list',$this->getBrandGroupByAdvertiser());
        }else{
            return redirect()->back()
            ->with(self::MESSAGE_TOAST, ["Error" => "You do not have permissions to view this page."]);
        }
    }
    /**
     * Update
     *
     * @param UsersFormRequest $request
     * @param int $user_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
//    public function update(DiscrepancyFormRequest $request, $discrepancy_id)
    public function update()
    {
        $req = $_POST;
        $discrepancy_id = $req['id'];
        $this->improveStoreRequest($req);
        $ignored_columns = ['_token','_method','base_cms_diff','payment_cms_diff','user_role','click_out',
                            'updated_at','created_at','tracks_created','tracks_updated','deal_explanation',
                            'payment_terms', 'invoice_flag'];
        try{
            $builder = \DB::connection('bo')->table('brands_group_discrepancy_bizdev');

            $old_discrepancy = (array)$builder->where('id',$discrepancy_id )->get()->first();
            $old_discrepancy = array_merge((array)$old_discrepancy,$req);

            $new_discrepancy = [];
            foreach ($old_discrepancy as $key => $value) {
                if(!in_array($key,$ignored_columns)){
                    $new_discrepancy[$key] = $value;
                }
            }

            insertOnDuplicateKeyUpdate($builder,array($new_discrepancy),$ignored_columns);

        }catch (\Exception $e){
            throw new \Exception('Error in Update method - connect and update discrepancy');
        }


        return response()->json(new \stdClass());

        /* ----   Old Version ----
        $req = $request->all();
        $this->improveStoreRequest($req);

        $ignored_columns = ['_token','_method','user_role','click_out','updated_at','created_at','tracks_created','tracks_updated','deal_explanation'];

        try{
            $builder = \DB::connection('bo')->table('brands_group_discrepancy_bizdev');

            $old_discrepancy = (array)$builder->where('id',$discrepancy_id )->get()->first();
            $old_discrepancy = array_merge((array)$old_discrepancy,$req);

            $new_discrepancy = [];
            foreach ($old_discrepancy as $key => $value) {
                if(!in_array($key,$ignored_columns)){
                    $new_discrepancy[$key] = $value;
                }
            }

            insertOnDuplicateKeyUpdate($builder,array($new_discrepancy),$ignored_columns);

        }catch (\Exception $e){
            throw new \Exception('Error in Update method - connect and update discrepancy');
        }

//        return redirect()->route('bo.discrepancies.index')
//            ->with('toastr_msgs', ["success" => "Brand group updated"]);
          */
    }


    private function dataTable_discrepanciesQuery($params, DatatableLib $datatable)
    {
        $params['recordsTotal'] =  brands_group_discrepancies_vw::all()->count();
        $params['searchColumns'] = ['owner','reviewer','brand_group_name','company_name','business_group','status'];

        $datatable->addColumn('id');
        $datatable->renderColumns([
                'brand_group_name' => 'brand_group_name',
                'company_name' => 'company_name' ,
                'advertiser_name' => 'advertiser_name' ,
                'invoice_group' => 'invoice_group' ,
                'currency' => 'currency' ,
                'brand_commission' => 'base_commission_amount' ,
                'cms_commission' => 'cms_commission_amount' ,
                'diff' => [
                    'db_name' => 'id',
                    'callback' => function ($obj) {
                        $name = $obj['base_commission_amount'] - $obj['cms_commission_amount'];
                        return $name;
                    }
                ],
                'date' => 'date' ,
                'owner' => 'owner' ,
                'reviewer' => 'reviewer' ,
                'status' => 'status' ,
                'business_group'=>'business_group',
                'actions' => function($obj) {
                    $res = '';

                    $res .= "<a href='". route('bo.discrepancies.edit', [$obj['_id']]) ."'
                        title='Edit' class='tooltips' ><span aria-hidden='true' class='icon-pencil'></span></a> ";
                    $res .= "<a href='". route('bo.discrepancies.{discrepancy_id}.approve', [$obj['_id']]) ."'
                        title='Approve' class='tooltips' ><span aria-hidden='true' class='icon-check'></span></a> ";

                    return $res;

                }
            ]
        );

        $brand_group_ids  = $this->getBrandGroupByAdvertiser();
        $params['query'] = brands_group_discrepancies_vw::select($datatable->getColumns())->whereIn('brand_group_id',$brand_group_ids->pluck('id'));

        return $datatable->fill($params);
    }

}
