<?php


namespace App\Http\Controllers\Bo;


use App\Entities\Models\Bo\BayesianData;
use App\Entities\Models\Bo\BayesianTasks;
use App\Http\Controllers\Controller;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use Illuminate\Http\Request;

class BayesianDataController extends Controller
{
    use DataTableTrait;
    use Select2Trait;

    const PERMISSION = 'bo.bayesian';
    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'BayesianData';

    /**
    * Index - main screen
    *
    * @param Request $request
    *
    * @return $this
*/
    public function index(Request $request)
    {

        $url_query = $request->all();

        if (!auth()->user()->can(self::PERMISSION . '.view')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }
        return view('pages.bo.bayesiandata.index')
            ->with('url_query', $url_query);

    }

    private function dataTable_bayesiandataQuery($params, DatatableLib $datatable)
    {
        $params['recordsTotal'] = BayesianData::all()->count();
        $params['searchColumns'] = ['test_name','kpi'];

        $datatable->addColumn('id');
        $datatable->renderColumns([
                'date' => 'date',
                'probability' => 'probability',
                'test_name' => 'test_name',
                'kpi' => 'kpi',
                'created_at' => 'created_at',
            ]
        );


        $params['query'] = BayesianData::select($datatable->getColumns());
        return $datatable->fill($params);
    }

}
