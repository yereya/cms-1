<?php


namespace App\Http\Controllers\Bo;

use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Bo\BayesianTasks;
use App\Http\Controllers\Controller;
use App\Http\Requests\Bo\BayesianTasksFormRequest;
use App\Http\Requests\UsersFormRequest;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use Illuminate\Http\Request;


class BayesianTasksController extends Controller
{

    use DataTableTrait;
    use Select2Trait;

    const PERMISSION = 'bo.bayesian';
    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'BayesianTasks';

    /**
     * Index - main screen
     *
     * @param Request $request
     *
     * @return $this
     */
    public function index(Request $request)
    {

        $url_query = $request->all();

        if (!auth()->user()->can(self::PERMISSION . '.view')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }
        return view('pages.bo.bayesiantasks.index')
            ->with('url_query', $url_query);

    }

    /**
     * Edit
     *
     * @param int $bayesiantask_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */

    public function edit($bayesiantask_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.edit')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }
        $advertiser_names = $this->getAdvertisers();
        $bayesiantask = BayesianTasks::find($bayesiantask_id);
        if (!$bayesiantask) {
            throw new \Exception('the task you have provided does not exists');
        }

        return view('pages.bo.bayesiantasks.form')
            ->with('bayesiantask', $bayesiantask)
            ->with('advertisers_list',$advertiser_names);
    }

    /**
     * Approve
         *
     * @param int $bayesiantasks_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function approve($bayesiantasks_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.edit')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }

        $bayesiantask = BayesianTasks::find($bayesiantasks_id);

        if (!$bayesiantask) {
            throw new \Exception('the task you have provided does not exists');
        }

        return view('pages.bo.bayesiantasks.form')
            ->with('bayesiantask', $bayesiantask);
    }

    /**
     * Stop
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function stop(Request $request)
    {
        if (!auth()->user()->can(self::PERMISSION . '.edit')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }
        $bayesiantasks_id = $request->all()['id'];
        $bayesiantask = BayesianTasks::find($bayesiantasks_id);

        if (!$bayesiantask) {
            throw new \Exception('the task you have provided does not exists');
        }
        $bayesiantask->setAttribute('task_status','done');
        $bayesiantask->save();
        $url_query = $request->all();
        return redirect()->route('bo.bayesiantasks.index')
            ->with('toastr_msgs', ["success" => "Task has been stopped"]);
    }

    /**
     * Store
     *
     * @param BayesianTasksFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(BayesianTasksFormRequest $request)
    {
        $excluded_columns = ['task_status'];
        $req = $request->all();
        $err = $this->validateBasianForm($req);

        if($err){
            return redirect()->route('bo.bayesiantasks.create')
                ->with('toastr_msgs', ["Error" => $err]);
        }

        foreach ($req as $key => $val){
            if(!in_array($key,$excluded_columns)){
                $req[strtoupper($key)] = $val;
                array_forget($req,$key);
            }
        }
        # Create new record in DB, on duplicate error redirect back
        try {
            BayesianTasks::create($req);
        } catch (\Exception $e) {
            if (strpos($e, 'Duplicate entry') !== false) {
                return redirect()->back()->with(self::MESSAGE_TOAST, ["Error" => "Tasks with current key already exists."]);
            } else {
                return redirect()->back()->with(self::MESSAGE_TOAST, ["Error" => $e]);
            }
        }

        return redirect()->route('bo.bayesiantasks.index')
            ->with('toastr_msgs', ["success" => "Task has been added"]);
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $advertiser_names = $this->getAdvertisers();
        if (auth()->user()->can(self::PERMISSION .'.add')) {
            return view('pages.bo.bayesiantasks.form')
                ->with('advertisers_list',$advertiser_names);
        } else {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["Error" => "You do not have permissions to view this page."]);
        }
    }

    public function validateBasianForm(&$request){
        if($request['learn_prior'] == '1' and !$request['days_for_prior']){
            return 'You have to fill days_for_prior with lear_prior ';

        }else if($request['learn_prior'] == '0' and $request['days_for_prior']){
            return 'You cant fill days_for_prior without learn_prior';

        }else if((in_array($request['expirament_type'],array('__design_pc', '__design_mobile'))
                and ( $request['test_description'] == '' or $request['lineup_label_a'] != '' or $request['lineup_label_b'] != ''))){
            return 'For design type you have to fill in test_description and leave lineup labels empty';

        }else if( (in_array($request['expirament_type'],array('__lineup_label_pc, __lineup_label_mobile'))
            and ( $request['test_description'] != '' or $request['lineup_label_a'] == '' or $request['lineup_label_b'] == ''))){
            return 'For lineup_label type you have to fill in lineup labels and leave test_description empty';
        }
        else if($request['end_date'] && !$request['RUN_ONCE'] ){
            $request['run_once'] = 1;
        }else if(!$request['end_date'] && $request['RUN_ONCE']){
            $request['end_date'] = date('Y-m-d',strtotime('-1 days'));
        }
        return false;

    }
    private function getAdvertisers(){
        $allowed_advertisers = session('user_advertiser_access');
        $adv_ids = Advertiser::select('id','name')->whereIn('id',$allowed_advertisers)->get();

        return $adv_ids;
    }


    /**
     * Update
     *
     * @param UsersFormRequest $request
     * @param int $user_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(BayesianTasksFormRequest $request, $bayesiantasks_id)
    {
        $req = $request->all();
        $err = $this->validateBasianForm($req);

        if($err){
            $bayesiantask = BayesianTasks::find($bayesiantasks_id);

            if (!$bayesiantask) {
                throw new \Exception('the task you have provided does not exists');
            }
            return redirect()->route('bo.bayesiantasks.edit',$bayesiantasks_id)
                ->with('bayesiantask', $bayesiantask)
                ->with('toastr_msgs', ["Error" => $err]);
        }

        $ignored_columns = ['_token', '_method', 'updated_at', 'created_at','advertiser_id','LAST_RUN'];
        foreach ($req as $key => $val){
            if(!in_array($key ,$ignored_columns)){
                switch ($val){
                    case "True":
                        $val = 1;
                        break;
                    case "False":
                        $val = 0;
                        break;
                    case "":
                        $val = null;
                        break;
                    default:
                        break;

                }
                $req[strtoupper($key)] = $val;
            }
            array_forget($req,$key);
        }

        try {
            $builder = \DB::connection('bo')->table('bayesian_tasks');

            insertOnDuplicateKeyUpdate($builder, array($req));

        } catch (\Exception $e) {
            throw new \Exception('Error in Update method - connect and update bayesian tasks');
        }

        return redirect()->route('bo.bayesiantasks.index')
            ->with('toastr_msgs', ["success" => "Tasks has been updated"]);
    }


    private function dataTable_bayesiantasksQuery($params, DatatableLib $datatable)
    {
        $params['recordsTotal'] = BayesianTasks::all()->count();
        $params['searchColumns'] = ['test_name','advertiser_name','page_name','expirament_type'];

        $datatable->addColumn('id');
        $datatable->renderColumns([
                'test_name' => 'test_name',
                'ab_testing_sale' => 'ab_testing_sale',
                'ab_testing_lead' => 'ab_testing_lead',
                'ab_testing_ctr' => 'ab_testing_ctr',
                'learn_prior' => 'learn_prior',
                'days_for_prior' => 'days_for_prior',
                'ab_testing_rpm' => 'ab_testing_rpm',
                'expirament_type' => 'expirament_type',
                'advertiser_name' => 'advertiser_name',
                'page_name' => 'page_name',
                'dates_to_omit' => 'dates_to_omit',
                'dates_to_omit_prior' => 'dates_to_omit_prior',
                'start_date' => 'start_date',
                'end_date' => 'end_date',
                'lineup_label_a' => 'lineup_label_a',
                'lineup_label_b' => 'lineup_label_b',
                'test_description' => 'test_description',
                'task_status' => 'task_status',
                'actions' => function ($obj) {
                    $res = '';
                    $res .= "<a href='" . route('bo.bayesiantasks.edit', [$obj['_id']]) . "'
                        title='Edit' class='tooltips' ><span aria-hidden='true' class='icon-pencil'></span></a> ";
                    return $res;
                }
            ]
        );


        $params['query'] = BayesianTasks::select($datatable->getColumns());
        return $datatable->fill($params);
    }

}
