<?php


namespace App\Http\Controllers\Bo;

use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Bo\BrandGroup;
use App\Entities\Models\Bo\BudgetManagerAlert;
use App\Entities\Models\Bo\Companies;
use App\Entities\Models\Bo\Publishers\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\UsersFormRequest;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class BudgetManagerController extends Controller
{
    use DataTableTrait;
    use Select2Trait;

    const PERMISSION = 'bo.budget_manager';
    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'BudgetManager';

    /**
     * Index - main screen
     *
     * @param Request $request
     *
     * @return $this
     */
    public function index(Request $request)
    {

        $url_query = $request->all();

        if (!auth()->user()->can(self::PERMISSION.'.view')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }

        $brand_groups  = $this->getbrandGoupsByAdvertiser();
        $landing_pages = $this->getLPByBrandGroup($brand_groups);
        $companies = $this->getCompaniesByAdvertiser($brand_groups);
        $allowed_advertisers = session('user_advertiser_access');
        $budgets = BudgetManagerAlert::select('*')->whereIn('advertiser_id',$allowed_advertisers)->get()->toArray();

        // Create list of existing dates (Month - Year)
        $dates_list = array_map(function($row){
            return date("M-Y", strtotime($row));
        },array_column($budgets,'from_date')) ;
        $dates_list = array_unique($dates_list);
        // Sort the dates
        usort($dates_list, function ($a, $b) {
            return strtotime($a) - strtotime($b);
        });

        return view('pages.bo.budget_manager.index')
            ->with('advertisers',  BudgetManagerAlert::select('id', 'advertiser_name')->whereIn('advertiser_id',$allowed_advertisers)->groupBy('advertiser_name')->get()->pluck('advertiser_name','advertiser_name'))
            ->with('devices',  BudgetManagerAlert::select('id', 'device')->whereIn('advertiser_id',$allowed_advertisers)->groupBy('device')->get()->pluck('device','device')->push('none'))
            ->with('group_names', BudgetManagerAlert::select('id', 'brand_group_name')->whereIn('advertiser_id',$allowed_advertisers)->groupBy('brand_group_name')->get()->pluck('brand_group_name','brand_group_name')->push('none'))
            ->with('full_group_names',$brand_groups)
            ->with('full_company_names',$companies)
            ->with('landing_pages',$landing_pages)
            ->with('dates_list',$dates_list)
            ->with('budget_names', BudgetManagerAlert::select('id', 'budget_name')->whereIn('advertiser_id',$allowed_advertisers)->groupBy('budget_name')->get()->pluck('budget_name','budget_name'))
            ->with('budgets',json_encode($budgets));
    }

    private function getLPByBrandGroup($brand_groups){

        $lp_list = \DB::connection('bo')->table('out_landingpages as lp')
            ->select('lp.id','lp.name','ca.brand_id','ob.brands_group_id')
            ->join('bo.out_campaigns as ca','lp.campaign_id','=','ca.id')
            ->join('bo.out_brands as ob','ca.brand_id','=','ob.id')
            ->whereIn('brands_group_id',$brand_groups)
            ->get()->toArray();
        return $lp_list;
    }

    private function getbrandGoupsByAdvertiser(){
        $allowed_advertisers = session('user_advertiser_access');
        $brand_group_ids     = Brand::select('brands_group_id')->whereIn('advertiser_id',$allowed_advertisers)->get();
        $brand_groups        = BrandGroup::select('id','name','company_id')->whereIn('id',$brand_group_ids)->get();
        return $brand_groups;
    }
    private function getCompaniesByAdvertiser($brand_group_ids){
        $companies = Companies::select('id','name')->whereIn('id',$brand_group_ids->pluck('company_id'))->get();
        return $companies;
    }

    public function getData()
    {
        if (!auth()->user()->can(self::PERMISSION.'.view')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }
        $allowed_advertisers = session('user_advertiser_access');
        $budgets = BudgetManagerAlert::select('*')->whereIn('advertiser_id',$allowed_advertisers)->get()->toArray();

        return \Response::json($budgets);
    }

    /**
     * Store
     *
     * @param UsersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $req = $_POST;
        $this->improveStoreRequest($req,true);

        # Create new record in DB, on duplicate error redirect back
        try{
            BudgetManagerAlert::create($req);
        }catch(\Exception $e){
            if(strpos($e, 'Duplicate entry') !== false){
                return response()->json(['error' => "Brand group for this month with current currency already exists."]);
            }else {
                return response()->json(['error' => $e]);
            }
        }
        return response()->json(new \stdClass());
    }
    /**
     * Store
     *
     * @param UsersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete()
    {
        $req = $_POST;

        # Create new record in DB, on duplicate error redirect back
        try{
            BudgetManagerAlert::where('id','=',$req['id'])->delete();
        }catch(\Exception $e){
            if(strpos($e, 'Duplicate entry') !== false){
                return response()->json(['error' => "Brand group for this month with current currency already exists."]);
            }else {
                return response()->json(['error' => $e]);
            }
        }
        return response()->json(new \stdClass());
    }
    /**
     * Modify request
     * @param array $req
     */
    private function improveStoreRequest(array &$req, $is_new=false)
    {
        try{
            try{
                if($req['advertiser_id']){
                    $req['advertiser_name'] = Advertiser::select('name')->where('id','=',$req['advertiser_id'])->first()->toArray()['name'];
                }else{
                    // If brand group was selected
                    if($req['brand_group_id']){
                        $req['advertiser_id']   = Brand::select('advertiser_id')->where('brands_group_id','=',$req['brand_group_id'])->first()->toArray()['advertiser_id'];
                        $req['advertiser_name'] = Advertiser::select('name')->where('id','=',$req['advertiser_id'])->first()->toArray()['name'];
                    }else{  // If company was selected
                        $brand_group_id = BrandGroup::select('id')->where('company_id','=',$req['company_id'])->first()->toArray()['id'];
                        $req['advertiser_id'] = Brand::select('advertiser_id')->where('brands_group_id','=',$brand_group_id)->first()->toArray()['advertiser_id'];
                        $req['advertiser_name'] = Advertiser::select('name')->where('id','=',$req['advertiser_id'])->first()->toArray()['name'];
                    }
                }
            }catch (\Exception $e){
                $req['advertiser_name'] = 'Not Found';
            }

            if($is_new)
                $req['id'] = BudgetManagerAlert::max('id') + 1;

            $req = array_map(function($row){
                if ($row == 'null' || !$row) {
                    return NULL;
                }else{
                    return $row;
                }
            },$req);

            $req['restrict'] = $req['restrict'] === 'Yes' ? 1 : 0;

        }catch (\Exception $e){

            throw new Exception('Error in improveStoreRequest function, BudgetManagerController.php, error message : ' . $e);
        }
    }

    /**
     * Update
     *
     * @param UsersFormRequest $request
     * @param int $user_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update()
    {
        $req = $_POST;
        $budget_manager_id = $req['id'];
        $this->improveStoreRequest($req);
        $ignored_columns = ['out_clicks','updated_at','created_at','usage','forecast'];

        try{
            $builder = \DB::connection('alert')->table('budget_controller');
            insertOnDuplicateKeyUpdate($builder,array($req),$ignored_columns);
        }catch (\Exception $e){
            throw new \Exception('Error in Update method for budget_manager');
        }
        return response()->json(new \stdClass());
    }

}
