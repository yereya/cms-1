<?php

namespace App\Http\Controllers\Bo;

use App\Entities\Repositories\Bo\Advertisers\AdvertisersRepo;

use App\Http\Controllers\Controller;
use App\Http\Requests\Bo\CompaniesRequest;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Bo\OutInterfaces\CompaniesLib;
use App\Libraries\Bo\OutInterfaces\OutConnection;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    use DataTableTrait;

    const PERMISSION = 'bo.companies';

    /**
     * @var array $assets
     */
    protected $assets = ['datatables', 'x-editable', 'form', 'bo-advertisers'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    /**
     * Index - main screen
     **
     *
     * @return $this
     */
    public function index()
    {
        $companies = (new CompaniesLib())->all()['data'];

        return view('pages.bo.companies.index')
            ->with(compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.bo.companies.form');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CompaniesRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(CompaniesRequest $request)
    {
        $values  = $this->normalizeValues($request);
        $company = $this->getModel($values);

        if ($company['status']) {
            return redirect()->route('bo.companies.index')
                ->with(self::MESSAGE_TOAST, "Company (Name: " .  $company['data']->name . ") created!");
        } else {
            return redirect()->route('bo.companies.create')
                ->with(self::MESSAGE_TOAST, ["warning" => $company['message']])
                ->withInput((array) $company['data']);
        }
    }
//
//    /**
//     * Display the specified resource.
//     *
//     * @param  int $id
//     * @return \Illuminate\Http\Response
//     */
//    public function show($id)
//    {
//        $advertisers_lib = new AdvertisersLib();
//
//        return view('pages.bo.companies.index')
//            ->with('advertiser', $advertisers_lib->find($id))
//            ->with('ios', $advertisers_lib->brands($id));
//    }
//
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = (new CompaniesLib())->find($id)['data'];
        return view('pages.bo.companies.form')
            ->with(compact('company'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param CompaniesRequest $request
     * @param  int             $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(CompaniesRequest $request, $id)
    {
        $values  = $this->normalizeValues($request);
        $company = $this->getModel($values, $id);

        if ($company['status']) {
            return redirect()->route('bo.companies.index', [$id])
                ->with(self::MESSAGE_TOAST, ["success" => "Advertiser (Name: {$company['data']->name}) updated!"]);
        } else {
            return redirect()->route('bo.companies.index')
                ->with(self::MESSAGE_TOAST, ["error: " => $company['message']])
                ->withInput((array) $company['data']);
        }
    }

    /**
     * filter advertiser rows by given parameter pairs
     *
     * @param Request $request
     * @return array
     * @internal param array $pairs
     */
    public function filter($request)
    {
        $condition_pairs = $request;
        $companies = (new CompaniesLib())->all();
        $filtered_companies = [];

        try {
            foreach($companies as $company) {
                $condition = true;
                // Current advertiser should match each of the expected values
                foreach($condition_pairs as $attribute => $expected_value) {
                    $condition = $condition
                        && ($company->{$attribute} ?? false)
                        && ($company->{$attribute} == $expected_value);
                }

                if ($condition)
                    $filtered_advertisers[] = $company;
            }
        } catch(\Exception $e) {
            return redirect()->route('bo.company.index')
                ->with(self::MESSAGE_TOAST, ["error" => "Advertiser filtering went wrong: " . $e->getMessage()]);
        }

        return $filtered_companies;
    }

    /**
     * Interface with out
     * create/update
     *
     * @param array $values
     * @param null  $company_id
     *
     * @return $this|mixed
     */
    private function getModel($values = [], $company_id = null)
    {
        $model = new CompaniesLib();
        try {
            if ($company_id) {
                return $model->update($company_id, $values);
            } else {
                return $model->create($values);
            }
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'message' => $e->getMessage(),
                'data' => []
            ];
        }
    }


    /**
     * @param array $request
     *
     * for some reason the key converted to lowercase and separate by underscore so i need to normalize it to it correct name
     * todo we need to check it
     *
     * @return mixed
     */
    private function normalizeValues($request)
    {
        $values = $request->all();
        $values['SAP_id'] = $values['s_a_p_id'];
        unset ($values['s_a_p_id']);

        return $values;
    }
}


