<?php

namespace App\Http\Controllers\Bo\Publishers;

use App\Entities\Models\Bo\Publishers\Media;
use App\Entities\Models\Bo\Publishers\Publisher;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Libraries\Bo\OutInterfaces\OutMedia;
use Illuminate\Http\Request;

class MediasController extends Controller
{
    const PERMISSION = 'bo.publisher.media';

    /**
     * @var array - assets (js, css)
     */
    protected $assets = ['datatables', 'form'];

    /**
     * Display a listing of the resource.
     *
     * @param Publisher $publisher
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Publisher $publisher)
    {
        return view('pages.bo.publishers.medias.index')
            ->withPublisher($publisher)
            ->withMedias(Media::wherePublisherId($publisher->id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Publisher $publisher
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Publisher $publisher)
    {
        return view('pages.bo.publishers.medias.media_form')
            ->withPublisher($publisher);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Requests\MediaRequest $request
     * @param Publisher              $publisher
     *
     * @return \Illuminate\Http\Response
     * @internal param $publisher_id
     */
    public function store(Requests\MediaRequest $request, Publisher $publisher)
    {
        $values = $request->all();
        $media  = new Media();
        $media->fill($values);
        $media->publisher_id = $publisher->id;

        $out = new OutMedia();

        try {
            $mongo = $out->storeMedia($this->buildOutValues($publisher->mongodb_id, $values, null));
            if (isset($mongo['status']) && $mongo['status']) {
                $media->mongodb_id = $mongo['message'];
                $media->save();
            }

            return redirect()->route('bo.publishers.{publisher}.medias.index', $publisher->id)
                ->with(self::MESSAGE_TOAST, ["success" => "Media (Name: $media->media_name) created!"]);
        } catch (\Exception $e) {
            return redirect()->route('bo.publishers.{publisher}.medias.index', $publisher->id)
                ->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Publisher $publisher
     * @param Media     $medias
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Publisher $publisher, Media $medias)
    {
        return view('pages.bo.publishers.medias.media_form')
            ->withMedia($medias)
            ->withPublisher($publisher);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\MediaRequest|Request $request
     * @param Publisher                     $publisher
     * @param Media                         $medias
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\MediaRequest $request, Publisher $publisher, Media $medias)
    {
        $values = $request->all();
        $medias->fill($values);

        $out = new OutMedia();

        try {
            $mongo = $out->setMedia($medias->mongodb_id,
                $this->buildOutValues($publisher->mongodb_id, $values, $medias->mongodb_id));

            if (isset($mongo['status']) && $mongo['status']) {
                $medias->save();
            }

            return redirect()->route('bo.publishers.{publisher}.medias.index', $publisher->id)
                ->with(self::MESSAGE_TOAST, ["success" => "Media (Name: $medias->media_name) created!"]);
        } catch (\Exception $e) {
            return redirect()->route('bo.publishers.{publisher}.medias.index', $publisher->id)
                ->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }

    /**
     * Build params to out request
     *
     * @param      $publisher_mongo_id
     * @param      $values
     * @param null $media_mongo_id - if create this param null, if update not null
     *
     * @return array
     */
    private function buildOutValues($publisher_mongo_id, $values, $media_mongo_id = null)
    {
        $mongo_vals = [
            'pubId'  => $publisher_mongo_id,
            'name'   => $values['media_name'],
            'type'   => snakeToWords($values['type']),
            'status' => snakeToWords($values['status']),
        ];

        if ($media_mongo_id) {
            $mongo_vals['_id'] = $media_mongo_id;
        }

        return $mongo_vals;
    }
}
