<?php

namespace App\Http\Controllers\Bo\Publishers;

use App\Entities\Models\Bo\Publishers\Pixel;
use App\Entities\Models\Bo\Publishers\Publisher;
use App\Http\Requests\Bo\Publishers\PixelsRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PixelsController extends Controller
{
    protected $assets = ['datatables', 'x-editable', 'form'];
    /**
     * Show the form for creating a new resource.
     *
     * @param $publisher_id
     * @return \Illuminate\Http\Response
     */
    public function create($publisher_id)
    {
        return view('pages.bo.publishers.pixels.pixel_form')
            ->with('publisher', Publisher::find($publisher_id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PixelsRequest $request
     * @param $publisher_id
     * @return \Illuminate\Http\Response
     */
    public function store(PixelsRequest $request, $publisher_id)
    {
        $values = $request->all();

        $pixel = new Pixel();
        try {
            $pixel->name = $values['name'];
            $pixel->publisher_id = $publisher_id;
            $pixel->url = $values['url'];

            $pixel->save();

            return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher_id)
                ->with('toastr_msgs', ["success" => "The Publisher Account {$pixel->name} added"]);
        } catch (Exception $e) {
            return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher_id)
                ->with('toastr_msgs', ["error" => "The Publisher Account not saved: " . $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $publisher_id
     * @param $pixel_id
     * @return \Illuminate\Http\Response
     */
    public function edit($publisher_id, $pixel_id)
    {
        return view('pages.bo.publishers.pixels.pixel_form')
            ->with('publisher', Publisher::find($publisher_id))
            ->with('pixel', Pixel::find($pixel_id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PixelsRequest $request
     * @param $publisher_id
     * @param $pixel_id
     * @return \Illuminate\Http\Response
     */
    public function update(PixelsRequest $request, $publisher_id, $pixel_id)
    {
        $values = $request->all();

        $pixel = Pixel::find($pixel_id);
        try {
            $pixel->fill($values);

            $pixel->save();

            return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher_id)
                ->with('toastr_msgs', ["success" => "The Publisher Account {$pixel->name} added"]);
        } catch (Exception $e) {
            return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher_id)
                ->with('toastr_msgs', ["error" => "The Publisher Account not saved: " . $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $publisher_id
     * @param $pixel_id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy($publisher_id, $pixel_id)
    {
        $pixel = Pixel::find($pixel_id);
        $name = $pixel->name;

        try {
            $pixel->delete();
            return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher_id)
                ->with('toastr_msgs', ["success" => "The Publisher Account {$name} added"]);
        } catch (Exception $e) {
            return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher_id)
                ->with('toastr_msgs', ["error" => "The Publisher Account not saved: " . $e->getMessage()]);
        }
    }
}
