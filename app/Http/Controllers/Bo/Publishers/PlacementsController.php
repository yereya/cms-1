<?php

namespace App\Http\Controllers\Bo\Publishers;

use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Bo\Publishers\Brand;
use App\Entities\Models\Bo\Publishers\Campaign;
use App\Entities\Models\Bo\Publishers\Media;
use App\Entities\Models\Bo\Publishers\Placement;
use App\Entities\Models\Bo\Publishers\Publisher;
use App\Http\Requests\Bo\Publishers\PlacementsRequest;
use App\Libraries\Bo\OutInterfaces\OutPlacement;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PlacementsController extends Controller
{
    const PERMISSION = 'bo.publisher.media.placement';

    /**
     * @var array $assets
     */
    protected $assets = ['datatables', 'x-editable', 'tags-input',  'form'];

    /**
     * Display a listing of the resource.
     *
     * @param Publisher $publisher
     * @param Media $medias
     * @return \Illuminate\Http\Response
     */
    public function index(Publisher $publisher, Media $medias)
    {
        return view('pages.bo.publishers.medias.placement.index')
            ->withPublisher($publisher)
            ->withMedia($medias)
            ->withPlacements(Placement::where('media_id', $medias->id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Publisher $publisher
     * @param Media $medias
     * @return \Illuminate\Http\Response
     */
    public function create(Publisher $publisher, Media $medias)
    {
        return view('pages.bo.publishers.medias.placement.placement_form')
            ->withPublisher($publisher)
            ->withMedia($medias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PlacementsRequest|Request $request
     * @param Publisher $publisher
     * @param Media $medias
     * @return \Illuminate\Http\Response
     */
    public function store(PlacementsRequest $request, Publisher $publisher, Media $medias)
    {
        $campaign = Campaign::find($request->get('campaign'));
        $placement = new Placement();
        $values = [
            'placement_name' => $request->get('placement_name'),
            'media_id' => $medias->id,
            'publisher_id' => $publisher->id,
            'pubId_mongodb_id' => $publisher->mongodb_id,
            'mediaId_mongodb_id' => $medias->mongodb_id,
            'campaign_id' => $campaign->id,
            'campaign_mongodb_id' => $campaign->mongodb_id
        ];

        $out = new OutPlacement();

        try {
            $mongo = $out->storePlacement($this->buildOutValues($values, null));
            if ($mongo['status']) {
                $values['mongodb_id'] = $mongo['message'];
                $placement->fill($values);
                $placement->save();
            }

            return redirect()->route('bo.publishers.{publisher}.medias.{medias}.placements.index', [$publisher->id, $medias->id])
                ->with('toastr_msgs', ["success" => "The Placement {$placement->placement_name} added"]);
        } catch (Exception $e) {
            return redirect()->route('bo.publishers.{publisher}.medias.{medias}.placements.index', [$publisher->id, $medias->id])
                ->with('toastr_msgs', ["error" => "The Placement not saved: " . $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Publisher $publisher
     * @param Media $medias
     * @param Placement $placements
     * @return \Illuminate\Http\Response
     */
    public function edit(Publisher $publisher, Media $medias, Placement $placements)
    {
        if (empty($placements->campaign_id)) {
            return redirect()->route('bo.publishers.{publisher}.medias.{medias}.placements.index', [$publisher->id, $medias->id])
                ->with('toastr_msgs', ["error" => "This data not correctly, placement not linked with campaign"]);
        }

        $campaign = Campaign::find($placements->campaign_id);
        $brand = Brand::find($campaign->brand_id);
        return view('pages.bo.publishers.medias.placement.placement_form')
            ->withPublisher($publisher)
            ->withMedia($medias)
            ->withPlacement($placements)
            ->withCampaignId($placements->campaign_id)
            ->withBrandId($campaign->brand_id)
            ->withAdvertiserId($brand->advertiser_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PlacementsRequest|Request $request
     * @param Publisher $publisher
     * @param Media $medias
     * @param Placement $placements
     * @return \Illuminate\Http\Response
     */
    public function update(PlacementsRequest $request, Publisher $publisher, Media $medias, Placement $placements)
    {
        $campaign = Campaign::find($request->get('campaign'));
        $values = [
            'placement_name' => $request->get('placement_name'),
            'media_id' => $medias->id,
            'publisher_id' => $publisher->id,
            'pubId_mongodb_id' => $publisher->mongodb_id,
            'mediaId_mongodb_id' => $medias->mongodb_id,
            'campaign_id' => $campaign->id,
            'campaign_mongodb_id' => $campaign->mongodb_id
        ];

        $out = new OutPlacement();

        try {
            $mongo = $out->setPlacement($placements->mongodb_id, $this->buildOutValues($values));
            if ($mongo['status']) {
                $placements->fill($values);
                $placements->save();
            }

            return redirect()->route('bo.publishers.{publisher}.medias.{medias}.placements.index', [$publisher->id, $medias->id])
                ->with('toastr_msgs', ["success" => "The Placement {$placements->placement_name} updated"]);
        } catch (Exception $e) {
            return redirect()->route('bo.publishers.{publisher}.medias.{medias}.placements.index', [$publisher->id, $medias->id])
                ->with('toastr_msgs', ["error" => "The Placement not saved: " . $e->getMessage()]);
        }
    }

    /**
     * Search brands by advertiser id
     *
     * @param Request $request
     * @return array
     */
    public function searchBrand(Request $request) {
        $advertiser_id = $request->get('value');
        if ($advertiser_id) {
            return ['status' => 'success', 'data' => Brand::select('id', 'name')->where('advertiser_id', $advertiser_id)->get()->toArray()];
        }
        return ['status' => 'error', 'data' => -1];
    }

    /**
     * Search campaigns by brand id
     *
     * @param Request $request
     * @return array
     */
    public function searchCampaign(Request $request) {
        $brand_id = $request->get('value');
        if ($brand_id) {
            return ['status' => 'success', 'data' => Campaign::select('id', 'name')->where('brand_id', $brand_id)->get()->toArray()];
        }
        return ['status' => 'error', 'data' => -1];
    }

    public function showLinks($publisher, $medias, $placement) {
        $plac = Placement::find($placement);
        $campaign = Campaign::find($plac->campaign_id);
        $brand = Brand::find($campaign->brand_id);
        $advertiser = Advertiser::find($brand->advertiser_id);

        $link = $advertiser->domain . '/track/click/?pid=' . $plac->mongodb_id;

        $suffix = [
            'none' => $link,
            'search' => $link . '&creative={creative}&keyword={keyword}&device={device}&network={network}&adposition={adposition}&matchtype={matchtype}&sid=<SID>',
            'display' => $link . '&creative={creative}&placement={placement}&device={device}&network={network}&adposition={adposition}&matchtype={matchtype}&sid=<SID>',
            'msn' => $link . '&creative={AdId}&device={device}&keyword={keyword}&network=msn&QueryString={QueryString}&msclkid={msclkid}&sid=<SID>',
            'facebook' => $link . '&age={age}&gender={gender}&sid=<SID>'
        ];

        return view('pages.bo.publishers.medias.placement.link_modal')
            ->withSuffix($suffix)
            ->withLink($link);
    }

    /**
     * Build params to out request
     *
     * @param $values
     * @param null $placement_mongodb_id - if create this param null, if update not null
     * @return array
     */
    private function buildOutValues($values, $placement_mongodb_id = null)
    {
        $mongo_vals = [
            'pubId' => $values['pubId_mongodb_id'],
            'mediaId' => $values['mediaId_mongodb_id'],
            'name' => $values['placement_name'],
            'type' => 'Direct Campaign',
            'data' => [
                'campaignId' => $values['campaign_mongdbo_id']
            ]
        ];

        if ($placement_mongodb_id) {
            $mongo_vals['_id'] = $placement_mongodb_id;
        }

        return $mongo_vals;
    }
}
