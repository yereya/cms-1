<?php

namespace App\Http\Controllers\Bo\Publishers;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\Conversion;
use App\Entities\Models\Bo\Publishers\Pixel;
use App\Entities\Models\Bo\Publishers\Publisher;
use App\Entities\Models\Bo\Publishers\PublisherAccountParams;
use App\Http\Requests\Bo\Publishers\AccountsRequest;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\CountValidator\Exception;

class AccountsController extends Controller
{
    protected $assets = ['datatables', 'x-editable', 'tags-input', 'form', 'bo_advertisers'];

    const PERMISSION = 'bo.publishers.accounts';

    /**
     * Display a listing of the resource.
     *
     * @param $publisher_id
     * @return \Illuminate\Http\Response
     */
    public function index($publisher_id)
    {
        $publisher = Publisher::find($publisher_id);

        if ($publisher->type == 'normal') {
            $results = Account::where('publisher_id', $publisher_id)->where('is_media', true)->get();
            $view = 'pages.bo.accounts.media.index';
            $with = 'medias';
        } else {
            $results = Account::where('publisher_id', $publisher_id)->where('is_media', false)->get();
            $view = 'pages.bo.accounts.account.index';
            $with = 'accounts';
        }

        return view($view)
            ->with('publisher', Publisher::find($publisher_id))
            ->with($with, $results)
            ->with('pixels', Pixel::where('publisher_id', $publisher_id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $publisher_id
     * @return \Illuminate\Http\Response
     */
    public function create($publisher_id)
    {
        $publisher = Publisher::find($publisher_id);

        if ($publisher->type == 'normal') {
            $view = 'pages.bo.accounts.media.media_form';
        } else {
            $view = 'pages.bo.accounts.account.account_form';
        }

        return view($view)
            ->with('publisher', $publisher);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $publisher_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $publisher_id)
    {
        $values = $request->all();
        $publisher = Publisher::find($publisher_id);

        if ($publisher->type == 'normal') {
            $message = $this->storeMedia($publisher, $values);
        } else {
            $message = $this->storeAccount($publisher, $values);
        }

        return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher)
            ->with('toastr_msgs', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $publisher_id
     * @param $account_id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit($publisher_id, $account_id)
    {
        $publisher = Publisher::find($publisher_id);
        $account = Account::find($account_id);
        if ($account->is_media) {
            return view('pages.bo.accounts.media.media_form')
                ->with('media', $account)
                ->with('publisher', $publisher);
        } else {
            $paccs = PublisherAccountParams::where('publisher_account_id', $account_id)->get();
            return view('pages.bo.accounts.account.account_form')
                ->with('account', $account)
                ->with('publisher', $publisher)
                ->with('accounts', $paccs);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AccountsRequest $request
     * @param $publisher_id
     * @param $account_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $publisher_id, $account_id)
    {
        $values = $request->all();
        $publisher = Publisher::find($publisher_id);
        $account = Account::find($account_id);
        if (!$account) {
            return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher_id)
                ->with('toastr_msgs', ['error', 'Not found this Publisher Account!']);
        }

        if ($account->is_media) {
            $message = $this->updateMedia($account, $values);
        } else {
            $message = $this->updateAccount($account, $values);
        }

        return redirect()->route('bo.publishers.{publisher_id}.accounts.index', $publisher)
            ->with('toastr_msgs', $message);
    }

    private function storeAccount($publisher, $values){
        $account = new Account();
        try {
            $account->name = $values['name'];
            $account->publisher_id = $publisher->id;
            $account->status = $values['status'];

            if (isset($values['conversions'])) {
                $account->lead = strpos($values['conversions'], 'lead') ? true : false;
                $account->canceled_sale = strpos($values['conversions'], 'canceled_sale') ? true : false;
                $account->canceled_lead = strpos($values['conversions'], 'canceled_lead') ? true : false;
                $account->adjustment = strpos($values['conversions'], 'adjustment') ? true : false;
                $account->sale = strpos($values['conversions'], 'sale') ? true : false;
                $account->click_out = strpos($values['conversions'], 'click_out') ? true : false;
                $account->install = strpos($values['conversions'], 'install') ? true : false;
                $account->call = strpos($values['conversions'], 'call') ? true : false;
            }

            $account->save();

            if (isset($values['accounts'])) {
                $accounts = explode(' ,', $values['accounts']);

                foreach ($accounts as $acc) {
                    $id = Account::where('name', $acc)->first();
                    if ($id) {
                        $relations = new PublisherAccounts();
                        $relations->publisher_account_id = $account->id;
                        $relations->account_id = $id->id;
                        $relations->save();
                    }
                }
            }

            $this->storeConversions($account->source_account_id, $values);

            return ["success" => "The Publisher Account {$account->name} added"];
        } catch (Exception $e) {
            return ["error" => "The Publisher Account not saved: " . $e->getMessage()];
        }
    }

    private function storeMedia($publisher, $values){
        $media = new Account();
        try {
            $media->fill($values);
            $media->is_media = true;
            $media->publisher_id = $publisher->id;
            $media->save();

            return ["success" => "The Publisher Media {$media->name} added"];
        } catch (\Exception $e) {
            return ["error" => "The Publisher Media not saved: " . $e->getMessage()];
        }
    }

    private function updateAccount($account, $values)
    {
        try {
            $account->fill($values);
            $account->save();

            PublisherAccountParams::where('publisher_account_id', $account->id)->delete();
            foreach ($values['accounts'] as $pac) {
                $pacc = new PublisherAccountParams();
                $acc = Account::where('source_account_id', $pac)->first();
                $pacc->publisher_account_id = $account->id;
                $pacc->account_id = $acc->id;
                $pacc->source_account_id = $pac;
                $pacc->save();
            }

            $this->storeConversions($account->source_account_id, $values);

            return ['success' => "This Publisher Account ($account->name) updated successful!"];
        } catch (\Exception $e) {
            return ['error' => 'Error updated this Publisher Account, try again later ' . $e->getMessage()];
        }
    }

    private function updateMedia($media, $values)
    {
        try {
            $media->fill($values);
            $media->save();

            return ['success', "This Publisher Media ($media->name) updated successful!"];
        } catch (\Exception $e) {
            return ['error', "Error updated this Publisher Media, try again later " . $e->getMessage()];
        }
    }

    /**
     * create new row in table conversion if conversion checked in account form
     *
     * @param $source_account_id
     * @param $values
     */
    private function storeConversions($source_account_id, $values) {
        if ($source_account_id && isset($values['conversions'])) {
            foreach ($values['conversions'] as $key => $value) {
                Conversion::create([
                    'track_id' => -1,
                    'source_account_id' => $source_account_id,
                    'clkid' => null,
                    'type' => 'event',
                    'event' => $key,
                    'price' => 0,
                    'timestamp' => time()
                ]);
            }
        }
    }
}
