<?php

namespace App\Http\Controllers\Bo;

use App\Entities\Repositories\Bo\Advertisers\AdvertisersRepo;
use App\Http\Requests\Bo\Advertisers\AdvertisersRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Bo\OutInterfaces\AdvertisersLib;
use App\Libraries\Bo\OutInterfaces\DomainsLib;
use App\Libraries\Bo\OutInterfaces\OutConnection;
use App\Libraries\Datatable\DatatableLib;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class AdvertisersController extends Controller
{
    use DataTableTrait;

    const PERMISSION = 'bo.advertisers';

    /**
     * @var array $assets
     */
    protected $assets = ['datatables', 'x-editable', 'form', 'bo-advertisers'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pages.bo.advertisers.index')
            ->with('advertisers', (new AdvertisersLib())->all()['data'] ?? []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.bo.advertisers.advertiser_form')
            ->with('domains', (new DomainsLib())->all()['data'] ?? []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AdvertisersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdvertisersRequest $request)
    {
        $values = $request->all();
        $advertiser = $this->getModel($values);

        if ($advertiser['status']) {
            return redirect()->route('bo.advertisers.index')
                ->with(self::MESSAGE_TOAST, "Advertiser (Name: " .  $advertiser['data']->name . ") created!");
        } else {
            return redirect()->route('bo.advertisers.create')
                ->with(self::MESSAGE_TOAST, ["warning" => $advertiser['message']])
                ->withInput((array) $advertiser['data']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advertisers_lib = new AdvertisersLib();

        return view('pages.bo.advertisers.brands.index')
            ->with('advertiser', $advertisers_lib->find($id))
            ->with('ios', $advertisers_lib->brands($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.bo.advertisers.advertiser_form')
            ->with('advertiser', (new AdvertisersLib())->find($id))
            ->with('domains', (new DomainsLib())->all()['data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AdvertisersRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(AdvertisersRequest $request, $id)
    {
        $values = $request->all();
        $advertiser = $this->getModel($values, $id);

        if ($advertiser['status']) {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.index', [$id])
                ->with(self::MESSAGE_TOAST, ["success" => "Advertiser (Name: {$advertiser['data']->name}) updated!"]);
        } else {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.index')
                ->with(self::MESSAGE_TOAST, ["error: " => $advertiser['message']])
                ->withInput((array) $advertiser['data']);
        }
    }

    /**
     * filter advertiser rows by given parameter pairs
     *
     * @param Request $request
     * @return array
     * @internal param array $pairs
     */
    public function filter($request)
    {
        $condition_pairs = $request;
        $advertisers = (new AdvertisersLib())->all();
        $filtered_advertisers = [];

        try {
            foreach($advertisers as $advertiser) {
                $condition = true;
                 // Current advertiser should match each of the expected values
                foreach($condition_pairs as $attribute => $expected_value) {
                    $condition = $condition
                        && ($advertiser->{$attribute} ?? false)
                        && ($advertiser->{$attribute} == $expected_value);
                }

                if ($condition)
                    $filtered_advertisers[] = $advertiser;
            }
        } catch(\Exception $e) {
            return redirect()->route('bo.advertisers.index')
                ->with(self::MESSAGE_TOAST, ["error" => "Advertiser filtering went wrong: " . $e->getMessage()]);
        }

        return $filtered_advertisers;
    }

    /**
     * Interface with out
     * create/update
     *
     * @param array $values
     * @param null $advertiser_id
     * @return $this|mixed
     */
    private function getModel($values = [], $advertiser_id = null)
    {
        $model = new AdvertisersLib();
        try {
            if ($advertiser_id) {
                return $model->update($advertiser_id, $values);
            } else {
                return $model->create($values);
            }
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'message' => $e->getMessage(),
                'data' => []
            ];
        }
    }
}


