<?php

namespace App\Http\Controllers\Bo\Advertisers;

use App\Http\Requests\Bo\Advertisers\CampaignGroupRequest;
use App\Http\Requests\Bo\Advertisers\CampaignLandingsRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\Bo\OutInterfaces\AdvertisersLib;
use App\Libraries\Bo\OutInterfaces\BrandsLib;
use App\Libraries\Bo\OutInterfaces\CampaignGroupsLib;
use App\Libraries\Bo\OutInterfaces\CampaignsGroupsLib;
use Illuminate\Http\Request;

class CampaignGroupController extends Controller
{
    const PERMISSION = 'bo.advertisers';

    /**
     * Show the form for creating a new resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function create($advertiser_id, $brand_id)
    {
        return view('pages.bo.advertisers.brands.campaign.campaign-groups.group_form')
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'])
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id)['data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CampaignGroupRequest|CampaignLandingsRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignGroupRequest $request, $advertiser_id, $brand_id)
    {
        $values = $request->all();

        try {
            $campaign_group = $this->getModel($values);

            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index', [$advertiser_id, $brand_id])
                ->with('toastr_msgs', ["success" => $campaign_group['message']]);
        } catch (\Exception $e) {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index',
                [$advertiser_id, $brand_id])
                ->with('toastr_msgs', ["error" => "The Campaign Group not saved " . $e->getMessage()])
                ->withInput((array) $campaign_group['data']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $group_id
     * @return \Illuminate\Http\Response
     */
    public function edit($advertiser_id, $brand_id, $group_id)
    {
        $group = (new CampaignGroupsLib())->find($advertiser_id, $brand_id, $group_id);

        return view('pages.bo.advertisers.brands.campaign.campaign-groups.group_form')
            ->with('group', $group['data'])
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'])
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id)['data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CampaignGroupRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @param $group_id
     * @return \Illuminate\Http\Response
     */
    public function update(CampaignGroupRequest $request, $advertiser_id, $brand_id, $group_id)
    {
        $values = $request->all();
        $status = 'success';

        try {
            $group = (new CampaignGroupsLib())->find($advertiser_id, $brand_id, $group_id);

            if ($group) {
                $group = $this->getModel($values, $group_id);
                $desc = $group['message'];
            } else {
                $status = 'error';
                $desc = "This group was not found!";
            }
        } catch (\Exception $e) {
            $status = 'error';
            $messages = $e->getMessage();
            $desc = "An error occured updating this group, please try again later. $messages";
        }

        return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index',
            [$advertiser_id, $brand_id])
            ->with('toastr_msgs', [$status => $desc])
            ->withInput((array) $group['data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $group_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($advertiser_id, $brand_id, $group_id)
    {
        $status = "error";
        $desc = '';
        $groups_lib = new CampaignGroupsLib();
        $groups_after_image = $groups_lib->find($advertiser_id, $brand_id, $group_id);
        $group = $groups_lib->delete($advertiser_id, $brand_id, $group_id);

        if ($group['status']) {
            $desc = "Campaign group {$groups_after_image['data']->name} has been deleted successfully!";
            $status = "success";
        } else {
            $desc = $group['message'];
        }

        return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index',
            [$advertiser_id, $brand_id])
            ->with('toastr_msgs', [$status => $desc]);
    }

    /**
     * Associate the chosen group with the current campaign
     *
     * @param Request $request
     * @internal param $advertiser_id
     * @internal param $brand_id
     * @internal param $campaign_id
     */
    public function associateGroup(Request $request)
    {
        $values = $request->all();
        $campaign_values = (new CampaignsGroupsLib())->create($values);
    }

    /**
     * Interact with out
     * create/update
     *
     * @param $values
     * @param null $group_id
     * @return $this|mixed
     */
    private function getModel($values, $group_id = null)
    {
        $model = new CampaignGroupsLib();
        try {
            if ($group_id) {
                $values['id'] = $group_id;
                return $model->update($group_id, $values);
            } else {
                return $model->create($values);
            }
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'message' => $e->getMessage(),
                'data' => []
            ];
        }
    }
}
