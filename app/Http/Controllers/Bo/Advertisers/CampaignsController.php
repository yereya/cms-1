<?php

namespace App\Http\Controllers\Bo\Advertisers;

use App\Http\Controllers\Bo\BrandsGroupController;
use App\Http\Requests\Bo\Advertisers\CampaignsRequest;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Bo\OutInterfaces\ActionsLib;
use App\Libraries\Bo\OutInterfaces\AdvertisersLib;
use App\Libraries\Bo\OutInterfaces\BrandsLib;
use App\Libraries\Bo\OutInterfaces\CampaignGroupsLib;
use App\Libraries\Bo\OutInterfaces\CampaignsLib;
use App\Libraries\Bo\OutInterfaces\CompaniesLib;
use App\Libraries\Bo\OutInterfaces\OutCampaigns;

class CampaignsController extends Controller
{
    use AjaxModalTrait;
    use Select2Trait;

    const PERMISSION = 'bo.advertisers';

    protected $assets = ['datatables', 'x-editable', 'form'];

    /**
     * Display a listing of the resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function index($advertiser_id, $brand_id)
    {
        $brand = (new BrandsLib())->find($advertiser_id, $brand_id)['data'] ?? [];
        $actions = (new ActionsLib())->all($advertiser_id, $brand_id)['data'] ?? [];
        $campaign_groups = (new CampaignGroupsLib())->all($advertiser_id, $brand_id)['data'];
        $campaigns = collect($brand->campaigns ?? [])->sortBy(function ($campaign) {
            return $campaign->updatedAt;
        });
        $advertiser = (new AdvertisersLib())->find($advertiser_id)['data'];
        $companies = (new CompaniesLib())->getList();


        /* Create an array - each action id index will have its own url query according to its parameters */
        $actions_url_queries = (new BrandsLib())->getActionsUrlQueries($advertiser_id, $brand_id);
        $brand_group_lists = (new BrandsGroupController())->select3_getBrandsGroupsByCompany($brand->company_id);
        return view('pages.bo.advertisers.brands.campaign.index')
            ->with('advertiser', $advertiser)
            ->with('brand', $brand)
            ->with('campaigns', $campaigns)
            ->with('companies', $companies)
            ->with('campaign_groups', $campaign_groups)
            ->with('actions_url_queries', $actions_url_queries)
            ->with('brand_groups',$brand_group_lists)
            ->with('brand_actions', $actions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function create($advertiser_id, $brand_id)
    {
        return view('pages.bo.advertisers.brands.campaign.campaign_form')
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'])
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id)['data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CampaignsRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignsRequest $request, $advertiser_id, $brand_id)
    {
        $values = $request->all();
        $advertiser = (new AdvertisersLib())->find($advertiser_id);
        $brand = (new BrandsLib())->find($advertiser_id, $brand_id);
        $campaign = $this->getModel($values, $brand['data']->id);

        if ($campaign['status']) {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index', [$advertiser_id, $brand_id])
                ->with('toastr_msgs', ["success" => "The campaign {$campaign['data']->name} has been added"]);
        } else {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.create',
                [$advertiser_id, $brand_id])
                ->with('toastr_msgs', ["error" => $campaign['message']])
                ->withInput((array) $campaign['data']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @return \Illuminate\Http\Response
     */
    public function edit($advertiser_id, $brand_id, $campaign_id)
    {
        return view('pages.bo.advertisers.brands.campaign.campaign_form')
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id))
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id))
            ->with('campaign', (new CampaignsLib())->getCampaign($advertiser_id, $brand_id, $campaign_id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CampaignsRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @return \Illuminate\Http\Response
     * @internal param $route_param
     */
    public function update(CampaignsRequest $request, $advertiser_id, $brand_id, $campaign_id)
    {
        $values = $request->all();
        $status = 'success';

        $route = ($required_id = request()->input('route_param') === 'campaign_edit')
            ? 'bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index'
            : 'bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.index';

        try {
            $advertiser = (new AdvertisersLib())->find($advertiser_id)['data'];
            $brand = (new BrandsLib())->find($advertiser_id, $brand_id)['data'];
            $campaign = (new CampaignsLib())->find($advertiser_id, $brand_id, $campaign_id)['data'];

            if ($campaign) {
                $campaign = $this->getModel($values, $brand->id, $campaign->id);

                if ($campaign) {
                    $desc = $campaign['message'];
                } else {
                    $status = 'error';
                    $desc = "Error OUT" . $campaign['message'];
                }
            } else {
                $status = 'error';
                $desc = "This campaign was not found!";
            }
        } catch (\Exception $e) {
            $status = 'error';
            $messages = $e->getMessage();
            $desc = "An error occured updating this Campaign, please try again later. $messages";
        }

        return redirect()->route($route,
            [$advertiser->id, $brand->id, !$required_id ? $campaign_id : ''])
            ->with('toastr_msgs', [$status => $desc])
            ->withInput((array) $campaign['data']);
    }

    /**
     * Interface with out
     * create/update
     *
     * @param array $values
     * @param $brand_id
     * @param null $campaign_id
     * @return $this|mixed
     */
    private function getModel($values, $brand_id, $campaign_id = null)
    {
        $params = array_merge($values, [
            'id' => $campaign_id ?? '',
            'brand_id' => $brand_id,
            'name' => $values['name'],
            'status' => $values['status'],
            'dynamic' => $values['lp_dynamic'] ?? []
        ]);

        $model = new CampaignsLib();

        if ($campaign_id) {
            return $model->update($campaign_id, $params);
        } else {
            return $model->create($params);
        }
    }

    /**
     * placement link modal
     *
     */
    private function ajaxModal_placementLink()
    {
        $values = request()->all();
        $current_group = (new CampaignsLib())
            ->find($values['advertiser'], $values['brand'], $values['campaign'])['data']->groups[0] ?? [];

        /* Create an array - each group id index will have its own url query according to its parameters */
        $groups_url_queries = (new CampaignGroupsLib())
            ->getGroupsUrlQueries($values['advertiser'], $values['brand']);

        return view('pages.bo.advertisers.brands.campaign.placement_form')
            ->with('brand_id', $values['brand'])
            ->with('domain', $values['domain'])
            ->with('campaign_id', $values['campaign'])
            ->with(compact('current_group'))
            ->with('placement', $values['placement'] ?? null)
            ->with('advertiser_id', $values['advertiser'])
            ->with(compact('groups_url_queries'))
            ->with('groups', (new CampaignGroupsLib())->all($values['advertiser'], $values['brand'])['data']);
    }
}
