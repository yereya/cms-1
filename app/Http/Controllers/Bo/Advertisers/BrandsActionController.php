<?php

namespace App\Http\Controllers\Bo\Advertisers;
use App\Http\Requests\Bo\Advertisers\BrandActionsRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Bo\Advertisers\CampaignGroupRequest;
use App\Libraries\Bo\OutInterfaces\ActionsLib;
use App\Libraries\Bo\OutInterfaces\AdvertisersLib;
use App\Libraries\Bo\OutInterfaces\BrandsLib;

class BrandsActionController extends Controller
{
    const PERMISSION = 'bo.advertisers';

    /**
     * Show the form for creating a new resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function create($advertiser_id, $brand_id)
    {
        return view('pages.bo.advertisers.brands.actions.action_form')
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'])
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id)['data']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandActionsRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function store(BrandActionsRequest $request, $advertiser_id, $brand_id)
    {
        $values = $request->all();
        $action = $this->getModel($values);

        if ($action['status']) {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index', [$advertiser_id, $brand_id])
                ->with('toastr_msgs', ["success" => $action['message']]);
        } else {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.actions.create', [$advertiser_id, $brand_id])
                ->with('toastr_msgs', ["error" => $action['message']])
                ->withInput((array) $action['data']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $action_id
     * @return \Illuminate\Http\Response
     */
    public function edit($advertiser_id, $brand_id, $action_id)
    {
        $action = (new ActionsLib())->find($advertiser_id, $brand_id, $action_id);

        return view('pages.bo.advertisers.brands.actions.action_form')
            ->with('action', $action['data'])
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'])
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id)['data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandActionsRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @param $action_id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandActionsRequest $request, $advertiser_id, $brand_id, $action_id)
    {
        $values = $request->all();
        $action = $this->getModel($values, $action_id);

        // todo route to the brand (campaigns) view
        if ($action['status']) {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index', [$advertiser_id, $brand_id])
                ->with('toastr_msgs', ["success" => $action['message']]);
        } else {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.actions.edit', [$advertiser_id, $brand_id, $action_id])
                ->with('toastr_msgs', ["error" => $action['message']])
                ->withInput((array) $action['data']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $action_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($advertiser_id, $brand_id, $action_id)
    {
        $status = 'error';
        $desc = '';
        $actions_lib = new ActionsLib();
        $action_after_image = (new ActionsLib())->find($advertiser_id, $brand_id, $action_id);
        $action = $actions_lib->delete($advertiser_id, $brand_id, $action_id);

        if ($action['status']) {
            $desc = "Action {$action_after_image['data']->name} has been deleted successfully!";
            $status = "success";
        } else {
            $desc = $action['message'];
        }

        return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index',
            [$advertiser_id, $brand_id, $action_id])
            ->with('toastr_msgs', [$status => $desc]);
    }

    /**
     * Associate the chosen group with the current campaign
     *
     * @param Request $request
     * @internal param $advertiser_id
     * @internal param $brand_id
     * @internal param $campaign_id
     */
    public function associateGroup(Request $request)
    {
        $values = $request->all();
        $campaign_values = (new CampaignsGroupsLib())->create($values);
    }

    /**
     * Interact with out
     * create/update
     *
     * @param $values
     * @param null $action_id
     * @return $this|mixed
     */
    private function getModel($values, $action_id = null)
    {
        $model = new ActionsLib();
        try {
            if ($action_id) {
                return $model->update($action_id, $values);
            } else {
                return $model->create($values);
            }
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'message' => $e->getMessage(),
                'data' => []
            ];
        }
    }
}
