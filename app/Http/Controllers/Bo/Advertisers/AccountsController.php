<?php

namespace App\Http\Controllers\Bo\Advertisers;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountReport;
use App\Entities\Models\Bo\Advertiser;
use App\Http\Requests\Bo\Advertisers\AccountsRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountsController extends Controller
{
    const PERMISSION = 'bo.advertisers.accounts';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * Display a listing of the resource.
     *
     * @param $advertiser_id
     * @return \Illuminate\Http\Response
     */
    public function index($advertiser_id)
    {
        return view('pages.bo.advertisers.accounts.index')
            ->with('advertiser', Advertiser::find($advertiser_id))
            ->with('accounts', Account::where('advertiser_id', $advertiser_id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $advertiser_id
     * @return \Illuminate\Http\Response
     */
    public function create($advertiser_id)
    {
        return view('pages.bo.advertisers.accounts.form')
            ->with('advertiser', Advertiser::find($advertiser_id));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AccountsRequest $request
     * @param $advertiser_id
     * @return \Illuminate\Http\Response
     */
    public function store(AccountsRequest $request, $advertiser_id)
    {
        $values = $request->all();
        $account = new Account();
        try {
            $values['advertiser_id'] = $advertiser_id;
            $account->fill($values);

            $account->save();

            foreach ($values['report'] as $rep) {
                $report = new AccountReport();
                $report->account_id = $account->id;
                $report->report_id = $rep;

                $report->save();
            }

            return redirect()->route('bo.advertisers.{advertiser_id}.accounts.index', $advertiser_id)
                ->with(self::MESSAGE_TOAST, ["success" => "Account (Name: $account->name) saved!"]);
        } catch (\Exception $e) {
            return redirect()->route('bo.advertisers.{advertiser_id}.accounts.create', $advertiser_id)
                ->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $advertiser_id
     * @param $account_id
     * @return \Illuminate\Http\Response
     */
    public function edit($advertiser_id, $account_id)
    {
        $account = Account::find($account_id);
        $reports = [];

        foreach ($account->reports as $rep) {
            $reports[] = $rep->report_id;
        }

        return view('pages.bo.advertisers.accounts.form')
            ->with('advertiser', Advertiser::find($advertiser_id))
            ->with('account', $account)
            ->with('report', $reports);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AccountsRequest $request
     * @param $advertiser_id
     * @param $account_id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountsRequest $request, $advertiser_id, $account_id)
    {
        $values = $request->all();
        $account = Account::find($account_id);

        AccountReport::where('account_id', $account_id)->delete();

        try {
            $account->fill($values);

            $account->save();

            foreach ($values['report'] as $report) {
                $rep = new AccountReport();
                $rep->account_id = $account_id;
                $rep->report_id = intval($report);

                $rep->save();
            }

            return redirect()->route('bo.advertisers.{advertiser_id}.accounts.index', $advertiser_id)
                ->with(self::MESSAGE_TOAST, ["success" => "Account (Name: $account->name) updated!"]);
        } catch (\Exception $e) {
            return redirect()->route('bo.advertisers.{advertiser_id}.accounts.edit', [$advertiser_id, $account_id])
                ->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }
}
