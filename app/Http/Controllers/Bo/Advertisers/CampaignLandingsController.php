<?php

namespace App\Http\Controllers\Bo\Advertisers;

use App\Http\Requests\Bo\Advertisers\CampaignLandingsRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\Bo\OutInterfaces\AdvertisersLib;
use App\Libraries\Bo\OutInterfaces\BrandsLib;
use App\Libraries\Bo\OutInterfaces\CampaignsLib;
use App\Libraries\Bo\OutInterfaces\LandingPagesLib;

class CampaignLandingsController extends Controller
{
    protected $assets = ['datatables', 'x-editable', 'form'];

    const PERMISSION = 'bo.advertisers';

    /**
     * Display a listing of the resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @return \Illuminate\Http\Response
     */
    public function index($advertiser_id, $brand_id, $campaign_id)
    {
        $campaign = (new CampaignsLib())->find($advertiser_id, $brand_id, $campaign_id)['data'];

        return view('pages.bo.advertisers.brands.campaign.landing.index')
            ->with('landings', $campaign->landing_pages)
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'])
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id)['data'])
            ->with('campaign', $campaign);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @return \Illuminate\Http\Response
     */
    public function create($advertiser_id, $brand_id, $campaign_id)
    {
        $campaign = (new CampaignsLib())->find($advertiser_id, $brand_id, $campaign_id)['data'];
        return view('pages.bo.advertisers.brands.campaign.landing.landing_form')
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'])
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id)['data'])
            ->with('campaign', $campaign);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CampaignLandingsRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @return \Illuminate\Http\Response
     */
    public function store(CampaignLandingsRequest $request, $advertiser_id, $brand_id, $campaign_id)
    {
        $values = $request->all();
        $values['url'] = trim($values['url']);
        $landing = $this->getModel($values, $campaign_id);
        if ($landing['status']) {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.index', [$advertiser_id, $brand_id, $campaign_id])
                ->with('toastr_msgs', ["success" => "The landing page {$landing['data']->name} added"]);
        } else {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.index',
                [$advertiser_id, $brand_id, $campaign_id])
                ->with('toastr_msgs', ["error" => $landing['message']])
                ->withInput((array) $landing['data']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     * @return \Illuminate\Http\Response
     */
    public function edit($advertiser_id, $brand_id, $campaign_id, $landing_id)
    {
        $landing = (new LandingPagesLib())->find($advertiser_id, $brand_id, $campaign_id, $landing_id)['data'];

        return view('pages.bo.advertisers.brands.campaign.landing.landing_form')
            ->with('landing', $landing)
            ->with('dynamics', $landing->parameters)
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'])
            ->with('brand', (new BrandsLib())->find($advertiser_id, $brand_id)['data'])
            ->with('campaign', (new CampaignsLib())->find($advertiser_id, $brand_id, $campaign_id)['data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CampaignLandingsRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     * @return \Illuminate\Http\Response
     */
    public function update(CampaignLandingsRequest $request, $advertiser_id, $brand_id, $campaign_id, $landing_id)
    {
        $values = $request->all();
        $values['url'] = trim($values['url']);
        $landing = $this->getModel($values, $campaign_id, $landing_id);

        if ($landing['status']) {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.index',
                [$advertiser_id, $brand_id, $campaign_id])
                ->with('toastr_msgs', ["success" => "This Landing Page ({$landing['data']->name}) was updated successfully!"]);
        } else {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.edit',
                [$advertiser_id, $brand_id, $campaign_id, $landing_id])
                ->with('toastr_msgs', ["error" => $landing['message']])
                ->withInput((array) $landing['data']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @param $campaign_id
     * @param $landing_id
     * @return \Illuminate\Http\Response
     */
    public function destroy($advertiser_id, $brand_id, $campaign_id, $landing_id)
    {
        $status = 'error';
        $desc = '';
        $landing_lib = new LandingPagesLib();
        $landing_after_image = $landing_lib->find($advertiser_id, $brand_id, $campaign_id, $landing_id);
        $landing = $landing_lib->delete($advertiser_id, $brand_id, $campaign_id, $landing_id);

        if ($landing['status']) {
            $status = "success";
            $desc = "This Landing Page ({$landing_after_image['data']->name}) has been deleted successful!";;
        } else {
            $desc = $landing['message'];
        }

        return redirect()->route('bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.{campaign_id}.lp.index',
            [$advertiser_id, $brand_id, $campaign_id])
            ->with('toastr_msgs', [$status => $desc]);

    }

    /**
     * Interact with out
     * create/update
     *
     * @param $values
     * @param $campaign_id
     * @param null $landing_id
     * @return $this|mixed
     * @internal param $brand_id
     */
    private function getModel($values, $campaign_id, $landing_id = null)
    {
        $params = [
            'url' => $values['url'],
            'id' => $landing_id ?? '',
            'name' => $values['name'],
            'page' => $values['page'],
            'status' => $values['status'],
            'campaign_id' => $campaign_id,
            'campaign_mongodb_id' => $values['campaign_mongodb_id'],
            'dynamic' => $values['dynamic'],
            'brand_id' => $values['brand_id'],
            'weight' => $values['weight'] ?? 100,
            'token' => $values['token_name'] ?? '',
            'advertiser_id' => $values['advertiser_id'],
            'use_token' => $values['use_token'] ?? false,
            'is_default' => $values['is_default'] ?? false,
        ];

        $model = new LandingPagesLib();

        try {
            if ($landing_id) {
                return $model->update($landing_id, $params);
            } else {
                return $model->create($params);
            }
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'message' => $e->getMessage(),
                'data' => [],
            ];
        }
    }
}
