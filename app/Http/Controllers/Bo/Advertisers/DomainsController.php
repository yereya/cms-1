<?php

namespace App\Http\Controllers\Bo\Advertisers;

use App\Http\Requests\Bo\Advertisers\DomainRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Bo\OutInterfaces\DomainsLib;
use App\Libraries\Bo\OutInterfaces\OutConnection;
use App\Libraries\Datatable\DatatableLib;
use DebugBar\DebugBar;
use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DomainsController extends Controller
{
    use DataTableTrait;

    /**
     * @var array $assets
     */
    protected $assets = ['datatables', 'x-editable', 'form', 'bo-domains'];

    const PERMISSION = 'bo.advertisers';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        /* Fetch all domains from out connection */
        $domains = (new DomainsLib())->all();

        return view('pages.bo.advertisers.domains.index')
            ->with('domains', $domains['data']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.bo.advertisers.domains.domain_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DomainRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DomainRequest $request)
    {
        $values = $request->all();
        $domain = $this->getModel($values);

        if ($domain['status']) {
            return redirect()->route('bo.advertisers.domains.index')
                ->with(self::MESSAGE_TOAST, ["success" => "Domain (Name: {$domain['data']->domain}) has been created!"]);
        } else {
            return redirect()->route('bo.advertisers.domains.create')
                ->with(self::MESSAGE_TOAST, ["error" => $domain['message']])
                ->withInput((array) $domain['data']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $domains_lib = new DomainsLib();

        return view('pages.bo.advertisers.domains.index')
            ->with('advertiser', $domains_lib->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.bo.advertisers.domains.domain_form')
            ->with('domain', (new DomainsLib())->find($id)['data']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DomainRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(DomainRequest $request, $id)
    {
        $values = $request->all();
        $domain = $this->getModel($values, $id);

        if ($domain['status']) {
            return redirect()->route('bo.advertisers.domains.index')
                ->with(self::MESSAGE_TOAST, ["success" => "Advertiser (Name: {$domain['data']->domain}) updated!"]);
        } else {
            return redirect()->route('bo.advertisers.domains.edit', [$id])
                ->with(self::MESSAGE_TOAST, ["error" => $domain['message']])
                ->withInput((array) $domain['data']);
        }
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $status = 'error';
        $desc = '';
        $domains_lib = new DomainsLib();
        $domain = $domains_lib->find($id);
        $domains_lib->delete($id);

        if ($domain['status']) {
            $desc = "This Landing Page ({$domain['data']->domain}) has been deleted successful!";
            $status = "success";
        } else {
            $desc = $domain['message'];
        }

        return redirect()->route('bo.advertisers.domains.index')
            ->with('toastr_msgs', [$status => $desc])
            ->with('domains', $domains_lib->all()['data']);
    }

    /**
     * Interface with out
     * create/update
     *
     * @param array $values
     * @param null $domain_id
     * @return $this|mixed
     */
    private function getModel($values = [], $domain_id = null)
    {
        $model = new DomainsLib();
        try {
            if ($domain_id) {
                return $model->update($domain_id, $values);
            } else {
                return $model->create($values);
            }
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'message' => $e->getMessage(),
                'data' => []
            ];
        }
    }
}


