<?php

namespace App\Http\Controllers\Bo\Advertisers;

use App\Http\Requests\Bo\Advertisers\BrandsRequest;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxModalTrait;
use App\Libraries\Bo\OutInterfaces\AdvertisersLib;
use App\Libraries\Bo\OutInterfaces\BrandsLib;
use App\Libraries\Bo\OutInterfaces\CompaniesLib;
use App\Libraries\Bo\OutInterfaces\DomainsLib;
use App\Libraries\Bo\OutInterfaces\OutBrands;

class BrandsController extends Controller
{

    use AjaxModalTrait;

    const PERMISSION = 'bo.advertisers';

    protected $assets = ['datatables', 'x-editable', 'form', 'bo-advertisers'];

    /**
     * Display a listing of the resource.
     *
     * @param $advertiser_id
     * @return \Illuminate\Http\Response
     */
    public function index($advertiser_id)
    {
        $advertiser = (new AdvertisersLib())->find($advertiser_id)['data'] ?? [];

        return view('pages.bo.advertisers.brands.index')
            ->with('ios', $advertiser->brands ?? [])
            ->with('advertiser', $advertiser)
            ->with('domains', (new DomainsLib())->all()['data'] ?? []);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $advertiser_id
     * @return \Illuminate\Http\Response
     */
    public function create($advertiser_id)
    {
        $advertiser =  (new AdvertisersLib())->find($advertiser_id)['data'] ?? [];
        $companies = (new CompaniesLib())->getList() ?? [];

        return view('pages.bo.advertisers.brands.io_form')
            ->with('advertiser',$advertiser)
            ->with('companies',$companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  BrandsRequest $request
     * @param $advertiser_id
     * @return \Illuminate\Http\Response
     */
    public function store(BrandsRequest $request, $advertiser_id)
    {
        $values = $request->all();
        $advertiser = (new AdvertisersLib())->find($advertiser_id)['data'];
        $brand = $this->getModel($values, $advertiser->id);

        if ($brand['status']) {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.index', $advertiser_id)
                ->with(self::MESSAGE_TOAST, ["success" => "The brand {$brand['data']->name} created!"]);
        } else {
            return redirect()->route('bo.advertisers.{advertiser_id}.brands.create', $advertiser_id)
                ->with(self::MESSAGE_TOAST, ["error" => $brand['message']])
                ->withInput((array) $brand['data']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function edit($advertiser_id, $brand_id)
    {
        return view('pages.bo.advertisers.brands.io_form')
            ->with('advertiser', (new AdvertisersLib())->find($advertiser_id)['data'] ?? [])
            ->with('io', (new BrandsLib())->find($advertiser_id, $brand_id)['data'] ?? []);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  BrandsRequest $request
     * @param $advertiser_id
     * @param $brand_id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandsRequest $request, $advertiser_id, $brand_id)
    {
        $values = $request->all();
        $route = ($required_id = request()->input('route_param') === 'campaign_index')
            ? 'bo.advertisers.{advertiser_id}.brands.{brand_id}.campaigns.index' : 'bo.advertisers.show';
        $brand = $this->getModel($values, $advertiser_id, $brand_id);

        if ($brand['status']) {
            return redirect()->route($route, [$advertiser_id, $required_id ? $brand_id : ''])
                ->with('toastr_msgs', ["success" => "This IO ({$brand['data']->name}) was updated successful!"]);
        } else {
            return redirect()->route($route, [$advertiser_id, $required_id ? $brand_id : ''])
                ->with('toastr_msgs', ["error" => $brand['message']])
                ->withInput((array) $brand['data']);
        }
    }

    /**
     * Interface with out
     * create/update
     *
     * @param array $values
     * @param $advertiser_id
     * @param null $brand_id
     * @return $this|mixed
     */
    private function getModel($values = [], $advertiser_id, $brand_id = null)
    {
        $model = new BrandsLib();
        try {
            if ($brand_id) {
                return $model->update($brand_id, $values);
            } else {
                return $model->create($values);
            }
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'message' => $e->getMessage(),
                'data' => []
            ];
        }
    }

    /**
     * Action link modal
     *
     */
    private function ajaxModal_actionLink()
    {
        $values = request()->all();
        $advertiser = session('advertiser');
        $actions = $advertiser->actions ?? [];

        /* Create an array - each action id index will have its own url query according to its parameters */
        $actions_url_queries = (new AdvertisersLib())->getActionsUrlQueries($advertiser->id, $values['brand_id']);

        return view('pages.bo.advertisers.brands.link_modal')
            ->with('domain', $values['domain'])
            ->with('brand_id', $values['brand_id'])
            ->with(compact('actions'))
            ->with(compact('advertiser'))
            ->with(compact('actions_url_queries'));
    }
}
