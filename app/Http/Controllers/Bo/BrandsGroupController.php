<?php


namespace App\Http\Controllers\Bo;


use App\Http\Controllers\Controller;
use App\Http\Requests\Bo\BrandsGroupRequest;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Bo\OutInterfaces\BrandsGroupLib;
use App\Libraries\Bo\OutInterfaces\CompaniesLib;

class BrandsGroupController extends Controller
{
    use DataTableTrait;
    use Select2Trait;

    const PERMISSION = 'bo.companies.brands_groups';

    /**
     * @var array $assets
     */
    protected $assets = ['datatables', 'x-editable', 'form', 'bo-advertisers'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    /**
     * Index - main screen
     **
     *
     * @param $company_id
     *
     * @return $this
     */
    public function index($company_id)
    {
        $brands_groups = (new BrandsGroupLib())->all($company_id)['data'];
        $company       = (new CompaniesLib())->find($company_id)['data'];

        return view('pages.bo.companies.brands-groups.index')
            ->with(compact('brands_groups', 'company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $company_id
     *
     * @return \Illuminate\Http\Response
     */
    public function create($company_id)
    {
        $company = (new CompaniesLib())->find($company_id)['data'];

        return view('pages.bo.companies.brands-groups.form')
            ->with(compact('company'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param BrandsGroupRequest $request
     * @param                    $company_id
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BrandsGroupRequest $request, $company_id)
    {
        $values      = $request->all();
        $brand_group = $this->getModel($values);

        if ($brand_group['status']) {
            return redirect()->route('bo.companies.brands-groups.index', [$brand_group['data']->company_id])
                ->with(self::MESSAGE_TOAST, "Brand Group (Name: " . $brand_group['data']->name . ") created!");
        } else {
            return redirect()->route('bo.companies.brands-groups.create', [$company_id])
                ->with(self::MESSAGE_TOAST, ["warning" => $brand_group['message']])
                ->withInput((array)$brand_group['data']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param      $company_id
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($company_id, $id)
    {
        $brand_group = (new BrandsGroupLib())->find($company_id, $id)['data'];
        $company     = ((new CompaniesLib())->find($company_id)['data']);

        return view('pages.bo.companies.brands-groups.form')
            ->with(compact('brand_group', 'company'));
    }
//

    /**
     * Update the specified resource in storage.
     *
     * @param BrandsGroupRequest $request
     * @param                    $company_id
     * @param  int               $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(BrandsGroupRequest $request, $company_id, $id)
    {
        $values      = $request->all();
        $brand_group = $this->getModel($values, $id);

        if ($brand_group['status']) {
            return redirect()->route('bo.companies.brands-groups.index', [$company_id])
                ->with(self::MESSAGE_TOAST, ["success" => "Brand Group (Name: {$brand_group['data']->name}) updated!"]);
        } else {
            return redirect()->route('bo.companies.brands-groups.index', [$company_id])
                ->with(self::MESSAGE_TOAST, ["error: " => $brand_group['message']])
                ->withInput((array)$brand_group['data']);
        }
    }

    /**
     * filter advertiser rows by given parameter pairs
     *
     * @param BrandsGroupRequest $request
     * @param                    $id
     *
     * @return array
     * @internal param array $pairs
     */
    public function filter(BrandsGroupRequest $request, $id)
    {
        $condition_pairs       = $request;
        $brands_group          = (new BrandsGroupLib())->all($id);
        $filtered_brands_group = [];

        try {
            foreach ($brands_group as $brand_group) {
                $condition = true;
                // Current advertiser should match each of the expected values
                foreach ($condition_pairs as $attribute => $expected_value) {
                    $condition = $condition
                        && ($brand_group->{$attribute} ?? false)
                        && ($brand_group->{$attribute} == $expected_value);
                }

                if ($condition) {
                    $filtered_advertisers[] = $brand_group;
                }
            }
        } catch (\Exception $e) {
            return redirect()->route('bo.brand_group.index')
                ->with(self::MESSAGE_TOAST, ["error" => "Advertiser filtering went wrong: " . $e->getMessage()]);
        }

        return $filtered_brands_group;
    }

    /**
     * Interface with out
     * create/update
     *
     * @param array $values
     * @param null  $brand_group_id
     *
     * @return $this|mixed
     */
    private function getModel($values = [], $brand_group_id = null)
    {
        $model = new BrandsGroupLib();
        try {
            if ($brand_group_id) {
                return $model->update($brand_group_id, $values);
            } else {
                return $model->create($values);
            }
        } catch (\Exception $e) {
            return [
                'status'  => 0,
                'message' => $e->getMessage(),
                'data'    => []
            ];
        }
    }


    /**
     * @param $params
     *
     * @return array
     */
    private function select2_getBrandsGroupsByCompany($params)
    {
        $company_id = $params['dependency']['0']['value'];
        $results = (new BrandsGroupLib())->getByCompanyId($company_id);

        return $results ?? [];
    }
    public function select3_getBrandsGroupsByCompany($company_id)
    {
        $results = (new BrandsGroupLib())->getByCompanyId($company_id);
        return $results ?? [];
    }
}