<?php

namespace App\Http\Controllers\Bo;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountReport;
use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Bo\Publishers\Publisher;
use App\Http\Controllers\Controller;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use DateTime;
use DateTimeZone;
use DB;
use Illuminate\Http\Request;

class AccountsController extends Controller
{
    use DataTableTrait;
    use Select2Trait;

    const PERMISSION = 'bo.accounts';

    /**
     * @var array - assets (js, css)
     */
    protected $assets = ['datatables', 'form'];

    /**
     * Index - main screen
     *
     * @param Request $request
     *
     * @return $this
     */
    public function index(Request $request)
    {
        $values = $request->all();

        return view('pages.bo.accounts.index')
            ->with('publisher_id', isset($values['publisher_id']) ? $values['publisher_id'] : null)
            ->with('advertiser_id', isset($values['advertiser_id']) ? $values['advertiser_id'] : null)
            ->with('link_datatable', $this->createUrl($request, 'bo/accounts/datatable'))
            ->with('link_create', $this->createUrl($request, 'bo/accounts/create'));
    }

    /**
     * @param Request $request - advertiser or publisher id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        $values = $request->all();

        return view('pages.bo.accounts.form')
            ->with('publisher_id', isset($values['publisher_id']) ? $values['publisher_id'] : '')
            ->with('publishers_list', Publisher::pluck('publisher_name', 'id'))
            ->with('advertiser_id', isset($values['advertiser_id']) ? $values['advertiser_id'] : '')
            ->with('advertisers_list', Advertiser::pluck('name', 'id'))
            ->with('store_link', $this->createUrl($request, 'bo/accounts'))->with('update_link', '')
            ->with('time_zone_location', $this->accountTimeZone())
            ->with('sale_group_list',
                Account::whereNotNull('sale_group')->distinct()->pluck('sale_group', 'sale_group'))
            ->with('product_name_list',
                Account::whereNotNull('product_name')->distinct()->pluck('product_name', 'product_name'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $values = $request->all();

        $account = new Account();
        try {
            $this->parseAccountTimeZone($values);
            $account->fill($values);

            $account->save();

            if (isset($values['report'])) {
                $reports = [];
                foreach ($values['report'] as $reports) {
                    $reports[] = [
                        'account_id' => $account->id,
                        'report_id' => $reports
                    ];
                }

                AccountReport::create($reports);
            }


            return redirect($this->createUrl($request, 'bo/accounts'))->with(self::MESSAGE_TOAST, ["success" => "Account (Name: $account->name) saved!"]);
        } catch (\Exception $e) {
            return redirect($this->createUrl($request, 'bo/accounts'))->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }

    /**
     * @param Request $request
     * @param         $account_id
     *
     * @return $this
     */
    public function edit(Request $request, $account_id)
    {
        $account = Account::with('reportsA')->find($account_id);
        $values = $request->all();

        $reports = [];

        foreach ($account->reports as $rep) {
            $reports[] = $rep->report_id;
        }

        return view('pages.bo.accounts.form')
            ->with('account', $account)
            ->with('report', $reports)
            ->with('time_zone_location', $this->accountTimeZone())
            ->with('publisher_id', isset($values['publisher_id']) ? $values['publisher_id'] : '')
            ->with('publishers_list', Publisher::pluck('publisher_name', 'id'))
            ->with('advertiser_id', isset($values['advertiser_id']) ? $values['advertiser_id'] : '')
            ->with('advertisers_list', Advertiser::pluck('name', 'id'))
            ->with('store_link', '')
            ->with('update_link', $this->createUrl($request, 'bo/accounts/' . $account_id))
            ->with('sale_group_list',
                Account::whereNotNull('sale_group')->distinct()->pluck('sale_group', 'sale_group'))
            ->with('product_name_list',
                Account::whereNotNull('product_name')->distinct()->pluck('product_name', 'product_name'));
    }

    /**
     * @param Request $request
     * @param $account_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $account_id)
    {
        $values = $request->all();
        $account = Account::find($account_id);

        try {
            $this->parseAccountTimeZone($values);

            $account->fill($values);

            $account->save();

            if (isset($values['report'])) {
                $reports = [];
                foreach ($values['report'] as $key => $report) {
                    $exists = AccountReport::where('account_id', $account_id)->where('report_id', $report)->first();
                    if ($exists) {
                        unset($values['report'][$key]);
                    } else {
                        $reports[] = [
                            'account_id' => $account_id,
                            'report_id' => $report
                        ];
                    }
                }

                AccountReport::insert($reports);
            }


            $request->replace(['publisher_id', 'advertiser_id']);

            return redirect($this->createUrl($request, 'bo/accounts'))->with(self::MESSAGE_TOAST, ["success" => "Account (Name: $account->name) updated!"]);
        } catch (\Exception $e) {
            return redirect($this->createUrl($request, 'bo/accounts'))->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }
    }

    /**
     * Create url
     *
     * @param $request
     * @param $route
     *
     * @return string
     */
    private function createUrl($request, $route)
    {
        $values = is_object($request) ? $request->all() : $request;

        $link = $route;
        if (isset($values['publisher_id'])) {
            $link .= '?publisher_id=' . $values['publisher_id'];
        } elseif (isset($values['advertiser_id'])) {
            $link .= '?advertiser_id=' . $values['advertiser_id'];
        }

        return $link;
    }

    /**
     * Select 2 get time zones by offset
     *
     * @param $params
     *
     * @return array
     */
    public function select2_timeZone($params) {
        $origin = new DateTimeZone($params['dependency'][0]['value']);
        $time = new DateTime("now", $origin);
        $offset = (integer) round($origin->getOffset($time) / 3600);

        $offsets = DB::connection('bo')->select('SELECT Name,
              TIMESTAMPDIFF(HOUR, UTC_TIMESTAMP(), CONVERT_TZ(UTC_TIMESTAMP(), "Etc/UTC", Name)) as Offset
              FROM mysql.time_zone_name');

        $result = [];
        foreach ($offsets as $off) {
            if ($off->Offset == $offset) {
                $result[] = [
                    'id' => $off->Name,
                    'text' => $off->Name
                ];
            }
        }

        return $result;
    }

    /**
     * Build account time zone list
     *
     * @return array
     */
    private function accountTimeZone() {
        $time_zone = Account::select('time_zone_location', 'time_zone')->groupBy('time_zone_location')->orderBy('time_zone', 'asc')->get();
        $locations = [];
        foreach ($time_zone as $zone) {
            $locations[] = [
                'id' => $zone->time_zone. ';' . $zone->time_zone_location,
                'text' => $zone->time_zone . ' ' . $zone->time_zone_location];
        }
        return $locations;
    }

    /**
     * Parse GET|POST params time zone to data base columns
     *
     * @param $values
     */
    private function parseAccountTimeZone(&$values) {
        if (isset($values['time_zone_location'])) {
            $times = explode(';', $values['time_zone_location']);
            if ($times) {
                $values['time_zone_location'] = $times[1];
                $values['time_zone'] = $times[0];
            }
        }
    }

    /**
     * create index datatable
     *
     * @param              $params
     * @param DatatableLib $datatable
     *
     * @return array
     */
    public function dataTable_accounts($params, DatatableLib $datatable)
    {
        $params['searchColumns'] = ['name', 'id', 'source_account_id', 'status', 'chanel'];
        $link = '';
        if (isset($params['publisher'])) {
            $link = 'publisher=' . $params['publisher'];
        } elseif (isset($params['advertiser'])) {
            $link = 'advertiser=' . $params['advertiser'];
        }

        $datatable->renderColumns([
            'status' => [
                'db_name' => 'status',
                'callback' => function ($obj) {
                    $status = $obj['status'];
                    $class = 'label-success';

                    if ($status == 'inactive') {
                        $class = 'label-danger';
                    } elseif ($status == 'pending') {
                        $class = 'label-default';
                    }

                    return "<span class='label label-sm $class'>$status</span>";
                }
            ],
            'actions' => [
                [
                    'name' => 'Edit account',
                    'icon' => 'icon-pencil fa-fw',
                    'route' => 'bo.accounts.edit',
                    'permissions' => 'edit',
                    'route_params' => $link,
                    'type' => 'link'
                ],
                [
                    'name' => 'Report Fields',
                    'icon' => 'icon-list fa-fw',
                    'route' => 'bo.accounts.{account_id}.report-fields.index',
                    'permissions' => 'brands.view',
                    'type' => 'link'
                ],
                [
                    'name' => 'Show Block Queries',
                    'icon' => 'fa fa-ban fa-fw',
                    'route' => 'bo.accounts.{account_id}.block-queries.index',
                    'permissions' => 'brands.view',
                    'type' => 'link'
                ],
                [
                    'name' => 'Show Blocked IPs',
                    'icon' => 'fa fa-minus-circle fa-fw',
                    'route' => 'bo.accounts.{account_id}.blocked-ips.index',
                    'permissions' => 'brands.view',
                    'type' => 'link'
                ]
            ]
        ]);

        $datatable->changeFilters([
            'advertiser' => 'advertiser_id',
            'publisher' => 'publisher_id'
        ]);

        $params['query'] = Account::select($datatable->getColumns())->orderBy('status', 'desc');
        $params['recordsTotal'] = $params['query']->count();

        return $datatable->fill($params);
    }
}
