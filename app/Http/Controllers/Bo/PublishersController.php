<?php namespace App\Http\Controllers\Bo;

use App\Entities\Models\Bo\Publishers\Publisher;
use App\Http\Requests\Bo\Publishers\PublishersRequest;
use App\Libraries\Bo\OutInterfaces\OutPublisher;
use App\Http\Controllers\Controller;

class PublishersController extends Controller
{

    const PERMISSION = 'bo.publishers';

    protected $assets = ['datatables', 'x-editable', 'form'];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pages.bo.publishers.index')
            ->with('publishers', Publisher::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pages.bo.publishers.publisher_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PublishersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(PublishersRequest $request)
    {
        $values = $request->all();

        $publisher = new Publisher();

        try {
            $mongo = $this->out($values);

            if ($mongo['status']) {
                $publisher->publisher_name = $values['publisher_name'];
                $publisher->status = $values['status'];
                $publisher->type = $values['type'];
                $publisher->mongodb_id = $mongo['message'];
                $publisher->save();

                return redirect()->route('bo.publishers.index')
                    ->with('toastr_msgs', ["success" => "The Publisher {$publisher->publisher_name} added"]);
            } else {
                return redirect()->route('bo.publishers.index')
                    ->with('toastr_msgs', ["error" => "Error OUT: " . $mongo['message']]);
            }
        } catch (Exception $e) {
            return redirect()->route('bo.publishers.index')
                ->with('toastr_msgs', ["error" => "The Publisher not saved: " . $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('pages.bo.publishers.publisher_form')
            ->with('publisher', Publisher::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PublishersRequest $request
     * @param  int $publisher_id
     * @return \Illuminate\Http\Response
     */
    public function update(PublishersRequest $request, $publisher_id)
    {
        $values = $request->all();

        try {
            $publisher = Publisher::find($publisher_id);

            if (!$publisher) {
                return redirect()->route('bo.publishers.index')
                    ->with('toastr_msgs', ['error' => 'Not found this publisher: ' . $publisher_id]);
            }

            $mongo = $this->out($values, $publisher->mongodb_id);

            if ($mongo['status']) {
                $publisher->fill($values);
                $publisher->save();

                return redirect()->route('bo.publishers.index')
                    ->with('toastr_msgs', ['success' => "This Publisher ($publisher->publisher_name) updated successful!"]);
            } else {
                return redirect()->route('bo.publishers.index')
                    ->with('toastr_msgs', ['error' => "Error OUT: " . $mongo['message']]);
            }
        } catch (\Exception $e) {
            return redirect()->route('bo.publishers.index')
                ->with('toastr_msgs', ['error' => "Error updated this Publisher, try again later $e->getMessage()"]);
        }
    }

    /**
     * Interface with out
     * create/update
     *
     * @param array $values
     * @param null $mongo_id
     * @return $this|mixed
     */
    private function out($values = [], $mongo_id = null) {
        $params = [
            'name' => $values['publisher_name'],
            'status' => snakeToWords($values['status']),
            'type' => $values['type'],
        ];
        $out = new OutPublisher();

        if ($mongo_id) {
            return $out->setPublisher($mongo_id, $params);
        } else {
            return $out->storePublisher($params);
        }
    }
}
