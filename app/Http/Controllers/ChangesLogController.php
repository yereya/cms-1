<?php

namespace App\Http\Controllers;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\AccountCampaign;
use App\Entities\Models\Bo\AccountCampaignAdGroup;
use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\Mrr\MrrChangeLogHistory;
use App\Entities\Models\Mrr\MrrChangeLogState;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\ChangeLog\ChangeLogLib;
use App\Libraries\Datatable\DatatableLib;
use App\Libraries\TpLogger;
use Illuminate\Http\Request;
use Session;

class ChangesLogController extends Controller
{
    use DataTableTrait;
    use Select2Trait;
    use AjaxModalTrait;

    const PERMISSION = 'changes_log';

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'datatables', 'change-log-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Changes Log';


    /**
     * @return mixed
     */
    public function index()
    {
        $advertisers = Advertiser::select('id', 'name as text')
            ->where('status', 'active')
            ->whereIn('id', session('user_advertiser_access'))
            ->orderBy('name')
            ->get();

        return view('pages.changes-log.index')
            ->with('advertisers', $advertisers);
    }


    /**
     * @return mixed
     */
    public function history()
    {
        // Permission check
        if (!auth()->user()->can(self::PERMISSION . '.history.view')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to view this page."]);
        }

        $advertisers = Advertiser::select('id', 'name as text')
            ->where('status', 'active')
            ->whereIn('id', session('user_advertiser_access'))
            ->orderBy('name')
            ->get();

        return view('pages.changes-log.history')
            ->with('show_user_filter', auth()->user()->can(self::PERMISSION . '.history.filter[user].view'))
            ->with('advertisers', $advertisers);
    }


    /**
     * Create new change
     *
     * @param $request
     * @param $arguments
     *
     * @return string
     */
    private function ajaxModal_newChange($request, $arguments)
    {
        //TODO add validation

        return ChangeLogLib::renderModalActionView($request);
    }


    /**
     * Create new change
     *
     * @param $request
     * @param $arguments
     *
     * @return string
     */
    private function ajaxModal_changeSummary($request, $arguments)
    {
        $change_summary = ChangeLogLib::getChangeSummary();

        if (is_array($change_summary) && isset($change_summary[0])) {

            $table_columns = array_keys($change_summary[0]);
            if (is_array($table_columns) && $table_columns > 0) {

                foreach ($table_columns as $key => $item) {
                    if (in_array($item, ['new_value', 'old_value', 'failure_reason'])) {
                        unset($table_columns[$key]);
                    }
                }

                return view('pages.changes-log.modals.change-summary')
                    ->with('change_summary', $change_summary)
                    ->with('table_columns', $table_columns);
            }
        }
    }


    /**
     * Create new change
     *
     * @param $request
     * @param $arguments
     *
     * @return string
     */
    private function ajaxModal_details($request, $arguments)
    {
        $change              = ChangeLogLib::getDetailsById($request['change_id']);
        $five_recent_changes = ChangeLogLib::getRecentChanges($request['user_id'], 5);

        return view('pages.changes-log.modals.details')
            ->with(compact('change', 'five_recent_changes'));
    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request            = $request->all();
        $request['filters'] = json_decode($request['filters'], true);
        $logger             = new TpLogger(53);
        $logger->info('Change Log debugger');
        $logger->info(['request', $request]);


        $validator = ChangeLogLib::validateStoreRequestArguments($request);

        if ($validator->fails()) {
            $logger->error(["The following rejections happened: ", json_encode($validator->messages()->all())]);

            return redirect()->back()
                ->with(self::MESSAGE_TOAST,
                    ["error" => "The following rejections happened: " . json_encode($validator->messages()->all())]);
        }

        // Handles the requested action
        ChangeLogLib::handleAction($request);

        $return = redirect()->back()
            ->with(self::MESSAGE_TOAST, ["success" => 'Change committed'])
            ->with(self::OPEN_AJAX_MODAL, [route('changes-log.ajax-modal', ['method' => 'changeSummary'])]);

        // Show the debug log new tab for permitted users
        if (auth()->user()->can(self::PERMISSION . '.debug-tab.view')) {
            $return->with(self::OPEN_TAB, route('reports.logs.{log_id}.list-group', [
                'log_id' => $logger->getParentRecordId(),
            ]));
        }

        return $return;
    }


    /**
     * Select2 accounts
     *
     * @param $params
     *
     * @return array
     */
    private function select2_accounts($params)
    {
        $advertiser_id = $params['dependency'][0]['value'];
        $query         = Account::where('advertiser_id', $advertiser_id)
            ->select('source_account_id', 'name', 'publisher_name')
            ->leftJoin('publishers', 'publishers.id', '=', 'accounts.publisher_id')
            ->orderBy('publisher_name', 'name')
            ->whereIn('advertiser_id', session('user_advertiser_access'));

        if (isset($params['query']) && count($params['query']) > 0) {
            $query->where('name', 'LIKE', '%' . $params['query'] . '%');
        }

        return $query->get()->map(function ($item) {
            return [
                'id'   => $item->source_account_id,
                'text' => '[' . $item->publisher_name . '] ' . $item->name,
            ];
        });
    }


    /**
     * Select2 Device
     *
     * @param $params
     *
     * @return array
     */
    private function select2_device($params)
    {
        return [
            ['id' => 'm', 'text' => 'Mobile'],
            ['id' => 't', 'text' => 'Tablet'],
            ['id' => 'c', 'text' => 'Computer'],
        ];
    }


    /**
     * Select2 Campaigns
     *
     * @param $params
     *
     * @return array
     */
    private function select2_campaigns($params)
    {
        $account_source_id = $params['dependency'][1]['value'];

        // Translate the account_source_id to the actual account id
        $account = Account::where('source_account_id', $account_source_id)->first();
        if (!$account) {
            return [];
        }

        $query = AccountCampaign::where('account_id', $account->id)
            ->orderBy('campaign_name');

        // Search
        if (isset($params['query']) && count($params['query']) > 0) {
            $query->where('campaign_name', 'LIKE', '%' . $params['query'] . '%');
        }

        return $query->get()->map(function ($item) {
            return [
                'id'   => $item->campaign_id,
                'text' => $item->campaign_name,
            ];
        });
    }


    /**
     * Select2 Ad Group
     *
     * @param $params
     *
     * @return array
     */
    private function select2_adGroups($params)
    {
        $campaign_id = $params['dependency'][2]['value'];

        $query = AccountCampaignAdGroup::whereIn('campaign_source_id', $campaign_id)
            ->orderBy('ad_group_name');

        // Search
        if (isset($params['query']) && count($params['query']) > 0) {
            $query->where('ad_group_name', 'LIKE', '%' . $params['query'] . '%');
        }

        return $query->get()->map(function ($item) {
            return [
                'id'   => $item->ad_group_source_id,
                'text' => $item->ad_group_name,
            ];
        });
    }


    /**
     * Select2 Match Type
     *
     * @param $params
     *
     * @return array
     */
    private function select2_matchType($params)
    {
        $ad_group_id = $params['dependency'][3]['value'];

        $query = MrrChangeLogState::whereIn('ad_group_id', $ad_group_id)
            ->groupBy('match_type')
            ->orderBy('match_type');

        // Search
        if (isset($params['query']) && count($params['query']) > 0) {
            $query->where('ad_group_name', 'LIKE', '%' . $params['query'] . '%');
        }

        return $query->get()->map(function ($item) {
            return [
                'id'   => $item->match_type,
                'text' => $item->match_type,
            ];
        });

    }


    /**
     * Select2 Ad Group
     *
     * @param $params
     *
     * @return array
     */
    private function select2_keywords($params)
    {
        $ad_group_id = $params['dependency'][3]['value'];
        $match_type  = $params['dependency'][4]['value'];

        $query = MrrChangeLogState::whereIn('ad_group_id', $ad_group_id)
            ->whereIn('match_type', $match_type)
            ->groupBy('keyword_id')
            ->orderBy('keyword_name');

        // Search
        if (isset($params['query']) && count($params['query']) > 0) {
            $query->where('keyword_name', 'LIKE', '%' . $params['query'] . '%');
        }

        return $query->get()->map(function ($item) {
            return [
                'id'   => $item->keyword_id,
                'text' => $item->keyword_name,
            ];
        });
    }


    /**
     * @param $params
     * @param DatatableLib $datatable
     *
     * @return array|null
     */
    private function dataTable_changeLogState($params, DatatableLib $datatable)
    {
        // Parse filters
        $filters            = $datatable->getFilters();
        $filters_to_columns = [
            'advertiser' => 'advertiser_id',
            'publisher'  => 'publisher_id',
            'account'    => 'account_id',
            'campaign'   => 'campaign_id',
            'device'     => 'device',
            'ad_group'   => 'ad_group_id',
            'match_type' => 'match_type',
            'keyword'    => 'keyword_id',
            'status'     => 'keyword_status',
        ];

        // Set Filters
        $datatable->changeFilters(array_except($filters_to_columns, ['device', 'match_type']));

        // Select the columns to search in
        $params['searchColumns'] = ['ad_group', 'campaign_name', 'keyword'];

        $query = MrrChangeLogState::whereNotNull('id')
            ->whereIn('advertiser_id', session('user_advertiser_access'));

        // Save the RecordsTotal to session
        // to avoid running this query again and again
        $session_name = \Request::route()->getName() . '_recordsTotal';
        if (!Session::has($session_name)) {
            Session::put($session_name, $query->count());
        }
        $params['recordsTotal'] = \Session::get($session_name);


        // Build group by logic
        // according to the currently set filter
        $group_by = ['campaign_id'];
        if (!empty($filters['device'])) {
            $group_by[] = 'device';
        }
        if (!empty($filters['campaign'])) {
            $group_by[] = 'ad_group_id';
        }
        if (!empty($filters['ad_group'])) {
            $group_by[] = 'match_type';
        }
        if (!empty($filters['match_type'])) {
            $group_by[] = 'keyword_id';
        }
        call_user_func_array([$query, 'groupBy'], $group_by);

        // Validate minimal required fields
        if (empty($filters['advertiser']) || empty($filters['account'])) {
            return null;
        }

        // Build the columns logic
        $change_level = ChangeLogLib::getChangeLevel($filters);
        $columns      = [
            'device'     => [
                'db_name'  => 'device', // Column name
                'callback' => function ($array) use ($filters) {
                    if (empty($filters['device'])) {
                        return null;
                    }
                    switch ($array['device']) {
                        case 'm' :
                            return 'Mobile';
                        case 't' :
                            return 'Tablet';
                        case 'c' :
                            return 'Computer';
                        case 'o' :
                            return 'Other';
                        case 'u' :
                            return 'Unset';
                        default :
                            return $array['device'];
                    }
                }
            ],
            'campaign'   => [
                'db_name'  => 'campaign_name', // Column name
                'callback' => function ($array) use ($filters) {
                    return $array['campaign_name'];
                }
            ],
            'ad_group'   => [
                'db_name'  => 'ad_group_name', // Column name
                'callback' => function ($array) use ($change_level) {
                    if ($change_level == 'campaign') {
                        return null;
                    }
                    return $array['ad_group_name'];
                }
            ],
            'match_type' => [
                'db_name'  => 'match_type', // Column name
                'callback' => function ($array) use ($change_level) {
                    if ($change_level == 'keyword') {
                        return $array['match_type'];
                    }

                    return null;
                }
            ],
            'keyword'    => [
                'db_name'  => 'keyword_name', // Column name
                'callback' => function ($array) use ($change_level, $filters) {
                    if ($change_level == 'keyword' && isset($filters['match_type'])) {
                        return $array['keyword_name'];
                    }

                    return null;
                }
            ],
            'budget'     => [
                'db_name'  => 'campaign_budget', // Column name
                'callback' => function ($array) use ($change_level) {
                    return $array['campaign_budget'];
                }
            ],
        ];

        $columns['id'] = [
            'db_name'  => $change_level . '_id', // Column name
            'callback' => function ($array) use ($change_level) {
                return view('partials.fields.datatables.checkbox')
                    ->with('name', 'checkbox[]')
                    ->with('value', $array[$change_level . '_id'])
                    ->render();
            }
        ];

        $bid_column_name = (in_array($change_level, ['account', 'campaign'])) ? 'ad_group_bid' : $change_level . '_bid';
        $columns['bid']  = [
            'db_name'  => $bid_column_name, // Column namae
            'callback' => function ($array) use ($change_level, $bid_column_name) {
                if (in_array($change_level, ['account', 'campaign'])) {
                    return null;
                }
                return $array[$bid_column_name];
            }
        ];

        $status_column_name = (in_array($change_level, ['account'])) ? 'campaign_status' : $change_level . '_status';
        $columns['status']  = [
            'db_name'  => $status_column_name, // Column name
            'callback' => function ($array) use ($change_level, $status_column_name) {
                if (in_array($change_level, ['account'])) {
                    return null;
                }
                if ($array[$status_column_name] == 1) {
                    return \View::make('partials.fields.status-label', [
                        'text'  => 'Active',
                        'class' => 'label-primary',
                    ])->render();
                } elseif ($array[$status_column_name] == 0) {
                    return \View::make('partials.fields.status-label', [
                        'text'  => 'Paused',
                        'class' => 'label-default',
                    ])->render();
                } else {
                    return $array[$status_column_name];
                }
            }
        ];


        // Execute
        $datatable->renderColumns($columns);
        $params['query'] = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }


    /**
     * @param $params
     * @param DatatableLib $datatable
     *
     * @return array|null
     */
    private function dataTable_changeHistory($params, DatatableLib $datatable)
    {
        // Parse filters
        $filters_to_columns = [
            'advertiser'  => 'advertiser_id',
            'publisher'   => 'publisher_id',
            'account'     => 'account_id',
            'campaign'    => 'campaign_id',
            'device'      => 'device',
            'ad_group'    => 'ad_group_id',
            'match_type'  => 'match_type',
            'keyword'     => 'keyword_id',
            'status'      => 'keyword_status',
            'change_type' => 'change_type',
            'user_id'     => 'user_id',
            'date_range'  => null,
        ];

        // Set Filters
        $datatable->changeFilters(array_except($filters_to_columns, ['device', 'match_type', 'change_type', 'user_id']));

        // Select the columns to search in
        $params['searchColumns'] = ['ad_group', 'campaign', 'keyword'];

        $query = MrrChangeLogHistory::whereNotNull('id')
            ->with('users')
            ->whereIn('advertiser_id', session('user_advertiser_access'));

        // Save the RecordsTotal to session
        // to avoid running this query again and again
        $session_name = \Request::route()->getName() . '_recordsTotal';
        if (!Session::has($session_name)) {
            Session::put($session_name, $query->count());
        }
        $params['recordsTotal'] = \Session::get($session_name);

        // If the user has permission to view all user changes
        if (!auth()->user()->can(self::PERMISSION . '.history.all-user-changes.view')) {
            $query->where('user_id', auth()->user()->id);
        }

        // TODO Remove this after Kostya will fix the filters issue
        $filters = $datatable->getFilters();
        if ($params['filters']['date_range'] ?? false) {
            $params['filters']['date_range'] = explode(' ', $params['filters']['date_range']);
            $query->where('created_at', '>=', $params['filters']['date_range'][0] . ' 00:00:00')
                ->where('created_at', '<=', $params['filters']['date_range'][1] . ' 23:59:59');
        }
        // END TODO

        $datatable->addColumn('id');
        // add columns that should be taken in query from the db, but aren't shown in data_table
        $datatable->addColumn('advertiser_id');
        $datatable->addColumn('account_id');
        $datatable->addColumn('campaign_id');
        $datatable->addColumn('ad_group_id');
        $datatable->addColumn('keyword_id');
        $datatable->addColumn('keyword_status');

        $columns = [
            'date'        => [
                'db_name'  => 'created_at', // Column name
                'callback' => function ($array) {
                    return $array['created_at'];
                }
            ],
            'account'     => [
                'db_name'  => 'account_name', // Column name
                'callback' => function ($array) {
                    return $array['account_name'];
                }
            ],
            'device'      => [
                'db_name'  => 'device', // Column name
                'callback' => function ($array) use ($filters) {
                    if (empty($filters['device'])) {
                        return null;
                    }
                    switch ($array['device']) {
                        case 'm' :
                            return 'Mobile';
                        case 't' :
                            return 'Tablet';
                        case 'c' :
                            return 'Computer';
                        case 'o' :
                            return 'Other';
                        case 'u' :
                            return 'Unset';
                        default :
                            return $array['device'];
                    }
                }
            ],
            'campaign'    => [
                'db_name'  => 'campaign_name', // Column name
                'callback' => function ($array) {
                    return $array['campaign_name'];
                }
            ],
            'ad_group'    => [
                'db_name'  => 'ad_group_name', // Column name
                'callback' => function ($array) use ($filters) {
                    if (empty($filters['campaign'])) {
                        return null;
                    }
                    return $array['ad_group_name'];
                }
            ],
            'match_type'  => [
                'db_name'  => 'match_type', // Column name
                'callback' => function ($array) use ($filters) {
                    if (empty($filters['ad_group'])) {
                        return null;
                    }
                    return $array['match_type'];
                }
            ],
            'keyword'     => [
                'db_name'  => 'keyword_name', // Column name
                'callback' => function ($array) use ($filters) {
                    if (empty($filters['match_type'])) {
                        return null;
                    }
                    return $array['keyword_name'];
                }
            ],
            'change_type' => [
                'db_name'  => 'change_type', // Column name
                'callback' => function ($array) {
                    return $array['change_type'];
                }
            ],
            'change_to'   => [
                'db_name'  => 'change_level', // Column name
                'callback' => function ($array) {
                    return $array['change_level'];
                }
            ],
            'old_value'   => [
                'db_name'  => 'old_value', // Column name
                'callback' => function ($array) {
                    switch ($array['change_type']) {
                        case 'status' :
                            if ($array['old_value'] === '1') {
                                return \View::make('partials.fields.status-label', [
                                    'text'  => 'Active',
                                    'class' => 'label-primary',
                                ])->render();
                            } elseif ($array['old_value'] === '0') {
                                return \View::make('partials.fields.status-label', [
                                    'text'  => 'Paused',
                                    'class' => 'label-default',
                                ])->render();
                            } else {
                                return $array['old_value'];
                            }

                        default :
                            return $array['old_value'] ?? '';
                    }
                }
            ],
            'new_value'   => [
                'db_name'  => 'new_value', // Column name
                'callback' => function ($array) {
                    switch ($array['change_type']) {
                        case 'status' :
                            if ($array['new_value'] === '1') {
                                return \View::make('partials.fields.status-label', [
                                    'text'  => 'Active',
                                    'class' => 'label-primary',
                                ])->render();
                            } elseif ($array['new_value'] === '0') {
                                return \View::make('partials.fields.status-label', [
                                    'text'  => 'Paused',
                                    'class' => 'label-default',
                                ])->render();
                            } else {
                                return $array['new_value'];
                            }

                        default :
                            return $array['new_value'] ?? '';
                    }
                }
            ],
            'user'        => [
                'db_name'  => 'user_id', // Column name
                'callback' => function ($array) {
                    return '<h6 style="margin: 0;">' . $array['users']['first_name'] . ' ' . $array['users']['last_name'] . '</h6>';
                }
            ],
            'actions'     => function ($array) {
                $res = '';

                $res .= \View::make('partials.fields.action-icon', [
                    'url'     => route('changes-log.ajax-modal', [
                        'method' => 'details',
                        'change_id' => $array['_id'],
                        'user_id' => $array['users']['id']
                    ]),
                    'icon'    => 'icon-magnifier',
                    'tooltip' => 'Details',
                    'attributes' => [
                        'data-toggle' => "modal",
                        'data-target' => '#ajax-modal'
                    ],
                ])->render();

                $res .= \View::make('partials.fields.action-icon', [
                    'url'     => route('changes-log.index', [
                        'advertiser' => $array['_advertiser_id'],
                        'account'    => $array['_account_id'],
                        'device'     => $array['device'],
                        'campaign'   => $array['_campaign_id'],
                        'ad_group'   => $array['_ad_group_id'],
                        'match_type' => $array['match_type'],
                        'keyword'    => $array['_keyword_id'],
                        'status'     => $array['_keyword_status'],
                    ]),
                    'icon'    => 'fa fa-sliders',
                    'tooltip' => 'Go to Changes Log Manager',
                ])->render();

                return $res;
            }
        ];

        $datatable->renderColumns($columns);
        $params['query'] = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }
}
