<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\DynamicLists\DynamicList;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\DynamicList\DynamicListRepo;
use App\Events\DynamicListEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Requests\Sites\DynamicListRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\ChoiceTrait;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use App\Libraries\Events\ModelEventActions;
use App\Libraries\SiteConnection\Exceptions\DynamicListException;
use SiteConnectionLib;
use View;


class DynamicListController extends Controller
{

    use DataTableTrait;
    use AjaxAlertTrait;
    use Select2Trait;
    use ChoiceTrait;

    /**
     * Site Permission variable.
     */
    const PERMISSION = 'top.dynamic-list';

    /**
     * @var string $assets .
     */
    protected $assets = ['datatables', 'form', 'top-app', 'nestable'];

    /**
     * @var string $page_title .
     */
    protected $page_title = 'Dynamic List Form';

    /**
     * @var DynamicListRepo $model_repo .
     */
    protected $model_repo;

    /**
     * @var Site $site .
     */
    protected $site;

    /**
     * DynamicListController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model_repo = new DynamicListRepo();
        $this->site       = SiteConnectionLib::getSite();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $site = $this->site;
        return view('pages.site.dynamic-list.index')
            ->with(compact('site'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $can  = $this->buildCanPermissions();
        $site = $this->site;

        if (!$can['add']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $is_static_list = 'auto';

        return view('pages.site.dynamic-list.form')
            ->with(compact('site', 'is_static_list'));
    }

    /**
     * Store dynamic list record.
     *
     * @param DynamicListRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws DynamicListException
     */
    public function store(DynamicListRequest $request)
    {
        $can = $this->buildCanPermissions();

        if (!$can['add']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $dynamic_list_record = $this->model_repo->createFromRequest($request);

        if (!$dynamic_list_record) {
            return redirect()
                ->route('sites.{site}.dynamic-list.edit', [$this->site->id, $dynamic_list_record->id])
                ->with(self::MESSAGE_TOAST, ["warning" => "the record wasn't created"]);
        }

        if (!$request->get('create_another')) {
            return redirect()
                ->route('sites.{site}.dynamic-list.index', $this->site->id)
                ->with(self::MESSAGE_TOAST, ["success" => "the record created successfully"]);
        }

        return redirect()
            ->route('sites.{site}.dynamic-list.create', [$this->site->id, $dynamic_list_record->id])
            ->with(self::MESSAGE_TOAST, ["success" => "the record created successfully"]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Site        $site
     * @param DynamicList $dynamic_list_record
     * $is_static_list - true is manually (or static), false is auto (or dynamic)
     *
     * @return \Illuminate\Http\Response
     * @internal param $site_id
     * @internal param DynamicList $dynamic_list_record
     */
    public function edit(Site $site, DynamicList $dynamic_list_record)
    {
        $can = $this->buildCanPermissions();

        if (!$can['edit']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $dynamic_list_record->load('labels', 'posts', 'filters', 'orders');
        $is_static_list = $dynamic_list_record->is_static_list ? 'manual' : 'auto';

        return view('pages.site.dynamic-list.form')
            ->with(compact('dynamic_list_record', 'site', 'is_static_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param DynamicListRequest $request
     * @param Site               $site
     * @param DynamicList        $dynamic_list_record
     *
     * @return \Illuminate\Http\Response
     */
    public function update(DynamicListRequest $request, Site $site, DynamicList $dynamic_list_record)
    {
        $can = $this->buildCanPermissions();
        if (!$can['edit']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $is_updated = $this->model_repo->updateFromRequest($dynamic_list_record, $request);

        if (!$is_updated) {
            return redirect()
                ->route('sites.{site}.dynamic-list.edit', [$this->site->id, $dynamic_list_record->id])
                ->with(self::MESSAGE_TOAST, ["warning" => "The data was not saved."])
                ->with(compact('dynamic_list_record'));
        }

        event(new DynamicListEvent($dynamic_list_record, ModelEventActions::UPDATED));

        if (!$request->get('create_another')) {
            $redirect_url = route('sites.{site}.dynamic-list.edit', [
                $this->site->id,
                $dynamic_list_record->id
            ]);

            return redirect()
                ->to($redirect_url . '#order-tab')// We use the hash value to load the order tab first.
                ->with(self::MESSAGE_TOAST, ["success" => "the record created successfully"]);
        }


        return redirect()
            ->route('sites.{site}.dynamic-list.create', [$this->site->id, $dynamic_list_record->id])
            ->with(self::MESSAGE_TOAST, ["success" => "the record created successfully"])
            ->with(compact('dynamic_list_record'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Site        $site
     * @param DynamicList $dynamic_list_record
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site, DynamicList $dynamic_list_record)
    {
        $can = $this->buildCanPermissions();


        if (!$can['delete']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $dynamic_list_record->delete();

        event(new DynamicListEvent($dynamic_list_record, ModelEventActions::DELETED));

        return redirect()->route('sites.{site}.dynamic-list.index', [$site->id])
            ->with(self::MESSAGE_TOAST, ["success" => "Dynamic record deleted"]);
    }

    /**
     * Duplicate
     *
     * @param             $site_id
     * @param DynamicList $dynamic_list
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate($site_id, DynamicList $dynamic_list)
    {
        $can = $this->buildCanPermissions();
        if (!$can['add']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $duplicated_dynamic_list = (new DynamicListRepo())->duplicate($dynamic_list);

        return redirect()
            ->route('sites.{site}.dynamic-list.edit', [$this->site->id, $duplicated_dynamic_list->id])
            ->with(self::MESSAGE_TOAST, ["success" => "Dynamic list Duplicated"]);

    }

    /**
     * Ajax Alert Duplicate
     *
     * @param $params
     *
     * @return $this
     */
    private function ajaxAlert_duplicate($params)
    {
        $list_id      = $params['list_id'];
        $dynamic_list = DynamicList::find($list_id);

        $text = "Are You sure you want to duplicate dynamic list <b><i> {$dynamic_list->name} </i></b> ?";

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.dynamic-list.{dynamic_list}.duplicate')
            ->with('route_params', [$this->site->id, $dynamic_list->id])
            ->with('action', 'form-confirm');
    }

    /**
     * DataTable list
     *
     * @param              $params
     * @param DatatableLib $datatable
     *
     * @return array
     */
    private function datatable_list($params, DatatableLib $datatable)
    {
        $params['searchColumns'] = [
            'id',
            'name'
        ];

        $can   = $this->buildCanPermissions();
        $query = DynamicList::with('contentType')->with('labels');

        $datatable->renderColumns([
                'id'           => 'id',
                'name'         => [
                    'id' => 'name',
                    'db_name' => 'name',
                    'callback' => function ($data) use ($can) {
                        $res = '';

                        if ($can['edit']) {
                            $res .= View::make('partials.containers.buttons.link', [
                                'route' => route('sites.{site}.dynamic-list.edit', [
                                    $this->site->id,
                                    $data['id']
                                ]),
                                'title' => 'Edit widget template',
                                'value' => $data['name']
                            ])->render();
                        }

                        return $res;
                    }
                ],
                'content_type' => [
                    'db_name'  => 'content_type_id', // Column name
                    'callback' => function ($array) {
                        return View::make('partials.containers.buttons.link', [
                            'route' => route('sites.{site}.posts.index', [
                                $this->site->id,
                                'content_type_id' => $array['content_type_id']
                            ]),
                            'value' => $array['content_type']['name']
                        ])->render();
                    }
                ],
                'actions'      => function ($data) use ($can) {
                    $res = '';
                    if ($can['edit']) {
                        $res .= View::make('partials.containers.buttons.link', [
                            'route' => route('sites.{site}.dynamic-list.edit', [
                                $this->site->id,
                                $data['id']
                            ]),
                            'title' => 'Edit widget template',
                            'icon'  => "icon-pencil",
                        ])->render();
                    }

                    if ($can['add']) {
                        $res .= '<span> ' . View::make('partials.containers.buttons.alert', [
                                'route'   => route('sites.{site}.dynamic-list.ajax-alert', [
                                    $this->site->id,
                                    'list_id' => $data['id'],
                                    'method'  => 'duplicate'
                                ]),
                                'title'   => 'Duplicate List',
                                'tooltip' => 'Duplicate List',
                                'icon'    => "fa fa-files-o",
                                'text'    => 'Are You sure you want to duplicate Dynamic List?',
                                'type'    => 'warning'
                            ])->render() . ' </span>';
                    }

                    if ($can['delete']) {
                        $res .= View::make('partials.containers.buttons.alert', [
                            'route' => route('sites.{site}.dynamic-list.ajax-alert', [
                                $this->site->id,
                                'dynamic_list_record' => $data['id'],
                                'method'              => 'delete'

                            ]),
                            'title' => 'Delete Dynamic Record',
                            'icon'  => "icon-trash",
                            'text'  => '',
                            'type'  => 'warning'
                        ])->render();
                    }

                    return $res;
                }
            ]
        );

        $params['recordsTotal'] = $query->count();
        $params['query']        = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }

    /**
     * Ajax sweet delete.
     *
     * @param $params
     *
     * @return mixed
     */
    private function ajaxAlert_delete($params)
    {
        $record = $this->model_repo->find($params['dynamic_list_record']);
        $text   = "Are You sure you want to Delete {$record->name} dynamic list record?";

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.dynamic-list.destroy')
            ->with('route_params', [$this->site->id, $record->id])
            ->with('action', 'delete');
    }

}
