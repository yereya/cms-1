<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Users\User;
use Illuminate\View\View;
use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use Venturecraft\Revisionable\Revision;
use App\Entities\Repositories\Sites\PostRepo;

use App\Http\Requests\UseRevisionRequest;
use App\Http\Requests\RevisionsByPeriodRequest;

class SitePostRevisionsController extends Controller
{

    /**
     * @ver array
     */
    protected $assets = [
        'https://cdnjs.cloudflare.com/ajax/libs/vis/4.20.1/vis.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/vis/4.20.1/vis-timeline-graph2d.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jsdiff/3.3.0/diff.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.1/vue.min.js',
        'assets/js/loader.vue.js',
        'assets/js/revisions.js',
        "assets/global/plugins/sweet-alert-2/sweetalert2.min.css",
        "assets/global/plugins/sweet-alert-2/sweetalert2.min.js"
    ];

    /**
     * Render the revisions view.
     *
     * @param Site     $site
     * @param integer  $post_id
     * @param PostRepo $post_repository
     *
     * @return View
     */
    public function show(Site $site, $post_id, PostRepo $post_repository)
    {
        $post                    = $post_repository->find($post_id);
        $post->content_type_data = $post->content;

        $vars = [
            'post'       => $post,
            'site'       => $site,
            'csrf_token' => csrf_token(),
            'end_date'   => date('Y-m-d'),
            'start_date' => date('Y-m-d', strtotime('-3 months')),
            'users' => User::select(['thumbnail','first_name','last_name'])->get()
        ];

        return view('pages.site.posts.revisions', [
            'inline_json_data' => sprintf('window.cms = %s;', json_encode($vars))
        ]);
    }

    /**
     * Get revisions for a given post in a time period.
     *
     * @param Site                     $site
     * @param integer                  $post_id
     * @param RevisionsByPeriodRequest $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function revisionsByPeriod(Site $site, $post_id, RevisionsByPeriodRequest $request)
    {
        $post_repository = new PostRepo;

        $post = $post_repository->find($post_id);

        $end_date   = $request->end_date . ' 23:59:59';
        $start_date = $request->start_date . ' 00:00:00';

        return $post_repository->revisionHistory($post, function ($query) use ($start_date, $end_date) {
            $query->whereBetween('created_at', [$start_date, $end_date]);
        });
    }

    /**
     * Get the previous revision for a given revision.
     *
     * @param Site    $site
     * @param integer $post_id
     * @param string  $key
     * @param string  $date
     *
     * @return array
     */
    public function previousRevision(Site $site, $post_id, $key, $date)
    {
        $time = strtotime($date);

        // Quick date validation.
        if ($time === false) {
            abort(400);
        }

        $types = ['App\Entities\Models\Sites\ContentType', 'App\Entities\Models\Sites\Post'];

        return [
            'key'      => $key,
            'post_id'  => $post_id,
            'revision' => Revision::where('created_at', '<', $date)
                ->whereIn('revisionable_type', $types)
                ->where('key', $key)
                ->orderBy('id', 'desc')
                ->take(1)
                ->first()
        ];
    }

    /**
     * Update a post or post content for the "REVISION PROPERTY" functionality.
     *
     * @param Site               $site
     * @param integer            $post_id
     * @param PostRepo           $post_repository
     * @param UseRevisionRequest $request
     *
     * @return array
     */
    public function update(Site $site, $post_id, PostRepo $post_repository, UseRevisionRequest $request)
    {
        $update_content_type = ($request->revisionable_type === 'App\Entities\Models\Sites\ContentType');
        $result              = $post_repository->savePost($post_id, $request->update_args, $update_content_type);

        if (!$result) {
            abort(400,'Bad request.');
        }

        return [
            'result'  => $result,
            'post_id' => $post_id,
            'site'    => $site->id,
            'msg'     => 'Revision property was successfully updated.'
        ];
    }


}