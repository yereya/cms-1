<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Section;
use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Requests\Sites\SiteSectionsFormRequest;
use App\Http\Traits\AjaxModalTrait;

/**
 * Class SiteSectionsController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteSectionsController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'top.sections';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Sections Management';

    /**
     * Display a listing of the resource.
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function index(Site $site)
    {
        $sections = $site->sections()->get();
        return view('pages.site.sections.index')->with(compact('site', 'sections'));
    }

    /**
     * Create
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function create(Site $site)
    {
        return view('pages.site.sections.form')->with(compact('site'));
    }

    /**
     * Store
     *
     * @param  SiteSectionsFormRequest $request
     * @param Site                     $site
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteSectionsFormRequest $request, Site $site)
    {
        $section = new Section($request->all(), $site);
        $section->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The section {$section->name} added"]);
        }

        return redirect()->route('sites.{site}.sections.index', $site->id)
                         ->with(self::MESSAGE_TOAST, ["success" => "The section {$section->name} added"]);
    }

    /**
     * Edit
     *
     * @param Site    $site
     * @param Section $section
     *
     * @return \Illuminate\View\View
     */
    public function edit(Site $site, Section $section)
    {
        return view('pages.site.sections.form', compact('site', 'section'));
    }

    /**
     * Update
     *
     * @param SiteSectionsFormRequest $request
     * @param Site                    $site
     * @param Section|int             $section
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteSectionsFormRequest $request, Site $site, Section $section)
    {
        $section->fill($request->all());
        $section->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The section {$section->name} saved"]);
        }

        return redirect()->route('sites.{site}.sections.index', $site->id)
                         ->with(self::MESSAGE_TOAST, ["success" => "The section {$section->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param Site    $site
     * @param Section $section
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, Section $section)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $section->delete();

        return redirect()->route('sites.{site}.sections.index', $site->id)
                         ->with(self::MESSAGE_TOAST, ["success" => "The section {$section->name} was deleted"]);
    }


    /**
     * Set Section
     * Saves the selected section to session and returns you back to your page
     *
     * @param Site $site
     * @param $section_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @internal param Section $section
     */
    public function setSection(Site $site, $section_id)
    {
        if ($section_id) {
            $section = Section::find($section_id);
            session([
                'site.section_id' => $section->id,
                'site.section_name' => $section->name,
            ]);
        } else {
            session([
                'site.section_id' => null,
                'site.section_name' => null,
            ]);
        }

        return redirect()->back();
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return \Illuminate\View\View
     */
    public function ajaxModal_deleteSection($params, $extra_params)
    {
        $site_id    = $extra_params[0];
        $section_id = $extra_params[1];

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Section')
                                                      ->with('btn_title', 'Delete Section')->with('route', [
                'sites.{site}.sections.destroy',
                $site_id,
                $section_id
            ]);
    }
}
