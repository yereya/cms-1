<?php namespace App\Http\Controllers\Sites\Templates;

use App\Entities\Models\Sites\ABTest;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteSetting;
use App\Entities\Models\Sites\SiteSettingDefault;
use App\Entities\Models\Sites\Templates\Column;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\Widget;
use App\Entities\Repositories\Sites\SettingsDefaultRepo;
use App\Entities\Repositories\Sites\TemplateRepo;
use App\Events\TemplateEvent;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;
use App\Libraries\Events\ModelEventActions;
use App\Libraries\PageInfo\TemplatePagesPostsInfoLib;
use App\Libraries\TemplateBuilder\BuilderLib as TemplateBuilder;
use App\Libraries\TemplateBuilder\Duplicate as TemplateDuplicate;
use App\Libraries\TemplateBuilder\TemplateScss;
use Illuminate\Http\Request;
use View;

/**
 * Class SiteTemplatesController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteTemplatesController extends Controller
{
    const PERMISSION = 'top.templates';
    use AjaxModalTrait;
    use AjaxAlertTrait;

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'nestable', 'top-app', 'code-mirror'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Templates Management';

    /**
     * SiteTemplatesController constructor.
     */
    public function __construct()
    {
        if (last(explode('.', \Route::currentRouteName())) == 'show') {
            $this->page_body_classes[] = 'page-sidebar-closed';
        }


        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $site           = SiteConnectionLib::getSite();
        $can            = $this->buildCanPermissions();
        $templates_tree = $site->templates()
            ->where('parent_id', null)
            ->with('childrenRecursive')
            ->get();

        return view('pages.site.templates.index')
            ->with(compact('site', 'templates_tree'))
            ->withCan($this->buildCanPermissions());
    }

    /**
     * Create
     *
     * @param $site_id
     *
     * @return \Illuminate\View\View
     */
    public function create($site_id)
    {
        $site      = SiteConnectionLib::getSite();
        $templates = $site->templates()->get();
        $parent_id = request()->get('parent_id');

        return view('pages.site.templates.form')->with(compact('site', 'templates', 'parent_id'));
    }

    /**
     * Store
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $site_id)
    {
        $site     = SiteConnectionLib::getSite();
        $template = new Template($request->all(), $site);
        $template->save();

        $settings_type = SiteSetting::getTemplateType($template->id);
        SettingsDefaultRepo::setDefaultSettingForType($site->id, $settings_type, SiteSettingDefault::TYPE_TEMPLATE);

        if ($request->get('create_another')) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["success" => "The template {$template->name} saved"]);
        }

        return redirect()
            ->route('sites.{site}.templates.index', $site->id)
            ->with(self::MESSAGE_TOAST, ["success" => "The template {$template->name} saved"]);
    }

    /**
     * Show
     *
     * @param          $site_id
     * @param Template $template
     *
     * @return \Illuminate\View\View
     */
    public function show($site_id, Template $template)
    {
        $site = SiteConnectionLib::getSite();

        return view('pages.site.templates.show')
            ->with(compact('site', 'template'))
            ->with('sidebar_class', 'page-sidebar-menu-closed');
    }

    /**
     * Builder
     *
     * @param          $site_id
     * @param Template $template
     *
     * @return View
     */
    public function builder($site_id, Template $template)
    {
        $site            = SiteConnectionLib::getSite();
        $top_assets_path = "top-assets/";
        $assets_path = "assets/";
        $js_dir          = public_path($top_assets_path . "js");

        \Assets::reset();

        \Assets::add(['site-templates-builder', 'nestable', 'datatables']);
        \Assets::addJs($top_assets_path . "libs/require.js");
        \Assets::addJs($assets_path . "js/main-builder.js?v=" . getFileUpdateTime($js_dir . '/app.js'));

        $template_repo         = new TemplateRepo();
        $template_classes      = $template_repo->getParentClasses($template);
        $template_bread_crumbs = $template_repo->getTemplateBreadCrumbsRoute($template);

        return view('pages.site.templates.builder')
            ->with(compact('site', 'template', 'template_classes', 'template_bread_crumbs'));
    }

    /**
     * Ajax Modal - update Scss
     *
     * @param Request  $request
     * @param          $site_id
     * @param Template $template
     */
    public function updateCustomScss(Request $request, $site_id, Template $template)
    {
        $template->fill($request->all());
        $template->save();
    }

    /**
     * Ajax Modal - update Scss
     *
     * @param Request  $request
     * @param Template $template
     */
    public function updateCustomHtml(Request $request, $site_id, Template $template)
    {
        $template->fill($request->all());
        $template->save();
    }

    /**
     * Store all template builder items and return them hierarchically
     *
     * @param Request  $request
     * @param          $site_id
     * @param Template $template
     *
     * @return array
     */
    public function storeBuilderItems(Request $request, $site_id, Template $template)
    {
        /** @var Site $site */
        $site = SiteConnectionLib::getSite();

        $builder = new TemplateBuilder($template);
        $builder->save($request->get('grid'));

        $template->grid   = $builder->getGrid();
        $template->assets = $builder->getAssets($site);

        event(new TemplateEvent($template, ModelEventActions::UPDATED, $site));

        return compact('site', 'template');
    }

    /**
     * Return all template builder Items Hierarchically
     *
     * @param          $site_id
     * @param Template $template
     *
     * @return array
     */
    public function getBuilderItems($site_id, Template $template)
    {
        /** @var Site $site */
        $site = SiteConnectionLib::getSite();

        $builder          = new TemplateBuilder($template);
        $template->grid   = $builder->getGrid();
        $template->assets = $builder->getAssets($site);

        return compact('site', 'template');
    }

    /**
     * Duplicate
     *
     * @param          $site_id
     * @param Template $template
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate($site_id, Template $template)
    {
        $duplicate_all_tree = request()->get('all-tree');

        $duplicate_lib = new TemplateDuplicate();
        if (isset($duplicate_all_tree)) {
            $duplicate_lib->duplicateTree($template);
        } else {
            $duplicate_lib->duplicateOne($template);
        }

        return redirect()
            ->route('sites.{site}.templates.index', $site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "Template duplicate"]);
    }

    /**
     * Edit
     *
     * @param          $site_id
     * @param Template $template
     *
     * @return \Illuminate\View\View
     */
    public function edit($site_id, Template $template)
    {
        $site      = SiteConnectionLib::getSite();
        $templates = $site->templates()
            ->where('id', '!=', $template->id)
            ->get();

        return view('pages.site.templates.form')->with(compact('site', 'templates', 'template'));
    }

    /**
     * Update
     *
     * @param Request  $request
     * @param          $site_id
     * @param Template $template
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $site_id, Template $template)
    {
        $site = SiteConnectionLib::getSite();
        $template->fill($request->all());
        $template->save();

        return redirect()
            ->route('sites.{site}.templates.index', $site->id)
            ->with(self::MESSAGE_TOAST, ["success" => "The template {$template->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param          $site_id
     * @param Template $template
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($site_id, Template $template)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $template->delete();
        $site     = SiteConnectionLib::getSite();

        event(new TemplateEvent($template, ModelEventActions::DELETED, $site));

        return redirect()
            ->route('sites.{site}.templates.index', $site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The Template {$template->name} was deleted"]);
    }

    /**
     * Generate the inline css of a given template.
     *
     * @param Site    $site
     * @param integer $template_id
     */
    public function generateCSS(Site $site, int $template_id)
    {
        $css = app(TemplateScss::class, [$template_id, $site])->generate(true);

        return response($css, 200)->header('Content-Type', 'text/css');
    }

    /**
     * Generate Template Scss
     * combines current template scss with all its ancestors
     *
     * @param $template
     *
     * @return mixed
     */
    private function getTemplateScss($template)
    {
        $scss = $template->scss;
        if ($parent = $template->parent) {
            $scss .= $this->getTemplateScss($parent);
        }

        return $scss;
    }

    /**
     * Ajax Alert Duplicate
     *
     * @param $params
     *
     * @return mixed
     */
    private function ajaxAlert_duplicate($params)
    {
        $template_id = $params['template_id'];
        $template    = Template::find($template_id);
        $site        = SiteConnectionLib::getSite();

        return view('pages.site.templates.alerts.duplicate-alert')
            ->with('route', route('sites.{site}.templates.{template}.duplicate', [$site->id, $template_id]))
            ->with('is_sub_template', $template->children->count());
    }

    /**
     * Ajax Modal - Template Delete confirmation
     *
     * @return \Illuminate\View\View
     */
    private function ajaxAlert_deleteTemplate()
    {

        $site        = SiteConnectionLib::getSite();
        $template_id = request()->get('template_id');
        $text        = 'Are you sure you want to delete template?';

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.templates.destroy')
            ->with('route_params', [$site->id, $template_id])
            ->with('action', 'delete');
    }

    /**
     * Ajax Modal - Row Form
     *
     * @param $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_row($params)
    {
        $row_data         = isset($params['data'])
            ? json_decode(trim(html_entity_decode(urldecode($params['data'])), '"'), true)
            : null;
        $parent_column_id = isset($params['column_id'])
            ? $params['column_id']
            : null;

        $site     = SiteConnectionLib::getSite();
        $ab_tests = ABTest::all();

        return view('pages.site.templates.modals.row')
            ->with(compact('row_data', 'parent_column_id', 'site', 'ab_tests'));
    }

    /**
     * Ajax Modal - Row Delete Confirmation
     *
     * @param $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_deleteRow($params)
    {
        $id      = $params['id'];
        $is_item = $params['is_item'];

        return view('pages.site.templates.modals.row-delete')->with(compact('id', 'is_item'));
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    private function ajaxModal_duplicateRow($params)
    {
        $id      = $params['id'];
        $is_item = $params['is_item'];

        return view('pages.site.templates.modals.row-duplicate')->with(compact('id', 'is_item'));
    }

    /**
     * Ajax Modal - Column Form
     *
     * @param $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_column($params)
    {
        $row_data = json_decode(trim(html_entity_decode(urldecode($params['row_data'])), '"'), true);
        $data     = isset($params['data']) ? json_decode(trim(html_entity_decode(urldecode($params['data'])), '"'),
            true) : null;

        return view('pages.site.templates.modals.column')->with(compact('row_data', 'data'));
    }

    /**
     * Ajax Modal - Column Delete Confirmation
     *
     * @param $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_deleteColumn($params)
    {
        $row_id = $params['row_id'];
        $id     = $params['id'];

        return view('pages.site.templates.modals.column-delete')->with(compact('row_id', 'id'));
    }

    /**
     * Ajax Modal Widgets List
     * returns widgets type list
     *
     * @param $params
     *
     * @return mixed
     */
    private function ajaxModal_widgetsList($params)
    {
        $uri_params = request()->route()->parameters();
        $column_id  = $params['column_id'];

        $site     = Site::findOrFail($uri_params['site']);
        $template = Template::findOrFail($uri_params['template']);
        $widgets  = Widget::all();

        return view('pages.site.templates.modals.widgets-list')->with(compact('site', 'template', 'widgets',
            'column_id'));
    }

    /**
     * Ajax Modal - Widget
     *
     * @param $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_widget($params)
    {
        $uri_params = request()->route()->parameters();
        $column_id  = $params['column_id'];

        $site     = Site::findOrFail($uri_params['site']);
        $template = Template::findOrFail($uri_params['template']);
        $widgets  = Widget::with('widgetData')->where('id', $params['widget_id'])->get();

        return view('pages.site.templates.modals.widget')->with(compact('site', 'template', 'widgets', 'column_id'));
    }

    /**
     * Ajax Modal - Widget Delete Confirmation
     *
     * @param array $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_widgetDelete($params)
    {
        $column_id = $params['column_id'];
        $id        = $params['id'];

        return view('pages.site.templates.modals.widget-delete')->with(compact('column_id', 'id'));
    }

    /**
     * Ajax Modal - Widget Data
     * This widget is accessed when editing or creating a widget
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_widgetData()
    {
        $uri_params  = request()->route()->parameters();
        $site        = getSiteFromRouteParameter();
        $template    = isset($uri_params['template']) ? getTemplateFromRouteParameter() : new Template();
        $widget      = Widget::with('templates')->find(request()->get('widget_id'));
        $column      = Column::find(request()->get('column_id'));
        $widget_data = WidgetData::find(request()->get('data_id'));

        return with(new WidgetFormBuilder($site, $template, $widget, $column, $widget_data))->view();
    }

    /**
     * Ajax Modal - Widget Duplicate
     * This widget is accessed when editing or creating a widget
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_widgetDuplicate()
    {
        $uri_params      = request()->route()->parameters();
        $site            = getSiteFromRouteParameter();
        $template        = isset($uri_params['template']) ? getTemplateFromRouteParameter() : new Template();
        $widget          = Widget::with('templates')->find(request()->get('widget_id'));
        $column          = Column::find(request()->get('column_id'));
        $widget_data     = WidgetData::find(request()->get('data_id'));
        $widget_data->id = null;

        return with(new WidgetFormBuilder($site, $template, $widget, $column, $widget_data))->view();
    }

    /**
     * Ajax Modal - show template's scss
     *
     * @param $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_customScss($params)
    {
        $uri_params = request()
            ->route()
            ->parameters();

        $template_id = request()->input('template');


        $site_id  = $uri_params['site'];
        $template = Template::findOrFail($template_id);

        return view('pages.site.templates.modals.custom-scss')->with(compact('site_id', 'template'));
    }

    /**
     * Ajax Modal - show template's custom html
     *
     * @param $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_customHtml($params)
    {
        $uri_params  = request()
            ->route()
            ->parameters();
        $template_id = request()->input('template');

        $site_id  = $uri_params['site'];
        $template = Template::findOrFail($template_id);

        return view('pages.site.templates.modals.custom-html')->with(compact('site_id', 'template'));
    }

    /**
     * Ajax Modal File Modal
     *
     * @param $params
     *
     * @return mixed
     */
    private function ajaxModal_fileModal($params)
    {
        return view('pages.site.content-type.data.file-manager-modal')
            ->with('title', 'File Manager')
            ->with('field', $params['field'] ?? null);
    }

    /**
     * Ajax Modal Pages Info
     */
    private function ajaxModal_pagesInfo()
    {
        $site                   = SiteConnectionLib::getSite();
        $template_id            = request()->get('template_id');
        $template               = Template::find($template_id);
        $template_page_info_lib = new TemplatePagesPostsInfoLib($site);
        $template_page_info_lib->build($template);
        $pages_info = $template_page_info_lib->getPagesInfo();
        $posts_info = $template_page_info_lib->getPostsInfo();

        return view('pages.site.templates.pages-info')
            ->with(compact('pages_info', 'posts_info'));
    }
}