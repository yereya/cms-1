<?php

namespace App\Http\Controllers\Sites;


use App\Entities\Models\Sites\Label;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\LabelRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class SiteLabelsController extends Controller
{
    use AjaxAlertTrait;
    use DataTableTrait;
    use AjaxModalTrait;
    use Select2Trait;

    const PERMISSION = 'top.labels';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'nestable', 'form'];


    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Labels';

    /**
     * index
     *
     * @return mixed
     */
    public function index()
    {
        $user = auth()->user();
        $can  = [
            'add'    => $user->can(self::PERMISSION . '.add'),
            'edit'   => $user->can(self::PERMISSION . '.edit'),
            'delete' => $user->can(self::PERMISSION . '.delete')
        ];
        $site = SiteConnectionLib::getSite();

        $label_tree = Label::where('parent_id', null)
            ->with('childrenRecursive')->get();

        return view('pages.site.labels.index')
            ->with(compact('site', 'label_tree', 'can'));
    }

    /**
     * Create
     *
     * @param Site $site
     *
     * @return View
     */
    public function create(Site $site)
    {
        $label_tree = LabelRepo::getAllInSelectFormat();

        return view('pages.site.labels.form')
            ->with(compact('site', 'label_tree'));
    }

    /**
     * store
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $site  = SiteConnectionLib::getSite();
        $label = (new LabelRepo())->storeFromRequest();
        if (request()->get('create_another')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["success" => "The label {$label->name} saved"]);
        }

        return redirect()->route('sites.{site}.labels.index', $site->id)
            ->with(self::MESSAGE_TOAST, ["success" => "The label {$label->name} saved"]);
    }

    /**
     * edit
     *
     * @param Site  $site
     * @param Label $label
     *
     * @return mixed
     */
    public function edit(Site $site, Label $label)
    {
        $label_tree    = LabelRepo::getAllInSelectFormat();
        $label_collect = $label; // Name changed due to problem with the name label being too general for views

        return view('pages.site.labels.form')
            ->with(compact('site', 'label_collect', 'label_tree'));
    }

    /**
     * update
     *
     * @param Request $request
     * @param Site    $site
     * @param Label   $label
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Site $site, Label $label)
    {
        $label->fill($request->all());
        $label->save();

        return redirect()->route('sites.{site}.labels.index', $site->id)
            ->with(self::MESSAGE_TOAST, ["success" => "The label {$label->name} saved"]);
    }

    /**
     * destroy
     *
     * @param Site  $site
     * @param Label $label
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, Label $label)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $label->delete();

        return redirect()->back()
            ->with(self::MESSAGE_TOAST, ["success" => "The Label \"{$label->name}\" was deleted"]);
    }

    /**
     * ajaxAlert_delete
     *
     * @return View
     */
    public function ajaxAlert_delete()
    {
        $site  = SiteConnectionLib::getSite();
        $label = Label::find(request()->get('label_id'));

        if (!count($label)) {
            return "the label could not be found.";
        }

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', "Please confirm deleting Label \"$label->name\"")
            ->with('route', 'sites.{site}.labels.destroy')
            ->with('action', 'delete')
            ->with('route_params', [$site->id, $label->id]);
    }

    /**
     * Get List of all labels
     *
     * @param $params
     *
     * @return mixed
     */
    public function select2_list($params)
    {
        $query = Label::select('id', 'name as text');

        $this->select2FilterByCurrentValue($query);
        $this->select2FilterBySearchQuery($query);

        return $query->get();
    }
}