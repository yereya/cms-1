<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Media;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteDomain;
use App\Entities\Repositories\Sites\LabelRepo;
use App\Entities\Repositories\Sites\MediaRepo;
use App\Http\Traits\AjaxModalTrait;
use App\Libraries\Elfinder\ElFinder;
use App\Libraries\Media\FileInfo;
use App\Libraries\Media\MediaUploader;
use App\Libraries\SiteConnection\SiteConnectionLib;
use Barryvdh\Elfinder\Connector;
use Barryvdh\Elfinder\ElfinderController;
use Barryvdh\Elfinder\Session\LaravelSession;
use File;
use Illuminate\Foundation\Application;

use Illuminate\Http\Request;

class FileManagerController extends ElfinderController
{
    use AjaxModalTrait;

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'top-app'];

    private $site;

    /**
     * FileManagerController constructor.
     * Override parent constructor
     *
     * @param Application $app
     * @param Request     $request
     */
    public function __construct(Application $app, Request $request)
    {
        if (app()->bound('debugbar')) {
            \Debugbar::disable(); //disable debugbar in iframe
        }

        $this->site = Site::find($request->get('site'));
        parent::__construct($app);
    }

    /**
     * @Override from parent class
     *
     * Trigger to showConnector in elfinder
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showConnector()
    {
        /** override this path to drop upload */
        $_POST['upload_path'] = [];

        $this->buildDirectory();
        $opts = $this->buildElfinderoptions();
        $connector = new Connector(new elFinder($opts));
        $connector->run();
        $response = $connector->getResponse();

        $to_save = json_decode($response->getContent()); //get content

        //if content have added property - save to database
        if (property_exists($to_save, 'files') && request()->has('isAlt')) {
            // need to open alt modal (custom functionality)
            return $this->altModalView($to_save);
        } else if (property_exists($to_save, 'files') && request()->has('isLabels')) {
            return $this->labelsModalView($to_save);
        } else if (property_exists($to_save, 'files') && request()->has('fileInfo')) {
            return $this->findByPath($to_save, request()->get('field_name'));
        }

        return $response;
    }

    /**
     * Get media by filename
     *
     * @param Request $request
     *
     * @return array
     */
    public function getFileId(Request $request) {
        $folder = empty($request->get('folder')) ? '' : $request->get('folder');
        $filename = $request->get('filename');

        $media = Media::where('folder', $folder)->where('filename', $filename)->first();

        $result = [
            'status' => !empty($media),
            'desc' => empty($media) ? 'This file not found.' : ''
        ];

        if ($media) {
            $result['data'] = [
                'id' => $media->id,
                'path' => $media->getFullMediaPath(),
                'filename' => $media->filename,
                'size' => $media->size
            ];
        }

        return $result;
    }

    /**
     * @Override showTinyMce4
     *
     * @return mixed
     */
    public function showTinyMCE4()
    {
        return $this->app['view']
            ->make($this->package . '::tinymce4')
            ->with($this->getViewVars())
            ->with('site', $this->site);
    }

    /**
     * Update file info
     *
     * @param Request $request
     *
     * @return array
     */
    public function updateFileInfo(Request $request) {
        $result = [
            'status' => 0,
            'desc' => '',
            'data' => [],
        ];

        if (!$request->has('media_id')) {
            $result['desc'] = 'Value media_id not found';
            return $result;
        }

        $media = Media::find($request->get('media_id'));

        if (!$media) {
            $result['desc'] = 'Media not found';
            return $result;
        }

        $media->fill($request->all());

        try {
            $media->save();
            $result['status'] = 1;
            $result['data'] = $media->toArray();
        } catch (\Exception $e) {
            $result['desc'] = 'Update not saved, ' . $e->getMessage();
        }

        return $result;
    }

    /**
     * Connect labels to media
     *
     * @param Request $request
     *
     * @return array
     */
    public function updateFileLabels(Request $request) {
        $result = [
            'status' => 0,
            'desc' => '',
            'data' => [],
        ];

        if (!$request->has('media_id')) {
            $result['desc'] = 'Value media_id not found';
            return $result;
        }

        $media_repo = new MediaRepo();
        $model = $media_repo->find($request->get('media_id'));

        if (!$model) {
            $result['desc'] = 'Media not found';
            return $result;
        }

        try {
            $result['status'] = 1;
            $result['data'] = $model->labels()->sync($request->get('labels') ?? []);
        } catch (\Exception $e) {
            $result['desc'] = 'Update not saved, ' . $e->getMessage();
        }

        return $result;
    }

    /**
     * Create modal to update media file
     *
     * currently alt only
     *
     * @param $hashes
     *
     * @return $this
     */
    private function altModalView($hashes) {
        $file = $this->findByPath($hashes);

        return view('pages.site.media.file-info-modal')
            ->with('file', $file)
            ->with('site', $this->site);
    }

    /**
     * Create labels modal
     *
     * @param $hashes
     *
     * @return $this
     */
    private function labelsModalView($hashes) {
        $name = $hashes->files[0]->path ?? null;
        $file = (new MediaRepo())->findByName($name, 'labels');
        $labels = (new LabelRepo())->model()->select('id', 'name as text')->get();

        $file_labels = isset($file->labels) ? $file->labels->pluck('id')->toArray() : [];

        return view('pages.site.media.file-label-modal')
            ->with(compact('file', 'labels', 'file_labels'))
            ->with('site', $this->site);
    }

    /**
     * Find media by path name
     *
     * @param $hashes
     *
     * @param null $field
     *
     * @return array
     */
    private function findByPath($hashes, $field = null) {
        $file = null;
        foreach ($hashes as $hash) {
            $file_path = substr($hash[0]->path, strlen('images'));
            $media = Media::where('path', $file_path)->first();
            $file['model'] = $media ? $media->toArray() : null;
            $file['full_path'] = $media ? $media->getFullMediaPath() : null;
            if ($field) {
                $file['field_name'] = $field;
            }
        }

        return $file;
    }

    /**
     * Save files info to media table
     *
     * @param array $files_info
     */
    private function store($files_info) {
        $medias = [];
        foreach ($files_info->added as $file) {
            $path = substr($file->url, strlen(url($this->getMediaPath()))); //get only path after media folder
            $folder = substr($path, 0, strlen($path) - strlen($file->name) - 1); //get sub folders (if exists), 1 remove directory separator

            if ($file->mime !== 'directory') {
                $media_file = new FileInfo($this->site);
                $media_file->setFilename($file->name);
                $media_file->setSiteId($this->site->id);
                $media_file->setSiteName($this->site->name);
                $media_file->setMime($file->mime);
                $media_file->setPath($path);
                $media_file->setFolder($folder);
                $medias[] = $media_file->out();
            }
        }

        try {
            Media::insert($medias);
            // upload files to sites
            $this->updateSite($medias);
        } catch (Exception $e) {
            // TODO save error
        }
    }

    /**
     * @Override from parent class
     *
     * Build ElFinder options
     *
     * @return array
     */
    private function  buildElfinderoptions() {
        $roots[] = [
            'driver' => 'LocalFileSystem', // driver for accessing file system (REQUIRED)
            'path' => $this->getMediaPath(true),
            'URL' => url($this->getMediaPath()), // URL to files (REQUIRED)
            'accessControl' => $this->app->config->get('elfinder.access') // filter callback (OPTIONAL)
        ];

        if (app()->bound('session.store')) {
            $sessionStore = app('session.store');
            $session = new LaravelSession($sessionStore);
        } else {
            $session = null;
        }

        $rootOptions = $this->app->config->get('elfinder.root_options', array());
        foreach ($roots as $key => $root) {
            $roots[$key] = array_merge($rootOptions, $root);
        }

        $opts = $this->app->config->get('elfinder.options', array());
        $opts = array_merge($opts, ['roots' => $roots, 'session' => $session]);

        return $opts;
    }

    /**
     * Check if directory exists if not create with permission 0755
     */
    private function buildDirectory() {
        $dirs = explode('/', $this->getMediaPath());

        $path = public_path();
        foreach ($dirs as $dir) {
            $path .= DIRECTORY_SEPARATOR . $dir;
            if (!File::isDirectory($path) || !File::exists($path)) {
                File::makeDirectory($path, 0775, true, true);
            }
        }
    }

    /**
     * Open file manager modal
     *
     * @param $params
     * @param $extra_params
     *
     * @return $this
     */
    public function ajaxModal_fileModal($params, $extra_params)
    {
        return view('pages.site.media.file-manager-modal')
            ->with('title', 'File Manager')
            ->with('field', $params['field'] ?? null)
            ->with('site', $this->site);
    }

    /**
     * @param $medias
     *
     * @return $this
     */
    private function updateSite($medias) {
        if (count($medias) == 0) {
            return null;
        }

        $paths = [];
        foreach ($medias as $media) {
            $paths[] = url($this->getMediaPath()) . $media['path'];
        }

        $domain = SiteDomain::where('site_id', $this->site->id)->where('domain_dev', (env('APP_ENV') == 'local'))->first();

        if ($domain) {
            $client = new MediaUploader($domain->domain);
            $result = $client->uploadFile($paths, MediaUploader::MEDIA_TYPE_FILE, $this->site->id);

            return $result;
        }
    }

    /**
     * Return media path
     *
     * @param bool $full
     *
     * @return string
     */
    private function getMediaPath(bool $full = false) {
        return $full
            ? public_path() . config('media.directory') . $this->site->id . DIRECTORY_SEPARATOR . 'images'
            : config('media.directory') . $this->site->id . DIRECTORY_SEPARATOR . 'images';
    }
}
