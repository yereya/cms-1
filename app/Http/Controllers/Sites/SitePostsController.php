<?php

namespace App\Http\Controllers\Sites;


use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Section;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Repositories\Sites\ContentAuthorRepo;
use App\Entities\Repositories\Sites\EntityLockRepo;
use App\Entities\Repositories\Sites\LabelRepo;
use App\Entities\Repositories\Sites\LinkContentTypeRepo;
use App\Entities\Repositories\Sites\MediaRepo;
use App\Entities\Repositories\Sites\PageRepo;
use App\Entities\Repositories\Sites\PostRepo;
use App\Entities\Repositories\Sites\SectionRepo;
use App\Entities\Repositories\Sites\SettingsDefaultRepo;
use App\Entities\Repositories\Sites\SiteContentTypeRepo;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SitePostFormRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\EntityLockTrait;
use App\Http\Traits\Select2Trait;
use App\Http\Traits\ToggleProperty;
use App\Libraries\Datatable\DatatableLib;
use App\Libraries\Widgets\Helpers\DynamicList;
use App\Libraries\Widgets\WidgetPostList;
use Illuminate\Http\Request;
use SiteSetting;
use SiteSettingDefault;
use View;

class SitePostsController extends Controller
{
    use AjaxModalTrait;
    use AjaxAlertTrait;
    use ToggleProperty;
    use DataTableTrait;
    use Select2Trait;
    use EntityLockTrait;

    const PERMISSION = 'top.posts';

    const PUBLISHED_STATUS_CLASS = [
        'draft'          => 'bg-grey-salt bold',
        'published'      => 'bg-blue bold',
        'future_publish' => 'bg-yellow-casablanca bold',
    ];

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'nestable', 'top-app', 'entity_lock'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Posts';

    /**
     * Hold the current site object.
     *
     * @ver Site
     */
    private $site;

    /**
     * SitePostsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->site = SiteConnectionLib::getSite() ?? Site::where('name', config('app.default_site'))->first();
    }

    /**
     * index
     *
     * @return mixed
     */
    public function index()
    {
        $can           = $this->buildCanPermissions();
        $content_types = (new SiteContentTypeRepo)->getBySiteId($this->site->id);

        // Build portlet action buttons
        $portlet_actions_menu_items = $content_types->map(function ($item) use ($can) {
            return [
                'title'      => "Add new $item->name",
                'route'      => $item->slug == 'pages' ?
                    route('sites.{site}.pages.create', [$this->site->id]) :
                    route('sites.{site}.posts.create', [$this->site->id, 'content_type_id' => $item->id]),
                'permission' => $can['add'],
            ];
        })->toArray();

        return view('pages.site.posts.index')
            ->withCan($this->buildCanPermissions())
            ->withSections(Section::all()->pluck('name', 'id'))
            ->withContentTypes($content_types->pluck('name', 'id'))
            ->withLabels(LabelRepo::getAllInSelectFormat())
            ->withSite($this->site)
            ->withPortletActionsMenuItems($portlet_actions_menu_items);
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $content_type_id = request()->get('content_type_id');
        $content_type    = SiteContentType::find($content_type_id);
        $labels          = LabelRepo::getAllInSelectFormat();
        $post_fields     = SiteContentTypeField::with('fieldGroup')
            ->where('type_id', $content_type->id)
            ->orderBy('priority')
            ->get();

        $post_repo      = new PostRepo();
        $grouped_fields = $post_repo->getGroupedFields($post_fields);
        $linked_posts   = $post_repo->getLinkedPosts($post_fields);
        $linked_to_post = []; //new post

        $user_selected_section = getSiteSectionId();

        $content_authors = (new ContentAuthorRepo)->getIdNameList();

        $sections = (new SectionRepo())->getIdNameList();

        return view('pages.site.posts.form')
            ->with(compact('content_type',
                'sections', 'user_selected_section', 'labels',
                'grouped_fields', 'content_authors', 'linked_posts', 'linked_to_post'))
            ->with('fields', $post_fields)
            ->with('site', $this->site);
    }

    /**
     * store
     *
     * @param SitePostFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(SitePostFormRequest $request)
    {
        $content_type_id = request()->get('content_type_id');
        $content_type    = SiteContentType::find($content_type_id);
        $post            = (new PostRepo())->createWithContentData($request, $content_type);

        $settings_type = SiteSetting::getContentTypeItemType($content_type->id, $post->id);
        SettingsDefaultRepo::setDefaultSettingForType($this->site->id, $settings_type,
            SiteSettingDefault::TYPE_CONTENT_TYPE_ITEM);

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The data was added"]);
        }

        return redirect()
            ->route('sites.{site}.posts.edit', [$this->site->id, $post->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The data was added"]);
    }

    /**
     * edit
     *
     * @param Site $site
     * @param Post $post
     *
     * @return mixed
     */
    public function edit(Site $site, Post $post)
    {
        $post->load('redirectUrls');
        $content_type = $post->contentType;

        $labels = LabelRepo::getAllInSelectFormat();

        $post_fields = SiteContentTypeField::with('fieldGroup')
            ->where('type_id', $post->content_type_id)
            ->orderBy('priority')
            ->get();

        (new MediaRepo)->replaceImageIdToPath($post->content, $post_fields);

        $post_repo = new PostRepo();
        $link_repo = new LinkContentTypeRepo();

        $linked_posts          = $post_repo->getLinkedPosts($post_fields);
        $linked_to_post        = $link_repo->getContentTypeLink($post_fields, $post->id);
        $grouped_fields        = $post_repo->getGroupedFields($post_fields);
        $user_selected_section = getSiteSectionId();
        $dynamic_lists         = $post_repo->getDynamicLists($post->id);

        $content_authors = (new ContentAuthorRepo)->getIdNameList();

        $sections = (new SectionRepo())->getIdNameList();

        $this->addLock($post->id, $site->id);

        $urls = $post_repo->getUrls($post->id);

        return view('pages.site.posts.form')
            ->with(compact('site', 'content_type', 'post',
                'sections', 'user_selected_section', 'labels',
                'grouped_fields', 'content_authors', 'linked_posts', 'linked_to_post',
                'dynamic_lists','urls'
            ))
            ->with('fields', $post_fields);
    }

    /**
     * update
     *
     * @param SitePostFormRequest $request
     * @param Site                $site
     * @param Post                $post
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SitePostFormRequest $request, Site $site, Post $post)
    {
        if (!$this->isMyLock($post->id, $site->id)) {

            return redirect()->back()->with(self::MESSAGE_TOAST, ["error" => "The Post is lock by someone else"]);
        }

        (new PostRepo())->updateWithContentData($request, $post);

        if ($request->has('likes_count')){
            $like = $post->likes()->first();
            if (!$like){
                $post->likes()->create(['like' => $request->input('likes_count')]);
            }else{
                $like->like = $request->input('likes_count');
                $like->save();
            }
        }

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The data was saved"]);
        }

        return redirect()
            ->route('sites.{site}.posts.edit', [$site->id, $post->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The data was saved"]);
    }

    /**
     * destroy
     *
     * @param Request $request
     * @param         $site_id
     * @param         $post_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $site_id, $post_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        if ($request->filters) {
            $filters = json_decode($request->filters, true);
        }

        $trashed = !!$request->trashed;
        if ($trashed) {
            $post = Post::withTrashed()->find($post_id);
            $post->forceDelete();
        } else {
            $post = Post::find($post_id);
            (new PostRepo())->delete($post);
        }

        $route_params = isset($filters) ? array_merge([$site_id], $filters) : [$site_id];

        return redirect()->route('sites.{site}.posts.index', $route_params)
            ->with(self::MESSAGE_TOAST, ["success" => "The data was deleted"]);
    }

    /**
     * @return mixed
     */
    public function ajaxAlert_delete()
    {
        $request = request();
        $trashed = !!$request->trashed;
        if ($trashed) {
            $post = Post::withTrashed()->find($request->route()->getParameter('post'));
        } else {
            $post = Post::find($request->route()->getParameter('post'));
        }

        list($action, $text) = $this->getAlertView($post, $this->site->id, $trashed);

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.posts.destroy')
            ->with('action', $action)
            ->with('route_params', [
                $this->site->id,
                $request->route()->getParameter('post'),
                'trashed' => $trashed,
                'filters' => $request->get('filters')
            ]);
    }

    /**
     * Duplicate
     *
     * @param      $site
     * @param Post $post
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate($site_id, Post $post)
    {
        $can = $this->buildCanPermissions();
        if (!$can['add']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $post_repo      = new PostRepo();
        $duplicate_post = $post_repo->duplicate($post);
        $isPage         = $post_repo->isPage($post);
        $route          = $isPage ? 'sites.{site}.pages.edit' : 'sites.{site}.posts.edit';

        return redirect()
            ->route($route, [$site_id, $duplicate_post->id])
            ->with(self::MESSAGE_TOAST, ["success" => ($isPage ? "Page" : "Post") . " Duplicate"]);
    }

    /**
     * Ajax Alert Duplicate
     *
     * @param $params
     *
     * @return $this
     */
    private function ajaxAlert_duplicate($params)
    {
        $post_id = request()->route()->getParameter('post');
        $post    = Post::find($post_id);

        $post_repo = new PostRepo();
        $isPage    = $post_repo->isPage($post);
        if ($isPage && !$post->children->isEmpty()) {
            $text = "Cannot duplicate page <b><i> {$post->name} </i></b> that has nested page.";

            return view('partials.containers.alerts.ajax-alert')
                ->with('action', 'info')
                ->with('text', $text);
        }

        $text = "Are You sure you want to duplicate <b><i> {$post->name} </i></b> " . ($isPage ? 'Page' : 'Post') . ' ?';

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.posts.{post}.duplicate')
            ->with('route_params', [$this->site->id, $post->id])
            ->with('action', 'form-confirm');
    }

    /**
     * Get Alert View
     *
     * @param $post
     * @param $site_id
     *
     * @return array
     */
    private function getAlertView($post, $site_id, $trashed): array
    {
        if ($trashed) {
            $action = 'delete';
            $text   = 'Do you want to permanently delete this post?';

            return [$action, $text];
        }

        $widget_data_repo      = new WidgetDataRepo();
        $widget_data_to_delete = $widget_data_repo->getWidgetDatasConnectedToPost($post);

        if (empty($widget_data_to_delete)) {
            $action = 'delete';
            $text   = 'Do you want to delete?';

            return [$action, $text];
        } else {
            $action = 'info';
            $text   = \View::make('pages.site.posts.post-delete-message', [
                'widget_data_to_delete' => $widget_data_to_delete,
                'site_id'               => $site_id
            ])->render();

            return [$action, $text];
        }
    }


    /**
     * @param              $params
     * @param DatatableLib $datatable
     *
     * @return array
     */
    public function dataTable_list($params, DatatableLib $datatable)
    {
        $page_content_type_id = (new SiteContentTypeRepo())->getPageContentTypeId();
        $can                  = $this->buildCanPermissions();

        $entity_lock_repo = new EntityLockRepo();
        $locked_posts     = $entity_lock_repo->getAllLocksForType($this->site->id, $this->getLockType())
            ->keyBy('entity_id');

        $datatable->ignoreFilters(['type', 'post_type']);

        $query = Post::with('contentType')
            ->with('section');

        $post_type = $params['filters']['post_type'] ?? null;
        $trashed   = false;
        if ($post_type) {
            if ($post_type == 'trashed') {
                $query->onlyTrashed();
                $trashed = true;
            } else {
                if ($post_type == 'published') {
                    $query->whereNotNull('published_at');
                } else {
                    if ($post_type == 'drafts') {
                        $query->whereNull('published_at');
                    }
                }
            }
        }

        $params['recordsTotal']  = $query->count();
        $params['searchColumns'] = ['name', 'slug', 'id'];

        $post_helper = new Post();
        $datatable->renderColumns([
            'id'           => 'id',
            'name'         => [
                'db_name'  => 'name', // Column name
                'callback' => function ($data) use ($page_content_type_id, $locked_posts) {
                    if ($data['content_type_id'] == $page_content_type_id) {
                        $post_route_edit = "sites.{site}.pages.edit";
                    } else {
                        $post_route_edit = "sites.{site}.posts.edit";
                    }

                    $res = View::make('partials.containers.buttons.link', [
                        'route' => route($post_route_edit, [
                            $this->site->id,
                            $data['id']
                        ]),
                        'value' => $data['name']
                    ])->render();

                    if (isset($locked_posts[$data['id']])) {
                        $user    = $locked_posts[$data['id']]->user;
                        $used_by = ' <span class="label left-offset label-xs bg-grey-salt bold">  Editing: ' . $user->username . ' </span>';
                        $res     .= $used_by;
                    }

                    return $res;
                }
            ],
            'slug'         => 'slug',
            'content_type' => [
                'db_name'  => 'content_type_id', // Column name
                'callback' => function ($array) {
                    return View::make('partials.containers.buttons.link', [
                        'route' => route('sites.{site}.posts.index', [
                            $this->site->id,
                            'content_type_id' => $array['content_type_id']
                        ]),
                        'value' => $array['content_type']['name']
                    ])->render();
                }

            ],
            'section'      => [
                'db_name'  => 'section_id', // Column name
                'callback' => function ($array) {
                    if (!$array['section_id']) {
                        return;
                    }

                    return View::make('partials.containers.buttons.tag', [
                        'route' => route('sites.{site}.posts.index', [
                            $this->site->id,
                            'section_id' => $array['section_id']
                        ]),
                        'class' => 'bold',
                        'link'  => true,
                        'value' => $array['section']['name']
                    ])->render();
                }
            ],
            'update'       => 'updated_at',
            'status'       => [
                'db_name'  => 'published_at', // Column name
                'callback' => function ($data) use ($post_helper) {
                    $post_helper->published_at = $data['published_at'] ?? null;

                    $tag_params = [
                        'value' => $post_helper->published_display_status,
                        'class' => self::PUBLISHED_STATUS_CLASS[$post_helper->published_status]
                    ];

                    return View::make('partials.containers.buttons.tag', $tag_params)->render();
                },
            ],
            'actions'      => function ($data) use ($can, $page_content_type_id, $params, $trashed) {
                if ($data['content_type_id'] == $page_content_type_id) {
                    $post_route_alert = "sites.{site}.pages.{page}.ajax-alert";
                    $post_route_edit  = "sites.{site}.pages.edit";
                } else {
                    $post_route_alert = "sites.{site}.posts.{post}.ajax-alert";
                    $post_route_edit  = "sites.{site}.posts.edit";
                }
                $res = '';

                if (!$trashed) {
                    if ($can['edit']) {
                        $res .= View::make('partials.containers.buttons.link', [
                            'route' => route($post_route_edit, [
                                $this->site->id,
                                $data['id']
                            ]),
                            'title' => 'Edit Post',
                            'icon'  => "icon-pencil",
                        ])->render();
                    }

                    if ($can['add']) {
                        $res .= '<span> ' . View::make('partials.containers.buttons.alert', [
                                'route'   => route('sites.{site}.posts.{post}.ajax-alert', [
                                    $this->site->id,
                                    'post_id' => $data['id'],
                                    'method'  => 'duplicate'
                                ]),
                                'title'   => 'Duplicate Post',
                                'tooltip' => 'Duplicate Post',
                                'icon'    => "fa fa-files-o",
                                'text'    => 'Are You sure you want to duplicate Post?',
                                'type'    => 'warning'
                            ])->render() . ' </span>';
                    }

                    $res .= View::make('partials.containers.buttons.link', [
                        'route' => route('sites.{site}.comments.index', [
                            $this->site->id,
                            "content_type_id" => $data['content_type_id'],
                            "post_id"         => $data['id']
                        ]),
                        'title' => 'Comments',
                        'icon'  => "icon-bubbles",
                    ])->render();
                }


                if ($can['delete']) {
                    $res .= View::make('partials.containers.buttons.alert', [
                        'route' => route($post_route_alert, [
                            $this->site->id,
                            $data['id'],
                            'method'  => 'delete',
                            'trashed' => $trashed,
                            'filters' => json_encode($params['filters'])
                        ]),
                        'title' => 'Delete Post',
                        'icon'  => "icon-trash",
                        'text'  => '',
                        'type'  => 'warning'
                    ])->render();
                }

                return $res;
            },
        ]);

        $params['query'] = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }

    /**
     * Gets posts by filter
     * Used by Dynamic Lists Old
     * require field content_type_id
     *
     * @param \App\Http\Controllers\Sites\Widgets\Request|Request $request
     *
     * @return null
     */
    public function find(Request $request)
    {
        if (!$request->has('content_type_id')) {
            return null;
        }

        $widget_data = $request->has('widget_data_id') ? WidgetData::find($request->get('widget_data_id')) : null;

        $included = (new WidgetPostList())->init($request, $widget_data)->findPosts();

        $post_ids = $included->pluck('id')->toArray();

        $not_included = Post::whereNotIn('id', $post_ids)
            ->where('content_type_id', $request->get('content_type_id'))
            ->get();

        $html = view()->make('widgets-forms.dynamic-list.nestable-post-list')
            ->with('included', $included)
            ->with('not_included', $not_included);

        return response()->json($html->render());
    }

    /**
     * Gets posts by filter
     * Used by Dynamic Lists New
     * require field content_type_id
     *
     * @param \App\Http\Controllers\Sites\Widgets\Request|Request $request
     *
     * @return null
     */
    public function findPostsToDynamicList(Request $request)
    {
        if (!$request->has('content_type_id')) {
            return null;
        }

        $dynamic_list = new DynamicList($request->all());

        $included = $dynamic_list->getBasePosts() ?? [];

        $not_included = $dynamic_list->getDisabledPosts() ?? [];

        $html = view()->make('widgets-forms.dynamic-list.nestable-post-list')
            ->with('included', $included)
            ->with('not_included', $not_included);

        return response()->json($html->render());
    }

    /**
     * @param $params
     *
     * @throws \Exception
     */
    public function select2_list($params)
    {
        $content_type_id = $params['dependency'][0]['value'] ?? null;
        $post_id         = $params['value'] ?? null;
        $query           = Post::select('id', 'name as text');

        // Pull preset value
        if (isset($params['getCurrentValue']) && $post_id) {
            $query->where('id', $post_id);
        } //list the values according to dependency
        elseif ($content_type_id) {
            $query->where('content_type_id', $content_type_id)
                ->orderBy('name');

            // Search
            if (isset($params['query']) && count($params['query']) > 0) {
                $query->where('name', 'LIKE', '%' . $params['query'] . '%');
            }
        } // Check dependencies
        else {
            throw new \Exception('Must get content type');
        }

        return $query->get();
    }

    public function select2_postByPage($params)
    {
        $page_id = $params['dependency'][0]['value'] ?? 0;

        $page = (new PageRepo())->find($page_id);

        if ($page && $page->type == 'archive') {
            return (new PostRepo())->postListByArchivePageToSelect($page->post_id);
        }

        return [];
    }
}