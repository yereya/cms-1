<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\DynamicLists\DynamicList;
use App\Entities\Models\Sites\Popup;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\DynamicList\DynamicListRepo;
use App\Entities\Repositories\Sites\PopupRepo;
use App\Entities\Repositories\Sites\TemplateRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\ChoiceTrait;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Datatable\DatatableLib;
use Illuminate\Http\Request;
use View;

class SitePopupsController extends Controller
{
    use DataTableTrait;
    use AjaxAlertTrait;
    use ChoiceTrait;

    const PERMISSION = 'top.popups';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Popups';

    /**
     * SitePostsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * index
     *
     * @return mixed
     */
    public function index($site_id)
    {
        $can = $this->buildCanPermissions();

        return view('pages.site.popups.index')
            ->with(compact('can', 'site_id'));
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create($site_id)
    {
        $templates = (new TemplateRepo())->getAllInSelectFormat();
        $posts     = Post::all()->pluck('name', 'id');

        return view('pages.site.popups.form')
            ->with(compact('site_id', 'posts', 'templates'));
    }

    /**
     * store
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, $site_id)
    {
        $popup              = new Popup($request->all());
        $popup->cookie_name = md5($popup->name);
        $popup->save();

        return redirect()
            ->route('sites.{site}.popups.edit', [$site_id, $popup->id])
            ->with(self::MESSAGE_TOAST, ["success" => "New Popup created"]);
    }

    /**
     * edit
     *
     * @param Site $site
     * @param Post $post
     *
     * @return mixed
     */
    public function edit($site_id, Popup $popup)
    {
        $templates = (new TemplateRepo())->getAllInSelectFormat();
        $posts     = Post::all()->pluck('name', 'id');

        return view('pages.site.popups.form')
            ->with(compact('site_id', 'popup', 'templates', 'posts'));
    }

    /**
     * update
     *
     * @param Request $request
     * @param Site    $site
     * @param Post    $post
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($site_id, Popup $popup)
    {
        $popup->fill(request()->all());
        $popup->save();

        return redirect()
            ->route('sites.{site}.popups.edit', [$site_id, $popup->id])
            ->with(self::MESSAGE_TOAST, ["success" => "Popup updated"]);
    }

    /**
     * destroy
     *
     * @param Site $site
     * @param Post $post
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($site_id, Popup $popup)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $popup->delete();

        return redirect()->route('sites.{site}.popups.index', [$site_id])
            ->with(self::MESSAGE_TOAST, ["success" => "Popup deleted"]);
    }

    /**
     * Duplicate
     */
    public function duplicate($site_id, Popup $popup)
    {
        $can = $this->buildCanPermissions();
        if (! $can['add']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $duplicate_popup = (new PopupRepo())->duplicate($popup);

        return redirect()
            ->route('sites.{site}.popups.edit', [$site_id, $duplicate_popup->id])
            ->with(self::MESSAGE_TOAST, ["success" => "Duplicate Popup"]);
    }

    /**
     * Ajax Alert Duplicate
     *
     * @param $params
     * @return $this
     */
    private function ajaxAlert_duplicate($params)
    {
        $site = SiteConnectionLib::getSite();
        $popup_id = $params['popup_id'];
        $popup = Popup::find($popup_id);
        $text = "Are You sure you want to duplicate <b><i> {$popup->name} </i></b> popup?";

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.popups.{popup}.duplicate')
            ->with('route_params', [$site->id, $popup->id])
            ->with('action', 'form-confirm');
    }

    /**
     * @return mixed
     */
    public function ajaxAlert_delete()
    {
        $site  = SiteConnectionLib::getSite();
        $popup = Popup::find(request()->get('popup_id'));

        $text = "Are You sure you want to Delete {$popup->name} popup?";

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.popups.destroy')
            ->with('route_params', [$site->id, $popup->id])
            ->with('action', 'delete');
    }


    /**
     * @param              $params
     * @param DatatableLib $datatable
     *
     * @return array
     */
    public function dataTable_list($params, DatatableLib $datatable)
    {
        $site = SiteConnectionLib::getSite();
        $can = $this->buildCanPermissions();

        $datatable->ignoreFilters(['type']);

        $query = (new Popup)->newQuery();

        $params['recordsTotal']  = $query->count();
        $params['searchColumns'] = ['name', 'slug', 'id'];

        $datatable->renderColumns([
            'id'                        => 'id',
            'name'                      => 'name',
            'template_id'               => 'template_id',
            'active'                    => [
                'db_name'  => 'active',
                'callback' => function ($data) {
                    return \View::make('partials.fields.status-label',
                        Popup::ACTIVE_LABELS[$data['active']]
                    )->render();
                }
            ],
            'height'                    => 'height',
            'width'                     => 'width',
            'start_date'                => 'start_date',
            'end_date'                  => 'end_date',
            'appear_event'              => [
                'db_name'  => 'appear_event',
                'callback' => function ($data) {

                    return \View::make('partials.fields.status-label',
                        Popup::APPEAR_EVENT_LABELS[$data['appear_event']]
                    )->render();
                }
            ],
            'param'                     => 'param',
            'cookie_time_on_conversion' => 'cookie_time_on_conversion',
            'cookie_time_on_close'      => 'cookie_time_on_close',
            'actions'                   => function ($data) use ($site, $can) {

                $res = '';

                if ($can['edit']) {
                    $res .= View::make('partials.containers.buttons.link', [
                        'route' => route('sites.{site}.popups.edit', [
                            $site->id,
                            $data['id']
                        ]),
                        'title' => 'Edit Popup',
                        'icon' => "icon-pencil",
                    ])->render();
                }

                if ($can['add']) {
                    $res .= '<span> '. View::make('partials.containers.buttons.alert', [
                            'route' => route('sites.{site}.popups.ajax-alert', [
                                $site->id,
                                'popup_id' => $data['id'],
                                'method'    => 'duplicate'
                            ]),
                            'title' => 'Duplicate Popup',
                            'tooltip' => 'Duplicate Popup',
                            'icon'  => "fa fa-files-o",
                            'text'  => 'Are You sure you want to duplicate Popup?',
                            'type'  => 'warning'
                        ])->render(). ' </span>';
                }

                if ($can['delete']) {
                    $res .= View::make('partials.containers.buttons.alert', [
                        'route' => route('sites.{site}.popups.ajax-alert', [
                            $site->id,
                            'popup_id' => $data['id'],
                            'method' => 'delete'
                        ]),
                        'title' => 'Delete Popup',
                        'icon' => "icon-trash",
                        'text' => '',
                        'type' => 'warning'
                    ])->render();
                }

                return $res;
            },
        ]);

        $params['query'] = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }

    /**
     * @param Request $request
     */
    public function choice_getParamHtml(Request $request)
    {

        $event = $request->get('type');
        $param = $request->get('param');

        $properties = Popup::PARAMS_PROPERTIES[$event] ?? null;

        if (!$properties) {
            return '';
        }

        return View::make('pages.site.popups.param')
            ->with(compact('properties', 'param'));
    }
}

