<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteDomain;
use App\Entities\Models\Sites\SiteMigration;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use App\Entities\Repositories\Sites\SiteRepo;
use App\Entities\Repositories\Sites\SiteSettingDefaultRepo;
use App\Entities\Repositories\SiteSettingRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SitesFormRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\SetupLayout;
use App\Libraries\Media\MediaUploader;
use App\Libraries\TemplateBuilder\ScssLib;
use File;
use Illuminate\Http\Request;
use View;

/**
 * Class SitesController
 *
 * @package App\Http\Controllers\Sites
 */
class SitesController extends Controller
{
    use AjaxAlertTrait;

    const PERMISSION = 'top.manage';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'code-mirror'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Sites Management';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pages.site.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function show(Site $site)
    {
        return view('pages.site.show')->with('site', $site);
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('pages.site.form');
    }

    /**
     * Store
     *
     * @param  SitesFormRequest $request
     *
     * @return \Illuminate\View\View
     */
    public function store(SitesFormRequest $request)
    {
        $site_repo = new SiteRepo();
        $site = $site_repo->saveFromRequest($request);

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The site {$site->name} added"]);
        }

        return redirect()->route('sites.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The site {$site->name} added"]);
    }

    /**
     * Edit
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function edit(Site $site)
    {
        return view('pages.site.form', compact('site'));
    }

    /**
     * Store
     *
     * @param  SitesFormRequest $request
     * @param  Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SitesFormRequest $request, Site $site)
    {
        $site_repo = new SiteRepo();
        $site = $site_repo->saveFromRequest($request, $site);

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The site {$site->name} added"]);
        }

        return redirect()->route('sites.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The site {$site->name} added"]);
    }

    /**
     * Destroy
     *
     * @param Request $request
     * @param Site $site
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Site $site)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $site->delete();
        insertSitesToSession();

        return redirect()->route('sites.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The Site {$site->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return \Illuminate\View\View
     */
    public function ajaxAlert_deleteSite($params, $extra_params)
    {
        $site_id = $extra_params[0]; //get the site id from the passed params

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', 'Please confirm deleting site #'.$site_id)
            ->with('route', 'sites.destroy')
            ->with('action', 'delete')
            ->with('route_params', $site_id);
    }

    /**
     * Migrations
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function migrations(Site $site)
    {
        if (!auth()->user()->can(self::PERMISSION . '.view')) {
            return view('partials.containers.error-modal')->with('error_msg', 'You do not have the permission to view this page.');
        }

        $migrations = SiteMigration::active()->orderBy('order')->get();

        return view('pages.site.migrations-modal')
            ->with(compact('site', 'migrations'));
    }

    /**
     * scssCompile
     *
     * @param Site $site
     *
     * @return array
     */
    public function scssCompile(Site $site)
    {
        try {
            $result = (new ScssLib($site))->compile(env('APP_ENV'));
            if (empty($result)) {
                list($status, $desc) = [1, 'Scss compiled successfully'];
            } else {
                $status = 0;
                $desc = View::make("pages.site.scss.show-scss-compile-error", [
                    'url'   => route('sites.{site}.show-temp-scss', [$site->id]),
                    'error' => $result
                ])->render();
            }
        } catch (\Exception $e) {
            $desc = $e->getMessage();
            $status = 0;
        }

        return json_encode([
            'status' => $status,
            'desc' => $desc
        ]);
    }

    /**
     * Get Temp Scss Content
     *
     * @return mixed
     */
    public function getTempScssContent()
    {
        $site = SiteConnectionLib::getSite();
        $content = (new ScssLib($site))->getTempScssContent();

        return view('pages.site.scss.show-temp-scss')
            ->with(compact('content'));
    }

    public function cssUpload(Site $site) {
        $status = 1;
        $data = null;
        try {
            $domain = SiteDomain::where('site_id', $site->id)->where('domain_dev', (env('APP_ENV') == 'local'))->first();
            $dir = $this->getMediaPath(true);
            $dir_path = $this->getMediaPath();
            $full_paths = File::files($dir);
            $paths = [];

            foreach ($full_paths as $path) {
                $start = strpos($path, $dir_path);
                if ($start !== false) {
                    $paths[] = url(substr($path, $start));
                }
            }

            $uploader = new MediaUploader($domain->domain);
            $results = $uploader->uploadFile($paths, MediaUploader::MEDIA_TYPE_CSS, $site->id);

            $desc = 'Css upload status: ';
            $data = $results[0][1] ?? null;
        } catch (\Exception $e) {
            $status = 0;
            $desc = $e->getMessage();
        }

        return [
            'status' => $status,
            'desc' => $desc . json_encode($data),
            'data' => $data
        ];
    }

    /**
     * Return media path
     *
     * @param bool $full
     *
     * @return string
     */
    private function getMediaPath(bool $full = false) {
        $site = SiteConnectionLib::getSite();
        $path = config('media.directory') . $site->id . DIRECTORY_SEPARATOR . 'css';

        return $full ? public_path($path) : $path;
    }
}
