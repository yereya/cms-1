<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\MemberGroup;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteMemberGroupsFormRequest;
use App\Http\Traits\AjaxModalTrait;
use SiteConnectionLib;

/**
 * Class SiteMemberGroupsController
 *
*@package App\Http\Controllers\Sites
 */
class SiteMemberGroupsController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'top.member-groups';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Member Groups Management';

    /**
     * Display a listing of the resource.
     *
     * @param $site_id

     *
*@return \Illuminate\View\View
     */
    public function index($site_id)
    {
        $site          = SiteConnectionLib::getSite();
        $member_groups = $site->memberGroups()->get();

        return view('pages.site.members.groups.index')
            ->with(compact('site', 'member_groups'))
            ->with('can', $this->buildCanPermissions());
    }

    /**
     * Create
     *
     * @param $site_id

     *
*@return \Illuminate\View\View
     */
    public function create($site_id)
    {
        $site          = SiteConnectionLib::getSite();
        $member_groups = $site->memberGroups()
            ->orderBy('name')
            ->pluck('name', 'id');

        return view('pages.site.members.groups.form')
            ->with(compact('site', 'member_groups'));
    }

    /**
     * Store

     *
*@param  SiteMemberGroupsFormRequest $request
     * @param                              $site_id

     *
*@return \Illuminate\Http\Response
     */
    public function store(SiteMemberGroupsFormRequest $request, $site_id)
    {
        $member_group = new MemberGroup($request->all());
        $member_group->save();

        if ($request->get('create_another')) {

            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["success" => "The member group {$member_group->name} was saved"]);
        }

        return redirect()->route('sites.{site}.member-groups.index', $site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The member group {$member_group->name} was saved"]);
    }

    /**
     * Edit

*
*@param                   $site_id
     * @param MemberGroup $member_group
     *
     *@return \Illuminate\View\View
     */
    public function edit($site_id, MemberGroup $member_group)
    {
        $site          = SiteConnectionLib::getSite();
        $member_groups = $site->memberGroups()->orderBy('name')->pluck('name', 'id');

        return view('pages.site.members.groups.form', compact('site', 'member_group', 'member_groups'));
    }

    /**
     * Update
     *
     *@param SiteMemberGroupsFormRequest  $request
     * @param                             $site_id
     * @param MemberGroup                 $member_group
     *
*@return \Illuminate\Http\Response
     * @internal param $member_group_id
     */
    public function update(SiteMemberGroupsFormRequest $request, $site_id, MemberGroup $member_group)
    {
        $member_group->fill($request->all());
        $member_group->save();

        if ($request->get('create_another')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["success" => "The member group {$member_group->name} was saved"]);
        }

        return redirect()->route('sites.{site}.member-groups.index', $site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The member group {$member_group->name} was saved"]);
    }
}
