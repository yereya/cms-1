<?php

namespace App\Http\Controllers\Sites;


use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\SettingsRepo;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxModalTrait;

/**
 * Class SiteSettingsController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteSettingsController extends Controller
{
    const PERMISSION = 'top.settings';

    use AjaxModalTrait;

    protected $assets = ['form', 'top-app'];

    /**
     * Ajax Modal Show Modal
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_showModal($params, $extra_params)
    {
        $site_id = $extra_params[0];
        $type = $params['type'];

        $settings = SettingsRepo::getSettingsWithExtraParams($site_id, $type);
        return view('pages.site.settings.modal')
            ->with(compact('site_id', 'type', 'settings'));
    }

    /**
     * Edit
     *
     * @param Site $site
     * @param $type
     *
     * @return $this
     */
    public function edit(Site $site, $type)
    {
        $settings = SettingsRepo::getSettingsWithExtraParams($site->id, $type);

        return view('pages.site.settings.edit')
            ->with(compact('site', 'type', 'settings'));
    }

    /**
     * Update Settings
     *
     * @param Site $site
     *
     * @return mixed
     */
    public function update(Site $site)
    {
        $settingsToUpdate = request()->get('settings');
        $type             = request()->get('type');
        SettingsRepo::saveSettings($site->id, $type, $settingsToUpdate);

        return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The setting has been saved"]);
    }
}