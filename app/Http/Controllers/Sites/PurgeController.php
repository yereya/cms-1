<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use App\Libraries\Cache\CacheClearer;

class PurgeController extends Controller
{

    /**
     * Purge the request site.
     *
     * @param Site         $site
     * @param CacheClearer $cache
     *
     * @return string
     */
    public function purgeSite(Site $site, CacheClearer $cache)
    {
        $cache->clearAll($site);
        $result = $cache->clearAll($site);

        if (!$result) {

            return 'Whoops, cache was not purged due to an unexpected server response.';
        }

        return 'Cache was successfully purged, Good job! 🤠';
    }

}