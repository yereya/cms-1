<?php


namespace App\Http\Controllers\Sites;


use App\Entities\Repositories\Sites\PostLikeRepo;
use App\Entities\Repositories\Sites\PostRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostLikeRequest;

class PostLikeController extends Controller
{
    /**
     * @param PostLikeRequest $request
     *
     * @return int
     */
    public function update(PostLikeRequest $request)
    {
        $status          = ['status' => 0];
        $post_likes_repo = new PostLikeRepo();
        $current_post    = (new PostRepo())->find($request->post_id);
        $is_completed    = $post_likes_repo->saveFromRequest($request);

        if ($is_completed) {
            $status = [
                'status' => 1,
                'likes'  => $current_post->likes_sum
            ];
        }

        return $status;
    }
}