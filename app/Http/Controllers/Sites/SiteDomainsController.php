<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\SiteDomain;
use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteDomainsFormRequest;
use App\Http\Traits\AjaxModalTrait;

/**
 * Class SiteDomainsController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteDomainsController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'top.domains';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Domains Management';

    /**
     * Display a listing of the resource.
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function index(Site $site)
    {
        $domains = $site->domains;

        return view('pages.site.domains.index')->with(compact('site', 'domains'));
    }

    /**
     * Create
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function create(Site $site)
    {
        return view('pages.site.domains.form')->with(compact('site'));
    }

    /**
     * Store
     *
     * @param  SiteDomainsFormRequest $request
     * @param  Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteDomainsFormRequest $request, Site $site)
    {
        $domain = new SiteDomain($request->all());
        $domain->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The domain {$domain->domain} added"]);
        }

        return redirect()->route('sites.{site}.domains.index', $domain->site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The domain {$domain->domain} added"]);
    }

    /**
     * Edit
     *
     * @param Site $site
     * @param SiteDomain $domain
     *
     * @return \Illuminate\View\View
     */
    public function edit(Site $site, $domain)
    {
        $domain = SiteDomain::find($domain);
        return view('pages.site.domains.form', compact('site', 'domain'));
    }

    /**
     * Update
     *
     * @param SiteDomainsFormRequest $request
     * @param Site $site
     * @param SiteDomain $domain
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteDomainsFormRequest $request, Site $site, $domain)
    {
        $domain = SiteDomain::find($domain);
        $domain->fill($request->all());
        $domain->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The domain {$domain->domain} saved"]);
        }

        return redirect()->route('sites.{site}.domains.index', $domain->site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The domain {$domain->domain} saved"]);
    }

    /**
     * Destroy
     *
     * @param Site $site
     * @param SiteDomain $domain
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, $domain)
    {
        $domain = SiteDomain::find($domain);
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $domain->delete();

        return redirect()->route('sites.{site}.domains.index', $domain->site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The domain {$domain->domain} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return \Illuminate\View\View
     */
    public function ajaxModal_deleteDomain($params, $extra_params)
    {
        $site_id = $extra_params[0];
        $domain_id = $extra_params[1];

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Domain')
            ->with('btn_title', 'Delete Domain')->with('route', [
                'sites.{site}.domains.destroy',
                $site_id,
                $domain_id
            ]);
    }
}
