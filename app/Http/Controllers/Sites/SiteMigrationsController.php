<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteMigration;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteMigrationsFormRequest;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\ToggleProperty;
use Illuminate\Http\Request;

/**
 * Class SitesController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteMigrationsController extends Controller
{
    use AjaxModalTrait;
    use ToggleProperty;

    const PERMISSION = 'top.migrations';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Migrations Management';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.site.migrations.index')->with('migrations', SiteMigration::all());
    }

    /**
     * Create
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.site.migrations.form');
    }

    /**
     * Store
     *
     * @param  SiteMigrationsFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteMigrationsFormRequest $request)
    {
        $migration = new SiteMigration($request->all());
        $migration->save();

        if ($request->get('create_another')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["success" => "The migration {$migration->name} added"]);
        }

        return redirect()->route('site-migrations.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The migration {$migration->name} added"]);
    }

    /**
     * Edit
     *
     * @param int $migration_id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($migration_id)
    {
        $migration = SiteMigration::findOrFail($migration_id);

        return view('pages.site.migrations.form', compact('migration'));
    }

    /**
     * Update
     *
     * @param SiteMigrationsFormRequest $request
     * @param int                       $migration_id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteMigrationsFormRequest $request, $migration_id)
    {
        $migration = SiteMigration::findOrFail($migration_id);
        $migration->fill($request->all());
        $migration->save();

        if ($request->get('create_another')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["success" => "The migration {$migration->name} saved"]);
        }

        return redirect()->route('site-migrations.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The migration {$migration->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param int $migration_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($migration_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $migration = SiteMigration::findOrFail($migration_id);
        $migration->delete();

        return redirect()->route('site-migrations.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The Migration {$migration->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_deleteSite($params, $extra_params)
    {
        $migration_id = $extra_params[0]; //get the site id from the passed params

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Migration')
                                                      ->with('btn_title', 'Delete migration')
                                                      ->with('route', ['site-migrations.destroy', $migration_id]);
    }

    /**
     * ToogleCheckbox sites
     *
     * @param $request
     * @param $params
     *
     * @return array
     */
    private function toggleCheckbox_sites(Request $request, $params)
    {
        $status       = 1;
        $desc         = '';
        $toggle_state = 0;

        $site = Site::findOrFail($request->get('site_id'));
        $migration = SiteMigration::findOrFail($request->get('migration_id'));

        $connection = SiteConnectionLib::getFacadeRoot();
        $connection->setSite($site);
        $connection->migrate()->run($migration);
        $connection->migrate()->log($migration);

        return [
            'status'      => $status,
            'desc'        => $desc,
            'toggleState' => $toggle_state
        ];
    }
}
