<?php

namespace App\Http\Controllers\Sites\Widgets;

use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Widgets\SiteWidgetTemplate;
use App\Entities\Repositories\RevisionRepo;
use App\Entities\Repositories\Sites\SiteDomainRepo;
use App\Entities\Repositories\Sites\SiteRepo;
use App\Entities\Repositories\Sites\Widgets\SiteWidgetTemplateRepo;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteWidgetTemplatesRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\ChoiceTrait;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\EntityLockTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use App\Libraries\Guzzle;
use App\Libraries\SiteWidgetTemplates\SiteWidgetTemplatesLib;
use App\Libraries\TemplateBuilder\ScssLib;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use SiteConnectionLib;
use Venturecraft\Revisionable\Revision;
use View;

class SiteWidgetTemplatesController extends Controller
{
    use AjaxAlertTrait;
    use DataTableTrait;
    use AjaxModalTrait;
    use Select2Trait;
    use ChoiceTrait;
    use EntityLockTrait;

    const PERMISSION = 'top.widget_templates';
    const MODEL = SiteWidgetTemplate::class;

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app', 'code-mirror', 'entity_lock'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Widget Template';

    /**
     * @var SiteWidgetTemplateRepo
     */
    protected $model_repo;

    /**
     * @var
     */
    protected $site;

    /**
     * SitePostsController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->site       = SiteConnectionLib::getSite();
        $this->model_repo = new SiteWidgetTemplateRepo();
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        $site_id = $this->site->id;

        return view('pages.site.widget-template.index')
            ->with(compact('site_id'));
    }

    /**
     * Create
     *
     * @param $site_id
     *
     * @return View
     * @internal param Site $site
     * @internal param Request $request
     */
    public function create($site_id)
    {
        $widgets             = $this->model_repo->getWidgetsNameAndId();
        $empty_scss_template = $this->getEmptyScssTemplate();

        return view('pages.site.widget-template.form')
            ->with(compact('site_id', 'widgets', 'empty_scss_template'));
    }

    /**
     * Store
     *
     * @param SiteWidgetTemplatesRequest $request
     * @param                            $site_id
     *
     * @return \Illuminate\Http\Response
     * @internal param Site $site
     */
    public function store(SiteWidgetTemplatesRequest $request, $site_id)
    {
        $can = $this->buildCanPermissions();
        if (!$can['add']) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $widget_template = $this->model_repo->prepareSiteWidgetTemplateData($request);
        $is_created      = $this->model_repo->save($widget_template);

        if (!$is_created) {
            return redirect()
                ->route('sites.{site}.widget-templates.edit', [$site_id, $widget_template->id])
                ->with(self::MESSAGE_TOAST, ["warning" => "The data was not saved."]);
        }

        //Change selector class after we have template id
        $template_class        = 'widget_tpl_' . $widget_template->id;
        $widget_template->scss = (new WidgetDataRepo())->replaceTempScssClass($widget_template->scss, $template_class);
        $widget_template->save();

        $this->model_repo->flushBlade($widget_template, $this->site);
        $compilation_state = (new ScssLib($this->site))->compile(env('APP_ENV'));

        if ($compilation_state) {
            return redirect()
                ->route('sites.{site}.widget-templates.edit', [$this->site->id, $widget_template->id])
                ->with(self::MESSAGE_TOAST, ["warning" => "there was problem with scss template : " . $not_compiled]);
        }


        return redirect()
            ->route('sites.{site}.widget-templates.edit', [$site_id, $widget_template->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The data was saved"]);
    }

    /**
     * Get Empty Scss Template
     *
     * @return string
     */
    private function getEmptyScssTemplate()
    {
        return '.' . WidgetData::PLACEHOLDER_CLASS . " {\n  \n}";
    }

    /**
     * Edit
     *
     * @param                    $site_id
     * @param SiteWidgetTemplate $widget_template
     *
     * @return View
     * @internal param $site_id
     */
    public function edit($site_id, SiteWidgetTemplate $widget_template)
    {
        $this->addLock($widget_template->id, $site_id);

        $widgets = $this->model_repo->getWidgetsNameAndId();

        $widget_template = $this->revertToRevision($widget_template);

        $empty_scss_template = $this->getEmptyScssTemplate();

        return view('pages.site.widget-template.form')
            ->with(compact('widget_template', 'site_id', 'widgets', 'empty_scss_template'));
    }

    /**
     * Revert to revision
     *
     * @param $model
     *
     * @return mixed
     */
    public function revertToRevision($model)
    {
        $revision_id = request()->get('revision_id');
        if (!$revision_id) {

            return $model;
        }

        $revision = Revision::find($revision_id);
        if ($revision) {
            $field_name           = $revision->key;
            $model->{$field_name} = $revision->old_value;

            session()->flash('toastr_msgs', ["warnning" => "Changed to revision. Please save changes"]);
        }

        return $model;
    }

    /**
     * Update
     *
     * @param SiteWidgetTemplatesRequest $request
     * @param                            $site_id
     * @param SiteWidgetTemplate         $widget_template
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SiteWidgetTemplatesRequest $request, $site_id, SiteWidgetTemplate $widget_template)
    {
        if (!$this->isMyLock($widget_template->id)) {
            $error_message = "Item is lock by another user.";
        } else {
            $can = $this->buildCanPermissions();
            if (!$can['edit']) {
                $error_message = "You do not have permissions to do this.";
            }
        }
        if (isset($error_message)) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => $error_message]);
        }

        $request         = $request->except(['_method', '_token']);
        $request['scss'] = (new WidgetDataRepo())->replaceTempScssClass($request['scss'], $widget_template->class);
        $is_updated      = $this->model_repo->update($widget_template, $request);

        if (!$is_updated) {
            return redirect()
                ->route('sites.{site}.widget-templates.edit', [$site_id, $widget_template->id])
                ->with(self::MESSAGE_TOAST, ["warning" => "there was an error."]);
        }
        $this->model_repo->flushBlades($this->site);

        return redirect()
            ->route('sites.{site}.widget-templates.edit', [$site_id, $widget_template->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The data was saved"]);
    }

    /**
     * Delete page with all is sub pages
     *
     * @param                    $site_id
     * @param SiteWidgetTemplate $widget_template
     *
     * @return \Illuminate\Http\RedirectResponse
     * @internal param Site $site
     */
    public function destroy($site_id, SiteWidgetTemplate $widget_template)
    {
        $can = $this->buildCanPermissions();
        if (!$can['delete']) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $widget_template->delete();

        return redirect()
            ->route('sites.{site}.widget-templates.index', [$site_id])
            ->with(self::MESSAGE_TOAST, ["success" => "Widget template deleted"]);
    }

    /**
     * Publish All
     *
     * @return \Illuminate\Http\RedirectResponse
     * @internal param $site_id
     */
    public function publishAll()
    {
        $error_params = [
            'type'  => 'warning',
            'title' => 'Something went wrong'
        ];

        if (!auth()->user()->can(self::PERMISSION . '.publish.add')) {

            $error_params['text'] = "You do not have permissions to do this.";

            return json_encode($error_params);
        }

        try {
            $result = $this->model_repo->publishAll($this->site);

            if (isset($result->status) && $result->status == 'ok') {
                $params = [
                    'type'  => 'confirm',
                    'text'  => 'Blade published for site ' . $this->site->name . ' Successfully',
                    'title' => 'Blade Published'
                ];
            } else {
                $error_params['text'] = $result->desc ?? "Unknown problem";
                $params               = $error_params;
            }
        } catch (Exception $e) {
            $params         = $error_params;
            $params['text'] = $e->getMessage();
        }

        return json_encode($params);
    }

    /**
     * Revision
     *
     * @return $this
     */
    public function revisions()
    {
        $request  = request();
        $model_id = $request->get('model_id');

        $site = $this->site;

        $revisions = (new RevisionRepo())->getByModelId(self::MODEL, $model_id);

        return view('pages.site.widget-template.revision')->with(compact('site', 'revisions'));
    }

    /**
     * Choice
     *
     * @return mixed
     */
    public function choice_getRevision()
    {
        $site = $this->site;

        $revision_id = request()->get('revision_id');
        if (!$revision_id) {

            return 'Revision Not Found';
        }

        $revision = Revision::find($revision_id);

        return view('pages.site.widget-template.revision_element')->with(compact('site', 'revision'));
    }

    /**
     * DataTable List Ajax Style
     *
     * @param              $params
     * @param DatatableLib $datatable
     *
     * @return array @param $params
     *
     * @param DatatableLib $datatable
     *
     * @return array
     */
    public function dataTable_list($params, DatatableLib $datatable)
    {
        $model_repo = new SiteWidgetTemplateRepo($this->site);
        $query      = $model_repo->getWithWidgetAndWidgetDataBySite($this->site);

        if (!empty($params['filters'])) {
            if (!empty($params['filters']['widget_data_id'])) {
                $widget_data_id       = $params['filters']['widget_data_id'];
                $widget_templates_ids = $this->model_repo->getWidgetDataByID($widget_data_id)
                    ->pluck('widget_template_id')
                    ->toArray();

                if (!empty($widget_templates_ids)) {
                    $query->whereIn('id', array_values($widget_templates_ids));
                }
            }
        }

        if (!empty($params['filters'])) {
            if (!empty($params['filters']['widget_id'])) {
                $widget_id = $params['filters']['widget_id'];
                $widget    = $this->model_repo->getWidgetById($widget_id)->pluck('id');;

                if (!empty($widget)) {
                    $query->whereIn('widget_id', [$widget_id]);
                }
            }
        }

        $filters_to_columns = [
            'widget_id'      => null,
            'widget_data_id' => null,
        ];

        $datatable->changeFilters($filters_to_columns);
        $can = $this->buildCanPermissions();

        $datatable->renderColumns([
            'id'      => 'id',
            'name'    => 'name',
            'widget'  => [
                'db_name'  => 'widget_id', // Column name
                'callback' => function ($data) {
                    return $data['widget']['name'];
                }
            ],
            'actions' => function ($data) use ($can) {
                $res = '';

                if ($can['edit']) {
                    $res .= View::make('partials.containers.buttons.link', [
                        'route' => route('sites.{site}.widget-templates.edit', [
                            $this->site->id,
                            $data['id']
                        ]),
                        'title' => 'Edit widget template',
                        'icon'  => "icon-pencil",
                    ])->render();
                }

                if ($can['add']) {
                    $res .= '<span> ' . View::make('partials.containers.buttons.alert', [
                            'route'   => route('sites.{site}.widget-templates.ajax-alert', [
                                $this->site->id,
                                'widget_template_id' => $data['id'],
                                'method'             => 'duplicate'
                            ]),
                            'title'   => 'Duplicate Blade',
                            'tooltip' => 'Duplicate Blade',
                            'icon'    => "fa fa-files-o",
                            'text'    => 'Are You sure you want to duplicate Blade?',
                            'type'    => 'warning'
                        ])->render() . ' </span>';
                }

                if ($can['delete']) {
                    $res .= View::make('partials.containers.buttons.alert', [
                        'route' => route('sites.{site}.widget-templates.ajax-alert', [
                            $this->site->id,
                            'widget_id' => $data['id'],
                            'method'    => 'delete'

                        ]),
                        'title' => 'Delete widget template',
                        'icon'  => "icon-trash",
                        'text'  => '',
                        'type'  => 'warning'
                    ])->render();
                }

                return $res;
            }
        ]);

        $params['searchColumns'] = ['id', 'name', 'file_name'];
        $params['recordsTotal']  = $query->count();
        $params['query']         = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }

    /**
     * Update or create new template with custom scss
     *
     * @param Request $request
     *
     * @return array
     */
    public function updateScss(Request $request)
    {
        $result = [
            'status' => 0,
            'desc'   => '',
            'data'   => ''
        ];

        try {
            $site_widget_template = SiteWidgetTemplate::find($request->get('widget_template_id'));
            if (!$site_widget_template) {
                $result['desc'] = 'Widget template id: ' . $request->get('widget_template_id') . ' could not be found';

                return $result;
            }

            /* todo comment this out when htmlValidator task will be reactivated */
//             $compile_error = $this->validateScss($site_widget_template, $request, $result);
//
//            if ($compile_error) {
//                $result['desc'] = $compile_error;
//
//                return $result;
//            }

            $result['status'] = 1;
            $result['desc']   = 'Custom SCSS updated';
        } catch (\Exception $e) {

            $result['desc'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Update or create new template with custom blade
     *
     * @param Request $request
     *
     * @return array
     */
    public function updateBlade(Request $request)
    {
        $result = [
            'status' => 0,
            'desc'   => '',
            'data'   => ''
        ];

        try {
            $site_widget_template = SiteWidgetTemplate::find($request->get('widget_template_id'));
            if (!$site_widget_template) {
                $result['desc'] = 'Widget template id: ' . $request->get('widget_template_id') . ' could not be found';

                return $result;
            }

            $site_widget_template->blade = $request->get('blade');
            $site_widget_template->save();

            $result['status'] = 1;
            $result['desc']   = 'Custom BLADE updated';
        } catch (\Exception $e) {
            $result['desc'] = $e->getMessage();
        }

        return $result;
    }

    /**
     * Duplicate
     * Duplicate one row
     *
     * @param                    $site_id
     * @param SiteWidgetTemplate $widget_template
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate($site_id, SiteWidgetTemplate $widget_template)
    {
        $can = $this->buildCanPermissions();
        if (!$can['add']) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $duplicate_widget_template = (new SiteWidgetTemplateRepo())->duplicate($widget_template);

        return redirect()
            ->route('sites.{site}.widget-templates.edit', [$this->site->id, $duplicate_widget_template->id])
            ->with(self::MESSAGE_TOAST, ["success" => "Template Duplicated."]);
    }

    /**
     * Ajax Alert Publish
     *
     * @return mixed
     * @internal param $params
     */
    private function ajaxAlert_publish()
    {
        $text                  = "Are You sure you want to Publish all widget template?";
        $url                   = route('sites.{site}.widget-templates.publish', [$this->site->id]);
        $js_on_confirm_command = "App.runAjaxRoute('$url')";

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('action', 'confirm')
            ->with('on_confirm', $js_on_confirm_command);
    }

    /**
     * Ajax Alert Flash Local
     *
     * @return mixed
     */
    private function ajaxAlert_flushLocal()
    {
        $this->model_repo->flushBlades($this->site);

        $text = "Local Blades Created Successfully";

        return view('partials.containers.alerts.ajax-alert')->with('text', $text)->with('action', 'info');
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    private function ajaxAlert_delete($params)
    {
        $widget_template = $this->model_repo->find($params['widget_id']);
        $text            = "Are You sure you want to Delete {$widget_template->name} widget template?";

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.widget-templates.destroy')
            ->with('route_params', [$this->site->id, $widget_template->id])
            ->with('action', 'delete');
    }

    /**
     * Update or create new template with custom scss
     *
     * @param Request $request
     *
     * @return array
     */

    /**
     * Get edit custom scss
     *
     * @return mixed
     */
    private function ajaxModal_scss()
    {
        $site                 = SiteConnectionLib::getSite();
        $widget_data          = WidgetData::where('widget_id', request()->route()->getParameter('widget'))->first();
        $site_widget_template = SiteWidgetTemplate::find(request()->get('widget_template_id'));

        if (!$widget_data || !$site_widget_template) {
            return [
                'status' => 0,
                'desc'   => 'Widget template not found, Please check it',
                'data'   => ''
            ];
        }

        return view('widgets-forms.template.edit-scss-modal')->with(compact('site', 'widget_data',
            'site_widget_template'));
    }

    /**
     * Get edit custom blade
     *
     * @return mixed
     */
    private function ajaxModal_blade()
    {
        $site                 = SiteConnectionLib::getSite();
        $widget_data          = WidgetData::where('widget_id', request()->route()->getParameter('widget'))->first();
        $site_widget_template = SiteWidgetTemplate::find(request()->get('widget_template_id'));

        if (!$widget_data || !$site_widget_template) {
            return [
                'status' => 0,
                'desc'   => 'Widget template not found, Please check it',
                'data'   => ''
            ];
        }

        return view('widgets-forms.template.edit-blade-modal')->with(compact('site', 'widget_data',
            'site_widget_template'));
    }

    /**
     * Ajax Alert Duplicate
     *
     * @param $params
     *
     * @return mixed
     */
    private function ajaxAlert_duplicate($params)
    {
        $widget_template_id = $params['widget_template_id'];
        $widget_template    = SiteWidgetTemplate::find($widget_template_id);
        $text               = "Are You sure you want to duplicate <b><i> {$widget_template->name} </i></b> template?";

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.widget-templates.{widget_template}.duplicate')
            ->with('route_params', [$this->site->id, $widget_template->id])
            ->with('action', 'form-confirm');
    }

    /**
     * Validate Scss
     *
     * @param $site_widget_template
     * @param $request
     *
     * @return string
     */
    public function validateScss(SiteWidgetTemplate $site_widget_template, Request $request)
    {
        /*backup the old scss*/
        $old_scss = $site_widget_template->scss;

        $site_widget_template->scss = $request->get('scss');
        $is_updated                 = $site_widget_template->save();

        if (!$is_updated) {

            return;
        }

        $compile_error = (new ScssLib($this->site))->compile('dev', 'main_temp.css');

        if (!$compile_error) {

            return;
        }

        $site_widget_template->scss = $old_scss;
        $site_widget_template->save();

        return $compile_error;
    }
}