<?php namespace App\Http\Controllers\Sites\Widgets;

use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use App\Entities\Repositories\Sites\Widgets\WidgetRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteWidgetsFormRequest;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\Select2Trait;

/**
 * Class SiteWidgetsController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteWidgetsController extends Controller
{
    use AjaxModalTrait;
    use Select2Trait;

    const PERMISSION = 'top.widgets';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Widgets Management';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pages.site.widgets.index')->with('widgets', SiteWidget::all());
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('pages.site.widgets.form');
    }

    /**
     * Store
     *
     * @param  SiteWidgetsFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteWidgetsFormRequest $request)
    {
        $widget = new SiteWidget($request->all());
        $widget->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The widget {$widget->name} added"]);
        }

        $site = SiteConnectionLib::getSite();

        return redirect()->route('sites.{site}.site-widgets.index', [$site->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The widget {$widget->name} added"]);
    }

    /**
     * Edit
     *
     * @param SiteWidget $widget
     *
     * @return \Illuminate\View\View
     */
    public function edit(SiteWidget $widget)
    {
        return view('pages.site.widgets.form', compact('widget'));
    }

    /**
     * Update
     *
     * @param SiteWidgetsFormRequest $request
     * @param SiteWidget             $widget
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteWidgetsFormRequest $request, SiteWidget $widget)
    {
        $widget->fill($request->all());
        $widget->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The widget {$widget->name} saved"]);
        }


        $site = SiteConnectionLib::getSite();

        return redirect()->route('sites.{site}.site-widgets.index', [$site->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The widget {$widget->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param SiteWidget $widget
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(SiteWidget $widget)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $widget->delete();


        $site = SiteConnectionLib::getSite();

        return redirect()->route('sites.{site}.site-widgets.index', [$site->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The widget {$widget->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_deleteWidget($params, $extra_params)
    {
        $widget_id = $extra_params[0]; //get the site id from the passed params

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Widget')
            ->with('btn_title', 'Delete widget')
            ->with('route', ['site-widgets.destroy', $widget_id]);
    }

    /**
     * Widgets List
     *
     * @param $params
     *
     * @return mixed
     */
    public function select2_list($params)
    {
        $widget_type_id = $params['dependency'][0]['value'] ?? null;
        $query          = SiteWidget::select('id', 'name as text');


        // Pull preset value
        if (isset($params['getCurrentValue'])) {
            return $query->where('id', $params['getCurrentValue'])->get();
        }

        if (isset($params['query']) && count($params['query']) > 0) {
            $query->where('name', 'LIKE', '%' . $params['query'] . '%');
        }

        if ($widget_type_id) {
            return $query
                ->where('widget_id', $widget_type_id)
                ->get();
        }

        return $query->get();
    }
}
