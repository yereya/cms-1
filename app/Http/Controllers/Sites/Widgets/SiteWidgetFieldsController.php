<?php namespace App\Http\Controllers\Sites\Widgets;

use App\Entities\Models\Sites\Widgets\Field;
use App\Entities\Models\Sites\Widgets\FieldType;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteWidgetFieldsFormRequest;
use App\Http\Traits\AjaxModalTrait;

/**
 * Class SiteWidgetFieldsController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteWidgetFieldsController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'top.widgets';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Widget Fields Management';

    /**
     * Display a listing of the resource.

*
*@param SiteWidget $widget
     *
* @return \Illuminate\View\View
     */
    public function index(SiteWidget $widget)
    {
        return view('pages.site.widgets.fields.index')->with(['widget' => $widget, 'fields' => $widget->fields]);
    }

    /**
     * Create

*
*@param SiteWidget $widget
     *
*@return \Illuminate\View\View
     */
    public function create(SiteWidget $widget)
    {
        $types = FieldType::pluck('name', 'id');

        return view('pages.site.widgets.fields.form', compact('widget', 'types'));
    }

    /**
     * Store
     *
     * @param  SiteWidgetFieldsFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteWidgetFieldsFormRequest $request)
    {
        $field = new Field($request->all());
        $field->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The field {$field->title} added"]);
        }

        return redirect()->route('site-widgets.{widget_id}.fields.index', $field->widget_id)
                         ->with(self::MESSAGE_TOAST, ["success" => "The widget {$field->title} added"]);
    }

    /**
     * Edit

*
*@param SiteWidget  $widget
     * @param Field $field
     *
*@return \Illuminate\View\View
     */
    public function edit(SiteWidget $widget, Field $field)
    {
        $types = FieldType::pluck('name', 'id');

        return view('pages.site.widgets.fields.form', compact('widget', 'field', 'types'));
    }

    /**
     * Update

*
*@param SiteWidgetFieldsFormRequest $request
     * @param SiteWidget            $widget
     * @param Field                 $field
     *
*@return \Illuminate\Http\Response
     */
    public function update(SiteWidgetFieldsFormRequest $request, SiteWidget $widget, Field $field)
    {
        $field->fill($request->all());
        $field->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The field {$field->title} saved"]);
        }

        return redirect()->route('site-widgets.{widget_id}.fields.index', $field->widget_id)
                         ->with(self::MESSAGE_TOAST, ["success" => "The field {$field->title} saved"]);
    }

    /**
     * Destroy

*
*@param SiteWidget  $widget
     * @param Field $field




*
*@return \Illuminate\Http\RedirectResponse
     */
    public function destroy(SiteWidget $widget, Field $field)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $field->delete();

        return redirect()->route('site-widgets.{widget_id}.fields.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The field {$field->title} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_deleteField($params, $extra_params)
    {
        $widget_id = $extra_params[0];
        $field_id  = $extra_params[1];

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Field')->with('btn_title', 'Delete Field')
                                                      ->with('route', [
                                                          'site-widgets.{widget}.fields.destroy',
                                                          $widget_id,
                                                          $field_id
                                                      ]);
    }
}
