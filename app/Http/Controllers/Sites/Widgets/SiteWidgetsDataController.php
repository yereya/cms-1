<?php namespace App\Http\Controllers\Sites\Widgets;

use App\Entities\Models\Sites\ABTest;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Templates\Column;
use App\Entities\Models\Sites\Templates\Template;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Models\Sites\Templates\Item;
use App\Entities\Models\Sites\Widgets\SiteWidget;
use App\Entities\Repositories\Sites\DynamicList\DynamicListRepo;
use App\Entities\Repositories\Sites\TemplateRepo;
use App\Entities\Repositories\Sites\Widgets\WidgetDataRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use App\Libraries\TemplateBuilder\ScssLib;
use App\Libraries\Widgets\WidgetBuilder;
use App\Libraries\Widgets\WidgetForm;
use App\Libraries\Widgets\WidgetFormBuilder;
use App\Libraries\Widgets\WidgetPostList;
use DB;
use Illuminate\Http\Request;

/**
 * Class SiteWidgetsDataController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteWidgetsDataController extends Controller
{
    use AjaxModalTrait;
    use DataTableTrait;
    use Select2Trait;
    const PERMISSION = 'top.templates';

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'datatables', 'top-app', 'nestable'];

    /**
     * SiteWidgetsDataController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        if (app()->bound('debugbar')) {
            \Debugbar::disable();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pages.site.widgets.site-widgets.index')
            ->with('site', SiteConnectionLib::getSite())
            ->with('widget_type', SiteWidget::all()
                ->pluck('name', 'id'))
            ->with('templates', Template::all()
                ->pluck('name', 'id'));
    }

    /**
     * Show
     *
     * @param Request    $request
     * @param Site       $site
     * @param SiteWidget $widget
     * @param WidgetData $widget_data
     *
     * @return array
     * @throws \Exception
     * @internal param Template $template
     */
    public function show(Request $request, Site $site, SiteWidget $widget, WidgetData $widget_data)
    {
        \SiteConnectionLib::setSite($site)
            ->addConnection();

        $builder = new WidgetBuilder($widget_data, $widget);

        return $builder->get();
    }


    /**
     * @return Controller|\Illuminate\View\View
     */
    public function create()
    {
        $can = $this->buildCanPermissions();

        if (!$can['add']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }
        return $this->generateWidgetView();
    }

    /**
     * Generate Widget view
     * 1. prepare all dependencies.
     * 2. create prepared view (with all dependencies).
     * 3. return response to the user.
     *
     * @param bool $is_duplicate
     *
     * @internal array  $dependencies
     * @internal WidgetFormBuilder $widget_view
     */
    public function generateWidgetView($is_duplicate = false)
    {
        $dependencies = $this->prepareDependencies($is_duplicate);

        if (!$dependencies) {
            redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "view didn\"t created please check with your admin"]);
        }

        $widget_view = $this->createView($dependencies);

        return $widget_view->view();
    }

    /**
     * Prepare Dependencies for widgetView.
     *
     * @param bool $is_duplicate
     *
     * @return array
     */
    public function prepareDependencies($is_duplicate = false)
    {
        $uri_params     = request()->route()->parameters();
        $request_params = request()->all();
        $params_mix     = array_merge($uri_params, $request_params);

        if (!empty($params_mix['data_id'])) {
            $params_mix['widget_data'] = $params_mix['data_id'];
        }

        $prepared_data = [
            'site'        => getSiteFromRouteParameter(),
            'template'    => isset($params_mix['template']) ? getTemplateFromParameter($params_mix['template']) : new Template(),
            'widget'      => SiteWidget::with('templates')
                ->find($params_mix['widget']?? $params_mix['widget_id']),
            'column'      => Column::find($params_mix['column_id'] ?? null),
            'widget_data' => WidgetData::find($params_mix['widget_data'] ?? null),
            'ab_tests'    => new ABTest
        ];

        if ($is_duplicate) {
            $prepared_data['widget_data']->id = null;
        }
        return $prepared_data;
    }

    /**
     * Create Widget View by calling
     * WidgetFormBuilder Class With all its dependencies.
     *
     * @param $filtered_data
     *
     * @return $this
     */
    public function createView($filtered_data)
    {
        return (new WidgetFormBuilder(
            $filtered_data['site'],
            $filtered_data['template'],
            $filtered_data['widget'],
            $filtered_data['column'],
            $filtered_data['widget_data'],
            $filtered_data['ab_tests']
        ));
    }

    /**
     * Store data.
     *
     * @param Request    $request
     * @param Site       $site
     * @param SiteWidget $widget
     *
     * @return WidgetData
     */
    public function store(Request $request, Site $site, SiteWidget $widget)
    {
        $widget_data = $this->commonStoreRoutine($request, $site, $widget);

        return $widget_data;
    }

    /**
     * @param Request    $request
     * @param Site       $site
     * @param SiteWidget $widget
     *
     * @return WidgetData|string
     */
    private function commonStoreRoutine(Request $request, Site $site, SiteWidget $widget)
    {
        // Changes the request url_filename to url for media
        if ($request->has('url')) {
            $request->merge(['image_id' => $request->get('url')]);
        }

        DB::beginTransaction();

        try {
            $widget_data      = new WidgetData;
            $widget_data_repo = new WidgetDataRepo();
            $widget_data->fill($request->all());
            $widget_data->data = WidgetForm::getFormSavedFields($widget, $request);
            $widget_data->save();

            $widget_data_repo->storeWidgetTab($widget_data, $request->all());
            $widget_data->scss = $widget_data_repo->replaceTempScssClass($widget_data->scss, $widget_data->class);
            $widget_data->save();

            if ($request->has('post_ids') || $request->has('post_ids_nestable') || $request->has('ribbon')) {
                $post_order = new WidgetPostList();
                $post_order->init($request, $widget_data)
                    ->save();
            }
        } catch (\Exception $e) {
            DB::rollBack();

            return json_encode($e->getMessage());
        }

        DB::commit();

        $widget_data->append('html');

        return $widget_data;
    }

    /**
     * StorePortlet
     * stores widget_data in portlet view (not modal)
     *
     * @param Request    $request
     * @param Site       $site
     * @param SiteWidget $widget
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storePortlet(Request $request, Site $site, SiteWidget $widget)
    {
        $widget_data = $this->commonStoreRoutine($request, $site, $widget);

        return redirect()->route('sites.{site}.widgets.index', [SiteConnectionLib::getSite()]);
    }

    /**
     * update
     *
     * @param Request    $request
     * @param Site       $site
     * @param SiteWidget $widget
     * @param WidgetData $widget_data
     *
     * @return WidgetData
     */
    public function update(Request $request, Site $site, SiteWidget $widget, WidgetData $widget_data)
    {
        $widget_data = $this->commonUpdateRoutine($request, $site, $widget, $widget_data);

        $compile_error = (new ScssLib($site))->compile(env('APP_ENV'));

        if($compile_error){

            return $compile_error;
        }

        return $widget_data;
    }

    /**
     * @param Request    $request
     * @param Site       $site
     * @param SiteWidget $widget
     * @param WidgetData $widget_data
     *
     * @return WidgetData
     */
    private function commonUpdateRoutine(Request $request, Site $site, SiteWidget $widget, WidgetData $widget_data)
    {
        if ($request->has('url')) {
            $request->merge(['image_id' => $request->get('url')]);
        }

        DB::beginTransaction();
        try {
            // Update the widget data
            $widget_data_repo = new WidgetDataRepo();
            $widget_data_repo->updateFromRequest($widget_data, $request);

            // Store only this widget type Tabbing|Group
            $widget_data_repo->storeWidgetTab($widget_data, $request->all());

            $widget_data->data = WidgetForm::getFormSavedFields($widget, $request);
            $widget_data->scss = $widget_data_repo->replaceTempScssClass($widget_data->scss, $widget_data->class);
            $widget_data->save();

            // Save to widget_data_post or to post_ids logic
            $post_order = new WidgetPostList();
            if ($request->has('post_ids') || $request->has('post_ids_nestable') || $request->has('ribbon')) {
                $post_order->init($request, $widget_data)
                    ->delete()
                    ->save();
            } else {
                $post_order->init($request, $widget_data)
                    ->delete();
            }

            $widget_data->abTests()
                ->sync((array)$request->get('ab_tests'));
        } catch (\Exception $e) {
            DB::rollBack();

            return json_encode($e->getMessage());
        }

        // Appends the html of the widget data to the collection
        $widget_data->append('html');

        DB::commit();

        if ($request->has('item_id')) {
            $item = Item::find($request->input('item_id'));
            if ($item) {
                $item->fill($request->input('item'));
                $item->save();
                $widget_data->widget_data_items = $item;
            }
        }

        return $widget_data;
    }

    /**
     * Update Portlet
     * updates widget_data in portlet view (not modal)
     *
     * @param Request    $request
     * @param Site       $site
     * @param SiteWidget $widget
     * @param WidgetData $widget_data
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePortlet(Request $request, Site $site, SiteWidget $widget, WidgetData $widget_data)
    {
        $widget_data = $this->commonUpdateRoutine($request, $site, $widget, $widget_data);

        return redirect()->route('sites.{site}.widgets.index', [SiteConnectionLib::getSite()]);
    }

    /**
     * Destroy
     *
     * @param Site       $site
     * @param SiteWidget $widget
     * @param            $widgetData_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, SiteWidget $widget, $widgetData_id)
    {
        // TODO QA this method
        if (!auth()
            ->user()
            ->can(self::PERMISSION . '.delete')
        ) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $widgetData = WidgetData::find($widgetData_id);
        $result     = $this->deleteWidgetData($widgetData);

        if (!$result['success']) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => $result['desc']]);
        }

        return redirect()
            ->route('sites.{site}.widgets.index', [SiteConnectionLib::getSite()])
            ->with(self::MESSAGE_TOAST, ["success" => $result['desc']]);
    }

    /**
     * Delete Widget Data
     * deletes widget data only if this widget data doesn't connected to any template
     *
     * @param WidgetData $widgetData
     *
     * @return array
     */
    private function deleteWidgetData(WidgetData $widgetData)
    {
        $appears_in_templates = Item::where('element_id', $widgetData->id)
            ->where('element_type', 'widget')
            ->get();

        if (!$appears_in_templates->isEmpty()) {
            return [
                'success' => 0,
                'desc'    => "The Widget Data {$widgetData->name} is in use in templates"
            ];
        }

        $widgetData->delete();

        return [
            'success' => 1,
            'desc'    => "The Widget Data {$widgetData->name} was deleted"
        ];
    }

    /**
     * Get widget names by widget type
     *
     * @param $params
     *
     * @return mixed
     */
    public function select2_list($params)
    {

        $widget_type_id = $params['dependency'][0]['value'] ?? null;
        $query          = WidgetData::select('id', 'name as text');

        // Pull preset value
        if (isset($params['getCurrentValue'])) {
            return $query->where('id', $params['getCurrentValue'])
                ->get();
        }

        if (isset($params['query']) && count($params['query']) > 0) {
            $query->where('name', 'LIKE', '%' . $params['query'] . '%');
        }

        if ($widget_type_id) {
            return $query->where('widget_id', $widget_type_id)
                ->get();
        }

        return $query->get();
    }

    /**
     * @param $params
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function select2_byTypeList($params)
    {
        $widget_repo = new WidgetDataRepo();

        $where[] = ['widget_id', '=', $params['widget_id']];

        if (!empty($params['getCurrentValue'])) {
            $where[] = ['id', '=', $params['value']];
            return $widget_repo->getWidgetByTypeToSelect($where);
        }

        if (isset($params['query']) && count($params['query']) > 0) {
            $where[] = ['name', 'LIKE', '%' . $params['query'] . '%'];
        }

        return $widget_repo->getWidgetByTypeToSelect($where);
    }

    /**
     * Select to dynamic list new
     *
     * @param $params
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function select2_dynamicList($params)
    {
        $widget_repo = new DynamicListRepo();
        $where       = [];
        if (!empty($params['getCurrentValue'])) {
            $where[] = ['id', '=', $params['value']];
            return $widget_repo->getDynamicList($where);
        }

        if (isset($params['query']) && count($params['query']) > 0) {
            $where[] = ['name', 'LIKE', '%' . $params['query'] . '%'];
        }

        return $widget_repo->getDynamicList($where);
    }


    /**
     * @return Controller|\Illuminate\View\View
     */
    public function edit()
    {
        $can = $this->buildCanPermissions();
        if (!$can['edit']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }
        return $this->generateWidgetView();
    }

    /**
     * Datatable
     *
     * @param              $params
     * @param DatatableLib $datatable
     *
     * @return array
     */
    private function dataTable_list($params, DatatableLib $datatable)
    {
        $query = WidgetData::whereNotNull('active');

        if (isset($params['filters']) && $params['filters']) {
            if (isset($params['filters']['templates'])) {
                $template_id = $params['filters']['templates'];
                $query->whereHas('templateRowColumnItem', function ($query) use ($template_id) {
                    $query->where('template_row_column_items.template_id', $template_id);
                });
            }

            if (isset($params['filters']['widget_type'])) {
                $widget_type = $params['filters']['widget_type'];
                $query->where('widget_id', $widget_type);
            }
        }

        $params['recordsTotal'] = $query->count();

        $query->with('widget');

        $params['searchColumns'] = ['name'];
        $filters_to_columns      = [
            'widget_type' => 'widget_id',
            'templates'   => null
        ];

        $datatable->changeFilters($filters_to_columns);

        $site_id = SiteConnectionLib::getSite();

        $datatable->addColumn('id');
        $datatable->renderColumns([
            'name'        => [
                'db_name'  => 'name',
                'callback' => function ($obj) {
                    return $obj['name'];
                }
            ],
            'widget_type' => [
                'db_name'  => 'widget_id',
                'callback' => function ($obj) {
                    return $obj['widget']['name'];
                }
            ],
            'created_at'  => [
                'db_name'  => 'created_at',
                'callback' => function ($obj) {
                    return $obj['created_at'];
                }
            ],
            'updated_at'  => [
                'db_name'  => 'updated_at',
                'callback' => function ($obj) {
                    return $obj['updated_at'];
                }
            ],

            'actions' => function ($data) use ($site_id) {
                $res = '';

                $res .= \View::make('partials.fields.action-icon', [
                    'url'     => route('sites.{site}.widgets.{widget}.widget-data.{widget_data}.edit', [
                        $site_id,
                        'widget'      => $data['widget_id'],
                        'widget_data' => $data['_id']
                    ]),
                    'icon'    => 'icon-pencil',
                    'tooltip' => 'Edit Widget'
                ])
                    ->render();

                $res .= \View::make('partials.fields.action-icon', [
                    'url'        => route('sites.{site}.widgets.{widget}.widget-data.ajax-modal', [
                        $site_id,
                        'widget'      => $data['widget_id'],
                        'widget_data' => $data['_id'],
                        'method'      => 'widgetDataDelete',
                    ]),
                    'icon'       => 'fa fa-trash',
                    'tooltip'    => 'Delete Widget',
                    'attributes' => [
                        'data-toggle' => "modal",
                        'data-target' => '#ajax-modal'
                    ],
                ])
                    ->render();

                return $res;
            },
        ]);

        $params['query'] = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }

    /**
     * Ajax Modal - Widget Data Delete Confirmation  CRUD
     *
     * @param array $params
     *
     * @return \Illuminate\View\View
     * todo - change the name of the widget and union the two ajaxModal deletes (crud and template builder)
     */
    private function ajaxModal_widgetDataDelete($params)
    {
        $uri_params = request()
            ->route()
            ->parameters();

        return view('pages.delete-confirmation-modal')
            ->with('title', 'Delete Widget Data')
            ->with('btn_title', 'Delete Widget Data')
            ->with('route', [
                'sites.{site}.widgets.{widget}.widget-data.destroy',
                $uri_params['site'],
                $uri_params['widget'],
                $uri_params['widget_data']
            ]);
    }

    /**
     * Ajax Modal - Widget Delete Confirmation
     * in template builder
     *
     * @param array $params
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_delete($params)
    {
        $column_id = $params['column_id'];
        $id        = $params['id'];

        return view('pages.site.templates.modals.widget-delete')
            ->with(compact('column_id', 'id'));
    }


    /**
     * Step 1
     * Ajax Modal - Widget Selector.
     *
     * @param $params
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function ajaxModal_select($params)
    {
        $uri_params = request()
            ->route()
            ->parameters();

        $param_mix = array_merge($uri_params, $params);

        $widgets   = SiteWidget::all();
        $site      = SiteConnectionLib::getSite();
        $column_id = $param_mix['column_id'] ?? null;
        $template  = !empty($param_mix['template'])
            ? (new  TemplateRepo())->find($param_mix['template'])
            : null;

        return view('pages.site.widgets.site-widgets.modals.widgets-list')
            ->with(compact('widgets', 'site', 'column_id', 'template'));
    }


    /**
     * Step 2
     * Ajax Modal - Choice from list of  widget-data or create new ones.
     *
     * @param $params
     *
     * @return $this
     */
    private function ajaxModal_type($params)
    {
        $dependencies = $this->getViewDependencies($params);

        return view('pages.site.templates.modals.widget')
            ->with($dependencies);
    }

    /**
     * @return array
     */
    private function getViewDependencies($params)
    {
        $uri_params = request()
            ->route()
            ->parameters();

        $column_id = $params['column_id'];
        $site      = Site::findOrFail($uri_params['site']);
        $template  = Template::findOrFail($params['template']);
        $widgets   = SiteWidget::with('widgetData')
            ->where('id', $params['widget_id'])
            ->get();

        return compact('column_id', 'site', 'template', 'widgets');
    }

    /**
     * Step 3
     * Ajax Modal - Widget Data
     * This widget is accessed when editing or creating a widget
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_createOrEdit($params)
    {
        return $this->generateWidgetView(false);
    }

    /**
     * Ajax Modal - Widget Duplicate
     * This widget is accessed when editing or creating a widget
     *
     * @return \Illuminate\View\View
     */
    private function ajaxModal_duplicate()
    {
        return $this->generateWidgetView(true);
    }
}