<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\SiteDevice;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteDevicesFormRequest;
use App\Http\Traits\AjaxModalTrait;

/**
 * Class SiteDevicesController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteDevicesController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'top.devices';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Devices Management';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('pages.site.devices.index')->with('devices', SiteDevice::all());
    }

    /**
     * Create
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $groups = ['desktop','tablet','mobile'];

        return view('pages.site.devices.form')->with(compact('groups'));
    }

    /**
     * Store
     *
     * @param  SiteDevicesFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteDevicesFormRequest $request)
    {
        $device = new SiteDevice($request->all());
        $device->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The device {$device->name} added"]);
        }

        return redirect()->route('site-devices.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The device {$device->name} added"]);
    }

    /**
     * Edit
     *
     * @param int $device_id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($device_id)
    {
        $device = SiteDevice::findOrFail($device_id);
        $groups = ['desktop','tablet','mobile'];

        return view('pages.site.devices.form', compact('device', 'groups'));
    }

    /**
     * Update
     *
     * @param SiteDevicesFormRequest $request
     * @param int                    $device_id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteDevicesFormRequest $request, $device_id)
    {
        $device = SiteDevice::findOrFail($device_id);
        $device->fill($request->all());
        $device->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The device {$device->name} saved"]);
        }

        return redirect()->route('site-devices.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The device {$device->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param int $device_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($device_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $device = SiteDevice::findOrFail($device_id);
        $device->delete();

        return redirect()->route('site-devices.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The device {$device->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_deleteDevice($params, $extra_params)
    {
        $device_id = $extra_params[0]; //get the site id from the passed params

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Device')
                                                      ->with('btn_title', 'Delete device')
                                                      ->with('route', ['site-devices.destroy', $device_id]);
    }
}
