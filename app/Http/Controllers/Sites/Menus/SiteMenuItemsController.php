<?php

namespace App\Http\Controllers\Sites\Menus;

use App\Entities\Models\Sites\MenuItem;
use App\Entities\Repositories\Sites\PageRepo;
use App\Http\Controllers\Controller;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Menu;
use App\Libraries\PageInfo\PagesUrls;

class SiteMenuItemsController extends Controller
{

    const PERMISSION = 'top.menus';

    protected $assets = ['form', 'nestable', 'menu-items'];

    // helper array used by store action to map between temporary client ids to new inserted db_id
    private $client_id_to_db_id_map = [];

    /**
     * @param Site $site
     * @param Menu $menu
     *
     * @return mixed
     */
    public function index(Site $site, Menu $menu)
    {
        $menus         = Menu::all()->pluck('name', 'id');
        $selected_menu = $menu;
        $empty_list    = [new MenuItem()];
        $menu_items    = MenuItem::getTree($menu->id);
        $pages         = (new PageRepo())->getRouteListFromApi();

        return view('pages.site.menu-items.index')
            ->with(compact('site', 'selected_menu', 'menus', 'menu_items', 'pages', 'empty_list'));
    }

    /**
     * Save array of menu items do db work with the follwing cases
     * 1. Menu item was previously existed - update fields
     * 2. Menu item was previously existed and mark as delete - delete
     * 3. Menu item is a new item - insert to db save a map between client_id to new db_id
     * 4. Menu item is a new item and mark as deleted - do nothing
     *
     * @param Site $site
     * @param Menu $menu
     *
     * @return array
     */
    public function store(Site $site, Menu $menu)
    {
        $user = auth()->user();
        if (!$user->can(self::PERMISSION . '.edit') || !$user->can(self::PERMISSION . '.add')) {
            return [
                'status' => 0,
                'desc'   => "You do not have permissions to do that",
            ];
        }

        try {
            $client_items_by_id = request()->get('links_by_id');
            $menu_items         = $menu->items()->get()->keyBy('id');
            foreach ($client_items_by_id as $link) {
                $client_id = $link['id'];
                if (isset($menu_items[$client_id])) {
                    if (isset($link['delete'])) {
                        $menu_items[$client_id]->delete();
                    } else {
                        $this->updateItem($menu_items[$client_id], $link);
                    }
                } else {
                    //ignore new items which was deleted
                    if (!isset($link['delete'])) {
                        $item                                     = $this->storeItem($menu->id, $link);
                        $this->client_id_to_db_id_map[$client_id] = $item->id;
                    }
                }
            }
            return ['status' => 1];
        } catch (Exception $e) {
            return [
                'status' => 0,
                'desc'   => $e->getMessage(),
            ];
        }
    }


    /**
     * Update menu item
     *
     * @param $item
     * @param $link_from_client
     */
    private function updateItem($item, $link_from_client)
    {
        $item->fill($link_from_client);
        $item->parent_id = $this->getDbParentId($link_from_client);
        $item->save();
    }

    /**
     * Create new Menu items
     *
     * @param $menu_id
     * @param $new_link
     *
     * @return MenuItem
     */
    private function storeItem($menu_id, $new_link)
    {
        $item            = new MenuItem($new_link);
        $item->post_id   = empty($new_link['post_id']) ? null : $new_link['post_id'];
        $item->menu_id   = $menu_id;
        $item->parent_id = $this->getDbParentId($new_link);
        $item->save();

        return $item;
    }

    /**
     * When parent_id is client temporary id i.e above 10000 return real inserted db id.
     * When parent_id is empty string i.e root item, return null cause root item must have null as parent_id
     *
     * @param array $link
     *
     * @return mixed|null
     */
    private function getDbParentId(Array $link)
    {
        if (empty($link["parent_id"])) {
            return null;
        } else {
            $parent_id = $link["parent_id"];
            if ($parent_id < 10000) {
                return $parent_id;
            } else {
                return $this->client_id_to_db_id_map[$link["parent_id"]];
            }
        }
    }


}