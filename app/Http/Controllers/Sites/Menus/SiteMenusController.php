<?php

namespace App\Http\Controllers\Sites\Menus;


use App\Entities\Models\Sites\SiteSetting;
use App\Entities\Models\Sites\SiteSettingDefault;
use App\Entities\Repositories\Sites\SettingsDefaultRepo;
use App\Http\Controllers\Controller;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Menu;
use App\Http\Traits\AjaxAlertTrait;

class SiteMenusController extends Controller
{
    const PERMISSION = 'top.menus';

    protected $assets = ['form', 'top-app'];

    use AjaxAlertTrait;

    /**
     * Redirect to random menu or to create page if there isn't any
     *
     * @param Site $site
     *
     * @return mixed
     */
    public function index(Site $site)
    {
        $firstMenu = Menu::first();
        if ($firstMenu) {
            return redirect()->route('sites.{site}.menus.{menu}.items.index', [$site->id, $firstMenu->id]);
        } else {
            return redirect()->route('sites.{site}.menus.create', [$site->id]);
        }
    }

    /**
     * @param Site $site
     *
     * @return mixed
     */
    public function create(Site $site)
    {
        return view('pages.site.menu.create')
            ->with(compact('site'));
    }

    /**
     * @param Site $site
     *
     * @return mixed
     */
    public function store(Site $site)
    {
        if (! auth()->user()->can(self::PERMISSION . '.add')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $menu = new Menu(request()->all());
        $menu->save();

        $settings_type = SiteSetting::getMenuType($menu->id);
        SettingsDefaultRepo::setDefaultSettingForType($site->id, $settings_type, SiteSettingDefault::TYPE_MENU);

        return redirect()->route('sites.{site}.menus.{menu}.items.index', [$site->id, $menu->id]);
    }

    /**
     * @param Site $site
     * @param      $menu_id
     *
     * @return mixed
     */
    public function edit(Site $site, $menu_id)
    {
        $menu = Menu::find($menu_id);
        return view('pages.site.menu.create')
            ->with(compact('site', 'menu'));
    }

    /**
     * @param Site $site
     * @param      $menu_id
     *
     * @return mixed
     */
    public function update(Site $site, $menu_id)
    {
        if (! auth()->user()->can(self::PERMISSION . '.edit')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $menu       = Menu::find($menu_id);
        $menu->name = request()->get('name');
        $menu->save();
        return redirect()->route('sites.{site}.menus.{menu}.items.index', [$site->id, $menu->id]);
    }

    /**
     * Send delete alert when trying to delete a menu
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    private function ajaxAlert_deleteMenu($params, $extra_params)
    {
        $site_id = $extra_params[0];
        $menu_id = $extra_params[1];
        $menu    = Menu::find($menu_id);

        $text = 'Do you want to delete menu ' . $menu->name . ' with all of is menu items?';
        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.menus.destroy')
            ->with('action', 'delete')
            ->with('route_params', [$site_id, $menu_id]);
    }

    /**
     * Delete a menu and all of its items redirect to index afterwards
     *
     * @param Site $site
     * @param      $menu_id
     *
     * @return mixed
     */
    public function destroy(Site $site, $menu_id)
    {
        if (! auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $menu = Menu::find($menu_id);
        $menu->items()->delete();
        $menu->delete();
        return redirect()->route('sites.{site}.menus.index', [$site->id]);
    }
}