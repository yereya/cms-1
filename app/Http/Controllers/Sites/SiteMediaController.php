<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use App\Libraries\Media\MediaUploader;


/**
 * Class SiteMediaController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteMediaController extends Controller
{
    const PERMISSION = 'top.media';

    /**
     * @var string $assets
     */
    protected $assets = ['elfinder', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Media Management';

    /**
     * Display a listing of the resource.
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function index(Site $site)
    {
        return view('pages.site.media.index')->with(compact('site'));
    }
}
