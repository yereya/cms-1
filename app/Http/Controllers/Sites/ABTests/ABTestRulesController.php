<?php namespace App\Http\Controllers\Sites\ABTests;

use App\Entities\Models\Sites\ABTest;
use App\Entities\Models\Sites\ABTestRule;
use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\Select2Trait;
use App\Http\Traits\ToggleProperty;
use Illuminate\Http\Request;

/**
 * Class ABTestRulesController
 *
 * @package App\Http\Controllers\Sites
 */
class ABTestRulesController extends Controller
{
    use ToggleProperty;
    use AjaxModalTrait;
    use Select2Trait;
    const PERMISSION = 'top.ab-tests';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site AB Test Rules Management';

    /**
     * Display a listing of the resource.
     *
     * @param Site   $site
     * @param ABTest $test
     *
     * @return \Illuminate\View\View
     */
    public function index(Site $site, ABTest $test)
    {
        $rules = $test->rules;

        return view('pages.site.ab-test-rules.index')
            ->with(compact('site', 'test', 'rules'));
    }

    /**
     * Create
     *
     * @param Site   $site
     * @param ABTest $test
     *
     * @return \Illuminate\View\View
     */
    public function create(Site $site, ABTest $test)
    {
        return view('pages.site.ab-test-rules.form')
            ->with(compact('site', 'test'));
    }

    /**
     * Store
     *
     * @param Request $request
     * @param Site    $site
     * @param ABTest  $test
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Site $site, ABTest $test)
    {
        $rule          = new ABTestRule($request->all(), $site);
        $rule->test_id = $test->id;
        $rule->save();

        if ($request->get('create_another')) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["success" => "The rule for test {$test->name} has been added"]);
        }

        return redirect()
            ->route('sites.{site}.ab-tests.{test}.rules.index', [$site->id, $test->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The rule for test {$test->name} has been added"]);
    }

    /**
     * Edit
     *
     * @param Site       $site
     * @param ABTest     $test
     * @param ABTestRule $rule
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site, ABTest $test, ABTestRule $rule)
    {
        return view('pages.site.ab-test-rules.form')->with(compact('site', 'test', 'rule'));
    }

    /**
     * Update
     *
     * @param Request    $request
     * @param Site       $site
     * @param ABTest     $test
     * @param ABTestRule $rule
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site, ABTest $test, ABTestRule $rule)
    {
        $rule->fill($request->all());
        $rule->save();

        if ($request->get('create_another')) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["success" => "The rule for test {$test->name} has been saved"]);
        }

        return redirect()
            ->route('sites.{site}.ab-tests.{test}.rules.index', [$site->id, $test->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The rule for test {$test->name} has been saved"]);
    }

    /**
     * Destroy
     *
     * @param Site       $site
     * @param ABTest     $test
     * @param ABTestRule $rule
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, ABTest $test, ABTestRule $rule)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $rule->delete();

        return redirect()
            ->route('sites.{site}.ab-tests.{test}.rules.index', [$site->id, $test->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The rule for test {$test->name} has been deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_deleteRule($params, $extra_params)
    {
        $site_id = $extra_params[0];
        $test_id = $extra_params[1];

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Rule')
                                                      ->with('btn_title', 'Delete Rule')
                                                      ->with('route', [
                                                          'sites.{site}.ab-tests.{test}.rules.destroy',
                                                          $site_id,
                                                          $test_id,
                                                          $params['rule_id']
                                                      ]);
    }
}
