<?php namespace App\Http\Controllers\Sites\ABTests;

use App\Entities\Models\Sites\ABTest;
use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteABTestsFormRequest;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\Select2Trait;
use App\Http\Traits\ToggleProperty;

/**
 * Class ABTestsController
 *
 * @package App\Http\Controllers\Sites
 */
class ABTestsController extends Controller
{
    use ToggleProperty;
    use AjaxModalTrait;
    use Select2Trait;
    const PERMISSION = 'top.ab-tests';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'ab-tests'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site AB Tests Management';

    /**
     * Display a listing of the resource.
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function index(Site $site)
    {
        $tests = $site->abTests()->get();

        return view('pages.site.ab-tests.index')->with(compact('site', 'tests'));
    }

    /**
     * Create
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function create(Site $site)
    {
        return view('pages.site.ab-tests.form')->with(compact('site'));
    }

    /**
     * Store
     *
     * @param SiteABTestsFormRequest $request
     * @param Site                   $site
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteABTestsFormRequest $request, Site $site)
    {
        $test = new ABTest($request->all(), $site);
        $test->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The test {$test->name} added"]);
        }

        return redirect()->route('sites.{site}.ab-tests.index', $site->id)
                         ->with(self::MESSAGE_TOAST, ["success" => "The test {$test->name} added"]);
    }

    /**
     * Edit
     *
     * @param Site   $site
     * @param ABTest $test
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site, ABTest $test)
    {
        return view('pages.site.ab-tests.form', compact('site', 'test'));
    }

    /**
     * Update
     *
     * @param SiteABTestsFormRequest $request
     * @param Site                   $site
     * @param ABTest                 $test
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteABTestsFormRequest $request, Site $site, ABTest $test)
    {
        $test->fill($request->all());
        $test->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The test {$test->name} saved"]);
        }

        return redirect()->route('sites.{site}.ab-tests.index', $site->id)
                         ->with(self::MESSAGE_TOAST, ["success" => "The test {$test->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param Site   $site
     * @param ABTest $test
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, ABTest $test)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $test->delete();

        return redirect()->route('sites.{site}.ab-tests.index', $site->id)
                         ->with(self::MESSAGE_TOAST, ["success" => "The test {$test->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_deleteTest($params, $extra_params)
    {
        $site_id = $extra_params[0];

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Test')->with('btn_title', 'Delete Test')
                                                      ->with('route', [
                                                          'sites.{site}.ab-tests.destroy',
                                                          $site_id,
                                                          $params['test_id']
                                                      ]);
    }
}
