<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Repositories\Sites\PostRatingRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostsRatingRequest;

class PostsRatingController extends Controller
{
    /**
     * Update or create resource in storage.
     *
     * @param PostsRatingRequest $request
     *
     * @return string
     */
    public function update(PostsRatingRequest $request)
    {
        $status       = ['status' => 0];
        $rate_it_repo = (new PostRatingRepo());
        $is_completed = $rate_it_repo->saveFromRequest($request);

        if ($is_completed) {
            $status = ['status' => 1];
        }

        return $status;
    }
}
