<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Site;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SiteDashboardController extends Controller
{
    const PERMISSION = 'top';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Sites Management';

    /**
     * Display a listing of the resource.
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function index(Site $site)
    {
        return view('pages.site.dashboard.index')->with('site', $site);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function show(Site $site)
    {
        return view('pages.site.dashboard.show')->with('site', $site);
    }
}
