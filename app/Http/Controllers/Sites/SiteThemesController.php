<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteTheme;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteThemesFormRequest;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\ToggleProperty;
use App\Libraries\SiteConnection\Manager;
use Illuminate\Http\Request;

/**
 * Class SiteThemesController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteThemesController extends Controller
{
    use AjaxModalTrait;
    use ToggleProperty;

    const PERMISSION = 'top.themes';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Themes Management';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.site-themes.index')->with('themes', SiteTheme::all());
    }

    /**
     * Create
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.site-themes.form');
    }

    /**
     * Store
     *
     * @param  SiteThemesFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteThemesFormRequest $request)
    {
        $theme = new SiteTheme($request->all());
        $theme->save();

        if ($request->get('create_another')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["success" => "The theme {$theme->name} added"]);
        }

        return redirect()->route('site-themes.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The theme {$theme->name} added"]);
    }

    /**
     * Edit
     *
     * @param int $theme_id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($theme_id)
    {
        $theme = SiteTheme::findOrFail($theme_id);

        return view('pages.site-themes.form', compact('theme'));
    }

    /**
     * Update
     *
     * @param SiteThemesFormRequest $request
     * @param int                       $theme_id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteThemesFormRequest $request, $theme_id)
    {
        $theme = SiteTheme::findOrFail($theme_id);
        $theme->fill($request->all());
        $theme->save();

        if ($request->get('create_another')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["success" => "The theme {$theme->name} saved"]);
        }

        return redirect()->route('site-themes.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The theme {$theme->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param int $theme_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($theme_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $theme = SiteTheme::findOrFail($theme_id);
        $theme->delete();

        return redirect()->route('site-themes.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The Theme {$theme->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_deleteSite($params, $extra_params)
    {
        $theme_id = $extra_params[0]; //get the site id from the passed params

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Theme')
                                                      ->with('btn_title', 'Delete theme')
                                                      ->with('route', ['site-themes.destroy', $theme_id]);
    }

    /**
     * ToogleCheckbox sites
     *
     * @param $request
     * @param $params
     *
     * @return array
     */
    private function toggleCheckbox_sites(Request $request, $params)
    {
        $status       = 1;
        $toggle_state = 0;

        $site = Site::findOrFail($request->get('site_id'));
        $theme = SiteTheme::findOrFail($request->get('theme_id'));

        if($theme->sites->contains($site)) {
            $theme->sites()->detach($site);
            $desc = "The theme {$theme->name} was detached from {$site->name} site";
        } else {
            $theme->sites()->attach($site);
            $toggle_state = 1;
            $desc = "The theme {$theme->name} was assigned to {$site->name} site";
        }

        return [
            'status'      => $status,
            'desc'        => $desc,
            'toggleState' => $toggle_state
        ];
    }
}
