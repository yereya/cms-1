<?php

namespace App\Http\Controllers\Sites\ContentType;

use App\Entities\Models\Sites\ContentAuthor;
use App\Entities\Models\Sites\Site;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteContentAuthorRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;

class SiteContentAuthorsController extends Controller
{

    use AjaxModalTrait, AjaxAlertTrait;

    const PERMISSION = 'top.content_author';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];


    /**
     * @var string $page_title
     */
    protected $page_title = 'Content Authors';

    protected $site;

    /**
     * SiteContentAuthorsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->site = SiteConnectionLib::getSite();
    }

    /**
     * @param Site $site
     *
     * @return string
     */
    public function index(Site $site)
    {
        $content_authors = ContentAuthor::with('image')->get();
        $can_delete      = auth()->user()->can(self::PERMISSION . '.delete');
        $page_title = $this->page_title;

        return view('pages.site.content-authors.index')
            ->with(compact('site', 'content_authors', 'can_delete', 'page_title'));
    }

    /**
     * Create
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function create(Site $site)
    {
        return view('pages.site.content-authors.form')
            ->with(compact('site'));
    }

    /**
     * Store
     *
     * @param SiteDomainsFormRequest|SiteContentAuthorRequest $request
     * @param  Site $site
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteContentAuthorRequest $request, Site $site)
    {
        $content_authors = new ContentAuthor($request->all());
        $content_authors->save();

        return redirect()->route('sites.{site}.content-authors.index', $site->id)
            ->with(self::MESSAGE_TOAST, ["success" => "The Content Author {$content_authors->first_name} added"]);
    }

    /**
     * Edit
     *
     * @param Site $site
     * @param $author_id
     *
     * @return $this
     */
    public function edit(Site $site, $author_id)
    {
        $content_authors = ContentAuthor::where('id', $author_id)
            ->with('image')
            ->first();

        return view('pages.site.content-authors.form')
            ->with(compact('site', 'content_authors'));
    }

    /**
     * Update
     *
     * @param SiteDomainsFormRequest|SiteContentAuthorRequest $request
     * @param Site $site
     * @param $author_id
     *
     * @return \Illuminate\Http\Response
     *
     */
    public function update(SiteContentAuthorRequest $request, Site $site, $author_id)
    {
        $content_authors = ContentAuthor::find($author_id);
        $content_authors->fill($request->all());
        $content_authors->save();

        return redirect()->route('sites.{site}.content-authors.index', $site->id)
            ->with(self::MESSAGE_TOAST, ["success" => "The Content Author {$content_authors->first_name} saved"]);

    }

    /**
     * Destroy
     *
     * @param Site $site
     * @param      $author_id
     *
     * @return mixed
     */
    public function destroy(Site $site, $author_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $new_author = request()->get('author_to_replace');

        $this->changeAuthorInAllContentTypes($site, $author_id, $new_author);
        ContentAuthor::find($author_id)->delete();

        return redirect()->route('sites.{site}.content-authors.index', [$site->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The author was deleted"]);
    }

    /**
     * Change Author In All Content Types
     *
     * @param Site $site
     * @param $old_author_id
     * @param $new_author_id
     *
     * @throws \Exception
     */
    private function changeAuthorInAllContentTypes(Site $site, $old_author_id, $new_author_id)
    {
        $new_author = ContentAuthor::find($new_author_id)->get();
        if (!$new_author) {
            throw new \Exception("New Author is not exists");
        }

        $content_types   = $site->contentTypes()->get();
        $site_connection = SiteConnectionLib::getConnection();

        foreach ($content_types as $content_type) {
            $table_name = $content_type->full_table;

            if ($site_connection->getSchemaBuilder()->hasColumn($table_name,'content_author_id')) {
                $site_connection->table($table_name)
                    ->where('content_author_id', $old_author_id)
                    ->update(['content_author_id' => $new_author_id]);
            }
        }
    }

    /**
     * Ajax Modal File Modal
     *
     * @param $params
     *
     * @return mixed
     */
    private function ajaxModal_fileModal($params)
    {
        return view('pages.site.content-type.data.file-manager-modal')
            ->with('title', 'File Manager')
            ->with('site', $this->site)
            ->with('field', $params['field'] ?? null);
    }


    /**
     * Ajax Alert Delete Contet Author
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    private function ajaxAlert_deleteContentAuthor($params, $extra_params)
    {
        $site_id   = $extra_params[0];
        $author_id = $extra_params[1];

        $author = ContentAuthor::find($author_id);

        $authors = ContentAuthor::where('id', '!=', $author_id)
            ->get()
            ->pluck('full_name', 'id');

        return view('pages.site.content-authors.delete-form')
            ->with(compact('author', 'authors', 'site_id'));
    }

}
