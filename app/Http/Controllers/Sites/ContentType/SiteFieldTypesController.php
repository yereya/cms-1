<?php namespace App\Http\Controllers\Sites\ContentType;

use App\Entities\Models\Sites\ContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Sites\SiteFieldTypesFormRequest;

/**
 * Class SiteFieldTypesController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteFieldTypesController extends Controller
{
    const PERMISSION = 'top.content_types';

    private $html;

    public function __construct()
    {
        $this->html = view()->make('partials.fields.metadata.repeater');
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request         $request
     * @param Site            $site
     * @param SiteContentType $content_type
     * @param                 $type
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Site $site, SiteContentType $content_type, $type)
    {
        $this->getType($type, $content_type);

        if ($request->has('field_id')) {
            $this->loadSaved($request->get('field_id'));
        }

        return response()->json($this->html->render());
    }

    /**
     * Get type and type attr from config file
     *
     * @param $type
     * @param $content_type
     */
    private function getType ($type, $content_type) {
        $type = collect(config('sites.field_types'))->first(function ($value, $key) use ($type) {
            return $value['type'] == strtolower($type);
        });

        if (is_null($type)) {
            return abort(404, "type: $type not found");
        }

        $this->html->with('metadata', $type['metadata'])
            ->with('content_type', $content_type);

        $type['type'] != 'content_type_link' ?: $this->contentType($content_type->site_id);
    }

    /**
     * Load only exists field
     *
     * @param $field_id
     */
    private function loadSaved($field_id)
    {
        if ($field_id) {
            $field = SiteContentTypeField::find($field_id);

            if ($field) {
                $this->html->with('data', $field->metadata);
            }
        }
    }

    /**
     * Insert content type list
     *
     * @param integer $site_id
     *
     * @return void
     */
    private function contentType($site_id)
    {
        $content_types = SiteContentType::select('id', 'name as text')
            ->where('site_id', $site_id)
            ->get();

        $this->html->with('content_types', $content_types);
    }
}
