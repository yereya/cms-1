<?php

namespace App\Http\Controllers\Sites\ContentType;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeFieldGroup;
use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Sites\SiteContentTypeFieldGroupsFormRequest;
use App\Http\Requests\Sites\SiteContentTypeFieldsFormRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;
use Illuminate\View\View;

/**
 * Class SiteContentTypeFieldGroupsController
 * @package App\Http\Controllers\Sites
 */
class SiteContentTypeFieldGroupsController extends Controller
{
    use AjaxModalTrait;
    use AjaxAlertTrait;

    const PERMISSION = 'top.content_types.fields';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app', 'nestable'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Content Type Field Groups Management';

    /**
     * Index
     *
     * @param Site $site
     * @param SiteContentType $content_type
     *
     * @return $this
     */
    public function index(Site $site, SiteContentType $content_type) {
        $groups = SiteContentTypeFieldGroup::where('site_id', $site->id)
            ->where('parent_id', 0)
            ->where('content_type_id', $content_type->id)
            ->with('childrenRecursive')
            ->get();

        $can = $this->buildCanPermissions();

        return view('pages.site.content-type.fields-groups.index')->with(compact('site', 'content_type', 'groups', 'can'));
    }

    /**
     * Create
     *
     * @param Site $site
     * @param SiteContentType $content_type
     *
     * @return $this
     */
    public function create(Site $site, SiteContentType $content_type) {
        $groups = SiteContentTypeFieldGroup::where('site_id', $site->id)->where('content_type_id', $content_type->id)->get()->pluck('name', 'id');
        return view('pages.site.content-type.fields-groups.form')->with(compact(['site', 'content_type', 'groups']));
    }

    /**
     * Edit Group
     *
     * @param Site $site
     * @param SiteContentType $content_type
     * @param SiteContentTypeFieldGroup $field_group
     *
     * @return $this
     */
    public function edit(Site $site, SiteContentType $content_type, SiteContentTypeFieldGroup $field_group) {
        $groups = SiteContentTypeFieldGroup::where('site_id', $site->id)->where('content_type_id', $content_type->id)->get()->pluck('name', 'id');
        return view('pages.site.content-type.fields-groups.form')->with(compact(['site', 'content_type', 'field_group', 'groups']));
    }

    /**
     * Update Group
     *
     * @param SiteContentTypeFieldGroupsFormRequest $request
     * @param Site $site
     * @param SiteContentType $content_type
     * @param SiteContentTypeFieldGroup $field_group
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(SiteContentTypeFieldGroupsFormRequest $request, Site $site, SiteContentType $content_type, SiteContentTypeFieldGroup $field_group) {
        try {
            $field_group->fill($request->all());
            $field_group->save();
        } catch (\Exception $e) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ['error' => $e->getMessage()]);
        }

        return redirect()->route('sites.{site}.content-types.{content_type}.field-groups.index',
            [$site->id, $content_type->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The field {$field_group->name} updated"]);
    }

    /**
     * Store
     *
     * @param SiteContentTypeFieldGroupsFormRequest|SiteContentTypeFieldsFormRequest $request
     * @param Site                                                                   $site
     * @param SiteContentType                                                        $content_type
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteContentTypeFieldGroupsFormRequest $request, Site $site, SiteContentType $content_type)
    {
        try {
            $max_priority = SiteContentTypeFieldGroup::where('content_type_id', $content_type->id)->max('priority');
            $new_group = new SiteContentTypeFieldGroup();
            $new_group->fill($request->all());
            $new_group->priority = $max_priority + 10;
            $new_group->save();
        } catch (\Exception $e) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }

        return redirect()->route('sites.{site}.content-types.{content_type}.field-groups.index',
            [$site->id, $content_type->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The field group {$new_group->name} added"]);
    }


    /**
     * Destroy
     *
     * @param Site                      $site
     * @param SiteContentType           $content_type
     * @param SiteContentTypeFieldGroup $field_group
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, SiteContentType $content_type, SiteContentTypeFieldGroup $field_group)
    {
        if (!auth()->user()->can(self::PERMISSION . '.groups.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        try {
            SiteContentTypeField::where('field_group_id', $field_group->id)->update(['field_group_id' => null]);
            $field_group->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["error" => $e->getMessage()]);
        }

        return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The field group was deleted"]);
    }

    /**
     * Save priority groups
     *
     * @param Request $request
     * @param Site $site
     * @param SiteContentType $content_type
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function order(Request $request, Site $site, SiteContentType $content_type) {
        if ($request->has('list')) {
            $list = json_decode($request->get('list'));
            $groups = SiteContentTypeFieldGroup::where('site_id', $site->id)
                ->where('content_type_id', $content_type->id)
                ->get();

            foreach ($groups as $group) {
                if (isset($list->{$group->id})) {
                    $new_group = $list->{$group->id};
                    $group->parent_id = empty($new_group->parent_id) ? null : $new_group->parent_id;
                    $group->priority = $new_group->priority;
                    $group->save();
                }
            }
        }

        return redirect()->route('sites.{site}.content-types.{content_type}.field-groups.index', [$site->id, $content_type->id]);
    }

    /**
     * Manager
     *
     * @param $params
     * @param $extra_params
     *
     * @return $this
     */
    public function ajaxModal_manager($params, $extra_params)
    {
        $site_id = $extra_params[0];
        $content_type_id = $extra_params[1];
        $field_groups = SiteContentTypeFieldGroup::where('site_id', $site_id)->where('content_type_id', $content_type_id)->orderBy('name')->get();

        return view('pages.site.content-type.fields-groups.groups-modal')
            ->with(compact('field_groups', 'site_id', 'content_type_id'));
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return \Illuminate\View\View
     */
    public function ajaxAlert_deleteGroup($params, $extra_params)
    {
        $site_id = $extra_params[0];
        $content_type_id = $extra_params[1];
        $field_group_id = $extra_params[2];
        $field_group_names = [];
        $this->getFieldValue('name', SiteContentTypeFieldGroup::where('id', $field_group_id)->with('childrenRecursive')->get()->toArray(), $field_group_names);

        $message = 'Please confirm the deletion of field group|s ' . PHP_EOL . implode(PHP_EOL, $field_group_names) . '. All group fields saves in none group.';

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $message)
            ->with('route', 'sites.{site}.content-types.{content_type}.field-groups.destroy')
            ->with('action', 'delete')
            ->with('route_params', [$site_id, $content_type_id, $field_group_id]);
    }

    /*
     * Get reclusive field value array
     */
    private function getFieldValue($value, $groups, &$store) {
        foreach ($groups as $group) {
            if (count($group['children_recursive']) != 0) {
                $store[] = $group[$value];
                $this->getFieldValue($value, $group['children_recursive'], $store);
            } else {
                $store[] = $group[$value];
            }
        }
    }
}
