<?php

namespace App\Http\Controllers\Sites\ContentType;

use App\Entities\Models\Sites\Comment;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Post;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SiteSetting;
use App\Entities\Models\Sites\SiteSettingDefault;
use App\Entities\Repositories\Sites\SettingsDefaultRepo;
use App\Entities\Repositories\Sites\SiteContentTypeRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteContentTypesFormRequest;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\Select2Trait;
use DB;

/**
 * Class SiteContentTypesController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteContentTypesController extends Controller
{
    use AjaxModalTrait, Select2Trait;

    const PERMISSION = 'top.content_types';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Content Types Management';

    /**
     * Display a listing of the resource.
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function index(Site $site)
    {
        $types = $site->contentTypes;

        return view('pages.site.content-type.types.index')->with(compact('site', 'types'));
    }

    /**
     * Create
     *
     * @param Site $site
     *
     * @return \Illuminate\View\View
     */
    public function create(Site $site)
    {
        return view('pages.site.content-type.types.form')->with(compact('site'));
    }

    /**
     * Store
     *
     * @param  SiteContentTypesFormRequest $request
     * @param  Site                        $site
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteContentTypesFormRequest $request, Site $site)
    {
        (new SiteContentTypeRepo())->addSlugParamToRequest($request);
        $type = new SiteContentType($request->all());

        try {
            $connection = SiteConnectionLib::getFacadeRoot();
            $connection->migrate()->setShortCodesHaystack([
                'table' => $request->get('table')
            ]);
            $connection->migrate()->run('create_content_type_table');

        } catch (\Exception $m) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ['error' => $m->getMessage()]);
        }

        $type->save();

        $settings_type = SiteSetting::getContentTypeType($type->id);
        SettingsDefaultRepo::setDefaultSettingForType($site->id, $settings_type, SiteSettingDefault::TYPE_CONTENT_TYPE);

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The type {$type->name} added"]);
        }

        return redirect()->route('sites.{site}.content-types.index', $type->site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The type {$type->name} added"]);
    }

    /**
     * Edit
     *
     * @param Site            $site
     * @param SiteContentType $content_type
     *
     * @return \Illuminate\View\View
     */
    public function edit(Site $site, SiteContentType $content_type)
    {
        return view('pages.site.content-type.types.form', compact('site'))->with('content_type', $content_type);
    }

    /**
     * Update
     *
     * @param SiteContentTypesFormRequest $request
     * @param Site                        $site
     * @param SiteContentType             $content_type
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteContentTypesFormRequest $request, Site $site, SiteContentType $content_type)
    {
        $content_type->fill($request->all());

        try {
            $connection = SiteConnectionLib::getFacadeRoot();
            $connection->migrate()->setShortCodesHaystack([
                'table' => $request->get('table')
            ]);
        } catch (\Exception $m) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ['error' => $m->getMessage()]);
        }

        $content_type->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The type {$content_type->name} saved"]);
        }

        return redirect()->route('sites.{site}.content-types.index', $content_type->site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The type {$content_type->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param Site            $site
     * @param SiteContentType $content_type
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Site $site, SiteContentType $content_type)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        DB::beginTransaction();

        $post_ids = Post::where('content_type_id', $content_type->id)->get()->pluck('id')->toArray();

        try {
            $connection = SiteConnectionLib::getFacadeRoot();
            $connection->migrate()->setShortCodesHaystack([
                'table' => $content_type->table
            ]);
            $connection->migrate()->run('create_content_type_table', 'down');

            //Remove from table posts
            Post::whereIn('id', array_values($post_ids))->delete();

            //Remove comments and likes
            Comment::whereIn('post_id', array_values($post_ids))->delete();

            $content_type->delete();
        } catch (\Exception $m) {
            DB::rollBack();
            return redirect()->back()->with(self::MESSAGE_TOAST, ['error' => $m->getMessage()]);
        }

        DB::commit();

        return redirect()->route('sites.{site}.content-types.index', $content_type->site_id)
            ->with(self::MESSAGE_TOAST, ["success" => "The type {$content_type->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return \Illuminate\View\View
     */
    public function ajaxModal_deleteContentType($params, $extra_params)
    {
        $site_id = $extra_params[0];
        $type_id = $extra_params[1];

        return view('pages.delete-confirmation-modal')->with('title', 'Delete Content Type')
            ->with('btn_title', 'Delete Content Type')->with('route', [
                'sites.{site}.content-types.destroy',
                $site_id,
                $type_id
            ]);
    }

}
