<?php

namespace App\Http\Controllers\Sites\ContentType;

use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeField;
use App\Entities\Models\Sites\ContentTypes\SiteContentTypeFieldGroup;
use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\Templates\WidgetData;
use App\Entities\Repositories\Sites\LinkContentTypeRepo;
use App\Entities\Repositories\Sites\ContentTypeRepo;
use App\Entities\Repositories\Sites\SiteContentTypeFieldRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteContentTypeFieldsFormRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\AjaxOrderTrait;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use DB;
use Illuminate\Http\Request;

/**
 * Class SiteContentTypeFieldsController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteContentTypeFieldsController extends Controller
{
    use AjaxModalTrait, AjaxOrderTrait, DataTableTrait, AjaxAlertTrait, Select2Trait;
    const PERMISSION = 'top.content_types.fields';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app', 'nestable'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Content Type Fields Management';

    /**
     * Display a listing of the resource.
     *
     * @param Site            $site
     * @param SiteContentType $content_type
     *
     * @return \Illuminate\View\View
     */
    public function index(Site $site, SiteContentType $content_type)
    {
        //get groups with fields
        $groups = SiteContentTypeFieldGroup::where('content_type_id', $content_type->id)
            ->where('site_id', $site->id)
            ->where('parent_id', 0)
            ->with('childrenRecursive')
            ->with('fields')
            ->orderBy('priority', 'asc')
            ->get();

        //create null group and add fields without group
        $empty_group = new SiteContentTypeFieldGroup(['name' => 'None Group']);
        $empty_group->setRelations([
            'fields' => SiteContentTypeField::where('type_id', $content_type->id)->where(function ($query) {
                $query->whereNull('field_group_id')->orWhere('field_group_id', 0);
            })->orderBy('priority', 'asc')->get()
        ]);
        $groups->push($empty_group);

        $can = $this->buildCanPermissions();

        return view('pages.site.content-type.fields.index')->with(compact('site', 'content_type', 'groups', 'can'));
    }

    /**
     * Create
     *
     * @param Site            $site
     * @param SiteContentType $content_type
     *
     * @return \Illuminate\View\View
     */
    public function create(Site $site, SiteContentType $content_type)
    {
        $field_group = SiteContentTypeFieldGroup::where('site_id', $site->id)
            ->where('content_type_id', $content_type->id)
            ->orderBy('name')
            ->get()
            ->map(function ($item) {
                return [
                    'id'   => $item->id,
                    'text' => $item->name
                ];
            });

        $field_types = config('sites.field_types');

        return view('pages.site.content-type.fields.form')
            ->with(compact('site', 'content_type', 'field_group', 'field_types'))
            ->with('action', 'CREATE');
    }

    /**
     * Store
     *
     * @param SiteContentTypeFieldsFormRequest $request
     * @param Site                             $site
     * @param SiteContentType                  $content_type
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SiteContentTypeFieldsFormRequest $request, Site $site, SiteContentType $content_type)
    {
        DB::beginTransaction();
        $values = $request->all();

        try {
            $field           = new SiteContentTypeField($this->buildMetadata($values));
            $field->priority = (new SiteContentTypeFieldRepo())->getCalculatedFieldPriority($content_type);
            $field->save();

            if ($values['type'] == 'content_type_link') {
                $link_repo = new LinkContentTypeRepo();
                $link_repo->syncContentTypeLinkStore($values);
            } else {
                (new ContentTypeRepo())->alterColumn($content_type, $field);
            }

            $field->save();
        } catch (\Exception $m) {
            DB::rollBack();

            return redirect()->back()->with(self::MESSAGE_TOAST, ['error' => $m->getMessage()]);
        }

        DB::commit();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The field {$field->name} added"]);
        }

        return redirect()->route('sites.{site}.content-types.{content_type}.fields.index', [
            $content_type->site_id,
            $content_type->id
        ])->with(self::MESSAGE_TOAST, ["success" => "The field {$field->name} added"]);
    }

    /**
     * Edit
     *
     * @param Site                 $site
     * @param SiteContentType      $content_type
     * @param SiteContentTypeField $field
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site, SiteContentType $content_type, $field)
    {
        $field       = SiteContentTypeField::find($field);
        $field_group = SiteContentTypeFieldGroup::where('site_id', $site->id)
            ->where('content_type_id', $content_type->id)
            ->orderBy('name')
            ->get()
            ->map(function ($item) {
                return [
                    'id'   => $item->id,
                    'text' => $item->name
                ];
            });

        $field_types = config('sites.field_types');

        return view('pages.site.content-type.fields.form',
            compact('site', 'content_type', 'field', 'field_group', 'field_types'))->with('action', 'EDIT');;
    }

    /**
     * @param Site            $site
     * @param SiteContentType $content_type
     * @param                 $field
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function duplicate(Site $site, $content_type, $field)
    {
        $type        = SiteContentType::find($content_type);
        $field       = SiteContentTypeField::find($field);
        $field_group = SiteContentTypeFieldGroup::where('site_id', $site->id)
            ->where('content_type_id', $type->id)
            ->orderBy('name')
            ->get()
            ->map(function ($item) {
                return [
                    'id'   => $item->id,
                    'text' => $item->name
                ];
            });

        $field_types = config('sites.field_types');

        return view('pages.site.content-type.fields.form', compact('site', 'field', 'field_group', 'field_types'))
            ->with('content_type', $type)
            ->with('action', 'DUPLICATE');
    }

    /**
     * Update
     *
     * @param SiteContentTypeFieldsFormRequest $request
     * @param Site                             $site
     * @param SiteContentType                  $content_type
     * @param SiteContentTypeField             $fields
     *
     * @return \Illuminate\Http\Response
     */
    public function update(
        SiteContentTypeFieldsFormRequest $request,
        Site $site,
        SiteContentType $content_type,
        SiteContentTypeField $fields
    )
    {
        DB::beginTransaction();
        $values = $request->all();

        try {
            $fields->fill($this->buildMetadata($values));

            if ($values['type'] == 'content_type_link') {
                $link_repo = new LinkContentTypeRepo();
                $link_repo->syncContentTypeLinkUpdate($values, $fields->name);
            } else {
                (new ContentTypeRepo())->changeColumn($content_type, $fields, $fields->name);
            }

            // Save fields only after columns were updated.
            $fields->save();
        } catch (\Exception $e) {
            DB::rollBack();

            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST,
                    ["error" => "The field not saved, because: " . json_encode($e->getMessage())]);
        }

        DB::commit();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The field {$fields->name} saved"]);
        }

        return redirect()->route('sites.{site}.content-types.{content_type}.fields.index', [
            $site->id,
            $content_type->id
        ])->with(self::MESSAGE_TOAST, ["success" => "The field {$fields->name} saved"]);
    }

    /**
     * Destroy
     *
     * @param Site                 $site
     * @param SiteContentType      $content_type
     * @param SiteContentTypeField $fields
     *
     * @return \Illuminate\Http\RedirectResponse
     * @internal param SiteContentTypeField $field
     */
    public function destroy(Site $site, SiteContentType $content_type, SiteContentTypeField $fields)
    {
        DB::beginTransaction();

        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()
                ->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        try {
            $content_type_repo = new ContentTypeRepo();

            if ($fields->type == 'content_type_link') { //remove all linked posts
                $link_repo         = new LinkContentTypeRepo();
                $sync_content_type = $link_repo->findLinkedContentType($fields);
                $sync_field        = $link_repo->findLinkedField($sync_content_type, $fields);

                $content_type_repo->removeField($sync_content_type->table, $sync_field->name);
                $link_repo->removeByFieldId($sync_field->id);
                $link_repo->removeByFieldId($fields->id);

                $sync_field->delete();
            } else {
                $content_type_repo->removeField($content_type->table, $fields->name);
            }

            $fields->delete();
        } catch (\Exception $m) {
            DB::rollBack();

            $message = $m->getMessage();

            $error_type = $this->resolveErrorType($message);

            if ($error_type === 'missing_column'){
                $fields->delete();

                return redirect()->route('sites.{site}.content-types.{content_type}.fields.index', [
                    $site->id,
                    $content_type->id
                ])->with(self::MESSAGE_TOAST, ["success" => "The field {$fields->name} was deleted"]);
            }

            return redirect()->back()->with(self::MESSAGE_TOAST, ['error' => $message]);
        }

        DB::commit();

        return redirect()->route('sites.{site}.content-types.{content_type}.fields.index', [
            $site->id,
            $content_type->id
        ])->with(self::MESSAGE_TOAST, ["success" => "The field {$fields->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxAlert_deleteField($params, $extra_params)
    {
        $site  = $extra_params[0];
        $type  = $extra_params[1];
        $field = $extra_params[2];

        $field = SiteContentTypeField::find($field);

        $text = view('pages.site.ajax-custom-alert')->with('name', $field->display_name)->render();

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.content-types.{content_type}.fields.destroy')
            ->with('action', 'delete')
            ->with('route_params', [$site, $type, $field]);
    }

    /**
     * Change priority fields
     *
     * @param $params
     *
     * @return array
     */
    public function ajaxOrder_fieldOrder($params)
    {
        $rows   = collect($params['rows'])->keyBy('itemId');
        $fields = SiteContentTypeField::whereIn('id', $rows->pluck('itemId'))->get();

        $res = [];
        foreach ($fields as $field) {
            $field->priority = $rows->get($field->id)['priority'];
            $res[]           = $field->save();
        }

        $final_result = !in_array(false, $res, true) ? 1 : 0;

        return [
            'status' => $final_result,
            'desc'   => ''
        ];
    }

    /**
     * @param Request         $request
     * @param Site            $site
     * @param SiteContentType $type
     *
     * @return string
     */
    public function priority(Request $request, Site $site, SiteContentType $type)
    {
        if ($request->has('list')) {
            $tree = json_decode($request->get('list'));

            $items = [];

            $this->iterateRecursiveOverContentType($items, $tree, null);

            try {
                $this->updateFieldGroupPriority($items, $site);
                $this->updateFieldPriority($items);
            } catch (\Exception $e) {
                return redirect()->route('sites.{site}.content-types.{content_type}.fields.index', [
                    $site->id,
                    $type->id
                ])->with(self::MESSAGE_TOAST, ["error" => "Error: " . json_encode($e->getMessage())]);
            }

            return redirect()
                ->route('sites.{site}.content-types.{content_type}.fields.index', [$site->id, $type->id])
                ->with(self::MESSAGE_TOAST, ["success" => "Fields priority saved successful!"]);
        }

        return redirect()
            ->route('sites.{site}.content-types.{content_type}.fields.index', [$site->id, $type->id])
            ->with(self::MESSAGE_TOAST, ["info" => "Nothings not saved!"]);
    }

    /**
     * Content Type Fields
     * only numeric
     * Use in Dynamic list widget
     *
     * @param $params
     *
     * @return array
     */
    public function select2_getNumericFields($params)
    {
        $needle_types = ['currency', 'number', 'percent'];

        return isset($params['dependency'][0]['value']) ? SiteContentTypeField::select('id', 'display_name AS text')
            ->where('type_id', $params['dependency'][0]['value'])
            ->whereIn('type', $needle_types)
            ->get() : null;
    }

    /**
     * Find content types
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse|null
     */
    public function find(Request $request)
    {
        if (!$request->has('content_type_id')) {
            return null;
        }

        $fields = SiteContentTypeField::where('type_id', $request->get('content_type_id'))
            ->orderBy('priority')
            ->get()
            ->map(function ($field) {
                return ['name' => $field->display_name . '[' . $field->type . ']', 'id' => $field->id];
            })
            ->pluck('name', 'id')
            ->toArray();

        if ($request->has('widget_data_id')) {
            $widget_data = WidgetData::find($request->get('widget_data_id'));
            $filters     = $this->findFilters($widget_data, 'fields_filter_ids');
            $orders      = $this->findFilters($widget_data, 'fields_order_ids');
        }

        $html = view()
            ->make('widgets-forms.dynamic-list.filters.index')
            ->with('id', 'nestable')
            ->with('fields', $fields)
            ->with('filters', $filters)
            ->with('orders', $orders)
            ->with('fields_filter_ids', $widget_data->data->fields_filter_ids ?? null)
            ->with('fields_order_ids', $widget_data->data->fields_order_ids ?? null);

        return response()->json($html->render());
    }

    /**
     * Get Filters and Orders (used in dynamic list widget)
     *
     * @param WidgetData $widget_data
     * @param            $attribute
     *
     * @return mixed|null
     */
    private function findFilters(WidgetData $widget_data, $attribute)
    {
        $metadata = $widget_data->data->{$attribute} ?? null;
        $fields   = null;

        if ($metadata) {
            $ids = [];
            foreach (json_decode($metadata) as $id) {
                $ids[] = $id->widget_data_id;
            }
            $fields = (new SiteContentTypeFieldRepo())->getFieldsByIdWithType($ids);
        }

        return $fields;
    }

    /**
     * Resolve the sql error type.
     *
     * @param string $error
     *
     * @return mixed
     */
    private function resolveErrorType($error)
    {
        $types = [
            'missing_column' => 'check that column/key exists'
        ];

        foreach ($types as $error_key => $error_description) {
            if (str_contains($error, $error_description)) {

                return $error_key;
            }
        }

        return null;
    }

    /**
     * Select2 Get Fields
     * Return fields list to select2 controls.
     * Deal with 2 cases one for specific item when select is first time called with preselect value.
     * case 2 is all the fields for the given type
     *
     * @param $params
     *
     * @return array
     * @throws \Exception
     */
    private function select2_getFields($params)
    {
        $content_type_id = $params['dependency'][0]['value'] ?? null;
        if (!$content_type_id) {
            throw new \Exception('Content Type id is required.');
        }

        $site_content_type_fields = SiteContentTypeField::select('id', 'name as text')
            ->where('type_id', $content_type_id);

        $this->select2FilterByCurrentValue($site_content_type_fields);
        $this->select2FilterBySearchQuery($site_content_type_fields);

        return $site_content_type_fields->get();
    }

    /**
     * Update group priority
     *
     * @param $items
     * @param $site
     */
    private function updateFieldGroupPriority($items, $site)
    {
        foreach ($items['group'] as $id => $group) {
            if ($id > 0) {
                SiteContentTypeFieldGroup::where('site_id', $site->id)->where('id', $id)->update($group);
            }
        }
    }

    /**
     * Update fields priority
     *
     * @param $items
     */
    private function updateFieldPriority($items)
    {
        foreach ($items['field'] as $id => $field) {
            SiteContentTypeField::where('id', $id)->update($field);
        }
    }

    /**
     * Create metadata string
     *
     * @param $values
     *
     * @return array
     */
    private function buildMetadata($values)
    {
        $metadata = [];
        if (isset($values['metadata'])) {
            $metadata = $values['metadata'];
        }

        if (isset($values['select'])) {
            $metadata['select'] = $this->parseSelectFieldType($values['select']);
        }

        if (isset($values['checkbox'])) {
            $metadata['checkbox'] = $this->parseCheckBoxFieldType($values['checkbox']);
        }

        if (isset($values['multi_select'])) {
            $metadata['multi_select'] = $this->parseMultiSelectFieldType($values['multi_select']);
        }

        unset($values['metadata']);
        unset($values['checkbox']);
        unset($values['select']);
        unset($values['multi_select']);

        if ($metadata) {
            $values['metadata'] = json_encode($metadata);
        }

        return $values;
    }

    /**
     * Parse checkbox fields
     *
     * @param $checkbox_rows
     *
     * @return string
     */
    private function parseCheckBoxFieldType($checkbox_rows)
    {
        $params = [];
        foreach ($checkbox_rows['key'] as $key => $name) {
            $params[] = [
                'priority' => $key,
                'key'      => $name,
                'value'    => true
            ];
        }

        return $params;
    }

    /**
     * Parse select fields
     *
     * @param $select_rows
     *
     * @return string
     */
    private function parseSelectFieldType($select_rows)
    {
        $params = [];
        foreach ($select_rows['key'] as $key => $name) {
            $params[] = [
                'priority' => $key,
                'default'  => (isset($select_rows['default']) && $select_rows['default'] == $key),
                'key'      => $name,
                'value'    => $select_rows['value'][$key] ?? null
            ];
        }

        return $params;
    }

    /**
     * Parse multi select fields
     *
     * @param $multi_select_rows
     *
     * @return string
     */
    private function parseMultiSelectFieldType($multi_select_rows)
    {
        $params = [];
        foreach ($multi_select_rows['key'] as $key => $name) {
            if (!empty($name)) {
                $params[] = [
                    'priority' => $key,
                    'default'  => $this->findDefaultInMultiSelect($key, $multi_select_rows),
                    'key'      => $name,
                    'value'    => $multi_select_rows['value'][$key] ?? null
                ];
            }
        }

        return $params;
    }

    /**
     * Find default value to current row
     *
     * @param $key
     * @param $rows
     *
     * @return bool
     */
    private function findDefaultInMultiSelect($key, $rows)
    {
        if (!isset($rows['default'])) {
            return false;
        }

        foreach ($rows['default'] as $default) {
            if ($default == $key) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param              $params
     * @param DatatableLib $datatable
     * @param              $uri_params
     *
     * @return array|null
     */
    private function dataTable_list($params, DatatableLib $datatable, $uri_params)
    {
        $permission_name = self::PERMISSION;
        $site_id         = $uri_params[0];
        $content_type_id = $uri_params[1];
        // Set Filters
        $datatable->changeFilters([
            'groups' => 'field_group_id',
        ]);

        $query = SiteContentTypeField::where('type_id', $content_type_id); //TODO replace the content type

        $params['recordsTotal']  = $query->count();
        $params['searchColumns'] = ['display_name'];

        $query->with('fieldGroup');

        $datatable->addColumn('id');
        $datatable->renderColumns([
            'priority'     => [
                'db_name'  => 'priority',
                'callback' => function ($data) {
                    return '<span item_id="' . $data['_id'] . '">' . $data['priority'] . '</span>';
                }
            ],
            'display_name' => 'name',
            'group'        => [
                'db_name'  => 'field_group_id',
                'callback' => function ($data) {
                    return $data['field_group']['name'] ?? '';
                }
            ],
            'required'     => [
                'db_name'  => 'is_required',
                'callback' => function ($data) {
                    if ($data['is_required']) {
                        return "<span class='label label-sm label-default'>Required</span>";
                    }

                    return '';
                }
            ],
            'actions'      => function ($data) use ($site_id, $permission_name, $content_type_id) {
                $res = '';

                if (auth()->user()->can($permission_name . '.edit')) {
                    $res .= \View::make('partials.fields.action-icon', [
                        'url'     => route('sites.{site}.content-types.{content_type}.fields.edit', [
                            $site_id,
                            $content_type_id,
                            $data['_id']
                        ]),
                        'icon'    => 'icon-pencil',
                        'tooltip' => 'Edit Field'
                    ])->render();
                }

                if (auth()->user()->can($permission_name . '.delete')) {
                    $res .= \View::make('partials.fields.action-icon', [
                        'url'        => route('sites.{site}.content-types.{content_type}.fields.ajax-modal', [
                            $site_id,
                            $content_type_id,
                            $data['_id'],
                            'method' => 'deleteField',
                        ]),
                        'icon'       => 'icon-trash',
                        'tooltip'    => 'Delete Field',
                        'attributes' => [
                            'data-toggle' => "modal",
                            'data-target' => '#ajax-modal'
                        ],
                    ])->render();
                }

                return $res;
            }
        ]);

        $params['query'] = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }

    /**
     * Iterate Recursive Over Content Type
     *
     * @param $groups
     * @param $tree - serialize array
     * @param $parent
     */
    private function iterateRecursiveOverContentType(&$groups, $tree, $parent)
    {
        $order = 10;
        foreach ($tree as $index => $item) {
            $item                               = (array)$item;
            $groups[$item['type']][$item['id']] = [
                'priority' => $order,
            ];
            if ($item['type'] == 'group') {
                $groups[$item['type']][$item['id']]['parent_id'] = $parent;
            } else {
                $groups[$item['type']][$item['id']]['field_group_id'] = $parent == 0 ? null : $parent;
            }
            $order += 10;

            if (isset($item['children'])) {
                $this->iterateRecursiveOverContentType($groups, $item['children'], $item['id']);
            }
        }
    }
}
