<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\ContactUs;
use App\Entities\Repositories\Sites\ContactUsRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\ContactUsRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Datatable\DatatableLib;
use View;

/**
 * @property ContactUsRepo model_repo
 */
class ContactUsController extends Controller
{
    use DataTableTrait;
    use AjaxAlertTrait;


    /**
     * Permission for site
     */
    const PERMISSION = 'top.contact-us';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Contact Us';

    /**
     * SiteContactUsController constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->model_repo = new ContactUsRepo();
    }

    /**
     * Display a listing of the resource.
     *
     * @param $site_id
     *
     * @return View
     */
    public function index($site_id)
    {
        return view('pages.site.contact-us.index')
            ->with(compact('site_id'));
    }

    /**
     * Show the record for creating a new resource.
     *
     * @param $site_id
     *
     * @return View
     */
    public function create($site_id)
    {
        $can = $this->buildCanPermissions();
        if (!$can['add']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        return view('pages.site.contact-us.form')
            ->with(compact('site_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ContactUsRequest $request
     * @param                  $site_id
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ContactUsRequest $request, $site_id)
    {
        $can = $this->buildCanPermissions();
        if (!$can['add']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $contact_us_record = $this->model_repo->prepareContactUsData($request);
        $is_created        = $this->model_repo->save($contact_us_record);

        if (!$is_created) {
            return redirect()
                ->route('sites.{site}.contact-us.edit', [$site_id, $contact_us_record->id])
                ->with(self::MESSAGE_TOAST, ['warning' => "The contact us record wasn't saved."]);
        }

        return ($request->get('create_another')
            ? redirect()
                ->back()->with(self::MESSAGE_TOAST, ['success' => "The contact us record saved successfully"])
            : redirect()
                ->route('sites.{site}.contact-us.index', [$site_id, $contact_us_record->id])
                ->with(self::MESSAGE_TOAST, ['success' => "The contact us record saved successfully"]));


    }

    /**
     * Show the record for editing the specified resource.
     *
     * @param               $site_id
     * @param ContactUs     $contact_us_record
     *
     * @return View
     * @internal param ContactUs $contact_us
     * @internal param int $id
     */
    public function edit($site_id, ContactUs $contact_us_record)
    {
        return view('pages.site.contact-us.form')
            ->with(compact('site_id', 'contact_us_record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ContactUsRequest     $request
     * @param                      $site_id
     * @param ContactUs            $contact_us_record
     *
     * @return View
     * @internal param $id
     */
    public function update(ContactUsRequest $request, $site_id, ContactUs $contact_us_record)
    {
        $can = $this->buildCanPermissions();
        if (!$can['edit']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $request    = $request->except(['_method', '_token']);
        $is_updated = $this->model_repo->update($contact_us_record, $request);

        if (!$is_updated) {
            return redirect()
                ->route('sites.{site}.contact-us.edit', [$site_id, $contact_us_record->id])
                ->with(self::MESSAGE_TOAST, ["warning" => "The contact us record wasn't saved."]);
        }

        return redirect()
            ->route('sites.{site}.contact-us.edit', [$site_id, $contact_us_record->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The contact us record saved successfully"]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param               $site_id
     * @param ContactUs     $contact_us_record
     *
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy($site_id, ContactUs $contact_us_record)
    {
        $can = $this->buildCanPermissions();
        if (!$can['delete']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $contact_us_record->delete();
        return redirect()->route('sites.{site}.contact-us.index', [$site_id])
            ->with(self::MESSAGE_TOAST, ["success" => "The contact us record  was deleted"]);
    }


    /**
     * Datatable
     *
     * @param              $params
     * @param DatatableLib $datatable
     *
     * @return array
     */
    public function datatable_list($params, DatatableLib $datatable)
    {

        $can     = $this->buildCanPermissions();
        $site_id = request()->route()->getParameter('site');
        $query   = (new ContactUs())->newQuery();

        $datatable->renderColumns([
                'id'      => 'id',
                'name'    => 'name',
                'email'   => 'email',
                'message' => 'message',
                'actions' => /**
                 * @param $data
                 *
                 * @return string
                 */
                    function ($data) use ($can, $site_id) {
                        $res = '';
                        if ($can['edit']) {
                            $res .= View::make('partials.containers.buttons.link', [
                                'route' => route('sites.{site}.contact-us.edit', [
                                    $site_id,
                                    $data['id']
                                ]),
                                'title' => 'Edit Record',
                                'icon'  => "icon-pencil",
                            ])->render();
                        }

                        if ($can['delete']) {
                            $res .= View::make('partials.containers.buttons.alert', [
                                'route' => route('sites.{site}.contact-us.ajax-alert', [
                                    $site_id,
                                    'id'     => $data['id'],
                                    'method' => 'delete'


                                ]),
                                'title' => 'Delete Contact Us Record',
                                'icon'  => "icon-trash",
                                'text'  => '',
                                'type'  => 'warning'
                            ])->render();
                        }
                        return $res;
                    }
            ]
        );

        $params['recordsTotal'] = $query->count();
        $params['query']        = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }

    /**
     * Sweet Alert Model
     *
     * @param $params
     *
     * @return mixed
     */
    private function ajaxAlert_delete($params)
    {
        $contact_us_record = $this->model_repo->find($params['id']);
        $site_id           = SiteConnectionLib::getSite()->id;
        $text              = "Are You sure you want to Delete {$contact_us_record->name} From your Records?";

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', $text)
            ->with('route', 'sites.{site}.contact-us.destroy')
            ->with('route_params', [$site_id, $contact_us_record->id])
            ->with('action', 'delete');
    }
}
