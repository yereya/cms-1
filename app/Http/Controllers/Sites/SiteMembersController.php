<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Member;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SiteMembersFormRequest;
use App\Http\Traits\AjaxModalTrait;
use SiteConnectionLib;

/**
 * Class SiteMembersController
 *
*@package App\Http\Controllers\Sites
 */
class SiteMembersController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'top.members';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Members Management';

    /**
     * Display a listing of the resource.
     *
     * @param $site_id

     *
*@return \Illuminate\View\View
     */
    public function index($site_id)
    {
        $site    = SiteConnectionLib::getSite();
        $members = $site->members()->get();

        return view('pages.site.members.index')
            ->with(compact('site', 'members'))
            ->with('can', $this->buildCanPermissions());
    }

    /**
     * Create
     *
     * @param $site_id

     *
*@return \Illuminate\View\View
     */
    public function create($site_id)
    {
        $site          = SiteConnectionLib::getSite();
        $member_groups = $site->memberGroups()->orderBy('name')->pluck('name', 'id');

        return view('pages.site.members.form')
            ->with(compact('site', 'member', 'member_groups'));
    }

    /**
     * Store

     *
*@param  SiteMembersFormRequest $request
     * @param                         $site_id

     *
*@return \Illuminate\Http\Response
     */
    public function store(SiteMembersFormRequest $request, $site_id)
    {
        $site   = SiteConnectionLib::getSite();
        $member = new Member($request->all());
        $member->save();

        if ($request->get('create_another')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["success" => "The member {$member->email} was saved"]);
        }

        return redirect()->route('sites.{site}.members.index', $site->id)
            ->with(self::MESSAGE_TOAST, ["success" => "The member {$member->email} was saved"]);
    }

    /**
     * Edit
     *
     * @param            $site_id
     * @param Member     $member
     *
     * @return \Illuminate\View\View
     * @internal param $member_id
     */
    public function edit($site_id, Member $member)
    {
        $site = SiteConnectionLib::getSite();
        $member->load('memberGroups');
        $member_groups = $site->memberGroups()
            ->orderBy('name')
            ->pluck('name', 'id');

        return view('pages.site.members.form')
            ->with(compact('site', 'member', 'member_groups', 'member_groups_selected'));
    }

    /**
     * Update
 
*
*@param SiteMembersFormRequest       $request
     * @param                        $site_id
     * @param Member                 $member
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SiteMembersFormRequest $request, $site_id, Member $member)
    {
        $site = SiteConnectionLib::getSite();
        $member->fill($request->all());
        $member->save();

        if ($request->get('create_another')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["success" => "The member {$member->email} was saved"]);
        }

        return redirect()->route('sites.{site}.members.index', $site->id)
            ->with(self::MESSAGE_TOAST, ["success" => "The member {$member->email} was saved"]);
    }
}
