<?php

namespace App\Http\Controllers\Sites;

use App\Libraries\PageInfo\PagesUrls;
use Illuminate\View\View;
use Illuminate\Http\Request;
use App\Entities\Models\Sites\Site;
use App\Http\Controllers\Controller;
use App\Entities\Repositories\Sites\SettingsRepo;

class GeneralSettingsController extends Controller
{

    /**
     * @ver array
     */
    protected $assets = [
        'top-app',
        'assets/global/plugins/sweet-alert-2/sweetalert2.min.js',
        'assets/global/plugins/sweet-alert-2/sweetalert2.min.css'
    ];

    /**
     * Render the general settings page.
     *
     * @param Site $site
     *
     * @return View
     */
    public function render(Site $site)
    {
        $settings = SettingsRepo::getSettingsWithExtraParams($site->id, ['site','pages'])
            ->keyBy('key');

        $pages = (new PagesUrls())
            ->resolveOnlyContentTypePages()
            ->sortBy('text')
            ->pluck('text','id');

        return view('pages.site.settings.general')
            ->with('site', $site)
            ->with('type', 'site')
            ->with('settings', $settings)
            ->with('pages',$pages);
    }

    /**
     * Update site settings.
     *
     * @param Site    $site
     * @param Request $request
     *
     * @return Response
     */
    public function update(Site $site, Request $request)
    {
        $settingsToUpdate = request()->get('settings');
        $types = ['site','pages'];
        SettingsRepo::saveSettings($site->id, $types , $settingsToUpdate);

        return redirect()
            ->back()
            ->with(self::MESSAGE_TOAST, ["success" => "The setting has been saved"]);
    }

}