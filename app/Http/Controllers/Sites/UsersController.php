<?php namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Site;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\Sites\SitesFormRequest;
use App\Http\Traits\AjaxModalTrait;
use Illuminate\Http\Request;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers\Sites
 */
class UsersController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'top.users';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Users Management';

    /**
     * Display a listing of the resource.
     *
     * @param int $site_id
     *
     * @return \Illuminate\View\View
     */
    public function index($site_id)
    {
        $site  = Site::findOrFail($site_id);
        $users = $site->users;

        return view('pages.site.users.index')->with(compact('site', 'users'));
    }

    /**
     * Create
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pages.site.form');
    }

    /**
     * Store
     *
     * @param  SitesFormRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(SitesFormRequest $request)
    {
        $site = new Site($request->all());

        $connection = SiteConnectionLib::getFacadeRoot();
        $connection->createConnection($site->db_name);
        $connection->testConnection();

        $site->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The site {$site->name} added"]);
        }

        return redirect()->route('sites.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The site {$site->name} added"]);
    }

    /**
     * Edit
     *
     * @param int $site_id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($site_id)
    {
        $site = Site::findOrFail($site_id);

        return view('pages.site.form', compact('site'));
    }

    /**
     * Store
     *
     * @param  SitesFormRequest $request
     * @param  int              $site_id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(SitesFormRequest $request, $site_id)
    {
        $site = Site::findOrFail($site_id);
        $site->fill($request->all());

        $connection = SiteConnectionLib::getFacadeRoot();
        $connection->testConnection();

        $site->save();

        if ($request->get('create_another')) {
            return redirect()->back()->with(self::MESSAGE_TOAST, ["success" => "The site {$site->name} added"]);
        }

        return redirect()->route('sites.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The site {$site->name} added"]);
    }

    /**
     * Destroy
     *
     * @param Request $request
     * @param int     $site_id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $site_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                             ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        $site = Site::findOrFail($site_id);
        $site->delete();

        return redirect()->route('sites.index')
                         ->with(self::MESSAGE_TOAST, ["success" => "The Site {$site->name} was deleted"]);
    }

    /**
     * Ajax Modal - Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxModal_deleteSite($params, $extra_params)
    {
        $site_id = $extra_params[0]; //get the site id from the passed params

        return view('pages.delete-confirmation-modal')->with('title', 'Delete site')->with('btn_title', 'Delete site')
                                                      ->with('route', ['sites.destroy', $site_id]);
    }
}
