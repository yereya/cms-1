<?php

namespace App\Http\Controllers\Sites;

use App\Entities\Models\Sites\Comment;
use App\Entities\Models\Sites\ContentTypes\SiteContentType;
use App\Entities\Models\Sites\Site;
use App\Entities\Repositories\Sites\CommentRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\ChoiceTrait;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Datatable\DatatableLib;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/**
 * Class SiteContentTypeCommentsController
 *
 * @package App\Http\Controllers\Sites
 */
class SiteCommentsController extends Controller
{
    use AjaxModalTrait;
    use DataTableTrait;
    use ChoiceTrait;

    const PERMISSION = 'top.comments';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'top-app'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Site Comments Management';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $can           = $this->buildCanPermissions();
        $site          = SiteConnectionLib::getSite();
        $content_types = SiteContentType::where('site_id', $site->id)
            ->get()
            ->pluck('name', 'id');

        return view('pages.site.comments.index')
            ->with(compact('site', 'content_types', 'can'));
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $site      = SiteConnectionLib::getSite();
        $replay_to = request()->get('replay_to');
        if ($replay_to) {
            $comment_to_replay = Comment::find($replay_to);
            $post_name         = $comment_to_replay->post->pluck('name', 'id');

        } else {
            $content_types = SiteContentType::where('site_id', $site->id)
                ->get()->pluck('name', 'id');
        }

        $status_options = Comment::STATUS_OPTIONS;

        $types = CommentRepo::TYPES;

        return view('pages.site.comments.form')
            ->with(compact('site', 'content_types', 'post_name',
                'comment_to_replay', 'status_options', 'types'));
    }

    /**
     * Store
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $site                = SiteConnectionLib::getSite();
        $comment             = new Comment($this->buildFillableParams($request));
        $comment->session_id = session()->getId();
        $comment->user_id    = auth()->user()->id;

        $comment->save();

        return redirect()
            ->route('sites.{site}.comments.edit', [$site->id, $comment->id])
            ->with(self::MESSAGE_TOAST, ["success" => "New comment has been created"]);
    }

    /**
     * Update
     *
     * @param Site    $site
     * @param Comment $comment
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Site $site, Comment $comment, Request $request)
    {
        $site       = SiteConnectionLib::getSite();

        $comment->fill($request->all());
        $comment->save();

        return redirect()
            ->route('sites.{site}.comments.edit', [$site->id, $comment->id])
            ->with(self::MESSAGE_TOAST, ["success" => "The comment has been update"]);
    }

    /**
     * Edit
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        $site       = SiteConnectionLib::getSite();
        $comment_id = Route::getCurrentRoute()->getParameter('comments');
        $comment    = Comment::find($comment_id);
        if ($comment->parent_id) {
            $comment_to_replay = Comment::find($comment->parent_id);
            $post_name         = $comment_to_replay->post->pluck('name', 'id');
        }

        $status_options = Comment::STATUS_OPTIONS;

        return view('pages.site.comments.form')
            ->with(compact('site', 'comment', 'post_name',
                'comment_to_replay', 'status_options'));
    }

    private function buildFillableParams($request)
    {
        $params = $request->only('post_id', 'email', 'author', 'content', 'status', 'visible', 'rating',
            'parent_id', 'type');

        $params['visible'] = isset($params['visible']) ? 1 : 0;

        return $params;
    }

    /**
     * Choice Comment Type
     *
     * @param Request $request
     */
    public function choice_type(Request $request)
    {
        $type = $request->get('type');

        $fields = CommentRepo::CONTENT_FIELDS_BY_TYPE[$type];

        return view('pages.site.comments.type-fields')
            ->with(compact('fields'));
    }

    /**
     * Data Table Comments
     *
     * @param              $params
     * @param DatatableLib $datatable
     *
     * @return array|null
     */
    private function dataTable_list($params, DatatableLib $datatable)
    {
        $can  = $this->buildCanPermissions();
        $site = SiteConnectionLib::getSite();

        $datatable->ignoreFilters(['content_type_id']);

        $query = (new Comment)->newQuery();

        $params['recordsTotal'] = $query->count();
        $params['query']        = $query;

        $datatable->addColumn('id');

        $columns = [
            'id'        => 'id',
            'post_id'   => 'post_id',
            'email'     => 'email',
            'author'    => 'author',
            'content'   => [
                'db_name'  => 'content',
                'callback' => function ($comment) {
                    $result = '';
                    foreach ($comment['content'] as $field => $comment_content) {
                        $result .= $field . ' => ' . $comment_content . '<br>';
                    }

                    return $result;
                }
            ],
            'status'    => [
                'db_name'  => 'status',
                'callback' => function ($obj) {
                    $status = Comment::STATUS_OPTIONS[$obj['status']];
                    $class  = Comment::STATUS_OPTIONS_CLASSES[$obj['status']];

                    return \View::make('partials.fields.status-label', [
                        'text'  => $status,
                        'class' => $class,
                    ])->render();
                }
            ],
            'visible'   => [
                'db_name'  => 'visible',
                'callback' => function ($obj) {
                    $visibility = Comment::VISIBILITY_OPTIONS[$obj['visible']];
                    $class      = Comment::VISIBILITY_OPTIONS_CLASSES[$obj['visible']];

                    return \View::make('partials.fields.status-label', [
                        'text'  => $visibility,
                        'class' => $class,
                    ])->render();
                }
            ],
            'rating'    => 'rating_overall',
            'replay_to' => 'parent_id',
            'actions'   => function ($data) use ($site, $can) {

                return \View::make('pages.site.comments.table-actions',
                    ['site_id' => $site->id, 'data' => $data, 'can' => $can])
                    ->render();
            },
        ];

        $datatable->renderColumns($columns);

        return $datatable->fill($params);
    }
}
