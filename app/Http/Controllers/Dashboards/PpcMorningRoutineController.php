<?php


namespace App\Http\Controllers\Dashboards;


use App\Entities\Repositories\Bo\AccountsRepo;
use App\Http\Controllers\Controller;
use App\Http\Traits\UserActionFeedbackTrait;
use App\Http\Traits\StatsTrait;

/**
 * Class PpcMorningRoutineController
 *
 * @package App\Http\Controllers\Dashboards
 */
class PpcMorningRoutineController extends Controller
{
    use StatsTrait;
    use UserActionFeedbackTrait;

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'top-app'];

    /**
     *  Permission name
     */
    const PERMISSION = 'dashboards.ppc-morning-routine';

    /**
     * @return $this
     */
    public function index()
    {
        $accounts = (new AccountsRepo())->getActiveUserAccounts();

        return view('pages.dashboards.ppc.morning-routine.index')
            ->with(compact('accounts'));
    }
}