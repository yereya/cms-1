<?php


namespace App\Http\Controllers\Dashboards;


use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Bo\Advertisers\TargetsRepo;
use App\Entities\Repositories\Dwh\DashDailyStatsRepo;
use App\Http\Controllers\Controller;
use App\Http\Traits\StatsTrait;

class TargetPpcController extends Controller
{
    use StatsTrait;

    protected $assets = ['form'];

    const PERMISSION = 'dashboards.advertisers-targets';

    /**
     * Index
     *
     * @return string
     */
    public function index()
    {

        $model_repo     = new TargetsRepo();
        $accounts = \DB::connection('bo')->table('accounts')
                        ->whereIn('id',session('user_account_access'))
                        ->get()->pluck('name','source_account_id')->toArray();
        $advertisers = \DB::connection('bo')->table('advertisers')
                            ->whereIn('id',session('user_advertiser_access'))
                            ->get()->pluck('name','id')->toArray();
        $sale_groups    = (new AccountsRepo())->getSalesGroup()->pluck('sale_group','sale_group');
        $business_group = $model_repo->getBusinessGroups();
        $traffic_source        = array( 'All' => 'All', 'Search' => 'Search', 'Without Search' => 'Without Search');

        // --- old ---
        //$accounts1      = (new DashDailyStatsRepo())->getAccounts()->pluck('account', 'account_id');
        //$advertisers    = $model_repo->getAdvertisers()->pluck('advertiser_name', 'advertiser_id');
        //$verticals      = $model_repo->getVerticals()->pluck('vertical', 'vertical');

        return view('pages.dashboards.ppc.advertiser-target.index')
            ->with(compact('accounts','advertisers','verticals','sale_groups','business_group','traffic_source'));


    }
}
