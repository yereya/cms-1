<?php

namespace App\Http\Controllers\Dashboards;

use App\Entities\Models\Users\User;
use App\Entities\Repositories\Alerts\DynamicRepo;
use App\Entities\Repositories\AlertStateRepo;
use App\Entities\Repositories\Bo\AccountsRepo;
use App\Entities\Repositories\Bo\Advertisers\TargetsRepo;
use App\Entities\Repositories\Dwh\DashDailyStatsRepo;
use App\Entities\Repositories\Users\UserRepo;
use App\Facades\SiteConnectionLib;
use App\Http\Controllers\Controller;
use App\Http\Requests\AlertStateRequest;
use App\Http\Traits\Select2Trait;
use App\Http\Traits\StatsTrait;
use App\Libraries\Dashboards\Controllers\TableStats;
use Assets;
use Illuminate\Http\Request;
use App\Entities\Repositories\Bo\Advertisers\ManagerTargetRepo;

/**
 * Class ManagerTargetController
 *
 * @package App\Http\Controllers\Dashboard
 */
class ManagerTargetController extends Controller
{
    use StatsTrait;
    use Select2Trait;


    /**
     * @var string $assets
     */
    protected $assets = ['form', 'top-app'];

    const PERMISSION = 'dashboards.target_manager';


    /**
     * @return $this
     */
    public function index(Request $request)
    {

        $dashboard_repo = new TargetsRepo();
        $accounts = (new DashDailyStatsRepo())->getAccounts()->pluck('account', 'account_id');
        $verticals = $dashboard_repo->getVerticals()->pluck('vertical', 'vertical');
        $advertisers = $dashboard_repo->getAdvertisers()->pluck('advertiser_name', 'advertiser_id');

        return view('pages.dashboards.ppc.manager-target.index')
            ->with(compact('accounts', 'verticals', 'advertisers'));
    }

}