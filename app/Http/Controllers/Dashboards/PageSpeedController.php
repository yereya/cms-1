<?php

namespace App\Http\Controllers\Dashboards;


use App\Console\Commands\CheckSitesPageSpeed;
use App\Entities\Repositories\AlertStateRepo;
use App\Entities\Repositories\PageSpeedRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests\AlertStateRequest;
use App\Http\Traits\Select2Trait;
use App\Http\Traits\StatsTrait;
use Artisan;
use Illuminate\Http\Request;

/**
 * Class ManagerTargetController
 *
 * @package App\Http\Controllers\Dashboard
 */
class PageSpeedController extends Controller
{
    use StatsTrait;
    use Select2Trait;


    /**
     * @var string $assets
     */
    protected $assets = ['form', 'top-app'];

    const PERMISSION = 'dashboards.page_speed';

    protected const ERROR_MESSAGE = 'Some error occurs ,please contact with your admin.';
    protected const SUCCESS_MESSAGE = 'Congratulation you been succeeded.';


    /**
     * @return $this
     */
    public function index()
    {
        return view('pages.dashboards.ppc.page-speed.index');
    }


    /**
     * @return mixed
     *  Refresh the scores
     */
    public function refreshFunc()
    {
        $exit_code = Artisan::call('check:sites:page_speed');

        if (!$exit_code) {
            return self::SUCCESS_MESSAGE;
        }

        return self::ERROR_MESSAGE;
    }
}