<?php

namespace App\Http\Controllers;

use App\Entities\Models\Users\User;
use App\Entities\Repositories\Sites\EntityLockRepo;
use Illuminate\Http\Request;
use View;

class EntityLockController extends Controller
{

    const PERMISSION = 'entity_lock';

    /**
     * Create
     *
     * @param Request $request
     * @return string
     */
    public function create(Request $request)
    {
        $params = $request->all();
        if (empty($params['entity_id']) || empty($params['type'])) {

            return $this->getJsonResult(EntityLockRepo::LOCK_CREATED, "Parameters are missing");
        }

        $hard_lock = $params['hard_lock'] ?? null;
        $site_id = $params['site_id'] ?? null;

        if ($hard_lock) {

            return $this->createHardLock($params['type'], $params['entity_id'], $site_id);
        } else {

            return $this->createLock($params['type'], $params['entity_id'], $site_id);
        }
    }

    /**
     * Create Lock
     *
     * @param $type
     * @param $entity_id
     * @param $site_id
     * @return string
     */
    private function createLock($type, $entity_id, $site_id)
    {
        $entity_lock_repo = new EntityLockRepo();
        $user = auth()->user();
        $lock_result = $entity_lock_repo->createLock($user, $type, $entity_id, $site_id);

        return $this->getResultByStatus($lock_result);
    }

    /**
     * Create Hard lock
     *
     * @return string
     */
    private function createHardLock($type, $entity_id, $site_id)
    {
        $entity_lock_repo = new EntityLockRepo();
        $user = auth()->user();
        if (auth()->user()->can(self::PERMISSION . '.hard_lock.add')) {
            $lock_result = $entity_lock_repo->createHardLock($user, $type, $entity_id, $site_id);

            return $this->getResultByStatus($lock_result);
        } else {

            return $this->getJsonResult(EntityLockRepo::LOCK_ERROR, 'You don\'t have permission for hard lock');
        }
    }

    /**
     * Get Result by Status
     *
     * @param $lock_result
     * @return string
     */
    private function getResultByStatus($lock_result)
    {
        if ($lock_result['status'] == EntityLockRepo::LOCK_CREATED) {

            return $this->getJsonResult(EntityLockRepo::LOCK_CREATED);
        } else {

            return $this->getLockMessage($lock_result['lock']);
        }

    }

    /**
     * Get Lock Message
     *
     * @param $lock
     * @return string
     */
    private function getLockMessage($lock)
    {
        $user = User::find($lock->user_id);
        $msg = View::make('pages.entity-lock.lock-error', [
            'user' => $user,
            'hard_lock_permission' => auth()->user()->can(self::PERMISSION . '.hard_lock.add')

        ])->render();

        return $this->getJsonResult(EntityLockRepo::LOCK_ERROR, $msg);
    }

    /**
     * Get Result
     *
     * @param $status
     * @param string $msg
     * @return string
     */
    private function getJsonResult($status, $msg = '')
    {
        return json_encode([
            'status' => $status,
            'desc' => $msg
        ]);
    }
}
