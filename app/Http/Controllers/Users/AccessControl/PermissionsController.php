<?php namespace App\Http\Controllers\Users\AccessControl;

use App\Entities\Models\Users\AccessControl\Permission;
use App\Entities\Models\Users\AccessControl\Role;
use App\Entities\Models\Users\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionsFormRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\ToggleProperty;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


/**
 * Class PermissionsController
 *
 * @package App\Http\Controllers
 */
class PermissionsController extends Controller
{
    use ToggleProperty;
    use AjaxAlertTrait;

    const PERMISSION = 'permissions';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Permissions Management';

    /**
     * Index
     *
     * @return \Illuminate\View\View
     */
    function index()
    {
        return view('pages.permissions.index')
            ->with('permissions', Permission::all());
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('pages.permissions.form');
    }

    /**
     * Store
     *
     * @param PermissionsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PermissionsFormRequest $request)
    {
        $permission = Permission::create($request->all());

        //Updates the permissions_role pivot table
        if (is_array($request->get('role_id'))) {
            $permissions = [];
            foreach ($request->input('role_id') as $role_id) {
                $permissions[] = ['role_id' => $role_id, 'permission_id' => $permission->id];
            }
            DB::table('user_permission_role')->Insert($permissions);
        }

        //Redirect back to create when create_another is set
        if ($request->get('create_another'))
            return redirect()->route('permissions.create')
                ->with('toastr_msgs', ["success" => "Permission {$request->get('name')}.{$request->get('type')} added"]);

        return redirect()->route('permissions.index')
            ->with('toastr_msgs', ["success" => "Permission {$request->get('name')}.{$request->get('type')} added"]);
    }

    /**
     * Edit
     *
     * @param int $permission_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function edit($permission_id)
    {
        $permission = Permission::find($permission_id);

        if (!$permission) {
            throw new \Exception('the permission you have provided does not exists');
        }

        return view('pages.permissions.form')
            ->with('permission', $permission);
    }

    /**
     * Update
     *
     * @param PermissionsFormRequest $request
     * @param int                    $permission_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(PermissionsFormRequest $request, $permission_id)
    {
        $permission = Permission::find($permission_id);

        if (!$permission) {
            throw new \Exception('the permission you have provided does not exists');
        }

        $params = $request->all();
        $params['roles_id'] = $params['roles_id'] ?? [];

        $permission->fill($params);
        $permission->save();

        return redirect()->route('permissions.index')
            ->with('toastr_msgs', ["success" => "Permission {$request->get('name')}.{$request->get('type')} updated"]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $permission_id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $permission_id)
    {
        if (auth()->user()->can(self::PERMISSION.'.delete')) {
            Permission::destroy($permission_id);
            return redirect()->route('permissions.index')
                ->with('toastr_msgs', ["success" => "Permission deleted"]);
        }
        return redirect()->route(self::PERMISSION.'.index')
            ->with('toastr_msgs', ["warning" => "You do not have permissions to do this."]);
    }





    /**
     * Allow to view which permission is user has
     * returned as a ajax modal
     *
     * @param $user_id
     * @return $this
     */
    public function user($user_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.view'))
            return view('partials.containers.error-modal')
                ->with('error_msg', 'You do not have the permission to view this page.');

        $special_permissions = User::find($user_id)
            ->specialPermissions()
            ->get();

        $roles = User::find($user_id)
            ->roles()
            ->with('permissions')
            ->get();

        $user_permissions = new Collection();
        foreach($roles as $role) {

            foreach($role->permissions as $permission) {
                $user_permissions->push($permission);
            }

            foreach($special_permissions as $special_permission) {
                $user_permissions->push($special_permission);
            }
        }

        return view('pages.permissions.user-modal')
            ->withUser(User::find($user_id))
            ->withRoles($roles->keyBy('id'))
            ->withUserPermissions($user_permissions->keyBy('id'))
            ->withPermissions(Permission::all());
    }
    
    
    public function rolePermissionsModal($role_id)
    {
        $role_permissions = Role::where('id', $role_id)->with('permissions')->first();

        return view('pages.permissions.role-permissions-modal')
            ->with('role_permissions', $role_permissions->permissions->keyBy('id'))
            ->with('role_id', $role_id)
            ->with('permissions', Permission::all());
    }


    private function toggleCheckbox_rolePermissions(Request $request, $params)
    {
        $result = [
            'state' => 0,
            'desc' => 'You are not permitted to do this or account does not exists.',
            'toggleState' => 0
        ];

        //Validation
        if (!auth()->user()->can(self::PERMISSION . '.modal[role_permission].edit')) {

            return $result;
        }

        $permission = Permission::find($request->input('permission_id'));
        if (!$permission) {
            throw new \Exception('the permission you have provided does not exists');
        }
        
        $role_permission = DB::table('user_permission_role')->where('permission_id', $request->get('permission_id'))->where('role_id', $request->get('role_id'))->first();

        $result['desc'] = 'Change saved.';
        $result['state'] = 1;
        if (count($role_permission)) {
            DB::table('user_permission_role')->where('permission_id', $request->get('permission_id'))->where('role_id', $request->get('role_id'))->delete();
            $result['toggleState'] = 0;
            $result['desc'] = "Detach permission: $permission->name";
        } else {
            $permissions[] = ['role_id' => $request->get('role_id'), 'permission_id' => $request->get('permission_id')];
            DB::table('user_permission_role')->Insert($permissions);
            $result['toggleState'] = 1;
            $result['desc'] = "Attached permission: $permission->name";
        }

        return $result;
    }
    
    
    /**
     * Ajax Modal Delete Role
     * Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    private function ajaxAlert_deletePermission($params, $extra_params)
    {
        $permission_id = $extra_params[0]; //get the permission id from the passed params

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', 'Please confirm deleting permission #'.$permission_id)
            ->with('route', 'permissions.destroy')
            ->with('action', 'delete')
            ->with('route_params', $permission_id);
    }
}