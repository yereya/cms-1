<?php namespace App\Http\Controllers\Users\AccessControl;

use App\Entities\Models\Sites\Site;
use App\Entities\Models\Sites\SitesModel;
use App\Entities\Models\Users\AccessControl\Permission;
use App\Entities\Models\Users\AccessControl\Role;
use App\Entities\Models\Users\AccessControl\SiteUserRole;
use App\Entities\Models\Users\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\RolesFormRequest;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\ToggleProperty;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class RolesController
 *
 * @package App\Http\Controllers
 */
class RolesController extends Controller
{
    use ToggleProperty;
    use AjaxAlertTrait;

    const PERMISSION = 'roles';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Roles Management';

    /**
     * Index
     *
     * @return \Illuminate\View\View
     */
    function index()
    {
        return view('pages.roles.index')
            ->with('roles', Role::all());
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $permissions = Permission::select('id', DB::raw('CONCAT(name, ".", type) as text'))->get();
        $permissions = arrayKeyValue2ArrayKey($permissions, 'id', 'text');

        return view('pages.roles.form')
            ->with('permissions', $permissions);
    }

    /**
     * Store: the form-duplicate is also submitted to this method
     *
     * @param RolesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RolesFormRequest $request)
    {
        $role_id = Role::create($request->all());

        //Updates the permissions_role pivot table
        if (is_array($request->get('permission_id'))) {
            $permissions = [];
            foreach ($request->input('permission_id') as $permission_id) {
                $permissions[] = ['role_id' => $role_id->id, 'permission_id' => $permission_id];
            }
            DB::table('user_permission_role')->Insert($permissions);
        }

        //Redirect back to create when create_another is set
        if ($request->get('create_another'))
            return redirect()->route('roles.create')
                ->with('success', true)
                ->with('toastr_msgs', ["success" => "Role {$request->get('name')} added"]);

        return redirect()->route('roles.index')
            ->with('success', true)
            ->with('toastr_msgs', ["success" => "Role {$request->get('name')} added"]);
    }

    /**
     * Edit
     *
     * @param int $role_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function edit($role_id)
    {
        $role = Role::find($role_id);
        $permissions = Permission::select('id', DB::raw('CONCAT(name, ".", type) as text'))
            ->orderBy('text')
            ->get();
        $permissions = arrayKeyValue2ArrayKey($permissions, 'id', 'text');

        if (!$role) {
            throw new \Exception('the role you have provided does not exists');
        }

        return view('pages.roles.form')
            ->with('role', $role)
            ->with('permissions', $permissions);
    }

    /**
     * Update
     *
     * @param RolesFormRequest $request
     * @param int              $role_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(RolesFormRequest $request, $role_id)
    {
        $role = Role::find($role_id);

        if (!$role) {
            throw new \Exception('the role you have provided does not exists');
        }

        $role->fill($request->all());
        $role->save();

        //Updates the permissions_role pivot table
        if (is_array($request->get('permission_id'))) {
            DB::table('user_permission_role')->where('role_id', $role_id)->delete();
            $permissions = [];
            foreach ($request->input('permission_id') as $permission_id) {
                $permissions[] = ['role_id' => $role_id, 'permission_id' => $permission_id];
            }
            DB::table('user_permission_role')->Insert($permissions);
        }


        return redirect()->route('roles.index')
            ->with('toastr_msgs', ["success" => "Role edited"]);
    }

    /**
     * Duplicates the role: opens same form as edit form, but submits it to roles.store
     *
     * @param int $role_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function duplicate($role_id)
    {
        $role = Role::find($role_id);
        $permissions = Permission::select('id', DB::raw('CONCAT(name, ".", type) as text'))->get();
        $permissions = arrayKeyValue2ArrayKey($permissions, 'id', 'text');

        if (!$role) {
            throw new \Exception('the role you have provided does not exists');
        }

        return view('pages.roles.duplicate')
            ->with('role', $role)
            ->with('permissions', $permissions);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $role_id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $role_id)
    {
        if (auth()->user()->can(self::PERMISSION.'.delete')) {
            Role::destroy($role_id);
            return redirect()->route(self::PERMISSION.'.index')->with('success', true)
                ->with('toastr_msgs', ["success" => "Role deleted"]);
        }
        return redirect()->route('roles.index')
            ->with('toastr_msgs', ["warning" => "You do not have permissions to do this."]);
    }


    /**
     * Allow to view and toggle Roles for users
     * returned as a ajax modal
     *
     * @param $user_id
     * @return $this
     */
    public function user($user_id)
    {
        if (!auth()->user()->can(self::PERMISSION . '.view'))
            return view('partials.containers.error-modal')
                ->with('error_msg', 'You do not have the permission to view this page.');

        $user_roles = $this->getUserRoles($user_id);

        $site_roles = [];
        foreach (Site::all()->toArray() as $site) {
            foreach (Role::all()->toArray() as $role) {
                $site_roles[] = $role;
                $site_roles[count($site_roles)-1]['site_id'] = $site['id'];
                $site_roles[count($site_roles)-1]['site_name'] = $site['name'];

                //Check if current user has this role
                foreach ($user_roles as $user_role) {
                    if ($user_role->site_id == $site['id'] && $user_role->role_id == $role['id']) {
                        $site_roles[count($site_roles)-1]['user_has_role'] = true;
                    }
                }
            }
        }

        return view('pages.users.roles-modal')
            ->with('user_roles', $user_roles)
            ->with('sites_roles', $site_roles)
            ->with('user_id', $user_id);
    }


    public function property_toggle(Request $request, $role_id)
    {
        $state = 0;
        $toggle_state = 0;
        switch ($request->input('action')) {
            case 'user_site_role' :
                if (!auth()->user()->can(self::PERMISSION .'.assign.edit') && !$request->input('site_id') OR !$request->input('user_id'))
                    break;

                $state = 1;
                $site_user_role = SiteUserRole::where('role_id', $role_id)
                    ->where('site_id', $request->input('site_id'))
                    ->where('user_id', $request->input('user_id'))
                    ->get();
                if ($site_user_role->isEmpty()) {
                    $site_user_role = new SiteUserRole();
                    $site_user_role->site_id = $request->input('site_id');
                    $site_user_role->user_id = $request->input('user_id');
                    $site_user_role->role_id = $role_id;
                    $site_user_role->save();

                    //change the toggle state to checked
                    $toggle_state = 1;
                }
                else {
                    SiteUserRole::where('role_id', $role_id)
                        ->where('site_id', $request->input('site_id'))
                        ->where('user_id', $request->input('user_id'))
                        ->delete();
                }

                break;
        }

        return [
            'status' => $state,
            'desc' => '',
            'toggleState' => $toggle_state
        ];
    }


    /**
     * ToogleCheckbox property toggler
     * which toggles the user having a certain role
     *
     * @param $request
     * @param $params
     * @return array
     */
    private function toggleCheckbox_user_site_role($request, $params) {
        $status = 1;
        $desc = '';
        $toggle_state = 0;

        $site_user_role = SiteUserRole::where('role_id', $request->input('role_id'))
            ->where('site_id', $request->input('site_id'))
            ->where('user_id', $request->input('user_id'))
            ->get();

        if ($site_user_role->isEmpty()) {
            $site_user_role = new SiteUserRole();
            $site_user_role->site_id = $request->input('site_id');
            $site_user_role->user_id = $request->input('user_id');
            $site_user_role->role_id = $request->input('role_id');
            $site_user_role->save();

            $toggle_state = 1;
            $desc = 'Role added.';
        }
        else {
            SiteUserRole::where('role_id', $request->input('role_id'))
                ->where('site_id', $request->input('site_id'))
                ->where('user_id', $request->input('user_id'))
                ->delete();
            $desc = 'Role removed.';
        }

        return [
            'status' => $status,
            'desc' => $desc,
            'toggleState' => $toggle_state
        ];
    }


    /**
     * Ajax Modal Delete Role
     * Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    private function ajaxAlert_deleteRole($params, $extra_params)
    {
        $role_id = $extra_params[0]; //get the role id from the passed params

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', 'Please confirm deleting role #'.$role_id)
            ->with('route', 'roles.destroy')
            ->with('action', 'delete')
            ->with('route_params', $role_id);
    }

    /**
     * Get site user object
     * @param $user_id
     *
     * @return Collection
     */
    private function getUserRoles($user_id) {
        $site_user_roles = SiteUserRole::where('user_id', $user_id)
            ->with('roles')
            ->with('users')
            ->get();

        $sites = Site::whereIn('id', $site_user_roles->pluck('site_id')
            ->toArray())->get()->keyBy('id');

        $user_roles = new Collection();

        foreach ($site_user_roles as $sur) {
            $user = $sur->users->first();
            $role = $sur->roles->first();

            /*Enter only if the site is found*/
            if($sites && !empty($sites->get($sur->site_id)->name)){
                $collect = new Collection();
                $collect->site_id = $sur->site_id;
                $collect->user_id = $sur->user_id;
                $collect->role_id = $sur->role_id;
                $collect->username = $user->username;
                $collect->role_name = $role->name;
                $collect->site_name = $sites->get($sur->site_id)->name;
                $user_roles->put($sur->site_id, $collect);
            }
        }

        return $user_roles;
    }
}