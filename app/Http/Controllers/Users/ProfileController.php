<?php namespace App\Http\Controllers\Users;

use App\Entities\Models\Users\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProfileFormRequest;

/**
 * Class ProfilesController
 *
 * @package App\Http\Controllers
 */
class ProfileController extends Controller
{
    const PERMISSION = 'users.profile';

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'page-profile'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'My Profile';

    /**
     * Index
     *
     * @param $user_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    function index($user_id)
    {
        $user = User::find($user_id);

        if (!$user) {
            throw new \Exception('the user you have provided does not exists');
        }

        // Set default thumbnail when nothing is set
        if (!$user->thumbnail) {
            $user->thumbnail = '/assets/global/img/thumbnail/MargeSimpson5.gif';
        }

        return view('pages.users.profile.index')
            ->with('user', $user);
    }


    /**
     * Update
     *
     * @param ProfileFormRequest $request
     * @param int $user_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(ProfileFormRequest $request, $user_id)
    {
        // Make sure the user is not trying to edit
        // another user without being permitted to
        if($user_id != auth()->user()->id && !auth()->user()->can(self::PERMISSION.'others.view')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You are trying to do an action you are not permitted to."]);
        }

        $request = $request->all();
        $user = User::find($user_id);

        if (!$user) {
            throw new \Exception('the user you have provided does not exists');
        }

        $user->fill($request);
        $user->save();

        return redirect()->back()
            ->with(self::MESSAGE_TOAST, ["success" => "Profile updated"]);
    }
}