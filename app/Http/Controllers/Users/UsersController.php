<?php namespace App\Http\Controllers\Users;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Users\AccessControl\AssignedAccount;
use App\Entities\Models\Users\AccessControl\SiteUserPermission;
use App\Entities\Models\Users\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\UsersFormRequest;
use App\Http\Traits\Select2Trait;
use App\Http\Traits\ToggleProperty;
use Illuminate\Http\Request;

/**
 * Class UsersController
 *
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    use ToggleProperty;
    use Select2Trait;

    const PERMISSION = 'users';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Users Management';

    /**
     * Index
     *
     * @return \Illuminate\View\View
     */
    function index()
    {
        return view('pages.users.index')
            ->with('users', User::all())->with("userLogsPermissions", auth()->user()->can('users.logs.view'));
    }

    /**
     * Create
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $default_password_expiry_date = date('Y-m-d', strtotime('+' . config('auth.passwords.users.expire') . ' days'));
        
        return view('pages.users.form')
            ->with('default_password_expiry_date', $default_password_expiry_date);
    }

    /**
     * Store
     *
     * @param UsersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UsersFormRequest $request)
    {
        $req = $request->all();
        User::create($req);

        //Redirect back to create when create_another is set
        if ($request->get('create_another'))
            return redirect()->route('users.create')
                ->with('toastr_msgs', ["success" => "User {$req['first_name']} {$req['last_name']} added"]);

        return redirect()->route('users.index')
            ->with('toastr_msgs', ["success" => "User {$req['first_name']} {$req['last_name']} added"]);
    }

    /**
     * Edit
     *
     * @param int $user_id
     *
     * @return \Illuminate\View\View
     * @throws \Exception
     */
    public function edit($user_id)
    {
        $user = User::find($user_id);

        if (!$user) {
            throw new \Exception('the user you have provided does not exists');
        }

        return view('pages.users.form')
            ->with('user', $user);
    }

    /**
     * Update
     *
     * @param UsersFormRequest $request
     * @param int $user_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(UsersFormRequest $request, $user_id)
    {
        $req = $request->all();
        $user = User::find($user_id);

        if (!$user) {
            throw new \Exception('the user you have provided does not exists');
        }

        $user->fill($req);
        $user->save();

        return redirect()->back()
            ->with('toastr_msgs', ["success" => "User {$req['first_name']} {$req['last_name']} added"]);
    }

    /**
     * Show all accounts
     *
     * @param $user_id
     * @return $this
     */
    public function account($user_id)
    {
        $accounts = Account::whereIn('publisher_id', [95,97,98,99,161,162,163,164])->get()->toArray();
        $assign = AssignedAccount::select('account_id')->where('user_id', $user_id)->get()->pluck('account_id')->filter()->toArray();

        $results = [];
        foreach ($accounts as $account) {
            $has = ['has_permissions' => in_array($account['id'], $assign) ? true : false];
            $results[] = array_merge($account, $has);
        }
        
        return view('pages.users.accounts-modal')->with('data', $results)->with('user_id', $user_id);
    }

    /**
     * ToggleCheckbox property toggler
     * Update user permissions in accounts
     *
     * @param Request $request
     * @param $params
     * @return array
     */
    public function toggleCheckbox_assign_accounts(Request $request, $params)
    {
        $state = 0;
        $desc = 'You are not permitted to do this or account does not exists.';
        $toggle_state = 0;

        //Validation
        if (!auth()->user()->can(self::PERMISSION . '.edit') ||
            !$request->input('account_id') ||
            Account::where('source_account_id', $request->input('account_id'))->get()->isEmpty()) {

            return [
                'state' => $state,
                'desc' => $desc,
                'toggleState' => $toggle_state
            ];
        }

        $account = Account::where('source_account_id', $request->input('account_id'))->first();
        if (!count($account)) {
            return [
                'state' => $state,
                'desc' => 'Could not find the account.',
                'toggleState' => $toggle_state
            ];
        }

        $account_user_permissions = AssignedAccount::where('user_id', $request->input('user_id'))
            ->where('account_id', $account->id)->get()->keyBy('account_id');

        $state = 1;

        if ($account_user_permissions->isEmpty()) {
            $account_user_permissions = new AssignedAccount();
            $account_user_permissions->user_id = $request->input('user_id');
            $account_user_permissions->account_id = $account->id;
            try {
                $account_user_permissions->save();
            } catch (\Exception $e) {
                dd($e->getMessage());
            }

            $toggle_state = 1;
            $desc = 'Account linked to user.';
        } else {
            AssignedAccount::where('user_id', $request->input('user_id'))
                ->where('account_id', $account->id)
                ->delete();
            $desc = 'Account unlinked to user.';
        }

        return [
            'state' => $state,
            'desc' => $desc,
            'toggleState' => $toggle_state
        ];
    }


    /**
     * Select2 users
     *
     * @param $params
     *
     * @return array
     */
    private function select2_users($params)
    {
        $query = User::whereNotNull('id');

        if (isset($params['query']) && count($params['query']) > 0) {
            $query->where('username', 'LIKE', '%' . $params['query'] . '%')
            ->orWhere('first_name', 'LIKE', '%' . $params['query'] . '%')
            ->orWhere('last_name', 'LIKE', '%' . $params['query'] . '%');
        }

        return $query->get()->map(function ($item) {
            return [
                'id'   => $item->id,
                'text' => $item->first_name . ' ' . $item->last_name,
            ];
        });
    }


    private function toggleCheckbox_userPermissions(Request $request, $params)
    {
        $result = [
            'state' => 0,
            'desc' => '',
            'toggleState' => 0
        ];

        // Checks if the user_id was passed
        if (!isset($params[1])) {
            $result['desc'] = 'The user id were not passed.';
        }
        $user_id = $params[1];

        //Validation
        if (!auth()->user()->can(self::PERMISSION . '.modal[user_permission].edit')) {
            return $result;
        }

        $site_user_permission = SiteUserPermission::whereSiteId($request->input('site_id'))->wherePermissionId($request->input('permission_id'))->whereUserId($user_id);

        $result['desc'] = 'Change saved.';
        $result['state'] = 1;
        if ($site_user_permission->get()->count()) {
            $site_user_permission->delete();
            $result['toggleState'] = 0;
        } else {
            SiteUserPermission::Insert([
                'site_id' => $request->input('site_id'),
                'permission_id' => $request->input('permission_id'),
                'user_id' => $user_id,
            ]);
            $result['toggleState'] = 1;
        }

        return $result;
    }



}
