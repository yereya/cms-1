<?php namespace App\Http\Controllers;

use App\Http\Traits\AjaxModalTrait;

/**
 * Class DashboardController
 *
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    use AjaxModalTrait;

    /**
     * @var string $assets
     */
    protected $assets = ['form'];

    /**
     * Index
     *
     * @return string
     */
    public function index()
    {
        return view('pages.dashboards.index');
    }
}