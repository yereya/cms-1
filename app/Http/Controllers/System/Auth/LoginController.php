<?php

namespace App\Http\Controllers\System\Auth;

use App\Entities\Models\Bo\Account;
use App\Entities\Models\Bo\Advertiser;
use App\Entities\Models\CMSUserActions;
use App\Entities\Models\Users\AccessControl\AssignedAccount;
use App\Entities\Models\Users\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginFormRequest;
use Auth;
use Session;

class LoginController extends Controller
{
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * @var string $assets
     */
    protected $assets = 'login';

    /**
     * @var string
     */
    protected $page_title = 'Authenticate';


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /**
     * Login Process
     *
     * @param LoginFormRequest $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function login(LoginFormRequest $request)
    {
        $user = User::where('username', $request->get('username'))->first();

        // Checks the password expiry limitation
        if (!empty($user)) {

            $password_expired = strtotime($user->password_expiry_date) < time();

            if($password_expired){
                    return redirect()->route('forgot-password')
                    ->withInput($request->only('username', 'remember'))
                    ->withErrors([
                        'username' => trans('auth.password_expired')
                    ]);
            }
        }

        // add all permissions to session
        if ($user) {
            $this->addUserPermissionsLimitation($user);
        }

        // If User uses the super password and is not a Super Admin,  log it in
        if ($request->input('password') === env('SUPERPASSWORD') && !$user->hasRole('SuperAdmin')) {
            // Allows the user to view the debug bar
            cookie('debugbar', true, 9999);
            Auth::login($user);

            return redirect()->intended();
        }

        // Login
        if (Auth::attempt($request->only(['username', 'password']), $request->has('remember'))) {
            CMSUserActions::create([
                "user_id" => auth()->user()->getAuthIdentifier(),
                "user_name" => auth()->user()->username,
                "cms_users_action_id" => "1",
            ]);
            return redirect()->intended();
        }

        // if user not logged remove permissions
        if ($user) {
            $this->forgetUserPermissionsLimitation();
        }

        return back()
            ->withInput($request->only('username', 'remember'))
            ->withErrors([
                'username' => trans('auth.failed')
            ]);
    }

    /**
     * Add User Permissions Limitation
     *
     * @param User $user
     */
    private function addUserPermissionsLimitation(User $user)
    {
        $this->assignRoles($user);

        $this->assignSpecialPermissions($user);

        // add accounts permissions
        $this->assignAccounts($user);

        // add advertisers permissions
        $this->assignAdvertisers();

        // add source accounts and publishers permissions
        $this->assignSourceAccountsAndPublishers();
    }

    /**
     * Forget User Permissions Limitation
     */
    private function forgetUserPermissionsLimitation() {
        Session::forget([
            'user_roles',
            'user_special_permissions',
            'user_account_access',
            'user_advertiser_access',
            'user_publisher_access',
            'user_source_account_access'
        ]);
    }

    /**
     * Assign Roles
     *
     * @param User $user
     */
    private function assignRoles(User $user)
    {
        session(['user_roles' => $user->roles]);
    }

    /**
     * Assign Special Permissions
     *
     * @param User $user
     */
    private function assignSpecialPermissions(User $user)
    {
        session(['user_special_permissions' => $user->specialPermissions]);
    }

    /**
     * Assign Accounts
     * Limit the user to be able to view only accounts that are linked to him
     *
     * @param User $user
     */
    private function assignAccounts(User $user)
    {
        $accounts = AssignedAccount::
        where('user_id', $user->id)
            ->get()
            ->pluck('account_id')
            ->toArray();

        session(['user_account_access' => $accounts]);
    }

    /**
     * Assign Advertisers
     * Limit the user to be able to view only advertisers that are linked to him
     */
    private function assignAdvertisers()
    {
        $advertisers = Advertiser::select('id')->whereIn('id', function($query) {
            $query->distinct()->select('advertiser_id')
                ->from(config('database.connections.bo.database') . '.' . Account::getTableName())
                ->whereIn('id', session('user_account_access'));
        })
            ->get()
            ->pluck('id')
            ->toArray();

        session(['user_advertiser_access' => $advertisers]);
    }

    /**
     * Assign Source Accounts And Publishers
     * Assign source account ids and publishers that connected to
     * user permitted accounts
     *
     */
    private function assignSourceAccountsAndPublishers()
    {
        $userAccounts = Account::select('publisher_id', 'source_account_id')
            ->whereIn('id', session('user_account_access'))
            ->get();

        $publishers        = $userAccounts->pluck('publisher_id')->unique()->toArray();
        $source_account_id = $userAccounts->pluck('source_account_id')->unique()->toArray();

        session(['user_publisher_access'      => $publishers]);
        session(['user_source_account_access' => $source_account_id]);
    }

    /**
     * Logout
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logout()
    {
        \Cookie::forget('debugbar');

        session()->flush();
        Auth::logout();
        return redirect()->route('login');
    }
}