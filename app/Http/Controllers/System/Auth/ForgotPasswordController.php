<?php

namespace App\Http\Controllers\System\Auth;

use App\Http\Controllers\Controller;
use App\Http\Traits\SendPasswordResetTrait;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{

    use SendPasswordResetTrait;

    /**
     * @var string $assets
     */
    protected $assets = 'forgot-password';

    /**
     * @var string
     */
    protected $page_title = 'forgot-password';


    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('auth.passwords.forgot');
    }
}
