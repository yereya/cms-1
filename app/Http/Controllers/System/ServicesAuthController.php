<?php


namespace app\Http\Controllers\System;


use App\Http\Controllers\Controller;
use App\Libraries\JWT\ServicesToken;

class ServicesAuthController extends Controller
{
    /**
     * auth
     *
     * @param $service
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function auth($service)
    {
        $user = auth()->user();
        $ut   = new ServicesToken($user, $service);
        $url  = $ut->getRedirectUrl();

        return redirect($url);
    }
}