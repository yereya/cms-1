<?php 
namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Libraries\JWT\UnicornToken;

/**
 * Class UnicornAuthController
 *
 * @package App\Http\Controllers\System
 */
class UnicornAuthController extends Controller
{
    const PERMISSION = 'unicorn.view';

    /**
     * login
     */
    public function login()
    {
        $user = auth()->user();
        $ut = new UnicornToken($user);
        $url = $ut->getRedirectUrl();

        return redirect($url);
    }
}
