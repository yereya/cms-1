<?php namespace App\Http\Controllers\Experiments;

use App\Entities\Models\Experiments\Monitor;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxAlertTrait;
use App\Http\Traits\DataTableTrait;
use Illuminate\Http\Request;
use App\Http\Traits\Select2Trait;

/**
 * Class MonitorController
 *
 * @package App\Http\Controllers
 */
class MonitorController extends Controller
{
    use Select2Trait;
    use DataTableTrait;
    use AjaxAlertTrait;

    const PERMISSION  = 'experiments.monitors';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Monitors';

    /**
     * Hardcoded categories by department, used for both
     * departments & categories dropdowns
     *
     * @var array
     */
    private $departments = [
        'marketing' => ['conversion', 'traffic', 'budget', 'tests', 'changes', 'outside issue - special report'],
        'sales' => ['link a/b', 'performance - CTR, ECPM, etc...'],
        'product' => ['link a/b', 'new feature', 'analytics - issues we have with product'],
        'it' => ['upload time', 'page load'],
        'external' => ['general']
    ];

    /**
     * Index
     *
     * @return string
     */
    public function index()
    {
        $monitors = Monitor::all();
        return view('pages.experiments.monitors.index')
            ->with('monitors', $monitors);
    }

    /**
     * Create Form, which is also used for showing a single monitor item
     *
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        // Format departments array a select format
        $departments = array_map(function ($department) {
            return [
                "id" => $department,
                "text" => $department
            ];
        }, array_keys($this->departments));

        // Assign the view into a variable so it can be conditionally modified
        $view = view('pages.experiments.monitors.form')
            ->with('departments', $departments)
            ->with('permission', self::PERMISSION);


        $params = $request->all();

        //If id is given, an edit/show form is required
        if (isset($params['id']) && $params['id'] > 0) {
            return $view->with('monitor', Monitor::find($params['id']));
        }

        // Else, A blank form for creating a new monitor
        return $view;
    }

    /**
     * Store/Create resource in storage.
     *
     * @param $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $params = $request->all();

        if(empty($params)){
           abort(400, 'Data is required for storing in this is resource');
        }

        Monitor::create($params);

        //Redirect back to create when create_another is set
        return redirect()->route('experiments.monitors.index')
            ->with('toastr_msgs', ["success" => "New Monitor was saved."]);
    }

    /**
     * update resource in storage.
     *
     * @param $request
     * @return mixed
     */
    public function update(Request $request, $monitor_id)
    {
        $params = $request->all();

        if(empty($params)){
            abort(400, 'Data is required for storing in this is resource');
        }

        $monitor = Monitor::find($monitor_id);
        $monitor->fill($params)->save();

        //Redirect back to create when create_another is set
        return redirect()->route('experiments.monitors.create',['id'=>$monitor_id])
            ->with('toastr_msgs', ["success" => "Monitor was updated."]);
    }


    /**
     * Soft Delete a monitor
     *
     * @param $monitor_id
     * @return int
     */
    public function destroy($monitor_id)
    {
        //make sure the user has edit permissions
        if(!auth()->user()->can(self::PERMISSION.'.edit')){
            return abort(403,'User is forbidden from performing this request');
        }

        if(Monitor::destroy($monitor_id)){
            return redirect()->route('experiments.monitors.index')
                ->with('toastr_msgs', ["success" => "Monitor #".$monitor_id." was deleted."]);
        }

        return abort(500,'could not delete monitor #'.$monitor_id);

    }

    /**
     * Select2 for categories, depending on selected department value
     *
     * @param $params
     * @return mixed
     */
    public function select2_categories($params)
    {
        $departmentKey = $params['dependency'][0]['value'];

        // return categories formatted for select
        return array_map(function ($category) {
            return ["id" => $category, "text" => $category];
        }, $this->departments[$departmentKey]);
    }

    /**
     * Ajax Alert delete monitor confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     */
    public function ajaxAlert_destroyMonitor($params, $extra_params)
    {
        $monitor_id = $extra_params[0];

        return view('partials.containers.alerts.ajax-alert')
            ->with('text', 'Please confirm deleting monitor #'.$monitor_id)
            ->with('route', 'experiments.monitors.destroy')
            ->with('action', 'delete')
            ->with('route_params', $monitor_id);
    }


}