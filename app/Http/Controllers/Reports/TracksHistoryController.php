<?php

namespace App\Http\Controllers\Reports;

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;

class TracksHistoryController extends Controller
{
    const PERMISSION = 'reports.tracks_history';

    /**
     * @var array - init assets collection
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $tracks = new Collection();

        $params     = $request->all();
        $tracker_id = isset($params['tracker_id']) ? $params['tracker_id'] : '';

        if ($tracker_id) {
            // call to recursive function
            $this->getTrackHistory($tracker_id, $tracks);
        }

        return view('pages.reports.bo.tracks-history.index')
            ->with('submit_route', 'reports.tracks-history')
            ->with('method', 'GET')
            ->with('track_history', $tracks);
    }

    /**
     * Get Track History
     * Finds tracks recursively
     *
     * @param $tracker_id
     * @param $tracks
     */
    private function getTrackHistory($tracker_id, &$tracks)
    {
        $track = DB::connection('dwh')->table('dwh_fact_tracks')->where('id', $tracker_id)->first();
        if (is_null($track))
            return;

        $tracks->push($track);
        if ($track->source_track) {
            $this->getTrackHistory($track->source_track, $tracks);
        }
    }

};