<?php namespace App\Http\Controllers\Reports\Publishers;

use App\Entities\Models\Reports\Publishers\Keyword;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Traits\Select2Trait;
use Illuminate\Http\Request;

/**
 * Class ReportKeywordsController
 *
 * @package App\Http\Controllers\Reports
 */
class KeywordsController extends Controller
{
    use Select2Trait;

    const PERMISSION = 'publishers.accounts.keywords';

    /**
     * Select2 trait method
     * 
     * @param $params
     * @return null
     */
    public function select2_getter($params)
    {
        $query = null;

        // Task ID
        if ($params['search_field'] == "task_id") {
            $query = Task::select('task_id as id', 'task_name as text');
            foreach ((array) $params['dependency'] as $dependency) {
                if ($dependency['value']) {
                    $query->where('system_id', $dependency['value']);
                }
            }
        }


        if ($params['search_field'] == 'keyword') {
            $query = Keyword::select('keyword_id as id', \DB::raw('CONCAT(keyword, " [", match_type, "]") as text'))
                ->groupBy('keyword_id');
        }
        elseif ($params['search_field'] == 'ad_group') {
            $query = Keyword::select('ad_group_id as id', 'ad_group as text')
                ->groupBy('ad_group_id');
        }

        //Dependence filter
        if (isset($params['dependency'])) {
            $selectors = [
                '#select_ad_group_campaign_id' => 'campaign_id'
            ];
            foreach ((array) $params['dependency'] as $dependency) {
                if ($dependency['value']) {
                    if (isset($selectors[$dependency['selector']])) {
                        $query->where($selectors[$dependency['selector']], '=', $dependency['value']);
                    }
                }
            }
        }

        //Filter through the results if a query was passed
        if (isset($params['query'])) {
            $query->where($params['search_field'], 'like', '%' . $params['query'] . '%');
        }

        //Set all the extra where params
        if (isset($params['where']) && is_array($params['where'])) {
            foreach ($params['where'] as $key => $val) {
                $query->where($key, '=', $val);
            }
        }

        return $query;
    }
}