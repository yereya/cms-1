<?php namespace App\Http\Controllers\Reports\Publishers;

use App\Entities\Models\Reports\Publishers\Account;
use App\Entities\Models\Reports\Publishers\Advertiser;
use App\Entities\Models\Reports\Publishers\Change;
use App\Entities\Models\Reports\Publishers\ChangeDay;
use App\Entities\Models\Reports\Publishers\Keyword;
use App\Entities\Models\Reports\WeekDay;
use App\Entities\Models\Users\User;
use App\Entities\Models\Users\UserAccount;
use App\Entities\Repositories\Reports\Publishers\BrandPerformance\ScraperParams;
use App\Entities\Repositories\Reports\Publishers\BrandPerformance\ScraperRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Http\Traits\DatatableFilterTrait;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\HtmlLib;

use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\JsonLib;
use App\Libraries\Scraper\Requests\BrandsPerformance\Providers\Fetcher\Translators\XmlLib;
use Illuminate\Http\Request;
use App\Http\Traits\ToggleProperty;
use App\Libraries\Scraper\Scraper as ScraperLib;
use App\Libraries\DebugLogger;


class ChangesController extends Controller
{
    use ToggleProperty;

    const PERMISSION = 'publishers.accounts.changes';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form', 'x-editable'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Changes Log';

    /**
     * @var array $breadcrumbs_exclude
     */
    public $breadcrumbs_excludes = [
        ['name' => 'bing'],
        ['name' => 'adwords'],
    ];


    /**
     * Display a listing of the resource.
     *
     * @param string $publisher
     * @param int $account_id
     * @return \Illuminate\Http\Response
     */
    public function index($publisher, $account_id)
    {
        return view('pages.publishers.accounts.changes.index')
            ->with('account', Account::whereAccountId($account_id)->first())
            ->with('logs_campaigns', Change::getCampaigns($account_id))
            ->with('logs_ad_group', Change::getAdGroups($account_id))
            ->with('logs_keywords', Change::getKeywords($account_id))
            ->with('logs_scheduling', Change::getScheduling($account_id));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param $account_id
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $publisher_id, $account_id)
    {
        if (Account::where('account_id', $account_id)->first()->isEmpty)
            redirect()->route('pages.publishers.accounts.index')
                ->with('toastr_msgs', ["success" => "This account is not available."]);;

        return view('pages.publishers.accounts.changes.form')
            ->with('adwords_account_model', Account::class)
            ->with('campaigns_list', Keyword::select('campaign_id as id', 'campaign as text')->where('account_id', $account_id)->groupBy('campaign_id')->get())
            ->with('ad_group_list', Keyword::select('ad_group_id as id', 'ad_group as text', 'campaign as data-campaign')->where('account_id', $account_id)->groupBy('ad_group_id')->get())
            ->with('state_types', Change::getStates())
            ->with('week_days_list', WeekDay::select('id', 'name as text')->get())
            ->with('account', Account::where('account_id', $account_id)->first());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\Reports\Publishers\Accounts\ChangeFormRequest $request
     * @param string $publisher
     * @param int $account_id
     * @return mixed
     */
    public function store(Requests\Reports\Publishers\Accounts\ChangeFormRequest $request, $publisher, $account_id)
    {
        $params = $request->all();
        $params['user_id'] = auth()->user()->id;
        $params['bid'] = (!empty($request->get('bid'))) ? $request->get('bid') : null;;
        $params['bid_multiplier'] = (!empty($request->get('bid_multiplier'))) ? $request->get('bid_multiplier') : null;
        $params['mobile_bid_adjustment'] = null;
        if (!empty($request->get('bid_multiplier'))) {
            $params['mobile_bid_adjustment'] = percentToDecimalFraction($request->get('mobile_bid_adjustment'));
        }
        $res = Change::create($params);

        if ($request->input('change_level') == 'scheduling') {
            $week_days = new WeekDay();

            //Add the scheduling days relation and time span
            foreach ($request->input('scheduling_days') as $day) {
                $change_day = new ChangeDay();
                $change_day->day = $week_days->getDayName($day, 'strtolower');
                $change_day->change_id = $res->id;
                $change_day->save();
            }
        }

        //Redirect back to create when create_another is set
        if ($request->get('create_another'))
            return redirect()->route('reports.publishers.{publisher}.accounts.{account_id}.change.create', [$publisher, $account_id, $request->get('change_level')])
                ->with('toastr_msgs', ["success" => "Change to {$request->input('change_level')} saved"]);

        return redirect()->route('reports.publishers.{publisher}.accounts.{account_id}.change.index', [$publisher, $account_id])
            ->with('toastr_msgs', ["success" => "Change to {$request->input('change_level')} saved"]);
    }


    /**
     * Display the specified resource.
     *
     * @param $publisher
     * @param $account_id
     * @param $change_id
     * @return $this
     */
    public function show($publisher, $account_id, $change_id)
    {
        $changes = [];
        $changes['change'] = Change::find($change_id)->toArray();
        $changes['user'] = User::find($changes['change']['user_id'])->toArray();

        if ($changes['change']['account_id']) {
            $changes['account'] = Account::select('account_name', 'advertiser_id')->where('account_id', $changes['change']['account_id'])->get()->toArray();
            $changes['account'] = isset($changes['account'][0]) ? $changes['account'][0] : null;
            $changes['advertiser'] = Advertiser::select('name')->where('id', $changes['account']['advertiser_id'])->get()->toArray();
            $changes['advertiser'] = isset($changes['advertiser'][0]) ? $changes['advertiser'][0] : null;
        }

        if ($changes['change']['ad_group_id']) {
            $changes['ad_group'] = Keyword::select('ad_group')->where('ad_group_id', $changes['change']['ad_group_id'])->groupBy('ad_group_id')->get()->toArray();
            $changes['ad_group'] = isset($changes['ad_group'][0]) ? $changes['ad_group'][0] : null;
        }

        if ($changes['change']['campaign_id']) {
            $changes['campaign'] = Keyword::select('campaign')->where('campaign_id', $changes['change']['campaign_id'])->groupBy('campaign_id')->get()->toArray();
            $changes['campaign'] = isset($changes['campaign'][0]) ? $changes['campaign'][0] : null;
        }

        if ($changes['change']['keyword_id']) {
            $changes['campaign'] = Keyword::select('keyword')->where('campaign_id', $changes['change']['keyword_id'])->groupBy('keyword_id')->get()->toArray();
            $changes['campaign'] = isset($changes['keyword_id'][0]) ? $changes['keyword_id'][0] : null;
        }

        $changes['days'] = ChangeDay::where('change_id', $change_id)->get()->toArray();

        // we should pull history by change_level and by appropriate id:
        // campaigns and Scheduling -> campaign_id
        // Ad Group -> ad_group_id
        // Keywords -> keyword_id
        $level_subject = ($changes['change']['change_level'] != 'scheduling') ? $changes['change']['change_level'] : 'campaign';
        $changes['change']['history'] =
            Change::where($level_subject .'_id', $changes['change'][$level_subject .'_id'])
                ->where('change_level', $changes['change']['change_level'])
                ->where('date_time_changed', '<', $changes['change']['date_time_changed'])
                ->orderBy('date_time_changed', 'desc')
                ->get()->toArray();

        return view('pages.publishers.accounts.changes.show-modal')
            ->with('publisher', $change_id)
            ->with('account_id', $account_id)
            ->with('changes', $changes);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param $account_id
     * @param $change_id
     *
     * @return $this
     * @throws \Exception
     */
    public function edit($publisher, $account_id, $change_id)
    {
        $change_item = Change::find($change_id);
        if ($change_item->change_level == 'scheduling') {
            $change_item->days = ChangeDay::select('wd.id', 'day AS text')
                ->leftJoin(WeekDay::getTableName() .' AS wd', ChangeDay::getTableName() .'.day', '=', 'wd.name')
                ->where('change_id', '=', $change_id)
                ->get();

        }

        if ($change_item->isEmpty) {
            throw new \Exception('the change you are looking for does not exists');
        }

        $account = Account::whereAccountId($account_id)->first();

        return view('pages.publishers.accounts.changes.form')
            ->with('change_item', $change_item)
            ->with('adwords_account_model', Account::class)
            ->with('campaigns_list', Keyword::select('campaign_id as id', 'campaign as text')->where('account_id', $account_id)->groupBy('campaign_id')->get())
            ->with('ad_group_list', Keyword::select('ad_group_id as id', 'ad_group as text')->where('account_id', $account_id)->groupBy('ad_group_id')->get())
            ->with('state_types', Change::getStates())
            ->with('week_days_list', WeekDay::select('id', 'name as text')->get())
            ->with('account', Account::where('account_id', $account_id)->first());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  string                   $publisher
     * @param  int                      $account_id
     * @param  int                      $change_id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function update(Request $request, $publisher, $account_id, $change_id)
    {

        $change = Change::find($change_id);

        if (!$change) {
            throw new \Exception('The change you are attempting to edit does not exist.');
        }

        $params = $request->all();
        $params['user_id'] = auth()->user()->id;
        $params['bid'] = (!empty($request->get('bid'))) ? $request->get('bid') : null;;
        $params['bid_multiplier'] = (!empty($request->get('bid_multiplier'))) ? $request->get('bid_multiplier') : null;
        $params['mobile_bid_adjustment'] = null;
        if (!empty($request->get('bid_multiplier'))) {
            $params['mobile_bid_adjustment'] = percentToDecimalFraction($request->get('mobile_bid_adjustment'));
        }
        $change->fill($params);
        $change->save();

        //Change the days if this is a scheduling change level
        if ($request->input('change_level') == 'scheduling') {
            $week_days = new WeekDay();
            ChangeDay::where('change_id', $change_id)->delete();

            //Add the scheduling days relation and time span
            foreach ($request->input('scheduling_days') as $day) {
                $change_day = new ChangeDay();
                $change_day->day = $week_days->getDayName($day, 'strtolower');
                $change_day->change_id = $change_id;
                $change_day->save();
            }
        }

        return redirect()->route('reports.publishers.{publisher}.accounts.{account_id}.change.index', [$publisher, $account_id, $request->input('change_level')])
            ->with('success', true)
            ->with('toastr_msgs', ["success" => "Change to {$request->input('change_level')} saved"]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $account_id
     * @param $change_id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $publisher, $account_id, $change_id, $uris = null)
    {
        if (auth()->user()->can(self::PERMISSION.'.delete')) {
            Change::destroy($change_id);

            return redirect()->back()
                ->with('toastr_msgs', ["success" => "Change deleted"]);
        }

        return redirect()->back()
            ->with('toastr_msgs', ["warning" => "You do not have permissions to do this."]);
    }


    /**
     * ToogleCheckbox property toggler
     * which toggles the property follow_up_state
     *
     * @param $request
     * @param $params
     * @return array
     */
    private function toggleCheckbox_follow_up_state($request, $params) {
        $status = 1;
        $desc = '';
        $toggle_state = 0;

        $changes = Change::find($request->input('change_id'));
        if ($changes->follow_up_state) {
            $changes->follow_up_state = 0;
            $changes->save();
            $desc = 'Follow up state set to UNDONE.';

        } else {
            $changes->follow_up_state = 1;
            $changes->save();
            $toggle_state = 1;
            $desc = 'Follow up state set to DONE.';
        }

        return [
            'status' => $status,
            'desc' => $desc,
            'toggleState' => $toggle_state
        ];
    }

    private function datatable()
    {
        
    }
    
    
    /**
     * @param $publisher
     * @return mixed
     */
    public function listAll($publisher) {
        $changes = array();
        foreach (Change::all() as $change) {
            $change->users;
            $change->accountByAccountId;
            $change->keywordByCampaignId;
            $changes[] = $change;
        }

        return view('pages.publishers.accounts.changes.list-all')
            ->with('changes', $changes)
            ->with('publisher', $publisher);
    }


    public function test1()
    {
        $params = [
            'brands_performance' => [
                'fetcher' => new ScraperRepo()
            ]
        ];
        $scraper = new ScraperLib($params);
        $scraper->runRequest('brands_performance');

        return DebugLogger::getRecording();

        //Debug
        $csv_string = 'reportname:acidreport
reportstartdate:2016-Jan-1
reportenddate:2016-Feb-2
reportmerchantid:0
reportdisplayby:date
"rowid","currencysymbol","totalrecords","affcustomid","period","impressions","clicks","installs","clickthroughratio","downloads","downloadratio","revsharecommission","totalcpacommission","cpacommissioncount","referralcommissiontotal","totalcommission"
"1","&#163;","390","-11er4n-3l","1/23/2016","1","1","0","100","0","0","0","0","0","0","0"
"1","&#163;","390","-11Ow5se-","1/26/2016","1","1","0","100","1","100","0","0","0","0","0"
"1","&#163;","390","-11TWBkSb","1/29/2016","1","1","0","100","0","0","0","0","0","0","0"
"1","&#163;","390","-11xL$lGW","1/27/2016","1","1","0","100","0","0","0","0","0","0","0"
"1","&#163;","390","-1J$uIvKe","1/21/2016","1","1","0","100","0","0","0","0","0","0","0"
"1","&#163;","390","-1JcmUIz","1/3/2016","1","1","0","100","0","0","0","0","0","0","0"
"1","&#163;","390","-1JQE9U5l","1/21/2016","1","1","0","100","0","0","0","0","0","0","0"';

//        $csv = new CsvLib;
//        dd($csv->translate($csv_string));

        $params = [];
        $method = 'GET';
        $url = 'http://google.com';

//        $fetcher = new Fetcher();
//        $fetcher->fetch($url, $params, $method);
//        $csv_string_ = $fetcher->getBody();
        $csv = new CsvLib;
        $parsed_result[] = $csv->translate($csv_string, 1);
        dd($parsed_result);
        $processing_relationship = ''; //TODO needs to come from the db or the current call
        $relationship = new RelationshipProcessor($parsed_result, $processing_relationship);
        //dd($fetcher->getHeader());
    }

    public function testXml() {
        $file = storage_path() . '/app/xml_new.xml';
//        $file = storage_path() . '/app/xml-report-traffic.xml';

        if (!file_exists($file)) {
            throw new \Exception('File not found');
        }

        $data = file_get_contents($file);

        $xml = new XmlLib();
        $result = $xml->translate($data, 'conversion');
//        $result = $xml->translate($data);

        dd($result);
    }

    public function testJson() {
        $file = storage_path() . '/app/paddy.json';
        //$file = storage_path() . '/app/json.json';

        if (!file_exists($file)) {
            throw new \Exception('File not found');
        }

        $data = file_get_contents($file);

        $json = new JsonLib();
        $result = $json->netReferTranslate($data, 'data', 'Key', 'Value');
        //$result = $json->translate($data, 'response.data.data');

        dd($result);
    }

    public function testHtml() {

        $scr = new ScraperRepo();

        dd($scr->get());

        $file2 = storage_path() . '\app\Karamba.html';

        if (!file_exists($file2)) {
            throw new \Exception('File not found');
        }

        $data = file_get_contents($file2);

        $html = new HtmlLib();

        //__VIEWSTATEGENERATOR
        //table#ucTrafficGridView0_gvTraffic
        $result = $html->translateValue($data, '#kosta_test', 'text');

        dd($result);
    }
}
