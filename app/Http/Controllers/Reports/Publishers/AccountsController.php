<?php namespace App\Http\Controllers\Reports\Publishers;

use App\Contracts\JsPlugins\XEditable;
use App\Entities\Models\Reports\Publishers\Account;
use App\Entities\Models\Reports\Publishers\Advertiser;
use App\Entities\Models\Users\UserAccount;
use App\Http\Controllers\Controller;
use App\Libraries\Scraper\Scraper as ScraperLib;
use Illuminate\Http\Request;

class AccountsController extends Controller implements XEditable
{

    const PERMISSION = 'publishers.accounts';

    /**
     * @var array $assets
     */
    protected $assets = ['datatables', 'x-editable', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Publisher accounts';

    /**
     * @var array $breadcrumbs_exclude
     */
    public $breadcrumbs_excludes = [
        ['name' => 'bing'],
        ['name' => 'adwords'],
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user_accounts = UserAccount::where('user_id', auth()->user()->id)->get();

        $accounts = array_pluck($user_accounts, 'account_id');

        return view('pages.publishers.accounts.index')
            ->with('accounts_adwords', Account::where('publisher', 'adwords')->with('advertisers')->whereIn('account_id', $accounts)->get())
            ->with('accounts_bing', Account::where('publisher', 'bing')->with('advertisers')->whereIn('account_id', $accounts)->get());
    }

    public function xEditableApi(Request $request)
    {
        $arguments = func_get_args();
        $publisher = $arguments[1];
        $params = $request->all();
        $res = [];

        //Getter
        if (isset($params['get'])) {
            if ($params['get'] == 'accountTypes') {
                $_res = Account::getAccountTypes();
                foreach ($_res as $item) {
                    $res[] = ['value' => $item['id'] , 'text' => $item['text']];
                }
            }
            elseif ($params['get'] == 'advertisers') {
                $res = Advertiser::select('id as value', 'name as text')->get()->toArray();
            }
        }
        //Setter
        elseif (isset($params['set'])) {
            $account = Account::where('publisher', $publisher)->where($params['pk_column'], $params['pk'])->first();

            if ($account) {
                if ($params['set'] == 'accountTypes') {
                    $new_account_type = Account::getAccountTypeName($params['value']);
                    $account->{$params['name']} = $new_account_type;
                }
                elseif ($params['set'] == 'advertisers') {
                    $account->{$params['name']} = $params['value'];
                }
                else {
                    $res = null;
                }

                $res = $account->save() ? $params : null;
            }
        }

        return $res;
    }


    /**
     * @param $publisher - type [adwords || bing]
     *
     * @return null
     */
    public function updateApi($publisher)
    {
        $response = array(
            'status' => 0,
            'desc' => ''
        );

        $scraper = new ScraperLib();
        if ($publisher == 'adwords') {
            $scraper->runRequest('keywords_performance', 'google');
            $response['status'] = 1;
        }
        elseif ($publisher == 'bing') {
            $scraper->runRequest('keywords_performance', 'bing');
            $response['status'] = 1;
        }
        else {
            $response['desc'] = 'No correct publisher were passed.';
        }

        return $response;
    }
}