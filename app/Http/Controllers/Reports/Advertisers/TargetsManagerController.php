<?php namespace App\Http\Controllers\Reports\Advertisers;

use App\Http\Controllers\Controller;
use App\Libraries\RecordManager\RecordManagerFactory;
use Illuminate\Support\Facades\Validator;
use Input;
use Illuminate\Http\Request;

class TargetsManagerController extends Controller
{
    const PERMISSION = 'bo.advertisers.advertisers-targets-manager';

    /**
     * @var array - init assets collection
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Advertiser targets Manager';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.bo.advertisers.targets-manager.index');
    }


    /**
     * Store and handle new campaign management attempt
     * This is called from the approve page, after the user approves the items
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $record_manipulator = RecordManagerFactory::create('AdvertisersTargets');
        $log_id = $record_manipulator->store();

        return redirect()->route('reports.advertisers.targets-manager.index')
            ->with(self::MESSAGE_TOAST, [
                "success" => "The changes were saved. <br> <a href='" . route('reports.logs.{log_id}.list-group', ['log_id' => $log_id]) . "'>View log</a>"
            ])
            ->with(self::OPEN_TAB, route('reports.logs.{log_id}.list-group', ['log_id' => $log_id]));
    }


    /**
     * The approval page for campaigns manipulations done on Advertiser campaigns
     *
     * @return mixed
     * @throws \App\Libraries\RecordManager\Exceptions\InvalidHandlerRequestedException
     */
    public function approve()
    {
        $record_manipulator = RecordManagerFactory::create('AdvertisersTargets');

        $candidates = $record_manipulator->fetchCandidates();

        // If no valid tokens were passed
        // lets show an error
        if (empty($candidates)) {
            return redirect()->route('reports.advertisers.targets-manager.index')
                ->with(self::MESSAGE_TOAST, ["warning" => "No valid records were passed, or no tokens were matched."])
                ->with(self::OPEN_TAB, route('reports.logs.{log_id}.list-group', [
                    'log_id' => $record_manipulator->getLoggerParentRecordId(),
                ]));
        }

        return view('pages.bo.advertisers.targets-manager.approve')
            ->with('approval_candidates', $candidates);
    }


    /**
     * @param Request $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws \App\Libraries\RecordManager\Exceptions\InvalidHandlerRequestedException
     */
    public function upload(Request $request)
    {
        $record_manipulator = RecordManagerFactory::create('AdvertisersTargets');

        $record_manipulator->setAction(Input::get('action'));
        $save_result = $record_manipulator->saveFile('file');

        // If we fail for some reason
        // redirect to the index page and popup a log
        if ($save_result === false) {
            return redirect()->route('reports.advertisers.targets-manager.index')
                ->with(self::OPEN_TAB, route('reports.logs.{log_id}.list-group', [
                    'log_id' => $record_manipulator->getLoggerParentRecordId(),
                ]));
        }

        // File validation
        if (is_a($save_result, Validator::class) && $save_result->fails()) {
            return redirect()->route('reports.advertisers.targets-manager.upload')
                ->withErrors($save_result->fails());
        }

        // For cases when the validation of the data itself has failed lets add a link that will show
        // the way to the log view
        if ($record_manipulator->isValidationFailedTracksExist()) {
            return redirect()->route('reports.advertisers.targets-manager.approve')
                ->with(self::MESSAGE_TOAST, [
                    "warning" => "Some or all records were removed due to data being invalid <br> <a href='" . route('reports.logs.{log_id}.list-group', ['log_id' => $record_manipulator->getLoggerParentRecordId()]) . "'>View log</a>"
                ]);
        }


        return redirect()->route('reports.advertisers.targets-manager.approve');
    }

}
