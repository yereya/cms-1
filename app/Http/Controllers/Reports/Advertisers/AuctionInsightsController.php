<?php namespace App\Http\Controllers\Reports\Advertisers;

use App\Http\Controllers\Controller;
use App\Libraries\RecordManager\RecordManagerFactory;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Input;
use Illuminate\Http\Request;

class AuctionInsightsController extends Controller
{
    const PERMISSION = 'bo.advertisers.auction_insights';

    /**
     * @var array - init assets collection
     */
    protected $assets = ['datatables', 'form'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Auction Insights Manager';

    const HANDLER_NAME = 'AuctionInsights';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.bo.advertisers.auction-insights-manager.index');
    }

    /**
     * @param Request $request
     *
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws \App\Libraries\RecordManager\Exceptions\InvalidHandlerRequestedException
     * @throws \App\Libraries\RecordManager\Exceptions\InvalidRecordActionException
     */
    public function upload(Request $request)
    {
        $record_manipulator = RecordManagerFactory::create(self::HANDLER_NAME);
        //set_time_limit(8000000000000000000);
        //ini_set('max_execution_time', 180); //3 minutes
        Session::put('connection_input', Input::get('save_to_new_adserver'));
        $save_result = $record_manipulator->saveAuctionFile('file');

        // If we fail for some reason
        // redirect to the index page and popup a log
        if ($save_result === false) {
            return redirect()->route('reports.advertisers.auction-insights-manager.index')
                ->with(self::MESSAGE_TOAST, ["unsuccessful" => "The changes were not saved, the CSV format failed"]);
        }

        return redirect()->route('reports.advertisers.auction-insights-manager.index')
            ->with(self::MESSAGE_TOAST, ["success" => "The auction insights were saved to mrr"]);
    }

}