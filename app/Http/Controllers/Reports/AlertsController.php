<?php namespace App\Http\Controllers\Reports;

use App\Entities\Models\Alerts\AlertLog;
use App\Entities\Models\Alerts\AlertType;
use App\Http\Controllers\Controller;
use App\Http\Traits\AjaxModalTrait;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\ToggleProperty;
use App\Libraries\Datatable\DatatableLib;
use Illuminate\Http\Request;

/**
 * Class AlertsController
 *
 * @package App\Http\Controllers
 */
class AlertsController extends Controller
{
    use AjaxModalTrait;
    use DataTableTrait;
    use ToggleProperty;

    const PERMISSION = 'reports.alerts';

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'datatables'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Alerts';

    /**
     * Index
     *
     * @return string
     */
    public function index()
    {
        return view('pages.reports.alerts.index');
    }

    /**
     * Notifications List
     * returns notifications list using long polling
     *
     * @param Request $request
     * @return mixed
     */
    public function notificationsList(Request $request)
    {
        $sleep_time          = 5;
        $time_limit          = 60;
        $current_cycle       = 0;
        $notifications_limit = 10;

        $ids = $request->ids;

        while (1) {

            if ($current_cycle > $time_limit) {
                return [
                    'status' => 0,
                    'desc' => 'Handle Notifications timeout',
                ];
            }

            $new_notifications_count = auth()->user()->alertLogs()
                ->wherePivot('viewed', '0')
                ->wherePivot('reminder_date', '<', date('Y-m-d H:i:s', time() ))
                ->count();

            $viewed_notifications_count = auth()->user()->alertLogs()
                ->wherePivot('viewed', '1')
                ->wherePivot('reminder_date', '<', date('Y-m-d H:i:s', time() ))
                ->count();

            $notifications_to_show = auth()->user()->alertLogs()
                ->with('alertType')
                ->wherePivot('viewed', '0')
                ->wherePivot('reminder_date', '<', date('Y-m-d H:i:s', time() ))
                ->limit($new_notifications_count > $notifications_limit ? $new_notifications_count:  $notifications_limit)
                ->get();

            if ($request->first_poll == 'true') {
                break;
            }

            // Lets test if we have any changes in our results from this request
            if (count($notifications_to_show) != count($ids)) {
                break;
            }

            // reject all items that are already displayed (represented by ids)
            $_notifications_to_show = $notifications_to_show->reject(function ($item) use ($ids) {
                return in_array((string)$item['id'], $ids, true);
            });

            if ($_notifications_to_show->count() != 0) {
                break;
            }

            $current_cycle += $sleep_time;
            sleep($sleep_time);
        }

        return view('partials.containers.dropdown-notifications-list')
            ->with('notifications_unread', $new_notifications_count)
            ->with('notifications_read', $viewed_notifications_count)
            ->with('notifications_to_show', $notifications_to_show->toArray());
    }

    /**
     * Update
     * updates given alert
     *
     * @param Request $request
     * @param $alert_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $alert_id)
    {
        $params     = $request->all();
        $reschedule = $params['reschedule'];

        $notification = auth()->user()->alertLogs()
            ->wherePivot('alert_log_id', $alert_id);

        $notification->updateExistingPivot($alert_id, array('reminder_date' => $reschedule), false);

        return redirect()->route('reports.alerts.index');
    }

    /**
     * Toggle Checkbox Viewed Status
     * Toggles the viewed state of alert
     *
     * @param Request $request
     * @return array
     */
    private function toggleCheckbox_viewedStatus(Request $request)
    {
        $params = $request->all();
        $notification = auth()->user()->alertLogs()
            ->wherePivot('alert_log_id', $params['alert_id']);

        if ($notification->get()->first()->pivot->viewed == 0) {
            $notification->updateExistingPivot($params['alert_id'], array('viewed' => 1), false);
            $toggle_state = 1;
        } else {
            $notification->updateExistingPivot($params['alert_id'], array('viewed' => 0), false);
            $toggle_state = 0;
        }

        return [
            'status' => 1,
            'desc'   => '',
            'toggleState' => $toggle_state
        ];
    }

    /**
     * Ajax Modal Alert Modal
     * Show alert modal
     *
     * @param $params
     * @param $notification_id
     * @return mixed
     */
    private function ajaxModal_alertModal($params, $notification_id)
    {
        $notification = auth()->user()->alertLogs()
            ->wherePivot('alert_log_id', $notification_id[0])
            ->get();

        $state = $notification->first()->pivot->viewed == 1 ? 'checked' : '';

        return view('pages.reports.alerts.alert-modal')
            ->with('notification', $notification)
            ->with('state', $state);
    }

    /**
     * Data Table Alerts Query
     * Builds data table for alerts
     *
     * @param $params
     * @param DatatableLib $datatable
     * @return array
     */
    private function dataTable_alertsQuery($params, DatatableLib $datatable)
    {
        $query = auth()->user()->alertLogs()
            ->wherePivot('user_id', auth()->user()->id)
            ->with('alertType.system');

        $params['recordsTotal'] = $query->count();

        $datatable->addColumn('alerts_log.id');
        $datatable->renderColumns([
                'created_at'  => 'created_at',
                'error_level' => [
                    'db_name'  => 'error_level',
                    'callback' => function ($obj) {
                        switch ($obj['error_level']) {
                            case 'info' :
                                return '<i class="glyphicon glyphicon-info-sign info-icon" title="Info"> </i>';
                                break;

                            case 'notice' :
                                return '<i class="glyphicon glyphicon-bullhorn notice-icon" title="Notice"> </i>';
                                break;

                            case 'warning' :
                                return '<i class="glyphicon glyphicon-warning-sign warning-icon" style="color: orange;" title="Warning"> </i>';
                                break;

                            case 'critical' :
                                return '<i class="glyphicon glyphicon-remove-sign error-icon" style="color: red;" title="Error"> </i>';
                                break;
                        }
                    }
                ],
                'system' => [
                    'db_name' => 'alert_type_id',
                    'callback' => function ($obj) {
                        return $obj['alert_type']['system']['name'];
                    }
                ],
                'description'   => 'description',
                'content'       => 'content',
                'reminder_date' => [
                    'db_name' => 'reminder_date',
                    'callback' => function ($obj) {
                        return $obj['pivot']['reminder_date'];
                    }
                ],
                'actions' => [
                    [
                        'type' => 'modal',
                        'name' => 'Show alert',
                        'icon' => 'icon-magnifier',
                        'route' => 'reports.alerts.{notification_id}.ajax-modal',
                        'route_params' => 'method=alertModal'
                    ]
                ]
            ]
        );

        $params['query'] = $query->select($datatable->getColumns());

        return $datatable->fill($params);
    }

}