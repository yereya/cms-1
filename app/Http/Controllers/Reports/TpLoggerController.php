<?php namespace App\Http\Controllers\Reports;

use App\Entities\Models\Scheduler\Task;
use App\Entities\Models\System;
use App\Entities\Models\Users\User;
use App\Http\Controllers\Controller;
use App\Entities\Models\Log;
use App\Http\Traits\DataTableTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\Datatable\DatatableLib;
use DB;
use Illuminate\Http\Request;

/**
 * Class TpLoggerController
 *
 * @package App\Http\Controllers
 */
class TpLoggerController extends Controller
{
    use DataTableTrait;
    use Select2Trait;

    const PERMISSION = 'reports.logs';

    /**
     * @var string $assets
     */
    protected $assets = ['form', 'datatables'];

    /**
     * @var string $page_title
     */
    protected $page_title = 'Logs';

    /**
     * Index
     *
     * @param Request $request
     *
     * @return string
     */
    public function index(Request $request)
    {
        $error_types = [];
        foreach (Log::getErrorTypes() as $item) {
            $error_types[$item] = $item;
        }

        $has_entry_params = $request->has('entry_record_id');
        $url_query = $request->all();

        // if record have field entry_record_id not type int
        // change to search in uuid_entry_record_id
        if ($has_entry_params && !is_int($url_query['entry_record_id'])) {
            $url_query['uuid_entry_record_id'] = $url_query['entry_record_id'];
            unset($url_query['entry_record_id']);
        }

        return view('pages.reports.logs.index')
            ->with('error_types', $error_types)
            ->with('tasks', Task::select('task_id as id', 'task_name as text')->where('task_id', '<>', 0)->get())
            ->with('systems', System::select('id', 'name as text')->get())
            ->with('initiator', User::select('id', 'username as text')->get())
            ->with('url_query', $url_query);
    }

    public function showModal(Request $request, $parent_record_id)
    {
        $logs = Log::where('parent_id', $parent_record_id)->orWhere('id', $parent_record_id)->orderBy('id', 'desc')->get();

        $title = $logs->last()->message ?? null;

        return view('pages.reports.logs.log-modal')
            ->with('title', $title)
            ->with('logs', $logs);
    }

    public function showDwhLogModal(Request $request, $parent_record_id) {
        $dwh_connection = DB::connection('dwh');
        $logs = $dwh_connection->table('logs_data_qa_results')->where('entry_record_id', $parent_record_id)->get();
        $dwh_connection->disconnect();

        $columns = [];
        if (!empty($logs->first())) {
            $columns = str_replace(['<tr>', '</tr>', '<td>'], '', $logs->first()->field_names);
            $columns = explode('</td>', $columns);
            $columns = array_filter($columns);
        }

        $parsed = [];
        foreach ($logs as $log) {
            $values = str_replace(['<tr>', '</tr>', '<td>'], '', $log->field_values);
            $values = explode('</td>', $values);
            $values = array_filter($values);

            $temp = [];
            foreach ($columns as $index => $column) {
                $temp[$column] = $values[$index] ?? null;
            }

            $temp['id'] = $log->id;
            $temp['parent_id'] = $parent_record_id;
            $parsed[] = $temp;
        }

        $dt_columns = [];
        $dt_columns[] = ['key' => 'id'];
        foreach ($columns as $col) {
            $dt_columns[] = ['key' => $col];
        }

        return view('pages.reports.logs.log-dwh-modal')
            ->with('title', 'dwh')
            ->with('parent_id', $parent_record_id)
            ->with('logs', $parsed)
            ->with('columns', $dt_columns);
    }

    public function listGroup(Request $request, $parent_record_id)
    {
        $logs = Log::where('parent_id', $parent_record_id)->orWhere('id', $parent_record_id)->orderBy('id', 'desc')->get();

        $title = $logs->last()->message ?? null;

        return view('pages.reports.logs.group-popup')
            ->with('title', $title)
            ->with('logs', $logs);
    }

    private function select2_tasks($params) {
        $tasks_id = [];
        foreach ($params['dependency'] as $dependency) {
            $tasks_id[] = $dependency['value'];
        }
        return Task::select('task_id as id', 'task_name as text')->whereIn('task_id', array_values($tasks_id))->get()->toArray();
    }

    private function dataTable_logQuery($params, DatatableLib $datatable)
    {
        $query = Log::whereNull('parent_id');
        $params['recordsTotal'] = $query->count();

        $params['searchColumns'] = ['message'];
        $query->with(['systems', 'users']);

        $datatable->addColumn('id');
        $datatable->addColumn('uuid_entry_record_id');
        $datatable->renderColumns([
                'recorded_at' => 'created_at',
                'run_result' => [
                    'db_name' => 'error_type',
                    'callback' => function ($obj) {
                        switch ($obj['error_type']) {
                            case 'info' :
                                return '<i class="glyphicon glyphicon-info-sign info-icon" title="Info"> </i>';
                                break;

                            case 'warning' :
                                return '<i class="glyphicon glyphicon-warning-sign warning-icon" style="color: orange;" title="Warning"> </i>';
                                break;

                            case 'error' :
                                return '<i class="glyphicon glyphicon-remove-sign error-icon" style="color: red;" title="Error"> </i>';
                                break;

                            case 'notice' :
                                return '<i class="glyphicon glyphicon-bullhorn notice-icon" title="Notice"> </i>';
                                break;

                            case 'debug' :
                                return '<i class="fa fa-bug debug-icon" title="Debug"> </i>';
                                break;
                        }
                    }
                ],
                'message' => [
                    'db_name' => 'message',
                    'callback' => function ($obj) {
                        if (isJson($obj['message'])) {
                            return arrayToString(json_decode($obj['message'], true));
                        } else {
                            return $obj['message'];
                        }
                    }
                ],
                'system' => [
                    'db_name' => 'system_id',
                    'callback' => function ($obj) {
                        $name = $obj['systems']['name'];
                        return "<a href='#'>$name</a>";
                    }
                ],
                'run_time' => [
                    'db_name' => 'id',
                    'callback' => function ($obj) {
                        $last_record = Log::select('created_at')->where('parent_id', $obj['id'])->orderBy('id', 'asc')->first();
                        $first_record = Log::select('created_at')->where('parent_id', $obj['id'])->orderBy('id', 'desc')->first();
                        $seconds_total = strtotime($first_record['created_at']) - strtotime($last_record['created_at']);

                        switch (true) {
                            case($seconds_total <= 60):
                                $time_format = 's\s';
                                break;
                            case($seconds_total > 60 and $seconds_total <= 3600):
                                $time_format = 'i\m s\s';
                                break;
                            case($seconds_total > 3600):
                                $time_format = 'H\h i\m s\s';
                                break;
                        }

                        return gmdate($time_format, $seconds_total);

                    }
                ],
                'initiator' => [
                    'db_name' => 'initiator_user_id',
                    'callback' => function ($obj) {
                        if ($obj['initiator_user_id'] == 0) {
                            return '<i class="fa fa-terminal tooltips" title="Ran in background"></i> Scheduler';
                        } else {
                            return $obj['users']['username'];
                        }
                    }
                ],
                'actions' => function($obj) {
                    $route = '#';
                    if($obj['_uuid_entry_record_id']) {
                        $route = route('reports.logs.{log_id}.show-dwh-modal', $obj['_uuid_entry_record_id']);
                    } else {
                        $route = route('reports.logs.{log_id}.show-modal', $obj['id']);
                    }

                    return '<a href="' . $route . '" title="" class="tooltips" data-toggle="modal" data-target="#ajax-modal" data-original-title="Show log with debug data">
                            <span aria-hidden="true" class="icon-magnifier"></span></a>';

                }
            ]
        );

        $params['query'] = $query->select($datatable->getColumns());
        return $datatable->fill($params,
            ["transformData" => function($d){
            $d = is_object($d) ? (array)$d : $d;
            $row['Recorded at'] = $d['Recorded_at'];
            $row['Run result'] = $d['Run_Result'];
            $row['System'] = $d['System'];
            $row['Message'] = $d['Message'];
            $last_record = $d['Last_Record_Time'];
            $first_record = $d['First_Record_Time'];
            $seconds_total = strtotime($last_record) - strtotime($first_record);
            switch (true) {
                case($seconds_total <= 60):
                    $time_format = 's\s';
                    break;
                case($seconds_total > 60 and $seconds_total <= 3600):
                    $time_format = 'i\m s\s';
                    break;
                case($seconds_total > 3600):
                    $time_format = 'H\h i\m s\s';
                    break;
            }
            $row['Run Time'] = gmdate($time_format, $seconds_total);
            $row['Initiator'] = $d['Initiator'];
            return $row;
        },
        "transformHeader" => function(){
            return ['Recorded at', 'Run result', 'System', 'Message','Run Time', 'Initiator'];
        },"SQL_Query" => function(){
            return <<<SQL
SELECT
`logs`.created_at AS 'Recorded_at',
`logs`.error_type AS 'Run_Result',
log_system_names.name AS 'System',
`logs`.message AS 'Message',
initiator_usernames.username AS 'Initiator',
runningTime.lastRecordTime AS 'Last_Record_Time',
runningTime.firstRecordTime AS 'First_Record_Time'
FROM 
(	
	SELECT `logs`.id AS id, NAME FROM `logs`,`systems`
	WHERE `logs`.system_id = `systems`.id AND `logs`.parent_id IS NULL) AS log_system_names,
(	
	SELECT id,created_at 
	FROM `logs` 
	WHERE parent_id IS NULL) AS created_at_table,
(
	SELECT `logs`.id AS id, firstRecordTime, lastRecordTime
	FROM(	
		SELECT `logs`.parent_id AS parent_id,
		`logs`.id AS id,
		firstRecord.created_at AS firstRecordTime,
		lastRecord.created_at AS lastRecordTime
		FROM
		(SELECT id, created_at FROM `logs` WHERE parent_id IS NULL) AS firstRecord, 
		(SELECT parent_id, MAX(created_at) AS created_at FROM `logs`GROUP BY parent_id) AS lastRecord, 
		`logs`
		WHERE firstRecord.id = lastRecord.parent_id AND `logs`.parent_id = firstRecord.id AND lastRecord.parent_id = `logs`.parent_id
		GROUP BY `logs`.parent_id) AS recordTimes,
		`logs`
	WHERE `logs`.id = recordTimes.parent_id
) AS runningTime,
(	
	SELECT `logs`.id AS id, `users`.id AS init_id,username FROM `logs`,`users`
	WHERE `logs`.initiator_user_id = `users`.id AND `logs`.parent_id IS NULL

) AS initiator_usernames
,
`logs`
WHERE created_at_table.id = `logs`.id 
AND log_system_names.id = `logs`.id 
AND initiator_usernames.init_id = `logs`.initiator_user_id
AND initiator_usernames.id = `logs`.id
AND runningTime.id = `logs`.id
SQL;
    }]);
//        return $datatable->fill($params, true);
    }
}
