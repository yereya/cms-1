<?php namespace App\Http\Controllers\Reports;

use App\Entities\Models\Reports\Queries\Filter;
use App\Entities\Repositories\Reports\FiltersRepo;
use App\Http\Traits\AjaxModalTrait;
use App\Libraries\Datatable\FilterForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FiltersController extends Controller
{
    use AjaxModalTrait;

    const PERMISSION = 'reports.filters';

    /**
     * Store
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $filter_form = new FilterForm();
        $filters = $filter_form->filter($request->get('fetch_form'));
        $filters['filter_name'] = $request->get('filter_name');

        // Validate input
        if (!$request->get('filter_name')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "Filter name must be provided."]);
        }

        // Save the filter
        $filter_id = FiltersRepo::save($filters, auth()->user()->id);

        // Build the redirect url
        $previous_url = app('url')->previous();
        if (strpos($previous_url, '?') !== false) {
            $previous_url = substr($previous_url, 0, strpos($previous_url, '?'));
        }
        $previous_url .= '?' . http_build_query(['filter_id' => $filter_id]);

        return redirect()->to($previous_url)
            ->with(self::MESSAGE_TOAST, ['info' => 'Filter saved.']);
    }


    public function destroy(\Request $request, $filter_id)
    {
        // Permission check
        if (!auth()->user()->can(self::PERMISSION . '.delete')) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }

        FiltersRepo::deleteFilter($filter_id);

        $previous_url = app('url')->previous();
        if (strpos($previous_url, '?') !== false) {
            $previous_url = substr($previous_url, 0, strpos($previous_url, '?'));
        }

        return redirect()->to($previous_url)
            ->with(self::MESSAGE_TOAST, ["info" => "Filter deleted."]);
    }


    /**
     * Display the filter save modal
     *
     * @param $params
     * @param $query_args
     *
     * @return mixed
     * @throws \Exception
     * @internal param $url_query_filter
     */
    private function ajaxModal_saveFilters($params, $query_args)
    {
        // Validate input
        if (!isset($params['filtersFormId'])) {
            throw new \Exception('FilterFormId attribute is missing.');
        }

        return view('partials.containers.form-modal')
            ->with('title', 'Save filter')
            ->with('form', [
                'route'  => route('reports.filters.store'),
                'method' => 'POST'
            ])
            ->with('inputs', [
                [
                    'include' => 'partials.fields.input',
                    'params'  => [
                        'label'    => 'Filter Name',
                        'name'     => 'filter_name',
                        'required' => true,
                    ],
                ]
            ])
            ->with('js', [
                'fetch_form' =>  $params['filtersFormId']
            ]);
    }


    /**
     * Ajax Modal Delete Role
     * Delete confirmation
     *
     * @param $params
     * @param $extra_params
     *
     * @return mixed
     * @throws \Exception
     * @internal param $extra_params
     */
    private function ajaxModal_deleteFilter($params, $extra_params)
    {
        // Validate input
        if (!isset($params['filter_id'])) {
            throw new \Exception('Filter Id is missing.');
        }

        return view('pages.delete-confirmation-modal')
            ->with('title', 'Delete bookmark')
            ->with('btn_title', 'Delete bookmark')
            ->with('route', ['reports.filters.destroy', $params['filter_id']]);
    }

}
