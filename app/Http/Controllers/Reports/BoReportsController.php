<?php namespace App\Http\Controllers\Reports;

use App\Entities\Models\Reports\Queries\ReportsQueryField;
use App\Entities\Models\Reports\Query;
use App\Entities\Repositories\Reports\FiltersRepo;
use App\Http\Controllers\Controller;
use App\Http\Middleware\Authenticate;
use App\Http\Traits\DatatableFilterTrait;
use App\Http\Traits\Select2Trait;
use App\Libraries\ReportQueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BoReportsController extends Controller
{
    use Select2Trait;
    use DatatableFilterTrait;

    const PERMISSION = 'reports';

    /**
     * @var array - init assets collection
     */
    protected $assets = ['datatables', 'x-editable', 'form'];

    /**
     * @var array - short code to change in query
     */
    protected $query_fields = [];

    /**
     * @var string - database connection name
     */
    protected $query_db_connect = 'bo';

    /**
     * @var string - query name to report
     */
    protected $report_name = 'bo_search_terms';

    /**
     * @var array
     */
    protected $report_download_time_limit = 5;

    /**
     * @var int - source names in data table source
     */
    protected $report_query_id = -1;

    public function index(Request $request, $report_id)
    {
        $report = Query::find($report_id);
        $this->getReportParams($report);
        $can = $this->buildCanPermissions();

        if (!$can['view']) {
            return redirect()->back()
                ->with(self::MESSAGE_TOAST, ["warning" => "You do not have permissions to do this."]);
        }
        //For specific view in reports 1,3,26
        $lastUpdate = $this->getLastUpdateResult();
        $lastCost = $this->getLastCostResult();

        $builder = ReportsQueryField::where('query_id', $this->report_query_id);

        $default_values = $this->setDefaultValues();
        $canDownload = $this->canDownloadState($can['download']);

        $filters = FiltersRepo::getSavedFilterFields($request->get('filter_id'));
        $view = view('pages.reports.reports-portlet')
            ->withReportId($this->report_query_id)
            ->withReportName($this->report_name)
            ->withDefaultValues($default_values)
            ->withFields(ReportQueryBuilder::buildPortletNavParams($builder, $filters))
            ->withSavedFilters(FiltersRepo::getSavedFilters(auth()->user()->id, $this->report_query_id))
            ->withRouteForm('reports.{report_id}.filter')
            ->withRouteSelect2Ajax('reports.{report_id}.select2')
            ->withSelectedFilterId($request->get('filter_id'))
            ->withSavedFilterFields($filters)
            ->withCanDownload($canDownload)
            ->withLastUpdate($lastUpdate)
            ->withLastCost($lastCost);

        return $view;
    }

    /**
     * Get 'Last Update' result for Report Params
     *
     * @param Query $report
     */
    private function getLastUpdateResult()
    {
        switch ($this->report_query_id) {
            case 1: //BO ADVERTISER REPORT (Last update)
                $query = "select date_sub(`high_water_mark`,INTERVAL -180 MINUTE) from `high_water_mark_tbl` where `table_name` = 'dwh_fact_tracks_mirror'";
                break;
            case 26: //DASH TRACKS REPORT (Last update)
                $query = "select date_sub(`high_water_mark`,INTERVAL -180 MINUTE) from `high_water_mark_tbl` where `table_name` = 'dash_tracks'";
                break;
            case 3: //BO DAILY STATS REPORT (Last conversions update)
                $query = "select date_sub(max(`tracks_update_date`), INTERVAL -180 MINUTE) from `dwh_fact_dailyStats` where `stats_date_tz` = curdate()";
                break;
            default:
                return null;
        }
        return $this->connectWithQuery($query);
    }

    /**
     * Get 'Last Cost' result for Report Params
     *
     */
    private function getLastCostResult(){
        if($this->report_query_id == 3) { //BO DAILY STATS REPORT (Last cost update)
            $query = "select date_sub(max(`publisher_update_date`), INTERVAL -180 MINUTE) from `dwh_fact_dailyStats` where `stats_date_tz` = curdate()";
            return $this->connectWithQuery($query);
        }
        return null;
    }

    /**
     * Connect to the DB and run the query
     * @param $query
     * @return array|mixed
     */
    private function connectWithQuery($query){
        try {
            $connection = DB::connection("dwh");

            # ----- Change Sessions Isolation Level ----
            //$result = $connection->select("SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");

            $result = $connection->select($query);

            /*normalize results that came from db that is not utf8*/
            $result = array_values(json_decode(json_encode($result[0]), true))[0];

            $connection->disconnect($this->query_db_connect);
        } catch (\Exception $e) {
            if (app()->bound('debugbar')) {
                \Debugbar::addMessage($e->getMessage());
            }
            return ['status' => 0, 'message' => "Query Error. Show more info in Debugbar. \n\n {$e->getMessage()}"];
        }

        return $result;
    }


    /**
     * Get Report Params
     *
     * @param Query $report
     */
    private function getReportParams(Query $report)
    {
        if (isset($report->permission) && !auth()->user()->can($report->permission)) {
            $this->can_permission = false;
            return;
        }

        $this->can_permission = true;
        $this->report_name = $report->display_name;
        $this->report_query_id = $report->id;

        $query_fields = json_decode($report->query_fields, true);
        $this->query_fields = $query_fields ? array_values($query_fields['query_fields']) : [];
    }

    /**
     * Select2 Report Where Values
     * This function is used in Select2Trait ('select2_' . $params['method']) as getter method
     *
     * @param $params
     * @return array
     */
    private function select2_reportWhereValues($params)
    {
        $search = $params['query'] ?? $params['value'];
        $column_name = $params['depend_select2_field'] ?? null;
        $multi_select = $params['multi_select'] ?? false;

        $builder = new ReportQueryBuilder();
        return $builder->getValues($column_name, $search, $multi_select);
    }


    /**
     * Select2 Report Having Values
     * This function is used in Select2Trait ('select2_' . $params['method']) as getter method
     *
     * @param $params
     *
     * @return array
     */
    private function select2_reportHavingValues($params)
    {
        $search = $params['query'];
        $column_name = $params['depend_select2_field'];

        $builder = new ReportQueryBuilder();
        return $builder->getValues($column_name, $search);
    }

    /**
     * Convert data results to formatted values
     * convert to percent, add char %, etc.
     *
     * @param $data
     * @return array
     */
    private function datatable_convertData($data)
    {
        if (!isset($data['data'])) { //if data null return
            return $data;
        }

        $select_display_formatters = ReportsQueryField::select('display_name', 'display_format')
            ->where('query_id', $this->report_query_id)
            ->whereNotNull('display_format')
            ->where('clause_type', 'select')
            ->get()
            ->keyBy('display_name')
            ->toArray();

        if (empty($select_display_formatters)) {
            return $data;
        }

        $results = [];
        foreach ($data['data'] as $_record) {
            $_record = (array)$_record;

            foreach ($select_display_formatters as $field_name => $format) {
                if (isset($_record[$field_name])) {
                    $display_format = $format['display_format'];

                    // To avoid using shortcodes lets parse what we can with str_replace
                    if (strpos($format['display_format'], '[self]') !== false && !empty($_record[$format['display_name']])) {
                        $_record[$field_name] = str_replace('[self]', $_record[$format['display_name']],
                            $display_format);
                    }
                }
            }

            $results[] = $_record;
        }

        $data['data'] = $results;
        return $data;
    }

    /**
     * Set Default Values
     * set defualt values for current report
     *
     */
    private function setDefaultValues()
    {
        // default values for group_by clause according to report
        $default_values['group_by'] = ReportQueryBuilder::getDefaultGroupValue($this->report_query_id);

        // default value for where clause - field advertiser_id in Advertisers report set to null
        $where_field = ReportQueryBuilder::getDefaultWhereValue($this->report_query_id);

        $default_values['where']['field'] = $where_field['where'] ?? [];
        $default_values['where']['value'] = $where_field['value'] ?? null;

        return $default_values;
    }


    /**
     *
     * Check if user can download reports from cms
     *
     * cases download can happen:
     * = local-env.
     * = office ips.
     * = can download permission is checked.
     *
     * @param $canDownload
     * @return bool
     */
    private function canDownloadState($canDownload)
    {
        $current_ip = request()->ip();
        $office_ips = config('auth.permitted_ips');


        if(env('APP_ENV') == 'local'){
            return true;
        }
        else if (in_array($current_ip, $office_ips) ) {
            return true;
        }

        else if ($canDownload) {
            return true;
        }
        return false;
    }

}
