<?php

namespace App\Http\Controllers\Reports\ScraperUI;

use App\Entities\Models\Reports\Publishers\BrandScraper\Scraper;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperAdvanceProperties;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperFetcher;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperFetcherProperties;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperProcessor;
use App\Entities\Repositories\Reports\Publishers\BrandPerformance\ScraperRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Reports\BrandScraper\FormBrandScraperRequest;
use Illuminate\Http\Request;

class ProcessorController extends Controller
{
    /**
     * Duplicate processor
     *
     * @param $scraper_id
     * @param $processor_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function duplicate($scraper_id, $processor_id) {
        try {
            $processor = ScraperProcessor::where('id', $processor_id)->get()->toArray()[0];
            if ($processor) {
                $new_processor = new ScraperProcessor();
                $new_processor->fill($processor);
                $new_processor->save();
            }
            return [
                'status' => 1,
                'desc' => 'Processor duplicate successful!'
            ];
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'desc' => 'Processor not found, not duplicated.'
            ];
        }
    }

    /**
     * Delete processor
     *
     * @param $scraper_id
     * @param $processor_id
     * @return array
     */
    public function destroy($scraper_id, $processor_id) {
        try {
            ScraperProcessor::find($processor_id)->delete();

            return [
                'status' => 1,
                'desc' => 'Processor deleted successful!'
            ];
        } catch (\Exception $e) {
            return [
                'status' => 0,
                'desc' => 'Processor not found, not deleted.'
            ];
        }
    }

    /**
     * Save/Update/Delete processors
     *
     * @param Request $request
     * @param $scraper_id
     * @return array
     */
    public function processors(Request $request, $scraper_id)
    {
        $processors = $request->input('params');
        $new = [];
        if ($processors) {
            foreach ($processors as $processor) {
                $is_new = 'new';
                if (isset($processor['id']) && $processor['id'] != 0) {
                    $new_processor = ScraperProcessor::find($processor['id']);
                    $is_new = 'updated';
                } else {
                    $new_processor = new ScraperProcessor();
                    $processor['scraper_id'] = $scraper_id;
                }

                try {
                    $new_processor->fill($processor);
                    $new_processor->save();

                    $new[$is_new][] = $new_processor->id;
                } catch (\Exception $e) {
                    return [
                        'status' => 0,
                        'desc' => 'Error: ' . $e->getMessage()
                    ];
                }
            }
        }

        return [
            'status' => 1,
            'desc' => 'Processors updated successful!',
            'data' => $new
        ];
    }
    
}
