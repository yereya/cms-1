<?php

namespace App\Http\Controllers\Reports\ScraperUI;

use App\Entities\Models\Log;
use App\Entities\Models\Reports\Publishers\BrandScraper\Scraper;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperFetcher;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperFetcherProperties;
use App\Entities\Models\Reports\Publishers\BrandScraper\ScraperProcessor;
use App\Entities\Repositories\Reports\Publishers\BrandPerformance\ScraperRepo;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Libraries\TpLogger;
use DB;
use Illuminate\Http\Request;
use App\Libraries\Scraper\Scraper as ScraperLib;

class FetchersController extends Controller
{
    const PERMISSION = 'scraper';

    /**
     * Display a listing of the resource.
     *
     * @param $scraper_id
     * @param $fetcher_id
     * @return \Illuminate\Http\Response
     */
    public function index($scraper_id, $fetcher_id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $scraper_id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $scraper_id)
    {
        $values = $request->all();
        $values['scraper_id'] = $scraper_id;
        $priority = ScraperFetcher::select(DB::raw('max(`priority`) as priority'))->where('scraper_id', $scraper_id)->first();
        $values['priority'] = $priority->priority + 1;
        $fetcher = ScraperFetcher::create($values);

        // Updates the fetcher properties
        foreach (ScraperFetcher::getPropertyTypes() as $property_name => $vale) {
            $properties = clearAttributesArray(@$values[$property_name]['key'], @$values[$property_name]['value']);

            $fetcher_properties = [];
            foreach ((array) $properties as $key => $value) {
                $fetcher_properties[] = [
                    'fetcher_id' => $fetcher->id,
                    'key' => $key,
                    'value' => $value,
                    'type_field' => ScraperFetcher::getPropertyType($property_name)
                ];
            }

            ScraperFetcherProperties::insert($fetcher_properties);
        }

        return [
            'fetcher' => ScraperFetcher::find($fetcher->id),
            'properties' => ScraperFetcherProperties::where('fetcher_id', $fetcher->id)->get()
        ];
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param $scraper_id
     * @param $fetcher_id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $scraper_id, $fetcher_id)
    {
        ScraperFetcherProperties::where('fetcher_id', $fetcher_id)->delete();

        $values = $request->all();
        // Updates the fetcher
        $fetcher                                = ScraperFetcher::find($fetcher_id);
        $fetcher->fill($values);

        if (isset($values['concat_n_days_results']) AND $values['concat_n_days_results'] > 0) {
            $fetcher->concat_n_days_results = $values['concat_n_days_results'];
        } else {
            $fetcher->concat_n_days_results = NULL;
        }

        $fetcher->save();

        // Updates the fetcher properties
        foreach (ScraperFetcher::getPropertyTypes() as $property_name => $vale) {
            $properties = clearAttributesArray(@$values[$property_name]['key'], @$values[$property_name]['value']);

            $fetcher_properties = [];
            foreach ((array) $properties as $key => $value) {
                $fetcher_properties[] = [
                    'fetcher_id' => $fetcher_id,
                    'key' => $key,
                    'value' => $value,
                    'type_field' => ScraperFetcher::getPropertyType($property_name)
                ];
            }

            ScraperFetcherProperties::insert($fetcher_properties);
        }

        return [
            'fetcher' => ScraperFetcher::find($fetcher->id),
            'properties' => ScraperFetcherProperties::where('fetcher_id', $fetcher->id)->get()
        ];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $scraper_id
     * @param $fetcher
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function destroy($scraper_id, $fetcher)
    {
        if ($fetcher) {
            ScraperFetcher::find($fetcher)->delete();

            return response()->json([
                'status' => 1,
                'desc' => ''
            ]);
        }

        return response()->json([
            'status' => 0,
            'desc' => 'No correct publisher were passed.'
        ]);
    }

    /**
     * @param Request $request
     * @param $scraper_id
     * @param $fetcher_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function actions(Request $request, $scraper_id, $fetcher_id) {
        $_params = [];
        parse_str($request->input('scraper'), $_params['scraper']);
        $_params['scraper']['id'] = $scraper_id;


        // When using test the debug should always be true
        $debug = $request->input('debug');
        if ($debug == null) {
            $debug = 1;
        }
        if (isset($debug) && $debug) {
            $_params['scraper']['debug'] = true;
        } else {
            $_params['scraper']['debug'] = false;
        }

        // Pull fetchers
        if ($fetcher_id != 0) {
            foreach ($request->input('fetchers') as $fetcher) {
                parse_str($fetcher, $_params['fetchers'][]);
            }
        } else {
            $_params['fetchers'] = ScraperFetcher::where('scraper_id', $scraper_id)->get()->toArray();
        }

        // Pull processors
        if ($request->input('processors')) {
            $_params['processors'] = $request->input('processors');
        } else {
            $_params['processors'] = ScraperProcessor::where('scraper_id', $scraper_id)->get()->toArray();
        }

        $scraper_repo = new ScraperRepo();
        $params = [
            'brands_performance' => [
                'fetcher' => [
                    'repo' =>$scraper_repo->with($_params)
                ],
            ]
        ];


        $scraper = new ScraperLib($params);
        $scraper->runRequest('brands_performance');
        $log_id = Log::select('id')->whereNull('parent_id')->where('task_id', $scraper_id)->orderBy('id', 'desc')->limit(1)->get()->pluck('id');

        return [
            'status' => 1,
            'data' => [
                'logId' => isset($log_id[0]) ? $log_id[0] : ''
            ]
        ];
    }

    /**
     * update fetcher orders
     *
     * @param Request $request
     * @return array
     */
    public function priority(Request $request) {
        $fetchers = $request->input('params');

        foreach ($fetchers as $fetcher) {
            $fet = ScraperFetcher::find($fetcher['fetcher_id']);
            $fet->priority = $fetcher['order'];
            $fet->save();
        }

        return [
            'status' => 1,
            'desc' => $request->input('params')
        ];
    }
}
