<?php
namespace App\Http\Controllers;
use App\Entities\Models\CMSUserActions;
use App\Entities\Models\CMSUserActionType;
use App\Http\Traits\DataTableTrait;
use App\Libraries\Datatable\DatatableLib;
use DB;
use Illuminate\Http\Request;

/**
 * Class TaskController
 *
 * @package App\Http\Controllers
 */
class CMSUserActionsController extends Controller
{
    use DataTableTrait;

    const PERMISSION = 'users';

    /**
     * @var string $assets
     */
    protected $assets = ['datatables', 'form'];

    public function exportCsv(Request $request)
    {
        $fileName = 'CMS_User_Actions_'. date("Y-m-d_H:i:s"). '.csv';

        $user_name = $request->get("user_name");
        $to = $request->get("to");
        $to = date("Y-m-d",strtotime($to  ." +1 day"));
        $from = $request->get("from");
        $from = date("Y-m-d",strtotime($from ." -1 day"));
        $actions = DB::table('report_requests_logs');

        $defaultDate = date("Y-m-d",strtotime("-3 month"));
        if($user_name != ".")
            $actions->where("user_name", $user_name);

        if($from == ""){
            $from = $defaultDate;
        }
        if($to == ""){
            $to = new \DateTime();
        }
        $actions->whereBetween('created_at', [$from, $to])->get();

        $actions = $actions->get();

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('id','user_id', 'user_name', 'cms_users_action_id','report_id', 'report_name', 'sql_query','run_duration', 'report_date_from', 'report_date_to','created_at', 'updated_at');

        $callback = function() use($actions, $columns) {
            $typesActions = CMSUserActionType::all()->toArray();
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($actions as $action) {
                $row['id']                      = $action->id;
                $row['user_id']                 = $action->user_id;
                $row['user_name']               = $action->user_name;
                $row['cms_users_action_id']     = $action->cms_users_action_id;
                $row['report_id']               = $action->report_id;
                $row['report_name']             = $action->report_name;
                $row['sql_query']               = $action->sql_query;
                $row['run_duration']            = $action->run_duration;
                $row['report_date_from']        = $action->report_date_from;
                $row['report_date_to']          = $action->report_date_to;
                $row['updated_at']              = $action->updated_at;
                $row['created_at']              = $action->created_at;

                fputcsv($file, array($row['id'],$row['user_id'], $row['user_name'], $typesActions[($action->cms_users_action_id)-1]["type"], $row['report_id'], $row['report_name'],$row['sql_query'], $row['run_duration'], $row['report_date_from'], $row['report_date_to'],$row['created_at'], $row['updated_at']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}