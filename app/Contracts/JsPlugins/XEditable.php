<?php namespace App\Contracts\JsPlugins;

use Illuminate\Http\Request;

interface XEditable
{
    /**
     * Convert the object to its JSON representation.
     *
     * @param Request $request
     * @return string
     */
    public function xEditableApi(Request $request);
}
